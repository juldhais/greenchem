USE [master]
GO
/****** Object:  Database [GreenChem]    Script Date: 03/11/2016 08.43.11 ******/
CREATE DATABASE [GreenChem] ON  PRIMARY 
( NAME = N'GreenChem', FILENAME = N'D:\Database\GreenChem.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GreenChem_log', FILENAME = N'D:\Database\GreenChem_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GreenChem] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GreenChem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GreenChem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GreenChem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GreenChem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GreenChem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GreenChem] SET ARITHABORT OFF 
GO
ALTER DATABASE [GreenChem] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [GreenChem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GreenChem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GreenChem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GreenChem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GreenChem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GreenChem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GreenChem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GreenChem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GreenChem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GreenChem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GreenChem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GreenChem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GreenChem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GreenChem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GreenChem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GreenChem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GreenChem] SET  MULTI_USER 
GO
ALTER DATABASE [GreenChem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GreenChem] SET DB_CHAINING OFF 
GO
USE [GreenChem]
GO
/****** Object:  Table [dbo].[AddEntitlement]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddEntitlement](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AddToAll] [bit] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[idEmployee] [int] NULL,
	[idLeaveType] [int] NULL,
	[Entitlement] [int] NOT NULL,
 CONSTRAINT [PK_AddEntitlement] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AdjustStock]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdjustStock](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idProduct] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](500) NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Cost] [money] NOT NULL,
	[Subtotal] [money] NOT NULL,
 CONSTRAINT [PK_AdjustStock] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Approval]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Approval](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idTransaction] [int] NULL,
	[Module] [varchar](50) NULL,
	[Approved] [bit] NOT NULL,
	[DateApproved] [datetime] NULL,
 CONSTRAINT [PK_Approval] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Attachment]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Attachment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](500) NULL,
	[FileData] [varbinary](max) NULL,
 CONSTRAINT [PK_Attachment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AutoNumber]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoNumber](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idUserAccount] [int] NULL,
	[idSupplier] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](1000) NULL,
	[Status] [varchar](50) NULL,
	[ETA] [datetime] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[AutoNumber] ADD [DeliveryStatus] [varchar](50) NULL
ALTER TABLE [dbo].[AutoNumber] ADD [DateDelivered] [datetime] NULL
 CONSTRAINT [PK_AutoNumber] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BahanBaku]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BahanBaku](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nomor] [varchar](50) NULL,
	[Nama] [varchar](100) NULL,
	[Kode] [varchar](50) NULL,
	[idFisik] [int] NULL,
	[idTipe] [int] NULL,
	[Fungsi] [varchar](100) NULL,
	[idSupplier] [int] NULL,
	[Web] [varchar](100) NULL,
	[Telepon] [varchar](100) NULL,
	[Person] [varchar](100) NULL,
	[Active] [bit] NOT NULL,
	[KodeMSDS] [varchar](100) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[BahanBaku] ADD [StatusBahanBaku] [varchar](50) NULL
ALTER TABLE [dbo].[BahanBaku] ADD [idSupplier2] [int] NULL
ALTER TABLE [dbo].[BahanBaku] ADD [idSupplier3] [int] NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[BahanBaku] ADD [AttachmentMSDS] [varchar](500) NULL
 CONSTRAINT [PK_BahanBaku] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BahanPenolong]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BahanPenolong](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nomor] [varchar](50) NULL,
	[Kode] [varchar](50) NULL,
	[Nama] [varchar](100) NULL,
	[idKategoriBahanPenolong] [int] NULL,
	[idStatusBahanPenolong] [int] NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[BahanPenolong] ADD [Specification] [varchar](500) NULL
ALTER TABLE [dbo].[BahanPenolong] ADD [AttachmentPicture] [varchar](500) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[BahanPenolong] ADD [Attachment] [varchar](500) NULL
 CONSTRAINT [PK_BahanPenolong] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BankGiro]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[BankGiro](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
 CONSTRAINT [PK_BankGiro] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BiayaKomposisi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[BiayaKomposisi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idBarang] [int] NULL,
	[Keterangan] [varchar](100) NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Harga] [money] NOT NULL,
	[Subtotal] [money] NOT NULL,
 CONSTRAINT [PK_BiayaKomposisi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BlackListBarang]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlackListBarang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idBarang] [int] NULL,
	[idPelanggan] [int] NULL,
 CONSTRAINT [PK_BlackListBarang] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Configuration](
	[K] [varchar](50) NOT NULL,
	[V] [varchar](500) NULL,
 CONSTRAINT [PK_Configuration_1] PRIMARY KEY CLUSTERED 
(
	[K] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idGroup] [int] NULL,
	[Code] [varchar](50) NULL,
	[Title] [varchar](50) NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Address] [varchar](500) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[Country] [varchar](100) NULL,
	[ZipCode] [varchar](50) NULL,
	[Phone1] [varchar](100) NULL,
	[Phone2] [varchar](100) NULL,
	[Mobile1] [varchar](100) NULL,
	[Mobile2] [varchar](100) NULL,
	[Fax] [varchar](100) NULL,
	[Website] [varchar](100) NULL,
	[Email1] [varchar](100) NULL,
	[Email2] [varchar](100) NULL,
	[CompanyName] [varchar](100) NULL,
	[AccountNumber] [varchar](100) NULL,
	[TaxNumber] [varchar](100) NULL,
	[RegisterNumber] [varchar](100) NULL,
	[Remarks] [varchar](500) NULL,
	[Image] [image] NULL,
	[Active] [bit] NOT NULL,
	[idPriority] [int] NULL,
	[idUserAccount] [int] NULL,
	[Position] [varchar](100) NULL,
	[HomeAddress] [varchar](500) NULL,
	[idUserAccount2] [int] NULL,
	[idUserAccount3] [int] NULL,
	[DateModified] [datetime] NULL,
	[idModifiedBy] [int] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Courier]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Courier](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Address] [varchar](500) NULL,
	[Phone] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[Active] [bit] NOT NULL,
	[SiteAddress] [varchar](500) NULL,
	[PIC] [varchar](50) NULL,
	[ExpeditionPhone] [varchar](50) NULL,
 CONSTRAINT [PK_Courier] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Currency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Symbol] [varchar](50) NULL,
	[Rate] [decimal](18, 4) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](100) NULL,
	[Address] [varchar](500) NULL,
	[Phone] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Customer] ADD [Fax] [varchar](100) NULL
ALTER TABLE [dbo].[Customer] ADD [Email] [varchar](100) NULL
ALTER TABLE [dbo].[Customer] ADD [ContactPerson] [varchar](500) NULL
ALTER TABLE [dbo].[Customer] ADD [ShipAddress] [varchar](500) NULL
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerComplaint]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerComplaint](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCustomer] [int] NULL,
	[idUserAccount] [int] NULL,
	[TransactionDate] [datetime] NOT NULL,
	[ComplaintDescription] [varchar](max) NULL,
	[DepositionTo] [varchar](100) NULL,
	[DueDate] [datetime] NULL,
	[Category] [varchar](100) NULL,
	[Action] [varchar](max) NULL,
	[ClosingDate] [datetime] NULL,
	[Status] [varchar](50) NULL,
	[PIC] [varchar](100) NULL,
	[NoBatch] [varchar](100) NULL,
	[VoiceDetail] [varchar](max) NULL,
 CONSTRAINT [PK_CustomerComplaint] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerSatisfaction]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerSatisfaction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCustomer] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[CustomerSupport] [varchar](50) NULL,
	[Time] [varchar](50) NULL,
	[Quality] [varchar](50) NULL,
	[TechnicalSupport] [varchar](50) NULL,
	[Suggestion] [varchar](max) NULL,
	[PIC] [varchar](100) NULL,
 CONSTRAINT [PK_CustomerSatisfaction] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerVoice]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerVoice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCustomer] [int] NULL,
	[idUserAccount] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[ComplaintDescription] [varchar](max) NULL,
	[DepositionTo] [varchar](100) NULL,
	[DueDate] [datetime] NULL,
	[Category] [varchar](100) NULL,
	[Action] [varchar](max) NULL,
	[ClosingDate] [datetime] NULL,
	[Status] [varchar](50) NULL,
	[CustomerSupport] [varchar](50) NULL,
	[Time] [varchar](50) NULL,
	[Quality] [varchar](50) NULL,
	[TechnicalSupport] [varchar](50) NULL,
	[Suggestion] [varchar](max) NULL,
	[PIC] [varchar](100) NULL,
	[CustomerName] [varchar](500) NULL,
	[CustomerPhone] [varchar](500) NULL,
	[CustomerFax] [varchar](500) NULL,
	[CustomerEmail] [varchar](500) NULL,
 CONSTRAINT [PK_CustomerVoice] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Department]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailInquiryIn]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailInquiryIn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idInquiryIn] [int] NULL,
	[idProduct] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Subtotal] [money] NOT NULL,
	[ProductName] [varchar](100) NULL,
 CONSTRAINT [PK_DetailInquiryIn] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPacking]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[DetailPacking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPacking] [int] NULL,
	[idBarang] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Isi] [decimal](18, 4) NULL,
	[Satuan] [varchar](50) NULL,
	[Box] [varchar](50) NULL,
 CONSTRAINT [PK_DetailPacking] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPemakaianVoucher]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetailPemakaianVoucher](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPemakaianVoucher] [int] NULL,
	[idVoucher] [int] NULL,
	[Jumlah] [money] NOT NULL,
 CONSTRAINT [PK_DetailPemakaianVoucher] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetailPemakaianVoucherPenjualan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetailPemakaianVoucherPenjualan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPemakaianVoucher] [int] NULL,
	[idPenjualan] [int] NULL,
	[Jumlah] [money] NOT NULL,
 CONSTRAINT [PK_DetailPemakaianVoucherPenjualan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetailPenyerahanProduksi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPenyerahanProduksi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPenyerahanProduksi] [int] NULL,
	[idProduct] [int] NULL,
	[Kemasan] [varchar](100) NULL,
	[Quantity] [decimal](18, 4) NULL,
	[Ratio] [decimal](18, 4) NULL,
	[UoM] [varchar](50) NULL,
 CONSTRAINT [PK_DetailPenyerahanProduksi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPermintaanProduksi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPermintaanProduksi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPermintaanProduksi] [int] NULL,
	[idProduct] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Packing] [varchar](500) NULL,
 CONSTRAINT [PK_DetailPermintaanProduksi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPOtoSupplierExpedisi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPOtoSupplierExpedisi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPOtoSupplierExpedisi] [int] NULL,
	[NamaBarang] [varchar](500) NULL,
	[NilaiBarang] [money] NOT NULL,
	[BeratDimensi] [decimal](18, 4) NOT NULL,
 CONSTRAINT [PK_DetailPOtoSupplierExpedisi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPOtoSupplierRegular]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPOtoSupplierRegular](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPOtoSupplierRegular] [int] NULL,
	[idBahanBaku] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[UoM] [varchar](50) NULL,
	[Price] [money] NOT NULL,
	[Subtotal] [money] NOT NULL,
	[idBahanPenolong] [int] NULL,
 CONSTRAINT [PK_DetailPOtoSupplierRegular] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPPB]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPPB](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPPB] [int] NULL,
	[idProduct] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Subtotal] [money] NOT NULL,
	[ProductName] [varchar](200) NULL,
	[UoM] [varchar](100) NULL,
 CONSTRAINT [PK_DetailPPB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailPPS]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailPPS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPPS] [int] NULL,
	[idProduct] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Subtotal] [money] NOT NULL,
	[ProductName] [varchar](200) NULL,
	[UoM] [varchar](100) NULL,
 CONSTRAINT [PK_DetailPPS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailQuotationIn]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailQuotationIn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idQuotationIn] [int] NULL,
	[idProduct] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Subtotal] [money] NOT NULL,
	[ProductName] [varchar](100) NULL,
 CONSTRAINT [PK_DetailQuotationIn] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailQuotationOut]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetailQuotationOut](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idQuotationOut] [int] NULL,
	[idProduct] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [decimal](18, 4) NOT NULL,
	[Subtotal] [money] NOT NULL,
 CONSTRAINT [PK_DetailQuotationOut] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetailSalesOrder]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetailSalesOrder](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idSalesOrder] [int] NULL,
	[idProduct] [int] NULL,
	[QuantityOrder] [decimal](18, 4) NOT NULL,
	[QuantityDelivered] [decimal](18, 4) NOT NULL,
	[QuantityReturn] [decimal](18, 4) NOT NULL,
	[Cost] [money] NOT NULL,
	[PriceOrder] [money] NOT NULL,
	[PriceDelivered] [money] NOT NULL,
	[ReturnPrice] [money] NOT NULL,
	[DiscountOrder] [decimal](18, 4) NOT NULL,
	[DiscountDelivered] [decimal](18, 4) NOT NULL,
	[DiscountReturn] [decimal](18, 4) NOT NULL,
	[SubtotalOrder] [money] NOT NULL,
	[SubtotalDelivered] [money] NOT NULL,
	[SubtotalReturn] [money] NOT NULL,
	[PriceLevel] [varchar](50) NULL,
	[Ratio] [decimal](18, 4) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[DetailSalesOrder] ADD [UoM] [varchar](50) NULL
ALTER TABLE [dbo].[DetailSalesOrder] ADD [Packaging] [varchar](100) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[DetailSalesOrder] ADD [NoBatch] [varchar](100) NULL
ALTER TABLE [dbo].[DetailSalesOrder] ADD [IsDelivered] [bit] NULL
 CONSTRAINT [PK_DetailSalesOrder] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetailTukarFaktur]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetailTukarFaktur](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idTukarFaktur] [int] NULL,
	[idPenjualan] [int] NULL,
 CONSTRAINT [PK_DetailTukarFaktur] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DiskonGrosir]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[DiskonGrosir](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[MinItem] [int] NOT NULL,
	[MinQty] [decimal](18, 4) NOT NULL,
	[LevelHarga] [varchar](50) NULL,
 CONSTRAINT [PK_DiskonGrosir] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DownloadDetailPenjualan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[DownloadDetailPenjualan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDownloadPenjualan] [int] NULL,
	[idKomponen] [int] NULL,
	[LevelHarga] [varchar](1) NULL,
	[idBarang] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Satuan] [varchar](50) NULL,
	[Isi] [decimal](18, 4) NOT NULL,
	[HargaBeli] [money] NOT NULL,
	[HargaJual] [money] NOT NULL,
	[DiskonPersen] [decimal](18, 4) NOT NULL,
	[Diskon] [money] NOT NULL,
	[Subtotal] [money] NOT NULL,
	[DiskonPersen2] [decimal](18, 4) NULL,
	[DiskonPersen3] [decimal](18, 4) NULL,
	[DiskonPersen4] [decimal](18, 4) NULL,
	[DiskonPersen5] [decimal](18, 4) NULL,
 CONSTRAINT [PK_DownloadDetailPenjualan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DownloadPenjualan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[DownloadPenjualan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPengguna] [int] NULL,
	[idDepartment] [int] NULL,
	[idPelanggan] [int] NULL,
	[idSalesman] [int] NULL,
	[idPesananPenjualan] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[Tanggal] [datetime] NOT NULL,
	[JatuhTempo] [datetime] NULL,
	[Keterangan] [varchar](500) NULL,
	[Subtotal] [money] NOT NULL,
	[Diskon] [money] NOT NULL,
	[Total] [money] NOT NULL,
	[Sisa] [money] NOT NULL,
	[Biaya] [money] NULL,
	[Keterangan2] [varchar](500) NULL,
	[Stok] [int] NULL,
	[Cetak] [int] NULL,
	[Koreksi] [int] NULL,
	[idMobil] [int] NULL,
 CONSTRAINT [PK_DownloadPenjualan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [varchar](500) NULL,
	[IDNo] [varchar](50) NULL,
	[Gender] [varchar](50) NULL,
	[MaritalStatus] [varchar](50) NULL,
	[Nationality] [varchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[KTP] [varchar](50) NULL,
	[SIMA] [varchar](50) NULL,
	[SIMB] [varchar](50) NULL,
	[SIMC] [varchar](50) NULL,
	[Passport] [varchar](50) NULL,
	[NPWP] [varchar](50) NULL,
	[Photo] [image] NULL,
	[Status] [varchar](50) NULL,
	[Address] [varchar](max) NULL,
	[Phone1] [varchar](50) NULL,
	[Phone2] [varchar](50) NULL,
	[WorkEmail] [varchar](50) NULL,
	[OtherEmail] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[idPayGrade] [int] NULL,
	[SalaryAmount] [money] NOT NULL,
	[Remarks] [varchar](500) NULL,
	[AccountNumber] [varchar](50) NULL,
	[Bank] [varchar](50) NULL,
	[Branch] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
	[LeaveEntitlement] [int] NULL,
	[idPosition] [int] NULL,
	[TerminateReason] [varchar](500) NULL,
	[TerminateDate] [datetime] NULL,
	[idUserAccount] [int] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeEducation]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeEducation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idEmployee] [int] NULL,
	[EducationLevel] [varchar](50) NULL,
	[Institute] [varchar](50) NULL,
	[Major] [varchar](50) NULL,
	[Year] [int] NULL,
	[Score] [decimal](18, 4) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_EmployeeEducation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeEmergencyContact]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeEmergencyContact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idEmployee] [int] NULL,
	[Name] [varchar](500) NULL,
	[Relationship] [varchar](50) NULL,
	[Phone1] [varchar](50) NULL,
	[Phone2] [varchar](50) NULL,
 CONSTRAINT [PK_EmployeeEmergencyContact] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeePosition]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeePosition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_EmployeePosition] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeSkill]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeSkill](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idEmployee] [int] NULL,
	[Year] [int] NULL,
	[Languange] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
 CONSTRAINT [PK_EmployeeSkill] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeWorkExperience]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeWorkExperience](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idEmployee] [int] NULL,
	[CompanyName] [varchar](50) NULL,
	[JobTitle] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Remarks] [varchar](500) NULL,
 CONSTRAINT [PK_EmployeeWorkExperience] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Expense]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Expense](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](500) NULL,
	[Amount] [money] NOT NULL,
	[idSalesman] [int] NULL,
	[idCustomer] [int] NULL,
 CONSTRAINT [PK_Expense] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Fisik]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fisik](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Fisik] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FixSisaPiutang]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FixSisaPiutang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPenjualan] [int] NULL,
	[Sisa] [money] NOT NULL,
 CONSTRAINT [PK_FixSisaPiutang] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GajiKaryawan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[GajiKaryawan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPengguna] [int] NULL,
	[idKaryawan] [int] NULL,
	[Tanggal] [datetime] NOT NULL,
	[Tipe] [varchar](50) NULL,
	[Gaji] [money] NOT NULL,
	[Kasbon] [money] NOT NULL,
	[Cicilan] [money] NOT NULL,
	[Bonus] [money] NOT NULL,
	[Potongan] [money] NOT NULL,
	[Total] [money] NOT NULL,
 CONSTRAINT [PK_GajiKaryawan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneralLetter]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GeneralLetter](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](500) NULL,
	[FileName] [varchar](500) NULL,
 CONSTRAINT [PK_GeneralLetters] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GiroKeluar]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[GiroKeluar](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPengguna] [int] NULL,
	[idBank] [int] NULL,
	[idPemasok] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[TanggalTerima] [datetime] NOT NULL,
	[TanggalCair] [datetime] NOT NULL,
	[Keterangan] [varchar](500) NULL,
	[Jumlah] [money] NOT NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_GiroKeluar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GiroMasuk]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[GiroMasuk](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPengguna] [int] NULL,
	[idBank] [int] NULL,
	[idPelanggan] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[TanggalTerima] [datetime] NOT NULL,
	[TanggalCair] [datetime] NOT NULL,
	[Keterangan] [varchar](500) NULL,
	[Jumlah] [money] NOT NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_GiroMasuk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GiroPembayaranHutang]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GiroPembayaranHutang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NomorHutang] [varchar](50) NULL,
	[NomorGiro] [varchar](50) NULL,
	[BankGiro] [varchar](50) NULL,
	[TanggalCair] [datetime] NOT NULL,
	[Jumlah] [money] NOT NULL,
 CONSTRAINT [PK_GiroPembayaranHutang] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GiroPembayaranPiutang]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GiroPembayaranPiutang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NomorPiutang] [varchar](50) NULL,
	[NomorGiro] [varchar](50) NULL,
	[BankGiro] [varchar](50) NULL,
	[TanggalCair] [datetime] NOT NULL,
	[Jumlah] [money] NOT NULL,
 CONSTRAINT [PK_GiroPembayaranPiutang] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GrupPelanggan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[GrupPelanggan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
	[Aktif] [bit] NULL,
 CONSTRAINT [PK_GrupPelanggan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HargaJual]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[HargaJual](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idBarang] [int] NULL,
	[Satuan] [varchar](50) NULL,
	[Isi] [decimal](18, 4) NOT NULL,
	[MinQty] [decimal](18, 4) NOT NULL,
	[Harga] [money] NOT NULL,
 CONSTRAINT [PK_HargaJual] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HargaPelanggan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HargaPelanggan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPelanggan] [int] NULL,
	[idBarang] [int] NULL,
	[HargaJual] [money] NOT NULL,
 CONSTRAINT [PK_HargaPelanggan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HistoryGajiKaryawan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[HistoryGajiKaryawan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idKaryawan] [int] NULL,
	[Tanggal] [datetime] NOT NULL,
	[Keterangan] [varchar](500) NULL,
	[GajiHarian] [money] NOT NULL,
	[GajiMingguan] [money] NOT NULL,
	[GajiBulanan] [money] NOT NULL,
 CONSTRAINT [PK_HistoryGajiKaryawan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HistoryPelanggan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[HistoryPelanggan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPelanggan] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[Tanggal] [datetime] NOT NULL,
	[Custom1] [varchar](500) NULL,
	[Custom2] [varchar](500) NULL,
	[Custom3] [varchar](500) NULL,
	[Custom4] [varchar](500) NULL,
	[Custom5] [varchar](500) NULL,
	[Custom6] [varchar](500) NULL,
	[Custom7] [varchar](500) NULL,
	[Custom8] [varchar](500) NULL,
	[Custom9] [varchar](500) NULL,
	[Custom10] [varchar](500) NULL,
	[Custom11] [varchar](500) NULL,
	[Custom12] [varchar](500) NULL,
 CONSTRAINT [PK_HistoryPelanggan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Holiday]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Holiday](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](100) NULL,
 CONSTRAINT [PK_Holiday] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InquiryIn]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InquiryIn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idUserAccount] [int] NULL,
	[idCustomer] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](1000) NULL,
 CONSTRAINT [PK_InquiryIn] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InquiryOut]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InquiryOut](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idUserAccount] [int] NULL,
	[idSupplier] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](1000) NULL,
 CONSTRAINT [PK_InquiryOut] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InternalComplaint]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InternalComplaint](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Description] [varchar](max) NULL,
	[Deposition] [varchar](max) NULL,
	[DueDate] [datetime] NULL,
	[Action] [varchar](max) NULL,
	[ClosingDate] [datetime] NULL,
	[Status] [varchar](50) NULL,
	[Suggestion] [varchar](max) NULL,
 CONSTRAINT [PK_InternalComplaint] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InternalLetter]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InternalLetter](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](500) NULL,
	[FileName] [varchar](500) NULL,
 CONSTRAINT [PK_InternalLetters] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Karyawan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Karyawan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](100) NULL,
	[Jabatan] [varchar](50) NULL,
	[JenisKelamin] [varchar](50) NULL,
	[Agama] [varchar](50) NULL,
	[NoKTP] [varchar](50) NULL,
	[Alamat] [varchar](max) NULL,
	[Telepon] [varchar](100) NULL,
	[GajiHarian] [money] NOT NULL,
	[GajiMingguan] [money] NOT NULL,
	[GajiBulanan] [money] NOT NULL,
	[Hutang] [money] NOT NULL,
	[CicilanHarian] [money] NOT NULL,
	[CicilanMingguan] [money] NOT NULL,
	[CicilanBulanan] [money] NOT NULL,
	[THR] [money] NOT NULL,
	[Aktif] [bit] NOT NULL,
	[Kasbon] [money] NULL,
	[Pinjaman] [money] NULL,
 CONSTRAINT [PK_Karyawan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KasbonKaryawan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[KasbonKaryawan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idKaryawan] [int] NULL,
	[Tanggal] [datetime] NOT NULL,
	[Keterangan] [varchar](max) NOT NULL,
	[Jumlah] [money] NOT NULL,
	[Lunas] [bit] NOT NULL,
 CONSTRAINT [PK_KasbonKaryawan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KategoriBahanPenolong]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KategoriBahanPenolong](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_KategoriBahanPenolong] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KesimpulanQC]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KesimpulanQC](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_KesimpulanQC] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kode1]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Kode1](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
 CONSTRAINT [PK_Kode1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kode2]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Kode2](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
 CONSTRAINT [PK_Kode2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kode3]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Kode3](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
 CONSTRAINT [PK_Kode3] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KomisiSalesman]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[KomisiSalesman](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Keterangan] [varchar](100) NULL,
	[idSalesman] [int] NULL,
	[idKategori] [int] NULL,
	[idJenis] [int] NULL,
	[idMerk] [int] NULL,
	[idGrup] [int] NULL,
	[idPemasok] [int] NULL,
	[idBarang] [int] NULL,
	[KomisiPersenKecil] [decimal](18, 4) NOT NULL,
	[KomisiPersenSedang] [decimal](18, 4) NOT NULL,
	[KomisiPersenBesar] [decimal](18, 4) NOT NULL,
	[KomisiRupiahKecil] [money] NOT NULL,
	[KomisiRupiahSedang] [money] NOT NULL,
	[KomisiRupiahBesar] [money] NOT NULL,
 CONSTRAINT [PK_KomisiSalesman] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Komposisi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Komposisi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idBarang] [int] NULL,
	[idBahanBaku] [int] NULL,
	[Persentase] [decimal](18, 4) NOT NULL,
	[Harga] [money] NOT NULL,
	[Variable1] [decimal](18, 4) NULL,
	[Variable2] [decimal](18, 4) NULL,
	[Variable3] [decimal](18, 4) NULL,
	[Variable4] [decimal](18, 4) NULL,
	[Variable5] [decimal](18, 4) NULL,
	[PersenVariable1] [decimal](18, 4) NULL,
	[PersenVariable2] [decimal](18, 4) NULL,
	[PersenVariable3] [decimal](18, 4) NULL,
	[PersenVariable4] [decimal](18, 4) NULL,
	[PersenVariable5] [decimal](18, 4) NULL,
	[Quantity] [decimal](18, 4) NULL,
	[TotalHarga] [money] NOT NULL,
 CONSTRAINT [PK_Komposisi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kriteria]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Kriteria](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
 CONSTRAINT [PK_Kriteria] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LabService]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LabService](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TransactionNumber] [varchar](50) NULL,
	[SE] [varchar](100) NULL,
	[TanggalPermintaan] [datetime] NOT NULL,
	[TanggalApproval] [datetime] NULL,
	[TargetLama] [varchar](100) NULL,
	[TanggalSelesai] [datetime] NULL,
	[LamaRiset] [varchar](100) NULL,
	[PermintaanLabService] [varchar](500) NULL,
	[HasilLabService] [varchar](500) NULL,
	[Status] [varchar](50) NULL,
	[Attachment] [varchar](500) NULL,
 CONSTRAINT [PK_LabService] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeaveType]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeaveType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_LeaveType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeaveUsage]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeaveUsage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idEmployee] [int] NULL,
	[idLeaveType] [int] NULL,
	[LeaveBalance] [int] NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NOT NULL,
	[LeaveDay] [int] NOT NULL,
	[Remarks] [varchar](500) NULL,
 CONSTRAINT [PK_LeaveUsage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LimitPiutang]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LimitPiutang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPelanggan] [int] NULL,
	[idKategori] [int] NULL,
	[Limit] [money] NOT NULL,
 CONSTRAINT [PK_LimitPiutang] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Location]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](100) NULL,
	[Remarks] [varchar](500) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LookupTable]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LookupTable](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
	[Tabel] [varchar](50) NULL,
	[Kolom] [varchar](50) NULL,
 CONSTRAINT [PK_LookupTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mobil]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Mobil](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
	[Keterangan] [varchar](500) NULL,
	[Aktif] [bit] NOT NULL,
 CONSTRAINT [PK_Mobil] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Nationality]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nationality](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Nationality] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Packaging]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Packaging](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_Packaging] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Packing]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Packing](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPesananPenjualan] [int] NULL,
	[idPengguna] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[Tanggal] [datetime] NOT NULL,
	[Status] [varchar](50) NULL,
	[Keterangan] [varchar](500) NULL,
 CONSTRAINT [PK_Packing] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaletAktualQC]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaletAktualQC](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_PaletAktualQC] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PayGrade]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PayGrade](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_PayGrade] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PemakaianVoucher]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PemakaianVoucher](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPelanggan] [int] NULL,
	[idPengguna] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[Tanggal] [datetime] NOT NULL,
 CONSTRAINT [PK_PemakaianVoucher] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PenagihanPiutang]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PenagihanPiutang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPelanggan] [int] NULL,
	[idPenjualan] [int] NULL,
	[idBank] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[Tanggal] [datetime] NOT NULL,
	[Total] [money] NOT NULL,
	[Potongan] [money] NOT NULL,
	[Bayar] [money] NOT NULL,
	[Sisa] [money] NOT NULL,
 CONSTRAINT [PK_PenagihanPiutang] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PengembanganProduk]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PengembanganProduk](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NoPRD] [varchar](50) NULL,
	[Judul] [varchar](max) NULL,
	[TanggalPermintaan] [datetime] NULL,
	[TanggalApproval] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[TanggalSelesai] [datetime] NULL,
	[LamaRiset] [int] NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_PengembanganProduk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PenyerahanProduksi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PenyerahanProduksi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[NomorPermintaanProduksi] [varchar](50) NULL,
	[TanggalPenyerahan] [datetime] NULL,
	[TanggalPermintaanProduksi] [datetime] NULL,
	[TanggalSelesaiProduksi] [datetime] NULL,
	[YangMenyerahkan] [varchar](50) NULL,
	[DisetujuiOleh] [varchar](50) NULL,
	[DiterimaOleh] [varchar](50) NULL,
	[Stock] [varchar](50) NULL,
	[Approved] [bit] NULL,
	[DateApproved] [datetime] NULL,
	[ApprovedBy] [int] NULL,
	[Approved2] [bit] NULL,
	[DateApproved2] [datetime] NULL,
	[ApprovedBy2] [int] NULL,
	[idPermintaanProduksi] [int] NULL,
 CONSTRAINT [PK_PenyerahanProduksi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PermintaanProduksi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PermintaanProduksi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](max) NULL,
	[TanggalDibutuhkan] [datetime] NULL,
	[PersyaratanPelaggan] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[ApprovedBy] [int] NULL,
	[DateApproved] [datetime] NULL,
	[NoRef] [varchar](50) NULL,
 CONSTRAINT [PK_PermintaanProduksi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PerubahanHargaBeli]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PerubahanHargaBeli](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nomor] [varchar](50) NULL,
	[idBarang] [int] NULL,
	[HargaBefore] [money] NOT NULL,
	[HargaAfter] [money] NOT NULL,
 CONSTRAINT [PK_PerubahanHargaBeli] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PinjamanKaryawan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PinjamanKaryawan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idKaryawan] [int] NULL,
	[Tanggal] [datetime] NOT NULL,
	[Keterangan] [varchar](max) NULL,
	[Jumlah] [money] NOT NULL,
	[CicilanHarian] [money] NOT NULL,
	[CicilanMingguan] [money] NOT NULL,
	[CicilanBulanan] [money] NOT NULL,
	[Lunas] [bit] NOT NULL,
 CONSTRAINT [PK_PinjamanKaryawan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[POtoSupplierExpedisi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POtoSupplierExpedisi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[NomorPO] [varchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[JatuhTempo] [varchar](50) NULL,
	[Untuk] [varchar](500) NULL,
	[Address] [varchar](500) NULL,
	[PIC] [varchar](500) NULL,
	[Phone] [varchar](500) NULL,
	[SendTo] [varchar](500) NULL,
	[SendToAddress] [varchar](500) NULL,
	[DONumber] [varchar](50) NULL,
	[Transportation] [varchar](50) NULL,
	[Subtotal] [money] NOT NULL,
	[BeratDimensi] [decimal](18, 4) NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[TotalBiayaExpedisi] [money] NOT NULL,
	[PPNPercent] [decimal](18, 4) NOT NULL,
	[PPNAmount] [money] NOT NULL,
	[AsuransiPercent] [decimal](18, 4) NOT NULL,
	[AsuransiAmount] [money] NOT NULL,
	[BiayaPalletized] [money] NOT NULL,
	[BiayaHandlingIn] [money] NOT NULL,
	[Total] [money] NOT NULL,
	[Vehicle] [varchar](500) NULL,
	[Approved] [bit] NULL,
	[DateApproved] [datetime] NULL,
	[ApprovedBy] [int] NULL,
 CONSTRAINT [PK_POtoSupplierExpedisi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[POtoSupplierRegular]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POtoSupplierRegular](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[NomorPO] [varchar](50) NULL,
	[NomorPB] [varchar](50) NULL,
	[Date] [datetime] NULL,
	[RFQ] [varchar](500) NULL,
	[idSupplier] [int] NULL,
	[Kurs] [varchar](500) NULL,
	[Terms] [varchar](500) NULL,
	[Remarks] [varchar](500) NULL,
	[ShippingAddress] [varchar](500) NULL,
	[BillingAddress] [varchar](500) NULL,
	[Subtotal] [money] NOT NULL,
	[PPNPercent] [decimal](18, 4) NOT NULL,
	[PPNAmount] [money] NOT NULL,
	[Total] [money] NOT NULL,
	[Approved] [bit] NULL,
	[DateApproved] [datetime] NULL,
	[ApprovedBy] [int] NULL,
 CONSTRAINT [PK_POtoSupplierRegular] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PPB]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PPB](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idCustomer] [int] NULL,
	[idProduct] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[PONumber] [varchar](50) NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Packaging] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
 CONSTRAINT [PK_PPB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PPS]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PPS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idCustomer] [int] NULL,
	[idProduct] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[PONumber] [varchar](50) NULL,
	[Division] [varchar](50) NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[Packaging] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[TanggalPenerimaan] [datetime] NULL,
 CONSTRAINT [PK_PPS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PrintCount]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrintCount](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Report] [varchar](500) NULL,
	[TransactionNumber] [varchar](500) NULL,
	[Count] [int] NOT NULL,
 CONSTRAINT [PK_PrintCount] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Priority]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Priority](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Priority] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCategory] [int] NULL,
	[idSupplier] [int] NULL,
	[idCurrency] [int] NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](100) NULL,
	[Status] [varchar](50) NULL,
	[UoM] [varchar](50) NULL,
	[SecondUoM] [varchar](50) NULL,
	[Ratio] [decimal](18, 4) NULL,
	[Cost] [money] NOT NULL,
	[SalesPrice] [money] NOT NULL,
	[SalesPrice2] [money] NULL,
	[SalesPrice3] [money] NULL,
	[SalesPrice4] [money] NULL,
	[SalesPrice5] [money] NULL,
	[Stock] [decimal](18, 4) NOT NULL,
	[Remarks] [varchar](500) NULL,
	[Active] [bit] NOT NULL,
	[AttachemntMSDS] [varchar](500) NULL,
	[AttachmentPI] [varchar](500) NULL,
	[StatusProduct] [varchar](50) NULL,
	[ThirdUOM] [varchar](50) NULL,
	[RatioThird] [decimal](18, 4) NULL,
	[Stock2] [decimal](18, 4) NULL,
	[StokMin] [decimal](18, 4) NULL,
	[StokMax] [decimal](18, 4) NULL,
	[DateModified] [datetime] NULL,
	[UserModified] [int] NULL,
	[PLUserModified] [int] NULL,
	[PLDateModified] [datetime] NULL,
	[TransitStock] [decimal](18, 4) NULL,
	[Stock3] [decimal](18, 4) NULL,
	[Stok4] [decimal](18, 4) NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProjectCategory]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectCategory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_ProjectCategory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProjectList]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectList](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idProjectCategory] [int] NULL,
	[idCustomer] [int] NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Description] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
	[idParent] [int] NULL,
 CONSTRAINT [PK_ProjectList] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProjectProgress]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectProgress](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idProjectList] [int] NULL,
	[idUserAccount] [int] NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Description] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_ProjectProgress] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProjectUserAccount]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectUserAccount](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idProjectList] [int] NULL,
	[idUserAccount] [int] NULL,
	[idSalesman] [int] NULL,
 CONSTRAINT [PK_ProjectUserAccount] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PromoTotalFaktur]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PromoTotalFaktur](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DariTanggal] [datetime] NOT NULL,
	[HinggaTanggal] [datetime] NOT NULL,
	[TotalFaktur] [money] NOT NULL,
	[idBarangBonus] [int] NULL,
	[QuantityBonus] [decimal](18, 4) NOT NULL,
	[IsiBonus] [decimal](18, 4) NOT NULL,
	[SatuanBonus] [varchar](50) NULL,
	[Keterangan] [varchar](100) NULL,
 CONSTRAINT [PK_PromoTotalFaktur] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QCBahanBaku]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QCBahanBaku](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TanggalMasuk] [datetime] NOT NULL,
	[idProduct] [int] NULL,
	[BatchNo] [varchar](500) NULL,
	[idSupplier] [int] NULL,
	[JumlahMasuk] [decimal](18, 4) NOT NULL,
	[UoM] [varchar](50) NULL,
	[Ratio] [decimal](18, 4) NOT NULL,
	[WaktuTerimaQC] [datetime] NULL,
	[WaktuSelesaiQC] [datetime] NULL,
	[LamaQC] [varchar](50) NULL,
	[DOCISO] [varchar](500) NULL,
	[PenampilanStandard] [varchar](500) NULL,
	[PenampilanAktual] [varchar](500) NULL,
	[BauStandard] [varchar](500) NULL,
	[BauAktual] [varchar](500) NULL,
	[BeratJenisStandard] [varchar](50) NULL,
	[BeratJenisAktual] [varchar](50) NULL,
	[pHStandard] [varchar](50) NULL,
	[pHAktual] [varchar](50) NULL,
	[KelarutanStandard] [varchar](50) NULL,
	[KelarutanAktual] [varchar](50) NULL,
	[SolidContentStandard] [varchar](50) NULL,
	[SolidContent] [varchar](50) NULL,
	[Kesimpulan] [varchar](500) NULL,
	[Expired] [datetime] NULL,
	[COA] [varchar](500) NULL,
	[Pemusnahan] [varchar](500) NULL,
	[Status] [varchar](500) NULL,
 CONSTRAINT [PK_QCBahanBaku] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QCBarangJadi]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QCBarangJadi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TanggalMasuk] [datetime] NOT NULL,
	[idProduct] [int] NULL,
	[BatchNo] [varchar](50) NULL,
	[JumlahMasuk] [decimal](18, 4) NOT NULL,
	[UoM] [varchar](50) NULL,
	[Ratio] [decimal](18, 4) NOT NULL,
	[WaktuTerimaQC] [datetime] NULL,
	[WaktuSelesaiQC] [datetime] NULL,
	[LamaQC] [varchar](50) NULL,
	[DOCISO] [varchar](500) NULL,
	[PenampilanStandard] [varchar](500) NULL,
	[PenampilanAktual] [varchar](500) NULL,
	[BauStandard] [varchar](500) NULL,
	[BauAktual] [varchar](500) NULL,
	[BeratJenisStandard] [varchar](50) NULL,
	[BeratJenisAktual] [varchar](50) NULL,
	[pHStandard] [varchar](50) NULL,
	[phAktual] [varchar](50) NULL,
	[KelarutanStandard] [varchar](50) NULL,
	[KelarutanAktual] [varchar](50) NULL,
	[SolidContentStandard] [varchar](50) NULL,
	[SolidContent] [varchar](50) NULL,
	[Kesimpulan] [varchar](500) NULL,
	[Expired] [datetime] NULL,
	[COA] [varchar](500) NULL,
	[Pemusnahan] [varchar](500) NULL,
	[Status] [varchar](500) NULL,
	[Remarks] [varchar](max) NULL,
 CONSTRAINT [PK_QCBarangJadi] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QCKemasan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QCKemasan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TanggalTerima] [datetime] NOT NULL,
	[idProduct] [int] NULL,
	[Quantity] [decimal](18, 4) NOT NULL,
	[UoM] [varchar](50) NULL,
	[Ratio] [decimal](18, 4) NOT NULL,
	[WaktuMulaiQC] [datetime] NULL,
	[WaktuSelesaiQC] [datetime] NULL,
	[LamaQC] [varchar](50) NULL,
	[DOCISO] [varchar](500) NULL,
	[PanjangStandard] [varchar](500) NULL,
	[PanjangAktual] [varchar](500) NULL,
	[LebarStandard] [varchar](500) NULL,
	[LebarAktual] [varchar](500) NULL,
	[TinggiStandard] [varchar](500) NULL,
	[TinggiAktual] [varchar](500) NULL,
	[DiameterStandard] [varchar](500) NULL,
	[DiameterAktual] [varchar](500) NULL,
	[FisikStandard] [varchar](500) NULL,
	[FisikAktual] [varchar](500) NULL,
	[FisikDalamStandard] [varchar](500) NULL,
	[FisikDalamAktual] [varchar](500) NULL,
	[FisikTutupStandard] [varchar](500) NULL,
	[FisikTutupAktual] [varchar](500) NULL,
	[DiamterTutupStandard] [varchar](500) NULL,
	[DiameterTutupAktual] [varchar](500) NULL,
	[WarnaTutupStandard] [varchar](500) NULL,
	[WarnaTutupAktual] [varchar](500) NULL,
	[WarnaKemasanStandard] [varchar](500) NULL,
	[WarnaKemasanAktual] [varchar](500) NULL,
	[KranStandard] [varchar](500) NULL,
	[KranAktual] [varchar](500) NULL,
	[WarnaProduk] [varchar](500) NULL,
	[Kesimpulan] [varchar](500) NULL,
	[Keterangan] [varchar](500) NULL,
 CONSTRAINT [PK_QCKemasan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QCPacking]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QCPacking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TanggalPelaksanaan] [datetime] NOT NULL,
	[MulaiPacking] [datetime] NULL,
	[SelesaiPacking] [datetime] NULL,
	[LamaPacking] [varchar](50) NULL,
	[idProduct] [int] NULL,
	[idCustomer] [int] NULL,
	[Jumlah] [decimal](18, 4) NOT NULL,
	[UoM] [varchar](50) NULL,
	[Ratio] [decimal](18, 4) NOT NULL,
	[BatchNo] [varchar](500) NULL,
	[TanggalPengiriman] [datetime] NULL,
	[PanjangStandard] [varchar](50) NULL,
	[PanjangAktual] [varchar](50) NULL,
	[LebarStandard] [varchar](50) NULL,
	[LebarAktual] [varchar](50) NULL,
	[TinggiStandard] [varchar](50) NULL,
	[TinggiAktual] [varchar](50) NULL,
	[KemasanStandard] [varchar](500) NULL,
	[KemasanAktual] [varchar](500) NULL,
	[LabelingMSDSStandard] [varchar](500) NULL,
	[LabelingMSDSAktual] [varchar](500) NULL,
	[AlatPendukungStandard] [varchar](500) NULL,
	[AlatPendukungAktual] [varchar](500) NULL,
	[PaletStandard] [varchar](500) NULL,
	[PaletAktual] [varchar](500) NULL,
	[SyaratKhususStandard] [varchar](500) NULL,
	[SyaratKhususAktual] [varchar](500) NULL,
	[Kesimpulan] [varchar](500) NULL,
	[Keterangan] [varchar](500) NULL,
 CONSTRAINT [PK_QCPacking] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuotationIn]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuotationIn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idUserAccount] [int] NULL,
	[idSupplier] [int] NULL,
	[idInquiryOut] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](1000) NULL,
	[SupplierName] [varchar](100) NULL,
 CONSTRAINT [PK_QuotationIn] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuotationOut]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuotationOut](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idUserAccount] [int] NULL,
	[idCustomer] [int] NULL,
	[idInquiryIn] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](1000) NULL,
	[CustomerText] [varchar](500) NULL,
 CONSTRAINT [PK_QuotationOut] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SaldoCOA]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaldoCOA](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCOA] [int] NULL,
	[Tanggal] [datetime] NOT NULL,
	[Debit] [money] NOT NULL,
	[Kredit] [money] NOT NULL,
 CONSTRAINT [PK_SaldoCOA] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Salesman]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Salesman](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Salesman] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesOrder]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesOrder](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[idCustomer] [int] NULL,
	[idCourier] [int] NULL,
	[idLocation] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[DateOrder] [datetime] NOT NULL,
	[DateDelivered] [datetime] NULL,
	[DateInvoice] [datetime] NULL,
	[DateReturn] [datetime] NULL,
	[Status] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[Remarks2] [varchar](500) NULL,
	[SubtotalOrder] [money] NOT NULL,
	[SubtotalDelivered] [money] NOT NULL,
	[SubtotalReturn] [money] NOT NULL,
	[DiscountOrder] [decimal](18, 4) NOT NULL,
	[DiscountDelivered] [decimal](18, 4) NOT NULL,
	[DiscountReturn] [decimal](18, 4) NOT NULL,
	[CostOrder] [money] NOT NULL,
	[CostDelivered] [money] NOT NULL,
	[CostReturn] [money] NOT NULL,
	[TaxOrder] [money] NOT NULL,
	[TaxDelivered] [money] NOT NULL,
	[TaxReturn] [money] NOT NULL,
	[TotalOrder] [money] NOT NULL,
	[TotalDelivered] [money] NOT NULL,
	[TotalReturn] [money] NOT NULL,
	[DateModified] [datetime] NULL,
	[LastEditedBy] [int] NULL,
	[DueDate] [datetime] NULL,
	[DatePaid] [datetime] NULL,
	[idCurrency] [int] NULL,
	[Rate] [decimal](18, 4) NULL,
	[Approved] [bit] NULL,
	[DateApproved] [datetime] NULL,
	[ApprovedBy] [int] NULL,
	[ChangeDODate] [datetime] NULL,
	[Stock] [varchar](50) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[SalesOrder] ADD [FOB] [varchar](500) NULL
ALTER TABLE [dbo].[SalesOrder] ADD [ShipVia] [varchar](500) NULL
ALTER TABLE [dbo].[SalesOrder] ADD [Terms] [varchar](100) NULL
ALTER TABLE [dbo].[SalesOrder] ADD [Approved2] [bit] NULL
ALTER TABLE [dbo].[SalesOrder] ADD [DateApproved2] [datetime] NULL
ALTER TABLE [dbo].[SalesOrder] ADD [ApprovedBy2] [int] NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[SalesOrder] ADD [InvoiceNumber] [varchar](50) NULL
ALTER TABLE [dbo].[SalesOrder] ADD [DONumber] [varchar](50) NULL
ALTER TABLE [dbo].[SalesOrder] ADD [idSalesman] [int] NULL
 CONSTRAINT [PK_SalesOrder] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SampleCode]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SampleCode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nomor] [varchar](50) NULL,
	[Nama] [varchar](100) NULL,
	[Kode] [varchar](50) NULL,
	[idFisik] [int] NULL,
	[idTipe] [int] NULL,
	[Fungsi] [varchar](100) NULL,
	[idSupplierSC] [int] NULL,
	[Web] [varchar](100) NULL,
	[Telepon] [varchar](100) NULL,
	[Person] [varchar](100) NULL,
	[Active] [bit] NOT NULL,
	[KodeMSDS] [varchar](100) NULL,
	[AttachmentMSDS] [varchar](500) NULL,
	[FisikText] [varchar](500) NULL,
	[TipeText] [varchar](500) NULL,
	[Quantity] [decimal](18, 4) NULL,
	[TanggalPenerimaan] [datetime] NULL,
 CONSTRAINT [PK_SampleCode] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Satuan]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Satuan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [varchar](50) NULL,
	[Urut] [int] NOT NULL,
 CONSTRAINT [PK_Satuan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StandardDeliveryTime]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StandardDeliveryTime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DateModified] [datetime] NOT NULL,
	[idUserAccount] [int] NULL,
	[idProduct] [int] NULL,
	[Status] [varchar](50) NULL,
	[Packaging] [varchar](500) NULL,
	[Column1] [varchar](50) NULL,
	[Column2] [varchar](50) NULL,
	[Column3] [varchar](50) NULL,
	[Column4] [varchar](50) NULL,
	[Column5] [varchar](50) NULL,
	[Column6] [varchar](50) NULL,
	[Column7] [varchar](50) NULL,
	[Column8] [varchar](50) NULL,
	[Column9] [varchar](50) NULL,
	[Column10] [varchar](50) NULL,
 CONSTRAINT [PK_StandardDeliveryTime] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StatusBahanPenolong]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatusBahanPenolong](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_StatusBahanPenolong] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StatusQC]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[StatusQC](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_StatusQC] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubDepartment]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubDepartment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_SubDepartment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Supplier](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](100) NULL,
	[Address] [varchar](500) NULL,
	[Phone] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[Active] [bit] NOT NULL,
	[Website] [varchar](100) NULL,
	[Fax] [varchar](100) NULL,
	[Person] [varchar](100) NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierSC]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SupplierSC](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](100) NULL,
	[Address] [varchar](500) NULL,
	[Phone] [varchar](50) NULL,
	[Remarks] [varchar](500) NULL,
	[Active] [bit] NOT NULL,
	[Website] [varchar](100) NULL,
	[Fax] [varchar](100) NULL,
	[Person] [varchar](100) NULL,
 CONSTRAINT [PK_SupplierSC] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Terms]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Terms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
 CONSTRAINT [PK_Terms] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tipe]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipe](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Tipe] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransferStock]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransferStock](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[TransactionNumber] [varchar](50) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[idProduct] [int] NULL,
	[Stock] [decimal](18, 4) NOT NULL,
	[Stock2] [decimal](18, 4) NOT NULL,
	[Cost] [money] NOT NULL,
	[Remarks] [varchar](max) NULL,
	[Approved] [bit] NULL,
	[DateApproved] [datetime] NULL,
	[ApprovedBy] [int] NULL,
	[Approved2] [bit] NULL,
	[DateApproved2] [datetime] NULL,
	[ApprovedBy2] [int] NULL,
	[Status] [varchar](50) NULL,
	[Stock3] [decimal](18, 4) NULL,
	[idCourier] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[TransferStock] ADD [FromLocation] [varchar](50) NULL
ALTER TABLE [dbo].[TransferStock] ADD [ToLocation] [varchar](50) NULL
ALTER TABLE [dbo].[TransferStock] ADD [Quantity] [decimal](18, 4) NULL
ALTER TABLE [dbo].[TransferStock] ADD [ShipVia] [varchar](500) NULL
ALTER TABLE [dbo].[TransferStock] ADD [FOB] [varchar](500) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[TransferStock] ADD [NoBatch] [varchar](50) NULL
ALTER TABLE [dbo].[TransferStock] ADD [Packaging] [varchar](50) NULL
ALTER TABLE [dbo].[TransferStock] ADD [IsDelivered] [bit] NULL
ALTER TABLE [dbo].[TransferStock] ADD [idLocation] [int] NULL
ALTER TABLE [dbo].[TransferStock] ADD [DateDelivered] [datetime] NULL
ALTER TABLE [dbo].[TransferStock] ADD [ShipAddress] [varchar](max) NULL
ALTER TABLE [dbo].[TransferStock] ADD [NoPKB] [varchar](50) NULL
ALTER TABLE [dbo].[TransferStock] ADD [SiteAddress] [varchar](500) NULL
ALTER TABLE [dbo].[TransferStock] ADD [ExpeditionPhone] [varchar](50) NULL
ALTER TABLE [dbo].[TransferStock] ADD [DONumber] [varchar](100) NULL
 CONSTRAINT [PK_TransferStock] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TukarFaktur]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TukarFaktur](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPengguna] [int] NULL,
	[idPelanggan] [int] NULL,
	[Nomor] [varchar](50) NULL,
	[Tanggal] [datetime] NOT NULL,
	[Total] [money] NOT NULL,
	[Keterangan] [varchar](500) NULL,
 CONSTRAINT [PK_TukarFaktur] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UpdateHargaByTanggal]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UpdateHargaByTanggal](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idBarang] [int] NULL,
	[Tanggal] [datetime] NOT NULL,
	[HargaJual1] [money] NOT NULL,
	[HargaJual2] [money] NOT NULL,
	[HargaJual3] [money] NOT NULL,
	[HargaJual4] [money] NOT NULL,
	[HargaJual5] [money] NOT NULL,
	[MinQtyKecil1] [decimal](18, 4) NOT NULL,
	[MinQtyKecil2] [decimal](18, 4) NOT NULL,
	[MinQtyKecil3] [decimal](18, 4) NOT NULL,
	[MinQtyKecil4] [decimal](18, 4) NOT NULL,
	[MinQtySedang1] [decimal](18, 4) NOT NULL,
	[MinQtySedang2] [decimal](18, 4) NOT NULL,
	[MinQtySedang3] [decimal](18, 4) NOT NULL,
	[MinQtySedang4] [decimal](18, 4) NOT NULL,
	[MinQtyBesar1] [decimal](18, 4) NOT NULL,
	[MinQtyBesar2] [decimal](18, 4) NOT NULL,
	[MinQtyBesar3] [decimal](18, 4) NOT NULL,
	[MinQtyBesar4] [decimal](18, 4) NOT NULL,
	[HargaKecil1] [money] NOT NULL,
	[HargaKecil2] [money] NOT NULL,
	[HargaKecil3] [money] NOT NULL,
	[HargaKecil4] [money] NOT NULL,
	[HargaSedang1] [money] NOT NULL,
	[HargaSedang2] [money] NOT NULL,
	[HargaSedang3] [money] NOT NULL,
	[HargaSedang4] [money] NOT NULL,
	[HargaBesar1] [money] NOT NULL,
	[HargaBesar2] [money] NOT NULL,
	[HargaBesar3] [money] NOT NULL,
	[HargaBesar4] [money] NOT NULL,
 CONSTRAINT [PK_UpdateHargaByTanggal] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserAccount]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserAccount](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idDepartment] [int] NULL,
	[idSubDepartment] [int] NULL,
	[idGroup] [int] NULL,
	[Name] [varchar](50) NULL,
	[UserName] [varchar](500) NULL,
	[Password] [varchar](50) NULL,
	[AccessRole] [varchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_UserAccount] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idUserAccount] [int] NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Remarks] [varchar](1000) NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VariableBahanBaku]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VariableBahanBaku](
	[idBahanBaku] [int] NOT NULL,
	[Variable1] [decimal](18, 4) NULL,
	[Variable2] [decimal](18, 4) NULL,
	[Variable3] [decimal](18, 4) NULL,
	[Variable4] [decimal](18, 4) NULL,
	[Variable5] [decimal](18, 4) NULL,
	[TotalQuantity] [decimal](18, 4) NULL,
	[HargaPokok] [money] NULL,
 CONSTRAINT [PK_PersenVariable] PRIMARY KEY CLUSTERED 
(
	[idBahanBaku] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Voucher]    Script Date: 03/11/2016 08.43.12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Voucher](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPelanggan] [int] NULL,
	[idPengguna] [int] NULL,
	[Tipe] [varchar](50) NULL,
	[NomorRetur] [varchar](50) NULL,
	[Nomor] [varchar](50) NULL,
	[Tanggal] [datetime] NOT NULL,
	[Keterangan] [varchar](500) NULL,
	[Jumlah] [money] NOT NULL,
	[Pakai] [money] NOT NULL,
	[Sisa] [money] NOT NULL,
 CONSTRAINT [PK_Voucher] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AddEntitlement]  WITH NOCHECK ADD  CONSTRAINT [FK_AddEntitlement_Employee] FOREIGN KEY([idEmployee])
REFERENCES [dbo].[Employee] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AddEntitlement] NOCHECK CONSTRAINT [FK_AddEntitlement_Employee]
GO
ALTER TABLE [dbo].[AddEntitlement]  WITH NOCHECK ADD  CONSTRAINT [FK_AddEntitlement_LeaveType] FOREIGN KEY([idLeaveType])
REFERENCES [dbo].[LeaveType] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AddEntitlement] NOCHECK CONSTRAINT [FK_AddEntitlement_LeaveType]
GO
ALTER TABLE [dbo].[AdjustStock]  WITH NOCHECK ADD  CONSTRAINT [FK_AdjustStock_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AdjustStock] NOCHECK CONSTRAINT [FK_AdjustStock_Product]
GO
ALTER TABLE [dbo].[AdjustStock]  WITH NOCHECK ADD  CONSTRAINT [FK_AdjustStock_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AdjustStock] NOCHECK CONSTRAINT [FK_AdjustStock_UserAccount]
GO
ALTER TABLE [dbo].[Approval]  WITH NOCHECK ADD  CONSTRAINT [FK_Approval_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Approval] NOCHECK CONSTRAINT [FK_Approval_UserAccount]
GO
ALTER TABLE [dbo].[AutoNumber]  WITH NOCHECK ADD  CONSTRAINT [FK_AutoNumber_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AutoNumber] NOCHECK CONSTRAINT [FK_AutoNumber_Department]
GO
ALTER TABLE [dbo].[AutoNumber]  WITH NOCHECK ADD  CONSTRAINT [FK_AutoNumber_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AutoNumber] NOCHECK CONSTRAINT [FK_AutoNumber_SubDepartment]
GO
ALTER TABLE [dbo].[AutoNumber]  WITH NOCHECK ADD  CONSTRAINT [FK_AutoNumber_Supplier] FOREIGN KEY([idSupplier])
REFERENCES [dbo].[Supplier] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AutoNumber] NOCHECK CONSTRAINT [FK_AutoNumber_Supplier]
GO
ALTER TABLE [dbo].[AutoNumber]  WITH NOCHECK ADD  CONSTRAINT [FK_AutoNumber_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[AutoNumber] NOCHECK CONSTRAINT [FK_AutoNumber_UserAccount]
GO
ALTER TABLE [dbo].[BahanBaku]  WITH NOCHECK ADD  CONSTRAINT [FK_BahanBaku_Fisik] FOREIGN KEY([idFisik])
REFERENCES [dbo].[Fisik] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[BahanBaku] NOCHECK CONSTRAINT [FK_BahanBaku_Fisik]
GO
ALTER TABLE [dbo].[BahanBaku]  WITH NOCHECK ADD  CONSTRAINT [FK_BahanBaku_Supplier] FOREIGN KEY([idSupplier])
REFERENCES [dbo].[Supplier] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[BahanBaku] NOCHECK CONSTRAINT [FK_BahanBaku_Supplier]
GO
ALTER TABLE [dbo].[BahanBaku]  WITH NOCHECK ADD  CONSTRAINT [FK_BahanBaku_Tipe] FOREIGN KEY([idTipe])
REFERENCES [dbo].[Tipe] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[BahanBaku] NOCHECK CONSTRAINT [FK_BahanBaku_Tipe]
GO
ALTER TABLE [dbo].[BahanPenolong]  WITH NOCHECK ADD  CONSTRAINT [FK_BahanPenolong_KategoriBahanPenolong] FOREIGN KEY([idKategoriBahanPenolong])
REFERENCES [dbo].[KategoriBahanPenolong] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[BahanPenolong] NOCHECK CONSTRAINT [FK_BahanPenolong_KategoriBahanPenolong]
GO
ALTER TABLE [dbo].[BahanPenolong]  WITH NOCHECK ADD  CONSTRAINT [FK_BahanPenolong_StatusBahanPenolong] FOREIGN KEY([idStatusBahanPenolong])
REFERENCES [dbo].[StatusBahanPenolong] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[BahanPenolong] NOCHECK CONSTRAINT [FK_BahanPenolong_StatusBahanPenolong]
GO
ALTER TABLE [dbo].[Contact]  WITH NOCHECK ADD  CONSTRAINT [FK_Contact_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Contact] NOCHECK CONSTRAINT [FK_Contact_Department]
GO
ALTER TABLE [dbo].[Contact]  WITH NOCHECK ADD  CONSTRAINT [FK_Contact_Group] FOREIGN KEY([idGroup])
REFERENCES [dbo].[Group] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Contact] NOCHECK CONSTRAINT [FK_Contact_Group]
GO
ALTER TABLE [dbo].[Contact]  WITH NOCHECK ADD  CONSTRAINT [FK_Contact_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Contact] NOCHECK CONSTRAINT [FK_Contact_SubDepartment]
GO
ALTER TABLE [dbo].[Customer]  WITH NOCHECK ADD  CONSTRAINT [FK_Customer_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Customer] NOCHECK CONSTRAINT [FK_Customer_Department]
GO
ALTER TABLE [dbo].[CustomerComplaint]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerComplaint_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[CustomerComplaint] NOCHECK CONSTRAINT [FK_CustomerComplaint_Customer]
GO
ALTER TABLE [dbo].[CustomerComplaint]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerComplaint_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[CustomerComplaint] NOCHECK CONSTRAINT [FK_CustomerComplaint_UserAccount]
GO
ALTER TABLE [dbo].[CustomerSatisfaction]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerSatisfaction_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[CustomerSatisfaction] NOCHECK CONSTRAINT [FK_CustomerSatisfaction_Customer]
GO
ALTER TABLE [dbo].[CustomerVoice]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerVoice_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[CustomerVoice] NOCHECK CONSTRAINT [FK_CustomerVoice_Customer]
GO
ALTER TABLE [dbo].[CustomerVoice]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerVoice_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[CustomerVoice] NOCHECK CONSTRAINT [FK_CustomerVoice_UserAccount]
GO
ALTER TABLE [dbo].[DetailPacking]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPacking_Packing] FOREIGN KEY([idPacking])
REFERENCES [dbo].[Packing] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPacking] NOCHECK CONSTRAINT [FK_DetailPacking_Packing]
GO
ALTER TABLE [dbo].[DetailPemakaianVoucher]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPemakaianVoucher_PemakaianVoucher] FOREIGN KEY([idPemakaianVoucher])
REFERENCES [dbo].[PemakaianVoucher] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPemakaianVoucher] NOCHECK CONSTRAINT [FK_DetailPemakaianVoucher_PemakaianVoucher]
GO
ALTER TABLE [dbo].[DetailPemakaianVoucherPenjualan]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPemakaianVoucherPenjualan_PemakaianVoucher] FOREIGN KEY([idPemakaianVoucher])
REFERENCES [dbo].[PemakaianVoucher] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPemakaianVoucherPenjualan] NOCHECK CONSTRAINT [FK_DetailPemakaianVoucherPenjualan_PemakaianVoucher]
GO
ALTER TABLE [dbo].[DetailPenyerahanProduksi]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPenyerahanProduksi_PenyerahanProduksi] FOREIGN KEY([idPenyerahanProduksi])
REFERENCES [dbo].[PenyerahanProduksi] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPenyerahanProduksi] NOCHECK CONSTRAINT [FK_DetailPenyerahanProduksi_PenyerahanProduksi]
GO
ALTER TABLE [dbo].[DetailPenyerahanProduksi]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPenyerahanProduksi_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPenyerahanProduksi] NOCHECK CONSTRAINT [FK_DetailPenyerahanProduksi_Product]
GO
ALTER TABLE [dbo].[DetailPermintaanProduksi]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPermintaanProduksi_PermintaanProduksi] FOREIGN KEY([idPermintaanProduksi])
REFERENCES [dbo].[PermintaanProduksi] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPermintaanProduksi] NOCHECK CONSTRAINT [FK_DetailPermintaanProduksi_PermintaanProduksi]
GO
ALTER TABLE [dbo].[DetailPermintaanProduksi]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPermintaanProduksi_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPermintaanProduksi] NOCHECK CONSTRAINT [FK_DetailPermintaanProduksi_Product]
GO
ALTER TABLE [dbo].[DetailPOtoSupplierExpedisi]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPOtoSupplierExpedisi_POtoSupplierExpedisi] FOREIGN KEY([idPOtoSupplierExpedisi])
REFERENCES [dbo].[POtoSupplierExpedisi] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPOtoSupplierExpedisi] NOCHECK CONSTRAINT [FK_DetailPOtoSupplierExpedisi_POtoSupplierExpedisi]
GO
ALTER TABLE [dbo].[DetailPOtoSupplierRegular]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPOtoSupplierRegular_BahanBaku] FOREIGN KEY([idBahanBaku])
REFERENCES [dbo].[BahanBaku] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPOtoSupplierRegular] NOCHECK CONSTRAINT [FK_DetailPOtoSupplierRegular_BahanBaku]
GO
ALTER TABLE [dbo].[DetailPOtoSupplierRegular]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPOtoSupplierRegular_POtoSupplierRegular] FOREIGN KEY([idPOtoSupplierRegular])
REFERENCES [dbo].[POtoSupplierRegular] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPOtoSupplierRegular] NOCHECK CONSTRAINT [FK_DetailPOtoSupplierRegular_POtoSupplierRegular]
GO
ALTER TABLE [dbo].[DetailPPB]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPPB_PPB] FOREIGN KEY([idPPB])
REFERENCES [dbo].[PPB] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPPB] NOCHECK CONSTRAINT [FK_DetailPPB_PPB]
GO
ALTER TABLE [dbo].[DetailPPB]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPPB_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPPB] NOCHECK CONSTRAINT [FK_DetailPPB_Product]
GO
ALTER TABLE [dbo].[DetailPPS]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPPS_PPS] FOREIGN KEY([idPPS])
REFERENCES [dbo].[PPS] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPPS] NOCHECK CONSTRAINT [FK_DetailPPS_PPS]
GO
ALTER TABLE [dbo].[DetailPPS]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailPPS_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailPPS] NOCHECK CONSTRAINT [FK_DetailPPS_Product]
GO
ALTER TABLE [dbo].[DetailSalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailSalesOrder_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailSalesOrder] NOCHECK CONSTRAINT [FK_DetailSalesOrder_Product]
GO
ALTER TABLE [dbo].[DetailSalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailSalesOrder_SalesOrder] FOREIGN KEY([idSalesOrder])
REFERENCES [dbo].[SalesOrder] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailSalesOrder] NOCHECK CONSTRAINT [FK_DetailSalesOrder_SalesOrder]
GO
ALTER TABLE [dbo].[DetailTukarFaktur]  WITH NOCHECK ADD  CONSTRAINT [FK_DetailTukarFaktur_TukarFaktur] FOREIGN KEY([idTukarFaktur])
REFERENCES [dbo].[TukarFaktur] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[DetailTukarFaktur] NOCHECK CONSTRAINT [FK_DetailTukarFaktur_TukarFaktur]
GO
ALTER TABLE [dbo].[Employee]  WITH NOCHECK ADD  CONSTRAINT [FK_Employee_PayGrade] FOREIGN KEY([idPayGrade])
REFERENCES [dbo].[PayGrade] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Employee] NOCHECK CONSTRAINT [FK_Employee_PayGrade]
GO
ALTER TABLE [dbo].[EmployeeEducation]  WITH NOCHECK ADD  CONSTRAINT [FK_EmployeeEducation_Employee] FOREIGN KEY([idEmployee])
REFERENCES [dbo].[Employee] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[EmployeeEducation] NOCHECK CONSTRAINT [FK_EmployeeEducation_Employee]
GO
ALTER TABLE [dbo].[EmployeeEmergencyContact]  WITH NOCHECK ADD  CONSTRAINT [FK_EmployeeEmergencyContact_Employee] FOREIGN KEY([idEmployee])
REFERENCES [dbo].[Employee] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[EmployeeEmergencyContact] NOCHECK CONSTRAINT [FK_EmployeeEmergencyContact_Employee]
GO
ALTER TABLE [dbo].[EmployeeSkill]  WITH NOCHECK ADD  CONSTRAINT [FK_EmployeeSkill_Employee] FOREIGN KEY([idEmployee])
REFERENCES [dbo].[Employee] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[EmployeeSkill] NOCHECK CONSTRAINT [FK_EmployeeSkill_Employee]
GO
ALTER TABLE [dbo].[EmployeeWorkExperience]  WITH NOCHECK ADD  CONSTRAINT [FK_EmployeeWorkExperience_Employee] FOREIGN KEY([idEmployee])
REFERENCES [dbo].[Employee] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[EmployeeWorkExperience] NOCHECK CONSTRAINT [FK_EmployeeWorkExperience_Employee]
GO
ALTER TABLE [dbo].[Expense]  WITH NOCHECK ADD  CONSTRAINT [FK_Expense_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Expense] NOCHECK CONSTRAINT [FK_Expense_Department]
GO
ALTER TABLE [dbo].[Expense]  WITH NOCHECK ADD  CONSTRAINT [FK_Expense_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Expense] NOCHECK CONSTRAINT [FK_Expense_SubDepartment]
GO
ALTER TABLE [dbo].[Expense]  WITH NOCHECK ADD  CONSTRAINT [FK_Expense_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Expense] NOCHECK CONSTRAINT [FK_Expense_UserAccount]
GO
ALTER TABLE [dbo].[GajiKaryawan]  WITH NOCHECK ADD  CONSTRAINT [FK_GajiKaryawan_Karyawan] FOREIGN KEY([idKaryawan])
REFERENCES [dbo].[Karyawan] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[GajiKaryawan] NOCHECK CONSTRAINT [FK_GajiKaryawan_Karyawan]
GO
ALTER TABLE [dbo].[GeneralLetter]  WITH NOCHECK ADD  CONSTRAINT [FK_GeneralLetter_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[GeneralLetter] NOCHECK CONSTRAINT [FK_GeneralLetter_Department]
GO
ALTER TABLE [dbo].[GeneralLetter]  WITH NOCHECK ADD  CONSTRAINT [FK_GeneralLetter_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[GeneralLetter] NOCHECK CONSTRAINT [FK_GeneralLetter_SubDepartment]
GO
ALTER TABLE [dbo].[GeneralLetter]  WITH NOCHECK ADD  CONSTRAINT [FK_GeneralLetter_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[GeneralLetter] NOCHECK CONSTRAINT [FK_GeneralLetter_UserAccount]
GO
ALTER TABLE [dbo].[InquiryIn]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryIn_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryIn] NOCHECK CONSTRAINT [FK_InquiryIn_Customer]
GO
ALTER TABLE [dbo].[InquiryIn]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryIn_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryIn] NOCHECK CONSTRAINT [FK_InquiryIn_Department]
GO
ALTER TABLE [dbo].[InquiryIn]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryIn_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryIn] NOCHECK CONSTRAINT [FK_InquiryIn_SubDepartment]
GO
ALTER TABLE [dbo].[InquiryIn]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryIn_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryIn] NOCHECK CONSTRAINT [FK_InquiryIn_UserAccount]
GO
ALTER TABLE [dbo].[InquiryOut]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryOut_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryOut] NOCHECK CONSTRAINT [FK_InquiryOut_Department]
GO
ALTER TABLE [dbo].[InquiryOut]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryOut_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryOut] NOCHECK CONSTRAINT [FK_InquiryOut_SubDepartment]
GO
ALTER TABLE [dbo].[InquiryOut]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryOut_Supplier] FOREIGN KEY([idSupplier])
REFERENCES [dbo].[Supplier] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryOut] NOCHECK CONSTRAINT [FK_InquiryOut_Supplier]
GO
ALTER TABLE [dbo].[InquiryOut]  WITH NOCHECK ADD  CONSTRAINT [FK_InquiryOut_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InquiryOut] NOCHECK CONSTRAINT [FK_InquiryOut_UserAccount]
GO
ALTER TABLE [dbo].[InternalLetter]  WITH NOCHECK ADD  CONSTRAINT [FK_InternalLetter_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InternalLetter] NOCHECK CONSTRAINT [FK_InternalLetter_Department]
GO
ALTER TABLE [dbo].[InternalLetter]  WITH NOCHECK ADD  CONSTRAINT [FK_InternalLetter_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InternalLetter] NOCHECK CONSTRAINT [FK_InternalLetter_SubDepartment]
GO
ALTER TABLE [dbo].[InternalLetter]  WITH NOCHECK ADD  CONSTRAINT [FK_InternalLetter_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[InternalLetter] NOCHECK CONSTRAINT [FK_InternalLetter_UserAccount]
GO
ALTER TABLE [dbo].[KasbonKaryawan]  WITH NOCHECK ADD  CONSTRAINT [FK_Kasbon_Karyawan] FOREIGN KEY([idKaryawan])
REFERENCES [dbo].[Karyawan] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[KasbonKaryawan] NOCHECK CONSTRAINT [FK_Kasbon_Karyawan]
GO
ALTER TABLE [dbo].[LeaveUsage]  WITH NOCHECK ADD  CONSTRAINT [FK_LeaveUsage_Employee] FOREIGN KEY([idEmployee])
REFERENCES [dbo].[Employee] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[LeaveUsage] NOCHECK CONSTRAINT [FK_LeaveUsage_Employee]
GO
ALTER TABLE [dbo].[LeaveUsage]  WITH NOCHECK ADD  CONSTRAINT [FK_LeaveUsage_LeaveType] FOREIGN KEY([idLeaveType])
REFERENCES [dbo].[LeaveType] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[LeaveUsage] NOCHECK CONSTRAINT [FK_LeaveUsage_LeaveType]
GO
ALTER TABLE [dbo].[PenyerahanProduksi]  WITH NOCHECK ADD  CONSTRAINT [FK_PenyerahanProduksi_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PenyerahanProduksi] NOCHECK CONSTRAINT [FK_PenyerahanProduksi_UserAccount]
GO
ALTER TABLE [dbo].[PinjamanKaryawan]  WITH NOCHECK ADD  CONSTRAINT [FK_PinjamanKaryawan_Karyawan] FOREIGN KEY([idKaryawan])
REFERENCES [dbo].[Karyawan] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PinjamanKaryawan] NOCHECK CONSTRAINT [FK_PinjamanKaryawan_Karyawan]
GO
ALTER TABLE [dbo].[PPB]  WITH NOCHECK ADD  CONSTRAINT [FK_PPB_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPB] NOCHECK CONSTRAINT [FK_PPB_Customer]
GO
ALTER TABLE [dbo].[PPB]  WITH NOCHECK ADD  CONSTRAINT [FK_PPB_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPB] NOCHECK CONSTRAINT [FK_PPB_Department]
GO
ALTER TABLE [dbo].[PPB]  WITH NOCHECK ADD  CONSTRAINT [FK_PPB_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPB] NOCHECK CONSTRAINT [FK_PPB_Product]
GO
ALTER TABLE [dbo].[PPB]  WITH NOCHECK ADD  CONSTRAINT [FK_PPB_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPB] NOCHECK CONSTRAINT [FK_PPB_SubDepartment]
GO
ALTER TABLE [dbo].[PPB]  WITH NOCHECK ADD  CONSTRAINT [FK_PPB_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPB] NOCHECK CONSTRAINT [FK_PPB_UserAccount]
GO
ALTER TABLE [dbo].[PPS]  WITH NOCHECK ADD  CONSTRAINT [FK_PPS_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPS] NOCHECK CONSTRAINT [FK_PPS_Customer]
GO
ALTER TABLE [dbo].[PPS]  WITH NOCHECK ADD  CONSTRAINT [FK_PPS_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPS] NOCHECK CONSTRAINT [FK_PPS_Department]
GO
ALTER TABLE [dbo].[PPS]  WITH NOCHECK ADD  CONSTRAINT [FK_PPS_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPS] NOCHECK CONSTRAINT [FK_PPS_Product]
GO
ALTER TABLE [dbo].[PPS]  WITH NOCHECK ADD  CONSTRAINT [FK_PPS_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPS] NOCHECK CONSTRAINT [FK_PPS_SubDepartment]
GO
ALTER TABLE [dbo].[PPS]  WITH NOCHECK ADD  CONSTRAINT [FK_PPS_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[PPS] NOCHECK CONSTRAINT [FK_PPS_UserAccount]
GO
ALTER TABLE [dbo].[Product]  WITH NOCHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([idCategory])
REFERENCES [dbo].[Category] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [dbo].[Product]  WITH NOCHECK ADD  CONSTRAINT [FK_Product_Supplier] FOREIGN KEY([idSupplier])
REFERENCES [dbo].[Supplier] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Product_Supplier]
GO
ALTER TABLE [dbo].[ProjectList]  WITH NOCHECK ADD  CONSTRAINT [FK_ProjectList_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[ProjectList] NOCHECK CONSTRAINT [FK_ProjectList_Customer]
GO
ALTER TABLE [dbo].[ProjectList]  WITH NOCHECK ADD  CONSTRAINT [FK_ProjectList_ProjectCategory] FOREIGN KEY([idProjectCategory])
REFERENCES [dbo].[ProjectCategory] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[ProjectList] NOCHECK CONSTRAINT [FK_ProjectList_ProjectCategory]
GO
ALTER TABLE [dbo].[ProjectList]  WITH NOCHECK ADD  CONSTRAINT [FK_ProjectList_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[ProjectList] NOCHECK CONSTRAINT [FK_ProjectList_UserAccount]
GO
ALTER TABLE [dbo].[ProjectProgress]  WITH NOCHECK ADD  CONSTRAINT [FK_ProjectProgress_ProjectList] FOREIGN KEY([idProjectList])
REFERENCES [dbo].[ProjectList] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[ProjectProgress] NOCHECK CONSTRAINT [FK_ProjectProgress_ProjectList]
GO
ALTER TABLE [dbo].[ProjectProgress]  WITH NOCHECK ADD  CONSTRAINT [FK_ProjectProgress_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[ProjectProgress] NOCHECK CONSTRAINT [FK_ProjectProgress_UserAccount]
GO
ALTER TABLE [dbo].[ProjectUserAccount]  WITH NOCHECK ADD  CONSTRAINT [FK_ProjectUserAccount_ProjectList] FOREIGN KEY([idProjectList])
REFERENCES [dbo].[ProjectList] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[ProjectUserAccount] NOCHECK CONSTRAINT [FK_ProjectUserAccount_ProjectList]
GO
ALTER TABLE [dbo].[ProjectUserAccount]  WITH NOCHECK ADD  CONSTRAINT [FK_ProjectUserAccount_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[ProjectUserAccount] NOCHECK CONSTRAINT [FK_ProjectUserAccount_UserAccount]
GO
ALTER TABLE [dbo].[QuotationIn]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationIn_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationIn] NOCHECK CONSTRAINT [FK_QuotationIn_Department]
GO
ALTER TABLE [dbo].[QuotationIn]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationIn_InquiryOut] FOREIGN KEY([idInquiryOut])
REFERENCES [dbo].[InquiryOut] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationIn] NOCHECK CONSTRAINT [FK_QuotationIn_InquiryOut]
GO
ALTER TABLE [dbo].[QuotationIn]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationIn_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationIn] NOCHECK CONSTRAINT [FK_QuotationIn_SubDepartment]
GO
ALTER TABLE [dbo].[QuotationIn]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationIn_Supplier] FOREIGN KEY([idSupplier])
REFERENCES [dbo].[Supplier] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationIn] NOCHECK CONSTRAINT [FK_QuotationIn_Supplier]
GO
ALTER TABLE [dbo].[QuotationIn]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationIn_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationIn] NOCHECK CONSTRAINT [FK_QuotationIn_UserAccount]
GO
ALTER TABLE [dbo].[QuotationOut]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationOut_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationOut] NOCHECK CONSTRAINT [FK_QuotationOut_Customer]
GO
ALTER TABLE [dbo].[QuotationOut]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationOut_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationOut] NOCHECK CONSTRAINT [FK_QuotationOut_Department]
GO
ALTER TABLE [dbo].[QuotationOut]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationOut_InquiryIn] FOREIGN KEY([idInquiryIn])
REFERENCES [dbo].[InquiryIn] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationOut] NOCHECK CONSTRAINT [FK_QuotationOut_InquiryIn]
GO
ALTER TABLE [dbo].[QuotationOut]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationOut_SubDepartment] FOREIGN KEY([idSubDepartment])
REFERENCES [dbo].[SubDepartment] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationOut] NOCHECK CONSTRAINT [FK_QuotationOut_SubDepartment]
GO
ALTER TABLE [dbo].[QuotationOut]  WITH NOCHECK ADD  CONSTRAINT [FK_QuotationOut_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[QuotationOut] NOCHECK CONSTRAINT [FK_QuotationOut_UserAccount]
GO
ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_Courier] FOREIGN KEY([idCourier])
REFERENCES [dbo].[Courier] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SalesOrder] NOCHECK CONSTRAINT [FK_SalesOrder_Courier]
GO
ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_Customer] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[Customer] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SalesOrder] NOCHECK CONSTRAINT [FK_SalesOrder_Customer]
GO
ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_Location] FOREIGN KEY([idLocation])
REFERENCES [dbo].[Location] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SalesOrder] NOCHECK CONSTRAINT [FK_SalesOrder_Location]
GO
ALTER TABLE [dbo].[SalesOrder]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrder_SalesOrder] FOREIGN KEY([id])
REFERENCES [dbo].[SalesOrder] ([id])
GO
ALTER TABLE [dbo].[SalesOrder] CHECK CONSTRAINT [FK_SalesOrder_SalesOrder]
GO
ALTER TABLE [dbo].[SalesOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesOrder_UserAccount] FOREIGN KEY([idUserAccount])
REFERENCES [dbo].[UserAccount] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SalesOrder] NOCHECK CONSTRAINT [FK_SalesOrder_UserAccount]
GO
ALTER TABLE [dbo].[SampleCode]  WITH NOCHECK ADD  CONSTRAINT [FK_SampleCode_Fisik] FOREIGN KEY([idFisik])
REFERENCES [dbo].[Fisik] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SampleCode] NOCHECK CONSTRAINT [FK_SampleCode_Fisik]
GO
ALTER TABLE [dbo].[SampleCode]  WITH NOCHECK ADD  CONSTRAINT [FK_SampleCode_SupplierSC] FOREIGN KEY([idSupplierSC])
REFERENCES [dbo].[SupplierSC] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SampleCode] NOCHECK CONSTRAINT [FK_SampleCode_SupplierSC]
GO
ALTER TABLE [dbo].[SampleCode]  WITH NOCHECK ADD  CONSTRAINT [FK_SampleCode_Tipe] FOREIGN KEY([idTipe])
REFERENCES [dbo].[Tipe] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SampleCode] NOCHECK CONSTRAINT [FK_SampleCode_Tipe]
GO
ALTER TABLE [dbo].[StandardDeliveryTime]  WITH NOCHECK ADD  CONSTRAINT [FK_StandardDeliveryTime_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[StandardDeliveryTime] NOCHECK CONSTRAINT [FK_StandardDeliveryTime_Product]
GO
ALTER TABLE [dbo].[SubDepartment]  WITH NOCHECK ADD  CONSTRAINT [FK_SubDepartment_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[SubDepartment] NOCHECK CONSTRAINT [FK_SubDepartment_Department]
GO
ALTER TABLE [dbo].[TransferStock]  WITH NOCHECK ADD  CONSTRAINT [FK_TransferStock_Product] FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[TransferStock] NOCHECK CONSTRAINT [FK_TransferStock_Product]
GO
ALTER TABLE [dbo].[UserAccount]  WITH NOCHECK ADD  CONSTRAINT [FK_UserAccount_Department] FOREIGN KEY([idDepartment])
REFERENCES [dbo].[Department] ([id])
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[UserAccount] NOCHECK CONSTRAINT [FK_UserAccount_Department]
GO
USE [master]
GO
ALTER DATABASE [GreenChem] SET  READ_WRITE 
GO
