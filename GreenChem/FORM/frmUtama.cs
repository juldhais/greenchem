﻿using System;
using System.Linq;
using System.Windows.Forms;
using JulFunctions;
using DevExpress.XtraEditors;

namespace GreenChem
{
    public partial class frmUtama : DevExpress.XtraEditors.XtraForm
    {
        public static int? _iddepartment = null;
        public static int? _idsubdepartment = null;
        public static int? _idgroup = null;
        public static int? _iduseraccount = null;

        public frmUtama()
        {
            InitializeComponent();
        }

        private void UserRole()
        {
            try
            {
                GreenChemDataContext db = new GreenChemDataContext();
                var useraccount = db.UserAccounts.First(x => x.id == _iduseraccount);
                Text = "GREENSYS  4.0 - Last Update : 03-11-2016 | " + useraccount.UserName;

                productToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Product");
                bahanBakuToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Bahan Baku");
                bahanPenolongToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Bahan Penolong");
                sampleCodeToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Sample Code");
                categoryToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Category");
                departmentToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Department");
                supplierToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Supplier");
                supplierSCToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Supplier SC");
                customerToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Customer");
                employeeToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Employee");
                expeditionToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Expedition");
                locationToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Location");
                currencyToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Currency");
                userAccountToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "User Account");
                contactListToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "ContactList");
                termsToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Terms");
                packagingToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Packaging");

                salesOrderToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Sales Order");
                deliveryOrderToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Delivery Order");
                salesInvoiceToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Sales Invoice");
                changeDOLocationToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Change DO Location");
                changeTSLocationToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Change TS Location");
                stockAdjustmentToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Stock Adjustment");
                expenseToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Expense");
                customerSatisfactionToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Customer Voice");
                customerComplaintToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Customer Voice");
                voiceOfCompanyToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Customer Voice");
                internalComplaintToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Internal Complaint");
                newInternalComplaintToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "New Internal Complaint Menu");
                penyerahanProduksiToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Penyerahan Produksi");
                transferStockToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Transfer Stock");
                workInProgressToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Work In Progress");
                qCBarangJadiToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "QC Barang Jadi");
                qCBahanBakuToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "QC Bahan Baku");
                qCPackingToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "QC Packing");
                qCKemasanToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "QC Kemasan");
                standardDeliveryTimeToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Standard Delivery Time");
                labServiceToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Lab Service");

                projectToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Project");
                pIMToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PIM");
                leaveToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Leave");

                permintaanProduksiToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Permintaan Produksi");
                pengembanganProdukToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Pengembangan Produk");

                externalLetterToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "External Letter");
                internalLetterToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Internal Letter");
                pOToSupplierToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PO to Supplier");
                pOToSupplierRegularToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PO to Supplier Regular");
                pOToSupplierExpeditionToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PO to Supplier Expedisi");
                rFQToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Inquiry In");
                inquiryOutToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Inquiry Out");
                quotationInToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Quotation In");
                quotationOutToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Quotation Out");
                pPBToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PPB");
                pPSToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PPS");

                salesOrderReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Sales Order Report");
                autoNumberingReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Auto Number Report");
                userLogReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "User Log Report");
                expenseReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Expense Report");
                customerSatisfactionReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Customer Satisfaction Report");
                customerComplaintReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Customer Complaint Report");
                customerVoiceReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Customer Voice Report");
                pPBReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PPB Report");
                pPSReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PPS Report");
                penyerahanProduksiReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Penyerahan Produksi Report");
                penyerahanProduksiReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Stock Report");
                stockReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Stock Report");
                pIMReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "PIM Report");

                settingGudangToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Configuration");
                configurationToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Configuration");
                designReportToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Design Report");
                settingGudangToolStripMenuItem.Visible = db.CheckUserRole(_iduseraccount.Value, "Setting Gudang");
            }
            catch { }
        }

        private void frmUtama_Load(object sender, EventArgs e)
        {
            frmLogin form = new frmLogin();
            form.ShowDialog();
            if (_iduseraccount == null) Close();
            UserRole();
        }

        private void productToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataProduct form = new MenuData.frmDataProduct();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void categoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataCategory form = new MenuData.frmDataCategory();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void departmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataDepartment form = new MenuData.frmDataDepartment();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void contactListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataContact form = new MenuData.frmDataContact();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void supplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataSupplier form = new MenuData.frmDataSupplier();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void customerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataCustomer form = new MenuData.frmDataCustomer();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void courierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataCourier form = new MenuData.frmDataCourier();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void locationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataLocation form = new MenuData.frmDataLocation();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void currencyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataCurrency form = new MenuData.frmDataCurrency();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void userAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmNewUserAccount form = new MenuData.frmNewUserAccount();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void salesOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmSalesOrder form = new MenuTransaction.frmSalesOrder();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void deliveryOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmDeliveryOrder form = new MenuTransaction.frmDeliveryOrder();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void salesInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmSalesInvoice form = new MenuTransaction.frmSalesInvoice();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void stockAdjustmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmStockAdjustment form = new MenuTransaction.frmStockAdjustment();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void generalLetterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmGeneralLetter form = new MenuTransaction.frmGeneralLetter();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void internalLetterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmInternalLetter form = new MenuTransaction.frmInternalLetter();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void pPBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmPPB form = new MenuTransaction.frmPPB();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void pPSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmPPS form = new MenuTransaction.frmPPS();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuOthers.frmConfiguration form = new MenuOthers.frmConfiguration();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void autoNumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmAutoNumber form = new MenuTransaction.frmAutoNumber();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void inquiryInCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmInquiryIn form = new MenuTransaction.frmInquiryIn();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void inquiryOutSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmInquiryOut form = new MenuTransaction.frmInquiryOut();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void quotationInCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmQuotationIn form = new MenuTransaction.frmQuotationIn();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void quotationOutSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmQuotationOut form = new MenuTransaction.frmQuotationOut();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void changeDOLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmChangeDOLocation form = new MenuTransaction.frmChangeDOLocation();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to log out?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;

                    GreenChemDataContext db = new GreenChemDataContext();
                    UserLog userlog = new UserLog();
                    userlog.idUserAccount = frmUtama._iduseraccount;
                    userlog.TransactionDate = DateTime.Now;
                    userlog.Remarks = "Log Out";
                    db.UserLogs.InsertOnSubmit(userlog);
                    db.SubmitChanges();

                    Application.Restart();
                    
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void salesOrderReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportSalesOrder form = new MenuReport.frmReportSalesOrder();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void userLogReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportUserLog form = new MenuReport.frmReportUserLog();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void autoNumberingReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmAutoNumberingReport form = new MenuReport.frmAutoNumberingReport();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void expenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmExpense form = new MenuTransaction.frmExpense();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void customerSatisfactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmCustomerSatisfaction form = new MenuTransaction.frmCustomerSatisfaction();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void salesmanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmDataSalesman form = new MenuData.frmDataSalesman();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void expenseReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmExpenseReport form = new MenuReport.frmExpenseReport();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void bahanBakuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmBahanBaku form = new MenuData.frmBahanBaku();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void bahanPenolongToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmBahanPenolong form = new MenuData.frmBahanPenolong();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void customerComplaintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmVoiceOfCustomer form = new frmVoiceOfCustomer();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void customerSatisfactionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportCustomerSatisfaction form = new MenuReport.frmReportCustomerSatisfaction();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void customerComplaintReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportCustomerComplaint form = new MenuReport.frmReportCustomerComplaint();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmChangePassword form = new MenuData.frmChangePassword();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void pPBReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportPPB report = new MenuReport.frmReportPPB();
            report.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void pPSReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportPPS report = new MenuReport.frmReportPPS();
            report.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void sampleCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmSampleCode form = new MenuData.frmSampleCode();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void supplierSCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmSupplierSC form = new MenuData.frmSupplierSC();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void approvalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmApproval form = new MenuTransaction.frmApproval();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void customerVoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmCustomerVoice form = new MenuTransaction.frmCustomerVoice();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void penyerahanProduksiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmPenyerahanProduksi form = new MenuTransaction.frmPenyerahanProduksi();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void penyerahanProduksiReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportPenyerahanProduksi form = new MenuReport.frmReportPenyerahanProduksi();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void stockReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmStockReport form = new MenuReport.frmStockReport();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void workInProgressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmWorkInProgress form = new MenuTransaction.frmWorkInProgress();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void qCBarangJadiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmQCBarangJadi form = new MenuTransaction.frmQCBarangJadi();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void qCBahanBakuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmQCBahanBaku form = new MenuTransaction.frmQCBahanBaku();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void qCPackingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmQCPacking form = new MenuTransaction.frmQCPacking();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void transferStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmTransferStock form = new MenuTransaction.frmTransferStock();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void standardDeliveryTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmStandardDeliveryTime form = new MenuTransaction.frmStandardDeliveryTime();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void labServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmLabService form = new MenuTransaction.frmLabService();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void designReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuOthers.frmCustomReport form = new MenuOthers.frmCustomReport();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void customerVoiceReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportCustomerVoice form = new MenuReport.frmReportCustomerVoice();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void termsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmTerms form = new MenuData.frmTerms();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void qCKemasanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmQCKemasan form = new MenuTransaction.frmQCKemasan();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void packagingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuData.frmPackaging form = new MenuData.frmPackaging();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void pOToSupplierRegularToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmPOtoSupplierRegular form = new MenuTransaction.frmPOtoSupplierRegular();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void pOToSuppplierExpedisiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmPOtoSupplierExpedisi form = new MenuTransaction.frmPOtoSupplierExpedisi();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void internalComplaintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmInternalComplaint form = new MenuTransaction.frmInternalComplaint();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void newInternalComplaintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmNewInternalComplaint form = new MenuTransaction.frmNewInternalComplaint();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void projectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction2.frmProject form = new MenuTransaction2.frmProject();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.WaitCursor;
        }

        private void pIMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuHR.frmPIM form = new MenuHR.frmPIM();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void leaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuHR.frmLeave form = new MenuHR.frmLeave();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void permintaanProduksiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction2.frmPermintaanProduksi form = new MenuTransaction2.frmPermintaanProduksi();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void pengembanganProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction2.frmPengembanganProduk form = new MenuTransaction2.frmPengembanganProduk();
            form.MdiParent = this;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void changeTSLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction.frmChangeTSLocation form = new MenuTransaction.frmChangeTSLocation();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void pIMReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmPIMReport form = new MenuReport.frmPIMReport();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void myInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                GreenChemDataContext db = new GreenChemDataContext();
                if (db.Employees.Any(x => x.idUserAccount == _iduseraccount))
                {
                    var employeeid = db.Employees.First(x => x.idUserAccount == _iduseraccount).id;
                    MenuHR.frmNewEditEmployee form = new MenuHR.frmNewEditEmployee(employeeid);
                    form.btnSave.Enabled = false;
                    form.ShowDialog();
                }
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void settingGudangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            GreenChem.MenuData.frmSettingGudang form = new GreenChem.MenuData.frmSettingGudang();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}
