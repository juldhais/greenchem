﻿namespace GreenChem.MenuData
{
    partial class frmNewContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtMidName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtFirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTitle = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbDepartment = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtPosition = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.cmbUserAccount = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPriority = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.cmbPriority = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSubDepartment = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txtEmail2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtEmail1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtMobile2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtMobile1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhone2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhone1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.txtZipCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtCountry = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtState = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtCity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.txtRegisterNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtTaxNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtAccountNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txtCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.picImage = new DevExpress.XtraEditors.PictureEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtRemarks = new DevExpress.XtraEditors.MemoEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtHomeAddress = new DevExpress.XtraEditors.MemoEdit();
            this.cmbUserAccount2 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmbUserAccount3 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMidName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPriority.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSubDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtZipCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemarks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtHomeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtLastName);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtMidName);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtFirstName);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtCode);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.cmbTitle);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(400, 135);
            this.panelControl1.TabIndex = 0;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(83, 102);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLastName.Properties.Appearance.Options.UseFont = true;
            this.txtLastName.Size = new System.Drawing.Size(303, 24);
            this.txtLastName.TabIndex = 13;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(15, 105);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(62, 17);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "Last Name";
            // 
            // txtMidName
            // 
            this.txtMidName.Location = new System.Drawing.Point(83, 72);
            this.txtMidName.Name = "txtMidName";
            this.txtMidName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMidName.Properties.Appearance.Options.UseFont = true;
            this.txtMidName.Size = new System.Drawing.Size(303, 24);
            this.txtMidName.TabIndex = 11;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(15, 75);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(62, 17);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Mid Name";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(83, 42);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFirstName.Properties.Appearance.Options.UseFont = true;
            this.txtFirstName.Size = new System.Drawing.Size(303, 24);
            this.txtFirstName.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(14, 45);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(63, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "First Name";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(256, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(24, 17);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Title";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(83, 12);
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCode.Properties.Appearance.Options.UseFont = true;
            this.txtCode.Properties.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(145, 24);
            this.txtCode.TabIndex = 5;
            this.txtCode.TabStop = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(46, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(31, 17);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Code";
            // 
            // cmbTitle
            // 
            this.cmbTitle.Location = new System.Drawing.Point(286, 12);
            this.cmbTitle.Name = "cmbTitle";
            this.cmbTitle.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTitle.Properties.Appearance.Options.UseFont = true;
            this.cmbTitle.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTitle.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbTitle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTitle.Properties.Items.AddRange(new object[] {
            "Mr",
            "Ms",
            "Mrs"});
            this.cmbTitle.Size = new System.Drawing.Size(100, 24);
            this.cmbTitle.TabIndex = 7;
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.EditValue = "";
            this.cmbDepartment.Location = new System.Drawing.Point(91, 11);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDepartment.Properties.Appearance.Options.UseFont = true;
            this.cmbDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDepartment.Properties.DisplayMember = "Name";
            this.cmbDepartment.Properties.NullText = "";
            this.cmbDepartment.Properties.ReadOnly = true;
            this.cmbDepartment.Properties.ValueMember = "id";
            this.cmbDepartment.Properties.View = this.searchLookUpEdit1View;
            this.cmbDepartment.Size = new System.Drawing.Size(295, 24);
            this.cmbDepartment.TabIndex = 1;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseFont = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.searchLookUpEdit1View.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.Row.Options.UseFont = true;
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.searchLookUpEdit1View.RowHeight = 23;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "id";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Code";
            this.gridColumn5.FieldName = "Code";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 120;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Name";
            this.gridColumn6.FieldName = "Name";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 264;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(16, 14);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(69, 17);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Department";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.cmbUserAccount3);
            this.panelControl2.Controls.Add(this.cmbUserAccount2);
            this.panelControl2.Controls.Add(this.txtPosition);
            this.panelControl2.Controls.Add(this.labelControl27);
            this.panelControl2.Controls.Add(this.cmbUserAccount);
            this.panelControl2.Controls.Add(this.btnPriority);
            this.panelControl2.Controls.Add(this.labelControl25);
            this.panelControl2.Controls.Add(this.cmbPriority);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.cmbSubDepartment);
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Controls.Add(this.cmbDepartment);
            this.panelControl2.Location = new System.Drawing.Point(12, 153);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(400, 208);
            this.panelControl2.TabIndex = 1;
            // 
            // txtPosition
            // 
            this.txtPosition.Location = new System.Drawing.Point(91, 71);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPosition.Properties.Appearance.Options.UseFont = true;
            this.txtPosition.Size = new System.Drawing.Size(295, 24);
            this.txtPosition.TabIndex = 5;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl27.Location = new System.Drawing.Point(8, 134);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(77, 17);
            this.labelControl27.TabIndex = 10;
            this.labelControl27.Text = "User Account";
            // 
            // cmbUserAccount
            // 
            this.cmbUserAccount.EditValue = "";
            this.cmbUserAccount.Location = new System.Drawing.Point(91, 131);
            this.cmbUserAccount.Name = "cmbUserAccount";
            this.cmbUserAccount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbUserAccount.Properties.Appearance.Options.UseFont = true;
            this.cmbUserAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUserAccount.Properties.DisplayMember = "Name";
            this.cmbUserAccount.Properties.NullText = "";
            this.cmbUserAccount.Properties.ReadOnly = true;
            this.cmbUserAccount.Properties.ValueMember = "id";
            this.cmbUserAccount.Properties.View = this.gridView4;
            this.cmbUserAccount.Size = new System.Drawing.Size(295, 24);
            this.cmbUserAccount.TabIndex = 11;
            // 
            // gridView4
            // 
            this.gridView4.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView4.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView4.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView4.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView4.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView4.Appearance.Row.Options.UseFont = true;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.RowHeight = 23;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "id";
            this.gridColumn11.FieldName = "id";
            this.gridColumn11.Name = "gridColumn11";
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Name";
            this.gridColumn12.FieldName = "Name";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            this.gridColumn12.Width = 264;
            // 
            // btnPriority
            // 
            this.btnPriority.Location = new System.Drawing.Point(336, 101);
            this.btnPriority.Name = "btnPriority";
            this.btnPriority.Size = new System.Drawing.Size(50, 24);
            this.btnPriority.TabIndex = 9;
            this.btnPriority.Text = "...";
            this.btnPriority.Click += new System.EventHandler(this.btnPriority_Click);
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl25.Location = new System.Drawing.Point(44, 104);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(41, 17);
            this.labelControl25.TabIndex = 7;
            this.labelControl25.Text = "Priority";
            // 
            // cmbPriority
            // 
            this.cmbPriority.EditValue = "";
            this.cmbPriority.Location = new System.Drawing.Point(91, 101);
            this.cmbPriority.Name = "cmbPriority";
            this.cmbPriority.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbPriority.Properties.Appearance.Options.UseFont = true;
            this.cmbPriority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPriority.Properties.DisplayMember = "Name";
            this.cmbPriority.Properties.NullText = "";
            this.cmbPriority.Properties.ValueMember = "id";
            this.cmbPriority.Properties.View = this.gridView3;
            this.cmbPriority.Size = new System.Drawing.Size(239, 24);
            this.cmbPriority.TabIndex = 8;
            // 
            // gridView3
            // 
            this.gridView3.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView3.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView3.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView3.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView3.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView3.Appearance.Row.Options.UseFont = true;
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn10});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.RowHeight = 23;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "id";
            this.gridColumn8.FieldName = "id";
            this.gridColumn8.Name = "gridColumn8";
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Name";
            this.gridColumn10.FieldName = "Name";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 264;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(39, 73);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(46, 17);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "Position";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(47, 44);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(38, 17);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "Owner";
            // 
            // cmbSubDepartment
            // 
            this.cmbSubDepartment.EditValue = "";
            this.cmbSubDepartment.Location = new System.Drawing.Point(91, 41);
            this.cmbSubDepartment.Name = "cmbSubDepartment";
            this.cmbSubDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSubDepartment.Properties.Appearance.Options.UseFont = true;
            this.cmbSubDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSubDepartment.Properties.DisplayMember = "Name";
            this.cmbSubDepartment.Properties.NullText = "";
            this.cmbSubDepartment.Properties.ValueMember = "id";
            this.cmbSubDepartment.Properties.View = this.gridView1;
            this.cmbSubDepartment.Size = new System.Drawing.Size(295, 24);
            this.cmbSubDepartment.TabIndex = 3;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 264;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txtEmail2);
            this.panelControl3.Controls.Add(this.labelControl16);
            this.panelControl3.Controls.Add(this.txtEmail1);
            this.panelControl3.Controls.Add(this.labelControl15);
            this.panelControl3.Controls.Add(this.txtWebsite);
            this.panelControl3.Controls.Add(this.labelControl14);
            this.panelControl3.Controls.Add(this.txtFax);
            this.panelControl3.Controls.Add(this.labelControl13);
            this.panelControl3.Controls.Add(this.txtMobile2);
            this.panelControl3.Controls.Add(this.labelControl12);
            this.panelControl3.Controls.Add(this.txtMobile1);
            this.panelControl3.Controls.Add(this.labelControl11);
            this.panelControl3.Controls.Add(this.txtPhone2);
            this.panelControl3.Controls.Add(this.labelControl10);
            this.panelControl3.Controls.Add(this.txtPhone1);
            this.panelControl3.Controls.Add(this.labelControl9);
            this.panelControl3.Location = new System.Drawing.Point(12, 367);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(400, 257);
            this.panelControl3.TabIndex = 2;
            // 
            // txtEmail2
            // 
            this.txtEmail2.Location = new System.Drawing.Point(83, 221);
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtEmail2.Properties.Appearance.Options.UseFont = true;
            this.txtEmail2.Size = new System.Drawing.Size(303, 24);
            this.txtEmail2.TabIndex = 25;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(35, 224);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(42, 17);
            this.labelControl16.TabIndex = 24;
            this.labelControl16.Text = "Email 2";
            // 
            // txtEmail1
            // 
            this.txtEmail1.Location = new System.Drawing.Point(83, 191);
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtEmail1.Properties.Appearance.Options.UseFont = true;
            this.txtEmail1.Size = new System.Drawing.Size(303, 24);
            this.txtEmail1.TabIndex = 23;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Location = new System.Drawing.Point(35, 194);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(42, 17);
            this.labelControl15.TabIndex = 22;
            this.labelControl15.Text = "Email 1";
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(83, 161);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWebsite.Properties.Appearance.Options.UseFont = true;
            this.txtWebsite.Size = new System.Drawing.Size(303, 24);
            this.txtWebsite.TabIndex = 21;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(30, 164);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(47, 17);
            this.labelControl14.TabIndex = 20;
            this.labelControl14.Text = "Website";
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(83, 131);
            this.txtFax.Name = "txtFax";
            this.txtFax.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFax.Properties.Appearance.Options.UseFont = true;
            this.txtFax.Size = new System.Drawing.Size(303, 24);
            this.txtFax.TabIndex = 19;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(58, 134);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(19, 17);
            this.labelControl13.TabIndex = 18;
            this.labelControl13.Text = "Fax";
            // 
            // txtMobile2
            // 
            this.txtMobile2.Location = new System.Drawing.Point(83, 101);
            this.txtMobile2.Name = "txtMobile2";
            this.txtMobile2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMobile2.Properties.Appearance.Options.UseFont = true;
            this.txtMobile2.Size = new System.Drawing.Size(303, 24);
            this.txtMobile2.TabIndex = 17;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(25, 104);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(52, 17);
            this.labelControl12.TabIndex = 16;
            this.labelControl12.Text = "Mobile 2";
            // 
            // txtMobile1
            // 
            this.txtMobile1.Location = new System.Drawing.Point(83, 71);
            this.txtMobile1.Name = "txtMobile1";
            this.txtMobile1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMobile1.Properties.Appearance.Options.UseFont = true;
            this.txtMobile1.Size = new System.Drawing.Size(303, 24);
            this.txtMobile1.TabIndex = 15;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(25, 74);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(52, 17);
            this.labelControl11.TabIndex = 14;
            this.labelControl11.Text = "Mobile 1";
            // 
            // txtPhone2
            // 
            this.txtPhone2.Location = new System.Drawing.Point(83, 41);
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhone2.Properties.Appearance.Options.UseFont = true;
            this.txtPhone2.Size = new System.Drawing.Size(303, 24);
            this.txtPhone2.TabIndex = 13;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(30, 44);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(47, 17);
            this.labelControl10.TabIndex = 12;
            this.labelControl10.Text = "Phone 2";
            // 
            // txtPhone1
            // 
            this.txtPhone1.Location = new System.Drawing.Point(83, 11);
            this.txtPhone1.Name = "txtPhone1";
            this.txtPhone1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhone1.Properties.Appearance.Options.UseFont = true;
            this.txtPhone1.Size = new System.Drawing.Size(303, 24);
            this.txtPhone1.TabIndex = 11;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(30, 14);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(47, 17);
            this.labelControl9.TabIndex = 10;
            this.labelControl9.Text = "Phone 1";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.txtZipCode);
            this.panelControl4.Controls.Add(this.labelControl21);
            this.panelControl4.Controls.Add(this.txtCountry);
            this.panelControl4.Controls.Add(this.labelControl20);
            this.panelControl4.Controls.Add(this.txtState);
            this.panelControl4.Controls.Add(this.labelControl19);
            this.panelControl4.Controls.Add(this.txtCity);
            this.panelControl4.Controls.Add(this.labelControl18);
            this.panelControl4.Controls.Add(this.txtAddress);
            this.panelControl4.Controls.Add(this.labelControl17);
            this.panelControl4.Location = new System.Drawing.Point(418, 12);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(464, 105);
            this.panelControl4.TabIndex = 3;
            // 
            // txtZipCode
            // 
            this.txtZipCode.Location = new System.Drawing.Point(307, 72);
            this.txtZipCode.Name = "txtZipCode";
            this.txtZipCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtZipCode.Properties.Appearance.Options.UseFont = true;
            this.txtZipCode.Size = new System.Drawing.Size(141, 24);
            this.txtZipCode.TabIndex = 21;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(248, 75);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(53, 17);
            this.labelControl21.TabIndex = 20;
            this.labelControl21.Text = "Zip Code";
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(68, 72);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCountry.Properties.Appearance.Options.UseFont = true;
            this.txtCountry.Size = new System.Drawing.Size(165, 24);
            this.txtCountry.TabIndex = 19;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(17, 75);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(45, 17);
            this.labelControl20.TabIndex = 18;
            this.labelControl20.Text = "Country";
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(283, 42);
            this.txtState.Name = "txtState";
            this.txtState.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtState.Properties.Appearance.Options.UseFont = true;
            this.txtState.Size = new System.Drawing.Size(165, 24);
            this.txtState.TabIndex = 17;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Location = new System.Drawing.Point(248, 45);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(29, 17);
            this.labelControl19.TabIndex = 16;
            this.labelControl19.Text = "State";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(68, 42);
            this.txtCity.Name = "txtCity";
            this.txtCity.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCity.Properties.Appearance.Options.UseFont = true;
            this.txtCity.Size = new System.Drawing.Size(165, 24);
            this.txtCity.TabIndex = 15;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(41, 45);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(21, 17);
            this.labelControl18.TabIndex = 14;
            this.labelControl18.Text = "City";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(68, 12);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(380, 24);
            this.txtAddress.TabIndex = 13;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(14, 15);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(48, 17);
            this.labelControl17.TabIndex = 12;
            this.labelControl17.Text = "Address";
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.txtRegisterNumber);
            this.panelControl5.Controls.Add(this.labelControl24);
            this.panelControl5.Controls.Add(this.txtTaxNumber);
            this.panelControl5.Controls.Add(this.labelControl23);
            this.panelControl5.Controls.Add(this.txtAccountNumber);
            this.panelControl5.Controls.Add(this.labelControl22);
            this.panelControl5.Controls.Add(this.txtCompanyName);
            this.panelControl5.Controls.Add(this.labelControl26);
            this.panelControl5.Location = new System.Drawing.Point(418, 123);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(464, 135);
            this.panelControl5.TabIndex = 4;
            // 
            // txtRegisterNumber
            // 
            this.txtRegisterNumber.Location = new System.Drawing.Point(114, 100);
            this.txtRegisterNumber.Name = "txtRegisterNumber";
            this.txtRegisterNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtRegisterNumber.Properties.Appearance.Options.UseFont = true;
            this.txtRegisterNumber.Size = new System.Drawing.Size(334, 24);
            this.txtRegisterNumber.TabIndex = 19;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl24.Location = new System.Drawing.Point(25, 103);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(83, 17);
            this.labelControl24.TabIndex = 18;
            this.labelControl24.Text = "Register Num.";
            // 
            // txtTaxNumber
            // 
            this.txtTaxNumber.Location = new System.Drawing.Point(114, 70);
            this.txtTaxNumber.Name = "txtTaxNumber";
            this.txtTaxNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTaxNumber.Properties.Appearance.Options.UseFont = true;
            this.txtTaxNumber.Size = new System.Drawing.Size(334, 24);
            this.txtTaxNumber.TabIndex = 17;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl23.Location = new System.Drawing.Point(53, 73);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(55, 17);
            this.labelControl23.TabIndex = 16;
            this.labelControl23.Text = "Tax Num.";
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.Location = new System.Drawing.Point(114, 40);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAccountNumber.Properties.Appearance.Options.UseFont = true;
            this.txtAccountNumber.Size = new System.Drawing.Size(334, 24);
            this.txtAccountNumber.TabIndex = 15;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl22.Location = new System.Drawing.Point(27, 43);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(81, 17);
            this.labelControl22.TabIndex = 14;
            this.labelControl22.Text = "Account Num.";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(114, 10);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCompanyName.Properties.Appearance.Options.UseFont = true;
            this.txtCompanyName.Size = new System.Drawing.Size(334, 24);
            this.txtCompanyName.TabIndex = 13;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl26.Location = new System.Drawing.Point(14, 13);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(94, 17);
            this.labelControl26.TabIndex = 12;
            this.labelControl26.Text = "Company Name";
            // 
            // picImage
            // 
            this.picImage.Location = new System.Drawing.Point(655, 394);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(227, 230);
            this.picImage.TabIndex = 7;
            this.picImage.DoubleClick += new System.EventHandler(this.picImage_DoubleClick);
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.txtRemarks);
            this.groupControl1.Location = new System.Drawing.Point(418, 394);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(231, 230);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRemarks.Location = new System.Drawing.Point(2, 25);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(227, 203);
            this.txtRemarks.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(802, 630);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 23;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(696, 630);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 22;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.txtHomeAddress);
            this.groupControl2.Location = new System.Drawing.Point(418, 264);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(464, 124);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "Home Address";
            // 
            // txtHomeAddress
            // 
            this.txtHomeAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtHomeAddress.Location = new System.Drawing.Point(2, 25);
            this.txtHomeAddress.Name = "txtHomeAddress";
            this.txtHomeAddress.Size = new System.Drawing.Size(460, 97);
            this.txtHomeAddress.TabIndex = 0;
            // 
            // cmbUserAccount2
            // 
            this.cmbUserAccount2.EditValue = "";
            this.cmbUserAccount2.Location = new System.Drawing.Point(91, 156);
            this.cmbUserAccount2.Name = "cmbUserAccount2";
            this.cmbUserAccount2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbUserAccount2.Properties.Appearance.Options.UseFont = true;
            this.cmbUserAccount2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUserAccount2.Properties.DisplayMember = "Name";
            this.cmbUserAccount2.Properties.NullText = "";
            this.cmbUserAccount2.Properties.ValueMember = "id";
            this.cmbUserAccount2.Properties.View = this.gridView2;
            this.cmbUserAccount2.Size = new System.Drawing.Size(295, 24);
            this.cmbUserAccount2.TabIndex = 13;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 23;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "id";
            this.gridColumn7.FieldName = "id";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Name";
            this.gridColumn9.FieldName = "Name";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            this.gridColumn9.Width = 264;
            // 
            // cmbUserAccount3
            // 
            this.cmbUserAccount3.EditValue = "";
            this.cmbUserAccount3.Location = new System.Drawing.Point(91, 181);
            this.cmbUserAccount3.Name = "cmbUserAccount3";
            this.cmbUserAccount3.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbUserAccount3.Properties.Appearance.Options.UseFont = true;
            this.cmbUserAccount3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUserAccount3.Properties.DisplayMember = "Name";
            this.cmbUserAccount3.Properties.NullText = "";
            this.cmbUserAccount3.Properties.ValueMember = "id";
            this.cmbUserAccount3.Properties.View = this.gridView5;
            this.cmbUserAccount3.Size = new System.Drawing.Size(295, 24);
            this.cmbUserAccount3.TabIndex = 14;
            // 
            // gridView5
            // 
            this.gridView5.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView5.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView5.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView5.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView5.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView5.Appearance.Row.Options.UseFont = true;
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.RowHeight = 23;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "id";
            this.gridColumn13.FieldName = "id";
            this.gridColumn13.Name = "gridColumn13";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Name";
            this.gridColumn14.FieldName = "Name";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 264;
            // 
            // frmNewContact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 672);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.picImage);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewContact";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Contact";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMidName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPriority.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSubDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtZipCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegisterNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRemarks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtHomeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLastName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtMidName;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtFirstName;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbDepartment;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbSubDepartment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txtEmail2;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtEmail1;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtWebsite;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtMobile2;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtMobile1;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtPhone2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtPhone1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TextEdit txtState;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtCity;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtZipCode;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtCountry;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.TextEdit txtRegisterNumber;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtTaxNumber;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit txtAccountNumber;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtCompanyName;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.PictureEdit picImage;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.MemoEdit txtRemarks;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTitle;
        private DevExpress.XtraEditors.SimpleButton btnPriority;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbPriority;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbUserAccount;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.TextEdit txtPosition;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.MemoEdit txtHomeAddress;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbUserAccount3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbUserAccount2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
    }
}