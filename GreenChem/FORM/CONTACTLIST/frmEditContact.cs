﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Linq;

namespace GreenChem.MenuData
{
    public partial class frmEditContact : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditContact(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            //cmbGroup.Properties.DataSource = db.Groups.GetName();
            cmbPriority.Properties.DataSource = db.Priorities.GetName();
            cmbUserAccount.Properties.DataSource = db.UserAccounts.GetName();
            cmbUserAccount2.Properties.DataSource = db.UserAccounts.GetName();
            cmbUserAccount3.Properties.DataSource = db.UserAccounts.GetName();

            if (!db.CheckUserRole(frmUtama._iduseraccount.Value, "Administrator"))
                cmbUserAccount.Enabled = false;

            var contact = db.Contacts.First(x => x.id == _id);
            txtCode.Text = contact.Code;
            cmbTitle.Text = contact.Title;
            txtFirstName.Text = contact.FirstName;
            txtMidName.Text = contact.MiddleName;
            txtLastName.Text = contact.LastName;
            cmbDepartment.EditValue = contact.idDepartment;
            cmbSubDepartment.EditValue = contact.idSubDepartment;
            cmbPriority.EditValue = contact.idPriority;
            txtPosition.Text = contact.Position;
            //cmbGroup.EditValue = contact.idGroup;
            cmbUserAccount.EditValue = contact.idUserAccount;
            cmbUserAccount2.EditValue = contact.idUserAccount2;
            cmbUserAccount3.EditValue = contact.idUserAccount3;
            txtPhone1.Text = contact.Phone1;
            txtPhone2.Text = contact.Phone2;
            txtMobile1.Text = contact.Mobile1;
            txtMobile2.Text = contact.Mobile2;
            txtFax.Text = contact.Fax;
            txtWebsite.Text = contact.Website;
            txtEmail1.Text = contact.Email1;
            txtEmail2.Text = contact.Email2;
            txtAddress.Text = contact.Address;
            txtCity.Text = contact.City;
            txtState.Text = contact.State;
            txtCountry.Text = contact.Country;
            txtZipCode.Text = contact.ZipCode;
            txtCompanyName.Text = contact.CompanyName;
            txtAccountNumber.Text = contact.AccountNumber;
            txtTaxNumber.Text = contact.TaxNumber;
            txtRegisterNumber.Text = contact.RegisterNumber;
            txtRemarks.Text = contact.Remarks;
            if (contact.Image != null) picImage.Image = Functions.ByteArrayToImage(contact.Image.ToArray());
            txtHomeAddress.Text = contact.HomeAddress;
        }

        private void btnGroup_Click(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            //frmDataGroup form = new frmDataGroup();
            //form.ShowDialog();
            //cmbGroup.Properties.DataSource = db.Groups.GetName();
            //Cursor.Current = Cursors.Default;
        }

        private void picImage_DoubleClick(object sender, EventArgs e)
        {
            picImage.LoadImage();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                Contact contact = db.Contacts.First(x => x.id == _id);
                contact.Code = txtCode.Text;
                contact.Title = cmbTitle.Text;
                contact.FirstName = txtFirstName.Text;
                contact.MiddleName = txtMidName.Text;
                contact.LastName = txtLastName.Text;
                contact.Position = txtPosition.Text;
                if (cmbDepartment.EditValue.IsNotEmpty()) contact.idDepartment = (int?)cmbDepartment.EditValue;
                if (cmbSubDepartment.EditValue.IsNotEmpty()) contact.idSubDepartment = (int?)cmbSubDepartment.EditValue;
                //if (cmbGroup.EditValue.IsNotEmpty()) contact.idGroup = (int?)cmbGroup.EditValue;
                if (cmbPriority.EditValue.IsNotEmpty()) contact.idPriority = (int?)cmbPriority.EditValue;
                if (cmbUserAccount.EditValue.IsNotEmpty()) contact.idUserAccount = (int?)cmbUserAccount.EditValue;
                if (cmbUserAccount2.EditValue.IsNotEmpty()) contact.idUserAccount2 = (int?)cmbUserAccount2.EditValue;
                if (cmbUserAccount3.EditValue.IsNotEmpty()) contact.idUserAccount3 = (int?)cmbUserAccount3.EditValue;
                contact.Phone1 = txtPhone1.Text;
                contact.Phone2 = txtPhone2.Text;
                contact.Mobile1 = txtMobile1.Text;
                contact.Mobile2 = txtMobile2.Text;
                contact.Fax = txtFax.Text;
                contact.Website = txtWebsite.Text;
                contact.Email1 = txtEmail1.Text;
                contact.Email2 = txtEmail2.Text;
                contact.Address = txtAddress.Text;
                contact.City = txtCity.Text;
                contact.Country = txtCountry.Text;
                contact.ZipCode = txtZipCode.Text;
                contact.CompanyName = txtCompanyName.Text;
                contact.AccountNumber = txtAccountNumber.Text;
                contact.TaxNumber = txtTaxNumber.Text;
                contact.RegisterNumber = txtRegisterNumber.Text;
                contact.Remarks = txtRemarks.Text;
                contact.Active = true;
                if (picImage.Image != null) contact.Image = picImage.Image.ImageToByteArray();
                else contact.Image = null;
                txtHomeAddress.Text = contact.HomeAddress;
                contact.DateModified = DateTime.Now;
                contact.idModifiedBy = frmUtama._iduseraccount;

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPriority_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDataPriority form = new frmDataPriority();
            form.ShowDialog();
            cmbPriority.Properties.DataSource = db.Priorities.GetName();
            Cursor.Current = Cursors.Default;
        }
    }
}