﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmDataContact : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataContact()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New ContactList");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit ContactList");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete ContactList");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print ContactList");

            if (System.IO.File.Exists("layout_contactlist.xml"))
                gridView.RestoreLayoutFromXml("layout_contactlist.xml");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.Contacts
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.idDepartment,
                                x.idSubDepartment,
                                x.idGroup,
                                x.idUserAccount,
                                x.idUserAccount2,
                                x.idUserAccount3,
                                x.Code,
                                x.Title,
                                x.FirstName,
                                x.MiddleName,
                                x.LastName,
                                x.Address,
                                x.City,
                                x.Country,
                                x.ZipCode,
                                x.Phone1,
                                x.Phone2,
                                x.Mobile1,
                                x.Mobile2,
                                x.Fax,
                                x.Website,
                                x.Email1,
                                x.Email2,
                                x.CompanyName,
                                x.AccountNumber,
                                x.TaxNumber,
                                x.RegisterNumber,
                                x.Remarks,
                                Department= x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                Group = x.Group.Name,
                                Priority = x.idPriority != null ? db.Priorities.First(a => a.id == x.idPriority).Name : "",
                                x.Position,
                                x.HomeAddress,
                                x.DateModified,
                                ModifiedBy = x.idModifiedBy != null ? db.UserAccounts.First(a => a.id == x.idModifiedBy).Name : ""
                            };

                if (db.CheckUserRole(frmUtama._iduseraccount.Value, "Manager"))
                {
                    if (frmUtama._iddepartment != null)
                        query = query.Where(x => x.idDepartment == null || x.idDepartment == frmUtama._iddepartment);
                }
                if (db.CheckUserRole(frmUtama._iduseraccount.Value, "User"))
                {
                    query = query.Where(x => x.idUserAccount == frmUtama._iduseraccount || x.idUserAccount2 == frmUtama._iduseraccount || x.idUserAccount3 == frmUtama._iduseraccount);
                }

                //bool admin = db.CheckUserRole(frmUtama._iduseraccount.Value ,"Administrator");
                //if (!admin)
                //{
                //    //if (frmUtama._iddepartment != null)
                //    //    query = query.Where(x => x.idDepartment == null || x.idDepartment == frmUtama._iddepartment);
                //    //if (frmUtama._idsubdepartment != null)
                //    //    query = query.Where(x => x.idSubDepartment == null || x.idSubDepartment == frmUtama._idsubdepartment);
                //    //if (frmUtama._idgroup != null)
                //    //    query = query.Where(x => x.idGroup == null || x.idGroup == frmUtama._idgroup);
                //    if (!db.CheckUserRole(frmUtama._iduseraccount.Value, "[Contact Visibility]"))
                //        query = query.Where(x => x.idUserAccount == frmUtama._iduseraccount);
                //}

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Code == txtSearch.Text ||
                        x.FirstName.Contains(txtSearch.Text) ||
                        x.MiddleName.Contains(txtSearch.Text) ||
                        x.LastName.Contains(txtSearch.Text) ||
                        x.Phone1.Contains(txtSearch.Text) ||
                        x.Mobile1.Contains(txtSearch.Text) ||
                        x.Fax == txtSearch.Text ||
                        x.Email1.Contains(txtSearch.Text) ||
                        x.Website.Contains(txtSearch.Text) ||
                        x.Address.Contains(txtSearch.Text) ||
                        x.City.Contains(txtSearch.Text) ||
                        x.CompanyName.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewContact form = new frmNewContact();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditContact form = new frmEditContact(id);
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("FirstName").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this contact : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    Contact contact = db.Contacts.First(x => x.id == id);
                    contact.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Contact deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click_1(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditContact form = new frmEditContact(id);
            form.btnSave.Visible = false;
            form.Text = "View Contact";
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.Contacts
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.idDepartment,
                                x.idSubDepartment,
                                x.idGroup,
                                x.idUserAccount,
                                x.Code,
                                x.Title,
                                x.FirstName,
                                x.MiddleName,
                                x.LastName,
                                x.Address,
                                x.City,
                                x.Country,
                                x.ZipCode,
                                x.Phone1,
                                x.Phone2,
                                x.Mobile1,
                                x.Mobile2,
                                x.Fax,
                                x.Website,
                                x.Email1,
                                x.Email2,
                                x.CompanyName,
                                x.AccountNumber,
                                x.TaxNumber,
                                x.RegisterNumber,
                                x.Remarks,
                                Department = x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                Group = x.Group.Name,
                                Priority = x.idPriority != null ? db.Priorities.First(a => a.id == x.idPriority).Name : ""
                            };

                bool admin = db.CheckUserRole(frmUtama._iduseraccount.Value, "Administrator");
                if (!admin)
                {
                    if (frmUtama._iddepartment != null)
                        query = query.Where(x => x.idDepartment == null || x.idDepartment == frmUtama._iddepartment);
                    if (frmUtama._idsubdepartment != null)
                        query = query.Where(x => x.idSubDepartment == null || x.idSubDepartment == frmUtama._idsubdepartment);
                    if (frmUtama._idgroup != null)
                        query = query.Where(x => x.idGroup == null || x.idGroup == frmUtama._idgroup);
                    if (!db.CheckUserRole(frmUtama._iduseraccount.Value, "[Contact Visibility]"))
                        query = query.Where(x => x.idUserAccount == frmUtama._iduseraccount);
                }

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Code == txtSearch.Text ||
                        x.FirstName.Contains(txtSearch.Text) ||
                        x.MiddleName.Contains(txtSearch.Text) ||
                        x.LastName.Contains(txtSearch.Text) ||
                        x.Phone1.Contains(txtSearch.Text) ||
                        x.Mobile1.Contains(txtSearch.Text) ||
                        x.Fax == txtSearch.Text ||
                        x.Email1.Contains(txtSearch.Text) ||
                        x.Website.Contains(txtSearch.Text) ||
                        x.Address.Contains(txtSearch.Text) ||
                        x.City.Contains(txtSearch.Text) ||
                        x.CompanyName.Contains(txtSearch.Text));

                rptContactList report = new rptContactList();
                report.DataSource = query;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_contactlist.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}