﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Linq;

namespace GreenChem.MenuData
{
    public partial class frmNewContact : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewContact()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            //cmbGroup.Properties.DataSource = db.Groups.GetName();
            cmbPriority.Properties.DataSource = db.Priorities.GetName();
            cmbUserAccount.Properties.DataSource = db.UserAccounts.GetName();
            cmbUserAccount2.Properties.DataSource = db.UserAccounts.GetName();
            cmbUserAccount3.Properties.DataSource = db.UserAccounts.GetName();
            cmbUserAccount.EditValue = frmUtama._iduseraccount;
            cmbDepartment.EditValue = frmUtama._iddepartment;
            cmbSubDepartment.EditValue = frmUtama._idsubdepartment;

            GenerateCode();
        }

        private void GenerateCode()
        {
            try
            {
                string prefix = db.Departments.First(x => x.id == cmbDepartment.EditValue.ToInteger()).Code;

                var query = (from x in db.Contacts
                             where x.Code.StartsWith(prefix)
                             orderby x.Code descending
                             select x.Code).First();

                int newnumber = query.Substring(prefix.Length, 5).ToInteger() + 1;
                txtCode.Text = prefix + newnumber.ToString("00000");

            }
            catch
            {
                string prefix = db.Departments.First(x => x.id == cmbDepartment.EditValue.ToInteger()).Code;
                txtCode.Text = prefix + "00001";
            }
        }

        private void btnGroup_Click(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            //frmDataGroup form = new frmDataGroup();
            //form.ShowDialog();
            //cmbGroup.Properties.DataSource = db.Groups.GetName();
            //Cursor.Current = Cursors.Default;
        }

        private void picImage_DoubleClick(object sender, EventArgs e)
        {
            picImage.LoadImage();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                Contact contact = new Contact();
                contact.Code = txtCode.Text;
                contact.Title = cmbTitle.Text;
                contact.FirstName = txtFirstName.Text;
                contact.MiddleName = txtMidName.Text;
                contact.LastName = txtLastName.Text;
                contact.Position = txtPosition.Text;
                if (cmbDepartment.EditValue.IsNotEmpty()) contact.idDepartment = (int?)cmbDepartment.EditValue;
                if (cmbSubDepartment.EditValue.IsNotEmpty()) contact.idSubDepartment = (int?)cmbSubDepartment.EditValue;
                //if (cmbGroup.EditValue.IsNotEmpty()) contact.idGroup = (int?)cmbGroup.EditValue;
                if (cmbPriority.EditValue.IsNotEmpty()) contact.idPriority = (int?)cmbPriority.EditValue;
                if (cmbUserAccount.EditValue.IsNotEmpty()) contact.idUserAccount = (int?)cmbUserAccount.EditValue;
                if (cmbUserAccount2.EditValue.IsNotEmpty()) contact.idUserAccount2 = (int?)cmbUserAccount2.EditValue;
                if (cmbUserAccount3.EditValue.IsNotEmpty()) contact.idUserAccount3 = (int?)cmbUserAccount3.EditValue;
                contact.Phone1 = txtPhone1.Text;
                contact.Phone2 = txtPhone2.Text;
                contact.Mobile1 = txtMobile1.Text;
                contact.Mobile2 = txtMobile2.Text;
                contact.Fax = txtFax.Text;
                contact.Website = txtWebsite.Text;
                contact.Email1 = txtEmail1.Text;
                contact.Email2 = txtEmail2.Text;
                contact.Address = txtAddress.Text;
                contact.City = txtCity.Text;
                contact.Country = txtCountry.Text;
                contact.ZipCode = txtZipCode.Text;
                contact.CompanyName = txtCompanyName.Text;
                contact.AccountNumber = txtAccountNumber.Text;
                contact.TaxNumber = txtTaxNumber.Text;
                contact.RegisterNumber = txtRegisterNumber.Text;
                contact.Remarks = txtRemarks.Text;
                contact.Active = true;
                if (picImage.Image != null) contact.Image = picImage.Image.ImageToByteArray();
                contact.HomeAddress = txtHomeAddress.Text;
                contact.DateModified = DateTime.Now;
                contact.idModifiedBy = frmUtama._iduseraccount;

                db.Contacts.InsertOnSubmit(contact);
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to cancel?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                Close();
            }
        }

        private void btnPriority_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDataPriority form = new frmDataPriority();
            form.ShowDialog();
            cmbPriority.Properties.DataSource = db.Priorities.GetName();
            Cursor.Current = Cursors.Default;
        }
    }
}