﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmBahanBaku : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmBahanBaku()
        {
            InitializeComponent();
            RefreshData();
            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Bahan Baku");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Bahan Baku");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Bahan Baku");
            btnRecap.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Recap Bahan Baku");
            btnFisik.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Fisik Bahan Baku");
            btnTipe.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Tipe Bahan Baku");
            btnEditSupplier.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Supplier Bahan Baku");
            cmbStatus.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Inactive Bahan Baku");

            if (System.IO.File.Exists("layout_bahanbaku.xml"))
                gridView.RestoreLayoutFromXml("layout_bahanbaku.xml");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.BahanBakus
                            where x.Active == true && x.StatusBahanBaku == cmbStatus.Text
                            orderby x.StatusBahanBaku, x.Nama
                            select new
                            {
                                x.id,
                                x.Nomor,
                                x.Kode,
                                //x.KodeMSDS,
                                x.Nama,
                                Fisik = x.Fisik.Name,
                                Tipe = x.Tipe.Name,
                                x.Fungsi,
                                Supplier = x.Supplier.Name,
                                Supplier2 = x.idSupplier2 != null ? db.Suppliers.First(a => a.id == x.idSupplier2).Name : "",
                                Supplier3 = x.idSupplier3 != null ? db.Suppliers.First(a => a.id == x.idSupplier3).Name : "",
                                Address = x.Supplier.Address,
                                Phone = x.Supplier.Phone,
                                Fax = x.Supplier.Fax,
                                Website = x.Supplier.Website,
                                Person = x.Supplier.Person,
                                x.StatusBahanBaku
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Nomor == txtSearch.Text ||
                            x.Kode == txtSearch.Text ||
                            x.Nama.Contains(txtSearch.Text) ||
                            x.Supplier.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewBahanBaku form = new frmNewBahanBaku();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditBahanBaku form = new frmEditBahanBaku(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this item : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    BahanBaku bahan = db.BahanBakus.First(x => x.id == id);
                    bahan.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("item deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportBahanBaku form = new MenuReport.frmReportBahanBaku();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }

        private void btnFisik_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDataFisik form = new frmDataFisik();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnTipe_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmTipe form = new frmTipe();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditBahanBaku form = new frmEditBahanBaku(id);
                form.btnSave.Visible = false;
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_bahanbaku.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnEditSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditBahanBaku form = new frmEditBahanBaku(id);
                form.txtKode.Enabled = false;
                form.txtNama.Enabled = false;
                form.txtMSDS.Enabled = false;
                form.cmbFisik.Enabled = false;
                form.cmbTipe.Enabled = false;
                form.txtFungsi.Enabled = false;
                form.cmbStatus.Enabled = false;
                form.txtKodeMSDS.Enabled = false;
                form.btnMSDS.Enabled = false;
                form.lblMSDS.Enabled = false;
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}