﻿namespace GreenChem.MenuData
{
    partial class frmNewBahanBaku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblMSDS = new DevExpress.XtraEditors.LabelControl();
            this.btnMSDS = new DevExpress.XtraEditors.SimpleButton();
            this.txtMSDS = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSupplier3 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSupplier2 = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtKodeMSDS = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSupplier = new DevExpress.XtraEditors.LookUpEdit();
            this.txtFungsi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtNomor = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTipe = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbFisik = new DevExpress.XtraEditors.LookUpEdit();
            this.txtNama = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtKode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSDS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKodeMSDS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFungsi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFisik.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblMSDS);
            this.panelControl1.Controls.Add(this.btnMSDS);
            this.panelControl1.Controls.Add(this.txtMSDS);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.cmbSupplier3);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.cmbSupplier2);
            this.panelControl1.Controls.Add(this.cmbStatus);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtKodeMSDS);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.cmbSupplier);
            this.panelControl1.Controls.Add(this.txtFungsi);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtNomor);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.cmbTipe);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.cmbFisik);
            this.panelControl1.Controls.Add(this.txtNama);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtKode);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 351);
            this.panelControl1.TabIndex = 0;
            // 
            // lblMSDS
            // 
            this.lblMSDS.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblMSDS.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lblMSDS.Location = new System.Drawing.Point(421, 318);
            this.lblMSDS.Name = "lblMSDS";
            this.lblMSDS.Size = new System.Drawing.Size(27, 17);
            this.lblMSDS.TabIndex = 43;
            this.lblMSDS.Text = "View";
            this.lblMSDS.Click += new System.EventHandler(this.lblMSDS_Click);
            // 
            // btnMSDS
            // 
            this.btnMSDS.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnMSDS.Appearance.Options.UseFont = true;
            this.btnMSDS.Location = new System.Drawing.Point(365, 314);
            this.btnMSDS.Name = "btnMSDS";
            this.btnMSDS.Size = new System.Drawing.Size(50, 25);
            this.btnMSDS.TabIndex = 42;
            this.btnMSDS.Text = "...";
            this.btnMSDS.Click += new System.EventHandler(this.btnMSDS_Click);
            // 
            // txtMSDS
            // 
            this.txtMSDS.Location = new System.Drawing.Point(75, 314);
            this.txtMSDS.Name = "txtMSDS";
            this.txtMSDS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMSDS.Properties.Appearance.Options.UseFont = true;
            this.txtMSDS.Size = new System.Drawing.Size(284, 24);
            this.txtMSDS.TabIndex = 41;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(4, 318);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(65, 17);
            this.labelControl18.TabIndex = 40;
            this.labelControl18.Text = "Attachment";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(10, 258);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(59, 17);
            this.labelControl10.TabIndex = 18;
            this.labelControl10.Text = "Supplier 3";
            // 
            // cmbSupplier3
            // 
            this.cmbSupplier3.Location = new System.Drawing.Point(75, 255);
            this.cmbSupplier3.Name = "cmbSupplier3";
            this.cmbSupplier3.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier3.Properties.Appearance.Options.UseFont = true;
            this.cmbSupplier3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbSupplier3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSupplier3.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbSupplier3.Properties.DisplayMember = "Name";
            this.cmbSupplier3.Properties.NullText = "";
            this.cmbSupplier3.Properties.ShowFooter = false;
            this.cmbSupplier3.Properties.ShowHeader = false;
            this.cmbSupplier3.Properties.ValueMember = "id";
            this.cmbSupplier3.Size = new System.Drawing.Size(373, 24);
            this.cmbSupplier3.TabIndex = 19;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(10, 228);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(59, 17);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Supplier 2";
            // 
            // cmbSupplier2
            // 
            this.cmbSupplier2.Location = new System.Drawing.Point(75, 225);
            this.cmbSupplier2.Name = "cmbSupplier2";
            this.cmbSupplier2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier2.Properties.Appearance.Options.UseFont = true;
            this.cmbSupplier2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbSupplier2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSupplier2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbSupplier2.Properties.DisplayMember = "Name";
            this.cmbSupplier2.Properties.NullText = "";
            this.cmbSupplier2.Properties.ShowFooter = false;
            this.cmbSupplier2.Properties.ShowHeader = false;
            this.cmbSupplier2.Properties.ValueMember = "id";
            this.cmbSupplier2.Size = new System.Drawing.Size(373, 24);
            this.cmbSupplier2.TabIndex = 17;
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "Active";
            this.cmbStatus.Location = new System.Drawing.Point(75, 285);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.cmbStatus.Size = new System.Drawing.Size(140, 23);
            this.cmbStatus.TabIndex = 21;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(32, 287);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(35, 17);
            this.labelControl8.TabIndex = 20;
            this.labelControl8.Text = "Status";
            // 
            // txtKodeMSDS
            // 
            this.txtKodeMSDS.Location = new System.Drawing.Point(75, 75);
            this.txtKodeMSDS.Name = "txtKodeMSDS";
            this.txtKodeMSDS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKodeMSDS.Properties.Appearance.Options.UseFont = true;
            this.txtKodeMSDS.Size = new System.Drawing.Size(373, 24);
            this.txtKodeMSDS.TabIndex = 7;
            this.txtKodeMSDS.Visible = false;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(34, 78);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(35, 17);
            this.labelControl11.TabIndex = 6;
            this.labelControl11.Text = "MSDS";
            this.labelControl11.Visible = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(10, 198);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(59, 17);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Supplier 1";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.Location = new System.Drawing.Point(75, 195);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier.Properties.Appearance.Options.UseFont = true;
            this.cmbSupplier.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbSupplier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSupplier.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbSupplier.Properties.DisplayMember = "Name";
            this.cmbSupplier.Properties.NullText = "";
            this.cmbSupplier.Properties.ShowFooter = false;
            this.cmbSupplier.Properties.ShowHeader = false;
            this.cmbSupplier.Properties.ValueMember = "id";
            this.cmbSupplier.Size = new System.Drawing.Size(373, 24);
            this.cmbSupplier.TabIndex = 15;
            // 
            // txtFungsi
            // 
            this.txtFungsi.Location = new System.Drawing.Point(75, 165);
            this.txtFungsi.Name = "txtFungsi";
            this.txtFungsi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFungsi.Properties.Appearance.Options.UseFont = true;
            this.txtFungsi.Size = new System.Drawing.Size(373, 24);
            this.txtFungsi.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(32, 168);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(37, 17);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Fungsi";
            // 
            // txtNomor
            // 
            this.txtNomor.Location = new System.Drawing.Point(348, 15);
            this.txtNomor.Name = "txtNomor";
            this.txtNomor.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNomor.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtNomor.Properties.Appearance.Options.UseFont = true;
            this.txtNomor.Properties.Appearance.Options.UseForeColor = true;
            this.txtNomor.Properties.ReadOnly = true;
            this.txtNomor.Size = new System.Drawing.Size(100, 24);
            this.txtNomor.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(321, 18);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(21, 17);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "No.";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(44, 138);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(25, 17);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Tipe";
            // 
            // cmbTipe
            // 
            this.cmbTipe.Location = new System.Drawing.Point(75, 135);
            this.cmbTipe.Name = "cmbTipe";
            this.cmbTipe.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTipe.Properties.Appearance.Options.UseFont = true;
            this.cmbTipe.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTipe.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbTipe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTipe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbTipe.Properties.DisplayMember = "Name";
            this.cmbTipe.Properties.NullText = "";
            this.cmbTipe.Properties.ShowFooter = false;
            this.cmbTipe.Properties.ShowHeader = false;
            this.cmbTipe.Properties.ValueMember = "id";
            this.cmbTipe.Size = new System.Drawing.Size(373, 24);
            this.cmbTipe.TabIndex = 11;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(45, 108);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Fisik";
            // 
            // cmbFisik
            // 
            this.cmbFisik.Location = new System.Drawing.Point(75, 105);
            this.cmbFisik.Name = "cmbFisik";
            this.cmbFisik.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbFisik.Properties.Appearance.Options.UseFont = true;
            this.cmbFisik.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbFisik.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbFisik.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbFisik.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbFisik.Properties.DisplayMember = "Name";
            this.cmbFisik.Properties.NullText = "";
            this.cmbFisik.Properties.ShowFooter = false;
            this.cmbFisik.Properties.ShowHeader = false;
            this.cmbFisik.Properties.ValueMember = "id";
            this.cmbFisik.Size = new System.Drawing.Size(373, 24);
            this.cmbFisik.TabIndex = 9;
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(75, 45);
            this.txtNama.Name = "txtNama";
            this.txtNama.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNama.Properties.Appearance.Options.UseFont = true;
            this.txtNama.Size = new System.Drawing.Size(373, 24);
            this.txtNama.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(34, 48);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 17);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Nama";
            // 
            // txtKode
            // 
            this.txtKode.Location = new System.Drawing.Point(75, 15);
            this.txtKode.Name = "txtKode";
            this.txtKode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKode.Properties.Appearance.Options.UseFont = true;
            this.txtKode.Size = new System.Drawing.Size(200, 24);
            this.txtKode.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(38, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(31, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Kode";
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(402, 369);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(296, 369);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "All Files|*.*";
            this.openFileDialog.Title = "Open File";
            // 
            // frmNewBahanBaku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 412);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewBahanBaku";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Bahan Baku";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSDS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKodeMSDS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFungsi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFisik.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtNomor;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit cmbTipe;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit cmbFisik;
        private DevExpress.XtraEditors.TextEdit txtNama;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtKode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtFungsi;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit cmbSupplier;
        private DevExpress.XtraEditors.TextEdit txtKodeMSDS;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LookUpEdit cmbSupplier3;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LookUpEdit cmbSupplier2;
        private DevExpress.XtraEditors.LabelControl lblMSDS;
        private DevExpress.XtraEditors.SimpleButton btnMSDS;
        private DevExpress.XtraEditors.TextEdit txtMSDS;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}