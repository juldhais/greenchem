﻿namespace GreenChem.MenuData
{
    partial class frmNewBahanPenolong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.picture = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtNomor = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbKategori = new DevExpress.XtraEditors.LookUpEdit();
            this.txtNama = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtKode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSpecification = new DevExpress.XtraEditors.MemoEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblViewAttachment = new DevExpress.XtraEditors.LabelControl();
            this.btnAttachment = new DevExpress.XtraEditors.SimpleButton();
            this.txtAttachment = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKategori.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachment.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblViewAttachment);
            this.panelControl1.Controls.Add(this.btnAttachment);
            this.panelControl1.Controls.Add(this.txtAttachment);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.picture);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtNomor);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.cmbStatus);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.cmbKategori);
            this.panelControl1.Controls.Add(this.txtNama);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtKode);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtSpecification);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 502);
            this.panelControl1.TabIndex = 0;
            // 
            // picture
            // 
            this.picture.Location = new System.Drawing.Point(19, 207);
            this.picture.Name = "picture";
            this.picture.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picture.Size = new System.Drawing.Size(429, 250);
            this.picture.TabIndex = 12;
            this.picture.DoubleClick += new System.EventHandler(this.picture_DoubleClick);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(38, 138);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(31, 17);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Spec.";
            // 
            // txtNomor
            // 
            this.txtNomor.Location = new System.Drawing.Point(348, 15);
            this.txtNomor.Name = "txtNomor";
            this.txtNomor.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNomor.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtNomor.Properties.Appearance.Options.UseFont = true;
            this.txtNomor.Properties.Appearance.Options.UseForeColor = true;
            this.txtNomor.Properties.ReadOnly = true;
            this.txtNomor.Size = new System.Drawing.Size(100, 24);
            this.txtNomor.TabIndex = 9;
            this.txtNomor.TabStop = false;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(321, 18);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(21, 17);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "No.";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(34, 108);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(35, 17);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Status";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(75, 105);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbStatus.Properties.DisplayMember = "Name";
            this.cmbStatus.Properties.NullText = "";
            this.cmbStatus.Properties.ShowFooter = false;
            this.cmbStatus.Properties.ShowHeader = false;
            this.cmbStatus.Properties.ValueMember = "id";
            this.cmbStatus.Size = new System.Drawing.Size(373, 24);
            this.cmbStatus.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(19, 78);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(50, 17);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Kategori";
            // 
            // cmbKategori
            // 
            this.cmbKategori.Location = new System.Drawing.Point(75, 75);
            this.cmbKategori.Name = "cmbKategori";
            this.cmbKategori.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbKategori.Properties.Appearance.Options.UseFont = true;
            this.cmbKategori.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbKategori.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbKategori.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbKategori.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbKategori.Properties.DisplayMember = "Name";
            this.cmbKategori.Properties.NullText = "";
            this.cmbKategori.Properties.ShowFooter = false;
            this.cmbKategori.Properties.ShowHeader = false;
            this.cmbKategori.Properties.ValueMember = "id";
            this.cmbKategori.Size = new System.Drawing.Size(373, 24);
            this.cmbKategori.TabIndex = 5;
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(75, 45);
            this.txtNama.Name = "txtNama";
            this.txtNama.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNama.Properties.Appearance.Options.UseFont = true;
            this.txtNama.Size = new System.Drawing.Size(373, 24);
            this.txtNama.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(34, 48);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 17);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Nama";
            // 
            // txtKode
            // 
            this.txtKode.Location = new System.Drawing.Point(75, 15);
            this.txtKode.Name = "txtKode";
            this.txtKode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKode.Properties.Appearance.Options.UseFont = true;
            this.txtKode.Size = new System.Drawing.Size(200, 24);
            this.txtKode.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(38, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(31, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Kode";
            // 
            // txtSpecification
            // 
            this.txtSpecification.Location = new System.Drawing.Point(75, 135);
            this.txtSpecification.Name = "txtSpecification";
            this.txtSpecification.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSpecification.Properties.Appearance.Options.UseFont = true;
            this.txtSpecification.Size = new System.Drawing.Size(373, 66);
            this.txtSpecification.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(402, 520);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(296, 520);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Fi" +
                "les (*.gif)|*.gif";
            this.openFileDialog.Title = "Choose Picture";
            // 
            // lblViewAttachment
            // 
            this.lblViewAttachment.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblViewAttachment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lblViewAttachment.Location = new System.Drawing.Point(421, 466);
            this.lblViewAttachment.Name = "lblViewAttachment";
            this.lblViewAttachment.Size = new System.Drawing.Size(27, 17);
            this.lblViewAttachment.TabIndex = 51;
            this.lblViewAttachment.Text = "View";
            this.lblViewAttachment.Click += new System.EventHandler(this.lblViewAttachment_Click);
            // 
            // btnAttachment
            // 
            this.btnAttachment.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnAttachment.Appearance.Options.UseFont = true;
            this.btnAttachment.Location = new System.Drawing.Point(355, 463);
            this.btnAttachment.Name = "btnAttachment";
            this.btnAttachment.Size = new System.Drawing.Size(60, 25);
            this.btnAttachment.TabIndex = 50;
            this.btnAttachment.Text = "...";
            this.btnAttachment.Click += new System.EventHandler(this.btnAttachment_Click);
            // 
            // txtAttachment
            // 
            this.txtAttachment.Location = new System.Drawing.Point(90, 463);
            this.txtAttachment.Name = "txtAttachment";
            this.txtAttachment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAttachment.Properties.Appearance.Options.UseFont = true;
            this.txtAttachment.Size = new System.Drawing.Size(259, 24);
            this.txtAttachment.TabIndex = 49;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(19, 466);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(65, 17);
            this.labelControl18.TabIndex = 48;
            this.labelControl18.Text = "Attachment";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.Filter = "All Files|*.*";
            this.openFileDialog2.Title = "Open File";
            // 
            // frmNewBahanPenolong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 562);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewBahanPenolong";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Bahan Penolong";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKategori.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachment.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtNomor;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit cmbKategori;
        private DevExpress.XtraEditors.TextEdit txtNama;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtKode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PictureEdit picture;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private DevExpress.XtraEditors.MemoEdit txtSpecification;
        private DevExpress.XtraEditors.LabelControl lblViewAttachment;
        private DevExpress.XtraEditors.SimpleButton btnAttachment;
        private DevExpress.XtraEditors.TextEdit txtAttachment;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
    }
}