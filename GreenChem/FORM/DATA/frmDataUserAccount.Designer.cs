﻿namespace GreenChem.MenuData
{
    partial class frmDataUserAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDepartment = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmbSuDepartment = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnGroup = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbGroup = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabData = new DevExpress.XtraTab.XtraTabPage();
            this.chkSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditStatusProduct = new DevExpress.XtraEditors.CheckEdit();
            this.chkEmployee = new DevExpress.XtraEditors.CheckEdit();
            this.chkBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkCurrency = new DevExpress.XtraEditors.CheckEdit();
            this.chkLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpedition = new DevExpress.XtraEditors.CheckEdit();
            this.chkContactList = new DevExpress.XtraEditors.CheckEdit();
            this.chkDepartment = new DevExpress.XtraEditors.CheckEdit();
            this.chkUserAccount = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomer = new DevExpress.XtraEditors.CheckEdit();
            this.chkSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkCategory = new DevExpress.XtraEditors.CheckEdit();
            this.chkProduct = new DevExpress.XtraEditors.CheckEdit();
            this.tabTransaction = new DevExpress.XtraTab.XtraTabPage();
            this.chkCustomerVoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkApproval = new DevExpress.XtraEditors.CheckEdit();
            this.chkPayInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkCancelInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkCreateInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkPOtoSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkInternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPS = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPB = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerSatisfaction = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpense = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkExternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkStockAdjustment = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeliveryOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.tabReport = new DevExpress.XtraTab.XtraTabPage();
            this.chkDeliveryOrderLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrderDetail = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesInvoiceSummary = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeliveryOrderSummary = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrderSummary = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryOutReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryInReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationOutReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationInReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkPOtoSupplierReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkInternalLetterReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkExternalLetterReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPSReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPBReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerComplaintReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerSatisfactionReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpenseReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkUserLogReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkAutoNumberReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrderReport = new DevExpress.XtraEditors.CheckEdit();
            this.tabOthers = new DevExpress.XtraTab.XtraTabPage();
            this.chkUser = new DevExpress.XtraEditors.CheckEdit();
            this.chkManager = new DevExpress.XtraEditors.CheckEdit();
            this.chkAdministrator = new DevExpress.XtraEditors.CheckEdit();
            this.chkAllowEditDeposition = new DevExpress.XtraEditors.CheckEdit();
            this.chkConfiguration = new DevExpress.XtraEditors.CheckEdit();
            this.chkReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkDelete = new DevExpress.XtraEditors.CheckEdit();
            this.chkEdit = new DevExpress.XtraEditors.CheckEdit();
            this.chkNew = new DevExpress.XtraEditors.CheckEdit();
            this.chkContactVisibility = new DevExpress.XtraEditors.CheckEdit();
            this.chkAllowEditContact = new DevExpress.XtraEditors.CheckEdit();
            this.chkAllowChangeStatus = new DevExpress.XtraEditors.CheckEdit();
            this.chkAllowChangeLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkPenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSuDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditStatusProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpedition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProduct.Properties)).BeginInit();
            this.tabTransaction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerVoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApproval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCancelInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerSatisfaction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStockAdjustment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrder.Properties)).BeginInit();
            this.tabReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderDetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoiceSummary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderSummary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderSummary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOutReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryInReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOutReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationInReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetterReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetterReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPSReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPBReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaintReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerSatisfactionReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserLogReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoNumberReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderReport.Properties)).BeginInit();
            this.tabOthers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdministrator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowEditDeposition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConfiguration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDelete.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactVisibility.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowEditContact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowChangeStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowChangeLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPenyerahanProduksi.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(250, 632);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colName});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.RowHeight = 23;
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 250;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(250, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(544, 40);
            this.panelControl1.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(452, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(346, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(198, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(102, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(6, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(339, 51);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtUserName.Properties.Appearance.Options.UseFont = true;
            this.txtUserName.Size = new System.Drawing.Size(103, 24);
            this.txtUserName.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(267, 54);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 17);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "User Name";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(339, 81);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPassword.Properties.Appearance.Options.UseFont = true;
            this.txtPassword.Properties.UseSystemPasswordChar = true;
            this.txtPassword.Size = new System.Drawing.Size(250, 24);
            this.txtPassword.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(277, 84);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 17);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Password";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(264, 114);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Department";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.EditValue = "";
            this.cmbDepartment.Location = new System.Drawing.Point(339, 111);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDepartment.Properties.Appearance.Options.UseFont = true;
            this.cmbDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDepartment.Properties.DisplayMember = "Name";
            this.cmbDepartment.Properties.NullText = "";
            this.cmbDepartment.Properties.ValueMember = "id";
            this.cmbDepartment.Properties.View = this.searchLookUpEdit1View;
            this.cmbDepartment.Size = new System.Drawing.Size(250, 24);
            this.cmbDepartment.TabIndex = 9;
            this.cmbDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDeleteNull_KeyDown);
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseFont = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.searchLookUpEdit1View.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.Row.Options.UseFont = true;
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.searchLookUpEdit1View.RowHeight = 23;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "id";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Code";
            this.gridColumn5.FieldName = "Code";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 120;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Name";
            this.gridColumn6.FieldName = "Name";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 264;
            // 
            // cmbSuDepartment
            // 
            this.cmbSuDepartment.EditValue = "";
            this.cmbSuDepartment.Location = new System.Drawing.Point(339, 141);
            this.cmbSuDepartment.Name = "cmbSuDepartment";
            this.cmbSuDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSuDepartment.Properties.Appearance.Options.UseFont = true;
            this.cmbSuDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSuDepartment.Properties.DisplayMember = "Name";
            this.cmbSuDepartment.Properties.NullText = "";
            this.cmbSuDepartment.Properties.ValueMember = "id";
            this.cmbSuDepartment.Properties.View = this.gridView1;
            this.cmbSuDepartment.Size = new System.Drawing.Size(250, 24);
            this.cmbSuDepartment.TabIndex = 11;
            this.cmbSuDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDeleteNull_KeyDown);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 264;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(276, 144);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(57, 17);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Sub Dept.";
            // 
            // btnGroup
            // 
            this.btnGroup.Location = new System.Drawing.Point(594, 171);
            this.btnGroup.Name = "btnGroup";
            this.btnGroup.Size = new System.Drawing.Size(50, 24);
            this.btnGroup.TabIndex = 14;
            this.btnGroup.Text = "...";
            this.btnGroup.Click += new System.EventHandler(this.btnGroup_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(296, 174);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(37, 17);
            this.labelControl8.TabIndex = 12;
            this.labelControl8.Text = "Group";
            // 
            // cmbGroup
            // 
            this.cmbGroup.EditValue = "";
            this.cmbGroup.Location = new System.Drawing.Point(339, 171);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbGroup.Properties.Appearance.Options.UseFont = true;
            this.cmbGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGroup.Properties.DisplayMember = "Name";
            this.cmbGroup.Properties.NullText = "";
            this.cmbGroup.Properties.ValueMember = "id";
            this.cmbGroup.Properties.View = this.gridView2;
            this.cmbGroup.Size = new System.Drawing.Size(250, 24);
            this.cmbGroup.TabIndex = 13;
            this.cmbGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDeleteNull_KeyDown);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 23;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "id";
            this.gridColumn7.FieldName = "id";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Code";
            this.gridColumn8.FieldName = "Code";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 120;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Name";
            this.gridColumn9.FieldName = "Name";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 264;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(494, 51);
            this.txtName.Name = "txtName";
            this.txtName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtName.Properties.Appearance.Options.UseFont = true;
            this.txtName.Size = new System.Drawing.Size(268, 24);
            this.txtName.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(453, 54);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(35, 17);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Name";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(256, 201);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabData;
            this.xtraTabControl1.Size = new System.Drawing.Size(538, 431);
            this.xtraTabControl1.TabIndex = 15;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabData,
            this.tabTransaction,
            this.tabReport,
            this.tabOthers});
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.chkSampleCode);
            this.tabData.Controls.Add(this.chkEditStatusProduct);
            this.tabData.Controls.Add(this.chkEmployee);
            this.tabData.Controls.Add(this.chkBahanPenolong);
            this.tabData.Controls.Add(this.chkBahanBaku);
            this.tabData.Controls.Add(this.chkCurrency);
            this.tabData.Controls.Add(this.chkLocation);
            this.tabData.Controls.Add(this.chkExpedition);
            this.tabData.Controls.Add(this.chkContactList);
            this.tabData.Controls.Add(this.chkDepartment);
            this.tabData.Controls.Add(this.chkUserAccount);
            this.tabData.Controls.Add(this.chkCustomer);
            this.tabData.Controls.Add(this.chkSupplier);
            this.tabData.Controls.Add(this.chkCategory);
            this.tabData.Controls.Add(this.chkProduct);
            this.tabData.Name = "tabData";
            this.tabData.Size = new System.Drawing.Size(532, 404);
            this.tabData.Text = "Data";
            // 
            // chkSampleCode
            // 
            this.chkSampleCode.Location = new System.Drawing.Point(217, 43);
            this.chkSampleCode.Name = "chkSampleCode";
            this.chkSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkSampleCode.Properties.Caption = "Sample Code";
            this.chkSampleCode.Size = new System.Drawing.Size(150, 22);
            this.chkSampleCode.TabIndex = 14;
            // 
            // chkEditStatusProduct
            // 
            this.chkEditStatusProduct.Location = new System.Drawing.Point(217, 15);
            this.chkEditStatusProduct.Name = "chkEditStatusProduct";
            this.chkEditStatusProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkEditStatusProduct.Properties.Appearance.Options.UseFont = true;
            this.chkEditStatusProduct.Properties.Caption = "Edit Status Product";
            this.chkEditStatusProduct.Size = new System.Drawing.Size(150, 22);
            this.chkEditStatusProduct.TabIndex = 13;
            // 
            // chkEmployee
            // 
            this.chkEmployee.Location = new System.Drawing.Point(18, 351);
            this.chkEmployee.Name = "chkEmployee";
            this.chkEmployee.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkEmployee.Properties.Appearance.Options.UseFont = true;
            this.chkEmployee.Properties.Caption = "Employee";
            this.chkEmployee.Size = new System.Drawing.Size(117, 22);
            this.chkEmployee.TabIndex = 12;
            // 
            // chkBahanPenolong
            // 
            this.chkBahanPenolong.Location = new System.Drawing.Point(18, 323);
            this.chkBahanPenolong.Name = "chkBahanPenolong";
            this.chkBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkBahanPenolong.Properties.Caption = "Bahan Penolong";
            this.chkBahanPenolong.Size = new System.Drawing.Size(117, 22);
            this.chkBahanPenolong.TabIndex = 11;
            // 
            // chkBahanBaku
            // 
            this.chkBahanBaku.Location = new System.Drawing.Point(18, 295);
            this.chkBahanBaku.Name = "chkBahanBaku";
            this.chkBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkBahanBaku.Properties.Caption = "Bahan Baku";
            this.chkBahanBaku.Size = new System.Drawing.Size(117, 22);
            this.chkBahanBaku.TabIndex = 10;
            // 
            // chkCurrency
            // 
            this.chkCurrency.Location = new System.Drawing.Point(18, 239);
            this.chkCurrency.Name = "chkCurrency";
            this.chkCurrency.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCurrency.Properties.Appearance.Options.UseFont = true;
            this.chkCurrency.Properties.Caption = "Currency";
            this.chkCurrency.Size = new System.Drawing.Size(117, 22);
            this.chkCurrency.TabIndex = 8;
            // 
            // chkLocation
            // 
            this.chkLocation.Location = new System.Drawing.Point(18, 211);
            this.chkLocation.Name = "chkLocation";
            this.chkLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkLocation.Properties.Appearance.Options.UseFont = true;
            this.chkLocation.Properties.Caption = "Location";
            this.chkLocation.Size = new System.Drawing.Size(117, 22);
            this.chkLocation.TabIndex = 7;
            // 
            // chkExpedition
            // 
            this.chkExpedition.Location = new System.Drawing.Point(18, 183);
            this.chkExpedition.Name = "chkExpedition";
            this.chkExpedition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExpedition.Properties.Appearance.Options.UseFont = true;
            this.chkExpedition.Properties.Caption = "Expedition";
            this.chkExpedition.Size = new System.Drawing.Size(117, 22);
            this.chkExpedition.TabIndex = 6;
            // 
            // chkContactList
            // 
            this.chkContactList.Location = new System.Drawing.Point(18, 99);
            this.chkContactList.Name = "chkContactList";
            this.chkContactList.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkContactList.Properties.Appearance.Options.UseFont = true;
            this.chkContactList.Properties.Caption = "Contact List";
            this.chkContactList.Size = new System.Drawing.Size(117, 22);
            this.chkContactList.TabIndex = 3;
            // 
            // chkDepartment
            // 
            this.chkDepartment.Location = new System.Drawing.Point(18, 71);
            this.chkDepartment.Name = "chkDepartment";
            this.chkDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDepartment.Properties.Appearance.Options.UseFont = true;
            this.chkDepartment.Properties.Caption = "Department";
            this.chkDepartment.Size = new System.Drawing.Size(117, 22);
            this.chkDepartment.TabIndex = 2;
            // 
            // chkUserAccount
            // 
            this.chkUserAccount.Location = new System.Drawing.Point(18, 267);
            this.chkUserAccount.Name = "chkUserAccount";
            this.chkUserAccount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkUserAccount.Properties.Appearance.Options.UseFont = true;
            this.chkUserAccount.Properties.Caption = "User Account";
            this.chkUserAccount.Size = new System.Drawing.Size(117, 22);
            this.chkUserAccount.TabIndex = 9;
            // 
            // chkCustomer
            // 
            this.chkCustomer.Location = new System.Drawing.Point(18, 155);
            this.chkCustomer.Name = "chkCustomer";
            this.chkCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomer.Properties.Appearance.Options.UseFont = true;
            this.chkCustomer.Properties.Caption = "Customer";
            this.chkCustomer.Size = new System.Drawing.Size(82, 22);
            this.chkCustomer.TabIndex = 5;
            // 
            // chkSupplier
            // 
            this.chkSupplier.Location = new System.Drawing.Point(18, 127);
            this.chkSupplier.Name = "chkSupplier";
            this.chkSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkSupplier.Properties.Caption = "Supplier";
            this.chkSupplier.Size = new System.Drawing.Size(75, 22);
            this.chkSupplier.TabIndex = 4;
            // 
            // chkCategory
            // 
            this.chkCategory.Location = new System.Drawing.Point(18, 43);
            this.chkCategory.Name = "chkCategory";
            this.chkCategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCategory.Properties.Appearance.Options.UseFont = true;
            this.chkCategory.Properties.Caption = "Category";
            this.chkCategory.Size = new System.Drawing.Size(82, 22);
            this.chkCategory.TabIndex = 1;
            // 
            // chkProduct
            // 
            this.chkProduct.Location = new System.Drawing.Point(18, 15);
            this.chkProduct.Name = "chkProduct";
            this.chkProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkProduct.Properties.Appearance.Options.UseFont = true;
            this.chkProduct.Properties.Caption = "Product";
            this.chkProduct.Size = new System.Drawing.Size(75, 22);
            this.chkProduct.TabIndex = 0;
            // 
            // tabTransaction
            // 
            this.tabTransaction.Controls.Add(this.chkPenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkCustomerVoice);
            this.tabTransaction.Controls.Add(this.chkApproval);
            this.tabTransaction.Controls.Add(this.chkPayInvoice);
            this.tabTransaction.Controls.Add(this.chkPrintInvoice);
            this.tabTransaction.Controls.Add(this.chkCancelInvoice);
            this.tabTransaction.Controls.Add(this.chkCreateInvoice);
            this.tabTransaction.Controls.Add(this.chkPOtoSupplier);
            this.tabTransaction.Controls.Add(this.chkInternalLetter);
            this.tabTransaction.Controls.Add(this.chkCustomerComplaint);
            this.tabTransaction.Controls.Add(this.chkPPS);
            this.tabTransaction.Controls.Add(this.chkPPB);
            this.tabTransaction.Controls.Add(this.chkCustomerSatisfaction);
            this.tabTransaction.Controls.Add(this.chkExpense);
            this.tabTransaction.Controls.Add(this.chkQuotationOut);
            this.tabTransaction.Controls.Add(this.chkQuotationIn);
            this.tabTransaction.Controls.Add(this.chkInquiryOut);
            this.tabTransaction.Controls.Add(this.chkInquiryIn);
            this.tabTransaction.Controls.Add(this.chkExternalLetter);
            this.tabTransaction.Controls.Add(this.chkStockAdjustment);
            this.tabTransaction.Controls.Add(this.chkSalesInvoice);
            this.tabTransaction.Controls.Add(this.chkDeliveryOrder);
            this.tabTransaction.Controls.Add(this.chkSalesOrder);
            this.tabTransaction.Name = "tabTransaction";
            this.tabTransaction.Size = new System.Drawing.Size(532, 404);
            this.tabTransaction.Text = "Transaction";
            // 
            // chkCustomerVoice
            // 
            this.chkCustomerVoice.Location = new System.Drawing.Point(18, 215);
            this.chkCustomerVoice.Name = "chkCustomerVoice";
            this.chkCustomerVoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerVoice.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerVoice.Properties.Caption = "Customer Voice";
            this.chkCustomerVoice.Size = new System.Drawing.Size(167, 22);
            this.chkCustomerVoice.TabIndex = 7;
            // 
            // chkApproval
            // 
            this.chkApproval.Location = new System.Drawing.Point(195, 320);
            this.chkApproval.Name = "chkApproval";
            this.chkApproval.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkApproval.Properties.Appearance.Options.UseFont = true;
            this.chkApproval.Properties.Caption = "Approval";
            this.chkApproval.Size = new System.Drawing.Size(182, 22);
            this.chkApproval.TabIndex = 21;
            // 
            // chkPayInvoice
            // 
            this.chkPayInvoice.Location = new System.Drawing.Point(18, 337);
            this.chkPayInvoice.Name = "chkPayInvoice";
            this.chkPayInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPayInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkPayInvoice.Properties.Caption = "Pay Invoice";
            this.chkPayInvoice.Size = new System.Drawing.Size(167, 22);
            this.chkPayInvoice.TabIndex = 11;
            // 
            // chkPrintInvoice
            // 
            this.chkPrintInvoice.Location = new System.Drawing.Point(18, 309);
            this.chkPrintInvoice.Name = "chkPrintInvoice";
            this.chkPrintInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPrintInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkPrintInvoice.Properties.Caption = "Print Invoice";
            this.chkPrintInvoice.Size = new System.Drawing.Size(167, 22);
            this.chkPrintInvoice.TabIndex = 10;
            // 
            // chkCancelInvoice
            // 
            this.chkCancelInvoice.Location = new System.Drawing.Point(18, 281);
            this.chkCancelInvoice.Name = "chkCancelInvoice";
            this.chkCancelInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCancelInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkCancelInvoice.Properties.Caption = "Cancel Invoice";
            this.chkCancelInvoice.Size = new System.Drawing.Size(167, 22);
            this.chkCancelInvoice.TabIndex = 9;
            // 
            // chkCreateInvoice
            // 
            this.chkCreateInvoice.Location = new System.Drawing.Point(18, 253);
            this.chkCreateInvoice.Name = "chkCreateInvoice";
            this.chkCreateInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCreateInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkCreateInvoice.Properties.Caption = "Create Invoice";
            this.chkCreateInvoice.Size = new System.Drawing.Size(167, 22);
            this.chkCreateInvoice.TabIndex = 8;
            // 
            // chkPOtoSupplier
            // 
            this.chkPOtoSupplier.Location = new System.Drawing.Point(195, 75);
            this.chkPOtoSupplier.Name = "chkPOtoSupplier";
            this.chkPOtoSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPOtoSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkPOtoSupplier.Properties.Caption = "PO to Supplier";
            this.chkPOtoSupplier.Size = new System.Drawing.Size(129, 22);
            this.chkPOtoSupplier.TabIndex = 14;
            // 
            // chkInternalLetter
            // 
            this.chkInternalLetter.Location = new System.Drawing.Point(195, 47);
            this.chkInternalLetter.Name = "chkInternalLetter";
            this.chkInternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkInternalLetter.Properties.Caption = "Internal Letter";
            this.chkInternalLetter.Size = new System.Drawing.Size(129, 22);
            this.chkInternalLetter.TabIndex = 13;
            // 
            // chkCustomerComplaint
            // 
            this.chkCustomerComplaint.Location = new System.Drawing.Point(18, 187);
            this.chkCustomerComplaint.Name = "chkCustomerComplaint";
            this.chkCustomerComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerComplaint.Properties.Caption = "Customer Complaint";
            this.chkCustomerComplaint.Size = new System.Drawing.Size(167, 22);
            this.chkCustomerComplaint.TabIndex = 6;
            // 
            // chkPPS
            // 
            this.chkPPS.Location = new System.Drawing.Point(195, 243);
            this.chkPPS.Name = "chkPPS";
            this.chkPPS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPS.Properties.Appearance.Options.UseFont = true;
            this.chkPPS.Properties.Caption = "PPS";
            this.chkPPS.Size = new System.Drawing.Size(182, 22);
            this.chkPPS.TabIndex = 20;
            // 
            // chkPPB
            // 
            this.chkPPB.Location = new System.Drawing.Point(195, 215);
            this.chkPPB.Name = "chkPPB";
            this.chkPPB.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPB.Properties.Appearance.Options.UseFont = true;
            this.chkPPB.Properties.Caption = "PPB";
            this.chkPPB.Size = new System.Drawing.Size(182, 22);
            this.chkPPB.TabIndex = 19;
            // 
            // chkCustomerSatisfaction
            // 
            this.chkCustomerSatisfaction.Location = new System.Drawing.Point(18, 159);
            this.chkCustomerSatisfaction.Name = "chkCustomerSatisfaction";
            this.chkCustomerSatisfaction.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerSatisfaction.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerSatisfaction.Properties.Caption = "Customer Satisfaction";
            this.chkCustomerSatisfaction.Size = new System.Drawing.Size(167, 22);
            this.chkCustomerSatisfaction.TabIndex = 5;
            // 
            // chkExpense
            // 
            this.chkExpense.Location = new System.Drawing.Point(17, 131);
            this.chkExpense.Name = "chkExpense";
            this.chkExpense.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExpense.Properties.Appearance.Options.UseFont = true;
            this.chkExpense.Properties.Caption = "Expense";
            this.chkExpense.Size = new System.Drawing.Size(148, 22);
            this.chkExpense.TabIndex = 4;
            // 
            // chkQuotationOut
            // 
            this.chkQuotationOut.Location = new System.Drawing.Point(195, 187);
            this.chkQuotationOut.Name = "chkQuotationOut";
            this.chkQuotationOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQuotationOut.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationOut.Properties.Caption = "Quotation Out (Customer)";
            this.chkQuotationOut.Size = new System.Drawing.Size(182, 22);
            this.chkQuotationOut.TabIndex = 18;
            // 
            // chkQuotationIn
            // 
            this.chkQuotationIn.Location = new System.Drawing.Point(195, 159);
            this.chkQuotationIn.Name = "chkQuotationIn";
            this.chkQuotationIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQuotationIn.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationIn.Properties.Caption = "Quotation In (Supplier)";
            this.chkQuotationIn.Size = new System.Drawing.Size(158, 22);
            this.chkQuotationIn.TabIndex = 17;
            // 
            // chkInquiryOut
            // 
            this.chkInquiryOut.Location = new System.Drawing.Point(195, 131);
            this.chkInquiryOut.Name = "chkInquiryOut";
            this.chkInquiryOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInquiryOut.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryOut.Properties.Caption = "Inquiry Out (Supplier)";
            this.chkInquiryOut.Size = new System.Drawing.Size(150, 22);
            this.chkInquiryOut.TabIndex = 16;
            // 
            // chkInquiryIn
            // 
            this.chkInquiryIn.Location = new System.Drawing.Point(195, 103);
            this.chkInquiryIn.Name = "chkInquiryIn";
            this.chkInquiryIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInquiryIn.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryIn.Properties.Caption = "Inquiry In (Customer)";
            this.chkInquiryIn.Size = new System.Drawing.Size(150, 22);
            this.chkInquiryIn.TabIndex = 15;
            // 
            // chkExternalLetter
            // 
            this.chkExternalLetter.Location = new System.Drawing.Point(194, 19);
            this.chkExternalLetter.Name = "chkExternalLetter";
            this.chkExternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkExternalLetter.Properties.Caption = "External Letter";
            this.chkExternalLetter.Size = new System.Drawing.Size(129, 22);
            this.chkExternalLetter.TabIndex = 12;
            // 
            // chkStockAdjustment
            // 
            this.chkStockAdjustment.Location = new System.Drawing.Point(17, 103);
            this.chkStockAdjustment.Name = "chkStockAdjustment";
            this.chkStockAdjustment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkStockAdjustment.Properties.Appearance.Options.UseFont = true;
            this.chkStockAdjustment.Properties.Caption = "Stock Adjustment";
            this.chkStockAdjustment.Size = new System.Drawing.Size(129, 22);
            this.chkStockAdjustment.TabIndex = 3;
            // 
            // chkSalesInvoice
            // 
            this.chkSalesInvoice.Location = new System.Drawing.Point(17, 75);
            this.chkSalesInvoice.Name = "chkSalesInvoice";
            this.chkSalesInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkSalesInvoice.Properties.Caption = "Sales Invoice";
            this.chkSalesInvoice.Size = new System.Drawing.Size(117, 22);
            this.chkSalesInvoice.TabIndex = 2;
            // 
            // chkDeliveryOrder
            // 
            this.chkDeliveryOrder.Location = new System.Drawing.Point(17, 47);
            this.chkDeliveryOrder.Name = "chkDeliveryOrder";
            this.chkDeliveryOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDeliveryOrder.Properties.Appearance.Options.UseFont = true;
            this.chkDeliveryOrder.Properties.Caption = "Delivery Order";
            this.chkDeliveryOrder.Size = new System.Drawing.Size(117, 22);
            this.chkDeliveryOrder.TabIndex = 1;
            // 
            // chkSalesOrder
            // 
            this.chkSalesOrder.Location = new System.Drawing.Point(17, 19);
            this.chkSalesOrder.Name = "chkSalesOrder";
            this.chkSalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrder.Properties.Caption = "Sales Order";
            this.chkSalesOrder.Size = new System.Drawing.Size(117, 22);
            this.chkSalesOrder.TabIndex = 0;
            // 
            // tabReport
            // 
            this.tabReport.Controls.Add(this.chkDeliveryOrderLocation);
            this.tabReport.Controls.Add(this.chkSalesOrderDetail);
            this.tabReport.Controls.Add(this.chkSalesInvoiceSummary);
            this.tabReport.Controls.Add(this.chkDeliveryOrderSummary);
            this.tabReport.Controls.Add(this.chkSalesOrderSummary);
            this.tabReport.Controls.Add(this.chkInquiryOutReport);
            this.tabReport.Controls.Add(this.chkInquiryInReport);
            this.tabReport.Controls.Add(this.chkQuotationOutReport);
            this.tabReport.Controls.Add(this.chkQuotationInReport);
            this.tabReport.Controls.Add(this.chkPOtoSupplierReport);
            this.tabReport.Controls.Add(this.chkInternalLetterReport);
            this.tabReport.Controls.Add(this.chkExternalLetterReport);
            this.tabReport.Controls.Add(this.chkPPSReport);
            this.tabReport.Controls.Add(this.chkPPBReport);
            this.tabReport.Controls.Add(this.chkCustomerComplaintReport);
            this.tabReport.Controls.Add(this.chkCustomerSatisfactionReport);
            this.tabReport.Controls.Add(this.chkExpenseReport);
            this.tabReport.Controls.Add(this.chkUserLogReport);
            this.tabReport.Controls.Add(this.chkAutoNumberReport);
            this.tabReport.Controls.Add(this.chkSalesOrderReport);
            this.tabReport.Name = "tabReport";
            this.tabReport.Size = new System.Drawing.Size(532, 404);
            this.tabReport.Text = "Report";
            // 
            // chkDeliveryOrderLocation
            // 
            this.chkDeliveryOrderLocation.Location = new System.Drawing.Point(291, 131);
            this.chkDeliveryOrderLocation.Name = "chkDeliveryOrderLocation";
            this.chkDeliveryOrderLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDeliveryOrderLocation.Properties.Appearance.Options.UseFont = true;
            this.chkDeliveryOrderLocation.Properties.Caption = "Delivery Order Location";
            this.chkDeliveryOrderLocation.Size = new System.Drawing.Size(175, 22);
            this.chkDeliveryOrderLocation.TabIndex = 12;
            // 
            // chkSalesOrderDetail
            // 
            this.chkSalesOrderDetail.Location = new System.Drawing.Point(291, 103);
            this.chkSalesOrderDetail.Name = "chkSalesOrderDetail";
            this.chkSalesOrderDetail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesOrderDetail.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrderDetail.Properties.Caption = "Sales Order Detail";
            this.chkSalesOrderDetail.Size = new System.Drawing.Size(175, 22);
            this.chkSalesOrderDetail.TabIndex = 11;
            // 
            // chkSalesInvoiceSummary
            // 
            this.chkSalesInvoiceSummary.Location = new System.Drawing.Point(291, 75);
            this.chkSalesInvoiceSummary.Name = "chkSalesInvoiceSummary";
            this.chkSalesInvoiceSummary.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesInvoiceSummary.Properties.Appearance.Options.UseFont = true;
            this.chkSalesInvoiceSummary.Properties.Caption = "Sales Invoice Summary";
            this.chkSalesInvoiceSummary.Size = new System.Drawing.Size(175, 22);
            this.chkSalesInvoiceSummary.TabIndex = 10;
            // 
            // chkDeliveryOrderSummary
            // 
            this.chkDeliveryOrderSummary.Location = new System.Drawing.Point(291, 47);
            this.chkDeliveryOrderSummary.Name = "chkDeliveryOrderSummary";
            this.chkDeliveryOrderSummary.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDeliveryOrderSummary.Properties.Appearance.Options.UseFont = true;
            this.chkDeliveryOrderSummary.Properties.Caption = "Delivery Order Summary";
            this.chkDeliveryOrderSummary.Size = new System.Drawing.Size(175, 22);
            this.chkDeliveryOrderSummary.TabIndex = 9;
            // 
            // chkSalesOrderSummary
            // 
            this.chkSalesOrderSummary.Location = new System.Drawing.Point(291, 19);
            this.chkSalesOrderSummary.Name = "chkSalesOrderSummary";
            this.chkSalesOrderSummary.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesOrderSummary.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrderSummary.Properties.Caption = "Sales Order Summary";
            this.chkSalesOrderSummary.Size = new System.Drawing.Size(175, 22);
            this.chkSalesOrderSummary.TabIndex = 8;
            // 
            // chkInquiryOutReport
            // 
            this.chkInquiryOutReport.Location = new System.Drawing.Point(291, 367);
            this.chkInquiryOutReport.Name = "chkInquiryOutReport";
            this.chkInquiryOutReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInquiryOutReport.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryOutReport.Properties.Caption = "Inquiry Out Report";
            this.chkInquiryOutReport.Size = new System.Drawing.Size(175, 22);
            this.chkInquiryOutReport.TabIndex = 19;
            // 
            // chkInquiryInReport
            // 
            this.chkInquiryInReport.Location = new System.Drawing.Point(291, 339);
            this.chkInquiryInReport.Name = "chkInquiryInReport";
            this.chkInquiryInReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInquiryInReport.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryInReport.Properties.Caption = "Inquiry In Report";
            this.chkInquiryInReport.Size = new System.Drawing.Size(175, 22);
            this.chkInquiryInReport.TabIndex = 18;
            // 
            // chkQuotationOutReport
            // 
            this.chkQuotationOutReport.Location = new System.Drawing.Point(291, 311);
            this.chkQuotationOutReport.Name = "chkQuotationOutReport";
            this.chkQuotationOutReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQuotationOutReport.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationOutReport.Properties.Caption = "Quotation Out Report";
            this.chkQuotationOutReport.Size = new System.Drawing.Size(175, 22);
            this.chkQuotationOutReport.TabIndex = 17;
            // 
            // chkQuotationInReport
            // 
            this.chkQuotationInReport.Location = new System.Drawing.Point(291, 283);
            this.chkQuotationInReport.Name = "chkQuotationInReport";
            this.chkQuotationInReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQuotationInReport.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationInReport.Properties.Caption = "Quotation In Report";
            this.chkQuotationInReport.Size = new System.Drawing.Size(175, 22);
            this.chkQuotationInReport.TabIndex = 16;
            // 
            // chkPOtoSupplierReport
            // 
            this.chkPOtoSupplierReport.Location = new System.Drawing.Point(291, 255);
            this.chkPOtoSupplierReport.Name = "chkPOtoSupplierReport";
            this.chkPOtoSupplierReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPOtoSupplierReport.Properties.Appearance.Options.UseFont = true;
            this.chkPOtoSupplierReport.Properties.Caption = "PO to Supplier Report";
            this.chkPOtoSupplierReport.Size = new System.Drawing.Size(175, 22);
            this.chkPOtoSupplierReport.TabIndex = 15;
            // 
            // chkInternalLetterReport
            // 
            this.chkInternalLetterReport.Location = new System.Drawing.Point(291, 227);
            this.chkInternalLetterReport.Name = "chkInternalLetterReport";
            this.chkInternalLetterReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInternalLetterReport.Properties.Appearance.Options.UseFont = true;
            this.chkInternalLetterReport.Properties.Caption = "Internal Letter Report";
            this.chkInternalLetterReport.Size = new System.Drawing.Size(175, 22);
            this.chkInternalLetterReport.TabIndex = 14;
            // 
            // chkExternalLetterReport
            // 
            this.chkExternalLetterReport.Location = new System.Drawing.Point(291, 199);
            this.chkExternalLetterReport.Name = "chkExternalLetterReport";
            this.chkExternalLetterReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExternalLetterReport.Properties.Appearance.Options.UseFont = true;
            this.chkExternalLetterReport.Properties.Caption = "External Letter Report";
            this.chkExternalLetterReport.Size = new System.Drawing.Size(175, 22);
            this.chkExternalLetterReport.TabIndex = 13;
            // 
            // chkPPSReport
            // 
            this.chkPPSReport.Location = new System.Drawing.Point(18, 215);
            this.chkPPSReport.Name = "chkPPSReport";
            this.chkPPSReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPSReport.Properties.Appearance.Options.UseFont = true;
            this.chkPPSReport.Properties.Caption = "PPS Report";
            this.chkPPSReport.Size = new System.Drawing.Size(175, 22);
            this.chkPPSReport.TabIndex = 7;
            // 
            // chkPPBReport
            // 
            this.chkPPBReport.Location = new System.Drawing.Point(18, 187);
            this.chkPPBReport.Name = "chkPPBReport";
            this.chkPPBReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPBReport.Properties.Appearance.Options.UseFont = true;
            this.chkPPBReport.Properties.Caption = "PPB Report";
            this.chkPPBReport.Size = new System.Drawing.Size(175, 22);
            this.chkPPBReport.TabIndex = 6;
            // 
            // chkCustomerComplaintReport
            // 
            this.chkCustomerComplaintReport.Location = new System.Drawing.Point(18, 159);
            this.chkCustomerComplaintReport.Name = "chkCustomerComplaintReport";
            this.chkCustomerComplaintReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerComplaintReport.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerComplaintReport.Properties.Caption = "Cust.Complaint Report";
            this.chkCustomerComplaintReport.Size = new System.Drawing.Size(175, 22);
            this.chkCustomerComplaintReport.TabIndex = 5;
            // 
            // chkCustomerSatisfactionReport
            // 
            this.chkCustomerSatisfactionReport.Location = new System.Drawing.Point(18, 131);
            this.chkCustomerSatisfactionReport.Name = "chkCustomerSatisfactionReport";
            this.chkCustomerSatisfactionReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerSatisfactionReport.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerSatisfactionReport.Properties.Caption = "Cust. Satisfaction Report";
            this.chkCustomerSatisfactionReport.Size = new System.Drawing.Size(175, 22);
            this.chkCustomerSatisfactionReport.TabIndex = 4;
            // 
            // chkExpenseReport
            // 
            this.chkExpenseReport.Location = new System.Drawing.Point(18, 103);
            this.chkExpenseReport.Name = "chkExpenseReport";
            this.chkExpenseReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExpenseReport.Properties.Appearance.Options.UseFont = true;
            this.chkExpenseReport.Properties.Caption = "Expense Report";
            this.chkExpenseReport.Size = new System.Drawing.Size(154, 22);
            this.chkExpenseReport.TabIndex = 3;
            // 
            // chkUserLogReport
            // 
            this.chkUserLogReport.Location = new System.Drawing.Point(18, 75);
            this.chkUserLogReport.Name = "chkUserLogReport";
            this.chkUserLogReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkUserLogReport.Properties.Appearance.Options.UseFont = true;
            this.chkUserLogReport.Properties.Caption = "User Log Report";
            this.chkUserLogReport.Size = new System.Drawing.Size(154, 22);
            this.chkUserLogReport.TabIndex = 2;
            // 
            // chkAutoNumberReport
            // 
            this.chkAutoNumberReport.Location = new System.Drawing.Point(18, 47);
            this.chkAutoNumberReport.Name = "chkAutoNumberReport";
            this.chkAutoNumberReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAutoNumberReport.Properties.Appearance.Options.UseFont = true;
            this.chkAutoNumberReport.Properties.Caption = "Auto Number Report";
            this.chkAutoNumberReport.Size = new System.Drawing.Size(154, 22);
            this.chkAutoNumberReport.TabIndex = 1;
            // 
            // chkSalesOrderReport
            // 
            this.chkSalesOrderReport.Location = new System.Drawing.Point(18, 19);
            this.chkSalesOrderReport.Name = "chkSalesOrderReport";
            this.chkSalesOrderReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesOrderReport.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrderReport.Properties.Caption = "Sales Order Report";
            this.chkSalesOrderReport.Size = new System.Drawing.Size(154, 22);
            this.chkSalesOrderReport.TabIndex = 0;
            // 
            // tabOthers
            // 
            this.tabOthers.Controls.Add(this.chkUser);
            this.tabOthers.Controls.Add(this.chkManager);
            this.tabOthers.Controls.Add(this.chkAdministrator);
            this.tabOthers.Controls.Add(this.chkAllowEditDeposition);
            this.tabOthers.Controls.Add(this.chkConfiguration);
            this.tabOthers.Controls.Add(this.chkReport);
            this.tabOthers.Controls.Add(this.chkDelete);
            this.tabOthers.Controls.Add(this.chkEdit);
            this.tabOthers.Controls.Add(this.chkNew);
            this.tabOthers.Controls.Add(this.chkContactVisibility);
            this.tabOthers.Controls.Add(this.chkAllowEditContact);
            this.tabOthers.Controls.Add(this.chkAllowChangeStatus);
            this.tabOthers.Controls.Add(this.chkAllowChangeLocation);
            this.tabOthers.Name = "tabOthers";
            this.tabOthers.Size = new System.Drawing.Size(532, 404);
            this.tabOthers.Text = "Others";
            // 
            // chkUser
            // 
            this.chkUser.Location = new System.Drawing.Point(303, 78);
            this.chkUser.Name = "chkUser";
            this.chkUser.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkUser.Properties.Appearance.Options.UseFont = true;
            this.chkUser.Properties.Caption = "User";
            this.chkUser.Size = new System.Drawing.Size(165, 22);
            this.chkUser.TabIndex = 12;
            // 
            // chkManager
            // 
            this.chkManager.Location = new System.Drawing.Point(303, 50);
            this.chkManager.Name = "chkManager";
            this.chkManager.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkManager.Properties.Appearance.Options.UseFont = true;
            this.chkManager.Properties.Caption = "Manager";
            this.chkManager.Size = new System.Drawing.Size(165, 22);
            this.chkManager.TabIndex = 11;
            // 
            // chkAdministrator
            // 
            this.chkAdministrator.Location = new System.Drawing.Point(303, 22);
            this.chkAdministrator.Name = "chkAdministrator";
            this.chkAdministrator.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAdministrator.Properties.Appearance.Options.UseFont = true;
            this.chkAdministrator.Properties.Caption = "Administrator";
            this.chkAdministrator.Size = new System.Drawing.Size(165, 22);
            this.chkAdministrator.TabIndex = 10;
            // 
            // chkAllowEditDeposition
            // 
            this.chkAllowEditDeposition.Location = new System.Drawing.Point(18, 106);
            this.chkAllowEditDeposition.Name = "chkAllowEditDeposition";
            this.chkAllowEditDeposition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAllowEditDeposition.Properties.Appearance.Options.UseFont = true;
            this.chkAllowEditDeposition.Properties.Caption = "Allow Edit Deposition";
            this.chkAllowEditDeposition.Size = new System.Drawing.Size(182, 22);
            this.chkAllowEditDeposition.TabIndex = 3;
            // 
            // chkConfiguration
            // 
            this.chkConfiguration.Location = new System.Drawing.Point(17, 178);
            this.chkConfiguration.Name = "chkConfiguration";
            this.chkConfiguration.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkConfiguration.Properties.Appearance.Options.UseFont = true;
            this.chkConfiguration.Properties.Caption = "Configuration";
            this.chkConfiguration.Size = new System.Drawing.Size(106, 22);
            this.chkConfiguration.TabIndex = 5;
            // 
            // chkReport
            // 
            this.chkReport.Location = new System.Drawing.Point(19, 295);
            this.chkReport.Name = "chkReport";
            this.chkReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkReport.Properties.Appearance.Options.UseFont = true;
            this.chkReport.Properties.Caption = "Report";
            this.chkReport.Size = new System.Drawing.Size(165, 22);
            this.chkReport.TabIndex = 9;
            // 
            // chkDelete
            // 
            this.chkDelete.Location = new System.Drawing.Point(19, 267);
            this.chkDelete.Name = "chkDelete";
            this.chkDelete.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDelete.Properties.Appearance.Options.UseFont = true;
            this.chkDelete.Properties.Caption = "Delete";
            this.chkDelete.Size = new System.Drawing.Size(165, 22);
            this.chkDelete.TabIndex = 8;
            // 
            // chkEdit
            // 
            this.chkEdit.Location = new System.Drawing.Point(19, 239);
            this.chkEdit.Name = "chkEdit";
            this.chkEdit.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkEdit.Properties.Appearance.Options.UseFont = true;
            this.chkEdit.Properties.Caption = "Edit";
            this.chkEdit.Size = new System.Drawing.Size(165, 22);
            this.chkEdit.TabIndex = 7;
            // 
            // chkNew
            // 
            this.chkNew.Location = new System.Drawing.Point(19, 211);
            this.chkNew.Name = "chkNew";
            this.chkNew.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkNew.Properties.Appearance.Options.UseFont = true;
            this.chkNew.Properties.Caption = "New";
            this.chkNew.Size = new System.Drawing.Size(165, 22);
            this.chkNew.TabIndex = 6;
            // 
            // chkContactVisibility
            // 
            this.chkContactVisibility.Location = new System.Drawing.Point(17, 150);
            this.chkContactVisibility.Name = "chkContactVisibility";
            this.chkContactVisibility.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkContactVisibility.Properties.Appearance.Options.UseFont = true;
            this.chkContactVisibility.Properties.Caption = "Contact Visibility";
            this.chkContactVisibility.Size = new System.Drawing.Size(154, 22);
            this.chkContactVisibility.TabIndex = 4;
            // 
            // chkAllowEditContact
            // 
            this.chkAllowEditContact.Location = new System.Drawing.Point(18, 78);
            this.chkAllowEditContact.Name = "chkAllowEditContact";
            this.chkAllowEditContact.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAllowEditContact.Properties.Appearance.Options.UseFont = true;
            this.chkAllowEditContact.Properties.Caption = "Allow Edit Contact";
            this.chkAllowEditContact.Size = new System.Drawing.Size(165, 22);
            this.chkAllowEditContact.TabIndex = 2;
            // 
            // chkAllowChangeStatus
            // 
            this.chkAllowChangeStatus.Location = new System.Drawing.Point(18, 50);
            this.chkAllowChangeStatus.Name = "chkAllowChangeStatus";
            this.chkAllowChangeStatus.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAllowChangeStatus.Properties.Appearance.Options.UseFont = true;
            this.chkAllowChangeStatus.Properties.Caption = "Allow Change Status";
            this.chkAllowChangeStatus.Size = new System.Drawing.Size(165, 22);
            this.chkAllowChangeStatus.TabIndex = 1;
            // 
            // chkAllowChangeLocation
            // 
            this.chkAllowChangeLocation.Location = new System.Drawing.Point(18, 22);
            this.chkAllowChangeLocation.Name = "chkAllowChangeLocation";
            this.chkAllowChangeLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAllowChangeLocation.Properties.Appearance.Options.UseFont = true;
            this.chkAllowChangeLocation.Properties.Caption = "Allow Change Location";
            this.chkAllowChangeLocation.Size = new System.Drawing.Size(165, 22);
            this.chkAllowChangeLocation.TabIndex = 0;
            // 
            // chkPenyerahanProduksi
            // 
            this.chkPenyerahanProduksi.Location = new System.Drawing.Point(195, 292);
            this.chkPenyerahanProduksi.Name = "chkPenyerahanProduksi";
            this.chkPenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkPenyerahanProduksi.Properties.Caption = "Penyerahan Produksi";
            this.chkPenyerahanProduksi.Size = new System.Drawing.Size(182, 22);
            this.chkPenyerahanProduksi.TabIndex = 22;
            // 
            // frmDataUserAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 632);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.btnGroup);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.cmbGroup);
            this.Controls.Add(this.cmbSuDepartment);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.cmbDepartment);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gridControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDataUserAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Account";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSuDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditStatusProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpedition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProduct.Properties)).EndInit();
            this.tabTransaction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerVoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApproval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCancelInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerSatisfaction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStockAdjustment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrder.Properties)).EndInit();
            this.tabReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderDetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoiceSummary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderSummary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderSummary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOutReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryInReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOutReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationInReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetterReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetterReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPSReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPBReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaintReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerSatisfactionReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserLogReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoNumberReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderReport.Properties)).EndInit();
            this.tabOthers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdministrator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowEditDeposition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConfiguration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDelete.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactVisibility.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowEditContact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowChangeStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllowChangeLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPenyerahanProduksi.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtUserName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbDepartment;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbSuDepartment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnGroup;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbGroup;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabData;
        private DevExpress.XtraEditors.CheckEdit chkEmployee;
        private DevExpress.XtraEditors.CheckEdit chkBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkCurrency;
        private DevExpress.XtraEditors.CheckEdit chkLocation;
        private DevExpress.XtraEditors.CheckEdit chkExpedition;
        private DevExpress.XtraEditors.CheckEdit chkContactList;
        private DevExpress.XtraEditors.CheckEdit chkDepartment;
        private DevExpress.XtraEditors.CheckEdit chkUserAccount;
        private DevExpress.XtraEditors.CheckEdit chkCustomer;
        private DevExpress.XtraEditors.CheckEdit chkSupplier;
        private DevExpress.XtraEditors.CheckEdit chkCategory;
        private DevExpress.XtraEditors.CheckEdit chkProduct;
        private DevExpress.XtraTab.XtraTabPage tabTransaction;
        private DevExpress.XtraEditors.CheckEdit chkCustomerComplaint;
        private DevExpress.XtraEditors.CheckEdit chkPPS;
        private DevExpress.XtraEditors.CheckEdit chkPPB;
        private DevExpress.XtraEditors.CheckEdit chkCustomerSatisfaction;
        private DevExpress.XtraEditors.CheckEdit chkExpense;
        private DevExpress.XtraEditors.CheckEdit chkQuotationOut;
        private DevExpress.XtraEditors.CheckEdit chkQuotationIn;
        private DevExpress.XtraEditors.CheckEdit chkInquiryOut;
        private DevExpress.XtraEditors.CheckEdit chkInquiryIn;
        private DevExpress.XtraEditors.CheckEdit chkExternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkStockAdjustment;
        private DevExpress.XtraEditors.CheckEdit chkSalesInvoice;
        private DevExpress.XtraEditors.CheckEdit chkDeliveryOrder;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrder;
        private DevExpress.XtraTab.XtraTabPage tabReport;
        private DevExpress.XtraEditors.CheckEdit chkCustomerComplaintReport;
        private DevExpress.XtraEditors.CheckEdit chkCustomerSatisfactionReport;
        private DevExpress.XtraEditors.CheckEdit chkExpenseReport;
        private DevExpress.XtraEditors.CheckEdit chkUserLogReport;
        private DevExpress.XtraEditors.CheckEdit chkAutoNumberReport;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrderReport;
        private DevExpress.XtraTab.XtraTabPage tabOthers;
        private DevExpress.XtraEditors.CheckEdit chkConfiguration;
        private DevExpress.XtraEditors.CheckEdit chkReport;
        private DevExpress.XtraEditors.CheckEdit chkDelete;
        private DevExpress.XtraEditors.CheckEdit chkEdit;
        private DevExpress.XtraEditors.CheckEdit chkNew;
        private DevExpress.XtraEditors.CheckEdit chkContactVisibility;
        private DevExpress.XtraEditors.CheckEdit chkAllowEditContact;
        private DevExpress.XtraEditors.CheckEdit chkAllowChangeStatus;
        private DevExpress.XtraEditors.CheckEdit chkAllowChangeLocation;
        private DevExpress.XtraEditors.CheckEdit chkPPSReport;
        private DevExpress.XtraEditors.CheckEdit chkPPBReport;
        private DevExpress.XtraEditors.CheckEdit chkInternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkInternalLetterReport;
        private DevExpress.XtraEditors.CheckEdit chkExternalLetterReport;
        private DevExpress.XtraEditors.CheckEdit chkPOtoSupplierReport;
        private DevExpress.XtraEditors.CheckEdit chkQuotationOutReport;
        private DevExpress.XtraEditors.CheckEdit chkQuotationInReport;
        private DevExpress.XtraEditors.CheckEdit chkInquiryOutReport;
        private DevExpress.XtraEditors.CheckEdit chkInquiryInReport;
        private DevExpress.XtraEditors.CheckEdit chkAllowEditDeposition;
        private DevExpress.XtraEditors.CheckEdit chkPOtoSupplier;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrderSummary;
        private DevExpress.XtraEditors.CheckEdit chkSalesInvoiceSummary;
        private DevExpress.XtraEditors.CheckEdit chkDeliveryOrderSummary;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrderDetail;
        private DevExpress.XtraEditors.CheckEdit chkDeliveryOrderLocation;
        private DevExpress.XtraEditors.CheckEdit chkAdministrator;
        private DevExpress.XtraEditors.CheckEdit chkCreateInvoice;
        private DevExpress.XtraEditors.CheckEdit chkPayInvoice;
        private DevExpress.XtraEditors.CheckEdit chkPrintInvoice;
        private DevExpress.XtraEditors.CheckEdit chkCancelInvoice;
        private DevExpress.XtraEditors.CheckEdit chkEditStatusProduct;
        private DevExpress.XtraEditors.CheckEdit chkUser;
        private DevExpress.XtraEditors.CheckEdit chkManager;
        private DevExpress.XtraEditors.CheckEdit chkSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkApproval;
        private DevExpress.XtraEditors.CheckEdit chkCustomerVoice;
        private DevExpress.XtraEditors.CheckEdit chkPenyerahanProduksi;
    }
}