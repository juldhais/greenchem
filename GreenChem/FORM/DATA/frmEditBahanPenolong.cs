﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.IO;
using System.Diagnostics;

namespace GreenChem.MenuData
{
    public partial class frmEditBahanPenolong : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        string _attachmentpicture;
        GreenChemDataContext db;

        public frmEditBahanPenolong(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbKategori.Properties.DataSource = db.KategoriBahanPenolongs.GetName();
            cmbStatus.Properties.DataSource = db.StatusBahanPenolongs.GetName();

            var bahan = db.BahanPenolongs.First(x => x.id == _id);
            txtKode.Text = bahan.Kode;
            txtNomor.Text = bahan.Nomor;
            txtNama.Text = bahan.Nama;
            txtSpecification.Text = bahan.Specification;
            cmbKategori.EditValue = bahan.idKategoriBahanPenolong;
            cmbStatus.EditValue = bahan.idStatusBahanPenolong;
            txtAttachment.Text = bahan.Attachment;
            cmbActive.Text = bahan.Active ? "Active" : "Inactive";

            _attachmentpicture = bahan.AttachmentPicture;
            string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_bahanpenolong" + Path.GetExtension(_attachmentpicture);
            Sync.DownloadFromDatabase(_attachmentpicture, localfile);
            picture.LoadAsync(localfile);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                BahanPenolong bahan = db.BahanPenolongs.First(x => x.id == _id);
                bahan.Nomor = txtNomor.Text;
                bahan.Kode = txtKode.Text;
                bahan.Nama = txtNama.Text;
                if (cmbKategori.EditValue.IsNotEmpty()) bahan.idKategoriBahanPenolong = cmbKategori.EditValue.ToInteger();
                if (cmbStatus.EditValue.IsNotEmpty()) bahan.idStatusBahanPenolong = cmbStatus.EditValue.ToInteger();
                bahan.Active = cmbActive.Text == "Active";
                bahan.Specification = txtSpecification.Text;
                bahan.AttachmentPicture = _attachmentpicture;
                db.SubmitChanges();

                //upload
                try
                {
                    if (_attachmentpicture.IsNotEmpty() && !_attachmentpicture.Contains("bahanpenolong_picture"))
                    {
                        string remotefile = "bahanpenolong_picture_" + bahan.Kode + "_" + bahan.id + Path.GetExtension(_attachmentpicture);
                        bahan.AttachmentPicture = remotefile;
                        Sync.UploadToDatabase(remotefile, _attachmentpicture);
                    }

                    db.SubmitChanges();
                }
                catch { }


                //upload
                try
                {
                    if (txtAttachment.Text.IsNotEmpty() && !txtAttachment.Text.Contains("product_msds"))
                    {
                        string remotefile = "bahanpenolong_" + bahan.Nomor + "_" + bahan.id + Path.GetExtension(txtAttachment.Text);
                        bahan.Attachment = remotefile;
                        Sync.UploadToDatabase(remotefile, txtAttachment.Text);
                    }

                    db.SubmitChanges();
                }
                catch { }

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void picture_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                openFileDialog.ShowDialog();

                var fileInfo = new FileInfo(openFileDialog.FileName);
                if (fileInfo.Length >= 330000)
                {
                    XtraMessageBox.Show("Choose smaller image file. File size limit is 300KB.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _attachmentpicture = string.Empty;
                }
                else
                {
                    picture.LoadAsync(openFileDialog.FileName);
                    _attachmentpicture = openFileDialog.FileName;
                }


            }
            catch
            {
                _attachmentpicture = string.Empty;
            }
        }

        private void btnAttachment_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
            txtAttachment.Text = openFileDialog2.FileName;
        }

        private void lblViewAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAttachment.Text.IsEmpty()) return;

                Cursor.Current = Cursors.WaitCursor;
                string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_bahanpenolong" + Path.GetExtension(txtAttachment.Text);
                Sync.DownloadFromDatabase(txtAttachment.Text, localfile);

                Process process = new Process();
                process.StartInfo.FileName = localfile;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}