﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmNewSupplier : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewSupplier()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                Supplier supplier = new Supplier();
                supplier.Code = txtCode.Text;
                supplier.Name = txtName.Text;
                supplier.Address = txtAddress.Text;
                supplier.Phone = txtPhone.Text;
                supplier.Remarks = txtRemarks.Text;
                supplier.Website = txtWeb.Text;
                supplier.Person = txtPerson.Text;
                supplier.Fax = txtFax.Text;
                supplier.Active = true;
                db.Suppliers.InsertOnSubmit(supplier);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCode.Text = string.Empty;
                txtName.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtPhone.Text = string.Empty;
                txtRemarks.Text = string.Empty;
                txtWeb.Text = string.Empty;
                txtFax.Text = string.Empty;
                txtPerson.Text = string.Empty;
                txtCode.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}