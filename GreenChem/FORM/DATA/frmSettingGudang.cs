﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmSettingGudang : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmSettingGudang()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            cmbStock_SelectedIndexChanged(null, null);
        }

        private void cmbStock_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtName.Text = db.GetConfiguration(cmbStock.Text + "_NAME");
                txtAddress.Text = db.GetConfiguration(cmbStock.Text + "_ADDRESS");
                txtPhone.Text = db.GetConfiguration(cmbStock.Text + "_PHONE");
                txtContactPerson.Text = db.GetConfiguration(cmbStock.Text + "_CONTACT");
                txtRemarks.Text = db.GetConfiguration(cmbStock.Text + "_REMARKS");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                db.SetConfiguration(cmbStock.Text + "_NAME", txtName.Text);
                db.SetConfiguration(cmbStock.Text + "_ADDRESS", txtAddress.Text);
                db.SetConfiguration(cmbStock.Text + "_PHONE", txtPhone.Text);
                db.SetConfiguration(cmbStock.Text + "_CONTACT", txtContactPerson.Text);
                db.SetConfiguration(cmbStock.Text + "_REMARKS", txtRemarks.Text);

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
