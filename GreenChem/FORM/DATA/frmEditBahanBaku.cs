﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Diagnostics;
using System.IO;

namespace GreenChem.MenuData
{
    public partial class frmEditBahanBaku : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        string _attachmentfolder;
        GreenChemDataContext db;

        public frmEditBahanBaku(int id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            cmbTipe.Properties.DataSource = db.Tipes.GetName();
            cmbFisik.Properties.DataSource = db.Fisiks.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            cmbSupplier2.Properties.DataSource = db.Suppliers.GetName();
            cmbSupplier3.Properties.DataSource = db.Suppliers.GetName();

            _attachmentfolder = db.GetConfiguration("AttachmentFolder");

            var bahanbaku = db.BahanBakus.First(x => x.id == _id);
            txtKode.Text = bahanbaku.Kode;
            txtNomor.Text = bahanbaku.Nomor;
            txtNama.Text = bahanbaku.Nama;
            txtKodeMSDS.Text = bahanbaku.KodeMSDS;
            cmbFisik.EditValue = bahanbaku.idFisik;
            cmbTipe.EditValue = bahanbaku.idTipe;
            txtFungsi.Text = bahanbaku.Fungsi;
            cmbSupplier.EditValue = bahanbaku.idSupplier;
            cmbSupplier2.EditValue = bahanbaku.idSupplier2;
            cmbSupplier3.EditValue = bahanbaku.idSupplier3;
            cmbStatus.Text = bahanbaku.StatusBahanBaku;
            txtMSDS.Text = bahanbaku.AttachmentMSDS;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                BahanBaku bahanbaku = db.BahanBakus.First(x => x.id == _id);
                if (cmbFisik.EditValue.IsNotEmpty()) bahanbaku.idFisik = cmbFisik.EditValue.ToInteger();
                if (cmbTipe.EditValue.IsNotEmpty()) bahanbaku.idTipe = cmbTipe.EditValue.ToInteger();
                if (cmbSupplier.EditValue.IsNotEmpty()) bahanbaku.idSupplier = cmbSupplier.EditValue.ToInteger();
                if (cmbSupplier2.EditValue.IsNotEmpty()) bahanbaku.idSupplier2 = cmbSupplier2.EditValue.ToInteger();
                if (cmbSupplier3.EditValue.IsNotEmpty()) bahanbaku.idSupplier3 = cmbSupplier3.EditValue.ToInteger();
                bahanbaku.Nomor = txtNomor.Text;
                bahanbaku.Kode = txtKode.Text;
                bahanbaku.Nama = txtNama.Text;
                bahanbaku.KodeMSDS = txtKodeMSDS.Text;
                bahanbaku.Fungsi = txtFungsi.Text;
                bahanbaku.Active = true;
                bahanbaku.StatusBahanBaku = cmbStatus.Text;
                db.SubmitChanges();

                //upload
                try
                {
                    if (txtMSDS.Text.IsNotEmpty() && !txtMSDS.Text.Contains("bahanbaku_msds"))
                    {
                        string remotefile = "bahanbaku_msds_" + bahanbaku.Kode + "_" + bahanbaku.id + Path.GetExtension(txtMSDS.Text);
                        bahanbaku.AttachmentMSDS = remotefile;
                        Sync.UploadToDatabase(remotefile, txtMSDS.Text);
                    }

                    db.SubmitChanges();
                }
                catch { }

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblMSDS_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMSDS.Text.IsEmpty()) return;

                Cursor.Current = Cursors.WaitCursor;
                string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_bahanbaku_msds" + Path.GetExtension(txtMSDS.Text);
                Sync.DownloadFromDatabase(txtMSDS.Text, localfile);

                Process process = new Process();
                process.StartInfo.FileName = localfile;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnMSDS_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtMSDS.Text = openFileDialog.FileName;
        }
    }
}