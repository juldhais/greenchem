﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Diagnostics;
using System.IO;

namespace GreenChem.MenuData
{
    public partial class frmEditProduct : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        string _attachmentfolder;
        GreenChemDataContext db;

        public frmEditProduct(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            cmbCategory.Properties.DataSource = db.Categories.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            cmbCurrency.Properties.DataSource = db.Currencies.GetName();

            _attachmentfolder = db.GetConfiguration("AttachmentFolder");

            var product = db.Products.First(x => x.id == _id);
            txtCode.Text = product.Code;
            txtName.Text = product.Name;
            txtUoM.Text = product.UoM;
            txtSecondUoM.Text = product.SecondUoM;
            txtRatio.EditValue = product.Ratio;
            txtThirdUOM.Text = product.ThirdUOM;
            txtThirdRatio.EditValue = product.ThirdUOM;
            cmbStatus.Text = product.Status;
            cmbCurrency.EditValue = product.idCurrency;
            cmbCategory.EditValue = product.idCategory;
            cmbSupplier.EditValue = product.idSupplier;
            txtCost.EditValue = product.Cost;
            txtSalesPrice.EditValue = product.SalesPrice;
            txtSalesPrice2.EditValue = product.SalesPrice2;
            txtSalesPrice3.EditValue = product.SalesPrice3;
            txtSalesPrice4.EditValue = product.SalesPrice4;
            txtSalesPrice5.EditValue = product.SalesPrice5;
            txtRemarks.Text = product.Remarks;
            cmbStatusProduct.Text = product.StatusProduct;
            txtMSDS.Text = product.AttachemntMSDS;
            txtPI.Text = product.AttachmentPI;
            txtMinStock.EditValue = product.StokMin;
            txtMaxStock.EditValue = product.StokMax;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (db.Products.Any(x => x.Code == txtCode.Text && x.Active == true && x.id != _id))
                {
                    XtraMessageBox.Show("Product code is already used.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCode.Focus();
                    return;
                }

                Product product = db.Products.First(x => x.id == _id);
                product.Code = txtCode.Text;
                product.Name = txtName.Text;
                product.idCategory = (int?)cmbCategory.EditValue;
                product.idSupplier = (int?)cmbSupplier.EditValue;
                product.idCurrency = (int?)cmbCurrency.EditValue;
                product.UoM = txtUoM.Text;
                product.SecondUoM = txtSecondUoM.Text;
                product.Ratio = txtRatio.EditValue.ToDecimal();
                product.ThirdUOM = txtThirdUOM.Text;
                product.RatioThird = txtThirdRatio.EditValue.ToDecimal();
                product.Status = cmbStatus.Text;
                product.Cost = txtCost.EditValue.ToDecimal();
                product.SalesPrice = txtSalesPrice.EditValue.ToDecimal();
                product.SalesPrice2 = txtSalesPrice2.EditValue.ToDecimal();
                product.SalesPrice3 = txtSalesPrice3.EditValue.ToDecimal();
                product.SalesPrice4 = txtSalesPrice4.EditValue.ToDecimal();
                product.SalesPrice5 = txtSalesPrice5.EditValue.ToDecimal();
                product.Remarks = txtRemarks.Text;
                product.Active = true;
                product.StatusProduct = cmbStatusProduct.Text;
                product.StokMin = txtMinStock.EditValue.ToDecimal();
                product.StokMax = txtMaxStock.EditValue.ToDecimal();
                product.DateModified = db.GetServerDate();
                product.UserModified = frmUtama._iduseraccount;

                if (product.Stock2 == null) product.Stock2 = 0;
                if (product.Stock3 == null) product.Stock3 = 0;
                if (product.TransitStock == null) product.TransitStock = 0;

                db.SubmitChanges();

                //upload
                try
                {
                    if (txtMSDS.Text.IsNotEmpty() && !txtMSDS.Text.Contains("product_msds"))
                    {
                        string remotefile = "product_msds_" + product.Code + "_" + product.id + Path.GetExtension(txtMSDS.Text);
                        product.AttachemntMSDS = remotefile;
                        //Sync.UploadToDatabase(remotefile, txtMSDS.Text);
                        Sync.UploadToDatabase(remotefile, txtMSDS.Text);
                    }
                    if (txtPI.Text.IsNotEmpty() && !txtPI.Text.Contains("product_pi"))
                    {
                        string remotefile = "product_pi_" + product.Code + "_" + product.id + Path.GetExtension(txtPI.Text);
                        product.AttachmentPI = remotefile;
                        //Sync.UploadToDatabase(remotefile, txtPI.Text);
                        Sync.UploadToDatabase(remotefile, txtPI.Text);
                    }

                    db.SubmitChanges();
                }
                catch { }

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnMSDS_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtMSDS.Text = openFileDialog.FileName;
        }

        private void btnPI_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtPI.Text = openFileDialog.FileName;
        }

        private void lblMSDS_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMSDS.Text.IsEmpty()) return;

                Cursor.Current = Cursors.WaitCursor;
                string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_msds" + Path.GetExtension(txtMSDS.Text);
                //Sync.DownloadFromDatabase(txtMSDS.Text, localfile);
                Sync.DownloadFromDatabase(txtMSDS.Text, localfile);

                Process process = new Process();
                process.StartInfo.FileName = localfile;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void lblPI_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPI.Text.IsEmpty()) return;

                Cursor.Current = Cursors.WaitCursor;
                string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_pi" + Path.GetExtension(txtPI.Text);
                //Sync.DownloadFromDatabase(txtPI.Text, localfile);
                Sync.DownloadFromDatabase(txtPI.Text, localfile);

                Process process = new Process();
                process.StartInfo.FileName = localfile;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

    }
}