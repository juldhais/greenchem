﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Diagnostics;
using System.IO;

namespace GreenChem.MenuData
{
    public partial class frmNewProduct : DevExpress.XtraEditors.XtraForm
    {
        string _attachmentfolder = "";
        GreenChemDataContext db;

        public frmNewProduct()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            cmbCategory.Properties.DataSource = db.Categories.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            cmbCurrency.Properties.DataSource = db.Currencies.GetName();
            cmbCurrency.EditValue = 1;

            _attachmentfolder = db.GetConfiguration("AttachmentFolder");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (db.Products.Any(x => x.Code == txtCode.Text && x.Active == true))
                {
                    XtraMessageBox.Show("Product code is already used.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCode.Focus();
                    return;
                }

                Product product = new Product();
                product.Code = txtCode.Text;
                product.Name = txtName.Text;
                product.idCategory = (int?)cmbCategory.EditValue;
                product.idSupplier = (int?)cmbSupplier.EditValue;
                product.idCurrency = (int?)cmbCurrency.EditValue;
                product.UoM = txtUoM.Text;
                product.SecondUoM = txtSecondUoM.Text;
                product.Ratio = txtRatio.EditValue.ToDecimal();
                product.ThirdUOM = txtThirdUOM.Text;
                product.RatioThird = txtThirdRatio.EditValue.ToDecimal();
                product.Status = cmbStatus.Text;
                product.Cost = txtCost.EditValue.ToDecimal();
                product.SalesPrice = txtSalesPrice.EditValue.ToDecimal();
                product.SalesPrice2 = txtSalesPrice2.EditValue.ToDecimal();
                product.SalesPrice3 = txtSalesPrice3.EditValue.ToDecimal();
                product.SalesPrice4 = txtSalesPrice4.EditValue.ToDecimal();
                product.SalesPrice5 = txtSalesPrice5.EditValue.ToDecimal();
                product.Remarks = txtRemarks.Text;
                product.Active = true;
                product.StatusProduct = cmbStatusProduct.Text;
                product.StokMin = txtMinStock.EditValue.ToDecimal();
                product.StokMax = txtMaxStock.EditValue.ToDecimal();
                product.DateModified = db.GetServerDate();
                product.UserModified = frmUtama._iduseraccount;

                product.Stock2 = 0;
                product.Stock3 = 0;
                product.TransitStock = 0;

                db.Products.InsertOnSubmit(product);
                db.SubmitChanges();

                //upload
                try
                {
                    if (txtMSDS.Text.IsNotEmpty() && !txtMSDS.Text.Contains("product_msds"))
                    {
                        string remotefile = "product_msds_" + product.Code + "_" + product.id + Path.GetExtension(txtMSDS.Text);
                        product.AttachemntMSDS = remotefile;
                        //Sync.UploadToDatabase(remotefile, txtMSDS.Text);
                        Sync.UploadToDatabase(remotefile, txtMSDS.Text);
                    }
                    if (txtPI.Text.IsNotEmpty() && !txtPI.Text.Contains("product_pi"))
                    {
                        string remotefile = "product_pi_" + product.Code + "_" + product.id + Path.GetExtension(txtPI.Text);
                        product.AttachmentPI = remotefile;
                        //Sync.UploadToDatabase(remotefile, txtPI.Text);
                        Sync.UploadToDatabase(remotefile, txtPI.Text);
                    }

                    db.SubmitChanges();
                }
                catch { }


                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCode.Text = string.Empty;
                txtName.Text = string.Empty;
                cmbCategory.EditValue = null;
                cmbSupplier.EditValue = null;
                txtUoM.Text = string.Empty;
                txtSecondUoM.Text = string.Empty;
                txtRatio.EditValue = 0;
                txtCost.EditValue = 0;
                txtSalesPrice.EditValue = 0;
                txtSalesPrice2.EditValue = 0;
                txtSalesPrice3.EditValue = 0;
                txtSalesPrice4.EditValue = 0;
                txtSalesPrice5.EditValue = 0;
                txtRemarks.Text = string.Empty;
                txtMSDS.Text = string.Empty;
                txtPI.Text = string.Empty;
                txtCode.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnMSDS_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtMSDS.Text = openFileDialog.FileName;
        }

        private void btnPI_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtPI.Text = openFileDialog.FileName;
        }

        private void lblMSDS_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Process process = new Process();
                process.StartInfo.FileName = txtMSDS.Text;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void lblPI_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Process process = new Process();
                process.StartInfo.FileName = txtPI.Text;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}