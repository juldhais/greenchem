﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmEditSupplier : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditSupplier(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            var supplier = db.Suppliers.First(x => x.id == _id);
            txtCode.Text = supplier.Code;
            txtName.Text = supplier.Name;
            txtAddress.Text = supplier.Address;
            txtPhone.Text = supplier.Phone;
            txtRemarks.Text = supplier.Remarks;
            txtWeb.Text = supplier.Website;
            txtFax.Text = supplier.Fax;
            txtPerson.Text = supplier.Person;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                Supplier supplier = db.Suppliers.First(x => x.id == _id);
                supplier.Code = txtCode.Text;
                supplier.Name = txtName.Text;
                supplier.Address = txtAddress.Text;
                supplier.Phone = txtPhone.Text;
                supplier.Remarks = txtRemarks.Text;
                supplier.Website = txtWeb.Text;
                supplier.Fax = txtFax.Text;
                supplier.Person = txtPerson.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}