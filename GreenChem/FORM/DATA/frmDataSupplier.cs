﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmDataSupplier : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataSupplier()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Supplier");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Supplier");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Supplier");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Supplier");

            if (System.IO.File.Exists("layout_supplier.xml"))
                gridView.RestoreLayoutFromXml("layout_supplier.xml");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.Suppliers
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.Code,
                                x.Name,
                                x.Address,
                                x.Phone,
                                x.Remarks,
                                x.Website,
                                x.Fax,
                                x.Person,
                                x.Active
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Code == txtSearch.Text ||
                                        x.Name.Contains(txtSearch.Text) ||
                                        x.Person.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewSupplier form = new frmNewSupplier();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEditSupplier form = new frmEditSupplier(gridView.GetFocusedRowCellValue("id").ToInteger());
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this supplier : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    Supplier supplier = db.Suppliers.First(x => x.id == id);
                    supplier.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Supplier deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            gridControl.ShowPrintPreview();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_supplier.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}