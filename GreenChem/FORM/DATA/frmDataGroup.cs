﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmDataGroup : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataGroup()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            groupBindingSource.DataSource = db.Groups;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetRowCellValue(e.RowHandle, "Active", true);
        }
    }
}