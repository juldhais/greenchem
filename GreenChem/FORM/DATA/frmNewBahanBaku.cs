﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.IO;
using System.Diagnostics;

namespace GreenChem.MenuData
{
    public partial class frmNewBahanBaku : DevExpress.XtraEditors.XtraForm
    {
        string _attachmentfolder = "";
        GreenChemDataContext db;

        public frmNewBahanBaku()
        {
            InitializeComponent();
            db = new GreenChemDataContext();

            cmbTipe.Properties.DataSource = db.Tipes.GetName();
            cmbFisik.Properties.DataSource = db.Fisiks.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            cmbSupplier2.Properties.DataSource = db.Suppliers.GetName();
            cmbSupplier3.Properties.DataSource = db.Suppliers.GetName();

            _attachmentfolder = db.GetConfiguration("AttachmentFolder");

            //nomor
            try
            {
                var query = (from x in db.BahanBakus
                             orderby x.Nomor descending
                             select x.Nomor).First();

                int number = query.ToInteger() + 1;
                txtNomor.Text = number.ToString("00000");
            }
            catch
            {
                txtNomor.Text = "00001";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                BahanBaku bahanbaku = new BahanBaku();
                if (cmbFisik.EditValue.IsNotEmpty()) bahanbaku.idFisik = cmbFisik.EditValue.ToInteger();
                if (cmbTipe.EditValue.IsNotEmpty()) bahanbaku.idTipe = cmbTipe.EditValue.ToInteger();
                if (cmbSupplier.EditValue.IsNotEmpty()) bahanbaku.idSupplier = cmbSupplier.EditValue.ToInteger();
                if (cmbSupplier2.EditValue.IsNotEmpty()) bahanbaku.idSupplier2 = cmbSupplier2.EditValue.ToInteger();
                if (cmbSupplier3.EditValue.IsNotEmpty()) bahanbaku.idSupplier3 = cmbSupplier3.EditValue.ToInteger();
                bahanbaku.Nomor = txtNomor.Text;
                bahanbaku.Kode = txtKode.Text;
                bahanbaku.Nama = txtNama.Text;
                bahanbaku.KodeMSDS = txtKodeMSDS.Text;
                bahanbaku.Fungsi = txtFungsi.Text;
                bahanbaku.Active = true;
                bahanbaku.StatusBahanBaku = cmbStatus.Text;
                db.BahanBakus.InsertOnSubmit(bahanbaku);
                db.SubmitChanges();

                //upload
                try
                {
                    if (txtMSDS.Text.IsNotEmpty() && !txtMSDS.Text.Contains("bahanbaku_msds"))
                    {
                        string remotefile = "bahanbaku_msds" + bahanbaku.Kode + "_" + bahanbaku.id + Path.GetExtension(txtMSDS.Text);
                        bahanbaku.AttachmentMSDS = remotefile;
                        Sync.UploadToDatabase(remotefile, txtMSDS.Text);
                    }

                    db.SubmitChanges();
                }
                catch { }

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);


                //clear
                try
                {
                    var query = (from x in db.BahanBakus
                                 orderby x.Nomor descending
                                 select x.Nomor).First();

                    int number = query.ToInteger() + 1;
                    txtNomor.Text = number.ToString("00000");
                }
                catch
                {
                    txtNomor.Text = "00001";
                }
                txtKode.Text = string.Empty;
                txtNama.Text = string.Empty;
                txtFungsi.Text = string.Empty;
                txtKode.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblMSDS_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMSDS.Text.IsEmpty()) return;

                Cursor.Current = Cursors.WaitCursor;
                Process process = new Process();
                process.StartInfo.FileName = txtMSDS.Text;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnMSDS_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtMSDS.Text = openFileDialog.FileName;
        }

    }
}