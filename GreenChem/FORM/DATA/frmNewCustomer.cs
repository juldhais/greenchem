﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmNewCustomer : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewCustomer()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                Customer customer = new Customer();
                customer.Code = txtCode.Text;
                customer.Name = txtName.Text;
                customer.idDepartment = (int?)cmbDepartment.EditValue;
                customer.Address = txtAddress.Text;
                customer.ShipAddress = txtShipAddress.Text;
                customer.Phone = txtPhone.Text;
                customer.Remarks = txtRemarks.Text;
                customer.ContactPerson = txtContactPerson.Text;
                customer.Active = true;
                db.Customers.InsertOnSubmit(customer);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCode.Text = string.Empty;
                txtName.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtPhone.Text = string.Empty;
                txtRemarks.Text = string.Empty;
                txtCode.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}