﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmChangePassword : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmChangePassword()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            var user = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount);
            txtUserName.Text = user.Name;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtOldPassword.Text.IsEmpty())
            {
                XtraMessageBox.Show("Old Password still empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtOldPassword.Focus();
                return;
            }
            if (txtNewPassword.Text.IsEmpty())
            {
                XtraMessageBox.Show("Old Password still empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNewPassword.Focus();
                return;
            }
            if (txtNewPassword.Text != txtConfirm.Text)
            {
                XtraMessageBox.Show("New password and confirmation didn't match.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNewPassword.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                UserAccount user = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount);
                if (txtOldPassword.Text != user.Password)
                {
                    XtraMessageBox.Show("Old password is wrong.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtOldPassword.Focus();
                    return;
                }

                user.Password = txtNewPassword.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Password saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}