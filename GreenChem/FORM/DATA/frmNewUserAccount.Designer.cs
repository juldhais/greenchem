﻿namespace GreenChem.MenuData
{
    partial class frmNewUserAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnGroup = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbGroup = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmbSuDepartment = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDepartment = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabData = new DevExpress.XtraTab.XtraTabPage();
            this.chkPriceList = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintSupplierSC = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteSupplierSC = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditSupplierSC = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewSupplierSC = new DevExpress.XtraEditors.CheckEdit();
            this.chkSupplierSC = new DevExpress.XtraEditors.CheckEdit();
            this.chkUpdatePL = new DevExpress.XtraEditors.CheckEdit();
            this.chkInactiveBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkInactiveBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkInactiveProduct = new DevExpress.XtraEditors.CheckEdit();
            this.chkPackaging = new DevExpress.XtraEditors.CheckEdit();
            this.chkTerms = new DevExpress.XtraEditors.CheckEdit();
            this.chkTipeSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkFisikSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkStatusBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkKategoriBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkTipeBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkFisikBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintContactList = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintCustomer = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkRecapSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkRecapBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkRecapBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintProduct = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteContactList = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditContactList = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewContactList = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteUserAccount = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditUserAccount = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewUserAccount = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteExpedition = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditExpedition = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewExpedition = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteEmployee = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditEmployee = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewEmployee = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteCustomer = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditCustomer = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewCustomer = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteDepartment = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditDepartment = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewDepartment = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteProduct = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditProduct = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewProduct = new DevExpress.XtraEditors.CheckEdit();
            this.chkSampleCode = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditStatusProduct = new DevExpress.XtraEditors.CheckEdit();
            this.chkEmployee = new DevExpress.XtraEditors.CheckEdit();
            this.chkBahanPenolong = new DevExpress.XtraEditors.CheckEdit();
            this.chkBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkCurrency = new DevExpress.XtraEditors.CheckEdit();
            this.chkLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpedition = new DevExpress.XtraEditors.CheckEdit();
            this.chkContactList = new DevExpress.XtraEditors.CheckEdit();
            this.chkDepartment = new DevExpress.XtraEditors.CheckEdit();
            this.chkUserAccount = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomer = new DevExpress.XtraEditors.CheckEdit();
            this.chkSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkCategory = new DevExpress.XtraEditors.CheckEdit();
            this.chkProduct = new DevExpress.XtraEditors.CheckEdit();
            this.tabTransaction = new DevExpress.XtraTab.XtraTabPage();
            this.chkUpdateRemarksTransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintTransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkStatusQCBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkKesimpulanQCBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkStatusQCBarangJadi = new DevExpress.XtraEditors.CheckEdit();
            this.chkKesimpulanQCBarangJadi = new DevExpress.XtraEditors.CheckEdit();
            this.chkChangeTSLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkSetDONumber = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintLabService = new DevExpress.XtraEditors.CheckEdit();
            this.chkApprove2TransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkApproveTransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkApprove2PenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkApprove2SalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintQCKemasan = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteQCKemasan = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditQCKemasan = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewQCKemasan = new DevExpress.XtraEditors.CheckEdit();
            this.chkQCKemasan = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteLabService = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditLabService = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewLabService = new DevExpress.XtraEditors.CheckEdit();
            this.chkLabService = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteStandardDeliveryTime = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditStandardDeliveryTime = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewStandardDeliveryTime = new DevExpress.XtraEditors.CheckEdit();
            this.chkStandardDeliveryTime = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintQCPacking = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteQCPacking = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditQCPacking = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewQCPacking = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintQCBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteQCBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditQCBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewQCBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintQCBarangJadi = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteQCBarangJadi = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditQCBarangJadi = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewQCBarangJadi = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteTransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditTransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewTransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkApprovePenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintPenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkSetDeposition = new DevExpress.XtraEditors.CheckEdit();
            this.chkSolveCustomerVoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteCustomerVoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditCustomerVoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewCustomerVoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteExpense = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditExpense = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewExpense = new DevExpress.XtraEditors.CheckEdit();
            this.chkSetLocationDeliveryOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintDeliveryOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteDeliveryOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditDeliveryOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewDeliveryOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkApproveSalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintSalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteSalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditSalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewSalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkQCPacking = new DevExpress.XtraEditors.CheckEdit();
            this.chkQCBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkQCBarangJadi = new DevExpress.XtraEditors.CheckEdit();
            this.chkWorkInProgress = new DevExpress.XtraEditors.CheckEdit();
            this.chkTransferStock = new DevExpress.XtraEditors.CheckEdit();
            this.chkChangeDOLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkPenyerahanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerVoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkPayInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkCancelInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkCreateInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpense = new DevExpress.XtraEditors.CheckEdit();
            this.chkStockAdjustment = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeliveryOrder = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrder = new DevExpress.XtraEditors.CheckEdit();
            this.tabTransaction2 = new DevExpress.XtraTab.XtraTabPage();
            this.chkViewAllProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkActivePIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkPositionPIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkNationalityPIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkClosePengembanganProduk = new DevExpress.XtraEditors.CheckEdit();
            this.chkProjectCategory = new DevExpress.XtraEditors.CheckEdit();
            this.chkSolveCustomerComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteCustomerComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditCustomerComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewCustomerComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintPengembanganProduk = new DevExpress.XtraEditors.CheckEdit();
            this.chkViewPengembanganProduk = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePengembanganProduk = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPengembanganProduk = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPengembanganProduk = new DevExpress.XtraEditors.CheckEdit();
            this.chkPengembanganProduk = new DevExpress.XtraEditors.CheckEdit();
            this.chkApprovePermintaanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkViewPermintaanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePermintaanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPermintaanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPermintaanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkPermintaanProduksi = new DevExpress.XtraEditors.CheckEdit();
            this.chkLeaveType = new DevExpress.XtraEditors.CheckEdit();
            this.chkViewLeave = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteLeave = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditLeave = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewLeave = new DevExpress.XtraEditors.CheckEdit();
            this.chkLeave = new DevExpress.XtraEditors.CheckEdit();
            this.chkPayGrade = new DevExpress.XtraEditors.CheckEdit();
            this.chkViewPIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkPIM = new DevExpress.XtraEditors.CheckEdit();
            this.chkCloseProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkCreateProjectProgress = new DevExpress.XtraEditors.CheckEdit();
            this.chkViewProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkProject = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewInternalComplaintMenu = new DevExpress.XtraEditors.CheckEdit();
            this.chkSetDepositionInternalComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkSolveInternalComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteInternalComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditInternalComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewInternalComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.chkInternalComplaint = new DevExpress.XtraEditors.CheckEdit();
            this.tabAutoNumber = new DevExpress.XtraTab.XtraTabPage();
            this.chkSetPONumberPPB = new DevExpress.XtraEditors.CheckEdit();
            this.chkApprovePOtoSupplierExpedisi = new DevExpress.XtraEditors.CheckEdit();
            this.chkApprovePOtoSupplierRegular = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintPOtoSupplierExpedisi = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePOtoSupplierExpedisi = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPOtoSupplierExpedisi = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPOtoSupplierExpedisi = new DevExpress.XtraEditors.CheckEdit();
            this.chkPOtoSupplierExpedisi = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintPOtoSupplierRegular = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePOtoSupplierRegular = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPOtoSupplierRegular = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPOtoSupplierRegular = new DevExpress.XtraEditors.CheckEdit();
            this.chkPOtoSupplierRegular = new DevExpress.XtraEditors.CheckEdit();
            this.chkChangeDeliveryStatus = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePPS = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPPS = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPPS = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePPB = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPPB = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPPB = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteQuotationOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditQuotationOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewQuotationOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteQuotationIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditQuotationIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewQuotationIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteInquiryOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditInquiryOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewInquiryOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteInquiryIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditInquiryIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewInquiryIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeletePOtoSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditPOtoSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewPOtoSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteInternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditInternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewInternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeleteExternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditExternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkNewExternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkPOtoSupplier = new DevExpress.XtraEditors.CheckEdit();
            this.chkInternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPS = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPB = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryOut = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryIn = new DevExpress.XtraEditors.CheckEdit();
            this.chkExternalLetter = new DevExpress.XtraEditors.CheckEdit();
            this.tabReport = new DevExpress.XtraTab.XtraTabPage();
            this.chkPIMReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerVoiceReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkStockReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkPenyerahanProduksiReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeliveryOrderLocation = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrderDetail = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesInvoiceSummary = new DevExpress.XtraEditors.CheckEdit();
            this.chkDeliveryOrderSummary = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrderSummary = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryOutReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkInquiryInReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationOutReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkQuotationInReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkPOtoSupplierReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkInternalLetterReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkExternalLetterReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPSReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkPPBReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerComplaintReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkCustomerSatisfactionReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkExpenseReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkUserLogReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkAutoNumberReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkSalesOrderReport = new DevExpress.XtraEditors.CheckEdit();
            this.tabOthers = new DevExpress.XtraTab.XtraTabPage();
            this.chkDesignReport = new DevExpress.XtraEditors.CheckEdit();
            this.chkUser = new DevExpress.XtraEditors.CheckEdit();
            this.chkManager = new DevExpress.XtraEditors.CheckEdit();
            this.chkAdministrator = new DevExpress.XtraEditors.CheckEdit();
            this.chkConfiguration = new DevExpress.XtraEditors.CheckEdit();
            this.chkEditSupplierBahanBaku = new DevExpress.XtraEditors.CheckEdit();
            this.chkSettingGudang = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSuDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.tabData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPriceList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSupplierSC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSupplierSC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSupplierSC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSupplierSC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplierSC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUpdatePL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactiveBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactiveBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactiveProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPackaging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTerms.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTipeSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFisikSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKategoriBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTipeBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFisikBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintContactList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecapSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecapBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecapBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteContactList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditContactList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewContactList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteUserAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditUserAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewUserAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteExpedition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditExpedition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewExpedition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSampleCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditStatusProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanPenolong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpedition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProduct.Properties)).BeginInit();
            this.tabTransaction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUpdateRemarksTransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintTransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusQCBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKesimpulanQCBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusQCBarangJadi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKesimpulanQCBarangJadi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChangeTSLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetDONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintLabService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprove2TransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApproveTransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprove2PenyerahanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprove2SalesOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCKemasan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteLabService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditLabService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewLabService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLabService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteStandardDeliveryTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditStandardDeliveryTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewStandardDeliveryTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStandardDeliveryTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCBarangJadi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCBarangJadi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCBarangJadi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCBarangJadi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteTransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditTransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewTransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePenyerahanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPenyerahanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePenyerahanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPenyerahanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPenyerahanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetDeposition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSolveCustomerVoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteCustomerVoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditCustomerVoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewCustomerVoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteExpense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditExpense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewExpense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetLocationDeliveryOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintDeliveryOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteDeliveryOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditDeliveryOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewDeliveryOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApproveSalesOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSalesOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSalesOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSalesOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSalesOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCBarangJadi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWorkInProgress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChangeDOLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPenyerahanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerVoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCancelInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStockAdjustment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrder.Properties)).BeginInit();
            this.tabTransaction2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewAllProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActivePIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPositionPIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNationalityPIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkClosePengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProjectCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSolveCustomerComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteCustomerComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditCustomerComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewCustomerComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewPengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePermintaanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewPermintaanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePermintaanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPermintaanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPermintaanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermintaanProduksi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLeaveType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewLeave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteLeave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditLeave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewLeave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLeave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayGrade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewPIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPIM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCloseProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateProjectProgress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInternalComplaintMenu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetDepositionInternalComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSolveInternalComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInternalComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInternalComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInternalComplaint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalComplaint.Properties)).BeginInit();
            this.tabAutoNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetPONumberPPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePOtoSupplierExpedisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePOtoSupplierRegular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPOtoSupplierExpedisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePOtoSupplierExpedisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPOtoSupplierExpedisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPOtoSupplierExpedisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierExpedisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPOtoSupplierRegular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePOtoSupplierRegular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPOtoSupplierRegular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPOtoSupplierRegular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierRegular.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChangeDeliveryStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQuotationOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQuotationOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQuotationOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQuotationIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQuotationIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQuotationIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInquiryOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInquiryOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInquiryOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInquiryIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInquiryIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInquiryIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePOtoSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPOtoSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPOtoSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteExternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditExternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewExternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetter.Properties)).BeginInit();
            this.tabReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPIMReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerVoiceReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStockReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPenyerahanProduksiReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderDetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoiceSummary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderSummary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderSummary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOutReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryInReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOutReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationInReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetterReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetterReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPSReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPBReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaintReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerSatisfactionReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserLogReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoNumberReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderReport.Properties)).BeginInit();
            this.tabOthers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDesignReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdministrator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConfiguration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSupplierBahanBaku.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSettingGudang.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(250, 652);
            this.gridControl.TabIndex = 0;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colName});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.RowHeight = 23;
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 250;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(250, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(734, 40);
            this.panelControl1.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(649, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(543, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(198, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(102, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(6, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(342, 91);
            this.txtName.Name = "txtName";
            this.txtName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtName.Properties.Appearance.Options.UseFont = true;
            this.txtName.Size = new System.Drawing.Size(250, 24);
            this.txtName.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(301, 94);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(35, 17);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Name";
            // 
            // btnGroup
            // 
            this.btnGroup.Location = new System.Drawing.Point(924, 117);
            this.btnGroup.Name = "btnGroup";
            this.btnGroup.Size = new System.Drawing.Size(50, 24);
            this.btnGroup.TabIndex = 14;
            this.btnGroup.Text = "...";
            this.btnGroup.Click += new System.EventHandler(this.btnGroup_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(626, 120);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(37, 17);
            this.labelControl8.TabIndex = 12;
            this.labelControl8.Text = "Group";
            // 
            // cmbGroup
            // 
            this.cmbGroup.EditValue = "";
            this.cmbGroup.Location = new System.Drawing.Point(669, 117);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbGroup.Properties.Appearance.Options.UseFont = true;
            this.cmbGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGroup.Properties.DisplayMember = "Name";
            this.cmbGroup.Properties.NullText = "";
            this.cmbGroup.Properties.ValueMember = "id";
            this.cmbGroup.Properties.View = this.gridView2;
            this.cmbGroup.Size = new System.Drawing.Size(250, 24);
            this.cmbGroup.TabIndex = 13;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 23;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "id";
            this.gridColumn7.FieldName = "id";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Code";
            this.gridColumn8.FieldName = "Code";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 120;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Name";
            this.gridColumn9.FieldName = "Name";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 264;
            // 
            // cmbSuDepartment
            // 
            this.cmbSuDepartment.EditValue = "";
            this.cmbSuDepartment.Location = new System.Drawing.Point(669, 87);
            this.cmbSuDepartment.Name = "cmbSuDepartment";
            this.cmbSuDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSuDepartment.Properties.Appearance.Options.UseFont = true;
            this.cmbSuDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSuDepartment.Properties.DisplayMember = "Name";
            this.cmbSuDepartment.Properties.NullText = "";
            this.cmbSuDepartment.Properties.ValueMember = "id";
            this.cmbSuDepartment.Properties.View = this.gridView1;
            this.cmbSuDepartment.Size = new System.Drawing.Size(250, 24);
            this.cmbSuDepartment.TabIndex = 11;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 264;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(606, 90);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(57, 17);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Sub Dept.";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.EditValue = "";
            this.cmbDepartment.Location = new System.Drawing.Point(669, 57);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDepartment.Properties.Appearance.Options.UseFont = true;
            this.cmbDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDepartment.Properties.DisplayMember = "Name";
            this.cmbDepartment.Properties.NullText = "";
            this.cmbDepartment.Properties.ValueMember = "id";
            this.cmbDepartment.Properties.View = this.searchLookUpEdit1View;
            this.cmbDepartment.Size = new System.Drawing.Size(250, 24);
            this.cmbDepartment.TabIndex = 9;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseFont = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.searchLookUpEdit1View.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.Row.Options.UseFont = true;
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.searchLookUpEdit1View.RowHeight = 23;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "id";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Code";
            this.gridColumn5.FieldName = "Code";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 120;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Name";
            this.gridColumn6.FieldName = "Name";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 264;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(632, 60);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Dept.";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(342, 121);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPassword.Properties.Appearance.Options.UseFont = true;
            this.txtPassword.Properties.UseSystemPasswordChar = true;
            this.txtPassword.Size = new System.Drawing.Size(250, 24);
            this.txtPassword.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(280, 124);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 17);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(342, 61);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtUserName.Properties.Appearance.Options.UseFont = true;
            this.txtUserName.Size = new System.Drawing.Size(250, 24);
            this.txtUserName.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(270, 64);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 17);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "User Name";
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 9F);
            this.xtraTabControl.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl.Location = new System.Drawing.Point(256, 151);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.tabData;
            this.xtraTabControl.Size = new System.Drawing.Size(723, 501);
            this.xtraTabControl.TabIndex = 0;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabData,
            this.tabTransaction,
            this.tabTransaction2,
            this.tabAutoNumber,
            this.tabReport,
            this.tabOthers});
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.chkEditSupplierBahanBaku);
            this.tabData.Controls.Add(this.chkPriceList);
            this.tabData.Controls.Add(this.chkPrintSupplierSC);
            this.tabData.Controls.Add(this.chkDeleteSupplierSC);
            this.tabData.Controls.Add(this.chkEditSupplierSC);
            this.tabData.Controls.Add(this.chkNewSupplierSC);
            this.tabData.Controls.Add(this.chkSupplierSC);
            this.tabData.Controls.Add(this.chkUpdatePL);
            this.tabData.Controls.Add(this.chkInactiveBahanPenolong);
            this.tabData.Controls.Add(this.chkInactiveBahanBaku);
            this.tabData.Controls.Add(this.chkInactiveProduct);
            this.tabData.Controls.Add(this.chkPackaging);
            this.tabData.Controls.Add(this.chkTerms);
            this.tabData.Controls.Add(this.chkTipeSampleCode);
            this.tabData.Controls.Add(this.chkFisikSampleCode);
            this.tabData.Controls.Add(this.chkStatusBahanPenolong);
            this.tabData.Controls.Add(this.chkKategoriBahanPenolong);
            this.tabData.Controls.Add(this.chkTipeBahanBaku);
            this.tabData.Controls.Add(this.chkFisikBahanBaku);
            this.tabData.Controls.Add(this.chkPrintContactList);
            this.tabData.Controls.Add(this.chkPrintCustomer);
            this.tabData.Controls.Add(this.chkPrintSupplier);
            this.tabData.Controls.Add(this.chkRecapSampleCode);
            this.tabData.Controls.Add(this.chkRecapBahanPenolong);
            this.tabData.Controls.Add(this.chkRecapBahanBaku);
            this.tabData.Controls.Add(this.chkPrintProduct);
            this.tabData.Controls.Add(this.chkDeleteContactList);
            this.tabData.Controls.Add(this.chkEditContactList);
            this.tabData.Controls.Add(this.chkNewContactList);
            this.tabData.Controls.Add(this.chkDeleteUserAccount);
            this.tabData.Controls.Add(this.chkEditUserAccount);
            this.tabData.Controls.Add(this.chkNewUserAccount);
            this.tabData.Controls.Add(this.chkDeleteLocation);
            this.tabData.Controls.Add(this.chkEditLocation);
            this.tabData.Controls.Add(this.chkNewLocation);
            this.tabData.Controls.Add(this.chkDeleteExpedition);
            this.tabData.Controls.Add(this.chkEditExpedition);
            this.tabData.Controls.Add(this.chkNewExpedition);
            this.tabData.Controls.Add(this.chkDeleteEmployee);
            this.tabData.Controls.Add(this.chkEditEmployee);
            this.tabData.Controls.Add(this.chkNewEmployee);
            this.tabData.Controls.Add(this.chkDeleteCustomer);
            this.tabData.Controls.Add(this.chkEditCustomer);
            this.tabData.Controls.Add(this.chkNewCustomer);
            this.tabData.Controls.Add(this.chkDeleteSupplier);
            this.tabData.Controls.Add(this.chkEditSupplier);
            this.tabData.Controls.Add(this.chkNewSupplier);
            this.tabData.Controls.Add(this.chkDeleteDepartment);
            this.tabData.Controls.Add(this.chkEditDepartment);
            this.tabData.Controls.Add(this.chkNewDepartment);
            this.tabData.Controls.Add(this.chkDeleteSampleCode);
            this.tabData.Controls.Add(this.chkEditSampleCode);
            this.tabData.Controls.Add(this.chkNewSampleCode);
            this.tabData.Controls.Add(this.chkDeleteBahanPenolong);
            this.tabData.Controls.Add(this.chkEditBahanPenolong);
            this.tabData.Controls.Add(this.chkNewBahanPenolong);
            this.tabData.Controls.Add(this.chkDeleteBahanBaku);
            this.tabData.Controls.Add(this.chkEditBahanBaku);
            this.tabData.Controls.Add(this.chkNewBahanBaku);
            this.tabData.Controls.Add(this.chkDeleteProduct);
            this.tabData.Controls.Add(this.chkEditProduct);
            this.tabData.Controls.Add(this.chkNewProduct);
            this.tabData.Controls.Add(this.chkSampleCode);
            this.tabData.Controls.Add(this.chkEditStatusProduct);
            this.tabData.Controls.Add(this.chkEmployee);
            this.tabData.Controls.Add(this.chkBahanPenolong);
            this.tabData.Controls.Add(this.chkBahanBaku);
            this.tabData.Controls.Add(this.chkCurrency);
            this.tabData.Controls.Add(this.chkLocation);
            this.tabData.Controls.Add(this.chkExpedition);
            this.tabData.Controls.Add(this.chkContactList);
            this.tabData.Controls.Add(this.chkDepartment);
            this.tabData.Controls.Add(this.chkUserAccount);
            this.tabData.Controls.Add(this.chkCustomer);
            this.tabData.Controls.Add(this.chkSupplier);
            this.tabData.Controls.Add(this.chkCategory);
            this.tabData.Controls.Add(this.chkProduct);
            this.tabData.Name = "tabData";
            this.tabData.Size = new System.Drawing.Size(717, 474);
            this.tabData.Text = "Data";
            // 
            // chkPriceList
            // 
            this.chkPriceList.Location = new System.Drawing.Point(159, 43);
            this.chkPriceList.Name = "chkPriceList";
            this.chkPriceList.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPriceList.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPriceList.Properties.Appearance.Options.UseFont = true;
            this.chkPriceList.Properties.Appearance.Options.UseForeColor = true;
            this.chkPriceList.Properties.Caption = "Price List";
            this.chkPriceList.Size = new System.Drawing.Size(85, 20);
            this.chkPriceList.TabIndex = 80;
            // 
            // chkPrintSupplierSC
            // 
            this.chkPrintSupplierSC.Location = new System.Drawing.Point(356, 242);
            this.chkPrintSupplierSC.Name = "chkPrintSupplierSC";
            this.chkPrintSupplierSC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintSupplierSC.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintSupplierSC.Properties.Appearance.Options.UseFont = true;
            this.chkPrintSupplierSC.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintSupplierSC.Properties.Caption = "Print";
            this.chkPrintSupplierSC.Size = new System.Drawing.Size(58, 20);
            this.chkPrintSupplierSC.TabIndex = 79;
            // 
            // chkDeleteSupplierSC
            // 
            this.chkDeleteSupplierSC.Location = new System.Drawing.Point(287, 242);
            this.chkDeleteSupplierSC.Name = "chkDeleteSupplierSC";
            this.chkDeleteSupplierSC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteSupplierSC.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteSupplierSC.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteSupplierSC.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteSupplierSC.Properties.Caption = "Delete";
            this.chkDeleteSupplierSC.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteSupplierSC.TabIndex = 78;
            // 
            // chkEditSupplierSC
            // 
            this.chkEditSupplierSC.Location = new System.Drawing.Point(223, 242);
            this.chkEditSupplierSC.Name = "chkEditSupplierSC";
            this.chkEditSupplierSC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditSupplierSC.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditSupplierSC.Properties.Appearance.Options.UseFont = true;
            this.chkEditSupplierSC.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditSupplierSC.Properties.Caption = "Edit";
            this.chkEditSupplierSC.Size = new System.Drawing.Size(58, 20);
            this.chkEditSupplierSC.TabIndex = 77;
            // 
            // chkNewSupplierSC
            // 
            this.chkNewSupplierSC.Location = new System.Drawing.Point(159, 242);
            this.chkNewSupplierSC.Name = "chkNewSupplierSC";
            this.chkNewSupplierSC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewSupplierSC.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewSupplierSC.Properties.Appearance.Options.UseFont = true;
            this.chkNewSupplierSC.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewSupplierSC.Properties.Caption = "New";
            this.chkNewSupplierSC.Size = new System.Drawing.Size(58, 20);
            this.chkNewSupplierSC.TabIndex = 76;
            // 
            // chkSupplierSC
            // 
            this.chkSupplierSC.Location = new System.Drawing.Point(18, 240);
            this.chkSupplierSC.Name = "chkSupplierSC";
            this.chkSupplierSC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSupplierSC.Properties.Appearance.Options.UseFont = true;
            this.chkSupplierSC.Properties.Caption = "Supplier SC";
            this.chkSupplierSC.Size = new System.Drawing.Size(101, 22);
            this.chkSupplierSC.TabIndex = 75;
            // 
            // chkUpdatePL
            // 
            this.chkUpdatePL.Location = new System.Drawing.Point(512, 17);
            this.chkUpdatePL.Name = "chkUpdatePL";
            this.chkUpdatePL.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkUpdatePL.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkUpdatePL.Properties.Appearance.Options.UseFont = true;
            this.chkUpdatePL.Properties.Appearance.Options.UseForeColor = true;
            this.chkUpdatePL.Properties.Caption = "Update PL";
            this.chkUpdatePL.Size = new System.Drawing.Size(86, 20);
            this.chkUpdatePL.TabIndex = 74;
            // 
            // chkInactiveBahanPenolong
            // 
            this.chkInactiveBahanPenolong.Location = new System.Drawing.Point(580, 102);
            this.chkInactiveBahanPenolong.Name = "chkInactiveBahanPenolong";
            this.chkInactiveBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkInactiveBahanPenolong.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkInactiveBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkInactiveBahanPenolong.Properties.Appearance.Options.UseForeColor = true;
            this.chkInactiveBahanPenolong.Properties.Caption = "Inactive";
            this.chkInactiveBahanPenolong.Size = new System.Drawing.Size(123, 20);
            this.chkInactiveBahanPenolong.TabIndex = 73;
            // 
            // chkInactiveBahanBaku
            // 
            this.chkInactiveBahanBaku.Location = new System.Drawing.Point(621, 74);
            this.chkInactiveBahanBaku.Name = "chkInactiveBahanBaku";
            this.chkInactiveBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkInactiveBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkInactiveBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkInactiveBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkInactiveBahanBaku.Properties.Caption = "Inactive";
            this.chkInactiveBahanBaku.Size = new System.Drawing.Size(82, 20);
            this.chkInactiveBahanBaku.TabIndex = 72;
            // 
            // chkInactiveProduct
            // 
            this.chkInactiveProduct.Location = new System.Drawing.Point(604, 17);
            this.chkInactiveProduct.Name = "chkInactiveProduct";
            this.chkInactiveProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkInactiveProduct.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkInactiveProduct.Properties.Appearance.Options.UseFont = true;
            this.chkInactiveProduct.Properties.Appearance.Options.UseForeColor = true;
            this.chkInactiveProduct.Properties.Caption = "Inactive";
            this.chkInactiveProduct.Size = new System.Drawing.Size(74, 20);
            this.chkInactiveProduct.TabIndex = 71;
            // 
            // chkPackaging
            // 
            this.chkPackaging.Location = new System.Drawing.Point(200, 380);
            this.chkPackaging.Name = "chkPackaging";
            this.chkPackaging.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPackaging.Properties.Appearance.Options.UseFont = true;
            this.chkPackaging.Properties.Caption = "Packaging";
            this.chkPackaging.Size = new System.Drawing.Size(117, 22);
            this.chkPackaging.TabIndex = 70;
            // 
            // chkTerms
            // 
            this.chkTerms.Location = new System.Drawing.Point(114, 380);
            this.chkTerms.Name = "chkTerms";
            this.chkTerms.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkTerms.Properties.Appearance.Options.UseFont = true;
            this.chkTerms.Properties.Caption = "Terms";
            this.chkTerms.Size = new System.Drawing.Size(117, 22);
            this.chkTerms.TabIndex = 69;
            // 
            // chkTipeSampleCode
            // 
            this.chkTipeSampleCode.Location = new System.Drawing.Point(511, 130);
            this.chkTipeSampleCode.Name = "chkTipeSampleCode";
            this.chkTipeSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkTipeSampleCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkTipeSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkTipeSampleCode.Properties.Appearance.Options.UseForeColor = true;
            this.chkTipeSampleCode.Properties.Caption = "Tipe";
            this.chkTipeSampleCode.Size = new System.Drawing.Size(58, 20);
            this.chkTipeSampleCode.TabIndex = 68;
            // 
            // chkFisikSampleCode
            // 
            this.chkFisikSampleCode.Location = new System.Drawing.Point(420, 130);
            this.chkFisikSampleCode.Name = "chkFisikSampleCode";
            this.chkFisikSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkFisikSampleCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkFisikSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkFisikSampleCode.Properties.Appearance.Options.UseForeColor = true;
            this.chkFisikSampleCode.Properties.Caption = "Fisik";
            this.chkFisikSampleCode.Size = new System.Drawing.Size(58, 20);
            this.chkFisikSampleCode.TabIndex = 67;
            // 
            // chkStatusBahanPenolong
            // 
            this.chkStatusBahanPenolong.Location = new System.Drawing.Point(511, 102);
            this.chkStatusBahanPenolong.Name = "chkStatusBahanPenolong";
            this.chkStatusBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkStatusBahanPenolong.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkStatusBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkStatusBahanPenolong.Properties.Appearance.Options.UseForeColor = true;
            this.chkStatusBahanPenolong.Properties.Caption = "Status";
            this.chkStatusBahanPenolong.Size = new System.Drawing.Size(74, 20);
            this.chkStatusBahanPenolong.TabIndex = 66;
            // 
            // chkKategoriBahanPenolong
            // 
            this.chkKategoriBahanPenolong.Location = new System.Drawing.Point(420, 102);
            this.chkKategoriBahanPenolong.Name = "chkKategoriBahanPenolong";
            this.chkKategoriBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkKategoriBahanPenolong.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkKategoriBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkKategoriBahanPenolong.Properties.Appearance.Options.UseForeColor = true;
            this.chkKategoriBahanPenolong.Properties.Caption = "Kategori";
            this.chkKategoriBahanPenolong.Size = new System.Drawing.Size(74, 20);
            this.chkKategoriBahanPenolong.TabIndex = 65;
            // 
            // chkTipeBahanBaku
            // 
            this.chkTipeBahanBaku.Location = new System.Drawing.Point(484, 74);
            this.chkTipeBahanBaku.Name = "chkTipeBahanBaku";
            this.chkTipeBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkTipeBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkTipeBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkTipeBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkTipeBahanBaku.Properties.Caption = "Tipe";
            this.chkTipeBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkTipeBahanBaku.TabIndex = 65;
            // 
            // chkFisikBahanBaku
            // 
            this.chkFisikBahanBaku.Location = new System.Drawing.Point(420, 74);
            this.chkFisikBahanBaku.Name = "chkFisikBahanBaku";
            this.chkFisikBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkFisikBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkFisikBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkFisikBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkFisikBahanBaku.Properties.Caption = "Fisik";
            this.chkFisikBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkFisikBahanBaku.TabIndex = 64;
            // 
            // chkPrintContactList
            // 
            this.chkPrintContactList.Location = new System.Drawing.Point(356, 438);
            this.chkPrintContactList.Name = "chkPrintContactList";
            this.chkPrintContactList.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintContactList.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintContactList.Properties.Appearance.Options.UseFont = true;
            this.chkPrintContactList.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintContactList.Properties.Caption = "Print";
            this.chkPrintContactList.Size = new System.Drawing.Size(58, 20);
            this.chkPrintContactList.TabIndex = 63;
            // 
            // chkPrintCustomer
            // 
            this.chkPrintCustomer.Location = new System.Drawing.Point(356, 270);
            this.chkPrintCustomer.Name = "chkPrintCustomer";
            this.chkPrintCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintCustomer.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintCustomer.Properties.Appearance.Options.UseFont = true;
            this.chkPrintCustomer.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintCustomer.Properties.Caption = "Print";
            this.chkPrintCustomer.Size = new System.Drawing.Size(58, 20);
            this.chkPrintCustomer.TabIndex = 38;
            // 
            // chkPrintSupplier
            // 
            this.chkPrintSupplier.Location = new System.Drawing.Point(356, 214);
            this.chkPrintSupplier.Name = "chkPrintSupplier";
            this.chkPrintSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintSupplier.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkPrintSupplier.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintSupplier.Properties.Caption = "Print";
            this.chkPrintSupplier.Size = new System.Drawing.Size(58, 20);
            this.chkPrintSupplier.TabIndex = 33;
            // 
            // chkRecapSampleCode
            // 
            this.chkRecapSampleCode.Location = new System.Drawing.Point(356, 130);
            this.chkRecapSampleCode.Name = "chkRecapSampleCode";
            this.chkRecapSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkRecapSampleCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkRecapSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkRecapSampleCode.Properties.Appearance.Options.UseForeColor = true;
            this.chkRecapSampleCode.Properties.Caption = "Recap";
            this.chkRecapSampleCode.Size = new System.Drawing.Size(58, 20);
            this.chkRecapSampleCode.TabIndex = 20;
            // 
            // chkRecapBahanPenolong
            // 
            this.chkRecapBahanPenolong.Location = new System.Drawing.Point(356, 102);
            this.chkRecapBahanPenolong.Name = "chkRecapBahanPenolong";
            this.chkRecapBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkRecapBahanPenolong.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkRecapBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkRecapBahanPenolong.Properties.Appearance.Options.UseForeColor = true;
            this.chkRecapBahanPenolong.Properties.Caption = "Recap";
            this.chkRecapBahanPenolong.Size = new System.Drawing.Size(58, 20);
            this.chkRecapBahanPenolong.TabIndex = 15;
            // 
            // chkRecapBahanBaku
            // 
            this.chkRecapBahanBaku.Location = new System.Drawing.Point(356, 74);
            this.chkRecapBahanBaku.Name = "chkRecapBahanBaku";
            this.chkRecapBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkRecapBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkRecapBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkRecapBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkRecapBahanBaku.Properties.Caption = "Recap";
            this.chkRecapBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkRecapBahanBaku.TabIndex = 10;
            // 
            // chkPrintProduct
            // 
            this.chkPrintProduct.Location = new System.Drawing.Point(356, 17);
            this.chkPrintProduct.Name = "chkPrintProduct";
            this.chkPrintProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintProduct.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintProduct.Properties.Appearance.Options.UseFont = true;
            this.chkPrintProduct.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintProduct.Properties.Caption = "Print";
            this.chkPrintProduct.Size = new System.Drawing.Size(58, 20);
            this.chkPrintProduct.TabIndex = 4;
            // 
            // chkDeleteContactList
            // 
            this.chkDeleteContactList.Location = new System.Drawing.Point(287, 438);
            this.chkDeleteContactList.Name = "chkDeleteContactList";
            this.chkDeleteContactList.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteContactList.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteContactList.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteContactList.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteContactList.Properties.Caption = "Delete";
            this.chkDeleteContactList.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteContactList.TabIndex = 62;
            // 
            // chkEditContactList
            // 
            this.chkEditContactList.Location = new System.Drawing.Point(223, 438);
            this.chkEditContactList.Name = "chkEditContactList";
            this.chkEditContactList.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditContactList.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditContactList.Properties.Appearance.Options.UseFont = true;
            this.chkEditContactList.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditContactList.Properties.Caption = "Edit";
            this.chkEditContactList.Size = new System.Drawing.Size(58, 20);
            this.chkEditContactList.TabIndex = 61;
            // 
            // chkNewContactList
            // 
            this.chkNewContactList.Location = new System.Drawing.Point(159, 438);
            this.chkNewContactList.Name = "chkNewContactList";
            this.chkNewContactList.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewContactList.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewContactList.Properties.Appearance.Options.UseFont = true;
            this.chkNewContactList.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewContactList.Properties.Caption = "New";
            this.chkNewContactList.Size = new System.Drawing.Size(58, 20);
            this.chkNewContactList.TabIndex = 60;
            // 
            // chkDeleteUserAccount
            // 
            this.chkDeleteUserAccount.Location = new System.Drawing.Point(287, 410);
            this.chkDeleteUserAccount.Name = "chkDeleteUserAccount";
            this.chkDeleteUserAccount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteUserAccount.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteUserAccount.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteUserAccount.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteUserAccount.Properties.Caption = "Delete";
            this.chkDeleteUserAccount.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteUserAccount.TabIndex = 58;
            // 
            // chkEditUserAccount
            // 
            this.chkEditUserAccount.Location = new System.Drawing.Point(223, 410);
            this.chkEditUserAccount.Name = "chkEditUserAccount";
            this.chkEditUserAccount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditUserAccount.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditUserAccount.Properties.Appearance.Options.UseFont = true;
            this.chkEditUserAccount.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditUserAccount.Properties.Caption = "Edit";
            this.chkEditUserAccount.Size = new System.Drawing.Size(58, 20);
            this.chkEditUserAccount.TabIndex = 57;
            // 
            // chkNewUserAccount
            // 
            this.chkNewUserAccount.Location = new System.Drawing.Point(159, 410);
            this.chkNewUserAccount.Name = "chkNewUserAccount";
            this.chkNewUserAccount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewUserAccount.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewUserAccount.Properties.Appearance.Options.UseFont = true;
            this.chkNewUserAccount.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewUserAccount.Properties.Caption = "New";
            this.chkNewUserAccount.Size = new System.Drawing.Size(58, 20);
            this.chkNewUserAccount.TabIndex = 56;
            // 
            // chkDeleteLocation
            // 
            this.chkDeleteLocation.Location = new System.Drawing.Point(287, 354);
            this.chkDeleteLocation.Name = "chkDeleteLocation";
            this.chkDeleteLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteLocation.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteLocation.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteLocation.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteLocation.Properties.Caption = "Delete";
            this.chkDeleteLocation.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteLocation.TabIndex = 50;
            // 
            // chkEditLocation
            // 
            this.chkEditLocation.Location = new System.Drawing.Point(223, 354);
            this.chkEditLocation.Name = "chkEditLocation";
            this.chkEditLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditLocation.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditLocation.Properties.Appearance.Options.UseFont = true;
            this.chkEditLocation.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditLocation.Properties.Caption = "Edit";
            this.chkEditLocation.Size = new System.Drawing.Size(58, 20);
            this.chkEditLocation.TabIndex = 49;
            // 
            // chkNewLocation
            // 
            this.chkNewLocation.Location = new System.Drawing.Point(159, 354);
            this.chkNewLocation.Name = "chkNewLocation";
            this.chkNewLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewLocation.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewLocation.Properties.Appearance.Options.UseFont = true;
            this.chkNewLocation.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewLocation.Properties.Caption = "New";
            this.chkNewLocation.Size = new System.Drawing.Size(58, 20);
            this.chkNewLocation.TabIndex = 48;
            // 
            // chkDeleteExpedition
            // 
            this.chkDeleteExpedition.Location = new System.Drawing.Point(287, 326);
            this.chkDeleteExpedition.Name = "chkDeleteExpedition";
            this.chkDeleteExpedition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteExpedition.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteExpedition.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteExpedition.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteExpedition.Properties.Caption = "Delete";
            this.chkDeleteExpedition.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteExpedition.TabIndex = 46;
            // 
            // chkEditExpedition
            // 
            this.chkEditExpedition.Location = new System.Drawing.Point(223, 326);
            this.chkEditExpedition.Name = "chkEditExpedition";
            this.chkEditExpedition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditExpedition.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditExpedition.Properties.Appearance.Options.UseFont = true;
            this.chkEditExpedition.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditExpedition.Properties.Caption = "Edit";
            this.chkEditExpedition.Size = new System.Drawing.Size(58, 20);
            this.chkEditExpedition.TabIndex = 45;
            // 
            // chkNewExpedition
            // 
            this.chkNewExpedition.Location = new System.Drawing.Point(159, 326);
            this.chkNewExpedition.Name = "chkNewExpedition";
            this.chkNewExpedition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewExpedition.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewExpedition.Properties.Appearance.Options.UseFont = true;
            this.chkNewExpedition.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewExpedition.Properties.Caption = "New";
            this.chkNewExpedition.Size = new System.Drawing.Size(58, 20);
            this.chkNewExpedition.TabIndex = 44;
            // 
            // chkDeleteEmployee
            // 
            this.chkDeleteEmployee.Location = new System.Drawing.Point(287, 298);
            this.chkDeleteEmployee.Name = "chkDeleteEmployee";
            this.chkDeleteEmployee.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteEmployee.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteEmployee.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteEmployee.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteEmployee.Properties.Caption = "Delete";
            this.chkDeleteEmployee.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteEmployee.TabIndex = 42;
            // 
            // chkEditEmployee
            // 
            this.chkEditEmployee.Location = new System.Drawing.Point(223, 298);
            this.chkEditEmployee.Name = "chkEditEmployee";
            this.chkEditEmployee.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditEmployee.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditEmployee.Properties.Appearance.Options.UseFont = true;
            this.chkEditEmployee.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditEmployee.Properties.Caption = "Edit";
            this.chkEditEmployee.Size = new System.Drawing.Size(58, 20);
            this.chkEditEmployee.TabIndex = 41;
            // 
            // chkNewEmployee
            // 
            this.chkNewEmployee.Location = new System.Drawing.Point(159, 298);
            this.chkNewEmployee.Name = "chkNewEmployee";
            this.chkNewEmployee.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewEmployee.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewEmployee.Properties.Appearance.Options.UseFont = true;
            this.chkNewEmployee.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewEmployee.Properties.Caption = "New";
            this.chkNewEmployee.Size = new System.Drawing.Size(58, 20);
            this.chkNewEmployee.TabIndex = 40;
            // 
            // chkDeleteCustomer
            // 
            this.chkDeleteCustomer.Location = new System.Drawing.Point(287, 270);
            this.chkDeleteCustomer.Name = "chkDeleteCustomer";
            this.chkDeleteCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteCustomer.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteCustomer.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteCustomer.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteCustomer.Properties.Caption = "Delete";
            this.chkDeleteCustomer.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteCustomer.TabIndex = 37;
            // 
            // chkEditCustomer
            // 
            this.chkEditCustomer.Location = new System.Drawing.Point(223, 270);
            this.chkEditCustomer.Name = "chkEditCustomer";
            this.chkEditCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditCustomer.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditCustomer.Properties.Appearance.Options.UseFont = true;
            this.chkEditCustomer.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditCustomer.Properties.Caption = "Edit";
            this.chkEditCustomer.Size = new System.Drawing.Size(58, 20);
            this.chkEditCustomer.TabIndex = 36;
            // 
            // chkNewCustomer
            // 
            this.chkNewCustomer.Location = new System.Drawing.Point(159, 270);
            this.chkNewCustomer.Name = "chkNewCustomer";
            this.chkNewCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewCustomer.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewCustomer.Properties.Appearance.Options.UseFont = true;
            this.chkNewCustomer.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewCustomer.Properties.Caption = "New";
            this.chkNewCustomer.Size = new System.Drawing.Size(58, 20);
            this.chkNewCustomer.TabIndex = 35;
            // 
            // chkDeleteSupplier
            // 
            this.chkDeleteSupplier.Location = new System.Drawing.Point(287, 214);
            this.chkDeleteSupplier.Name = "chkDeleteSupplier";
            this.chkDeleteSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteSupplier.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteSupplier.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteSupplier.Properties.Caption = "Delete";
            this.chkDeleteSupplier.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteSupplier.TabIndex = 32;
            // 
            // chkEditSupplier
            // 
            this.chkEditSupplier.Location = new System.Drawing.Point(223, 214);
            this.chkEditSupplier.Name = "chkEditSupplier";
            this.chkEditSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditSupplier.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkEditSupplier.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditSupplier.Properties.Caption = "Edit";
            this.chkEditSupplier.Size = new System.Drawing.Size(58, 20);
            this.chkEditSupplier.TabIndex = 31;
            // 
            // chkNewSupplier
            // 
            this.chkNewSupplier.Location = new System.Drawing.Point(159, 214);
            this.chkNewSupplier.Name = "chkNewSupplier";
            this.chkNewSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewSupplier.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkNewSupplier.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewSupplier.Properties.Caption = "New";
            this.chkNewSupplier.Size = new System.Drawing.Size(58, 20);
            this.chkNewSupplier.TabIndex = 30;
            // 
            // chkDeleteDepartment
            // 
            this.chkDeleteDepartment.Location = new System.Drawing.Point(287, 186);
            this.chkDeleteDepartment.Name = "chkDeleteDepartment";
            this.chkDeleteDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteDepartment.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteDepartment.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteDepartment.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteDepartment.Properties.Caption = "Delete";
            this.chkDeleteDepartment.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteDepartment.TabIndex = 28;
            // 
            // chkEditDepartment
            // 
            this.chkEditDepartment.Location = new System.Drawing.Point(223, 186);
            this.chkEditDepartment.Name = "chkEditDepartment";
            this.chkEditDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditDepartment.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditDepartment.Properties.Appearance.Options.UseFont = true;
            this.chkEditDepartment.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditDepartment.Properties.Caption = "Edit";
            this.chkEditDepartment.Size = new System.Drawing.Size(58, 20);
            this.chkEditDepartment.TabIndex = 27;
            // 
            // chkNewDepartment
            // 
            this.chkNewDepartment.Location = new System.Drawing.Point(159, 186);
            this.chkNewDepartment.Name = "chkNewDepartment";
            this.chkNewDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewDepartment.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewDepartment.Properties.Appearance.Options.UseFont = true;
            this.chkNewDepartment.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewDepartment.Properties.Caption = "New";
            this.chkNewDepartment.Size = new System.Drawing.Size(58, 20);
            this.chkNewDepartment.TabIndex = 26;
            // 
            // chkDeleteSampleCode
            // 
            this.chkDeleteSampleCode.Location = new System.Drawing.Point(287, 130);
            this.chkDeleteSampleCode.Name = "chkDeleteSampleCode";
            this.chkDeleteSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteSampleCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteSampleCode.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteSampleCode.Properties.Caption = "Delete";
            this.chkDeleteSampleCode.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteSampleCode.TabIndex = 19;
            // 
            // chkEditSampleCode
            // 
            this.chkEditSampleCode.Location = new System.Drawing.Point(223, 130);
            this.chkEditSampleCode.Name = "chkEditSampleCode";
            this.chkEditSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditSampleCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkEditSampleCode.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditSampleCode.Properties.Caption = "Edit";
            this.chkEditSampleCode.Size = new System.Drawing.Size(58, 20);
            this.chkEditSampleCode.TabIndex = 18;
            // 
            // chkNewSampleCode
            // 
            this.chkNewSampleCode.Location = new System.Drawing.Point(159, 130);
            this.chkNewSampleCode.Name = "chkNewSampleCode";
            this.chkNewSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewSampleCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkNewSampleCode.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewSampleCode.Properties.Caption = "New";
            this.chkNewSampleCode.Size = new System.Drawing.Size(58, 20);
            this.chkNewSampleCode.TabIndex = 17;
            // 
            // chkDeleteBahanPenolong
            // 
            this.chkDeleteBahanPenolong.Location = new System.Drawing.Point(287, 102);
            this.chkDeleteBahanPenolong.Name = "chkDeleteBahanPenolong";
            this.chkDeleteBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteBahanPenolong.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteBahanPenolong.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteBahanPenolong.Properties.Caption = "Delete";
            this.chkDeleteBahanPenolong.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteBahanPenolong.TabIndex = 14;
            // 
            // chkEditBahanPenolong
            // 
            this.chkEditBahanPenolong.Location = new System.Drawing.Point(223, 102);
            this.chkEditBahanPenolong.Name = "chkEditBahanPenolong";
            this.chkEditBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditBahanPenolong.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkEditBahanPenolong.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditBahanPenolong.Properties.Caption = "Edit";
            this.chkEditBahanPenolong.Size = new System.Drawing.Size(58, 20);
            this.chkEditBahanPenolong.TabIndex = 13;
            // 
            // chkNewBahanPenolong
            // 
            this.chkNewBahanPenolong.Location = new System.Drawing.Point(159, 102);
            this.chkNewBahanPenolong.Name = "chkNewBahanPenolong";
            this.chkNewBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewBahanPenolong.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkNewBahanPenolong.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewBahanPenolong.Properties.Caption = "New";
            this.chkNewBahanPenolong.Size = new System.Drawing.Size(58, 20);
            this.chkNewBahanPenolong.TabIndex = 12;
            // 
            // chkDeleteBahanBaku
            // 
            this.chkDeleteBahanBaku.Location = new System.Drawing.Point(287, 74);
            this.chkDeleteBahanBaku.Name = "chkDeleteBahanBaku";
            this.chkDeleteBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteBahanBaku.Properties.Caption = "Delete";
            this.chkDeleteBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteBahanBaku.TabIndex = 9;
            // 
            // chkEditBahanBaku
            // 
            this.chkEditBahanBaku.Location = new System.Drawing.Point(223, 74);
            this.chkEditBahanBaku.Name = "chkEditBahanBaku";
            this.chkEditBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkEditBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditBahanBaku.Properties.Caption = "Edit";
            this.chkEditBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkEditBahanBaku.TabIndex = 8;
            // 
            // chkNewBahanBaku
            // 
            this.chkNewBahanBaku.Location = new System.Drawing.Point(159, 74);
            this.chkNewBahanBaku.Name = "chkNewBahanBaku";
            this.chkNewBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkNewBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewBahanBaku.Properties.Caption = "New";
            this.chkNewBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkNewBahanBaku.TabIndex = 7;
            // 
            // chkDeleteProduct
            // 
            this.chkDeleteProduct.Location = new System.Drawing.Point(287, 17);
            this.chkDeleteProduct.Name = "chkDeleteProduct";
            this.chkDeleteProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteProduct.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteProduct.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteProduct.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteProduct.Properties.Caption = "Delete";
            this.chkDeleteProduct.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteProduct.TabIndex = 3;
            // 
            // chkEditProduct
            // 
            this.chkEditProduct.Location = new System.Drawing.Point(223, 17);
            this.chkEditProduct.Name = "chkEditProduct";
            this.chkEditProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditProduct.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditProduct.Properties.Appearance.Options.UseFont = true;
            this.chkEditProduct.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditProduct.Properties.Caption = "Edit";
            this.chkEditProduct.Size = new System.Drawing.Size(58, 20);
            this.chkEditProduct.TabIndex = 2;
            // 
            // chkNewProduct
            // 
            this.chkNewProduct.Location = new System.Drawing.Point(159, 17);
            this.chkNewProduct.Name = "chkNewProduct";
            this.chkNewProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewProduct.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewProduct.Properties.Appearance.Options.UseFont = true;
            this.chkNewProduct.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewProduct.Properties.Caption = "New";
            this.chkNewProduct.Size = new System.Drawing.Size(58, 20);
            this.chkNewProduct.TabIndex = 1;
            // 
            // chkSampleCode
            // 
            this.chkSampleCode.Location = new System.Drawing.Point(18, 128);
            this.chkSampleCode.Name = "chkSampleCode";
            this.chkSampleCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSampleCode.Properties.Appearance.Options.UseFont = true;
            this.chkSampleCode.Properties.Caption = "Sample Code";
            this.chkSampleCode.Size = new System.Drawing.Size(150, 22);
            this.chkSampleCode.TabIndex = 16;
            // 
            // chkEditStatusProduct
            // 
            this.chkEditStatusProduct.Location = new System.Drawing.Point(420, 17);
            this.chkEditStatusProduct.Name = "chkEditStatusProduct";
            this.chkEditStatusProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditStatusProduct.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditStatusProduct.Properties.Appearance.Options.UseFont = true;
            this.chkEditStatusProduct.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditStatusProduct.Properties.Caption = "Edit Status";
            this.chkEditStatusProduct.Size = new System.Drawing.Size(86, 20);
            this.chkEditStatusProduct.TabIndex = 5;
            // 
            // chkEmployee
            // 
            this.chkEmployee.Location = new System.Drawing.Point(18, 296);
            this.chkEmployee.Name = "chkEmployee";
            this.chkEmployee.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkEmployee.Properties.Appearance.Options.UseFont = true;
            this.chkEmployee.Properties.Caption = "Employee";
            this.chkEmployee.Size = new System.Drawing.Size(117, 22);
            this.chkEmployee.TabIndex = 39;
            // 
            // chkBahanPenolong
            // 
            this.chkBahanPenolong.Location = new System.Drawing.Point(18, 100);
            this.chkBahanPenolong.Name = "chkBahanPenolong";
            this.chkBahanPenolong.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkBahanPenolong.Properties.Appearance.Options.UseFont = true;
            this.chkBahanPenolong.Properties.Caption = "Bahan Penolong";
            this.chkBahanPenolong.Size = new System.Drawing.Size(117, 22);
            this.chkBahanPenolong.TabIndex = 11;
            // 
            // chkBahanBaku
            // 
            this.chkBahanBaku.Location = new System.Drawing.Point(18, 72);
            this.chkBahanBaku.Name = "chkBahanBaku";
            this.chkBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkBahanBaku.Properties.Caption = "Bahan Baku";
            this.chkBahanBaku.Size = new System.Drawing.Size(117, 22);
            this.chkBahanBaku.TabIndex = 6;
            // 
            // chkCurrency
            // 
            this.chkCurrency.Location = new System.Drawing.Point(18, 380);
            this.chkCurrency.Name = "chkCurrency";
            this.chkCurrency.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCurrency.Properties.Appearance.Options.UseFont = true;
            this.chkCurrency.Properties.Caption = "Currency";
            this.chkCurrency.Size = new System.Drawing.Size(117, 22);
            this.chkCurrency.TabIndex = 51;
            // 
            // chkLocation
            // 
            this.chkLocation.Location = new System.Drawing.Point(18, 352);
            this.chkLocation.Name = "chkLocation";
            this.chkLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkLocation.Properties.Appearance.Options.UseFont = true;
            this.chkLocation.Properties.Caption = "Location";
            this.chkLocation.Size = new System.Drawing.Size(117, 22);
            this.chkLocation.TabIndex = 47;
            // 
            // chkExpedition
            // 
            this.chkExpedition.Location = new System.Drawing.Point(18, 324);
            this.chkExpedition.Name = "chkExpedition";
            this.chkExpedition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExpedition.Properties.Appearance.Options.UseFont = true;
            this.chkExpedition.Properties.Caption = "Expedition";
            this.chkExpedition.Size = new System.Drawing.Size(117, 22);
            this.chkExpedition.TabIndex = 43;
            // 
            // chkContactList
            // 
            this.chkContactList.Location = new System.Drawing.Point(18, 436);
            this.chkContactList.Name = "chkContactList";
            this.chkContactList.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkContactList.Properties.Appearance.Options.UseFont = true;
            this.chkContactList.Properties.Caption = "Contact List";
            this.chkContactList.Size = new System.Drawing.Size(117, 22);
            this.chkContactList.TabIndex = 59;
            // 
            // chkDepartment
            // 
            this.chkDepartment.Location = new System.Drawing.Point(18, 184);
            this.chkDepartment.Name = "chkDepartment";
            this.chkDepartment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDepartment.Properties.Appearance.Options.UseFont = true;
            this.chkDepartment.Properties.Caption = "Department";
            this.chkDepartment.Size = new System.Drawing.Size(117, 22);
            this.chkDepartment.TabIndex = 25;
            // 
            // chkUserAccount
            // 
            this.chkUserAccount.Location = new System.Drawing.Point(18, 408);
            this.chkUserAccount.Name = "chkUserAccount";
            this.chkUserAccount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkUserAccount.Properties.Appearance.Options.UseFont = true;
            this.chkUserAccount.Properties.Caption = "User Account";
            this.chkUserAccount.Size = new System.Drawing.Size(117, 22);
            this.chkUserAccount.TabIndex = 55;
            // 
            // chkCustomer
            // 
            this.chkCustomer.Location = new System.Drawing.Point(18, 268);
            this.chkCustomer.Name = "chkCustomer";
            this.chkCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomer.Properties.Appearance.Options.UseFont = true;
            this.chkCustomer.Properties.Caption = "Customer";
            this.chkCustomer.Size = new System.Drawing.Size(82, 22);
            this.chkCustomer.TabIndex = 34;
            // 
            // chkSupplier
            // 
            this.chkSupplier.Location = new System.Drawing.Point(18, 212);
            this.chkSupplier.Name = "chkSupplier";
            this.chkSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkSupplier.Properties.Caption = "Supplier";
            this.chkSupplier.Size = new System.Drawing.Size(75, 22);
            this.chkSupplier.TabIndex = 29;
            // 
            // chkCategory
            // 
            this.chkCategory.Location = new System.Drawing.Point(18, 156);
            this.chkCategory.Name = "chkCategory";
            this.chkCategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCategory.Properties.Appearance.Options.UseFont = true;
            this.chkCategory.Properties.Caption = "Category";
            this.chkCategory.Size = new System.Drawing.Size(82, 22);
            this.chkCategory.TabIndex = 21;
            // 
            // chkProduct
            // 
            this.chkProduct.Location = new System.Drawing.Point(18, 15);
            this.chkProduct.Name = "chkProduct";
            this.chkProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkProduct.Properties.Appearance.Options.UseFont = true;
            this.chkProduct.Properties.Caption = "Product";
            this.chkProduct.Size = new System.Drawing.Size(75, 22);
            this.chkProduct.TabIndex = 0;
            // 
            // tabTransaction
            // 
            this.tabTransaction.Controls.Add(this.chkUpdateRemarksTransferStock);
            this.tabTransaction.Controls.Add(this.chkPrintTransferStock);
            this.tabTransaction.Controls.Add(this.chkStatusQCBahanBaku);
            this.tabTransaction.Controls.Add(this.chkKesimpulanQCBahanBaku);
            this.tabTransaction.Controls.Add(this.chkStatusQCBarangJadi);
            this.tabTransaction.Controls.Add(this.chkKesimpulanQCBarangJadi);
            this.tabTransaction.Controls.Add(this.chkChangeTSLocation);
            this.tabTransaction.Controls.Add(this.chkSetDONumber);
            this.tabTransaction.Controls.Add(this.chkPrintLabService);
            this.tabTransaction.Controls.Add(this.chkApprove2TransferStock);
            this.tabTransaction.Controls.Add(this.chkApproveTransferStock);
            this.tabTransaction.Controls.Add(this.chkApprove2PenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkApprove2SalesOrder);
            this.tabTransaction.Controls.Add(this.chkPrintQCKemasan);
            this.tabTransaction.Controls.Add(this.chkDeleteQCKemasan);
            this.tabTransaction.Controls.Add(this.chkEditQCKemasan);
            this.tabTransaction.Controls.Add(this.chkNewQCKemasan);
            this.tabTransaction.Controls.Add(this.chkQCKemasan);
            this.tabTransaction.Controls.Add(this.chkDeleteLabService);
            this.tabTransaction.Controls.Add(this.chkEditLabService);
            this.tabTransaction.Controls.Add(this.chkNewLabService);
            this.tabTransaction.Controls.Add(this.chkLabService);
            this.tabTransaction.Controls.Add(this.chkDeleteStandardDeliveryTime);
            this.tabTransaction.Controls.Add(this.chkEditStandardDeliveryTime);
            this.tabTransaction.Controls.Add(this.chkNewStandardDeliveryTime);
            this.tabTransaction.Controls.Add(this.chkStandardDeliveryTime);
            this.tabTransaction.Controls.Add(this.chkPrintQCPacking);
            this.tabTransaction.Controls.Add(this.chkDeleteQCPacking);
            this.tabTransaction.Controls.Add(this.chkEditQCPacking);
            this.tabTransaction.Controls.Add(this.chkNewQCPacking);
            this.tabTransaction.Controls.Add(this.chkPrintQCBahanBaku);
            this.tabTransaction.Controls.Add(this.chkDeleteQCBahanBaku);
            this.tabTransaction.Controls.Add(this.chkEditQCBahanBaku);
            this.tabTransaction.Controls.Add(this.chkNewQCBahanBaku);
            this.tabTransaction.Controls.Add(this.chkPrintQCBarangJadi);
            this.tabTransaction.Controls.Add(this.chkDeleteQCBarangJadi);
            this.tabTransaction.Controls.Add(this.chkEditQCBarangJadi);
            this.tabTransaction.Controls.Add(this.chkNewQCBarangJadi);
            this.tabTransaction.Controls.Add(this.chkDeleteTransferStock);
            this.tabTransaction.Controls.Add(this.chkEditTransferStock);
            this.tabTransaction.Controls.Add(this.chkNewTransferStock);
            this.tabTransaction.Controls.Add(this.chkApprovePenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkPrintPenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkDeletePenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkEditPenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkNewPenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkSetDeposition);
            this.tabTransaction.Controls.Add(this.chkSolveCustomerVoice);
            this.tabTransaction.Controls.Add(this.chkDeleteCustomerVoice);
            this.tabTransaction.Controls.Add(this.chkEditCustomerVoice);
            this.tabTransaction.Controls.Add(this.chkNewCustomerVoice);
            this.tabTransaction.Controls.Add(this.chkDeleteExpense);
            this.tabTransaction.Controls.Add(this.chkEditExpense);
            this.tabTransaction.Controls.Add(this.chkNewExpense);
            this.tabTransaction.Controls.Add(this.chkSetLocationDeliveryOrder);
            this.tabTransaction.Controls.Add(this.chkPrintDeliveryOrder);
            this.tabTransaction.Controls.Add(this.chkDeleteDeliveryOrder);
            this.tabTransaction.Controls.Add(this.chkEditDeliveryOrder);
            this.tabTransaction.Controls.Add(this.chkNewDeliveryOrder);
            this.tabTransaction.Controls.Add(this.chkApproveSalesOrder);
            this.tabTransaction.Controls.Add(this.chkPrintSalesOrder);
            this.tabTransaction.Controls.Add(this.chkDeleteSalesOrder);
            this.tabTransaction.Controls.Add(this.chkEditSalesOrder);
            this.tabTransaction.Controls.Add(this.chkNewSalesOrder);
            this.tabTransaction.Controls.Add(this.chkQCPacking);
            this.tabTransaction.Controls.Add(this.chkQCBahanBaku);
            this.tabTransaction.Controls.Add(this.chkQCBarangJadi);
            this.tabTransaction.Controls.Add(this.chkWorkInProgress);
            this.tabTransaction.Controls.Add(this.chkTransferStock);
            this.tabTransaction.Controls.Add(this.chkChangeDOLocation);
            this.tabTransaction.Controls.Add(this.chkPenyerahanProduksi);
            this.tabTransaction.Controls.Add(this.chkCustomerVoice);
            this.tabTransaction.Controls.Add(this.chkPayInvoice);
            this.tabTransaction.Controls.Add(this.chkPrintInvoice);
            this.tabTransaction.Controls.Add(this.chkCancelInvoice);
            this.tabTransaction.Controls.Add(this.chkCreateInvoice);
            this.tabTransaction.Controls.Add(this.chkExpense);
            this.tabTransaction.Controls.Add(this.chkStockAdjustment);
            this.tabTransaction.Controls.Add(this.chkSalesInvoice);
            this.tabTransaction.Controls.Add(this.chkDeliveryOrder);
            this.tabTransaction.Controls.Add(this.chkSalesOrder);
            this.tabTransaction.Name = "tabTransaction";
            this.tabTransaction.Size = new System.Drawing.Size(717, 474);
            this.tabTransaction.Text = "Transaction";
            // 
            // chkUpdateRemarksTransferStock
            // 
            this.chkUpdateRemarksTransferStock.Location = new System.Drawing.Point(332, 267);
            this.chkUpdateRemarksTransferStock.Name = "chkUpdateRemarksTransferStock";
            this.chkUpdateRemarksTransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkUpdateRemarksTransferStock.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkUpdateRemarksTransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkUpdateRemarksTransferStock.Properties.Appearance.Options.UseForeColor = true;
            this.chkUpdateRemarksTransferStock.Properties.Caption = "Update Remarks Transfer Stock";
            this.chkUpdateRemarksTransferStock.Size = new System.Drawing.Size(202, 20);
            this.chkUpdateRemarksTransferStock.TabIndex = 80;
            // 
            // chkPrintTransferStock
            // 
            this.chkPrintTransferStock.Location = new System.Drawing.Point(189, 267);
            this.chkPrintTransferStock.Name = "chkPrintTransferStock";
            this.chkPrintTransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintTransferStock.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintTransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkPrintTransferStock.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintTransferStock.Properties.Caption = "Print Transfer Stock";
            this.chkPrintTransferStock.Size = new System.Drawing.Size(137, 20);
            this.chkPrintTransferStock.TabIndex = 79;
            // 
            // chkStatusQCBahanBaku
            // 
            this.chkStatusQCBahanBaku.Location = new System.Drawing.Point(540, 325);
            this.chkStatusQCBahanBaku.Name = "chkStatusQCBahanBaku";
            this.chkStatusQCBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkStatusQCBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkStatusQCBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkStatusQCBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkStatusQCBahanBaku.Properties.Caption = "Status";
            this.chkStatusQCBahanBaku.Size = new System.Drawing.Size(89, 20);
            this.chkStatusQCBahanBaku.TabIndex = 78;
            // 
            // chkKesimpulanQCBahanBaku
            // 
            this.chkKesimpulanQCBahanBaku.Location = new System.Drawing.Point(445, 325);
            this.chkKesimpulanQCBahanBaku.Name = "chkKesimpulanQCBahanBaku";
            this.chkKesimpulanQCBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkKesimpulanQCBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkKesimpulanQCBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkKesimpulanQCBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkKesimpulanQCBahanBaku.Properties.Caption = "Kesimpulan";
            this.chkKesimpulanQCBahanBaku.Size = new System.Drawing.Size(89, 20);
            this.chkKesimpulanQCBahanBaku.TabIndex = 77;
            // 
            // chkStatusQCBarangJadi
            // 
            this.chkStatusQCBarangJadi.Location = new System.Drawing.Point(540, 297);
            this.chkStatusQCBarangJadi.Name = "chkStatusQCBarangJadi";
            this.chkStatusQCBarangJadi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkStatusQCBarangJadi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkStatusQCBarangJadi.Properties.Appearance.Options.UseFont = true;
            this.chkStatusQCBarangJadi.Properties.Appearance.Options.UseForeColor = true;
            this.chkStatusQCBarangJadi.Properties.Caption = "Status";
            this.chkStatusQCBarangJadi.Size = new System.Drawing.Size(89, 20);
            this.chkStatusQCBarangJadi.TabIndex = 76;
            // 
            // chkKesimpulanQCBarangJadi
            // 
            this.chkKesimpulanQCBarangJadi.Location = new System.Drawing.Point(445, 297);
            this.chkKesimpulanQCBarangJadi.Name = "chkKesimpulanQCBarangJadi";
            this.chkKesimpulanQCBarangJadi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkKesimpulanQCBarangJadi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkKesimpulanQCBarangJadi.Properties.Appearance.Options.UseFont = true;
            this.chkKesimpulanQCBarangJadi.Properties.Appearance.Options.UseForeColor = true;
            this.chkKesimpulanQCBarangJadi.Properties.Caption = "Kesimpulan";
            this.chkKesimpulanQCBarangJadi.Size = new System.Drawing.Size(89, 20);
            this.chkKesimpulanQCBarangJadi.TabIndex = 75;
            // 
            // chkChangeTSLocation
            // 
            this.chkChangeTSLocation.Location = new System.Drawing.Point(189, 99);
            this.chkChangeTSLocation.Name = "chkChangeTSLocation";
            this.chkChangeTSLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkChangeTSLocation.Properties.Appearance.Options.UseFont = true;
            this.chkChangeTSLocation.Properties.Caption = "Change TS Location";
            this.chkChangeTSLocation.Size = new System.Drawing.Size(155, 22);
            this.chkChangeTSLocation.TabIndex = 74;
            // 
            // chkSetDONumber
            // 
            this.chkSetDONumber.Location = new System.Drawing.Point(547, 45);
            this.chkSetDONumber.Name = "chkSetDONumber";
            this.chkSetDONumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSetDONumber.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSetDONumber.Properties.Appearance.Options.UseFont = true;
            this.chkSetDONumber.Properties.Appearance.Options.UseForeColor = true;
            this.chkSetDONumber.Properties.Caption = "Set DO Number";
            this.chkSetDONumber.Size = new System.Drawing.Size(115, 20);
            this.chkSetDONumber.TabIndex = 73;
            // 
            // chkPrintLabService
            // 
            this.chkPrintLabService.Location = new System.Drawing.Point(381, 437);
            this.chkPrintLabService.Name = "chkPrintLabService";
            this.chkPrintLabService.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintLabService.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintLabService.Properties.Appearance.Options.UseFont = true;
            this.chkPrintLabService.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintLabService.Properties.Caption = "Print";
            this.chkPrintLabService.Size = new System.Drawing.Size(58, 20);
            this.chkPrintLabService.TabIndex = 72;
            // 
            // chkApprove2TransferStock
            // 
            this.chkApprove2TransferStock.Location = new System.Drawing.Point(502, 241);
            this.chkApprove2TransferStock.Name = "chkApprove2TransferStock";
            this.chkApprove2TransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApprove2TransferStock.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApprove2TransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkApprove2TransferStock.Properties.Appearance.Options.UseForeColor = true;
            this.chkApprove2TransferStock.Properties.Caption = "Plant Mgr. Approve";
            this.chkApprove2TransferStock.Size = new System.Drawing.Size(143, 20);
            this.chkApprove2TransferStock.TabIndex = 71;
            // 
            // chkApproveTransferStock
            // 
            this.chkApproveTransferStock.Location = new System.Drawing.Point(386, 241);
            this.chkApproveTransferStock.Name = "chkApproveTransferStock";
            this.chkApproveTransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApproveTransferStock.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApproveTransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkApproveTransferStock.Properties.Appearance.Options.UseForeColor = true;
            this.chkApproveTransferStock.Properties.Caption = "Coor. Approve";
            this.chkApproveTransferStock.Size = new System.Drawing.Size(110, 20);
            this.chkApproveTransferStock.TabIndex = 70;
            // 
            // chkApprove2PenyerahanProduksi
            // 
            this.chkApprove2PenyerahanProduksi.Location = new System.Drawing.Point(561, 213);
            this.chkApprove2PenyerahanProduksi.Name = "chkApprove2PenyerahanProduksi";
            this.chkApprove2PenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApprove2PenyerahanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApprove2PenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkApprove2PenyerahanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkApprove2PenyerahanProduksi.Properties.Caption = "Plant Mgr. Approve";
            this.chkApprove2PenyerahanProduksi.Size = new System.Drawing.Size(143, 20);
            this.chkApprove2PenyerahanProduksi.TabIndex = 69;
            // 
            // chkApprove2SalesOrder
            // 
            this.chkApprove2SalesOrder.Location = new System.Drawing.Point(547, 17);
            this.chkApprove2SalesOrder.Name = "chkApprove2SalesOrder";
            this.chkApprove2SalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApprove2SalesOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApprove2SalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkApprove2SalesOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkApprove2SalesOrder.Properties.Caption = "SM Approve";
            this.chkApprove2SalesOrder.Size = new System.Drawing.Size(89, 20);
            this.chkApprove2SalesOrder.TabIndex = 68;
            // 
            // chkPrintQCKemasan
            // 
            this.chkPrintQCKemasan.Location = new System.Drawing.Point(381, 381);
            this.chkPrintQCKemasan.Name = "chkPrintQCKemasan";
            this.chkPrintQCKemasan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintQCKemasan.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintQCKemasan.Properties.Appearance.Options.UseFont = true;
            this.chkPrintQCKemasan.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintQCKemasan.Properties.Caption = "Print";
            this.chkPrintQCKemasan.Size = new System.Drawing.Size(58, 20);
            this.chkPrintQCKemasan.TabIndex = 67;
            // 
            // chkDeleteQCKemasan
            // 
            this.chkDeleteQCKemasan.Location = new System.Drawing.Point(317, 381);
            this.chkDeleteQCKemasan.Name = "chkDeleteQCKemasan";
            this.chkDeleteQCKemasan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteQCKemasan.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteQCKemasan.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteQCKemasan.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteQCKemasan.Properties.Caption = "Delete";
            this.chkDeleteQCKemasan.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteQCKemasan.TabIndex = 66;
            // 
            // chkEditQCKemasan
            // 
            this.chkEditQCKemasan.Location = new System.Drawing.Point(253, 381);
            this.chkEditQCKemasan.Name = "chkEditQCKemasan";
            this.chkEditQCKemasan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditQCKemasan.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditQCKemasan.Properties.Appearance.Options.UseFont = true;
            this.chkEditQCKemasan.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditQCKemasan.Properties.Caption = "Edit";
            this.chkEditQCKemasan.Size = new System.Drawing.Size(58, 20);
            this.chkEditQCKemasan.TabIndex = 65;
            // 
            // chkNewQCKemasan
            // 
            this.chkNewQCKemasan.Location = new System.Drawing.Point(189, 381);
            this.chkNewQCKemasan.Name = "chkNewQCKemasan";
            this.chkNewQCKemasan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewQCKemasan.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewQCKemasan.Properties.Appearance.Options.UseFont = true;
            this.chkNewQCKemasan.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewQCKemasan.Properties.Caption = "New";
            this.chkNewQCKemasan.Size = new System.Drawing.Size(58, 20);
            this.chkNewQCKemasan.TabIndex = 64;
            // 
            // chkQCKemasan
            // 
            this.chkQCKemasan.Location = new System.Drawing.Point(18, 379);
            this.chkQCKemasan.Name = "chkQCKemasan";
            this.chkQCKemasan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQCKemasan.Properties.Appearance.Options.UseFont = true;
            this.chkQCKemasan.Properties.Caption = "QC Kemasan";
            this.chkQCKemasan.Size = new System.Drawing.Size(129, 22);
            this.chkQCKemasan.TabIndex = 63;
            // 
            // chkDeleteLabService
            // 
            this.chkDeleteLabService.Location = new System.Drawing.Point(317, 437);
            this.chkDeleteLabService.Name = "chkDeleteLabService";
            this.chkDeleteLabService.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteLabService.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteLabService.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteLabService.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteLabService.Properties.Caption = "Delete";
            this.chkDeleteLabService.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteLabService.TabIndex = 62;
            // 
            // chkEditLabService
            // 
            this.chkEditLabService.Location = new System.Drawing.Point(253, 437);
            this.chkEditLabService.Name = "chkEditLabService";
            this.chkEditLabService.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditLabService.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditLabService.Properties.Appearance.Options.UseFont = true;
            this.chkEditLabService.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditLabService.Properties.Caption = "Edit";
            this.chkEditLabService.Size = new System.Drawing.Size(58, 20);
            this.chkEditLabService.TabIndex = 61;
            // 
            // chkNewLabService
            // 
            this.chkNewLabService.Location = new System.Drawing.Point(189, 437);
            this.chkNewLabService.Name = "chkNewLabService";
            this.chkNewLabService.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewLabService.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewLabService.Properties.Appearance.Options.UseFont = true;
            this.chkNewLabService.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewLabService.Properties.Caption = "New";
            this.chkNewLabService.Size = new System.Drawing.Size(58, 20);
            this.chkNewLabService.TabIndex = 60;
            // 
            // chkLabService
            // 
            this.chkLabService.Location = new System.Drawing.Point(18, 435);
            this.chkLabService.Name = "chkLabService";
            this.chkLabService.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkLabService.Properties.Appearance.Options.UseFont = true;
            this.chkLabService.Properties.Caption = "Lab Service";
            this.chkLabService.Size = new System.Drawing.Size(129, 22);
            this.chkLabService.TabIndex = 59;
            // 
            // chkDeleteStandardDeliveryTime
            // 
            this.chkDeleteStandardDeliveryTime.Location = new System.Drawing.Point(317, 409);
            this.chkDeleteStandardDeliveryTime.Name = "chkDeleteStandardDeliveryTime";
            this.chkDeleteStandardDeliveryTime.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteStandardDeliveryTime.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteStandardDeliveryTime.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteStandardDeliveryTime.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteStandardDeliveryTime.Properties.Caption = "Delete";
            this.chkDeleteStandardDeliveryTime.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteStandardDeliveryTime.TabIndex = 58;
            // 
            // chkEditStandardDeliveryTime
            // 
            this.chkEditStandardDeliveryTime.Location = new System.Drawing.Point(253, 409);
            this.chkEditStandardDeliveryTime.Name = "chkEditStandardDeliveryTime";
            this.chkEditStandardDeliveryTime.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditStandardDeliveryTime.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditStandardDeliveryTime.Properties.Appearance.Options.UseFont = true;
            this.chkEditStandardDeliveryTime.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditStandardDeliveryTime.Properties.Caption = "Edit";
            this.chkEditStandardDeliveryTime.Size = new System.Drawing.Size(58, 20);
            this.chkEditStandardDeliveryTime.TabIndex = 57;
            // 
            // chkNewStandardDeliveryTime
            // 
            this.chkNewStandardDeliveryTime.Location = new System.Drawing.Point(189, 409);
            this.chkNewStandardDeliveryTime.Name = "chkNewStandardDeliveryTime";
            this.chkNewStandardDeliveryTime.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewStandardDeliveryTime.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewStandardDeliveryTime.Properties.Appearance.Options.UseFont = true;
            this.chkNewStandardDeliveryTime.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewStandardDeliveryTime.Properties.Caption = "New";
            this.chkNewStandardDeliveryTime.Size = new System.Drawing.Size(58, 20);
            this.chkNewStandardDeliveryTime.TabIndex = 56;
            // 
            // chkStandardDeliveryTime
            // 
            this.chkStandardDeliveryTime.Location = new System.Drawing.Point(18, 407);
            this.chkStandardDeliveryTime.Name = "chkStandardDeliveryTime";
            this.chkStandardDeliveryTime.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkStandardDeliveryTime.Properties.Appearance.Options.UseFont = true;
            this.chkStandardDeliveryTime.Properties.Caption = "Standard Delivery Time";
            this.chkStandardDeliveryTime.Size = new System.Drawing.Size(167, 22);
            this.chkStandardDeliveryTime.TabIndex = 55;
            // 
            // chkPrintQCPacking
            // 
            this.chkPrintQCPacking.Location = new System.Drawing.Point(381, 353);
            this.chkPrintQCPacking.Name = "chkPrintQCPacking";
            this.chkPrintQCPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintQCPacking.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintQCPacking.Properties.Appearance.Options.UseFont = true;
            this.chkPrintQCPacking.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintQCPacking.Properties.Caption = "Print";
            this.chkPrintQCPacking.Size = new System.Drawing.Size(58, 20);
            this.chkPrintQCPacking.TabIndex = 54;
            // 
            // chkDeleteQCPacking
            // 
            this.chkDeleteQCPacking.Location = new System.Drawing.Point(317, 353);
            this.chkDeleteQCPacking.Name = "chkDeleteQCPacking";
            this.chkDeleteQCPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteQCPacking.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteQCPacking.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteQCPacking.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteQCPacking.Properties.Caption = "Delete";
            this.chkDeleteQCPacking.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteQCPacking.TabIndex = 53;
            // 
            // chkEditQCPacking
            // 
            this.chkEditQCPacking.Location = new System.Drawing.Point(253, 353);
            this.chkEditQCPacking.Name = "chkEditQCPacking";
            this.chkEditQCPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditQCPacking.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditQCPacking.Properties.Appearance.Options.UseFont = true;
            this.chkEditQCPacking.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditQCPacking.Properties.Caption = "Edit";
            this.chkEditQCPacking.Size = new System.Drawing.Size(58, 20);
            this.chkEditQCPacking.TabIndex = 52;
            // 
            // chkNewQCPacking
            // 
            this.chkNewQCPacking.Location = new System.Drawing.Point(189, 353);
            this.chkNewQCPacking.Name = "chkNewQCPacking";
            this.chkNewQCPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewQCPacking.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewQCPacking.Properties.Appearance.Options.UseFont = true;
            this.chkNewQCPacking.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewQCPacking.Properties.Caption = "New";
            this.chkNewQCPacking.Size = new System.Drawing.Size(58, 20);
            this.chkNewQCPacking.TabIndex = 51;
            // 
            // chkPrintQCBahanBaku
            // 
            this.chkPrintQCBahanBaku.Location = new System.Drawing.Point(381, 325);
            this.chkPrintQCBahanBaku.Name = "chkPrintQCBahanBaku";
            this.chkPrintQCBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintQCBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintQCBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkPrintQCBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintQCBahanBaku.Properties.Caption = "Print";
            this.chkPrintQCBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkPrintQCBahanBaku.TabIndex = 49;
            // 
            // chkDeleteQCBahanBaku
            // 
            this.chkDeleteQCBahanBaku.Location = new System.Drawing.Point(317, 325);
            this.chkDeleteQCBahanBaku.Name = "chkDeleteQCBahanBaku";
            this.chkDeleteQCBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteQCBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteQCBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteQCBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteQCBahanBaku.Properties.Caption = "Delete";
            this.chkDeleteQCBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteQCBahanBaku.TabIndex = 48;
            // 
            // chkEditQCBahanBaku
            // 
            this.chkEditQCBahanBaku.Location = new System.Drawing.Point(253, 325);
            this.chkEditQCBahanBaku.Name = "chkEditQCBahanBaku";
            this.chkEditQCBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditQCBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditQCBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkEditQCBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditQCBahanBaku.Properties.Caption = "Edit";
            this.chkEditQCBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkEditQCBahanBaku.TabIndex = 47;
            // 
            // chkNewQCBahanBaku
            // 
            this.chkNewQCBahanBaku.Location = new System.Drawing.Point(189, 325);
            this.chkNewQCBahanBaku.Name = "chkNewQCBahanBaku";
            this.chkNewQCBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewQCBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewQCBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkNewQCBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewQCBahanBaku.Properties.Caption = "New";
            this.chkNewQCBahanBaku.Size = new System.Drawing.Size(58, 20);
            this.chkNewQCBahanBaku.TabIndex = 46;
            // 
            // chkPrintQCBarangJadi
            // 
            this.chkPrintQCBarangJadi.Location = new System.Drawing.Point(381, 297);
            this.chkPrintQCBarangJadi.Name = "chkPrintQCBarangJadi";
            this.chkPrintQCBarangJadi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintQCBarangJadi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintQCBarangJadi.Properties.Appearance.Options.UseFont = true;
            this.chkPrintQCBarangJadi.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintQCBarangJadi.Properties.Caption = "Print";
            this.chkPrintQCBarangJadi.Size = new System.Drawing.Size(58, 20);
            this.chkPrintQCBarangJadi.TabIndex = 44;
            // 
            // chkDeleteQCBarangJadi
            // 
            this.chkDeleteQCBarangJadi.Location = new System.Drawing.Point(317, 297);
            this.chkDeleteQCBarangJadi.Name = "chkDeleteQCBarangJadi";
            this.chkDeleteQCBarangJadi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteQCBarangJadi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteQCBarangJadi.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteQCBarangJadi.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteQCBarangJadi.Properties.Caption = "Delete";
            this.chkDeleteQCBarangJadi.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteQCBarangJadi.TabIndex = 43;
            // 
            // chkEditQCBarangJadi
            // 
            this.chkEditQCBarangJadi.Location = new System.Drawing.Point(253, 297);
            this.chkEditQCBarangJadi.Name = "chkEditQCBarangJadi";
            this.chkEditQCBarangJadi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditQCBarangJadi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditQCBarangJadi.Properties.Appearance.Options.UseFont = true;
            this.chkEditQCBarangJadi.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditQCBarangJadi.Properties.Caption = "Edit";
            this.chkEditQCBarangJadi.Size = new System.Drawing.Size(58, 20);
            this.chkEditQCBarangJadi.TabIndex = 42;
            // 
            // chkNewQCBarangJadi
            // 
            this.chkNewQCBarangJadi.Location = new System.Drawing.Point(189, 297);
            this.chkNewQCBarangJadi.Name = "chkNewQCBarangJadi";
            this.chkNewQCBarangJadi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewQCBarangJadi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewQCBarangJadi.Properties.Appearance.Options.UseFont = true;
            this.chkNewQCBarangJadi.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewQCBarangJadi.Properties.Caption = "New";
            this.chkNewQCBarangJadi.Size = new System.Drawing.Size(58, 20);
            this.chkNewQCBarangJadi.TabIndex = 41;
            // 
            // chkDeleteTransferStock
            // 
            this.chkDeleteTransferStock.Location = new System.Drawing.Point(317, 241);
            this.chkDeleteTransferStock.Name = "chkDeleteTransferStock";
            this.chkDeleteTransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteTransferStock.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteTransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteTransferStock.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteTransferStock.Properties.Caption = "Delete";
            this.chkDeleteTransferStock.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteTransferStock.TabIndex = 38;
            // 
            // chkEditTransferStock
            // 
            this.chkEditTransferStock.Location = new System.Drawing.Point(253, 241);
            this.chkEditTransferStock.Name = "chkEditTransferStock";
            this.chkEditTransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditTransferStock.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditTransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkEditTransferStock.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditTransferStock.Properties.Caption = "Edit";
            this.chkEditTransferStock.Size = new System.Drawing.Size(58, 20);
            this.chkEditTransferStock.TabIndex = 37;
            // 
            // chkNewTransferStock
            // 
            this.chkNewTransferStock.Location = new System.Drawing.Point(189, 241);
            this.chkNewTransferStock.Name = "chkNewTransferStock";
            this.chkNewTransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewTransferStock.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewTransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkNewTransferStock.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewTransferStock.Properties.Caption = "New";
            this.chkNewTransferStock.Size = new System.Drawing.Size(58, 20);
            this.chkNewTransferStock.TabIndex = 36;
            // 
            // chkApprovePenyerahanProduksi
            // 
            this.chkApprovePenyerahanProduksi.Location = new System.Drawing.Point(445, 213);
            this.chkApprovePenyerahanProduksi.Name = "chkApprovePenyerahanProduksi";
            this.chkApprovePenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApprovePenyerahanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApprovePenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkApprovePenyerahanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkApprovePenyerahanProduksi.Properties.Caption = "Coor. Approve";
            this.chkApprovePenyerahanProduksi.Size = new System.Drawing.Size(110, 20);
            this.chkApprovePenyerahanProduksi.TabIndex = 34;
            // 
            // chkPrintPenyerahanProduksi
            // 
            this.chkPrintPenyerahanProduksi.Location = new System.Drawing.Point(381, 213);
            this.chkPrintPenyerahanProduksi.Name = "chkPrintPenyerahanProduksi";
            this.chkPrintPenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintPenyerahanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintPenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkPrintPenyerahanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintPenyerahanProduksi.Properties.Caption = "Print";
            this.chkPrintPenyerahanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkPrintPenyerahanProduksi.TabIndex = 33;
            // 
            // chkDeletePenyerahanProduksi
            // 
            this.chkDeletePenyerahanProduksi.Location = new System.Drawing.Point(317, 213);
            this.chkDeletePenyerahanProduksi.Name = "chkDeletePenyerahanProduksi";
            this.chkDeletePenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePenyerahanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePenyerahanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePenyerahanProduksi.Properties.Caption = "Delete";
            this.chkDeletePenyerahanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePenyerahanProduksi.TabIndex = 32;
            // 
            // chkEditPenyerahanProduksi
            // 
            this.chkEditPenyerahanProduksi.Location = new System.Drawing.Point(253, 213);
            this.chkEditPenyerahanProduksi.Name = "chkEditPenyerahanProduksi";
            this.chkEditPenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPenyerahanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkEditPenyerahanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPenyerahanProduksi.Properties.Caption = "Edit";
            this.chkEditPenyerahanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkEditPenyerahanProduksi.TabIndex = 31;
            // 
            // chkNewPenyerahanProduksi
            // 
            this.chkNewPenyerahanProduksi.Location = new System.Drawing.Point(189, 213);
            this.chkNewPenyerahanProduksi.Name = "chkNewPenyerahanProduksi";
            this.chkNewPenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPenyerahanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkNewPenyerahanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPenyerahanProduksi.Properties.Caption = "New";
            this.chkNewPenyerahanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkNewPenyerahanProduksi.TabIndex = 30;
            // 
            // chkSetDeposition
            // 
            this.chkSetDeposition.Location = new System.Drawing.Point(445, 185);
            this.chkSetDeposition.Name = "chkSetDeposition";
            this.chkSetDeposition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSetDeposition.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSetDeposition.Properties.Appearance.Options.UseFont = true;
            this.chkSetDeposition.Properties.Appearance.Options.UseForeColor = true;
            this.chkSetDeposition.Properties.Caption = "Set Deposition";
            this.chkSetDeposition.Size = new System.Drawing.Size(101, 20);
            this.chkSetDeposition.TabIndex = 28;
            // 
            // chkSolveCustomerVoice
            // 
            this.chkSolveCustomerVoice.Location = new System.Drawing.Point(381, 185);
            this.chkSolveCustomerVoice.Name = "chkSolveCustomerVoice";
            this.chkSolveCustomerVoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSolveCustomerVoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSolveCustomerVoice.Properties.Appearance.Options.UseFont = true;
            this.chkSolveCustomerVoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkSolveCustomerVoice.Properties.Caption = "Solve";
            this.chkSolveCustomerVoice.Size = new System.Drawing.Size(58, 20);
            this.chkSolveCustomerVoice.TabIndex = 27;
            // 
            // chkDeleteCustomerVoice
            // 
            this.chkDeleteCustomerVoice.Location = new System.Drawing.Point(317, 185);
            this.chkDeleteCustomerVoice.Name = "chkDeleteCustomerVoice";
            this.chkDeleteCustomerVoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteCustomerVoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteCustomerVoice.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteCustomerVoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteCustomerVoice.Properties.Caption = "Delete";
            this.chkDeleteCustomerVoice.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteCustomerVoice.TabIndex = 26;
            // 
            // chkEditCustomerVoice
            // 
            this.chkEditCustomerVoice.Location = new System.Drawing.Point(253, 185);
            this.chkEditCustomerVoice.Name = "chkEditCustomerVoice";
            this.chkEditCustomerVoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditCustomerVoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditCustomerVoice.Properties.Appearance.Options.UseFont = true;
            this.chkEditCustomerVoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditCustomerVoice.Properties.Caption = "Edit";
            this.chkEditCustomerVoice.Size = new System.Drawing.Size(58, 20);
            this.chkEditCustomerVoice.TabIndex = 25;
            // 
            // chkNewCustomerVoice
            // 
            this.chkNewCustomerVoice.Location = new System.Drawing.Point(189, 185);
            this.chkNewCustomerVoice.Name = "chkNewCustomerVoice";
            this.chkNewCustomerVoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewCustomerVoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewCustomerVoice.Properties.Appearance.Options.UseFont = true;
            this.chkNewCustomerVoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewCustomerVoice.Properties.Caption = "New";
            this.chkNewCustomerVoice.Size = new System.Drawing.Size(58, 20);
            this.chkNewCustomerVoice.TabIndex = 24;
            // 
            // chkDeleteExpense
            // 
            this.chkDeleteExpense.Location = new System.Drawing.Point(317, 157);
            this.chkDeleteExpense.Name = "chkDeleteExpense";
            this.chkDeleteExpense.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteExpense.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteExpense.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteExpense.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteExpense.Properties.Caption = "Delete";
            this.chkDeleteExpense.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteExpense.TabIndex = 22;
            // 
            // chkEditExpense
            // 
            this.chkEditExpense.Location = new System.Drawing.Point(253, 157);
            this.chkEditExpense.Name = "chkEditExpense";
            this.chkEditExpense.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditExpense.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditExpense.Properties.Appearance.Options.UseFont = true;
            this.chkEditExpense.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditExpense.Properties.Caption = "Edit";
            this.chkEditExpense.Size = new System.Drawing.Size(58, 20);
            this.chkEditExpense.TabIndex = 21;
            // 
            // chkNewExpense
            // 
            this.chkNewExpense.Location = new System.Drawing.Point(189, 157);
            this.chkNewExpense.Name = "chkNewExpense";
            this.chkNewExpense.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewExpense.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewExpense.Properties.Appearance.Options.UseFont = true;
            this.chkNewExpense.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewExpense.Properties.Caption = "New";
            this.chkNewExpense.Size = new System.Drawing.Size(58, 20);
            this.chkNewExpense.TabIndex = 20;
            // 
            // chkSetLocationDeliveryOrder
            // 
            this.chkSetLocationDeliveryOrder.Location = new System.Drawing.Point(445, 45);
            this.chkSetLocationDeliveryOrder.Name = "chkSetLocationDeliveryOrder";
            this.chkSetLocationDeliveryOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSetLocationDeliveryOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSetLocationDeliveryOrder.Properties.Appearance.Options.UseFont = true;
            this.chkSetLocationDeliveryOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkSetLocationDeliveryOrder.Properties.Caption = "Set Location";
            this.chkSetLocationDeliveryOrder.Size = new System.Drawing.Size(101, 20);
            this.chkSetLocationDeliveryOrder.TabIndex = 11;
            // 
            // chkPrintDeliveryOrder
            // 
            this.chkPrintDeliveryOrder.Location = new System.Drawing.Point(381, 45);
            this.chkPrintDeliveryOrder.Name = "chkPrintDeliveryOrder";
            this.chkPrintDeliveryOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintDeliveryOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintDeliveryOrder.Properties.Appearance.Options.UseFont = true;
            this.chkPrintDeliveryOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintDeliveryOrder.Properties.Caption = "Print";
            this.chkPrintDeliveryOrder.Size = new System.Drawing.Size(58, 20);
            this.chkPrintDeliveryOrder.TabIndex = 10;
            // 
            // chkDeleteDeliveryOrder
            // 
            this.chkDeleteDeliveryOrder.Location = new System.Drawing.Point(317, 45);
            this.chkDeleteDeliveryOrder.Name = "chkDeleteDeliveryOrder";
            this.chkDeleteDeliveryOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteDeliveryOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteDeliveryOrder.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteDeliveryOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteDeliveryOrder.Properties.Caption = "Delete";
            this.chkDeleteDeliveryOrder.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteDeliveryOrder.TabIndex = 9;
            // 
            // chkEditDeliveryOrder
            // 
            this.chkEditDeliveryOrder.Location = new System.Drawing.Point(253, 45);
            this.chkEditDeliveryOrder.Name = "chkEditDeliveryOrder";
            this.chkEditDeliveryOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditDeliveryOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditDeliveryOrder.Properties.Appearance.Options.UseFont = true;
            this.chkEditDeliveryOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditDeliveryOrder.Properties.Caption = "Edit";
            this.chkEditDeliveryOrder.Size = new System.Drawing.Size(58, 20);
            this.chkEditDeliveryOrder.TabIndex = 8;
            // 
            // chkNewDeliveryOrder
            // 
            this.chkNewDeliveryOrder.Location = new System.Drawing.Point(189, 45);
            this.chkNewDeliveryOrder.Name = "chkNewDeliveryOrder";
            this.chkNewDeliveryOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewDeliveryOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewDeliveryOrder.Properties.Appearance.Options.UseFont = true;
            this.chkNewDeliveryOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewDeliveryOrder.Properties.Caption = "New";
            this.chkNewDeliveryOrder.Size = new System.Drawing.Size(58, 20);
            this.chkNewDeliveryOrder.TabIndex = 7;
            // 
            // chkApproveSalesOrder
            // 
            this.chkApproveSalesOrder.Location = new System.Drawing.Point(445, 17);
            this.chkApproveSalesOrder.Name = "chkApproveSalesOrder";
            this.chkApproveSalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApproveSalesOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApproveSalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkApproveSalesOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkApproveSalesOrder.Properties.Caption = "SE Approve";
            this.chkApproveSalesOrder.Size = new System.Drawing.Size(89, 20);
            this.chkApproveSalesOrder.TabIndex = 5;
            // 
            // chkPrintSalesOrder
            // 
            this.chkPrintSalesOrder.Location = new System.Drawing.Point(381, 17);
            this.chkPrintSalesOrder.Name = "chkPrintSalesOrder";
            this.chkPrintSalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintSalesOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintSalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkPrintSalesOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintSalesOrder.Properties.Caption = "Print";
            this.chkPrintSalesOrder.Size = new System.Drawing.Size(58, 20);
            this.chkPrintSalesOrder.TabIndex = 4;
            // 
            // chkDeleteSalesOrder
            // 
            this.chkDeleteSalesOrder.Location = new System.Drawing.Point(317, 17);
            this.chkDeleteSalesOrder.Name = "chkDeleteSalesOrder";
            this.chkDeleteSalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteSalesOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteSalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteSalesOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteSalesOrder.Properties.Caption = "Delete";
            this.chkDeleteSalesOrder.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteSalesOrder.TabIndex = 3;
            // 
            // chkEditSalesOrder
            // 
            this.chkEditSalesOrder.Location = new System.Drawing.Point(253, 17);
            this.chkEditSalesOrder.Name = "chkEditSalesOrder";
            this.chkEditSalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditSalesOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditSalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkEditSalesOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditSalesOrder.Properties.Caption = "Edit";
            this.chkEditSalesOrder.Size = new System.Drawing.Size(58, 20);
            this.chkEditSalesOrder.TabIndex = 2;
            // 
            // chkNewSalesOrder
            // 
            this.chkNewSalesOrder.Location = new System.Drawing.Point(189, 17);
            this.chkNewSalesOrder.Name = "chkNewSalesOrder";
            this.chkNewSalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewSalesOrder.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewSalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkNewSalesOrder.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewSalesOrder.Properties.Caption = "New";
            this.chkNewSalesOrder.Size = new System.Drawing.Size(58, 20);
            this.chkNewSalesOrder.TabIndex = 1;
            // 
            // chkQCPacking
            // 
            this.chkQCPacking.Location = new System.Drawing.Point(18, 351);
            this.chkQCPacking.Name = "chkQCPacking";
            this.chkQCPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQCPacking.Properties.Appearance.Options.UseFont = true;
            this.chkQCPacking.Properties.Caption = "QC Packing";
            this.chkQCPacking.Size = new System.Drawing.Size(129, 22);
            this.chkQCPacking.TabIndex = 50;
            // 
            // chkQCBahanBaku
            // 
            this.chkQCBahanBaku.Location = new System.Drawing.Point(18, 323);
            this.chkQCBahanBaku.Name = "chkQCBahanBaku";
            this.chkQCBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQCBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkQCBahanBaku.Properties.Caption = "QC Bahan Baku";
            this.chkQCBahanBaku.Size = new System.Drawing.Size(129, 22);
            this.chkQCBahanBaku.TabIndex = 45;
            // 
            // chkQCBarangJadi
            // 
            this.chkQCBarangJadi.Location = new System.Drawing.Point(18, 295);
            this.chkQCBarangJadi.Name = "chkQCBarangJadi";
            this.chkQCBarangJadi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQCBarangJadi.Properties.Appearance.Options.UseFont = true;
            this.chkQCBarangJadi.Properties.Caption = "QC Barang Jadi";
            this.chkQCBarangJadi.Size = new System.Drawing.Size(129, 22);
            this.chkQCBarangJadi.TabIndex = 40;
            // 
            // chkWorkInProgress
            // 
            this.chkWorkInProgress.Location = new System.Drawing.Point(18, 267);
            this.chkWorkInProgress.Name = "chkWorkInProgress";
            this.chkWorkInProgress.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkWorkInProgress.Properties.Appearance.Options.UseFont = true;
            this.chkWorkInProgress.Properties.Caption = "Work In Progress";
            this.chkWorkInProgress.Size = new System.Drawing.Size(129, 22);
            this.chkWorkInProgress.TabIndex = 39;
            // 
            // chkTransferStock
            // 
            this.chkTransferStock.Location = new System.Drawing.Point(18, 239);
            this.chkTransferStock.Name = "chkTransferStock";
            this.chkTransferStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkTransferStock.Properties.Appearance.Options.UseFont = true;
            this.chkTransferStock.Properties.Caption = "Transfer Stock";
            this.chkTransferStock.Size = new System.Drawing.Size(117, 22);
            this.chkTransferStock.TabIndex = 35;
            // 
            // chkChangeDOLocation
            // 
            this.chkChangeDOLocation.Location = new System.Drawing.Point(18, 99);
            this.chkChangeDOLocation.Name = "chkChangeDOLocation";
            this.chkChangeDOLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkChangeDOLocation.Properties.Appearance.Options.UseFont = true;
            this.chkChangeDOLocation.Properties.Caption = "Change DO Location";
            this.chkChangeDOLocation.Size = new System.Drawing.Size(155, 22);
            this.chkChangeDOLocation.TabIndex = 17;
            // 
            // chkPenyerahanProduksi
            // 
            this.chkPenyerahanProduksi.Location = new System.Drawing.Point(18, 211);
            this.chkPenyerahanProduksi.Name = "chkPenyerahanProduksi";
            this.chkPenyerahanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPenyerahanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkPenyerahanProduksi.Properties.Caption = "Production Handover";
            this.chkPenyerahanProduksi.Size = new System.Drawing.Size(167, 22);
            this.chkPenyerahanProduksi.TabIndex = 29;
            // 
            // chkCustomerVoice
            // 
            this.chkCustomerVoice.Location = new System.Drawing.Point(18, 183);
            this.chkCustomerVoice.Name = "chkCustomerVoice";
            this.chkCustomerVoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerVoice.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerVoice.Properties.Caption = "Voice of Company";
            this.chkCustomerVoice.Size = new System.Drawing.Size(167, 22);
            this.chkCustomerVoice.TabIndex = 23;
            // 
            // chkPayInvoice
            // 
            this.chkPayInvoice.Location = new System.Drawing.Point(381, 73);
            this.chkPayInvoice.Name = "chkPayInvoice";
            this.chkPayInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPayInvoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPayInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkPayInvoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkPayInvoice.Properties.Caption = "Pay";
            this.chkPayInvoice.Size = new System.Drawing.Size(44, 20);
            this.chkPayInvoice.TabIndex = 16;
            // 
            // chkPrintInvoice
            // 
            this.chkPrintInvoice.Location = new System.Drawing.Point(317, 73);
            this.chkPrintInvoice.Name = "chkPrintInvoice";
            this.chkPrintInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintInvoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkPrintInvoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintInvoice.Properties.Caption = "Print";
            this.chkPrintInvoice.Size = new System.Drawing.Size(59, 20);
            this.chkPrintInvoice.TabIndex = 15;
            // 
            // chkCancelInvoice
            // 
            this.chkCancelInvoice.Location = new System.Drawing.Point(253, 73);
            this.chkCancelInvoice.Name = "chkCancelInvoice";
            this.chkCancelInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkCancelInvoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkCancelInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkCancelInvoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkCancelInvoice.Properties.Caption = "Cancel";
            this.chkCancelInvoice.Size = new System.Drawing.Size(73, 20);
            this.chkCancelInvoice.TabIndex = 14;
            // 
            // chkCreateInvoice
            // 
            this.chkCreateInvoice.Location = new System.Drawing.Point(189, 73);
            this.chkCreateInvoice.Name = "chkCreateInvoice";
            this.chkCreateInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkCreateInvoice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkCreateInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkCreateInvoice.Properties.Appearance.Options.UseForeColor = true;
            this.chkCreateInvoice.Properties.Caption = "Create";
            this.chkCreateInvoice.Size = new System.Drawing.Size(67, 20);
            this.chkCreateInvoice.TabIndex = 13;
            // 
            // chkExpense
            // 
            this.chkExpense.Location = new System.Drawing.Point(18, 155);
            this.chkExpense.Name = "chkExpense";
            this.chkExpense.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExpense.Properties.Appearance.Options.UseFont = true;
            this.chkExpense.Properties.Caption = "Expense";
            this.chkExpense.Size = new System.Drawing.Size(148, 22);
            this.chkExpense.TabIndex = 19;
            // 
            // chkStockAdjustment
            // 
            this.chkStockAdjustment.Location = new System.Drawing.Point(18, 127);
            this.chkStockAdjustment.Name = "chkStockAdjustment";
            this.chkStockAdjustment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkStockAdjustment.Properties.Appearance.Options.UseFont = true;
            this.chkStockAdjustment.Properties.Caption = "Stock Adjustment";
            this.chkStockAdjustment.Size = new System.Drawing.Size(129, 22);
            this.chkStockAdjustment.TabIndex = 18;
            // 
            // chkSalesInvoice
            // 
            this.chkSalesInvoice.Location = new System.Drawing.Point(18, 71);
            this.chkSalesInvoice.Name = "chkSalesInvoice";
            this.chkSalesInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesInvoice.Properties.Appearance.Options.UseFont = true;
            this.chkSalesInvoice.Properties.Caption = "Sales Invoice";
            this.chkSalesInvoice.Size = new System.Drawing.Size(117, 22);
            this.chkSalesInvoice.TabIndex = 12;
            // 
            // chkDeliveryOrder
            // 
            this.chkDeliveryOrder.Location = new System.Drawing.Point(18, 43);
            this.chkDeliveryOrder.Name = "chkDeliveryOrder";
            this.chkDeliveryOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDeliveryOrder.Properties.Appearance.Options.UseFont = true;
            this.chkDeliveryOrder.Properties.Caption = "Delivery Order";
            this.chkDeliveryOrder.Size = new System.Drawing.Size(117, 22);
            this.chkDeliveryOrder.TabIndex = 6;
            // 
            // chkSalesOrder
            // 
            this.chkSalesOrder.Location = new System.Drawing.Point(18, 15);
            this.chkSalesOrder.Name = "chkSalesOrder";
            this.chkSalesOrder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesOrder.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrder.Properties.Caption = "Sales Order";
            this.chkSalesOrder.Size = new System.Drawing.Size(117, 22);
            this.chkSalesOrder.TabIndex = 0;
            // 
            // tabTransaction2
            // 
            this.tabTransaction2.Controls.Add(this.chkViewAllProject);
            this.tabTransaction2.Controls.Add(this.chkActivePIM);
            this.tabTransaction2.Controls.Add(this.chkPositionPIM);
            this.tabTransaction2.Controls.Add(this.chkNationalityPIM);
            this.tabTransaction2.Controls.Add(this.chkClosePengembanganProduk);
            this.tabTransaction2.Controls.Add(this.chkProjectCategory);
            this.tabTransaction2.Controls.Add(this.chkSolveCustomerComplaint);
            this.tabTransaction2.Controls.Add(this.chkDeleteCustomerComplaint);
            this.tabTransaction2.Controls.Add(this.chkEditCustomerComplaint);
            this.tabTransaction2.Controls.Add(this.chkNewCustomerComplaint);
            this.tabTransaction2.Controls.Add(this.chkCustomerComplaint);
            this.tabTransaction2.Controls.Add(this.chkPrintPengembanganProduk);
            this.tabTransaction2.Controls.Add(this.chkViewPengembanganProduk);
            this.tabTransaction2.Controls.Add(this.chkDeletePengembanganProduk);
            this.tabTransaction2.Controls.Add(this.chkEditPengembanganProduk);
            this.tabTransaction2.Controls.Add(this.chkNewPengembanganProduk);
            this.tabTransaction2.Controls.Add(this.chkPengembanganProduk);
            this.tabTransaction2.Controls.Add(this.chkApprovePermintaanProduksi);
            this.tabTransaction2.Controls.Add(this.chkViewPermintaanProduksi);
            this.tabTransaction2.Controls.Add(this.chkDeletePermintaanProduksi);
            this.tabTransaction2.Controls.Add(this.chkEditPermintaanProduksi);
            this.tabTransaction2.Controls.Add(this.chkNewPermintaanProduksi);
            this.tabTransaction2.Controls.Add(this.chkPermintaanProduksi);
            this.tabTransaction2.Controls.Add(this.chkLeaveType);
            this.tabTransaction2.Controls.Add(this.chkViewLeave);
            this.tabTransaction2.Controls.Add(this.chkDeleteLeave);
            this.tabTransaction2.Controls.Add(this.chkEditLeave);
            this.tabTransaction2.Controls.Add(this.chkNewLeave);
            this.tabTransaction2.Controls.Add(this.chkLeave);
            this.tabTransaction2.Controls.Add(this.chkPayGrade);
            this.tabTransaction2.Controls.Add(this.chkViewPIM);
            this.tabTransaction2.Controls.Add(this.chkDeletePIM);
            this.tabTransaction2.Controls.Add(this.chkEditPIM);
            this.tabTransaction2.Controls.Add(this.chkNewPIM);
            this.tabTransaction2.Controls.Add(this.chkPIM);
            this.tabTransaction2.Controls.Add(this.chkCloseProject);
            this.tabTransaction2.Controls.Add(this.chkCreateProjectProgress);
            this.tabTransaction2.Controls.Add(this.chkViewProject);
            this.tabTransaction2.Controls.Add(this.chkDeleteProject);
            this.tabTransaction2.Controls.Add(this.chkEditProject);
            this.tabTransaction2.Controls.Add(this.chkNewProject);
            this.tabTransaction2.Controls.Add(this.chkProject);
            this.tabTransaction2.Controls.Add(this.chkNewInternalComplaintMenu);
            this.tabTransaction2.Controls.Add(this.chkSetDepositionInternalComplaint);
            this.tabTransaction2.Controls.Add(this.chkSolveInternalComplaint);
            this.tabTransaction2.Controls.Add(this.chkDeleteInternalComplaint);
            this.tabTransaction2.Controls.Add(this.chkEditInternalComplaint);
            this.tabTransaction2.Controls.Add(this.chkNewInternalComplaint);
            this.tabTransaction2.Controls.Add(this.chkInternalComplaint);
            this.tabTransaction2.Name = "tabTransaction2";
            this.tabTransaction2.Size = new System.Drawing.Size(717, 474);
            this.tabTransaction2.Text = "Transaction";
            // 
            // chkViewAllProject
            // 
            this.chkViewAllProject.Location = new System.Drawing.Point(189, 104);
            this.chkViewAllProject.Name = "chkViewAllProject";
            this.chkViewAllProject.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkViewAllProject.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkViewAllProject.Properties.Appearance.Options.UseFont = true;
            this.chkViewAllProject.Properties.Appearance.Options.UseForeColor = true;
            this.chkViewAllProject.Properties.Caption = "View All Project";
            this.chkViewAllProject.Size = new System.Drawing.Size(124, 20);
            this.chkViewAllProject.TabIndex = 77;
            // 
            // chkActivePIM
            // 
            this.chkActivePIM.Location = new System.Drawing.Point(364, 162);
            this.chkActivePIM.Name = "chkActivePIM";
            this.chkActivePIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkActivePIM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkActivePIM.Properties.Appearance.Options.UseFont = true;
            this.chkActivePIM.Properties.Appearance.Options.UseForeColor = true;
            this.chkActivePIM.Properties.Caption = "Active";
            this.chkActivePIM.Size = new System.Drawing.Size(75, 20);
            this.chkActivePIM.TabIndex = 76;
            // 
            // chkPositionPIM
            // 
            this.chkPositionPIM.Location = new System.Drawing.Point(283, 162);
            this.chkPositionPIM.Name = "chkPositionPIM";
            this.chkPositionPIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPositionPIM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPositionPIM.Properties.Appearance.Options.UseFont = true;
            this.chkPositionPIM.Properties.Appearance.Options.UseForeColor = true;
            this.chkPositionPIM.Properties.Caption = "Position";
            this.chkPositionPIM.Size = new System.Drawing.Size(75, 20);
            this.chkPositionPIM.TabIndex = 75;
            // 
            // chkNationalityPIM
            // 
            this.chkNationalityPIM.Location = new System.Drawing.Point(189, 162);
            this.chkNationalityPIM.Name = "chkNationalityPIM";
            this.chkNationalityPIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNationalityPIM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNationalityPIM.Properties.Appearance.Options.UseFont = true;
            this.chkNationalityPIM.Properties.Appearance.Options.UseForeColor = true;
            this.chkNationalityPIM.Properties.Caption = "Nationality";
            this.chkNationalityPIM.Size = new System.Drawing.Size(101, 20);
            this.chkNationalityPIM.TabIndex = 74;
            // 
            // chkClosePengembanganProduk
            // 
            this.chkClosePengembanganProduk.Location = new System.Drawing.Point(511, 246);
            this.chkClosePengembanganProduk.Name = "chkClosePengembanganProduk";
            this.chkClosePengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkClosePengembanganProduk.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkClosePengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.chkClosePengembanganProduk.Properties.Appearance.Options.UseForeColor = true;
            this.chkClosePengembanganProduk.Properties.Caption = "Close";
            this.chkClosePengembanganProduk.Size = new System.Drawing.Size(58, 20);
            this.chkClosePengembanganProduk.TabIndex = 73;
            // 
            // chkProjectCategory
            // 
            this.chkProjectCategory.Location = new System.Drawing.Point(628, 80);
            this.chkProjectCategory.Name = "chkProjectCategory";
            this.chkProjectCategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkProjectCategory.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkProjectCategory.Properties.Appearance.Options.UseFont = true;
            this.chkProjectCategory.Properties.Appearance.Options.UseForeColor = true;
            this.chkProjectCategory.Properties.Caption = "Category";
            this.chkProjectCategory.Size = new System.Drawing.Size(86, 20);
            this.chkProjectCategory.TabIndex = 72;
            // 
            // chkSolveCustomerComplaint
            // 
            this.chkSolveCustomerComplaint.Location = new System.Drawing.Point(384, 274);
            this.chkSolveCustomerComplaint.Name = "chkSolveCustomerComplaint";
            this.chkSolveCustomerComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSolveCustomerComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSolveCustomerComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkSolveCustomerComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkSolveCustomerComplaint.Properties.Caption = "Solve";
            this.chkSolveCustomerComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkSolveCustomerComplaint.TabIndex = 71;
            // 
            // chkDeleteCustomerComplaint
            // 
            this.chkDeleteCustomerComplaint.Location = new System.Drawing.Point(320, 274);
            this.chkDeleteCustomerComplaint.Name = "chkDeleteCustomerComplaint";
            this.chkDeleteCustomerComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteCustomerComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteCustomerComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteCustomerComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteCustomerComplaint.Properties.Caption = "Delete";
            this.chkDeleteCustomerComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteCustomerComplaint.TabIndex = 70;
            // 
            // chkEditCustomerComplaint
            // 
            this.chkEditCustomerComplaint.Location = new System.Drawing.Point(256, 274);
            this.chkEditCustomerComplaint.Name = "chkEditCustomerComplaint";
            this.chkEditCustomerComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditCustomerComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditCustomerComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkEditCustomerComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditCustomerComplaint.Properties.Caption = "Edit";
            this.chkEditCustomerComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkEditCustomerComplaint.TabIndex = 69;
            // 
            // chkNewCustomerComplaint
            // 
            this.chkNewCustomerComplaint.Location = new System.Drawing.Point(192, 274);
            this.chkNewCustomerComplaint.Name = "chkNewCustomerComplaint";
            this.chkNewCustomerComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewCustomerComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewCustomerComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkNewCustomerComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewCustomerComplaint.Properties.Caption = "New";
            this.chkNewCustomerComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkNewCustomerComplaint.TabIndex = 68;
            // 
            // chkCustomerComplaint
            // 
            this.chkCustomerComplaint.Location = new System.Drawing.Point(21, 272);
            this.chkCustomerComplaint.Name = "chkCustomerComplaint";
            this.chkCustomerComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerComplaint.Properties.Caption = "Customer Complaint";
            this.chkCustomerComplaint.Size = new System.Drawing.Size(165, 22);
            this.chkCustomerComplaint.TabIndex = 67;
            // 
            // chkPrintPengembanganProduk
            // 
            this.chkPrintPengembanganProduk.Location = new System.Drawing.Point(447, 246);
            this.chkPrintPengembanganProduk.Name = "chkPrintPengembanganProduk";
            this.chkPrintPengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintPengembanganProduk.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintPengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.chkPrintPengembanganProduk.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintPengembanganProduk.Properties.Caption = "Print";
            this.chkPrintPengembanganProduk.Size = new System.Drawing.Size(58, 20);
            this.chkPrintPengembanganProduk.TabIndex = 66;
            // 
            // chkViewPengembanganProduk
            // 
            this.chkViewPengembanganProduk.Location = new System.Drawing.Point(383, 246);
            this.chkViewPengembanganProduk.Name = "chkViewPengembanganProduk";
            this.chkViewPengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkViewPengembanganProduk.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkViewPengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.chkViewPengembanganProduk.Properties.Appearance.Options.UseForeColor = true;
            this.chkViewPengembanganProduk.Properties.Caption = "View";
            this.chkViewPengembanganProduk.Size = new System.Drawing.Size(58, 20);
            this.chkViewPengembanganProduk.TabIndex = 65;
            // 
            // chkDeletePengembanganProduk
            // 
            this.chkDeletePengembanganProduk.Location = new System.Drawing.Point(319, 246);
            this.chkDeletePengembanganProduk.Name = "chkDeletePengembanganProduk";
            this.chkDeletePengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePengembanganProduk.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePengembanganProduk.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePengembanganProduk.Properties.Caption = "Delete";
            this.chkDeletePengembanganProduk.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePengembanganProduk.TabIndex = 64;
            // 
            // chkEditPengembanganProduk
            // 
            this.chkEditPengembanganProduk.Location = new System.Drawing.Point(255, 246);
            this.chkEditPengembanganProduk.Name = "chkEditPengembanganProduk";
            this.chkEditPengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPengembanganProduk.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.chkEditPengembanganProduk.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPengembanganProduk.Properties.Caption = "Edit";
            this.chkEditPengembanganProduk.Size = new System.Drawing.Size(58, 20);
            this.chkEditPengembanganProduk.TabIndex = 63;
            // 
            // chkNewPengembanganProduk
            // 
            this.chkNewPengembanganProduk.Location = new System.Drawing.Point(191, 246);
            this.chkNewPengembanganProduk.Name = "chkNewPengembanganProduk";
            this.chkNewPengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPengembanganProduk.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.chkNewPengembanganProduk.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPengembanganProduk.Properties.Caption = "New";
            this.chkNewPengembanganProduk.Size = new System.Drawing.Size(58, 20);
            this.chkNewPengembanganProduk.TabIndex = 62;
            // 
            // chkPengembanganProduk
            // 
            this.chkPengembanganProduk.Location = new System.Drawing.Point(20, 244);
            this.chkPengembanganProduk.Name = "chkPengembanganProduk";
            this.chkPengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.chkPengembanganProduk.Properties.Caption = "Pengembangan Produk";
            this.chkPengembanganProduk.Size = new System.Drawing.Size(165, 22);
            this.chkPengembanganProduk.TabIndex = 61;
            // 
            // chkApprovePermintaanProduksi
            // 
            this.chkApprovePermintaanProduksi.Location = new System.Drawing.Point(445, 218);
            this.chkApprovePermintaanProduksi.Name = "chkApprovePermintaanProduksi";
            this.chkApprovePermintaanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApprovePermintaanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApprovePermintaanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkApprovePermintaanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkApprovePermintaanProduksi.Properties.Caption = "Approve";
            this.chkApprovePermintaanProduksi.Size = new System.Drawing.Size(101, 20);
            this.chkApprovePermintaanProduksi.TabIndex = 60;
            // 
            // chkViewPermintaanProduksi
            // 
            this.chkViewPermintaanProduksi.Location = new System.Drawing.Point(381, 218);
            this.chkViewPermintaanProduksi.Name = "chkViewPermintaanProduksi";
            this.chkViewPermintaanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkViewPermintaanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkViewPermintaanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkViewPermintaanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkViewPermintaanProduksi.Properties.Caption = "View";
            this.chkViewPermintaanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkViewPermintaanProduksi.TabIndex = 59;
            // 
            // chkDeletePermintaanProduksi
            // 
            this.chkDeletePermintaanProduksi.Location = new System.Drawing.Point(317, 218);
            this.chkDeletePermintaanProduksi.Name = "chkDeletePermintaanProduksi";
            this.chkDeletePermintaanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePermintaanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePermintaanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePermintaanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePermintaanProduksi.Properties.Caption = "Delete";
            this.chkDeletePermintaanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePermintaanProduksi.TabIndex = 58;
            // 
            // chkEditPermintaanProduksi
            // 
            this.chkEditPermintaanProduksi.Location = new System.Drawing.Point(253, 218);
            this.chkEditPermintaanProduksi.Name = "chkEditPermintaanProduksi";
            this.chkEditPermintaanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPermintaanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPermintaanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkEditPermintaanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPermintaanProduksi.Properties.Caption = "Edit";
            this.chkEditPermintaanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkEditPermintaanProduksi.TabIndex = 57;
            // 
            // chkNewPermintaanProduksi
            // 
            this.chkNewPermintaanProduksi.Location = new System.Drawing.Point(189, 218);
            this.chkNewPermintaanProduksi.Name = "chkNewPermintaanProduksi";
            this.chkNewPermintaanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPermintaanProduksi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPermintaanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkNewPermintaanProduksi.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPermintaanProduksi.Properties.Caption = "New";
            this.chkNewPermintaanProduksi.Size = new System.Drawing.Size(58, 20);
            this.chkNewPermintaanProduksi.TabIndex = 56;
            // 
            // chkPermintaanProduksi
            // 
            this.chkPermintaanProduksi.Location = new System.Drawing.Point(18, 216);
            this.chkPermintaanProduksi.Name = "chkPermintaanProduksi";
            this.chkPermintaanProduksi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPermintaanProduksi.Properties.Appearance.Options.UseFont = true;
            this.chkPermintaanProduksi.Properties.Caption = "Permintaan Produksi";
            this.chkPermintaanProduksi.Size = new System.Drawing.Size(148, 22);
            this.chkPermintaanProduksi.TabIndex = 55;
            // 
            // chkLeaveType
            // 
            this.chkLeaveType.Location = new System.Drawing.Point(445, 190);
            this.chkLeaveType.Name = "chkLeaveType";
            this.chkLeaveType.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkLeaveType.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkLeaveType.Properties.Appearance.Options.UseFont = true;
            this.chkLeaveType.Properties.Appearance.Options.UseForeColor = true;
            this.chkLeaveType.Properties.Caption = "Leave Type";
            this.chkLeaveType.Size = new System.Drawing.Size(101, 20);
            this.chkLeaveType.TabIndex = 54;
            // 
            // chkViewLeave
            // 
            this.chkViewLeave.Location = new System.Drawing.Point(381, 190);
            this.chkViewLeave.Name = "chkViewLeave";
            this.chkViewLeave.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkViewLeave.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkViewLeave.Properties.Appearance.Options.UseFont = true;
            this.chkViewLeave.Properties.Appearance.Options.UseForeColor = true;
            this.chkViewLeave.Properties.Caption = "View";
            this.chkViewLeave.Size = new System.Drawing.Size(58, 20);
            this.chkViewLeave.TabIndex = 53;
            // 
            // chkDeleteLeave
            // 
            this.chkDeleteLeave.Location = new System.Drawing.Point(317, 190);
            this.chkDeleteLeave.Name = "chkDeleteLeave";
            this.chkDeleteLeave.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteLeave.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteLeave.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteLeave.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteLeave.Properties.Caption = "Delete";
            this.chkDeleteLeave.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteLeave.TabIndex = 52;
            // 
            // chkEditLeave
            // 
            this.chkEditLeave.Location = new System.Drawing.Point(253, 190);
            this.chkEditLeave.Name = "chkEditLeave";
            this.chkEditLeave.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditLeave.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditLeave.Properties.Appearance.Options.UseFont = true;
            this.chkEditLeave.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditLeave.Properties.Caption = "Edit";
            this.chkEditLeave.Size = new System.Drawing.Size(58, 20);
            this.chkEditLeave.TabIndex = 51;
            // 
            // chkNewLeave
            // 
            this.chkNewLeave.Location = new System.Drawing.Point(189, 190);
            this.chkNewLeave.Name = "chkNewLeave";
            this.chkNewLeave.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewLeave.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewLeave.Properties.Appearance.Options.UseFont = true;
            this.chkNewLeave.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewLeave.Properties.Caption = "New";
            this.chkNewLeave.Size = new System.Drawing.Size(58, 20);
            this.chkNewLeave.TabIndex = 50;
            // 
            // chkLeave
            // 
            this.chkLeave.Location = new System.Drawing.Point(18, 188);
            this.chkLeave.Name = "chkLeave";
            this.chkLeave.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkLeave.Properties.Appearance.Options.UseFont = true;
            this.chkLeave.Properties.Caption = "Leave";
            this.chkLeave.Size = new System.Drawing.Size(148, 22);
            this.chkLeave.TabIndex = 49;
            // 
            // chkPayGrade
            // 
            this.chkPayGrade.Location = new System.Drawing.Point(445, 136);
            this.chkPayGrade.Name = "chkPayGrade";
            this.chkPayGrade.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPayGrade.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPayGrade.Properties.Appearance.Options.UseFont = true;
            this.chkPayGrade.Properties.Appearance.Options.UseForeColor = true;
            this.chkPayGrade.Properties.Caption = "PayGrade";
            this.chkPayGrade.Size = new System.Drawing.Size(101, 20);
            this.chkPayGrade.TabIndex = 48;
            // 
            // chkViewPIM
            // 
            this.chkViewPIM.Location = new System.Drawing.Point(381, 136);
            this.chkViewPIM.Name = "chkViewPIM";
            this.chkViewPIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkViewPIM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkViewPIM.Properties.Appearance.Options.UseFont = true;
            this.chkViewPIM.Properties.Appearance.Options.UseForeColor = true;
            this.chkViewPIM.Properties.Caption = "View";
            this.chkViewPIM.Size = new System.Drawing.Size(58, 20);
            this.chkViewPIM.TabIndex = 47;
            // 
            // chkDeletePIM
            // 
            this.chkDeletePIM.Location = new System.Drawing.Point(317, 136);
            this.chkDeletePIM.Name = "chkDeletePIM";
            this.chkDeletePIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePIM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePIM.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePIM.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePIM.Properties.Caption = "Delete";
            this.chkDeletePIM.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePIM.TabIndex = 46;
            // 
            // chkEditPIM
            // 
            this.chkEditPIM.Location = new System.Drawing.Point(253, 136);
            this.chkEditPIM.Name = "chkEditPIM";
            this.chkEditPIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPIM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPIM.Properties.Appearance.Options.UseFont = true;
            this.chkEditPIM.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPIM.Properties.Caption = "Edit";
            this.chkEditPIM.Size = new System.Drawing.Size(58, 20);
            this.chkEditPIM.TabIndex = 45;
            // 
            // chkNewPIM
            // 
            this.chkNewPIM.Location = new System.Drawing.Point(189, 136);
            this.chkNewPIM.Name = "chkNewPIM";
            this.chkNewPIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPIM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPIM.Properties.Appearance.Options.UseFont = true;
            this.chkNewPIM.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPIM.Properties.Caption = "New";
            this.chkNewPIM.Size = new System.Drawing.Size(58, 20);
            this.chkNewPIM.TabIndex = 44;
            // 
            // chkPIM
            // 
            this.chkPIM.Location = new System.Drawing.Point(18, 134);
            this.chkPIM.Name = "chkPIM";
            this.chkPIM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPIM.Properties.Appearance.Options.UseFont = true;
            this.chkPIM.Properties.Caption = "PIM";
            this.chkPIM.Size = new System.Drawing.Size(148, 22);
            this.chkPIM.TabIndex = 43;
            // 
            // chkCloseProject
            // 
            this.chkCloseProject.Location = new System.Drawing.Point(565, 80);
            this.chkCloseProject.Name = "chkCloseProject";
            this.chkCloseProject.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkCloseProject.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkCloseProject.Properties.Appearance.Options.UseFont = true;
            this.chkCloseProject.Properties.Appearance.Options.UseForeColor = true;
            this.chkCloseProject.Properties.Caption = "Close";
            this.chkCloseProject.Size = new System.Drawing.Size(58, 20);
            this.chkCloseProject.TabIndex = 42;
            // 
            // chkCreateProjectProgress
            // 
            this.chkCreateProjectProgress.Location = new System.Drawing.Point(445, 80);
            this.chkCreateProjectProgress.Name = "chkCreateProjectProgress";
            this.chkCreateProjectProgress.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkCreateProjectProgress.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkCreateProjectProgress.Properties.Appearance.Options.UseFont = true;
            this.chkCreateProjectProgress.Properties.Appearance.Options.UseForeColor = true;
            this.chkCreateProjectProgress.Properties.Caption = "Create Progress";
            this.chkCreateProjectProgress.Size = new System.Drawing.Size(114, 20);
            this.chkCreateProjectProgress.TabIndex = 41;
            // 
            // chkViewProject
            // 
            this.chkViewProject.Location = new System.Drawing.Point(381, 78);
            this.chkViewProject.Name = "chkViewProject";
            this.chkViewProject.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkViewProject.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkViewProject.Properties.Appearance.Options.UseFont = true;
            this.chkViewProject.Properties.Appearance.Options.UseForeColor = true;
            this.chkViewProject.Properties.Caption = "View";
            this.chkViewProject.Size = new System.Drawing.Size(58, 20);
            this.chkViewProject.TabIndex = 40;
            // 
            // chkDeleteProject
            // 
            this.chkDeleteProject.Location = new System.Drawing.Point(317, 78);
            this.chkDeleteProject.Name = "chkDeleteProject";
            this.chkDeleteProject.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteProject.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteProject.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteProject.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteProject.Properties.Caption = "Delete";
            this.chkDeleteProject.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteProject.TabIndex = 39;
            // 
            // chkEditProject
            // 
            this.chkEditProject.Location = new System.Drawing.Point(253, 78);
            this.chkEditProject.Name = "chkEditProject";
            this.chkEditProject.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditProject.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditProject.Properties.Appearance.Options.UseFont = true;
            this.chkEditProject.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditProject.Properties.Caption = "Edit";
            this.chkEditProject.Size = new System.Drawing.Size(58, 20);
            this.chkEditProject.TabIndex = 38;
            // 
            // chkNewProject
            // 
            this.chkNewProject.Location = new System.Drawing.Point(189, 78);
            this.chkNewProject.Name = "chkNewProject";
            this.chkNewProject.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewProject.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewProject.Properties.Appearance.Options.UseFont = true;
            this.chkNewProject.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewProject.Properties.Caption = "New";
            this.chkNewProject.Size = new System.Drawing.Size(58, 20);
            this.chkNewProject.TabIndex = 37;
            // 
            // chkProject
            // 
            this.chkProject.Location = new System.Drawing.Point(18, 76);
            this.chkProject.Name = "chkProject";
            this.chkProject.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkProject.Properties.Appearance.Options.UseFont = true;
            this.chkProject.Properties.Caption = "Project";
            this.chkProject.Size = new System.Drawing.Size(167, 22);
            this.chkProject.TabIndex = 36;
            // 
            // chkNewInternalComplaintMenu
            // 
            this.chkNewInternalComplaintMenu.Location = new System.Drawing.Point(18, 48);
            this.chkNewInternalComplaintMenu.Name = "chkNewInternalComplaintMenu";
            this.chkNewInternalComplaintMenu.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkNewInternalComplaintMenu.Properties.Appearance.Options.UseFont = true;
            this.chkNewInternalComplaintMenu.Properties.Caption = "New Intenal Complaint";
            this.chkNewInternalComplaintMenu.Size = new System.Drawing.Size(167, 22);
            this.chkNewInternalComplaintMenu.TabIndex = 35;
            // 
            // chkSetDepositionInternalComplaint
            // 
            this.chkSetDepositionInternalComplaint.Location = new System.Drawing.Point(445, 22);
            this.chkSetDepositionInternalComplaint.Name = "chkSetDepositionInternalComplaint";
            this.chkSetDepositionInternalComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSetDepositionInternalComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSetDepositionInternalComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkSetDepositionInternalComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkSetDepositionInternalComplaint.Properties.Caption = "Set Deposition";
            this.chkSetDepositionInternalComplaint.Size = new System.Drawing.Size(101, 20);
            this.chkSetDepositionInternalComplaint.TabIndex = 34;
            // 
            // chkSolveInternalComplaint
            // 
            this.chkSolveInternalComplaint.Location = new System.Drawing.Point(381, 22);
            this.chkSolveInternalComplaint.Name = "chkSolveInternalComplaint";
            this.chkSolveInternalComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSolveInternalComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSolveInternalComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkSolveInternalComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkSolveInternalComplaint.Properties.Caption = "Solve";
            this.chkSolveInternalComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkSolveInternalComplaint.TabIndex = 33;
            // 
            // chkDeleteInternalComplaint
            // 
            this.chkDeleteInternalComplaint.Location = new System.Drawing.Point(317, 22);
            this.chkDeleteInternalComplaint.Name = "chkDeleteInternalComplaint";
            this.chkDeleteInternalComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteInternalComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteInternalComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteInternalComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteInternalComplaint.Properties.Caption = "Delete";
            this.chkDeleteInternalComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteInternalComplaint.TabIndex = 32;
            // 
            // chkEditInternalComplaint
            // 
            this.chkEditInternalComplaint.Location = new System.Drawing.Point(253, 22);
            this.chkEditInternalComplaint.Name = "chkEditInternalComplaint";
            this.chkEditInternalComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditInternalComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditInternalComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkEditInternalComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditInternalComplaint.Properties.Caption = "Edit";
            this.chkEditInternalComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkEditInternalComplaint.TabIndex = 31;
            // 
            // chkNewInternalComplaint
            // 
            this.chkNewInternalComplaint.Location = new System.Drawing.Point(189, 22);
            this.chkNewInternalComplaint.Name = "chkNewInternalComplaint";
            this.chkNewInternalComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewInternalComplaint.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewInternalComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkNewInternalComplaint.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewInternalComplaint.Properties.Caption = "New";
            this.chkNewInternalComplaint.Size = new System.Drawing.Size(58, 20);
            this.chkNewInternalComplaint.TabIndex = 30;
            // 
            // chkInternalComplaint
            // 
            this.chkInternalComplaint.Location = new System.Drawing.Point(18, 20);
            this.chkInternalComplaint.Name = "chkInternalComplaint";
            this.chkInternalComplaint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInternalComplaint.Properties.Appearance.Options.UseFont = true;
            this.chkInternalComplaint.Properties.Caption = "Intenal Complaint";
            this.chkInternalComplaint.Size = new System.Drawing.Size(167, 22);
            this.chkInternalComplaint.TabIndex = 29;
            // 
            // tabAutoNumber
            // 
            this.tabAutoNumber.Controls.Add(this.chkSetPONumberPPB);
            this.tabAutoNumber.Controls.Add(this.chkApprovePOtoSupplierExpedisi);
            this.tabAutoNumber.Controls.Add(this.chkApprovePOtoSupplierRegular);
            this.tabAutoNumber.Controls.Add(this.chkPrintPOtoSupplierExpedisi);
            this.tabAutoNumber.Controls.Add(this.chkDeletePOtoSupplierExpedisi);
            this.tabAutoNumber.Controls.Add(this.chkEditPOtoSupplierExpedisi);
            this.tabAutoNumber.Controls.Add(this.chkNewPOtoSupplierExpedisi);
            this.tabAutoNumber.Controls.Add(this.chkPOtoSupplierExpedisi);
            this.tabAutoNumber.Controls.Add(this.chkPrintPOtoSupplierRegular);
            this.tabAutoNumber.Controls.Add(this.chkDeletePOtoSupplierRegular);
            this.tabAutoNumber.Controls.Add(this.chkEditPOtoSupplierRegular);
            this.tabAutoNumber.Controls.Add(this.chkNewPOtoSupplierRegular);
            this.tabAutoNumber.Controls.Add(this.chkPOtoSupplierRegular);
            this.tabAutoNumber.Controls.Add(this.chkChangeDeliveryStatus);
            this.tabAutoNumber.Controls.Add(this.chkDeletePPS);
            this.tabAutoNumber.Controls.Add(this.chkEditPPS);
            this.tabAutoNumber.Controls.Add(this.chkNewPPS);
            this.tabAutoNumber.Controls.Add(this.chkDeletePPB);
            this.tabAutoNumber.Controls.Add(this.chkEditPPB);
            this.tabAutoNumber.Controls.Add(this.chkNewPPB);
            this.tabAutoNumber.Controls.Add(this.chkDeleteQuotationOut);
            this.tabAutoNumber.Controls.Add(this.chkEditQuotationOut);
            this.tabAutoNumber.Controls.Add(this.chkNewQuotationOut);
            this.tabAutoNumber.Controls.Add(this.chkDeleteQuotationIn);
            this.tabAutoNumber.Controls.Add(this.chkEditQuotationIn);
            this.tabAutoNumber.Controls.Add(this.chkNewQuotationIn);
            this.tabAutoNumber.Controls.Add(this.chkDeleteInquiryOut);
            this.tabAutoNumber.Controls.Add(this.chkEditInquiryOut);
            this.tabAutoNumber.Controls.Add(this.chkNewInquiryOut);
            this.tabAutoNumber.Controls.Add(this.chkDeleteInquiryIn);
            this.tabAutoNumber.Controls.Add(this.chkEditInquiryIn);
            this.tabAutoNumber.Controls.Add(this.chkNewInquiryIn);
            this.tabAutoNumber.Controls.Add(this.chkDeletePOtoSupplier);
            this.tabAutoNumber.Controls.Add(this.chkEditPOtoSupplier);
            this.tabAutoNumber.Controls.Add(this.chkNewPOtoSupplier);
            this.tabAutoNumber.Controls.Add(this.chkDeleteInternalLetter);
            this.tabAutoNumber.Controls.Add(this.chkEditInternalLetter);
            this.tabAutoNumber.Controls.Add(this.chkNewInternalLetter);
            this.tabAutoNumber.Controls.Add(this.chkDeleteExternalLetter);
            this.tabAutoNumber.Controls.Add(this.chkEditExternalLetter);
            this.tabAutoNumber.Controls.Add(this.chkNewExternalLetter);
            this.tabAutoNumber.Controls.Add(this.chkPOtoSupplier);
            this.tabAutoNumber.Controls.Add(this.chkInternalLetter);
            this.tabAutoNumber.Controls.Add(this.chkPPS);
            this.tabAutoNumber.Controls.Add(this.chkPPB);
            this.tabAutoNumber.Controls.Add(this.chkQuotationOut);
            this.tabAutoNumber.Controls.Add(this.chkQuotationIn);
            this.tabAutoNumber.Controls.Add(this.chkInquiryOut);
            this.tabAutoNumber.Controls.Add(this.chkInquiryIn);
            this.tabAutoNumber.Controls.Add(this.chkExternalLetter);
            this.tabAutoNumber.Name = "tabAutoNumber";
            this.tabAutoNumber.Size = new System.Drawing.Size(717, 474);
            this.tabAutoNumber.Text = "Auto Numbering";
            // 
            // chkSetPONumberPPB
            // 
            this.chkSetPONumberPPB.Location = new System.Drawing.Point(410, 286);
            this.chkSetPONumberPPB.Name = "chkSetPONumberPPB";
            this.chkSetPONumberPPB.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSetPONumberPPB.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSetPONumberPPB.Properties.Appearance.Options.UseFont = true;
            this.chkSetPONumberPPB.Properties.Appearance.Options.UseForeColor = true;
            this.chkSetPONumberPPB.Properties.Caption = "Set PO Number";
            this.chkSetPONumberPPB.Size = new System.Drawing.Size(133, 20);
            this.chkSetPONumberPPB.TabIndex = 49;
            // 
            // chkApprovePOtoSupplierExpedisi
            // 
            this.chkApprovePOtoSupplierExpedisi.Location = new System.Drawing.Point(474, 129);
            this.chkApprovePOtoSupplierExpedisi.Name = "chkApprovePOtoSupplierExpedisi";
            this.chkApprovePOtoSupplierExpedisi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApprovePOtoSupplierExpedisi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApprovePOtoSupplierExpedisi.Properties.Appearance.Options.UseFont = true;
            this.chkApprovePOtoSupplierExpedisi.Properties.Appearance.Options.UseForeColor = true;
            this.chkApprovePOtoSupplierExpedisi.Properties.Caption = "Approve";
            this.chkApprovePOtoSupplierExpedisi.Size = new System.Drawing.Size(79, 20);
            this.chkApprovePOtoSupplierExpedisi.TabIndex = 48;
            // 
            // chkApprovePOtoSupplierRegular
            // 
            this.chkApprovePOtoSupplierRegular.Location = new System.Drawing.Point(474, 101);
            this.chkApprovePOtoSupplierRegular.Name = "chkApprovePOtoSupplierRegular";
            this.chkApprovePOtoSupplierRegular.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkApprovePOtoSupplierRegular.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkApprovePOtoSupplierRegular.Properties.Appearance.Options.UseFont = true;
            this.chkApprovePOtoSupplierRegular.Properties.Appearance.Options.UseForeColor = true;
            this.chkApprovePOtoSupplierRegular.Properties.Caption = "Approve";
            this.chkApprovePOtoSupplierRegular.Size = new System.Drawing.Size(79, 20);
            this.chkApprovePOtoSupplierRegular.TabIndex = 47;
            // 
            // chkPrintPOtoSupplierExpedisi
            // 
            this.chkPrintPOtoSupplierExpedisi.Location = new System.Drawing.Point(410, 129);
            this.chkPrintPOtoSupplierExpedisi.Name = "chkPrintPOtoSupplierExpedisi";
            this.chkPrintPOtoSupplierExpedisi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintPOtoSupplierExpedisi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintPOtoSupplierExpedisi.Properties.Appearance.Options.UseFont = true;
            this.chkPrintPOtoSupplierExpedisi.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintPOtoSupplierExpedisi.Properties.Caption = "Print";
            this.chkPrintPOtoSupplierExpedisi.Size = new System.Drawing.Size(58, 20);
            this.chkPrintPOtoSupplierExpedisi.TabIndex = 46;
            // 
            // chkDeletePOtoSupplierExpedisi
            // 
            this.chkDeletePOtoSupplierExpedisi.Location = new System.Drawing.Point(336, 129);
            this.chkDeletePOtoSupplierExpedisi.Name = "chkDeletePOtoSupplierExpedisi";
            this.chkDeletePOtoSupplierExpedisi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePOtoSupplierExpedisi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePOtoSupplierExpedisi.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePOtoSupplierExpedisi.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePOtoSupplierExpedisi.Properties.Caption = "Delete";
            this.chkDeletePOtoSupplierExpedisi.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePOtoSupplierExpedisi.TabIndex = 45;
            // 
            // chkEditPOtoSupplierExpedisi
            // 
            this.chkEditPOtoSupplierExpedisi.Location = new System.Drawing.Point(272, 129);
            this.chkEditPOtoSupplierExpedisi.Name = "chkEditPOtoSupplierExpedisi";
            this.chkEditPOtoSupplierExpedisi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPOtoSupplierExpedisi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPOtoSupplierExpedisi.Properties.Appearance.Options.UseFont = true;
            this.chkEditPOtoSupplierExpedisi.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPOtoSupplierExpedisi.Properties.Caption = "Edit";
            this.chkEditPOtoSupplierExpedisi.Size = new System.Drawing.Size(58, 20);
            this.chkEditPOtoSupplierExpedisi.TabIndex = 44;
            // 
            // chkNewPOtoSupplierExpedisi
            // 
            this.chkNewPOtoSupplierExpedisi.Location = new System.Drawing.Point(208, 129);
            this.chkNewPOtoSupplierExpedisi.Name = "chkNewPOtoSupplierExpedisi";
            this.chkNewPOtoSupplierExpedisi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPOtoSupplierExpedisi.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPOtoSupplierExpedisi.Properties.Appearance.Options.UseFont = true;
            this.chkNewPOtoSupplierExpedisi.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPOtoSupplierExpedisi.Properties.Caption = "New";
            this.chkNewPOtoSupplierExpedisi.Size = new System.Drawing.Size(58, 20);
            this.chkNewPOtoSupplierExpedisi.TabIndex = 43;
            // 
            // chkPOtoSupplierExpedisi
            // 
            this.chkPOtoSupplierExpedisi.Location = new System.Drawing.Point(18, 127);
            this.chkPOtoSupplierExpedisi.Name = "chkPOtoSupplierExpedisi";
            this.chkPOtoSupplierExpedisi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPOtoSupplierExpedisi.Properties.Appearance.Options.UseFont = true;
            this.chkPOtoSupplierExpedisi.Properties.Caption = "PO to Supplier Expedisi";
            this.chkPOtoSupplierExpedisi.Size = new System.Drawing.Size(167, 22);
            this.chkPOtoSupplierExpedisi.TabIndex = 42;
            // 
            // chkPrintPOtoSupplierRegular
            // 
            this.chkPrintPOtoSupplierRegular.Location = new System.Drawing.Point(410, 101);
            this.chkPrintPOtoSupplierRegular.Name = "chkPrintPOtoSupplierRegular";
            this.chkPrintPOtoSupplierRegular.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPrintPOtoSupplierRegular.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPrintPOtoSupplierRegular.Properties.Appearance.Options.UseFont = true;
            this.chkPrintPOtoSupplierRegular.Properties.Appearance.Options.UseForeColor = true;
            this.chkPrintPOtoSupplierRegular.Properties.Caption = "Print";
            this.chkPrintPOtoSupplierRegular.Size = new System.Drawing.Size(58, 20);
            this.chkPrintPOtoSupplierRegular.TabIndex = 41;
            // 
            // chkDeletePOtoSupplierRegular
            // 
            this.chkDeletePOtoSupplierRegular.Location = new System.Drawing.Point(336, 101);
            this.chkDeletePOtoSupplierRegular.Name = "chkDeletePOtoSupplierRegular";
            this.chkDeletePOtoSupplierRegular.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePOtoSupplierRegular.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePOtoSupplierRegular.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePOtoSupplierRegular.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePOtoSupplierRegular.Properties.Caption = "Delete";
            this.chkDeletePOtoSupplierRegular.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePOtoSupplierRegular.TabIndex = 40;
            // 
            // chkEditPOtoSupplierRegular
            // 
            this.chkEditPOtoSupplierRegular.Location = new System.Drawing.Point(272, 101);
            this.chkEditPOtoSupplierRegular.Name = "chkEditPOtoSupplierRegular";
            this.chkEditPOtoSupplierRegular.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPOtoSupplierRegular.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPOtoSupplierRegular.Properties.Appearance.Options.UseFont = true;
            this.chkEditPOtoSupplierRegular.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPOtoSupplierRegular.Properties.Caption = "Edit";
            this.chkEditPOtoSupplierRegular.Size = new System.Drawing.Size(58, 20);
            this.chkEditPOtoSupplierRegular.TabIndex = 39;
            // 
            // chkNewPOtoSupplierRegular
            // 
            this.chkNewPOtoSupplierRegular.Location = new System.Drawing.Point(208, 101);
            this.chkNewPOtoSupplierRegular.Name = "chkNewPOtoSupplierRegular";
            this.chkNewPOtoSupplierRegular.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPOtoSupplierRegular.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPOtoSupplierRegular.Properties.Appearance.Options.UseFont = true;
            this.chkNewPOtoSupplierRegular.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPOtoSupplierRegular.Properties.Caption = "New";
            this.chkNewPOtoSupplierRegular.Size = new System.Drawing.Size(58, 20);
            this.chkNewPOtoSupplierRegular.TabIndex = 38;
            // 
            // chkPOtoSupplierRegular
            // 
            this.chkPOtoSupplierRegular.Location = new System.Drawing.Point(18, 99);
            this.chkPOtoSupplierRegular.Name = "chkPOtoSupplierRegular";
            this.chkPOtoSupplierRegular.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPOtoSupplierRegular.Properties.Appearance.Options.UseFont = true;
            this.chkPOtoSupplierRegular.Properties.Caption = "PO to Supplier Regular";
            this.chkPOtoSupplierRegular.Size = new System.Drawing.Size(167, 22);
            this.chkPOtoSupplierRegular.TabIndex = 37;
            // 
            // chkChangeDeliveryStatus
            // 
            this.chkChangeDeliveryStatus.Location = new System.Drawing.Point(410, 73);
            this.chkChangeDeliveryStatus.Name = "chkChangeDeliveryStatus";
            this.chkChangeDeliveryStatus.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkChangeDeliveryStatus.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkChangeDeliveryStatus.Properties.Appearance.Options.UseFont = true;
            this.chkChangeDeliveryStatus.Properties.Appearance.Options.UseForeColor = true;
            this.chkChangeDeliveryStatus.Properties.Caption = "Change Delivery Status";
            this.chkChangeDeliveryStatus.Size = new System.Drawing.Size(154, 20);
            this.chkChangeDeliveryStatus.TabIndex = 36;
            // 
            // chkDeletePPS
            // 
            this.chkDeletePPS.Location = new System.Drawing.Point(336, 314);
            this.chkDeletePPS.Name = "chkDeletePPS";
            this.chkDeletePPS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePPS.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePPS.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePPS.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePPS.Properties.Caption = "Delete";
            this.chkDeletePPS.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePPS.TabIndex = 35;
            // 
            // chkEditPPS
            // 
            this.chkEditPPS.Location = new System.Drawing.Point(272, 314);
            this.chkEditPPS.Name = "chkEditPPS";
            this.chkEditPPS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPPS.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPPS.Properties.Appearance.Options.UseFont = true;
            this.chkEditPPS.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPPS.Properties.Caption = "Edit";
            this.chkEditPPS.Size = new System.Drawing.Size(58, 20);
            this.chkEditPPS.TabIndex = 34;
            // 
            // chkNewPPS
            // 
            this.chkNewPPS.Location = new System.Drawing.Point(208, 314);
            this.chkNewPPS.Name = "chkNewPPS";
            this.chkNewPPS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPPS.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPPS.Properties.Appearance.Options.UseFont = true;
            this.chkNewPPS.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPPS.Properties.Caption = "New";
            this.chkNewPPS.Size = new System.Drawing.Size(58, 20);
            this.chkNewPPS.TabIndex = 33;
            // 
            // chkDeletePPB
            // 
            this.chkDeletePPB.Location = new System.Drawing.Point(336, 286);
            this.chkDeletePPB.Name = "chkDeletePPB";
            this.chkDeletePPB.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePPB.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePPB.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePPB.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePPB.Properties.Caption = "Delete";
            this.chkDeletePPB.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePPB.TabIndex = 31;
            // 
            // chkEditPPB
            // 
            this.chkEditPPB.Location = new System.Drawing.Point(272, 286);
            this.chkEditPPB.Name = "chkEditPPB";
            this.chkEditPPB.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPPB.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPPB.Properties.Appearance.Options.UseFont = true;
            this.chkEditPPB.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPPB.Properties.Caption = "Edit";
            this.chkEditPPB.Size = new System.Drawing.Size(58, 20);
            this.chkEditPPB.TabIndex = 30;
            // 
            // chkNewPPB
            // 
            this.chkNewPPB.Location = new System.Drawing.Point(208, 286);
            this.chkNewPPB.Name = "chkNewPPB";
            this.chkNewPPB.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPPB.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPPB.Properties.Appearance.Options.UseFont = true;
            this.chkNewPPB.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPPB.Properties.Caption = "New";
            this.chkNewPPB.Size = new System.Drawing.Size(58, 20);
            this.chkNewPPB.TabIndex = 29;
            // 
            // chkDeleteQuotationOut
            // 
            this.chkDeleteQuotationOut.Location = new System.Drawing.Point(336, 258);
            this.chkDeleteQuotationOut.Name = "chkDeleteQuotationOut";
            this.chkDeleteQuotationOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteQuotationOut.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteQuotationOut.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteQuotationOut.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteQuotationOut.Properties.Caption = "Delete";
            this.chkDeleteQuotationOut.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteQuotationOut.TabIndex = 27;
            // 
            // chkEditQuotationOut
            // 
            this.chkEditQuotationOut.Location = new System.Drawing.Point(272, 258);
            this.chkEditQuotationOut.Name = "chkEditQuotationOut";
            this.chkEditQuotationOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditQuotationOut.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditQuotationOut.Properties.Appearance.Options.UseFont = true;
            this.chkEditQuotationOut.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditQuotationOut.Properties.Caption = "Edit";
            this.chkEditQuotationOut.Size = new System.Drawing.Size(58, 20);
            this.chkEditQuotationOut.TabIndex = 26;
            // 
            // chkNewQuotationOut
            // 
            this.chkNewQuotationOut.Location = new System.Drawing.Point(208, 258);
            this.chkNewQuotationOut.Name = "chkNewQuotationOut";
            this.chkNewQuotationOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewQuotationOut.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewQuotationOut.Properties.Appearance.Options.UseFont = true;
            this.chkNewQuotationOut.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewQuotationOut.Properties.Caption = "New";
            this.chkNewQuotationOut.Size = new System.Drawing.Size(58, 20);
            this.chkNewQuotationOut.TabIndex = 25;
            // 
            // chkDeleteQuotationIn
            // 
            this.chkDeleteQuotationIn.Location = new System.Drawing.Point(336, 230);
            this.chkDeleteQuotationIn.Name = "chkDeleteQuotationIn";
            this.chkDeleteQuotationIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteQuotationIn.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteQuotationIn.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteQuotationIn.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteQuotationIn.Properties.Caption = "Delete";
            this.chkDeleteQuotationIn.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteQuotationIn.TabIndex = 23;
            // 
            // chkEditQuotationIn
            // 
            this.chkEditQuotationIn.Location = new System.Drawing.Point(272, 230);
            this.chkEditQuotationIn.Name = "chkEditQuotationIn";
            this.chkEditQuotationIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditQuotationIn.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditQuotationIn.Properties.Appearance.Options.UseFont = true;
            this.chkEditQuotationIn.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditQuotationIn.Properties.Caption = "Edit";
            this.chkEditQuotationIn.Size = new System.Drawing.Size(58, 20);
            this.chkEditQuotationIn.TabIndex = 22;
            // 
            // chkNewQuotationIn
            // 
            this.chkNewQuotationIn.Location = new System.Drawing.Point(208, 230);
            this.chkNewQuotationIn.Name = "chkNewQuotationIn";
            this.chkNewQuotationIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewQuotationIn.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewQuotationIn.Properties.Appearance.Options.UseFont = true;
            this.chkNewQuotationIn.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewQuotationIn.Properties.Caption = "New";
            this.chkNewQuotationIn.Size = new System.Drawing.Size(58, 20);
            this.chkNewQuotationIn.TabIndex = 21;
            // 
            // chkDeleteInquiryOut
            // 
            this.chkDeleteInquiryOut.Location = new System.Drawing.Point(336, 202);
            this.chkDeleteInquiryOut.Name = "chkDeleteInquiryOut";
            this.chkDeleteInquiryOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteInquiryOut.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteInquiryOut.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteInquiryOut.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteInquiryOut.Properties.Caption = "Delete";
            this.chkDeleteInquiryOut.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteInquiryOut.TabIndex = 19;
            // 
            // chkEditInquiryOut
            // 
            this.chkEditInquiryOut.Location = new System.Drawing.Point(272, 202);
            this.chkEditInquiryOut.Name = "chkEditInquiryOut";
            this.chkEditInquiryOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditInquiryOut.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditInquiryOut.Properties.Appearance.Options.UseFont = true;
            this.chkEditInquiryOut.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditInquiryOut.Properties.Caption = "Edit";
            this.chkEditInquiryOut.Size = new System.Drawing.Size(58, 20);
            this.chkEditInquiryOut.TabIndex = 18;
            // 
            // chkNewInquiryOut
            // 
            this.chkNewInquiryOut.Location = new System.Drawing.Point(208, 202);
            this.chkNewInquiryOut.Name = "chkNewInquiryOut";
            this.chkNewInquiryOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewInquiryOut.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewInquiryOut.Properties.Appearance.Options.UseFont = true;
            this.chkNewInquiryOut.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewInquiryOut.Properties.Caption = "New";
            this.chkNewInquiryOut.Size = new System.Drawing.Size(58, 20);
            this.chkNewInquiryOut.TabIndex = 17;
            // 
            // chkDeleteInquiryIn
            // 
            this.chkDeleteInquiryIn.Location = new System.Drawing.Point(336, 174);
            this.chkDeleteInquiryIn.Name = "chkDeleteInquiryIn";
            this.chkDeleteInquiryIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteInquiryIn.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteInquiryIn.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteInquiryIn.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteInquiryIn.Properties.Caption = "Delete";
            this.chkDeleteInquiryIn.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteInquiryIn.TabIndex = 15;
            // 
            // chkEditInquiryIn
            // 
            this.chkEditInquiryIn.Location = new System.Drawing.Point(272, 174);
            this.chkEditInquiryIn.Name = "chkEditInquiryIn";
            this.chkEditInquiryIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditInquiryIn.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditInquiryIn.Properties.Appearance.Options.UseFont = true;
            this.chkEditInquiryIn.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditInquiryIn.Properties.Caption = "Edit";
            this.chkEditInquiryIn.Size = new System.Drawing.Size(58, 20);
            this.chkEditInquiryIn.TabIndex = 14;
            // 
            // chkNewInquiryIn
            // 
            this.chkNewInquiryIn.Location = new System.Drawing.Point(208, 174);
            this.chkNewInquiryIn.Name = "chkNewInquiryIn";
            this.chkNewInquiryIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewInquiryIn.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewInquiryIn.Properties.Appearance.Options.UseFont = true;
            this.chkNewInquiryIn.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewInquiryIn.Properties.Caption = "New";
            this.chkNewInquiryIn.Size = new System.Drawing.Size(58, 20);
            this.chkNewInquiryIn.TabIndex = 13;
            // 
            // chkDeletePOtoSupplier
            // 
            this.chkDeletePOtoSupplier.Location = new System.Drawing.Point(336, 73);
            this.chkDeletePOtoSupplier.Name = "chkDeletePOtoSupplier";
            this.chkDeletePOtoSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeletePOtoSupplier.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeletePOtoSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkDeletePOtoSupplier.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeletePOtoSupplier.Properties.Caption = "Delete";
            this.chkDeletePOtoSupplier.Size = new System.Drawing.Size(58, 20);
            this.chkDeletePOtoSupplier.TabIndex = 11;
            // 
            // chkEditPOtoSupplier
            // 
            this.chkEditPOtoSupplier.Location = new System.Drawing.Point(272, 73);
            this.chkEditPOtoSupplier.Name = "chkEditPOtoSupplier";
            this.chkEditPOtoSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditPOtoSupplier.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditPOtoSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkEditPOtoSupplier.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditPOtoSupplier.Properties.Caption = "Edit";
            this.chkEditPOtoSupplier.Size = new System.Drawing.Size(58, 20);
            this.chkEditPOtoSupplier.TabIndex = 10;
            // 
            // chkNewPOtoSupplier
            // 
            this.chkNewPOtoSupplier.Location = new System.Drawing.Point(208, 73);
            this.chkNewPOtoSupplier.Name = "chkNewPOtoSupplier";
            this.chkNewPOtoSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewPOtoSupplier.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewPOtoSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkNewPOtoSupplier.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewPOtoSupplier.Properties.Caption = "New";
            this.chkNewPOtoSupplier.Size = new System.Drawing.Size(58, 20);
            this.chkNewPOtoSupplier.TabIndex = 9;
            // 
            // chkDeleteInternalLetter
            // 
            this.chkDeleteInternalLetter.Location = new System.Drawing.Point(336, 45);
            this.chkDeleteInternalLetter.Name = "chkDeleteInternalLetter";
            this.chkDeleteInternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteInternalLetter.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteInternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteInternalLetter.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteInternalLetter.Properties.Caption = "Delete";
            this.chkDeleteInternalLetter.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteInternalLetter.TabIndex = 7;
            // 
            // chkEditInternalLetter
            // 
            this.chkEditInternalLetter.Location = new System.Drawing.Point(272, 45);
            this.chkEditInternalLetter.Name = "chkEditInternalLetter";
            this.chkEditInternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditInternalLetter.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditInternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkEditInternalLetter.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditInternalLetter.Properties.Caption = "Edit";
            this.chkEditInternalLetter.Size = new System.Drawing.Size(58, 20);
            this.chkEditInternalLetter.TabIndex = 6;
            // 
            // chkNewInternalLetter
            // 
            this.chkNewInternalLetter.Location = new System.Drawing.Point(208, 45);
            this.chkNewInternalLetter.Name = "chkNewInternalLetter";
            this.chkNewInternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewInternalLetter.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewInternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkNewInternalLetter.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewInternalLetter.Properties.Caption = "New";
            this.chkNewInternalLetter.Size = new System.Drawing.Size(58, 20);
            this.chkNewInternalLetter.TabIndex = 5;
            // 
            // chkDeleteExternalLetter
            // 
            this.chkDeleteExternalLetter.Location = new System.Drawing.Point(336, 17);
            this.chkDeleteExternalLetter.Name = "chkDeleteExternalLetter";
            this.chkDeleteExternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeleteExternalLetter.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeleteExternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkDeleteExternalLetter.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeleteExternalLetter.Properties.Caption = "Delete";
            this.chkDeleteExternalLetter.Size = new System.Drawing.Size(58, 20);
            this.chkDeleteExternalLetter.TabIndex = 3;
            // 
            // chkEditExternalLetter
            // 
            this.chkEditExternalLetter.Location = new System.Drawing.Point(272, 17);
            this.chkEditExternalLetter.Name = "chkEditExternalLetter";
            this.chkEditExternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditExternalLetter.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditExternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkEditExternalLetter.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditExternalLetter.Properties.Caption = "Edit";
            this.chkEditExternalLetter.Size = new System.Drawing.Size(58, 20);
            this.chkEditExternalLetter.TabIndex = 2;
            // 
            // chkNewExternalLetter
            // 
            this.chkNewExternalLetter.Location = new System.Drawing.Point(208, 17);
            this.chkNewExternalLetter.Name = "chkNewExternalLetter";
            this.chkNewExternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkNewExternalLetter.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkNewExternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkNewExternalLetter.Properties.Appearance.Options.UseForeColor = true;
            this.chkNewExternalLetter.Properties.Caption = "New";
            this.chkNewExternalLetter.Size = new System.Drawing.Size(58, 20);
            this.chkNewExternalLetter.TabIndex = 1;
            // 
            // chkPOtoSupplier
            // 
            this.chkPOtoSupplier.Location = new System.Drawing.Point(18, 71);
            this.chkPOtoSupplier.Name = "chkPOtoSupplier";
            this.chkPOtoSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPOtoSupplier.Properties.Appearance.Options.UseFont = true;
            this.chkPOtoSupplier.Properties.Caption = "PO to Supplier";
            this.chkPOtoSupplier.Size = new System.Drawing.Size(129, 22);
            this.chkPOtoSupplier.TabIndex = 8;
            // 
            // chkInternalLetter
            // 
            this.chkInternalLetter.Location = new System.Drawing.Point(18, 43);
            this.chkInternalLetter.Name = "chkInternalLetter";
            this.chkInternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkInternalLetter.Properties.Caption = "Internal Letter";
            this.chkInternalLetter.Size = new System.Drawing.Size(129, 22);
            this.chkInternalLetter.TabIndex = 4;
            // 
            // chkPPS
            // 
            this.chkPPS.Location = new System.Drawing.Point(18, 312);
            this.chkPPS.Name = "chkPPS";
            this.chkPPS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPS.Properties.Appearance.Options.UseFont = true;
            this.chkPPS.Properties.Caption = "PPS";
            this.chkPPS.Size = new System.Drawing.Size(182, 22);
            this.chkPPS.TabIndex = 32;
            // 
            // chkPPB
            // 
            this.chkPPB.Location = new System.Drawing.Point(18, 284);
            this.chkPPB.Name = "chkPPB";
            this.chkPPB.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPB.Properties.Appearance.Options.UseFont = true;
            this.chkPPB.Properties.Caption = "PPB";
            this.chkPPB.Size = new System.Drawing.Size(182, 22);
            this.chkPPB.TabIndex = 28;
            // 
            // chkQuotationOut
            // 
            this.chkQuotationOut.Location = new System.Drawing.Point(18, 256);
            this.chkQuotationOut.Name = "chkQuotationOut";
            this.chkQuotationOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQuotationOut.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationOut.Properties.Caption = "Quotation Out (Customer)";
            this.chkQuotationOut.Size = new System.Drawing.Size(182, 22);
            this.chkQuotationOut.TabIndex = 24;
            // 
            // chkQuotationIn
            // 
            this.chkQuotationIn.Location = new System.Drawing.Point(18, 228);
            this.chkQuotationIn.Name = "chkQuotationIn";
            this.chkQuotationIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkQuotationIn.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationIn.Properties.Caption = "Quotation In (Supplier)";
            this.chkQuotationIn.Size = new System.Drawing.Size(158, 22);
            this.chkQuotationIn.TabIndex = 20;
            // 
            // chkInquiryOut
            // 
            this.chkInquiryOut.Location = new System.Drawing.Point(18, 200);
            this.chkInquiryOut.Name = "chkInquiryOut";
            this.chkInquiryOut.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInquiryOut.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryOut.Properties.Caption = "Inquiry Out (Supplier)";
            this.chkInquiryOut.Size = new System.Drawing.Size(150, 22);
            this.chkInquiryOut.TabIndex = 16;
            // 
            // chkInquiryIn
            // 
            this.chkInquiryIn.Location = new System.Drawing.Point(18, 172);
            this.chkInquiryIn.Name = "chkInquiryIn";
            this.chkInquiryIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkInquiryIn.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryIn.Properties.Caption = "RFQ";
            this.chkInquiryIn.Size = new System.Drawing.Size(150, 22);
            this.chkInquiryIn.TabIndex = 12;
            // 
            // chkExternalLetter
            // 
            this.chkExternalLetter.Location = new System.Drawing.Point(18, 15);
            this.chkExternalLetter.Name = "chkExternalLetter";
            this.chkExternalLetter.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExternalLetter.Properties.Appearance.Options.UseFont = true;
            this.chkExternalLetter.Properties.Caption = "External Letter";
            this.chkExternalLetter.Size = new System.Drawing.Size(129, 22);
            this.chkExternalLetter.TabIndex = 0;
            // 
            // tabReport
            // 
            this.tabReport.Controls.Add(this.chkPIMReport);
            this.tabReport.Controls.Add(this.chkCustomerVoiceReport);
            this.tabReport.Controls.Add(this.chkStockReport);
            this.tabReport.Controls.Add(this.chkPenyerahanProduksiReport);
            this.tabReport.Controls.Add(this.chkDeliveryOrderLocation);
            this.tabReport.Controls.Add(this.chkSalesOrderDetail);
            this.tabReport.Controls.Add(this.chkSalesInvoiceSummary);
            this.tabReport.Controls.Add(this.chkDeliveryOrderSummary);
            this.tabReport.Controls.Add(this.chkSalesOrderSummary);
            this.tabReport.Controls.Add(this.chkInquiryOutReport);
            this.tabReport.Controls.Add(this.chkInquiryInReport);
            this.tabReport.Controls.Add(this.chkQuotationOutReport);
            this.tabReport.Controls.Add(this.chkQuotationInReport);
            this.tabReport.Controls.Add(this.chkPOtoSupplierReport);
            this.tabReport.Controls.Add(this.chkInternalLetterReport);
            this.tabReport.Controls.Add(this.chkExternalLetterReport);
            this.tabReport.Controls.Add(this.chkPPSReport);
            this.tabReport.Controls.Add(this.chkPPBReport);
            this.tabReport.Controls.Add(this.chkCustomerComplaintReport);
            this.tabReport.Controls.Add(this.chkCustomerSatisfactionReport);
            this.tabReport.Controls.Add(this.chkExpenseReport);
            this.tabReport.Controls.Add(this.chkUserLogReport);
            this.tabReport.Controls.Add(this.chkAutoNumberReport);
            this.tabReport.Controls.Add(this.chkSalesOrderReport);
            this.tabReport.Name = "tabReport";
            this.tabReport.Size = new System.Drawing.Size(717, 474);
            this.tabReport.Text = "Report";
            // 
            // chkPIMReport
            // 
            this.chkPIMReport.Location = new System.Drawing.Point(21, 327);
            this.chkPIMReport.Name = "chkPIMReport";
            this.chkPIMReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPIMReport.Properties.Appearance.Options.UseFont = true;
            this.chkPIMReport.Properties.Caption = "PIM Report";
            this.chkPIMReport.Size = new System.Drawing.Size(175, 22);
            this.chkPIMReport.TabIndex = 23;
            // 
            // chkCustomerVoiceReport
            // 
            this.chkCustomerVoiceReport.Location = new System.Drawing.Point(21, 187);
            this.chkCustomerVoiceReport.Name = "chkCustomerVoiceReport";
            this.chkCustomerVoiceReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerVoiceReport.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerVoiceReport.Properties.Caption = "Cust. Voice Report";
            this.chkCustomerVoiceReport.Size = new System.Drawing.Size(175, 22);
            this.chkCustomerVoiceReport.TabIndex = 22;
            // 
            // chkStockReport
            // 
            this.chkStockReport.Location = new System.Drawing.Point(21, 299);
            this.chkStockReport.Name = "chkStockReport";
            this.chkStockReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkStockReport.Properties.Appearance.Options.UseFont = true;
            this.chkStockReport.Properties.Caption = "Stock Report";
            this.chkStockReport.Size = new System.Drawing.Size(175, 22);
            this.chkStockReport.TabIndex = 9;
            // 
            // chkPenyerahanProduksiReport
            // 
            this.chkPenyerahanProduksiReport.Location = new System.Drawing.Point(21, 271);
            this.chkPenyerahanProduksiReport.Name = "chkPenyerahanProduksiReport";
            this.chkPenyerahanProduksiReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPenyerahanProduksiReport.Properties.Appearance.Options.UseFont = true;
            this.chkPenyerahanProduksiReport.Properties.Caption = "Penyerahan Produksi Report";
            this.chkPenyerahanProduksiReport.Size = new System.Drawing.Size(202, 22);
            this.chkPenyerahanProduksiReport.TabIndex = 8;
            // 
            // chkDeliveryOrderLocation
            // 
            this.chkDeliveryOrderLocation.Location = new System.Drawing.Point(244, 131);
            this.chkDeliveryOrderLocation.Name = "chkDeliveryOrderLocation";
            this.chkDeliveryOrderLocation.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeliveryOrderLocation.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeliveryOrderLocation.Properties.Appearance.Options.UseFont = true;
            this.chkDeliveryOrderLocation.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeliveryOrderLocation.Properties.Caption = "Delivery Order Location";
            this.chkDeliveryOrderLocation.Size = new System.Drawing.Size(175, 20);
            this.chkDeliveryOrderLocation.TabIndex = 14;
            // 
            // chkSalesOrderDetail
            // 
            this.chkSalesOrderDetail.Location = new System.Drawing.Point(244, 103);
            this.chkSalesOrderDetail.Name = "chkSalesOrderDetail";
            this.chkSalesOrderDetail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSalesOrderDetail.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSalesOrderDetail.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrderDetail.Properties.Appearance.Options.UseForeColor = true;
            this.chkSalesOrderDetail.Properties.Caption = "Sales Order Detail";
            this.chkSalesOrderDetail.Size = new System.Drawing.Size(175, 20);
            this.chkSalesOrderDetail.TabIndex = 13;
            // 
            // chkSalesInvoiceSummary
            // 
            this.chkSalesInvoiceSummary.Location = new System.Drawing.Point(244, 75);
            this.chkSalesInvoiceSummary.Name = "chkSalesInvoiceSummary";
            this.chkSalesInvoiceSummary.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSalesInvoiceSummary.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSalesInvoiceSummary.Properties.Appearance.Options.UseFont = true;
            this.chkSalesInvoiceSummary.Properties.Appearance.Options.UseForeColor = true;
            this.chkSalesInvoiceSummary.Properties.Caption = "Sales Invoice Summary";
            this.chkSalesInvoiceSummary.Size = new System.Drawing.Size(175, 20);
            this.chkSalesInvoiceSummary.TabIndex = 12;
            // 
            // chkDeliveryOrderSummary
            // 
            this.chkDeliveryOrderSummary.Location = new System.Drawing.Point(244, 47);
            this.chkDeliveryOrderSummary.Name = "chkDeliveryOrderSummary";
            this.chkDeliveryOrderSummary.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkDeliveryOrderSummary.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkDeliveryOrderSummary.Properties.Appearance.Options.UseFont = true;
            this.chkDeliveryOrderSummary.Properties.Appearance.Options.UseForeColor = true;
            this.chkDeliveryOrderSummary.Properties.Caption = "Delivery Order Summary";
            this.chkDeliveryOrderSummary.Size = new System.Drawing.Size(175, 20);
            this.chkDeliveryOrderSummary.TabIndex = 11;
            // 
            // chkSalesOrderSummary
            // 
            this.chkSalesOrderSummary.Location = new System.Drawing.Point(244, 19);
            this.chkSalesOrderSummary.Name = "chkSalesOrderSummary";
            this.chkSalesOrderSummary.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkSalesOrderSummary.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkSalesOrderSummary.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrderSummary.Properties.Appearance.Options.UseForeColor = true;
            this.chkSalesOrderSummary.Properties.Caption = "Sales Order Summary";
            this.chkSalesOrderSummary.Size = new System.Drawing.Size(175, 20);
            this.chkSalesOrderSummary.TabIndex = 10;
            // 
            // chkInquiryOutReport
            // 
            this.chkInquiryOutReport.Location = new System.Drawing.Point(244, 327);
            this.chkInquiryOutReport.Name = "chkInquiryOutReport";
            this.chkInquiryOutReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkInquiryOutReport.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkInquiryOutReport.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryOutReport.Properties.Appearance.Options.UseForeColor = true;
            this.chkInquiryOutReport.Properties.Caption = "Inquiry Out Report";
            this.chkInquiryOutReport.Size = new System.Drawing.Size(175, 20);
            this.chkInquiryOutReport.TabIndex = 21;
            // 
            // chkInquiryInReport
            // 
            this.chkInquiryInReport.Location = new System.Drawing.Point(244, 299);
            this.chkInquiryInReport.Name = "chkInquiryInReport";
            this.chkInquiryInReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkInquiryInReport.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkInquiryInReport.Properties.Appearance.Options.UseFont = true;
            this.chkInquiryInReport.Properties.Appearance.Options.UseForeColor = true;
            this.chkInquiryInReport.Properties.Caption = "Inquiry In Report";
            this.chkInquiryInReport.Size = new System.Drawing.Size(175, 20);
            this.chkInquiryInReport.TabIndex = 20;
            // 
            // chkQuotationOutReport
            // 
            this.chkQuotationOutReport.Location = new System.Drawing.Point(244, 271);
            this.chkQuotationOutReport.Name = "chkQuotationOutReport";
            this.chkQuotationOutReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkQuotationOutReport.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkQuotationOutReport.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationOutReport.Properties.Appearance.Options.UseForeColor = true;
            this.chkQuotationOutReport.Properties.Caption = "Quotation Out Report";
            this.chkQuotationOutReport.Size = new System.Drawing.Size(175, 20);
            this.chkQuotationOutReport.TabIndex = 19;
            // 
            // chkQuotationInReport
            // 
            this.chkQuotationInReport.Location = new System.Drawing.Point(244, 243);
            this.chkQuotationInReport.Name = "chkQuotationInReport";
            this.chkQuotationInReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkQuotationInReport.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkQuotationInReport.Properties.Appearance.Options.UseFont = true;
            this.chkQuotationInReport.Properties.Appearance.Options.UseForeColor = true;
            this.chkQuotationInReport.Properties.Caption = "Quotation In Report";
            this.chkQuotationInReport.Size = new System.Drawing.Size(175, 20);
            this.chkQuotationInReport.TabIndex = 18;
            // 
            // chkPOtoSupplierReport
            // 
            this.chkPOtoSupplierReport.Location = new System.Drawing.Point(244, 215);
            this.chkPOtoSupplierReport.Name = "chkPOtoSupplierReport";
            this.chkPOtoSupplierReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkPOtoSupplierReport.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkPOtoSupplierReport.Properties.Appearance.Options.UseFont = true;
            this.chkPOtoSupplierReport.Properties.Appearance.Options.UseForeColor = true;
            this.chkPOtoSupplierReport.Properties.Caption = "PO to Supplier Report";
            this.chkPOtoSupplierReport.Size = new System.Drawing.Size(175, 20);
            this.chkPOtoSupplierReport.TabIndex = 17;
            // 
            // chkInternalLetterReport
            // 
            this.chkInternalLetterReport.Location = new System.Drawing.Point(244, 187);
            this.chkInternalLetterReport.Name = "chkInternalLetterReport";
            this.chkInternalLetterReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkInternalLetterReport.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkInternalLetterReport.Properties.Appearance.Options.UseFont = true;
            this.chkInternalLetterReport.Properties.Appearance.Options.UseForeColor = true;
            this.chkInternalLetterReport.Properties.Caption = "Internal Letter Report";
            this.chkInternalLetterReport.Size = new System.Drawing.Size(175, 20);
            this.chkInternalLetterReport.TabIndex = 16;
            // 
            // chkExternalLetterReport
            // 
            this.chkExternalLetterReport.Location = new System.Drawing.Point(244, 159);
            this.chkExternalLetterReport.Name = "chkExternalLetterReport";
            this.chkExternalLetterReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkExternalLetterReport.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkExternalLetterReport.Properties.Appearance.Options.UseFont = true;
            this.chkExternalLetterReport.Properties.Appearance.Options.UseForeColor = true;
            this.chkExternalLetterReport.Properties.Caption = "External Letter Report";
            this.chkExternalLetterReport.Size = new System.Drawing.Size(175, 20);
            this.chkExternalLetterReport.TabIndex = 15;
            // 
            // chkPPSReport
            // 
            this.chkPPSReport.Location = new System.Drawing.Point(21, 243);
            this.chkPPSReport.Name = "chkPPSReport";
            this.chkPPSReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPSReport.Properties.Appearance.Options.UseFont = true;
            this.chkPPSReport.Properties.Caption = "PPS Report";
            this.chkPPSReport.Size = new System.Drawing.Size(175, 22);
            this.chkPPSReport.TabIndex = 7;
            // 
            // chkPPBReport
            // 
            this.chkPPBReport.Location = new System.Drawing.Point(21, 215);
            this.chkPPBReport.Name = "chkPPBReport";
            this.chkPPBReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkPPBReport.Properties.Appearance.Options.UseFont = true;
            this.chkPPBReport.Properties.Caption = "PPB Report";
            this.chkPPBReport.Size = new System.Drawing.Size(175, 22);
            this.chkPPBReport.TabIndex = 6;
            // 
            // chkCustomerComplaintReport
            // 
            this.chkCustomerComplaintReport.Location = new System.Drawing.Point(18, 159);
            this.chkCustomerComplaintReport.Name = "chkCustomerComplaintReport";
            this.chkCustomerComplaintReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerComplaintReport.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerComplaintReport.Properties.Caption = "Cust.Complaint Report";
            this.chkCustomerComplaintReport.Size = new System.Drawing.Size(175, 22);
            this.chkCustomerComplaintReport.TabIndex = 5;
            // 
            // chkCustomerSatisfactionReport
            // 
            this.chkCustomerSatisfactionReport.Location = new System.Drawing.Point(18, 131);
            this.chkCustomerSatisfactionReport.Name = "chkCustomerSatisfactionReport";
            this.chkCustomerSatisfactionReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkCustomerSatisfactionReport.Properties.Appearance.Options.UseFont = true;
            this.chkCustomerSatisfactionReport.Properties.Caption = "Cust. Satisfaction Report";
            this.chkCustomerSatisfactionReport.Size = new System.Drawing.Size(175, 22);
            this.chkCustomerSatisfactionReport.TabIndex = 4;
            // 
            // chkExpenseReport
            // 
            this.chkExpenseReport.Location = new System.Drawing.Point(18, 75);
            this.chkExpenseReport.Name = "chkExpenseReport";
            this.chkExpenseReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkExpenseReport.Properties.Appearance.Options.UseFont = true;
            this.chkExpenseReport.Properties.Caption = "Expense Report";
            this.chkExpenseReport.Size = new System.Drawing.Size(154, 22);
            this.chkExpenseReport.TabIndex = 2;
            // 
            // chkUserLogReport
            // 
            this.chkUserLogReport.Location = new System.Drawing.Point(18, 103);
            this.chkUserLogReport.Name = "chkUserLogReport";
            this.chkUserLogReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkUserLogReport.Properties.Appearance.Options.UseFont = true;
            this.chkUserLogReport.Properties.Caption = "User Log Report";
            this.chkUserLogReport.Size = new System.Drawing.Size(154, 22);
            this.chkUserLogReport.TabIndex = 3;
            // 
            // chkAutoNumberReport
            // 
            this.chkAutoNumberReport.Location = new System.Drawing.Point(18, 47);
            this.chkAutoNumberReport.Name = "chkAutoNumberReport";
            this.chkAutoNumberReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAutoNumberReport.Properties.Appearance.Options.UseFont = true;
            this.chkAutoNumberReport.Properties.Caption = "Auto Number Report";
            this.chkAutoNumberReport.Size = new System.Drawing.Size(154, 22);
            this.chkAutoNumberReport.TabIndex = 1;
            // 
            // chkSalesOrderReport
            // 
            this.chkSalesOrderReport.Location = new System.Drawing.Point(18, 19);
            this.chkSalesOrderReport.Name = "chkSalesOrderReport";
            this.chkSalesOrderReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSalesOrderReport.Properties.Appearance.Options.UseFont = true;
            this.chkSalesOrderReport.Properties.Caption = "Sales Order Report";
            this.chkSalesOrderReport.Size = new System.Drawing.Size(154, 22);
            this.chkSalesOrderReport.TabIndex = 0;
            // 
            // tabOthers
            // 
            this.tabOthers.Controls.Add(this.chkSettingGudang);
            this.tabOthers.Controls.Add(this.chkDesignReport);
            this.tabOthers.Controls.Add(this.chkUser);
            this.tabOthers.Controls.Add(this.chkManager);
            this.tabOthers.Controls.Add(this.chkAdministrator);
            this.tabOthers.Controls.Add(this.chkConfiguration);
            this.tabOthers.Name = "tabOthers";
            this.tabOthers.Size = new System.Drawing.Size(717, 474);
            this.tabOthers.Text = "Others";
            // 
            // chkDesignReport
            // 
            this.chkDesignReport.Location = new System.Drawing.Point(21, 50);
            this.chkDesignReport.Name = "chkDesignReport";
            this.chkDesignReport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkDesignReport.Properties.Appearance.Options.UseFont = true;
            this.chkDesignReport.Properties.Caption = "Design Report";
            this.chkDesignReport.Size = new System.Drawing.Size(130, 22);
            this.chkDesignReport.TabIndex = 13;
            // 
            // chkUser
            // 
            this.chkUser.Location = new System.Drawing.Point(21, 139);
            this.chkUser.Name = "chkUser";
            this.chkUser.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkUser.Properties.Appearance.Options.UseFont = true;
            this.chkUser.Properties.Caption = "User";
            this.chkUser.Size = new System.Drawing.Size(165, 22);
            this.chkUser.TabIndex = 12;
            // 
            // chkManager
            // 
            this.chkManager.Location = new System.Drawing.Point(21, 111);
            this.chkManager.Name = "chkManager";
            this.chkManager.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkManager.Properties.Appearance.Options.UseFont = true;
            this.chkManager.Properties.Caption = "Manager";
            this.chkManager.Size = new System.Drawing.Size(165, 22);
            this.chkManager.TabIndex = 11;
            // 
            // chkAdministrator
            // 
            this.chkAdministrator.Location = new System.Drawing.Point(21, 83);
            this.chkAdministrator.Name = "chkAdministrator";
            this.chkAdministrator.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkAdministrator.Properties.Appearance.Options.UseFont = true;
            this.chkAdministrator.Properties.Caption = "Administrator";
            this.chkAdministrator.Size = new System.Drawing.Size(165, 22);
            this.chkAdministrator.TabIndex = 10;
            // 
            // chkConfiguration
            // 
            this.chkConfiguration.Location = new System.Drawing.Point(21, 22);
            this.chkConfiguration.Name = "chkConfiguration";
            this.chkConfiguration.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkConfiguration.Properties.Appearance.Options.UseFont = true;
            this.chkConfiguration.Properties.Caption = "Configuration";
            this.chkConfiguration.Size = new System.Drawing.Size(106, 22);
            this.chkConfiguration.TabIndex = 5;
            // 
            // chkEditSupplierBahanBaku
            // 
            this.chkEditSupplierBahanBaku.Location = new System.Drawing.Point(540, 74);
            this.chkEditSupplierBahanBaku.Name = "chkEditSupplierBahanBaku";
            this.chkEditSupplierBahanBaku.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.chkEditSupplierBahanBaku.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.chkEditSupplierBahanBaku.Properties.Appearance.Options.UseFont = true;
            this.chkEditSupplierBahanBaku.Properties.Appearance.Options.UseForeColor = true;
            this.chkEditSupplierBahanBaku.Properties.Caption = "Supplier";
            this.chkEditSupplierBahanBaku.Size = new System.Drawing.Size(75, 20);
            this.chkEditSupplierBahanBaku.TabIndex = 81;
            // 
            // chkSettingGudang
            // 
            this.chkSettingGudang.Location = new System.Drawing.Point(21, 167);
            this.chkSettingGudang.Name = "chkSettingGudang";
            this.chkSettingGudang.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.chkSettingGudang.Properties.Appearance.Options.UseFont = true;
            this.chkSettingGudang.Properties.Caption = "Setting Gudang";
            this.chkSettingGudang.Size = new System.Drawing.Size(165, 22);
            this.chkSettingGudang.TabIndex = 14;
            // 
            // frmNewUserAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 652);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.btnGroup);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.cmbGroup);
            this.Controls.Add(this.cmbSuDepartment);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.cmbDepartment);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gridControl);
            this.Name = "frmNewUserAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Account";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSuDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.tabData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkPriceList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSupplierSC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSupplierSC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSupplierSC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSupplierSC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplierSC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUpdatePL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactiveBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactiveBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactiveProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPackaging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTerms.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTipeSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFisikSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKategoriBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTipeBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFisikBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintContactList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecapSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecapBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecapBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteContactList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditContactList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewContactList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteUserAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditUserAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewUserAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteExpedition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditExpedition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewExpedition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSampleCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditStatusProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanPenolong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpedition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContactList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProduct.Properties)).EndInit();
            this.tabTransaction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkUpdateRemarksTransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintTransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusQCBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKesimpulanQCBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStatusQCBarangJadi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKesimpulanQCBarangJadi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChangeTSLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetDONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintLabService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprove2TransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApproveTransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprove2PenyerahanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprove2SalesOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCKemasan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteLabService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditLabService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewLabService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLabService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteStandardDeliveryTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditStandardDeliveryTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewStandardDeliveryTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStandardDeliveryTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintQCBarangJadi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQCBarangJadi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQCBarangJadi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQCBarangJadi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteTransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditTransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewTransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePenyerahanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPenyerahanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePenyerahanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPenyerahanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPenyerahanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetDeposition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSolveCustomerVoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteCustomerVoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditCustomerVoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewCustomerVoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteExpense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditExpense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewExpense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetLocationDeliveryOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintDeliveryOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteDeliveryOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditDeliveryOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewDeliveryOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApproveSalesOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintSalesOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteSalesOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSalesOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewSalesOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQCBarangJadi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWorkInProgress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTransferStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChangeDOLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPenyerahanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerVoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCancelInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStockAdjustment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrder.Properties)).EndInit();
            this.tabTransaction2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkViewAllProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActivePIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPositionPIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNationalityPIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkClosePengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProjectCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSolveCustomerComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteCustomerComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditCustomerComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewCustomerComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewPengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePermintaanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewPermintaanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePermintaanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPermintaanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPermintaanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPermintaanProduksi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLeaveType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewLeave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteLeave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditLeave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewLeave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLeave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPayGrade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewPIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPIM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCloseProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateProjectProgress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkViewProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInternalComplaintMenu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSetDepositionInternalComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSolveInternalComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInternalComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInternalComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInternalComplaint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalComplaint.Properties)).EndInit();
            this.tabAutoNumber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSetPONumberPPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePOtoSupplierExpedisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkApprovePOtoSupplierRegular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPOtoSupplierExpedisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePOtoSupplierExpedisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPOtoSupplierExpedisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPOtoSupplierExpedisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierExpedisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintPOtoSupplierRegular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePOtoSupplierRegular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPOtoSupplierRegular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPOtoSupplierRegular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierRegular.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChangeDeliveryStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQuotationOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQuotationOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQuotationOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteQuotationIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditQuotationIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewQuotationIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInquiryOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInquiryOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInquiryOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInquiryIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInquiryIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInquiryIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeletePOtoSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditPOtoSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewPOtoSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteInternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditInternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewInternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeleteExternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditExternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewExternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetter.Properties)).EndInit();
            this.tabReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkPIMReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerVoiceReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStockReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPenyerahanProduksiReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderDetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesInvoiceSummary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeliveryOrderSummary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderSummary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryOutReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInquiryInReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationOutReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkQuotationInReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPOtoSupplierReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInternalLetterReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExternalLetterReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPSReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPPBReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerComplaintReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomerSatisfactionReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpenseReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserLogReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoNumberReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesOrderReport.Properties)).EndInit();
            this.tabOthers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDesignReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdministrator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConfiguration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEditSupplierBahanBaku.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSettingGudang.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnGroup;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbGroup;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbSuDepartment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbDepartment;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtUserName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage tabData;
        private DevExpress.XtraEditors.CheckEdit chkSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkEditStatusProduct;
        private DevExpress.XtraEditors.CheckEdit chkEmployee;
        private DevExpress.XtraEditors.CheckEdit chkBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkCurrency;
        private DevExpress.XtraEditors.CheckEdit chkLocation;
        private DevExpress.XtraEditors.CheckEdit chkExpedition;
        private DevExpress.XtraEditors.CheckEdit chkContactList;
        private DevExpress.XtraEditors.CheckEdit chkDepartment;
        private DevExpress.XtraEditors.CheckEdit chkUserAccount;
        private DevExpress.XtraEditors.CheckEdit chkCustomer;
        private DevExpress.XtraEditors.CheckEdit chkSupplier;
        private DevExpress.XtraEditors.CheckEdit chkCategory;
        private DevExpress.XtraEditors.CheckEdit chkProduct;
        private DevExpress.XtraTab.XtraTabPage tabTransaction;
        private DevExpress.XtraEditors.CheckEdit chkPenyerahanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkCustomerVoice;
        private DevExpress.XtraEditors.CheckEdit chkPayInvoice;
        private DevExpress.XtraEditors.CheckEdit chkPrintInvoice;
        private DevExpress.XtraEditors.CheckEdit chkCancelInvoice;
        private DevExpress.XtraEditors.CheckEdit chkCreateInvoice;
        private DevExpress.XtraEditors.CheckEdit chkExpense;
        private DevExpress.XtraEditors.CheckEdit chkStockAdjustment;
        private DevExpress.XtraEditors.CheckEdit chkSalesInvoice;
        private DevExpress.XtraEditors.CheckEdit chkDeliveryOrder;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrder;
        private DevExpress.XtraTab.XtraTabPage tabReport;
        private DevExpress.XtraEditors.CheckEdit chkDeliveryOrderLocation;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrderDetail;
        private DevExpress.XtraEditors.CheckEdit chkSalesInvoiceSummary;
        private DevExpress.XtraEditors.CheckEdit chkDeliveryOrderSummary;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrderSummary;
        private DevExpress.XtraEditors.CheckEdit chkInquiryOutReport;
        private DevExpress.XtraEditors.CheckEdit chkInquiryInReport;
        private DevExpress.XtraEditors.CheckEdit chkQuotationOutReport;
        private DevExpress.XtraEditors.CheckEdit chkQuotationInReport;
        private DevExpress.XtraEditors.CheckEdit chkPOtoSupplierReport;
        private DevExpress.XtraEditors.CheckEdit chkInternalLetterReport;
        private DevExpress.XtraEditors.CheckEdit chkExternalLetterReport;
        private DevExpress.XtraEditors.CheckEdit chkPPSReport;
        private DevExpress.XtraEditors.CheckEdit chkPPBReport;
        private DevExpress.XtraEditors.CheckEdit chkCustomerComplaintReport;
        private DevExpress.XtraEditors.CheckEdit chkCustomerSatisfactionReport;
        private DevExpress.XtraEditors.CheckEdit chkExpenseReport;
        private DevExpress.XtraEditors.CheckEdit chkUserLogReport;
        private DevExpress.XtraEditors.CheckEdit chkAutoNumberReport;
        private DevExpress.XtraEditors.CheckEdit chkSalesOrderReport;
        private DevExpress.XtraTab.XtraTabPage tabOthers;
        private DevExpress.XtraEditors.CheckEdit chkUser;
        private DevExpress.XtraEditors.CheckEdit chkManager;
        private DevExpress.XtraEditors.CheckEdit chkAdministrator;
        private DevExpress.XtraEditors.CheckEdit chkConfiguration;
        private DevExpress.XtraEditors.CheckEdit chkDeleteProduct;
        private DevExpress.XtraEditors.CheckEdit chkEditProduct;
        private DevExpress.XtraEditors.CheckEdit chkNewProduct;
        private DevExpress.XtraEditors.CheckEdit chkDeleteContactList;
        private DevExpress.XtraEditors.CheckEdit chkEditContactList;
        private DevExpress.XtraEditors.CheckEdit chkNewContactList;
        private DevExpress.XtraEditors.CheckEdit chkDeleteUserAccount;
        private DevExpress.XtraEditors.CheckEdit chkEditUserAccount;
        private DevExpress.XtraEditors.CheckEdit chkNewUserAccount;
        private DevExpress.XtraEditors.CheckEdit chkDeleteLocation;
        private DevExpress.XtraEditors.CheckEdit chkEditLocation;
        private DevExpress.XtraEditors.CheckEdit chkNewLocation;
        private DevExpress.XtraEditors.CheckEdit chkDeleteExpedition;
        private DevExpress.XtraEditors.CheckEdit chkEditExpedition;
        private DevExpress.XtraEditors.CheckEdit chkNewExpedition;
        private DevExpress.XtraEditors.CheckEdit chkDeleteEmployee;
        private DevExpress.XtraEditors.CheckEdit chkEditEmployee;
        private DevExpress.XtraEditors.CheckEdit chkNewEmployee;
        private DevExpress.XtraEditors.CheckEdit chkDeleteCustomer;
        private DevExpress.XtraEditors.CheckEdit chkEditCustomer;
        private DevExpress.XtraEditors.CheckEdit chkNewCustomer;
        private DevExpress.XtraEditors.CheckEdit chkDeleteSupplier;
        private DevExpress.XtraEditors.CheckEdit chkEditSupplier;
        private DevExpress.XtraEditors.CheckEdit chkNewSupplier;
        private DevExpress.XtraEditors.CheckEdit chkDeleteDepartment;
        private DevExpress.XtraEditors.CheckEdit chkEditDepartment;
        private DevExpress.XtraEditors.CheckEdit chkNewDepartment;
        private DevExpress.XtraEditors.CheckEdit chkDeleteSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkEditSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkNewSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkDeleteBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkEditBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkNewBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkDeleteBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkEditBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkNewBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkChangeDOLocation;
        private DevExpress.XtraEditors.CheckEdit chkTransferStock;
        private DevExpress.XtraEditors.CheckEdit chkWorkInProgress;
        private DevExpress.XtraTab.XtraTabPage tabAutoNumber;
        private DevExpress.XtraEditors.CheckEdit chkPOtoSupplier;
        private DevExpress.XtraEditors.CheckEdit chkInternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkPPS;
        private DevExpress.XtraEditors.CheckEdit chkPPB;
        private DevExpress.XtraEditors.CheckEdit chkQuotationOut;
        private DevExpress.XtraEditors.CheckEdit chkQuotationIn;
        private DevExpress.XtraEditors.CheckEdit chkInquiryOut;
        private DevExpress.XtraEditors.CheckEdit chkInquiryIn;
        private DevExpress.XtraEditors.CheckEdit chkExternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkQCBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkQCBarangJadi;
        private DevExpress.XtraEditors.CheckEdit chkQCPacking;
        private DevExpress.XtraEditors.CheckEdit chkDeleteSalesOrder;
        private DevExpress.XtraEditors.CheckEdit chkEditSalesOrder;
        private DevExpress.XtraEditors.CheckEdit chkNewSalesOrder;
        private DevExpress.XtraEditors.CheckEdit chkPrintSalesOrder;
        private DevExpress.XtraEditors.CheckEdit chkPrintProduct;
        private DevExpress.XtraEditors.CheckEdit chkPrintCustomer;
        private DevExpress.XtraEditors.CheckEdit chkPrintSupplier;
        private DevExpress.XtraEditors.CheckEdit chkRecapSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkRecapBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkRecapBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkPrintContactList;
        private DevExpress.XtraEditors.CheckEdit chkApproveSalesOrder;
        private DevExpress.XtraEditors.CheckEdit chkPrintDeliveryOrder;
        private DevExpress.XtraEditors.CheckEdit chkDeleteDeliveryOrder;
        private DevExpress.XtraEditors.CheckEdit chkEditDeliveryOrder;
        private DevExpress.XtraEditors.CheckEdit chkNewDeliveryOrder;
        private DevExpress.XtraEditors.CheckEdit chkSetLocationDeliveryOrder;
        private DevExpress.XtraEditors.CheckEdit chkDeleteExpense;
        private DevExpress.XtraEditors.CheckEdit chkEditExpense;
        private DevExpress.XtraEditors.CheckEdit chkNewExpense;
        private DevExpress.XtraEditors.CheckEdit chkDeleteCustomerVoice;
        private DevExpress.XtraEditors.CheckEdit chkEditCustomerVoice;
        private DevExpress.XtraEditors.CheckEdit chkNewCustomerVoice;
        private DevExpress.XtraEditors.CheckEdit chkSetDeposition;
        private DevExpress.XtraEditors.CheckEdit chkSolveCustomerVoice;
        private DevExpress.XtraEditors.CheckEdit chkDeletePenyerahanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkEditPenyerahanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkNewPenyerahanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkApprovePenyerahanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkPrintPenyerahanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkDeleteTransferStock;
        private DevExpress.XtraEditors.CheckEdit chkEditTransferStock;
        private DevExpress.XtraEditors.CheckEdit chkNewTransferStock;
        private DevExpress.XtraEditors.CheckEdit chkPrintQCPacking;
        private DevExpress.XtraEditors.CheckEdit chkDeleteQCPacking;
        private DevExpress.XtraEditors.CheckEdit chkEditQCPacking;
        private DevExpress.XtraEditors.CheckEdit chkNewQCPacking;
        private DevExpress.XtraEditors.CheckEdit chkPrintQCBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkDeleteQCBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkEditQCBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkNewQCBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkPrintQCBarangJadi;
        private DevExpress.XtraEditors.CheckEdit chkDeleteQCBarangJadi;
        private DevExpress.XtraEditors.CheckEdit chkEditQCBarangJadi;
        private DevExpress.XtraEditors.CheckEdit chkNewQCBarangJadi;
        private DevExpress.XtraEditors.CheckEdit chkDeletePPS;
        private DevExpress.XtraEditors.CheckEdit chkEditPPS;
        private DevExpress.XtraEditors.CheckEdit chkNewPPS;
        private DevExpress.XtraEditors.CheckEdit chkDeletePPB;
        private DevExpress.XtraEditors.CheckEdit chkEditPPB;
        private DevExpress.XtraEditors.CheckEdit chkNewPPB;
        private DevExpress.XtraEditors.CheckEdit chkDeleteQuotationOut;
        private DevExpress.XtraEditors.CheckEdit chkEditQuotationOut;
        private DevExpress.XtraEditors.CheckEdit chkNewQuotationOut;
        private DevExpress.XtraEditors.CheckEdit chkDeleteQuotationIn;
        private DevExpress.XtraEditors.CheckEdit chkEditQuotationIn;
        private DevExpress.XtraEditors.CheckEdit chkNewQuotationIn;
        private DevExpress.XtraEditors.CheckEdit chkDeleteInquiryOut;
        private DevExpress.XtraEditors.CheckEdit chkEditInquiryOut;
        private DevExpress.XtraEditors.CheckEdit chkNewInquiryOut;
        private DevExpress.XtraEditors.CheckEdit chkDeleteInquiryIn;
        private DevExpress.XtraEditors.CheckEdit chkEditInquiryIn;
        private DevExpress.XtraEditors.CheckEdit chkNewInquiryIn;
        private DevExpress.XtraEditors.CheckEdit chkDeletePOtoSupplier;
        private DevExpress.XtraEditors.CheckEdit chkEditPOtoSupplier;
        private DevExpress.XtraEditors.CheckEdit chkNewPOtoSupplier;
        private DevExpress.XtraEditors.CheckEdit chkDeleteInternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkEditInternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkNewInternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkDeleteExternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkEditExternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkNewExternalLetter;
        private DevExpress.XtraEditors.CheckEdit chkPenyerahanProduksiReport;
        private DevExpress.XtraEditors.CheckEdit chkStockReport;
        private DevExpress.XtraEditors.CheckEdit chkDeleteLabService;
        private DevExpress.XtraEditors.CheckEdit chkEditLabService;
        private DevExpress.XtraEditors.CheckEdit chkNewLabService;
        private DevExpress.XtraEditors.CheckEdit chkLabService;
        private DevExpress.XtraEditors.CheckEdit chkDeleteStandardDeliveryTime;
        private DevExpress.XtraEditors.CheckEdit chkEditStandardDeliveryTime;
        private DevExpress.XtraEditors.CheckEdit chkNewStandardDeliveryTime;
        private DevExpress.XtraEditors.CheckEdit chkStandardDeliveryTime;
        private DevExpress.XtraEditors.CheckEdit chkFisikBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkTipeBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkStatusBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkKategoriBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkTipeSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkFisikSampleCode;
        private DevExpress.XtraEditors.CheckEdit chkChangeDeliveryStatus;
        private DevExpress.XtraEditors.CheckEdit chkCustomerVoiceReport;
        private DevExpress.XtraEditors.CheckEdit chkTerms;
        private DevExpress.XtraEditors.CheckEdit chkPrintQCKemasan;
        private DevExpress.XtraEditors.CheckEdit chkDeleteQCKemasan;
        private DevExpress.XtraEditors.CheckEdit chkEditQCKemasan;
        private DevExpress.XtraEditors.CheckEdit chkNewQCKemasan;
        private DevExpress.XtraEditors.CheckEdit chkQCKemasan;
        private DevExpress.XtraEditors.CheckEdit chkPackaging;
        private DevExpress.XtraEditors.CheckEdit chkPrintPOtoSupplierRegular;
        private DevExpress.XtraEditors.CheckEdit chkDeletePOtoSupplierRegular;
        private DevExpress.XtraEditors.CheckEdit chkEditPOtoSupplierRegular;
        private DevExpress.XtraEditors.CheckEdit chkNewPOtoSupplierRegular;
        private DevExpress.XtraEditors.CheckEdit chkPOtoSupplierRegular;
        private DevExpress.XtraEditors.CheckEdit chkPrintPOtoSupplierExpedisi;
        private DevExpress.XtraEditors.CheckEdit chkDeletePOtoSupplierExpedisi;
        private DevExpress.XtraEditors.CheckEdit chkEditPOtoSupplierExpedisi;
        private DevExpress.XtraEditors.CheckEdit chkNewPOtoSupplierExpedisi;
        private DevExpress.XtraEditors.CheckEdit chkPOtoSupplierExpedisi;
        private DevExpress.XtraEditors.CheckEdit chkDesignReport;
        private DevExpress.XtraTab.XtraTabPage tabTransaction2;
        private DevExpress.XtraEditors.CheckEdit chkSetDepositionInternalComplaint;
        private DevExpress.XtraEditors.CheckEdit chkSolveInternalComplaint;
        private DevExpress.XtraEditors.CheckEdit chkDeleteInternalComplaint;
        private DevExpress.XtraEditors.CheckEdit chkEditInternalComplaint;
        private DevExpress.XtraEditors.CheckEdit chkNewInternalComplaint;
        private DevExpress.XtraEditors.CheckEdit chkInternalComplaint;
        private DevExpress.XtraEditors.CheckEdit chkInactiveProduct;
        private DevExpress.XtraEditors.CheckEdit chkInactiveBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkInactiveBahanPenolong;
        private DevExpress.XtraEditors.CheckEdit chkApprove2SalesOrder;
        private DevExpress.XtraEditors.CheckEdit chkApprovePOtoSupplierExpedisi;
        private DevExpress.XtraEditors.CheckEdit chkApprovePOtoSupplierRegular;
        private DevExpress.XtraEditors.CheckEdit chkApprove2PenyerahanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkApprove2TransferStock;
        private DevExpress.XtraEditors.CheckEdit chkApproveTransferStock;
        private DevExpress.XtraEditors.CheckEdit chkUpdatePL;
        private DevExpress.XtraEditors.CheckEdit chkSetPONumberPPB;
        private DevExpress.XtraEditors.CheckEdit chkNewInternalComplaintMenu;
        private DevExpress.XtraEditors.CheckEdit chkSupplierSC;
        private DevExpress.XtraEditors.CheckEdit chkPrintSupplierSC;
        private DevExpress.XtraEditors.CheckEdit chkDeleteSupplierSC;
        private DevExpress.XtraEditors.CheckEdit chkEditSupplierSC;
        private DevExpress.XtraEditors.CheckEdit chkNewSupplierSC;
        private DevExpress.XtraEditors.CheckEdit chkPrintLabService;
        private DevExpress.XtraEditors.CheckEdit chkSetDONumber;
        private DevExpress.XtraEditors.CheckEdit chkCreateProjectProgress;
        private DevExpress.XtraEditors.CheckEdit chkViewProject;
        private DevExpress.XtraEditors.CheckEdit chkDeleteProject;
        private DevExpress.XtraEditors.CheckEdit chkEditProject;
        private DevExpress.XtraEditors.CheckEdit chkNewProject;
        private DevExpress.XtraEditors.CheckEdit chkProject;
        private DevExpress.XtraEditors.CheckEdit chkCloseProject;
        private DevExpress.XtraEditors.CheckEdit chkPIM;
        private DevExpress.XtraEditors.CheckEdit chkViewPIM;
        private DevExpress.XtraEditors.CheckEdit chkDeletePIM;
        private DevExpress.XtraEditors.CheckEdit chkEditPIM;
        private DevExpress.XtraEditors.CheckEdit chkNewPIM;
        private DevExpress.XtraEditors.CheckEdit chkPayGrade;
        private DevExpress.XtraEditors.CheckEdit chkLeaveType;
        private DevExpress.XtraEditors.CheckEdit chkViewLeave;
        private DevExpress.XtraEditors.CheckEdit chkDeleteLeave;
        private DevExpress.XtraEditors.CheckEdit chkEditLeave;
        private DevExpress.XtraEditors.CheckEdit chkNewLeave;
        private DevExpress.XtraEditors.CheckEdit chkLeave;
        private DevExpress.XtraEditors.CheckEdit chkApprovePermintaanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkViewPermintaanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkDeletePermintaanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkEditPermintaanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkNewPermintaanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkPermintaanProduksi;
        private DevExpress.XtraEditors.CheckEdit chkPrintPengembanganProduk;
        private DevExpress.XtraEditors.CheckEdit chkViewPengembanganProduk;
        private DevExpress.XtraEditors.CheckEdit chkDeletePengembanganProduk;
        private DevExpress.XtraEditors.CheckEdit chkEditPengembanganProduk;
        private DevExpress.XtraEditors.CheckEdit chkNewPengembanganProduk;
        private DevExpress.XtraEditors.CheckEdit chkPengembanganProduk;
        private DevExpress.XtraEditors.CheckEdit chkChangeTSLocation;
        private DevExpress.XtraEditors.CheckEdit chkSolveCustomerComplaint;
        private DevExpress.XtraEditors.CheckEdit chkDeleteCustomerComplaint;
        private DevExpress.XtraEditors.CheckEdit chkEditCustomerComplaint;
        private DevExpress.XtraEditors.CheckEdit chkNewCustomerComplaint;
        private DevExpress.XtraEditors.CheckEdit chkCustomerComplaint;
        private DevExpress.XtraEditors.CheckEdit chkProjectCategory;
        private DevExpress.XtraEditors.CheckEdit chkStatusQCBarangJadi;
        private DevExpress.XtraEditors.CheckEdit chkKesimpulanQCBarangJadi;
        private DevExpress.XtraEditors.CheckEdit chkStatusQCBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkKesimpulanQCBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkUpdateRemarksTransferStock;
        private DevExpress.XtraEditors.CheckEdit chkPrintTransferStock;
        private DevExpress.XtraEditors.CheckEdit chkClosePengembanganProduk;
        private DevExpress.XtraEditors.CheckEdit chkPIMReport;
        private DevExpress.XtraEditors.CheckEdit chkActivePIM;
        private DevExpress.XtraEditors.CheckEdit chkPositionPIM;
        private DevExpress.XtraEditors.CheckEdit chkNationalityPIM;
        private DevExpress.XtraEditors.CheckEdit chkViewAllProject;
        private DevExpress.XtraEditors.CheckEdit chkPriceList;
        private DevExpress.XtraEditors.CheckEdit chkEditSupplierBahanBaku;
        private DevExpress.XtraEditors.CheckEdit chkSettingGudang;
    }
}