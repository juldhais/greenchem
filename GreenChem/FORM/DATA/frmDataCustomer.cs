﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmDataCustomer : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataCustomer()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Customer");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Customer");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Customer");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Customer");

            if (System.IO.File.Exists("layout_customer.xml"))
                gridView.RestoreLayoutFromXml("layout_customer.xml");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.Customers
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.Code,
                                x.Name,
                                x.Address,
                                x.Phone,
                                x.Remarks,
                                x.Active,
                                x.ContactPerson
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Code == txtSearch.Text || x.Name.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewCustomer form = new frmNewCustomer();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEditCustomer form = new frmEditCustomer(gridView.GetFocusedRowCellValue("id").ToInteger());
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this customer : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    Customer customer = db.Customers.First(x => x.id == id);
                    customer.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Customer deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            gridControl.ShowPrintPreview();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_customer.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}