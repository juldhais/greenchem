﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmNewSampleCode : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewSampleCode()
        {
            InitializeComponent();
            db = new GreenChemDataContext();

            //cmbTipe.Properties.DataSource = db.Tipes.GetName();
            //cmbFisik.Properties.DataSource = db.Fisiks.GetName();
            cmbSupplier.Properties.DataSource = db.SupplierSCs.GetName();

            //nomor
            try
            {
                var query = (from x in db.SampleCodes
                             orderby x.Nomor descending
                             select x.Nomor).First();

                int number = query.ToInteger() + 1;
                txtNomor.Text = number.ToString("00000");
            }
            catch
            {
                txtNomor.Text = "00001";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                SampleCode samplecode = new SampleCode();
                //if (cmbFisik.EditValue.IsNotEmpty()) samplecode.idFisik = cmbFisik.EditValue.ToInteger();
                //if (cmbTipe.EditValue.IsNotEmpty()) samplecode.idTipe = cmbTipe.EditValue.ToInteger();
                if (cmbSupplier.EditValue.IsNotEmpty()) samplecode.idSupplierSC = cmbSupplier.EditValue.ToInteger();
                samplecode.FisikText = txtFisik.Text;
                samplecode.TipeText = txtTipe.Text;
                samplecode.Nomor = txtNomor.Text;
                samplecode.Kode = txtKode.Text;
                samplecode.Nama = txtNama.Text;
                samplecode.KodeMSDS = txtKodeMSDS.Text;
                samplecode.AttachmentMSDS = "";
                samplecode.Fungsi = txtFungsi.Text;
                samplecode.Quantity = txtQuantity.EditValue.ToDecimal();
                samplecode.TanggalPenerimaan = (DateTime?)cmbTanggalPenerimaan.EditValue;
                samplecode.Active = true;
                db.SampleCodes.InsertOnSubmit(samplecode);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);


                //clear
                try
                {
                    var query = (from x in db.SampleCodes
                                 orderby x.Nomor descending
                                 select x.Nomor).First();

                    int number = query.ToInteger() + 1;
                    txtNomor.Text = number.ToString("00000");
                }
                catch
                {
                    txtNomor.Text = "00001";
                }
                txtKode.Text = string.Empty;
                txtNama.Text = string.Empty;
                txtFungsi.Text = string.Empty;
                txtFisik.Text = string.Empty;
                txtTipe.Text = string.Empty;
                txtKode.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}