﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmBahanPenolong : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmBahanPenolong()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Bahan Penolong");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Bahan Penolong");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Bahan Penolong");
            btnRecap.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Recap Bahan Penolong");
            btnKategori.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Kategori Bahan Penolong");
            btnStatus.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Status Bahan Penolong");
            cmbStatus.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Inactive Bahan Penolong");

            if (System.IO.File.Exists("layout_bahanpenolong.xml"))
                gridView.RestoreLayoutFromXml("layout_bahanpenolong.xml");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                bool aktif = cmbStatus.Text == "Active";

                var query = from x in db.BahanPenolongs
                            where x.Active == aktif
                            select new
                            {
                                x.id,
                                x.Nomor,
                                x.Kode,
                                x.Nama,
                                Kategori = x.KategoriBahanPenolong.Name,
                                Status = x.StatusBahanPenolong.Name,
                                x.Specification
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Nomor == txtSearch.Text || x.Kode == txtSearch.Text || x.Nama.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewBahanPenolong form = new frmNewBahanPenolong();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditBahanPenolong form = new frmEditBahanPenolong(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this item : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    BahanPenolong bahan = db.BahanPenolongs.First(x => x.id == id);
                    bahan.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Item deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuReport.frmReportBahanPenolong form = new MenuReport.frmReportBahanPenolong();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnKategori_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmKategoriBahanPenolong form = new frmKategoriBahanPenolong();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmStatusBahanPenolong form = new frmStatusBahanPenolong();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditBahanPenolong form = new frmEditBahanPenolong(id);
                form.btnSave.Visible = false;
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_bahanpenolong.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}