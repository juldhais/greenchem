﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmNewUserAccount : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        GreenChemDataContext db;

        public frmNewUserAccount()
        {
            InitializeComponent();
            RefreshData();
            EditMode(false);
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbDepartment.Properties.DataSource = db.Departments.GetName();
                cmbSuDepartment.Properties.DataSource = db.SubDepartments.GetName();
                cmbGroup.Properties.DataSource = db.Groups.GetName();

                var query = from x in db.UserAccounts
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.Name
                            };

                gridControl.DataSource = query;
                RefreshDetail();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void RefreshDetail()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                _id = gridView.GetFocusedRowCellValue("id").ToInteger();
                db = new GreenChemDataContext();

                var useraccount = db.UserAccounts.First(x => x.id == _id);
                txtUserName.Text = useraccount.Name;
                txtName.Text = useraccount.UserName;
                txtPassword.Text = useraccount.Password;
                cmbDepartment.EditValue = useraccount.idDepartment;
                cmbSuDepartment.EditValue = useraccount.idSubDepartment;
                cmbGroup.EditValue = useraccount.idGroup;
                
                chkProduct.Checked = useraccount.AccessRole.Contains("[Product]");
                chkNewProduct.Checked = useraccount.AccessRole.Contains("[New Product]");
                chkEditProduct.Checked = useraccount.AccessRole.Contains("[Edit Product]");
                chkDeleteProduct.Checked = useraccount.AccessRole.Contains("[Delete Product]");
                chkPrintProduct.Checked = useraccount.AccessRole.Contains("[Print Product]");
                chkEditStatusProduct.Checked = useraccount.AccessRole.Contains("[Edit Status Product]");
                chkInactiveProduct.Checked = useraccount.AccessRole.Contains("[Inactive Product]");
                chkUpdatePL.Checked = useraccount.AccessRole.Contains("[Update Price List]");
                chkPriceList.Checked = useraccount.AccessRole.Contains("[Price List]");

                chkBahanBaku.Checked = useraccount.AccessRole.Contains("[Bahan Baku]");
                chkNewBahanBaku.Checked = useraccount.AccessRole.Contains("[New Bahan Baku]");
                chkEditBahanBaku.Checked = useraccount.AccessRole.Contains("[Edit Bahan Baku]");
                chkDeleteBahanBaku.Checked = useraccount.AccessRole.Contains("[Delete Bahan Baku]");
                chkRecapBahanBaku.Checked = useraccount.AccessRole.Contains("[Recap Bahan Baku]");
                chkFisikBahanBaku.Checked = useraccount.AccessRole.Contains("[Fisik Bahan Baku]");
                chkTipeBahanBaku.Checked = useraccount.AccessRole.Contains("[Tipe Bahan Baku]");
                chkEditSupplierBahanBaku.Checked = useraccount.AccessRole.Contains("[Edit Supplier Bahan Baku]");
                chkInactiveBahanBaku.Checked = useraccount.AccessRole.Contains("[Inactive Bahan Baku]");
                
                chkBahanPenolong.Checked = useraccount.AccessRole.Contains("[Bahan Penolong]");
                chkNewBahanPenolong.Checked = useraccount.AccessRole.Contains("[New Bahan Penolong]");
                chkEditBahanPenolong.Checked = useraccount.AccessRole.Contains("[Edit Bahan Penolong]");
                chkDeleteBahanPenolong.Checked = useraccount.AccessRole.Contains("[Delete Bahan Penolong]");
                chkRecapBahanPenolong.Checked = useraccount.AccessRole.Contains("[Recap Bahan Penolong]");
                chkKategoriBahanPenolong.Checked = useraccount.AccessRole.Contains("[Kategori Bahan Penolong]");
                chkStatusBahanPenolong.Checked = useraccount.AccessRole.Contains("[Status Bahan Penolong]");
                chkInactiveBahanPenolong.Checked = useraccount.AccessRole.Contains("[Inactive Bahan Penolong]");

                chkSampleCode.Checked = useraccount.AccessRole.Contains("[Sample Code]");
                chkNewSampleCode.Checked = useraccount.AccessRole.Contains("[New Sample Code]");
                chkEditSampleCode.Checked = useraccount.AccessRole.Contains("[Edit Sample Code]");
                chkDeleteSampleCode.Checked = useraccount.AccessRole.Contains("[Delete Sample Code]");
                chkRecapSampleCode.Checked = useraccount.AccessRole.Contains("[Recap Sample Code]");
                chkFisikSampleCode.Checked = useraccount.AccessRole.Contains("[Fisik Sample Code]");
                chkTipeSampleCode.Checked = useraccount.AccessRole.Contains("[Tipe Sample Code]");

                chkCategory.Checked = useraccount.AccessRole.Contains("[Category]");

                chkDepartment.Checked = useraccount.AccessRole.Contains("[Department]");
                chkNewDepartment.Checked = useraccount.AccessRole.Contains("[New Department]");
                chkEditDepartment.Checked = useraccount.AccessRole.Contains("[Edit Department]");
                chkDeleteDepartment.Checked = useraccount.AccessRole.Contains("[Delete Department]");

                chkSupplier.Checked = useraccount.AccessRole.Contains("[Supplier]");
                chkNewSupplier.Checked = useraccount.AccessRole.Contains("[New Supplier]");
                chkEditSupplier.Checked = useraccount.AccessRole.Contains("[Edit Supplier]");
                chkDeleteSupplier.Checked = useraccount.AccessRole.Contains("[Delete Supplier]");
                chkPrintSupplier.Checked = useraccount.AccessRole.Contains("[Print Supplier]");

                chkSupplierSC.Checked = useraccount.AccessRole.Contains("[Supplier SC]");
                chkNewSupplierSC.Checked = useraccount.AccessRole.Contains("[New Supplier SC]");
                chkEditSupplierSC.Checked = useraccount.AccessRole.Contains("[Edit Supplier SC]");
                chkDeleteSupplierSC.Checked = useraccount.AccessRole.Contains("[Delete Supplier SC]");
                chkPrintSupplierSC.Checked = useraccount.AccessRole.Contains("[Print Supplier SC]");

                chkCustomer.Checked = useraccount.AccessRole.Contains("[Customer]");
                chkNewCustomer.Checked = useraccount.AccessRole.Contains("[New Customer]");
                chkEditCustomer.Checked = useraccount.AccessRole.Contains("[Edit Customer]");
                chkDeleteCustomer.Checked = useraccount.AccessRole.Contains("[Delete Customer]");
                chkPrintCustomer.Checked = useraccount.AccessRole.Contains("[Print Customer]");

                chkEmployee.Checked = useraccount.AccessRole.Contains("[Employee]");
                chkNewEmployee.Checked = useraccount.AccessRole.Contains("[New Employee]");
                chkEditEmployee.Checked = useraccount.AccessRole.Contains("[Edit Employee]");
                chkDeleteEmployee.Checked = useraccount.AccessRole.Contains("[Delete Employee]");

                chkExpedition.Checked = useraccount.AccessRole.Contains("[Expedition]");
                chkNewExpedition.Checked = useraccount.AccessRole.Contains("[New Expedition]");
                chkEditExpedition.Checked = useraccount.AccessRole.Contains("[Edit Expedition]");
                chkDeleteExpedition.Checked = useraccount.AccessRole.Contains("[Delete Expedition]");

                chkLocation.Checked = useraccount.AccessRole.Contains("[Location]");
                chkNewLocation.Checked = useraccount.AccessRole.Contains("[New Location]");
                chkEditLocation.Checked = useraccount.AccessRole.Contains("[Edit Location]");
                chkDeleteLocation.Checked = useraccount.AccessRole.Contains("[Delete Location]");

                chkCurrency.Checked = useraccount.AccessRole.Contains("[Currency]");
                chkTerms.Checked = useraccount.AccessRole.Contains("[Terms]");
                chkPackaging.Checked = useraccount.AccessRole.Contains("[Packaging]");

                chkUserAccount.Checked = useraccount.AccessRole.Contains("[User Account]");
                chkNewUserAccount.Checked = useraccount.AccessRole.Contains("[New User Account]");
                chkEditUserAccount.Checked = useraccount.AccessRole.Contains("[Edit User Account]");
                chkDeleteUserAccount.Checked = useraccount.AccessRole.Contains("[Delete User Account]");

                chkContactList.Checked = useraccount.AccessRole.Contains("[ContactList]");
                chkNewContactList.Checked = useraccount.AccessRole.Contains("[New ContactList]");
                chkEditContactList.Checked = useraccount.AccessRole.Contains("[Edit ContactList]");
                chkDeleteContactList.Checked = useraccount.AccessRole.Contains("[Delete ContactList]");
                chkPrintContactList.Checked = useraccount.AccessRole.Contains("[Print ContactList]");
                

                chkSalesOrder.Checked = useraccount.AccessRole.Contains("[Sales Order]");
                chkNewSalesOrder.Checked = useraccount.AccessRole.Contains("[New Sales Order]");
                chkEditSalesOrder.Checked = useraccount.AccessRole.Contains("[Edit Sales Order]");
                chkDeleteSalesOrder.Checked = useraccount.AccessRole.Contains("[Delete Sales Order]");
                chkPrintSalesOrder.Checked = useraccount.AccessRole.Contains("[Print Sales Order]");
                chkApproveSalesOrder.Checked = useraccount.AccessRole.Contains("[Approve Sales Order]");
                chkApprove2SalesOrder.Checked = useraccount.AccessRole.Contains("[Approve2 Sales Order]");

                chkDeliveryOrder.Checked = useraccount.AccessRole.Contains("[Delivery Order]");
                chkNewDeliveryOrder.Checked = useraccount.AccessRole.Contains("[New Delivery Order]");
                chkEditDeliveryOrder.Checked = useraccount.AccessRole.Contains("[Edit Delivery Order]");
                chkDeleteDeliveryOrder.Checked = useraccount.AccessRole.Contains("[Delete Delivery Order]");
                chkPrintDeliveryOrder.Checked = useraccount.AccessRole.Contains("[Print Delivery Order]");
                chkSetLocationDeliveryOrder.Checked = useraccount.AccessRole.Contains("[Set Location Delivery Order]");
                chkSetDONumber.Checked = useraccount.AccessRole.Contains("[Set DO Number]");

                chkSalesInvoice.Checked = useraccount.AccessRole.Contains("[Sales Invoice]");
                chkCreateInvoice.Checked = useraccount.AccessRole.Contains("[Create Invoice]");
                chkCancelInvoice.Checked = useraccount.AccessRole.Contains("[Cancel Invoice]");
                chkPrintInvoice.Checked = useraccount.AccessRole.Contains("[Print Invoice]");
                chkPayInvoice.Checked = useraccount.AccessRole.Contains("[Pay Invoice]");

                chkChangeDOLocation.Checked = useraccount.AccessRole.Contains("[Change DO Location]");
                chkChangeTSLocation.Checked = useraccount.AccessRole.Contains("[Change TS Location]");
                chkStockAdjustment.Checked = useraccount.AccessRole.Contains("[Stock Adjustment]");

                chkExpense.Checked = useraccount.AccessRole.Contains("[Expense]");
                chkNewExpense.Checked = useraccount.AccessRole.Contains("[New Expense]");
                chkEditExpense.Checked = useraccount.AccessRole.Contains("[Edit Expense]");
                chkDeleteExpense.Checked = useraccount.AccessRole.Contains("[Delete Expense]");

                chkCustomerVoice.Checked = useraccount.AccessRole.Contains("[Customer Voice]");
                chkNewCustomerVoice.Checked = useraccount.AccessRole.Contains("[New Customer Voice]");
                chkEditCustomerVoice.Checked = useraccount.AccessRole.Contains("[Edit Customer Voice]");
                chkDeleteCustomerVoice.Checked = useraccount.AccessRole.Contains("[Delete Customer Voice]");
                chkSolveCustomerVoice.Checked = useraccount.AccessRole.Contains("[Solve Customer Voice]");
                chkSetDeposition.Checked = useraccount.AccessRole.Contains("[Set Deposition]");

                chkInternalComplaint.Checked = useraccount.AccessRole.Contains("[Internal Complaint]");
                chkNewInternalComplaintMenu.Checked = useraccount.AccessRole.Contains("[New Internal Complaint Menu]");
                chkNewInternalComplaint.Checked = useraccount.AccessRole.Contains("[New Internal Complaint]");
                chkEditInternalComplaint.Checked = useraccount.AccessRole.Contains("[Edit Internal Complaint]");
                chkDeleteInternalComplaint.Checked = useraccount.AccessRole.Contains("[Delete Internal Complaint]");
                chkSolveInternalComplaint.Checked = useraccount.AccessRole.Contains("[Solve Internal Complaint]");
                chkSetDepositionInternalComplaint.Checked = useraccount.AccessRole.Contains("[Set Deposition Internal Complaint]");

                chkProject.Checked = useraccount.AccessRole.Contains("[Project]");
                chkNewProject.Checked = useraccount.AccessRole.Contains("[New Project]");
                chkEditProject.Checked = useraccount.AccessRole.Contains("[Edit Project]");
                chkDeleteProject.Checked = useraccount.AccessRole.Contains("[Delete Project]");
                chkViewProject.Checked = useraccount.AccessRole.Contains("[View Project]");
                chkCreateProjectProgress.Checked = useraccount.AccessRole.Contains("[Create Project Progress]");
                chkCloseProject.Checked = useraccount.AccessRole.Contains("[Close Project]");
                chkProjectCategory.Checked = useraccount.AccessRole.Contains("[Project Category]");
                chkViewAllProject.Checked = useraccount.AccessRole.Contains("[View All Project]");

                chkPIM.Checked = useraccount.AccessRole.Contains("[PIM]");
                chkNewPIM.Checked = useraccount.AccessRole.Contains("[New PIM]");
                chkEditPIM.Checked = useraccount.AccessRole.Contains("[Edit PIM]");
                chkDeletePIM.Checked = useraccount.AccessRole.Contains("[Delete PIM]");
                chkViewPIM.Checked = useraccount.AccessRole.Contains("[View PIM]");
                chkPayGrade.Checked = useraccount.AccessRole.Contains("[Pay Grade]");
                chkNationalityPIM.Checked = useraccount.AccessRole.Contains("[Nationality PIM]");
                chkPositionPIM.Checked = useraccount.AccessRole.Contains("[Position PIM]");
                chkActivePIM.Checked = useraccount.AccessRole.Contains("[Active PIM]");

                chkLeave.Checked = useraccount.AccessRole.Contains("[Leave]");
                chkNewLeave.Checked = useraccount.AccessRole.Contains("[New Leave]");
                chkEditLeave.Checked = useraccount.AccessRole.Contains("[Edit Leave]");
                chkDeleteLeave.Checked = useraccount.AccessRole.Contains("[Delete Leave]");
                chkViewLeave.Checked = useraccount.AccessRole.Contains("[View Leave]");
                chkLeaveType.Checked = useraccount.AccessRole.Contains("[Leave Type]");

                chkPermintaanProduksi.Checked = useraccount.AccessRole.Contains("[Permintaan Produksi]");
                chkNewPermintaanProduksi.Checked = useraccount.AccessRole.Contains("[New Permintaan Produksi]");
                chkEditPermintaanProduksi.Checked = useraccount.AccessRole.Contains("[Edit Permintaan Produksi]");
                chkDeletePermintaanProduksi.Checked = useraccount.AccessRole.Contains("[Delete Permintaan Produksi]");
                chkViewPermintaanProduksi.Checked = useraccount.AccessRole.Contains("[View Permintaan Produksi]");
                chkApprovePermintaanProduksi.Checked = useraccount.AccessRole.Contains("[Approve Permintaan Produksi]");

                chkPengembanganProduk.Checked = useraccount.AccessRole.Contains("[Pengembangan Produk]");
                chkNewPengembanganProduk.Checked = useraccount.AccessRole.Contains("[New Pengembangan Produk]");
                chkEditPengembanganProduk.Checked = useraccount.AccessRole.Contains("[Edit Pengembangan Produk]");
                chkDeletePengembanganProduk.Checked = useraccount.AccessRole.Contains("[Delete Pengembangan Produk]");
                chkViewPengembanganProduk.Checked = useraccount.AccessRole.Contains("[View Pengembangan Produk]");
                chkPrintPengembanganProduk.Checked = useraccount.AccessRole.Contains("[Print Pengembangan Produk]");
                chkClosePengembanganProduk.Checked = useraccount.AccessRole.Contains("[Close Pengembangan Produk]");

                chkCustomerComplaint.Checked = useraccount.AccessRole.Contains("[Customer Complaint]");
                chkNewCustomerComplaint.Checked = useraccount.AccessRole.Contains("[New Customer Complaint]");
                chkEditCustomerComplaint.Checked = useraccount.AccessRole.Contains("[Edit Customer Complaint]");
                chkDeleteCustomerComplaint.Checked = useraccount.AccessRole.Contains("[Delete Customer Complaint]");
                chkSolveCustomerComplaint.Checked = useraccount.AccessRole.Contains("[Solve Customer Complaint]");

                chkPenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[Penyerahan Produksi]");
                chkNewPenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[New Penyerahan Produksi]");
                chkEditPenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[Edit Penyerahan Produksi]");
                chkDeletePenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[Delete Penyerahan Produksi]");
                chkPrintPenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[Print Penyerahan Produksi]");
                chkApprovePenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[Approve Penyerahan Produksi]");
                chkApprove2PenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[Approve2 Penyerahan Produksi]");

                chkTransferStock.Checked = useraccount.AccessRole.Contains("[Transfer Stock]");
                chkNewTransferStock.Checked = useraccount.AccessRole.Contains("[New Transfer Stock]");
                chkEditTransferStock.Checked = useraccount.AccessRole.Contains("[Edit Transfer Stock]");
                chkDeleteTransferStock.Checked = useraccount.AccessRole.Contains("[Delete Transfer Stock]");
                chkApproveTransferStock.Checked = useraccount.AccessRole.Contains("[Approve Transfer Stock]");
                chkApprove2TransferStock.Checked = useraccount.AccessRole.Contains("[Approve2 Transfer Stock]");
                chkPrintTransferStock.Checked = useraccount.AccessRole.Contains("[Print Transfer Stock]");
                chkUpdateRemarksTransferStock.Checked = useraccount.AccessRole.Contains("[Update Remarks Transfer Stock]");

                chkWorkInProgress.Checked = useraccount.AccessRole.Contains("[Work In Progress]");

                chkQCBarangJadi.Checked = useraccount.AccessRole.Contains("[QC Barang Jadi]");
                chkNewQCBarangJadi.Checked = useraccount.AccessRole.Contains("[New QC Barang Jadi]");
                chkEditQCBarangJadi.Checked = useraccount.AccessRole.Contains("[Edit QC Barang Jadi]");
                chkDeleteQCBarangJadi.Checked = useraccount.AccessRole.Contains("[Delete QC Barang Jadi]");
                chkPrintQCBarangJadi.Checked = useraccount.AccessRole.Contains("[Print QC Barang Jadi]");
                chkKesimpulanQCBarangJadi.Checked = useraccount.AccessRole.Contains("[Kesimpulan QC Barang Jadi]");
                chkStatusQCBarangJadi.Checked = useraccount.AccessRole.Contains("[Status QC Barang Jadi]");

                chkQCBahanBaku.Checked = useraccount.AccessRole.Contains("[QC Bahan Baku]");
                chkNewQCBahanBaku.Checked = useraccount.AccessRole.Contains("[New QC Bahan Baku]");
                chkEditQCBahanBaku.Checked = useraccount.AccessRole.Contains("[Edit QC Bahan Baku]");
                chkDeleteQCBahanBaku.Checked = useraccount.AccessRole.Contains("[Delete QC Bahan Baku]");
                chkPrintQCBahanBaku.Checked = useraccount.AccessRole.Contains("[Print QC Bahan Baku]");
                chkKesimpulanQCBahanBaku.Checked = useraccount.AccessRole.Contains("[Kesimpulan QC Bahan Baku]");
                chkStatusQCBahanBaku.Checked = useraccount.AccessRole.Contains("[Status QC Bahan Baku]");

                chkQCPacking.Checked = useraccount.AccessRole.Contains("[QC Packing]");
                chkNewQCPacking.Checked = useraccount.AccessRole.Contains("[New QC Packing]");
                chkEditQCPacking.Checked = useraccount.AccessRole.Contains("[Edit QC Packing]");
                chkDeleteQCPacking.Checked = useraccount.AccessRole.Contains("[Delete QC Packing]");
                chkPrintQCPacking.Checked = useraccount.AccessRole.Contains("[Print QC Packing]");

                chkQCKemasan.Checked = useraccount.AccessRole.Contains("[QC Kemasan]");
                chkNewQCKemasan.Checked = useraccount.AccessRole.Contains("[New QC Kemasan]");
                chkEditQCKemasan.Checked = useraccount.AccessRole.Contains("[Edit QC Kemasan]");
                chkDeleteQCKemasan.Checked = useraccount.AccessRole.Contains("[Delete QC Kemasan]");
                chkPrintQCKemasan.Checked = useraccount.AccessRole.Contains("[Print QC Kemasan]");

                chkStandardDeliveryTime.Checked = useraccount.AccessRole.Contains("[Standard Delivery Time]");
                chkNewStandardDeliveryTime.Checked = useraccount.AccessRole.Contains("[New Standard Delivery Time]");
                chkEditStandardDeliveryTime.Checked = useraccount.AccessRole.Contains("[Edit Standard Delivery Time]");
                chkDeleteStandardDeliveryTime.Checked = useraccount.AccessRole.Contains("[Delete Standard Delivery Time]");

                chkLabService.Checked = useraccount.AccessRole.Contains("[Lab Service]");
                chkNewLabService.Checked = useraccount.AccessRole.Contains("[New Lab Service]");
                chkEditLabService.Checked = useraccount.AccessRole.Contains("[Edit Lab Service]");
                chkDeleteLabService.Checked = useraccount.AccessRole.Contains("[Delete Lab Service]");
                chkPrintLabService.Checked = useraccount.AccessRole.Contains("[Print Lab Service]");

                chkExternalLetter.Checked = useraccount.AccessRole.Contains("[External Letter]");
                chkNewExternalLetter.Checked = useraccount.AccessRole.Contains("[New External Letter]");
                chkEditExternalLetter.Checked = useraccount.AccessRole.Contains("[Edit External Letter]");
                chkDeleteExternalLetter.Checked = useraccount.AccessRole.Contains("[Delete External Letter]");
                
                chkInternalLetter.Checked = useraccount.AccessRole.Contains("[Internal Letter]");
                chkNewInternalLetter.Checked = useraccount.AccessRole.Contains("[New Internal Letter]");
                chkEditInternalLetter.Checked = useraccount.AccessRole.Contains("[Edit Internal Letter]");
                chkDeleteInternalLetter.Checked = useraccount.AccessRole.Contains("[Delete Internal Letter]");
                
                chkPOtoSupplier.Checked = useraccount.AccessRole.Contains("[PO to Supplier]");
                chkNewPOtoSupplier.Checked = useraccount.AccessRole.Contains("[New PO to Supplier]");
                chkEditPOtoSupplier.Checked = useraccount.AccessRole.Contains("[Edit PO to Supplier]");
                chkDeletePOtoSupplier.Checked = useraccount.AccessRole.Contains("[Delete PO to Supplier]");
                chkChangeDeliveryStatus.Checked = useraccount.AccessRole.Contains("[Change Delivery Status]");

                chkPOtoSupplierRegular.Checked = useraccount.AccessRole.Contains("[PO to Supplier Regular]");
                chkNewPOtoSupplierRegular.Checked = useraccount.AccessRole.Contains("[New PO to Supplier Regular]");
                chkEditPOtoSupplierRegular.Checked = useraccount.AccessRole.Contains("[Edit PO to Supplier Regular]");
                chkDeletePOtoSupplierRegular.Checked = useraccount.AccessRole.Contains("[Delete PO to Supplier Regular]");
                chkPrintPOtoSupplierRegular.Checked = useraccount.AccessRole.Contains("[Print PO to Supplier Regular]");
                chkApprovePOtoSupplierRegular.Checked = useraccount.AccessRole.Contains("[Approve PO to Supplier Regular]");

                chkPOtoSupplierExpedisi.Checked = useraccount.AccessRole.Contains("[PO to Supplier Expedisi]");
                chkNewPOtoSupplierExpedisi.Checked = useraccount.AccessRole.Contains("[New PO to Supplier Expedisi]");
                chkEditPOtoSupplierExpedisi.Checked = useraccount.AccessRole.Contains("[Edit PO to Supplier Expedisi]");
                chkDeletePOtoSupplierExpedisi.Checked = useraccount.AccessRole.Contains("[Delete PO to Supplier Expedisi]");
                chkPrintPOtoSupplierExpedisi.Checked = useraccount.AccessRole.Contains("[Print PO to Supplier Expedisi]");
                chkApprovePOtoSupplierExpedisi.Checked = useraccount.AccessRole.Contains("[Approve PO to Supplier Expedisi]");

                chkInquiryIn.Checked = useraccount.AccessRole.Contains("[Inquiry In]");
                chkNewInquiryIn.Checked = useraccount.AccessRole.Contains("[New Inquiry In]");
                chkEditInquiryIn.Checked = useraccount.AccessRole.Contains("[Edit Inquiry In]");
                chkDeleteInquiryIn.Checked = useraccount.AccessRole.Contains("[Delete Inquiry In]");
                
                chkInquiryOut.Checked = useraccount.AccessRole.Contains("[Inquiry Out]");
                chkNewInquiryOut.Checked = useraccount.AccessRole.Contains("[New Inquiry Out]");
                chkEditInquiryOut.Checked = useraccount.AccessRole.Contains("[Edit Inquiry Out]");
                chkDeleteInquiryOut.Checked = useraccount.AccessRole.Contains("[Delete Inquiry Out]");
                
                chkQuotationIn.Checked = useraccount.AccessRole.Contains("[Quotation In]");
                chkNewQuotationIn.Checked = useraccount.AccessRole.Contains("[New Quotation In]");
                chkEditQuotationIn.Checked = useraccount.AccessRole.Contains("[Edit Quotation In]");
                chkDeleteQuotationIn.Checked = useraccount.AccessRole.Contains("[Delete Quotation In]");

                chkQuotationOut.Checked = useraccount.AccessRole.Contains("[Quotation Out]");
                chkNewQuotationOut.Checked = useraccount.AccessRole.Contains("[New Quotation Out]");
                chkEditQuotationOut.Checked = useraccount.AccessRole.Contains("[Edit Quotation Out]");
                chkDeleteQuotationOut.Checked = useraccount.AccessRole.Contains("[Delete Quotation Out]");
                
                chkPPB.Checked = useraccount.AccessRole.Contains("[PPB]");
                chkNewPPB.Checked = useraccount.AccessRole.Contains("[New PPB]");
                chkEditPPB.Checked = useraccount.AccessRole.Contains("[Edit PPB]");
                chkDeletePPB.Checked = useraccount.AccessRole.Contains("[Delete PPB]");
                chkSetPONumberPPB.Checked = useraccount.AccessRole.Contains("[Set PO Number PPB]");
                
                chkPPS.Checked = useraccount.AccessRole.Contains("[PPS]");
                chkNewPPS.Checked = useraccount.AccessRole.Contains("[New PPS]");
                chkEditPPS.Checked = useraccount.AccessRole.Contains("[Edit PPS]");
                chkDeletePPS.Checked = useraccount.AccessRole.Contains("[Delete PPS]");
                
                chkSalesOrderReport.Checked = useraccount.AccessRole.Contains("[Sales Order Report]");
                chkAutoNumberReport.Checked = useraccount.AccessRole.Contains("[Auto Number Report]");
                chkUserLogReport.Checked = useraccount.AccessRole.Contains("[User Log Report]");
                chkExpenseReport.Checked = useraccount.AccessRole.Contains("[Expense Report]");
                chkCustomerSatisfactionReport.Checked = useraccount.AccessRole.Contains("[Customer Satisfaction Report]");
                chkCustomerComplaintReport.Checked = useraccount.AccessRole.Contains("[Customer Complaint Report]");
                chkCustomerVoiceReport.Checked = useraccount.AccessRole.Contains("[Customer Voice Report]");
                chkPPBReport.Checked = useraccount.AccessRole.Contains("[PPB Report]");
                chkPPSReport.Checked = useraccount.AccessRole.Contains("[PPS Report]");
                chkExternalLetterReport.Checked = useraccount.AccessRole.Contains("[External Letter Report]");
                chkInternalLetterReport.Checked = useraccount.AccessRole.Contains("[Internal Letter Report]");
                chkPOtoSupplierReport.Checked = useraccount.AccessRole.Contains("[PO to Supplier Report]");
                chkQuotationInReport.Checked = useraccount.AccessRole.Contains("[Quotation In Report]");
                chkQuotationOutReport.Checked = useraccount.AccessRole.Contains("[Quotation Out Report]");
                chkInquiryInReport.Checked = useraccount.AccessRole.Contains("[Inquiry In Report]");
                chkInquiryOutReport.Checked = useraccount.AccessRole.Contains("[Inquiry Out Report]");
                chkSalesOrderSummary.Checked = useraccount.AccessRole.Contains("[Sales Order Summary]");
                chkDeliveryOrderSummary.Checked = useraccount.AccessRole.Contains("[Delivery Order Summary]");
                chkSalesInvoiceSummary.Checked = useraccount.AccessRole.Contains("[Sales Invoice Summary]");
                chkSalesOrderDetail.Checked = useraccount.AccessRole.Contains("[Sales Order Detail]");
                chkDeliveryOrderLocation.Checked = useraccount.AccessRole.Contains("[Delivery Order Location]");
                chkPenyerahanProduksiReport.Checked = useraccount.AccessRole.Contains("[Penyerahan Produksi Report]");
                chkStockReport.Checked = useraccount.AccessRole.Contains("[Stock Report]");
                chkPIMReport.Checked = useraccount.AccessRole.Contains("[PIM Report]");

                chkConfiguration.Checked = useraccount.AccessRole.Contains("[Configuration]");
                chkDesignReport.Checked = useraccount.AccessRole.Contains("[Design Report]");
                chkAdministrator.Checked = useraccount.AccessRole.Contains("[Administrator]");
                chkManager.Checked = useraccount.AccessRole.Contains("[Manager]");
                chkUser.Checked = useraccount.AccessRole.Contains("[User]");
                chkSettingGudang.Checked = useraccount.AccessRole.Contains("[Setting Gudang]");
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void EditMode(bool isedit)
        {
            gridControl.Enabled = !isedit;
            txtUserName.Enabled = isedit;
            txtName.Enabled = isedit;
            txtPassword.Enabled = isedit;
            cmbDepartment.Enabled = isedit;
            cmbSuDepartment.Enabled = isedit;
            cmbGroup.Enabled = isedit;

            foreach (var item in tabData.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Enabled = isedit;
                }
            }

            foreach (var item in tabTransaction.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Enabled = isedit;
                }
            }

            foreach (var item in tabTransaction2.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Enabled = isedit;
                }
            }

            foreach (var item in tabAutoNumber.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Enabled = isedit;
                }
            }

            foreach (var item in tabReport.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Enabled = isedit;
                }
            }

            foreach (var item in tabOthers.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Enabled = isedit;
                }
            }

            btnNew.Enabled = !isedit;
            btnEdit.Enabled = !isedit;
            btnDelete.Enabled = !isedit;
            btnSave.Enabled = isedit;
            btnCancel.Enabled = isedit;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            EditMode(true);
            _id = null;
            txtUserName.Text = string.Empty;
            txtName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            cmbDepartment.EditValue = null;
            cmbSuDepartment.EditValue = null;
            cmbGroup.EditValue = null;

            foreach (var item in tabData.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Checked = true;
                }
            }

            foreach (var item in tabTransaction.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Checked = true;
                }
            }

            foreach (var item in tabTransaction2.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Checked = true;
                }
            }

            foreach (var item in tabAutoNumber.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Checked = true;
                }
            }

            foreach (var item in tabReport.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Checked = true;
                }
            }

            foreach (var item in tabOthers.Controls)
            {
                if (item.GetType() == typeof(CheckEdit))
                {
                    CheckEdit control = (CheckEdit)item;
                    control.Checked = true;
                }
            }

            txtUserName.Focus();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditMode(true);
            RefreshDetail();
            txtUserName.Focus();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshDetail();
                string name = gridView.GetFocusedRowCellValue("Name").Safe(); ;
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this user : " + name, Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    UserAccount useraccount = db.UserAccounts.First(x => x.id == _id);
                    useraccount.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                if (db.UserAccounts.Any(x => x.Name == txtUserName.Text && x.Active == true && x.id != _id))
                {
                    XtraMessageBox.Show("User name already used.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUserName.Focus();
                    return;
                }

                UserAccount useraccount = _id == null ? new UserAccount() : db.UserAccounts.First(x => x.id == _id);
                useraccount.Name = txtUserName.Text;
                useraccount.UserName = txtName.Text;
                useraccount.Password = txtPassword.Text;
                useraccount.idDepartment = (int?)cmbDepartment.EditValue;
                useraccount.idSubDepartment = (int?)cmbSuDepartment.EditValue;
                useraccount.idGroup = (int?)cmbGroup.EditValue;
                useraccount.AccessRole = string.Empty;
                useraccount.Active = true;

                if (chkProduct.Checked) useraccount.AccessRole += "[Product]";
                if (chkNewProduct.Checked) useraccount.AccessRole += "[New Product]";
                if (chkEditProduct.Checked) useraccount.AccessRole += "[Edit Product]";
                if (chkDeleteProduct.Checked) useraccount.AccessRole += "[Delete Product]";
                if (chkPrintProduct.Checked) useraccount.AccessRole += "[Print Product]";
                if (chkEditStatusProduct.Checked) useraccount.AccessRole += "[Edit Status Product]";
                if (chkInactiveProduct.Checked) useraccount.AccessRole += "[Inactive Product]";
                if (chkUpdatePL.Checked) useraccount.AccessRole += "[Update Price List]";
                if (chkPriceList.Checked) useraccount.AccessRole += "[Price List]";

                if (chkBahanBaku.Checked) useraccount.AccessRole += "[Bahan Baku]";
                if (chkNewBahanBaku.Checked) useraccount.AccessRole += "[New Bahan Baku]";
                if (chkEditBahanBaku.Checked) useraccount.AccessRole += "[Edit Bahan Baku]";
                if (chkDeleteBahanBaku.Checked) useraccount.AccessRole += "[Delete Bahan Baku]";
                if (chkRecapBahanBaku.Checked) useraccount.AccessRole += "[Recap Bahan Baku]";
                if (chkFisikBahanBaku.Checked) useraccount.AccessRole += "[Fisik Bahan Baku]";
                if (chkTipeBahanBaku.Checked) useraccount.AccessRole += "[Tipe Bahan Baku]";
                if (chkEditSupplierBahanBaku.Checked) useraccount.AccessRole += "[Edit Supplier Bahan Baku]";
                if (chkInactiveBahanBaku.Checked) useraccount.AccessRole += "[Inactive Bahan Baku]";

                if (chkBahanPenolong.Checked) useraccount.AccessRole += "[Bahan Penolong]";
                if (chkNewBahanPenolong.Checked) useraccount.AccessRole += "[New Bahan Penolong]";
                if (chkEditBahanPenolong.Checked) useraccount.AccessRole += "[Edit Bahan Penolong]";
                if (chkDeleteBahanPenolong.Checked) useraccount.AccessRole += "[Delete Bahan Penolong]";
                if (chkRecapBahanPenolong.Checked) useraccount.AccessRole += "[Recap Bahan Penolong]";
                if (chkKategoriBahanPenolong.Checked) useraccount.AccessRole += "[Kategori Bahan Penolong]";
                if (chkStatusBahanPenolong.Checked) useraccount.AccessRole += "[Status Bahan Penolong]";
                if (chkInactiveBahanPenolong.Checked) useraccount.AccessRole += "[Inactive Bahan Penolong]";

                if (chkSampleCode.Checked) useraccount.AccessRole += "[Sample Code]";
                if (chkNewSampleCode.Checked) useraccount.AccessRole += "[New Sample Code]";
                if (chkEditSampleCode.Checked) useraccount.AccessRole += "[Edit Sample Code]";
                if (chkDeleteSampleCode.Checked) useraccount.AccessRole += "[Delete Sample Code]";
                if (chkRecapSampleCode.Checked) useraccount.AccessRole += "[Recap Sample Code]";
                if (chkFisikSampleCode.Checked) useraccount.AccessRole += "[Fisik Sample Code]";
                if (chkTipeSampleCode.Checked) useraccount.AccessRole += "[Tipe Sample Code]";

                if (chkCategory.Checked) useraccount.AccessRole += "[Category]";

                if (chkDepartment.Checked) useraccount.AccessRole += "[Department]";
                if (chkNewDepartment.Checked) useraccount.AccessRole += "[New Department]";
                if (chkEditDepartment.Checked) useraccount.AccessRole += "[Edit Department]";
                if (chkDeleteDepartment.Checked) useraccount.AccessRole += "[Delete Department]";

                if (chkSupplier.Checked) useraccount.AccessRole += "[Supplier]";
                if (chkNewSupplier.Checked) useraccount.AccessRole += "[New Supplier]";
                if (chkEditSupplier.Checked) useraccount.AccessRole += "[Edit Supplier]";
                if (chkDeleteSupplier.Checked) useraccount.AccessRole += "[Delete Supplier]";
                if (chkPrintSupplier.Checked) useraccount.AccessRole += "[Print Supplier]";

                if (chkSupplierSC.Checked) useraccount.AccessRole += "[Supplier SC]";
                if (chkNewSupplierSC.Checked) useraccount.AccessRole += "[New Supplier SC]";
                if (chkEditSupplierSC.Checked) useraccount.AccessRole += "[Edit Supplier SC]";
                if (chkDeleteSupplierSC.Checked) useraccount.AccessRole += "[Delete Supplier SC]";
                if (chkPrintSupplierSC.Checked) useraccount.AccessRole += "[Print Supplier SC]";

                if (chkCustomer.Checked) useraccount.AccessRole += "[Customer]";
                if (chkNewCustomer.Checked) useraccount.AccessRole += "[New Customer]";
                if (chkEditCustomer.Checked) useraccount.AccessRole += "[Edit Customer]";
                if (chkDeleteCustomer.Checked) useraccount.AccessRole += "[Delete Customer]";
                if (chkPrintCustomer.Checked) useraccount.AccessRole += "[Print Customer]";

                if (chkEmployee.Checked) useraccount.AccessRole += "[Employee]";
                if (chkNewEmployee.Checked) useraccount.AccessRole += "[New Employee]";
                if (chkEditEmployee.Checked) useraccount.AccessRole += "[Edit Employee]";
                if (chkDeleteEmployee.Checked) useraccount.AccessRole += "[Delete Employee]";

                if (chkExpedition.Checked) useraccount.AccessRole += "[Expedition]";
                if (chkNewExpedition.Checked) useraccount.AccessRole += "[New Expedition]";
                if (chkEditExpedition.Checked) useraccount.AccessRole += "[Edit Expedition]";
                if (chkDeleteExpedition.Checked) useraccount.AccessRole += "[Delete Expedition]";

                if (chkLocation.Checked) useraccount.AccessRole += "[Location]";
                if (chkNewLocation.Checked) useraccount.AccessRole += "[New Location]";
                if (chkEditLocation.Checked) useraccount.AccessRole += "[Edit Location]";
                if (chkDeleteLocation.Checked) useraccount.AccessRole += "[Delete Location]";

                if (chkCurrency.Checked) useraccount.AccessRole += "[Currency]";
                if (chkTerms.Checked) useraccount.AccessRole += "[Terms]";
                if (chkPackaging.Checked) useraccount.AccessRole += "[Packaging]";

                if (chkUserAccount.Checked) useraccount.AccessRole += "[User Account]";
                if (chkNewUserAccount.Checked) useraccount.AccessRole += "[New User Account]";
                if (chkEditUserAccount.Checked) useraccount.AccessRole += "[Edit User Account]";
                if (chkDeleteUserAccount.Checked) useraccount.AccessRole += "[Delete User Account]";

                if (chkContactList.Checked) useraccount.AccessRole += "[ContactList]";
                if (chkNewContactList.Checked) useraccount.AccessRole += "[New ContactList]";
                if (chkEditContactList.Checked) useraccount.AccessRole += "[Edit ContactList]";
                if (chkDeleteContactList.Checked) useraccount.AccessRole += "[Delete ContactList]";
                if (chkPrintContactList.Checked) useraccount.AccessRole += "[Print ContactList]";
                

                if (chkSalesOrder.Checked) useraccount.AccessRole += "[Sales Order]";
                if (chkNewSalesOrder.Checked) useraccount.AccessRole += "[New Sales Order]";
                if (chkEditSalesOrder.Checked) useraccount.AccessRole += "[Edit Sales Order]";
                if (chkDeleteSalesOrder.Checked) useraccount.AccessRole += "[Delete Sales Order]";
                if (chkPrintSalesOrder.Checked) useraccount.AccessRole += "[Print Sales Order]";
                if (chkApproveSalesOrder.Checked) useraccount.AccessRole += "[Approve Sales Order]";
                if (chkApprove2SalesOrder.Checked) useraccount.AccessRole += "[Approve2 Sales Order]";

                if (chkDeliveryOrder.Checked) useraccount.AccessRole += "[Delivery Order]";
                if (chkNewDeliveryOrder.Checked) useraccount.AccessRole += "[New Delivery Order]";
                if (chkEditDeliveryOrder.Checked) useraccount.AccessRole += "[Edit Delivery Order]";
                if (chkDeleteDeliveryOrder.Checked) useraccount.AccessRole += "[Delete Delivery Order]";
                if (chkPrintDeliveryOrder.Checked) useraccount.AccessRole += "[Print Delivery Order]";
                if (chkSetLocationDeliveryOrder.Checked) useraccount.AccessRole += "[Set Location Delivery Order]";
                if (chkSetDONumber.Checked) useraccount.AccessRole += "[Set DO Number]";

                if (chkSalesInvoice.Checked) useraccount.AccessRole += "[Sales Invoice]";
                if (chkCreateInvoice.Checked) useraccount.AccessRole += "[Create Invoice]";
                if (chkCancelInvoice.Checked) useraccount.AccessRole += "[Cancel Invoice]";
                if (chkPrintInvoice.Checked) useraccount.AccessRole += "[Print Invoice]";
                if (chkPayInvoice.Checked) useraccount.AccessRole += "[Pay Invoice]";

                if (chkChangeDOLocation.Checked) useraccount.AccessRole += "[Change DO Location]";
                if (chkChangeTSLocation.Checked) useraccount.AccessRole += "[Change TS Location]";
                if (chkStockAdjustment.Checked) useraccount.AccessRole += "[Stock Adjustment]";

                if (chkExpense.Checked) useraccount.AccessRole += "[Expense]";
                if (chkNewExpense.Checked) useraccount.AccessRole += "[New Expense]";
                if (chkEditExpense.Checked) useraccount.AccessRole += "[Edit Expense]";
                if (chkDeleteExpense.Checked) useraccount.AccessRole += "[Delete Expense]";

                if (chkCustomerVoice.Checked) useraccount.AccessRole += "[Customer Voice]";
                if (chkNewCustomerVoice.Checked) useraccount.AccessRole += "[New Customer Voice]";
                if (chkEditCustomerVoice.Checked) useraccount.AccessRole += "[Edit Customer Voice]";
                if (chkDeleteCustomerVoice.Checked) useraccount.AccessRole += "[Delete Customer Voice]";
                if (chkSolveCustomerVoice.Checked) useraccount.AccessRole += "[Solve Customer Voice]";
                if (chkSetDeposition.Checked) useraccount.AccessRole += "[Set Deposition]";

                if (chkCustomerComplaint.Checked) useraccount.AccessRole += "[Customer Complaint]";
                if (chkNewCustomerComplaint.Checked) useraccount.AccessRole += "[New Customer Complaint]";
                if (chkEditCustomerComplaint.Checked) useraccount.AccessRole += "[Edit Customer Complaint]";
                if (chkDeleteCustomerComplaint.Checked) useraccount.AccessRole += "[Delete Customer Complaint]";
                if (chkSolveCustomerComplaint.Checked) useraccount.AccessRole += "[Solve Customer Complaint]";

                if (chkInternalComplaint.Checked) useraccount.AccessRole += "[Internal Complaint]";
                if (chkNewInternalComplaintMenu.Checked) useraccount.AccessRole += "[New Internal Complaint Menu]";
                if (chkNewInternalComplaint.Checked) useraccount.AccessRole += "[New Internal Complaint]";
                if (chkEditInternalComplaint.Checked) useraccount.AccessRole += "[Edit Internal Complaint]";
                if (chkDeleteInternalComplaint.Checked) useraccount.AccessRole += "[Delete Internal Complaint]";
                if (chkSolveInternalComplaint.Checked) useraccount.AccessRole += "[Solve Internal Complaint]";
                if (chkSetDepositionInternalComplaint.Checked) useraccount.AccessRole += "[Set Deposition Internal Complaint]";

                if (chkProject.Checked) useraccount.AccessRole += "[Project]";
                if (chkNewProject.Checked) useraccount.AccessRole += "[New Project]";
                if (chkEditProject.Checked) useraccount.AccessRole += "[Edit Project]";
                if (chkDeleteProject.Checked) useraccount.AccessRole += "[Delete Project]";
                if (chkViewProject.Checked) useraccount.AccessRole += "[View Project]";
                if (chkCreateProjectProgress.Checked) useraccount.AccessRole += "[Create Project Progress]";
                if (chkCloseProject.Checked) useraccount.AccessRole += "[Close Project]";
                if (chkProjectCategory.Checked) useraccount.AccessRole += "[Project Category]";
                if (chkViewAllProject.Checked) useraccount.AccessRole += "[View All Project]";

                if (chkPIM.Checked) useraccount.AccessRole += "[PIM]";
                if (chkNewPIM.Checked) useraccount.AccessRole += "[New PIM]";
                if (chkEditPIM.Checked) useraccount.AccessRole += "[Edit PIM]";
                if (chkDeletePIM.Checked) useraccount.AccessRole += "[Delete PIM]";
                if (chkViewPIM.Checked) useraccount.AccessRole += "[View PIM]";
                if (chkPayGrade.Checked) useraccount.AccessRole += "[Pay Grade]";
                if (chkNationalityPIM.Checked) useraccount.AccessRole += "[Nationality PIM]";
                if (chkPositionPIM.Checked) useraccount.AccessRole += "[Position PIM]";
                if (chkActivePIM.Checked) useraccount.AccessRole += "[Active PIM]";

                if (chkLeave.Checked) useraccount.AccessRole += "[Leave]";
                if (chkNewLeave.Checked) useraccount.AccessRole += "[New Leave]";
                if (chkEditLeave.Checked) useraccount.AccessRole += "[Edit Leave]";
                if (chkDeleteLeave.Checked) useraccount.AccessRole += "[Delete Leave]";
                if (chkViewLeave.Checked) useraccount.AccessRole += "[View Leave]";
                if (chkLeaveType.Checked) useraccount.AccessRole += "[Leave Type]";

                if (chkPermintaanProduksi.Checked) useraccount.AccessRole += "[Permintaan Produksi]";
                if (chkNewPermintaanProduksi.Checked) useraccount.AccessRole += "[New Permintaan Produksi]";
                if (chkEditPermintaanProduksi.Checked) useraccount.AccessRole += "[Edit Permintaan Produksi]";
                if (chkDeletePermintaanProduksi.Checked) useraccount.AccessRole += "[Delete Permintaan Produksi]";
                if (chkViewPermintaanProduksi.Checked) useraccount.AccessRole += "[View Permintaan Produksi]";
                if (chkApprovePermintaanProduksi.Checked) useraccount.AccessRole += "[Approve Permintaan Produksi]";

                if (chkPengembanganProduk.Checked) useraccount.AccessRole += "[Pengembangan Produk]";
                if (chkNewPengembanganProduk.Checked) useraccount.AccessRole += "[New Pengembangan Produk]";
                if (chkEditPengembanganProduk.Checked) useraccount.AccessRole += "[Edit Pengembangan Produk]";
                if (chkDeletePengembanganProduk.Checked) useraccount.AccessRole += "[Delete Pengembangan Produk]";
                if (chkViewPengembanganProduk.Checked) useraccount.AccessRole += "[View Pengembangan Produk]";
                if (chkPrintPengembanganProduk.Checked) useraccount.AccessRole += "[Print Pengembangan Produk]";
                if (chkClosePengembanganProduk.Checked) useraccount.AccessRole += "[Close Pengembangan Produk]";

                if (chkPenyerahanProduksi.Checked) useraccount.AccessRole += "[Penyerahan Produksi]";
                if (chkNewPenyerahanProduksi.Checked) useraccount.AccessRole += "[New Penyerahan Produksi]";
                if (chkEditPenyerahanProduksi.Checked) useraccount.AccessRole += "[Edit Penyerahan Produksi]";
                if (chkDeletePenyerahanProduksi.Checked) useraccount.AccessRole += "[Delete Penyerahan Produksi]";
                if (chkPrintPenyerahanProduksi.Checked) useraccount.AccessRole += "[Print Penyerahan Produksi]";
                if (chkApprovePenyerahanProduksi.Checked) useraccount.AccessRole += "[Approve Penyerahan Produksi]";
                if (chkApprove2PenyerahanProduksi.Checked) useraccount.AccessRole += "[Approve2 Penyerahan Produksi]";
                
                if (chkTransferStock.Checked) useraccount.AccessRole += "[Transfer Stock]";
                if (chkNewTransferStock.Checked) useraccount.AccessRole += "[New Transfer Stock]";
                if (chkEditTransferStock.Checked) useraccount.AccessRole += "[Edit Transfer Stock]";
                if (chkDeleteTransferStock.Checked) useraccount.AccessRole += "[Delete Transfer Stock]";
                if (chkApproveTransferStock.Checked) useraccount.AccessRole += "[Approve Transfer Stock]";
                if (chkApprove2TransferStock.Checked) useraccount.AccessRole += "[Approve2 Transfer Stock]";
                if (chkPrintTransferStock.Checked) useraccount.AccessRole += "[Print Transfer Stock]";
                if (chkUpdateRemarksTransferStock.Checked) useraccount.AccessRole += "[Update Remarks Transfer Stock]";
                
                if (chkWorkInProgress.Checked) useraccount.AccessRole += "[Work In Progress]";

                if (chkQCBarangJadi.Checked) useraccount.AccessRole += "[QC Barang Jadi]";
                if (chkNewQCBarangJadi.Checked) useraccount.AccessRole += "[New QC Barang Jadi]";
                if (chkEditQCBarangJadi.Checked) useraccount.AccessRole += "[Edit QC Barang Jadi]";
                if (chkDeleteQCBarangJadi.Checked) useraccount.AccessRole += "[Delete QC Barang Jadi]";
                if (chkPrintQCBarangJadi.Checked) useraccount.AccessRole += "[Print QC Barang Jadi]";
                if (chkKesimpulanQCBarangJadi.Checked) useraccount.AccessRole += "[Kesimpulan QC Barang Jadi]";
                if (chkStatusQCBarangJadi.Checked) useraccount.AccessRole += "[Status QC Barang Jadi]";

                if (chkQCBahanBaku.Checked) useraccount.AccessRole += "[QC Bahan Baku]";
                if (chkNewQCBahanBaku.Checked) useraccount.AccessRole += "[New QC Bahan Baku]";
                if (chkEditQCBahanBaku.Checked) useraccount.AccessRole += "[Edit QC Bahan Baku]";
                if (chkDeleteQCBahanBaku.Checked) useraccount.AccessRole += "[Delete QC Bahan Baku]";
                if (chkPrintQCBahanBaku.Checked) useraccount.AccessRole += "[Print QC Bahan Baku]";
                if (chkKesimpulanQCBahanBaku.Checked) useraccount.AccessRole += "[Kesimpulan QC Bahan Baku]";
                if (chkStatusQCBahanBaku.Checked) useraccount.AccessRole += "[Status QC Bahan Baku]";

                if (chkQCPacking.Checked) useraccount.AccessRole += "[QC Packing]";
                if (chkNewQCPacking.Checked) useraccount.AccessRole += "[New QC Packing]";
                if (chkEditQCPacking.Checked) useraccount.AccessRole += "[Edit QC Packing]";
                if (chkDeleteQCPacking.Checked) useraccount.AccessRole += "[Delete QC Packing]";
                if (chkPrintQCPacking.Checked) useraccount.AccessRole += "[Print QC Packing]";

                if (chkQCKemasan.Checked) useraccount.AccessRole += "[QC Kemasan]";
                if (chkNewQCKemasan.Checked) useraccount.AccessRole += "[New QC Kemasan]";
                if (chkEditQCKemasan.Checked) useraccount.AccessRole += "[Edit QC Kemasan]";
                if (chkDeleteQCKemasan.Checked) useraccount.AccessRole += "[Delete QC Kemasan]";
                if (chkPrintQCKemasan.Checked) useraccount.AccessRole += "[Print QC Kemasan]";

                if (chkStandardDeliveryTime.Checked) useraccount.AccessRole += "[Standard Delivery Time]";
                if (chkNewStandardDeliveryTime.Checked) useraccount.AccessRole += "[New Standard Delivery Time]";
                if (chkEditStandardDeliveryTime.Checked) useraccount.AccessRole += "[Edit Standard Delivery Time]";
                if (chkDeleteStandardDeliveryTime.Checked) useraccount.AccessRole += "[Delete Standard Delivery Time]";

                if (chkLabService.Checked) useraccount.AccessRole += "[Lab Service]";
                if (chkNewLabService.Checked) useraccount.AccessRole += "[New Lab Service]";
                if (chkEditLabService.Checked) useraccount.AccessRole += "[Edit Lab Service]";
                if (chkDeleteLabService.Checked) useraccount.AccessRole += "[Delete Lab Service]";
                if (chkPrintLabService.Checked) useraccount.AccessRole += "[Print Lab Service]";

                if (chkExternalLetter.Checked) useraccount.AccessRole += "[External Letter]";
                if (chkNewExternalLetter.Checked) useraccount.AccessRole += "[New External Letter]";
                if (chkEditExternalLetter.Checked) useraccount.AccessRole += "[Edit External Letter]";
                if (chkDeleteExternalLetter.Checked) useraccount.AccessRole += "[Delete External Letter]";

                if (chkInternalLetter.Checked) useraccount.AccessRole += "[Internal Letter]";
                if (chkNewInternalLetter.Checked) useraccount.AccessRole += "[New Internal Letter]";
                if (chkEditInternalLetter.Checked) useraccount.AccessRole += "[Edit Internal Letter]";
                if (chkDeleteInternalLetter.Checked) useraccount.AccessRole += "[Delete Internal Letter]";

                if (chkPOtoSupplier.Checked) useraccount.AccessRole += "[PO to Supplier]";
                if (chkNewPOtoSupplier.Checked) useraccount.AccessRole += "[New PO to Supplier]";
                if (chkEditPOtoSupplier.Checked) useraccount.AccessRole += "[Edit PO to Supplier]";
                if (chkDeletePOtoSupplier.Checked) useraccount.AccessRole += "[Delete PO to Supplier]";
                if (chkChangeDeliveryStatus.Checked) useraccount.AccessRole += "[Change Delivery Status]";

                if (chkPOtoSupplierRegular.Checked) useraccount.AccessRole += "[PO to Supplier Regular]";
                if (chkNewPOtoSupplierRegular.Checked) useraccount.AccessRole += "[New PO to Supplier Regular]";
                if (chkEditPOtoSupplierRegular.Checked) useraccount.AccessRole += "[Edit PO to Supplier Regular]";
                if (chkDeletePOtoSupplierRegular.Checked) useraccount.AccessRole += "[Delete PO to Supplier Regular]";
                if (chkPrintPOtoSupplierRegular.Checked) useraccount.AccessRole += "[Print PO to Supplier Regular]";
                if (chkApprovePOtoSupplierRegular.Checked) useraccount.AccessRole += "[Approve PO to Supplier Regular]";

                if (chkPOtoSupplierExpedisi.Checked) useraccount.AccessRole += "[PO to Supplier Expedisi]";
                if (chkNewPOtoSupplierExpedisi.Checked) useraccount.AccessRole += "[New PO to Supplier Expedisi]";
                if (chkEditPOtoSupplierExpedisi.Checked) useraccount.AccessRole += "[Edit PO to Supplier Expedisi]";
                if (chkDeletePOtoSupplierExpedisi.Checked) useraccount.AccessRole += "[Delete PO to Supplier Expedisi]";
                if (chkPrintPOtoSupplierExpedisi.Checked) useraccount.AccessRole += "[Print PO to Supplier Expedisi]";
                if (chkApprovePOtoSupplierExpedisi.Checked) useraccount.AccessRole += "[Approve PO to Supplier Expedisi]";

                if (chkInquiryIn.Checked) useraccount.AccessRole += "[Inquiry In]";
                if (chkNewInquiryIn.Checked) useraccount.AccessRole += "[New Inquiry In]";
                if (chkEditInquiryIn.Checked) useraccount.AccessRole += "[Edit Inquiry In]";
                if (chkDeleteInquiryIn.Checked) useraccount.AccessRole += "[Delete Inquiry In]";

                if (chkInquiryOut.Checked) useraccount.AccessRole += "[Inquiry Out]";
                if (chkNewInquiryOut.Checked) useraccount.AccessRole += "[New Inquiry Out]";
                if (chkEditInquiryOut.Checked) useraccount.AccessRole += "[Edit Inquiry Out]";
                if (chkDeleteInquiryOut.Checked) useraccount.AccessRole += "[Delete Inquiry Out]";

                if (chkQuotationIn.Checked) useraccount.AccessRole += "[Quotation In]";
                if (chkNewQuotationIn.Checked) useraccount.AccessRole += "[New Quotation In]";
                if (chkEditQuotationIn.Checked) useraccount.AccessRole += "[Edit Quotation In]";
                if (chkDeleteQuotationIn.Checked) useraccount.AccessRole += "[Delete Quotation In]";

                if (chkQuotationOut.Checked) useraccount.AccessRole += "[Quotation Out]";
                if (chkNewQuotationOut.Checked) useraccount.AccessRole += "[New Quotation Out]";
                if (chkEditQuotationOut.Checked) useraccount.AccessRole += "[Edit Quotation Out]";
                if (chkDeleteQuotationOut.Checked) useraccount.AccessRole += "[Delete Quotation Out]";

                if (chkPPB.Checked) useraccount.AccessRole += "[PPB]";
                if (chkNewPPB.Checked) useraccount.AccessRole += "[New PPB]";
                if (chkEditPPB.Checked) useraccount.AccessRole += "[Edit PPB]";
                if (chkDeletePPB.Checked) useraccount.AccessRole += "[Delete PPB]";
                if (chkSetPONumberPPB.Checked) useraccount.AccessRole += "[Set PO Number PPB]";

                if (chkPPS.Checked) useraccount.AccessRole += "[PPS]";
                if (chkNewPPS.Checked) useraccount.AccessRole += "[New PPS]";
                if (chkEditPPS.Checked) useraccount.AccessRole += "[Edit PPS]";
                if (chkDeletePPS.Checked) useraccount.AccessRole += "[Delete PPS]";
                

                if (chkSalesOrderReport.Checked) useraccount.AccessRole += "[Sales Order Report]";
                if (chkAutoNumberReport.Checked) useraccount.AccessRole += "[Auto Number Report]";
                if (chkUserLogReport.Checked) useraccount.AccessRole += "[User Log Report]";
                if (chkExpenseReport.Checked) useraccount.AccessRole += "[Expense Report]";
                if (chkCustomerSatisfactionReport.Checked) useraccount.AccessRole += "[Customer Satisfaction Report]";
                if (chkCustomerComplaintReport.Checked) useraccount.AccessRole += "[Customer Complaint Report]";
                if (chkCustomerVoiceReport.Checked) useraccount.AccessRole += "[Customer Voice Report]";
                if (chkPPBReport.Checked) useraccount.AccessRole += "[PPB Report]";
                if (chkPPSReport.Checked) useraccount.AccessRole += "[PPS Report]";
                if (chkExternalLetterReport.Checked) useraccount.AccessRole += "[External Letter Report]";
                if (chkInternalLetterReport.Checked) useraccount.AccessRole += "[Internal Letter Report]";
                if (chkPOtoSupplierReport.Checked) useraccount.AccessRole += "[PO to Supplier Report]";
                if (chkQuotationInReport.Checked) useraccount.AccessRole += "[Quotation In Report]";
                if (chkQuotationOutReport.Checked) useraccount.AccessRole += "[Quotation Out Report]";
                if (chkInquiryInReport.Checked) useraccount.AccessRole += "[Inquiry In Report]";
                if (chkInquiryOutReport.Checked) useraccount.AccessRole += "[Inquiry Out Report]";
                if (chkSalesOrderSummary.Checked) useraccount.AccessRole += "[Sales Order Summary]";
                if (chkDeliveryOrderSummary.Checked) useraccount.AccessRole += "[Delivery Order Summary]";
                if (chkSalesInvoiceSummary.Checked) useraccount.AccessRole += "[Sales Invoice Summary]";
                if (chkSalesOrderDetail.Checked) useraccount.AccessRole += "[Sales Order Detail]";
                if (chkDeliveryOrderLocation.Checked) useraccount.AccessRole += "[Delivery Order Location]";
                if (chkPenyerahanProduksiReport.Checked) useraccount.AccessRole += "[Penyerahan Produksi Report]";
                if (chkStockReport.Checked) useraccount.AccessRole += "[Stock Report]";
                if (chkPIMReport.Checked) useraccount.AccessRole += "[PIM Report]";

                if (chkAdministrator.Checked) useraccount.AccessRole += "[Administrator]";
                if (chkManager.Checked) useraccount.AccessRole += "[Manager]";
                if (chkUser.Checked) useraccount.AccessRole += "[User]";

                if (chkConfiguration.Checked) useraccount.AccessRole += "[Configuration]";
                if (chkDesignReport.Checked) useraccount.AccessRole += "[Design Report]";
                if (chkSettingGudang.Checked) useraccount.AccessRole += "[Setting Gudang]";

                if (_id == null) db.UserAccounts.InsertOnSubmit(useraccount);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                EditMode(false);
                RefreshData();
                gridControl.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            RefreshDetail();
        }

        private void btnGroup_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDataGroup form = new frmDataGroup();
            form.ShowDialog();
            db = new GreenChemDataContext();
            cmbGroup.Properties.DataSource = db.Groups.GetName();
            Cursor.Current = Cursors.Default;
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }

    }
}