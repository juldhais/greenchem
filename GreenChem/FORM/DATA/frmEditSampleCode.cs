﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmEditSampleCode : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditSampleCode(int id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            //cmbTipe.Properties.DataSource = db.Tipes.GetName();
            //cmbFisik.Properties.DataSource = db.Fisiks.GetName();
            cmbSupplier.Properties.DataSource = db.SupplierSCs.GetName();

            var sample = db.SampleCodes.First(x => x.id == _id);
            txtKode.Text = sample.Kode;
            txtNomor.Text = sample.Nomor;
            txtNama.Text = sample.Nama;
            txtKodeMSDS.Text = sample.KodeMSDS;
            //cmbFisik.EditValue = bahanbaku.idFisik;
            //cmbTipe.EditValue = bahanbaku.idTipe;
            txtFisik.Text = sample.FisikText;
            txtTipe.Text = sample.TipeText;
            txtFungsi.Text = sample.Fungsi;
            cmbSupplier.EditValue = sample.idSupplierSC;
            txtQuantity.EditValue = sample.Quantity;
            cmbTanggalPenerimaan.EditValue = sample.TanggalPenerimaan;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                SampleCode samplecode = db.SampleCodes.First(x => x.id == _id);
                //if (cmbFisik.EditValue.IsNotEmpty()) samplecode.idFisik = cmbFisik.EditValue.ToInteger();
                //if (cmbTipe.EditValue.IsNotEmpty()) samplecode.idTipe = cmbTipe.EditValue.ToInteger();
                if (cmbSupplier.EditValue.IsNotEmpty()) samplecode.idSupplierSC = cmbSupplier.EditValue.ToInteger();
                samplecode.FisikText = txtFisik.Text;
                samplecode.TipeText = txtTipe.Text;
                samplecode.Nomor = txtNomor.Text;
                samplecode.Kode = txtKode.Text;
                samplecode.Nama = txtNama.Text;
                samplecode.KodeMSDS = txtKodeMSDS.Text;
                samplecode.Fungsi = txtFungsi.Text;
                samplecode.Quantity = txtQuantity.EditValue.ToDecimal();
                samplecode.TanggalPenerimaan = (DateTime?)cmbTanggalPenerimaan.EditValue;
                samplecode.Active = true;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}