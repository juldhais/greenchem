﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmDataDepartment : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataDepartment()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            departmentBindingSource.DataSource = db.Departments;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetRowCellValue(e.RowHandle, "Active", true);
        }

        private void btnSubDepartment_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            db.SubmitChanges();
            int iddepartment = gridView.GetFocusedRowCellValue("id").ToInteger();
            string name = gridView.GetFocusedRowCellValue("Name").Safe();
            frmDataSubDepartment form = new frmDataSubDepartment(iddepartment);
            form.Text = "Sub Department : " + name;
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }
    }
}