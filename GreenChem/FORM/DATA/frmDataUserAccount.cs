﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmDataUserAccount : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        GreenChemDataContext db;

        public frmDataUserAccount()
        {
            InitializeComponent();
            RefreshData();
            EditMode(false);
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbDepartment.Properties.DataSource = db.Departments.GetName();
                cmbSuDepartment.Properties.DataSource = db.SubDepartments.GetName();
                cmbGroup.Properties.DataSource = db.Groups.GetName();

                var query = from x in db.UserAccounts
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.Name
                            };

                gridControl.DataSource = query;
                RefreshDetail();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void RefreshDetail()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                _id = gridView.GetFocusedRowCellValue("id").ToInteger();
                db = new GreenChemDataContext();

                var useraccount = db.UserAccounts.First(x => x.id == _id);
                txtUserName.Text = useraccount.Name;
                txtName.Text = useraccount.UserName;
                txtPassword.Text = useraccount.Password;
                cmbDepartment.EditValue = useraccount.idDepartment;
                cmbSuDepartment.EditValue = useraccount.idSubDepartment;
                cmbGroup.EditValue = useraccount.idGroup;
                chkProduct.Checked = useraccount.AccessRole.Contains("[Product]");
                chkCategory.Checked = useraccount.AccessRole.Contains("[Category]");
                chkDepartment.Checked = useraccount.AccessRole.Contains("[Department]");
                chkContactList.Checked = useraccount.AccessRole.Contains("[ContactList]");
                chkSupplier.Checked = useraccount.AccessRole.Contains("[Supplier]");
                chkCustomer.Checked = useraccount.AccessRole.Contains("[Customer]");
                chkExpedition.Checked = useraccount.AccessRole.Contains("[Expedition]");
                chkLocation.Checked = useraccount.AccessRole.Contains("[Location]");
                chkCurrency.Checked = useraccount.AccessRole.Contains("[Currency]");
                chkUserAccount.Checked = useraccount.AccessRole.Contains("[User Account]");
                chkBahanBaku.Checked = useraccount.AccessRole.Contains("[Bahan Baku]");
                chkBahanPenolong.Checked = useraccount.AccessRole.Contains("[Bahan Penolong]");
                chkEmployee.Checked = useraccount.AccessRole.Contains("[Employee]");
                chkEditStatusProduct.Checked = useraccount.AccessRole.Contains("[Edit Status Product]");
                chkSampleCode.Checked = useraccount.AccessRole.Contains("[Sample Code]");

                chkSalesOrder.Checked = useraccount.AccessRole.Contains("[Sales Order]");
                chkDeliveryOrder.Checked = useraccount.AccessRole.Contains("[Delivery Order]");
                chkSalesInvoice.Checked = useraccount.AccessRole.Contains("[Sales Invoice]");
                chkStockAdjustment.Checked = useraccount.AccessRole.Contains("[Stock Adjustment]");
                chkExternalLetter.Checked = useraccount.AccessRole.Contains("[General Letter]");
                chkInternalLetter.Checked = useraccount.AccessRole.Contains("[Internal Letter]");
                chkPOtoSupplier.Checked = useraccount.AccessRole.Contains("[PO to Supplier]");
                chkInquiryIn.Checked = useraccount.AccessRole.Contains("[Inquiry In]");
                chkInquiryOut.Checked = useraccount.AccessRole.Contains("[Inquiry Out]");
                chkQuotationIn.Checked = useraccount.AccessRole.Contains("[Quotation In]");
                chkQuotationOut.Checked = useraccount.AccessRole.Contains("[Quotation Out]");
                chkExpense.Checked = useraccount.AccessRole.Contains("[Expense]");
                chkCustomerSatisfaction.Checked = useraccount.AccessRole.Contains("[Customer Satisfaction]");
                chkCustomerComplaint.Checked = useraccount.AccessRole.Contains("[Customer Complaint]");
                chkCustomerVoice.Checked = useraccount.AccessRole.Contains("[Customer Voice]");
                chkPPB.Checked = useraccount.AccessRole.Contains("[PPB]");
                chkPPS.Checked = useraccount.AccessRole.Contains("[PPS]");
                chkCreateInvoice.Checked = useraccount.AccessRole.Contains("[Create Invoice]");
                chkCancelInvoice.Checked = useraccount.AccessRole.Contains("[Cancel Invoice]");
                chkPrintInvoice.Checked = useraccount.AccessRole.Contains("[Print Invoice]");
                chkPayInvoice.Checked = useraccount.AccessRole.Contains("[Pay Invoice]");
                chkApproval.Checked = useraccount.AccessRole.Contains("[Approval]");
                chkPenyerahanProduksi.Checked = useraccount.AccessRole.Contains("[Penyerahan Produksi]");

                chkSalesOrderReport.Checked = useraccount.AccessRole.Contains("[Sales Order Report]");
                chkAutoNumberReport.Checked = useraccount.AccessRole.Contains("[Auto Number Report]");
                chkUserLogReport.Checked = useraccount.AccessRole.Contains("[User Log Report]");
                chkExpenseReport.Checked = useraccount.AccessRole.Contains("[Expense Report]");
                chkCustomerSatisfactionReport.Checked = useraccount.AccessRole.Contains("[Customer Satisfaction Report]");
                chkCustomerComplaintReport.Checked = useraccount.AccessRole.Contains("[Customer Complaint Report]");
                chkPPBReport.Checked = useraccount.AccessRole.Contains("[PPB Report]");
                chkPPSReport.Checked = useraccount.AccessRole.Contains("[PPS Report]");
                chkExternalLetterReport.Checked = useraccount.AccessRole.Contains("[External Letter Report]");
                chkInternalLetterReport.Checked = useraccount.AccessRole.Contains("[Internal Letter Report]");
                chkPOtoSupplierReport.Checked = useraccount.AccessRole.Contains("[PO to Supplier Report]");
                chkQuotationInReport.Checked= useraccount.AccessRole.Contains("[Quotation In Report]");
                chkQuotationOutReport.Checked = useraccount.AccessRole.Contains("[Quotation Out Report]");
                chkInquiryInReport.Checked = useraccount.AccessRole.Contains("[Inquiry In Report]");
                chkInquiryOutReport.Checked = useraccount.AccessRole.Contains("[Inquiry Out Report]");
                chkSalesOrderSummary.Checked = useraccount.AccessRole.Contains("[Sales Order Summary]");
                chkDeliveryOrderSummary.Checked = useraccount.AccessRole.Contains("[Delivery Order Summary]");
                chkSalesInvoiceSummary.Checked = useraccount.AccessRole.Contains("[Sales Invoice Summary]");
                chkSalesOrderDetail.Checked = useraccount.AccessRole.Contains("[Sales Order Detail]");
                chkDeliveryOrderLocation.Checked = useraccount.AccessRole.Contains("[Delivery Order Location]");

                chkAllowChangeLocation.Checked = useraccount.AccessRole.Contains("[Allow Change Location]");
                chkAllowChangeStatus.Checked = useraccount.AccessRole.Contains("[Allow Change Status]");
                chkAllowEditContact.Checked = useraccount.AccessRole.Contains("[Allow Edit Contact]");
                chkAllowEditDeposition.Checked = useraccount.AccessRole.Contains("[Allow Edit Deposition]");
                chkContactVisibility.Checked = useraccount.AccessRole.Contains("[Contact Visibility]");
                chkNew.Checked = useraccount.AccessRole.Contains("[New]");
                chkEdit.Checked = useraccount.AccessRole.Contains("[Edit]");
                chkDelete.Checked = useraccount.AccessRole.Contains("[Delete]");
                chkReport.Checked = useraccount.AccessRole.Contains("[Report]");
                chkConfiguration.Checked = useraccount.AccessRole.Contains("[Configuration]");
                chkAdministrator.Checked = useraccount.AccessRole.Contains("[Administrator]");
                chkManager.Checked = useraccount.AccessRole.Contains("[Manager]");
                chkUser.Checked = useraccount.AccessRole.Contains("[User]");
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void EditMode(bool isedit)
        {
            gridControl.Enabled = !isedit;
            txtUserName.Enabled = isedit;
            txtName.Enabled = isedit;
            txtPassword.Enabled = isedit;
            cmbDepartment.Enabled = isedit;
            cmbSuDepartment.Enabled = isedit;
            cmbGroup.Enabled = isedit;
            chkProduct.Enabled = isedit;
            chkCategory.Enabled = isedit;
            chkDepartment.Enabled = isedit;
            chkContactList.Enabled = isedit;
            chkSupplier.Enabled = isedit;
            chkCustomer.Enabled = isedit;
            chkExpedition.Enabled = isedit;
            chkLocation.Enabled = isedit;
            chkCurrency.Enabled = isedit;
            chkUserAccount.Enabled = isedit;
            chkBahanBaku.Enabled = isedit;
            chkBahanPenolong.Enabled = isedit;
            chkEmployee.Enabled = isedit;
            chkEditStatusProduct.Enabled = isedit;
            chkSampleCode.Enabled = isedit;

            chkSalesOrder.Enabled = isedit;
            chkDeliveryOrder.Enabled = isedit;
            chkSalesInvoice.Enabled = isedit;
            chkStockAdjustment.Enabled = isedit;
            chkExternalLetter.Enabled = isedit;
            chkInternalLetter.Enabled = isedit;
            chkPOtoSupplier.Enabled = isedit;
            chkInquiryIn.Enabled = isedit;
            chkInquiryOut.Enabled = isedit;
            chkQuotationIn.Enabled = isedit;
            chkQuotationOut.Enabled = isedit;
            chkExpense.Enabled = isedit;
            chkCustomerSatisfaction.Enabled = isedit;
            chkCustomerComplaint.Enabled = isedit;
            chkCustomerVoice.Enabled = isedit;
            chkPPB.Enabled = isedit;
            chkPPS.Enabled = isedit;
            chkCreateInvoice.Enabled = isedit;
            chkCancelInvoice.Enabled = isedit;
            chkPrintInvoice.Enabled = isedit;
            chkPayInvoice.Enabled = isedit;
            chkApproval.Enabled = isedit;
            chkPenyerahanProduksi.Enabled= isedit;

            chkSalesOrderReport.Enabled = isedit;
            chkAutoNumberReport.Enabled = isedit;
            chkUserLogReport.Enabled = isedit;
            chkExpenseReport.Enabled = isedit;
            chkCustomerSatisfactionReport.Enabled = isedit;
            chkCustomerComplaintReport.Enabled = isedit;
            chkPPBReport.Enabled = isedit;
            chkPPSReport.Enabled = isedit;
            chkExternalLetterReport.Enabled = isedit;
            chkInternalLetterReport.Enabled = isedit;
            chkPOtoSupplierReport.Enabled = isedit;
            chkQuotationInReport.Enabled = isedit;
            chkQuotationOutReport.Enabled = isedit;
            chkInquiryInReport.Enabled = isedit;
            chkInquiryOutReport.Enabled = isedit;
            chkSalesOrderSummary.Enabled = isedit;
            chkDeliveryOrderSummary.Enabled = isedit;
            chkSalesInvoiceSummary.Enabled = isedit;
            chkSalesOrderDetail.Enabled = isedit;
            chkDeliveryOrderLocation.Enabled = isedit;

            chkAllowChangeLocation.Enabled = isedit;
            chkAllowChangeStatus.Enabled = isedit;
            chkAllowEditContact.Enabled = isedit;
            chkContactVisibility.Enabled = isedit;
            chkAllowEditDeposition.Enabled = isedit;

            chkNew.Enabled = isedit;
            chkEdit.Enabled = isedit;
            chkDelete.Enabled = isedit;
            chkReport.Enabled = isedit;
            chkConfiguration.Enabled = isedit;
            chkAdministrator.Enabled = isedit;
            chkManager.Enabled = isedit;
            chkUser.Enabled = isedit;

            btnNew.Enabled = !isedit;
            btnEdit.Enabled = !isedit;
            btnDelete.Enabled = !isedit;
            btnSave.Enabled = isedit;
            btnCancel.Enabled = isedit;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            EditMode(true);
            _id = null;
            txtUserName.Text = string.Empty;
            txtName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            cmbDepartment.EditValue = null;
            cmbSuDepartment.EditValue = null;
            cmbGroup.EditValue = null;
            chkProduct.Checked = true;
            chkCategory.Checked = true;
            chkDepartment.Checked = true;
            chkContactList.Checked = true;
            chkSupplier.Checked = true;
            chkCustomer.Checked = true;
            chkExpedition.Checked = true;
            chkLocation.Checked = true;
            chkCurrency.Checked = true;
            chkUserAccount.Checked = true;
            chkBahanBaku.Checked = true;
            chkBahanPenolong.Checked = true;
            chkEmployee.Checked = true;
            chkEditStatusProduct.Checked = true;
            chkSampleCode.Checked = true;

            chkSalesOrder.Checked = true;
            chkDeliveryOrder.Checked = true;
            chkSalesInvoice.Checked = true;
            chkExternalLetter.Checked = true;
            chkInternalLetter.Checked = true;
            chkPOtoSupplier.Checked = true;
            chkStockAdjustment.Checked = true;
            chkInquiryIn.Checked = true;
            chkInquiryOut.Checked = true;
            chkQuotationIn.Checked = true;
            chkQuotationOut.Checked = true;
            chkExpense.Checked = true;
            chkCustomerSatisfaction.Checked = true;
            chkCustomerComplaint.Checked = true;
            chkCustomerVoice.Checked = true;
            chkPPB.Checked = true;
            chkPPS.Checked = true;
            chkCreateInvoice.Checked = true;
            chkCancelInvoice.Checked = true;
            chkPrintInvoice.Checked = true;
            chkPayInvoice.Checked = true;
            chkApproval.Checked = true;
            chkPenyerahanProduksi.Checked = true;

            chkSalesOrderReport.Checked = true;
            chkAutoNumberReport.Checked = true;
            chkUserLogReport.Checked = true;
            chkExpenseReport.Checked = true;
            chkCustomerSatisfactionReport.Checked = true;
            chkCustomerComplaint.Checked = true;
            chkPPBReport.Checked = true;
            chkPPSReport.Checked = true;
            chkExternalLetterReport.Checked = true;
            chkInternalLetterReport.Checked = true;
            chkPOtoSupplierReport.Checked = true;
            chkQuotationInReport.Checked = true;
            chkQuotationOutReport.Checked = true;
            chkInquiryInReport.Checked = true;
            chkInquiryOutReport.Checked = true;
            chkSalesOrderSummary.Checked = true;
            chkDeliveryOrderSummary.Checked = true;
            chkSalesInvoiceSummary.Checked = true;
            chkSalesOrderDetail.Checked = true;
            chkDeliveryOrderLocation.Checked = true;

            chkAllowChangeLocation.Checked = true;
            chkAllowChangeStatus.Checked = true;
            chkAllowEditContact.Checked = true;
            chkAllowEditDeposition.Checked = true;
            chkContactVisibility.Checked = true;
            chkAdministrator.Checked = true;
            chkManager.Checked = true;
            chkUser.Checked = true;

            chkNew.Checked = true;
            chkEdit.Checked = true;
            chkDelete.Checked = true;
            chkReport.Checked = true;
            chkConfiguration.Checked = true;
            txtUserName.Focus();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditMode(true);
            RefreshDetail();
            txtUserName.Focus();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshDetail();
                string name = gridView.GetFocusedRowCellValue("Name").Safe(); ;
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this user : " + name, Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    UserAccount useraccount = db.UserAccounts.First(x => x.id == _id);
                    useraccount.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                if (db.UserAccounts.Any(x => x.Name == txtUserName.Text && x.Active == true && x.id != _id))
                {
                    XtraMessageBox.Show("User name already used.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUserName.Focus();
                    return;
                }

                UserAccount useraccount = _id == null ? new UserAccount() : db.UserAccounts.First(x => x.id == _id);
                useraccount.Name = txtUserName.Text;
                useraccount.UserName = txtName.Text;
                useraccount.Password = txtPassword.Text;
                useraccount.idDepartment = (int?)cmbDepartment.EditValue;
                useraccount.idSubDepartment = (int?)cmbSuDepartment.EditValue;
                useraccount.idGroup = (int?)cmbGroup.EditValue;
                useraccount.AccessRole = string.Empty;
                useraccount.Active = true;

                if (chkProduct.Checked) useraccount.AccessRole += "[Product]";
                if (chkCategory.Checked) useraccount.AccessRole += "[Category]";
                if (chkDepartment.Checked) useraccount.AccessRole += "[Department]";
                if (chkContactList.Checked) useraccount.AccessRole += "[ContactList]";
                if (chkSupplier.Checked) useraccount.AccessRole += "[Supplier]";
                if (chkCustomer.Checked) useraccount.AccessRole += "[Customer]";
                if (chkExpedition.Checked) useraccount.AccessRole += "[Expedition]";
                if (chkLocation.Checked) useraccount.AccessRole += "[Location]";
                if (chkCurrency.Checked) useraccount.AccessRole += "[Currency]";
                if (chkUserAccount.Checked) useraccount.AccessRole += "[User Account]";
                if (chkBahanBaku.Checked) useraccount.AccessRole += "[Bahan Baku]";
                if (chkBahanPenolong.Checked) useraccount.AccessRole += "[Bahan Penolong]";
                if (chkEmployee.Checked) useraccount.AccessRole += "[Employee]";
                if (chkEditStatusProduct.Checked) useraccount.AccessRole += "[Edit Status Product]";
                if (chkSampleCode.Checked) useraccount.AccessRole += "[Sample Code]";

                if (chkSalesOrder.Checked) useraccount.AccessRole += "[Sales Order]";
                if (chkDeliveryOrder.Checked) useraccount.AccessRole += "[Delivery Order]";
                if (chkSalesInvoice.Checked) useraccount.AccessRole += "[Sales Invoice]";
                if (chkStockAdjustment.Checked) useraccount.AccessRole += "[Stock Adjustment]";
                if (chkExternalLetter.Checked) useraccount.AccessRole += "[General Letter]";
                if (chkInternalLetter.Checked) useraccount.AccessRole += "[Internal Letter]";
                if (chkPOtoSupplier.Checked) useraccount.AccessRole += "[PO to Supplier]";
                if (chkInquiryIn.Checked) useraccount.AccessRole += "[Inquiry In]";
                if (chkInquiryOut.Checked) useraccount.AccessRole += "[Inquiry Out]";
                if (chkQuotationIn.Checked) useraccount.AccessRole += "[Quotation In]";
                if (chkQuotationOut.Checked) useraccount.AccessRole += "[Quotation Out]";
                if (chkExpense.Checked) useraccount.AccessRole += "[Expense]";
                if (chkCustomerSatisfaction.Checked) useraccount.AccessRole += "[Customer Satisfaction]";
                if (chkCustomerComplaint.Checked) useraccount.AccessRole += "[Customer Complaint]";
                if (chkCustomerVoice.Checked) useraccount.AccessRole += "[Customer Voice]";
                if (chkPPB.Checked) useraccount.AccessRole += "[PPB]";
                if (chkPPS.Checked) useraccount.AccessRole += "[PPS]";
                if (chkCreateInvoice.Checked) useraccount.AccessRole += "[Create Invoice]";
                if (chkCancelInvoice.Checked) useraccount.AccessRole += "[Cancel Invoice]";
                if (chkPrintInvoice.Checked) useraccount.AccessRole += "[Print Invoice]";
                if (chkPayInvoice.Checked) useraccount.AccessRole += "[Pay Invoice]";
                if (chkApproval.Checked) useraccount.AccessRole += "[Approval]";
                if (chkPenyerahanProduksi.Checked) useraccount.AccessRole += "[Penyerahan Produksi]";

                if (chkSalesOrderReport.Checked) useraccount.AccessRole += "[Sales Order Report]";
                if (chkAutoNumberReport.Checked) useraccount.AccessRole += "[Auto Number Report]";
                if (chkUserLogReport.Checked) useraccount.AccessRole += "[User Log Report]";
                if (chkExpenseReport.Checked) useraccount.AccessRole += "[Expense Report]";
                if (chkCustomerSatisfactionReport.Checked) useraccount.AccessRole += "[Customer Satisfaction Report]";
                if (chkCustomerComplaintReport.Checked) useraccount.AccessRole += "[Customer Complaint Report]";
                if (chkPPBReport.Checked) useraccount.AccessRole += "[PPB Report]";
                if (chkPPSReport.Checked) useraccount.AccessRole += "[PPS Report]";
                if (chkExternalLetterReport.Checked) useraccount.AccessRole += "[External Letter Report]";
                if (chkInternalLetterReport.Checked) useraccount.AccessRole += "[Internal Letter Report]";
                if (chkPOtoSupplierReport.Checked) useraccount.AccessRole += "[PO to Supplier Report]";
                if (chkQuotationInReport.Checked) useraccount.AccessRole += "[Quotation In Report]";
                if (chkQuotationOutReport.Checked) useraccount.AccessRole += "[Quotation Out Report]";
                if (chkInquiryInReport.Checked) useraccount.AccessRole += "[Inquiry In Report]";
                if (chkInquiryOutReport.Checked) useraccount.AccessRole += "[Inquiry Out Report]";
                if (chkSalesOrderSummary.Checked) useraccount.AccessRole += "[Sales Order Summary]";
                if (chkDeliveryOrderSummary.Checked) useraccount.AccessRole += "[Delivery Order Summary]";
                if (chkSalesInvoiceSummary.Checked) useraccount.AccessRole += "[Sales Invoice Summary]";
                if (chkSalesOrderDetail.Checked) useraccount.AccessRole += "[Sales Order Detail]";
                if (chkDeliveryOrderLocation.Checked) useraccount.AccessRole += "[Delivery Order Location]";

                if (chkAllowChangeLocation.Checked) useraccount.AccessRole += "[Allow Change Location]";
                if (chkAllowChangeStatus.Checked) useraccount.AccessRole += "[Allow Change Status]";
                if (chkAllowEditContact.Checked) useraccount.AccessRole += "[Allow Edit Contact]";
                if (chkAllowEditDeposition.Checked) useraccount.AccessRole += "[Allow Edit Deposition]";
                if (chkContactVisibility.Checked) useraccount.AccessRole += "[Contact Visibility]";
                if (chkAdministrator.Checked) useraccount.AccessRole += "[Administrator]";
                if (chkManager.Checked) useraccount.AccessRole += "[Manager]";
                if (chkUser.Checked) useraccount.AccessRole += "[User]";

                if (chkNew.Checked) useraccount.AccessRole += "[New]";
                if (chkEdit.Checked) useraccount.AccessRole += "[Edit]";
                if (chkDelete.Checked) useraccount.AccessRole += "[Delete]";
                if (chkReport.Checked) useraccount.AccessRole += "[Report]";
                if (chkConfiguration.Checked) useraccount.AccessRole += "[Configuration]";
                
                if (_id == null) db.UserAccounts.InsertOnSubmit(useraccount);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                EditMode(false);
                RefreshData();
                gridControl.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            RefreshDetail();
        }

        private void btnGroup_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDataGroup form = new frmDataGroup();
            form.ShowDialog();
            db = new GreenChemDataContext();
            cmbGroup.Properties.DataSource = db.Groups.GetName();
            Cursor.Current = Cursors.Default;
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}