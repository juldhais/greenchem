﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmSampleCode : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmSampleCode()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Sample Code");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Sample Code");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Sample Code");
            btnRecap.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Recap Sample Code");
            btnFisik.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Fisik Sample Code");
            btnTipe.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Tipe Sample Code");

            if (System.IO.File.Exists("layout_samplecode.xml"))
                gridView.RestoreLayoutFromXml("layout_samplecode.xml");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.SampleCodes
                            where x.Active == true
                            orderby x.Nama
                            select new
                            {
                                x.id,
                                x.Nomor,
                                x.Kode,
                                x.KodeMSDS,
                                x.Nama,
                                Fisik = x.FisikText,
                                Tipe = x.TipeText,
                                x.Fungsi,
                                Supplier = x.SupplierSC.Name,
                                Address = x.SupplierSC.Address,
                                Phone = x.SupplierSC.Phone,
                                Fax = x.SupplierSC.Fax,
                                Website = x.SupplierSC.Website,
                                Person = x.SupplierSC.Person,
                                x.Quantity,
                                x.TanggalPenerimaan
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Nomor == txtSearch.Text ||
                            x.Kode == txtSearch.Text ||
                            x.Nama.Contains(txtSearch.Text) ||
                            x.Supplier.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewSampleCode form = new frmNewSampleCode();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditSampleCode form = new frmEditSampleCode(id);
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this item : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    SampleCode bahan = db.SampleCodes.First(x => x.id == id);
                    bahan.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("item deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            //MenuReport.frmReportSampleCode form = new MenuReport.frmReportSampleCode();
            //form.Show();
            //form.BringToFront();

            try
            {
                gridControl.ShowPrintPreview();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }

        private void btnFisik_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDataFisik form = new frmDataFisik();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnTipe_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmTipe form = new frmTipe();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_samplecode.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}