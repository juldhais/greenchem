﻿namespace GreenChem.MenuData
{
    partial class frmNewSampleCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTipe = new DevExpress.XtraEditors.TextEdit();
            this.txtFisik = new DevExpress.XtraEditors.TextEdit();
            this.txtKodeMSDS = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSupplier = new DevExpress.XtraEditors.LookUpEdit();
            this.txtFungsi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtNomor = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtNama = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtKode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtQuantity = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTanggalPenerimaan = new DevExpress.XtraEditors.DateEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisik.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKodeMSDS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFungsi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPenerimaan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPenerimaan.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmbTanggalPenerimaan);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtQuantity);
            this.panelControl1.Controls.Add(this.txtTipe);
            this.panelControl1.Controls.Add(this.txtFisik);
            this.panelControl1.Controls.Add(this.txtKodeMSDS);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.cmbSupplier);
            this.panelControl1.Controls.Add(this.txtFungsi);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtNomor);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtNama);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtKode);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 262);
            this.panelControl1.TabIndex = 0;
            // 
            // txtTipe
            // 
            this.txtTipe.Location = new System.Drawing.Point(75, 135);
            this.txtTipe.Name = "txtTipe";
            this.txtTipe.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTipe.Properties.Appearance.Options.UseFont = true;
            this.txtTipe.Size = new System.Drawing.Size(373, 24);
            this.txtTipe.TabIndex = 5;
            // 
            // txtFisik
            // 
            this.txtFisik.Location = new System.Drawing.Point(75, 105);
            this.txtFisik.Name = "txtFisik";
            this.txtFisik.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFisik.Properties.Appearance.Options.UseFont = true;
            this.txtFisik.Size = new System.Drawing.Size(373, 24);
            this.txtFisik.TabIndex = 4;
            // 
            // txtKodeMSDS
            // 
            this.txtKodeMSDS.Location = new System.Drawing.Point(75, 75);
            this.txtKodeMSDS.Name = "txtKodeMSDS";
            this.txtKodeMSDS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKodeMSDS.Properties.Appearance.Options.UseFont = true;
            this.txtKodeMSDS.Size = new System.Drawing.Size(373, 24);
            this.txtKodeMSDS.TabIndex = 3;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(34, 78);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(35, 17);
            this.labelControl11.TabIndex = 6;
            this.labelControl11.Text = "MSDS";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(21, 198);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(48, 17);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Supplier";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.Location = new System.Drawing.Point(75, 195);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier.Properties.Appearance.Options.UseFont = true;
            this.cmbSupplier.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbSupplier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSupplier.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbSupplier.Properties.DisplayMember = "Name";
            this.cmbSupplier.Properties.NullText = "";
            this.cmbSupplier.Properties.ShowFooter = false;
            this.cmbSupplier.Properties.ShowHeader = false;
            this.cmbSupplier.Properties.ValueMember = "id";
            this.cmbSupplier.Size = new System.Drawing.Size(373, 24);
            this.cmbSupplier.TabIndex = 7;
            // 
            // txtFungsi
            // 
            this.txtFungsi.Location = new System.Drawing.Point(75, 165);
            this.txtFungsi.Name = "txtFungsi";
            this.txtFungsi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFungsi.Properties.Appearance.Options.UseFont = true;
            this.txtFungsi.Size = new System.Drawing.Size(373, 24);
            this.txtFungsi.TabIndex = 6;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(32, 168);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(37, 17);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Fungsi";
            // 
            // txtNomor
            // 
            this.txtNomor.Location = new System.Drawing.Point(348, 15);
            this.txtNomor.Name = "txtNomor";
            this.txtNomor.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNomor.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtNomor.Properties.Appearance.Options.UseFont = true;
            this.txtNomor.Properties.Appearance.Options.UseForeColor = true;
            this.txtNomor.Properties.ReadOnly = true;
            this.txtNomor.Size = new System.Drawing.Size(100, 24);
            this.txtNomor.TabIndex = 1;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(321, 18);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(21, 17);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "No.";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(44, 138);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(25, 17);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Tipe";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(45, 108);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Fisik";
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(75, 45);
            this.txtNama.Name = "txtNama";
            this.txtNama.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNama.Properties.Appearance.Options.UseFont = true;
            this.txtNama.Size = new System.Drawing.Size(373, 24);
            this.txtNama.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(34, 48);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 17);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Nama";
            // 
            // txtKode
            // 
            this.txtKode.Location = new System.Drawing.Point(75, 15);
            this.txtKode.Name = "txtKode";
            this.txtKode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKode.Properties.Appearance.Options.UseFont = true;
            this.txtKode.Size = new System.Drawing.Size(200, 24);
            this.txtKode.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(38, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(31, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Kode";
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(402, 280);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(296, 280);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtQuantity
            // 
            this.txtQuantity.EditValue = "";
            this.txtQuantity.Location = new System.Drawing.Point(75, 225);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQuantity.Properties.Appearance.Options.UseFont = true;
            this.txtQuantity.Properties.Appearance.Options.UseTextOptions = true;
            this.txtQuantity.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtQuantity.Properties.DisplayFormat.FormatString = "n2";
            this.txtQuantity.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQuantity.Properties.EditFormat.FormatString = "n2";
            this.txtQuantity.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQuantity.Properties.Mask.EditMask = "n2";
            this.txtQuantity.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtQuantity.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtQuantity.Size = new System.Drawing.Size(80, 24);
            this.txtQuantity.TabIndex = 8;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(49, 228);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(20, 17);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Qty";
            // 
            // cmbTanggalPenerimaan
            // 
            this.cmbTanggalPenerimaan.EditValue = null;
            this.cmbTanggalPenerimaan.Location = new System.Drawing.Point(298, 225);
            this.cmbTanggalPenerimaan.Name = "cmbTanggalPenerimaan";
            this.cmbTanggalPenerimaan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalPenerimaan.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalPenerimaan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalPenerimaan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalPenerimaan.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalPenerimaan.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(199, 228);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(93, 17);
            this.labelControl9.TabIndex = 17;
            this.labelControl9.Text = "Tgl. Penerimaan";
            // 
            // frmNewSampleCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 322);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewSampleCode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Sample Code";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisik.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKodeMSDS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFungsi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPenerimaan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPenerimaan.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtKodeMSDS;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit cmbSupplier;
        private DevExpress.XtraEditors.TextEdit txtFungsi;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtNomor;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtNama;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtKode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtTipe;
        private DevExpress.XtraEditors.TextEdit txtFisik;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtQuantity;
        private DevExpress.XtraEditors.DateEdit cmbTanggalPenerimaan;
        private DevExpress.XtraEditors.LabelControl labelControl9;
    }
}