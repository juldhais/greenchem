﻿namespace GreenChem.MenuData
{
    partial class frmEditProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaxStock = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtMinStock = new DevExpress.XtraEditors.TextEdit();
            this.lblPI = new DevExpress.XtraEditors.LabelControl();
            this.lblMSDS = new DevExpress.XtraEditors.LabelControl();
            this.btnPI = new DevExpress.XtraEditors.SimpleButton();
            this.btnMSDS = new DevExpress.XtraEditors.SimpleButton();
            this.txtPI = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtMSDS = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatusProduct = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtThirdRatio = new DevExpress.XtraEditors.TextEdit();
            this.txtThirdUOM = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtRatio = new DevExpress.XtraEditors.TextEdit();
            this.txtSecondUoM = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtUoM = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalesPrice5 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalesPrice4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalesPrice3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalesPrice2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.cmbCurrency = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtRemarks = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalesPrice = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtCost = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSupplier = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaxStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSDS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatusProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdRatio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdUOM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecondUoM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUoM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemarks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(552, 490);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(446, 490);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl21);
            this.panelControl1.Controls.Add(this.txtMaxStock);
            this.panelControl1.Controls.Add(this.labelControl20);
            this.panelControl1.Controls.Add(this.txtMinStock);
            this.panelControl1.Controls.Add(this.lblPI);
            this.panelControl1.Controls.Add(this.lblMSDS);
            this.panelControl1.Controls.Add(this.btnPI);
            this.panelControl1.Controls.Add(this.btnMSDS);
            this.panelControl1.Controls.Add(this.txtPI);
            this.panelControl1.Controls.Add(this.labelControl19);
            this.panelControl1.Controls.Add(this.txtMSDS);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.labelControl17);
            this.panelControl1.Controls.Add(this.cmbStatusProduct);
            this.panelControl1.Controls.Add(this.txtThirdRatio);
            this.panelControl1.Controls.Add(this.txtThirdUOM);
            this.panelControl1.Controls.Add(this.labelControl16);
            this.panelControl1.Controls.Add(this.txtRatio);
            this.panelControl1.Controls.Add(this.txtSecondUoM);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.txtUoM);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtSalesPrice5);
            this.panelControl1.Controls.Add(this.labelControl15);
            this.panelControl1.Controls.Add(this.txtSalesPrice4);
            this.panelControl1.Controls.Add(this.labelControl14);
            this.panelControl1.Controls.Add(this.txtSalesPrice3);
            this.panelControl1.Controls.Add(this.labelControl13);
            this.panelControl1.Controls.Add(this.txtSalesPrice2);
            this.panelControl1.Controls.Add(this.labelControl12);
            this.panelControl1.Controls.Add(this.cmbCurrency);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.cmbStatus);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.txtRemarks);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtSalesPrice);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.txtCost);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.cmbSupplier);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.cmbCategory);
            this.panelControl1.Controls.Add(this.txtName);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtCode);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(620, 472);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(452, 287);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(60, 17);
            this.labelControl21.TabIndex = 51;
            this.labelControl21.Text = "Max Stock";
            // 
            // txtMaxStock
            // 
            this.txtMaxStock.EditValue = "";
            this.txtMaxStock.Location = new System.Drawing.Point(518, 284);
            this.txtMaxStock.Name = "txtMaxStock";
            this.txtMaxStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMaxStock.Properties.Appearance.Options.UseFont = true;
            this.txtMaxStock.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMaxStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMaxStock.Properties.DisplayFormat.FormatString = "n2";
            this.txtMaxStock.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMaxStock.Properties.EditFormat.FormatString = "n2";
            this.txtMaxStock.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMaxStock.Properties.Mask.EditMask = "n2";
            this.txtMaxStock.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMaxStock.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtMaxStock.Size = new System.Drawing.Size(80, 24);
            this.txtMaxStock.TabIndex = 50;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(455, 257);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(57, 17);
            this.labelControl20.TabIndex = 49;
            this.labelControl20.Text = "Min Stock";
            // 
            // txtMinStock
            // 
            this.txtMinStock.EditValue = "";
            this.txtMinStock.Location = new System.Drawing.Point(518, 254);
            this.txtMinStock.Name = "txtMinStock";
            this.txtMinStock.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMinStock.Properties.Appearance.Options.UseFont = true;
            this.txtMinStock.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMinStock.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMinStock.Properties.DisplayFormat.FormatString = "n2";
            this.txtMinStock.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMinStock.Properties.EditFormat.FormatString = "n2";
            this.txtMinStock.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtMinStock.Properties.Mask.EditMask = "n2";
            this.txtMinStock.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtMinStock.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtMinStock.Size = new System.Drawing.Size(80, 24);
            this.txtMinStock.TabIndex = 48;
            // 
            // lblPI
            // 
            this.lblPI.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblPI.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lblPI.Location = new System.Drawing.Point(571, 437);
            this.lblPI.Name = "lblPI";
            this.lblPI.Size = new System.Drawing.Size(27, 17);
            this.lblPI.TabIndex = 43;
            this.lblPI.Text = "View";
            this.lblPI.Click += new System.EventHandler(this.lblPI_Click);
            // 
            // lblMSDS
            // 
            this.lblMSDS.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblMSDS.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lblMSDS.Location = new System.Drawing.Point(571, 407);
            this.lblMSDS.Name = "lblMSDS";
            this.lblMSDS.Size = new System.Drawing.Size(27, 17);
            this.lblMSDS.TabIndex = 39;
            this.lblMSDS.Text = "View";
            this.lblMSDS.Click += new System.EventHandler(this.lblMSDS_Click);
            // 
            // btnPI
            // 
            this.btnPI.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPI.Appearance.Options.UseFont = true;
            this.btnPI.Location = new System.Drawing.Point(486, 434);
            this.btnPI.Name = "btnPI";
            this.btnPI.Size = new System.Drawing.Size(60, 25);
            this.btnPI.TabIndex = 42;
            this.btnPI.Text = "...";
            this.btnPI.Click += new System.EventHandler(this.btnPI_Click);
            // 
            // btnMSDS
            // 
            this.btnMSDS.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnMSDS.Appearance.Options.UseFont = true;
            this.btnMSDS.Location = new System.Drawing.Point(486, 404);
            this.btnMSDS.Name = "btnMSDS";
            this.btnMSDS.Size = new System.Drawing.Size(60, 25);
            this.btnMSDS.TabIndex = 38;
            this.btnMSDS.Text = "...";
            this.btnMSDS.Click += new System.EventHandler(this.btnMSDS_Click);
            // 
            // txtPI
            // 
            this.txtPI.Location = new System.Drawing.Point(100, 434);
            this.txtPI.Name = "txtPI";
            this.txtPI.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPI.Properties.Appearance.Options.UseFont = true;
            this.txtPI.Size = new System.Drawing.Size(380, 24);
            this.txtPI.TabIndex = 41;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Location = new System.Drawing.Point(84, 437);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(10, 17);
            this.labelControl19.TabIndex = 40;
            this.labelControl19.Text = "PI";
            // 
            // txtMSDS
            // 
            this.txtMSDS.Location = new System.Drawing.Point(100, 404);
            this.txtMSDS.Name = "txtMSDS";
            this.txtMSDS.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMSDS.Properties.Appearance.Options.UseFont = true;
            this.txtMSDS.Size = new System.Drawing.Size(380, 24);
            this.txtMSDS.TabIndex = 37;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(59, 407);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(35, 17);
            this.labelControl18.TabIndex = 36;
            this.labelControl18.Text = "MSDS";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(370, 197);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(82, 17);
            this.labelControl17.TabIndex = 22;
            this.labelControl17.Text = "Active/Inactive";
            // 
            // cmbStatusProduct
            // 
            this.cmbStatusProduct.EditValue = "Active";
            this.cmbStatusProduct.Location = new System.Drawing.Point(458, 195);
            this.cmbStatusProduct.Name = "cmbStatusProduct";
            this.cmbStatusProduct.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatusProduct.Properties.Appearance.Options.UseFont = true;
            this.cmbStatusProduct.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatusProduct.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatusProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatusProduct.Properties.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.cmbStatusProduct.Size = new System.Drawing.Size(140, 23);
            this.cmbStatusProduct.TabIndex = 23;
            // 
            // txtThirdRatio
            // 
            this.txtThirdRatio.Location = new System.Drawing.Point(538, 135);
            this.txtThirdRatio.Name = "txtThirdRatio";
            this.txtThirdRatio.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtThirdRatio.Properties.Appearance.Options.UseFont = true;
            this.txtThirdRatio.Properties.Appearance.Options.UseTextOptions = true;
            this.txtThirdRatio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtThirdRatio.Properties.DisplayFormat.FormatString = "n2";
            this.txtThirdRatio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtThirdRatio.Properties.EditFormat.FormatString = "n2";
            this.txtThirdRatio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtThirdRatio.Properties.Mask.EditMask = "n2";
            this.txtThirdRatio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtThirdRatio.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtThirdRatio.Size = new System.Drawing.Size(60, 24);
            this.txtThirdRatio.TabIndex = 15;
            // 
            // txtThirdUOM
            // 
            this.txtThirdUOM.Location = new System.Drawing.Point(457, 135);
            this.txtThirdUOM.Name = "txtThirdUOM";
            this.txtThirdUOM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtThirdUOM.Properties.Appearance.Options.UseFont = true;
            this.txtThirdUOM.Size = new System.Drawing.Size(80, 24);
            this.txtThirdUOM.TabIndex = 14;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(398, 138);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(53, 17);
            this.labelControl16.TabIndex = 13;
            this.labelControl16.Text = "3rd UoM";
            // 
            // txtRatio
            // 
            this.txtRatio.EditValue = "";
            this.txtRatio.Location = new System.Drawing.Point(328, 135);
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtRatio.Properties.Appearance.Options.UseFont = true;
            this.txtRatio.Properties.Appearance.Options.UseTextOptions = true;
            this.txtRatio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtRatio.Properties.DisplayFormat.FormatString = "n2";
            this.txtRatio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtRatio.Properties.EditFormat.FormatString = "n2";
            this.txtRatio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtRatio.Properties.Mask.EditMask = "n2";
            this.txtRatio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtRatio.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtRatio.Size = new System.Drawing.Size(60, 24);
            this.txtRatio.TabIndex = 12;
            // 
            // txtSecondUoM
            // 
            this.txtSecondUoM.Location = new System.Drawing.Point(247, 135);
            this.txtSecondUoM.Name = "txtSecondUoM";
            this.txtSecondUoM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSecondUoM.Properties.Appearance.Options.UseFont = true;
            this.txtSecondUoM.Size = new System.Drawing.Size(80, 24);
            this.txtSecondUoM.TabIndex = 11;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(186, 138);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(55, 17);
            this.labelControl9.TabIndex = 10;
            this.labelControl9.Text = "2nd UoM";
            // 
            // txtUoM
            // 
            this.txtUoM.Location = new System.Drawing.Point(100, 135);
            this.txtUoM.Name = "txtUoM";
            this.txtUoM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtUoM.Properties.Appearance.Options.UseFont = true;
            this.txtUoM.Size = new System.Drawing.Size(80, 24);
            this.txtUoM.TabIndex = 9;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(65, 138);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(29, 17);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "UoM";
            // 
            // txtSalesPrice5
            // 
            this.txtSalesPrice5.Location = new System.Drawing.Point(100, 344);
            this.txtSalesPrice5.Name = "txtSalesPrice5";
            this.txtSalesPrice5.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalesPrice5.Properties.Appearance.Options.UseFont = true;
            this.txtSalesPrice5.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalesPrice5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSalesPrice5.Properties.DisplayFormat.FormatString = "n2";
            this.txtSalesPrice5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice5.Properties.EditFormat.FormatString = "n2";
            this.txtSalesPrice5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice5.Properties.Mask.EditMask = "n2";
            this.txtSalesPrice5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalesPrice5.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSalesPrice5.Size = new System.Drawing.Size(140, 24);
            this.txtSalesPrice5.TabIndex = 33;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Location = new System.Drawing.Point(21, 347);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(73, 17);
            this.labelControl15.TabIndex = 32;
            this.labelControl15.Text = "Sales Price 5";
            // 
            // txtSalesPrice4
            // 
            this.txtSalesPrice4.Location = new System.Drawing.Point(100, 314);
            this.txtSalesPrice4.Name = "txtSalesPrice4";
            this.txtSalesPrice4.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalesPrice4.Properties.Appearance.Options.UseFont = true;
            this.txtSalesPrice4.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalesPrice4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSalesPrice4.Properties.DisplayFormat.FormatString = "n2";
            this.txtSalesPrice4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice4.Properties.EditFormat.FormatString = "n2";
            this.txtSalesPrice4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice4.Properties.Mask.EditMask = "n2";
            this.txtSalesPrice4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalesPrice4.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSalesPrice4.Size = new System.Drawing.Size(140, 24);
            this.txtSalesPrice4.TabIndex = 31;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(21, 317);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(73, 17);
            this.labelControl14.TabIndex = 30;
            this.labelControl14.Text = "Sales Price 4";
            // 
            // txtSalesPrice3
            // 
            this.txtSalesPrice3.Location = new System.Drawing.Point(100, 284);
            this.txtSalesPrice3.Name = "txtSalesPrice3";
            this.txtSalesPrice3.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalesPrice3.Properties.Appearance.Options.UseFont = true;
            this.txtSalesPrice3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalesPrice3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSalesPrice3.Properties.DisplayFormat.FormatString = "n2";
            this.txtSalesPrice3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice3.Properties.EditFormat.FormatString = "n2";
            this.txtSalesPrice3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice3.Properties.Mask.EditMask = "n2";
            this.txtSalesPrice3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalesPrice3.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSalesPrice3.Size = new System.Drawing.Size(140, 24);
            this.txtSalesPrice3.TabIndex = 29;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(21, 287);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(73, 17);
            this.labelControl13.TabIndex = 28;
            this.labelControl13.Text = "Sales Price 3";
            // 
            // txtSalesPrice2
            // 
            this.txtSalesPrice2.Location = new System.Drawing.Point(100, 254);
            this.txtSalesPrice2.Name = "txtSalesPrice2";
            this.txtSalesPrice2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalesPrice2.Properties.Appearance.Options.UseFont = true;
            this.txtSalesPrice2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalesPrice2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSalesPrice2.Properties.DisplayFormat.FormatString = "n2";
            this.txtSalesPrice2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice2.Properties.EditFormat.FormatString = "n2";
            this.txtSalesPrice2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice2.Properties.Mask.EditMask = "n2";
            this.txtSalesPrice2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalesPrice2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSalesPrice2.Size = new System.Drawing.Size(140, 24);
            this.txtSalesPrice2.TabIndex = 27;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(21, 257);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(73, 17);
            this.labelControl12.TabIndex = 26;
            this.labelControl12.Text = "Sales Price 2";
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.Location = new System.Drawing.Point(458, 165);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbCurrency.Properties.Appearance.Options.UseFont = true;
            this.cmbCurrency.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbCurrency.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbCurrency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCurrency.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbCurrency.Properties.DisplayMember = "Name";
            this.cmbCurrency.Properties.NullText = "";
            this.cmbCurrency.Properties.ShowFooter = false;
            this.cmbCurrency.Properties.ShowHeader = false;
            this.cmbCurrency.Properties.ValueMember = "id";
            this.cmbCurrency.Size = new System.Drawing.Size(140, 24);
            this.cmbCurrency.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(401, 168);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(51, 17);
            this.labelControl11.TabIndex = 18;
            this.labelControl11.Text = "Currency";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(100, 165);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "Beta",
            "Standard",
            "Standard*",
            "Standard**",
            "Standard***",
            "OK"});
            this.cmbStatus.Size = new System.Drawing.Size(140, 23);
            this.cmbStatus.TabIndex = 17;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(59, 167);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(35, 17);
            this.labelControl10.TabIndex = 16;
            this.labelControl10.Text = "Status";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(100, 374);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtRemarks.Properties.Appearance.Options.UseFont = true;
            this.txtRemarks.Size = new System.Drawing.Size(498, 24);
            this.txtRemarks.TabIndex = 35;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(44, 377);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 17);
            this.labelControl8.TabIndex = 34;
            this.labelControl8.Text = "Remarks";
            // 
            // txtSalesPrice
            // 
            this.txtSalesPrice.Location = new System.Drawing.Point(100, 224);
            this.txtSalesPrice.Name = "txtSalesPrice";
            this.txtSalesPrice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalesPrice.Properties.Appearance.Options.UseFont = true;
            this.txtSalesPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalesPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSalesPrice.Properties.DisplayFormat.FormatString = "n2";
            this.txtSalesPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice.Properties.EditFormat.FormatString = "n2";
            this.txtSalesPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalesPrice.Properties.Mask.EditMask = "n2";
            this.txtSalesPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalesPrice.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSalesPrice.Size = new System.Drawing.Size(140, 24);
            this.txtSalesPrice.TabIndex = 25;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(21, 227);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(73, 17);
            this.labelControl7.TabIndex = 24;
            this.labelControl7.Text = "Sales Price 1";
            // 
            // txtCost
            // 
            this.txtCost.Enabled = false;
            this.txtCost.Location = new System.Drawing.Point(100, 194);
            this.txtCost.Name = "txtCost";
            this.txtCost.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCost.Properties.Appearance.Options.UseFont = true;
            this.txtCost.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCost.Properties.DisplayFormat.FormatString = "n2";
            this.txtCost.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCost.Properties.EditFormat.FormatString = "n2";
            this.txtCost.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCost.Properties.Mask.EditMask = "n2";
            this.txtCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCost.Properties.ReadOnly = true;
            this.txtCost.Size = new System.Drawing.Size(140, 24);
            this.txtCost.TabIndex = 21;
            this.txtCost.TabStop = false;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(43, 197);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(51, 17);
            this.labelControl6.TabIndex = 20;
            this.labelControl6.Text = "Price List";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(46, 108);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(48, 17);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Supplier";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.Location = new System.Drawing.Point(100, 105);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier.Properties.Appearance.Options.UseFont = true;
            this.cmbSupplier.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbSupplier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSupplier.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbSupplier.Properties.DisplayMember = "Name";
            this.cmbSupplier.Properties.NullText = "";
            this.cmbSupplier.Properties.ShowFooter = false;
            this.cmbSupplier.Properties.ShowHeader = false;
            this.cmbSupplier.Properties.ValueMember = "id";
            this.cmbSupplier.Size = new System.Drawing.Size(498, 24);
            this.cmbSupplier.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(41, 78);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 17);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Category";
            // 
            // cmbCategory
            // 
            this.cmbCategory.Location = new System.Drawing.Point(100, 75);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbCategory.Properties.Appearance.Options.UseFont = true;
            this.cmbCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCategory.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbCategory.Properties.DisplayMember = "Name";
            this.cmbCategory.Properties.NullText = "";
            this.cmbCategory.Properties.ShowFooter = false;
            this.cmbCategory.Properties.ShowHeader = false;
            this.cmbCategory.Properties.ValueMember = "id";
            this.cmbCategory.Size = new System.Drawing.Size(498, 24);
            this.cmbCategory.TabIndex = 5;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(100, 45);
            this.txtName.Name = "txtName";
            this.txtName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtName.Properties.Appearance.Options.UseFont = true;
            this.txtName.Size = new System.Drawing.Size(498, 24);
            this.txtName.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(59, 48);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(35, 17);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Name";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(100, 15);
            this.txtCode.Name = "txtCode";
            this.txtCode.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCode.Properties.Appearance.Options.UseFont = true;
            this.txtCode.Size = new System.Drawing.Size(498, 24);
            this.txtCode.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(63, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(31, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Code";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "All Files|*.*";
            this.openFileDialog.Title = "Open File";
            // 
            // frmEditProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 532);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditProduct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Product";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaxStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMSDS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatusProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdRatio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtThirdUOM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRatio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecondUoM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUoM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemarks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtSalesPrice5;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtSalesPrice4;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtSalesPrice3;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtSalesPrice2;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LookUpEdit cmbCurrency;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtRemarks;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtSalesPrice;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtCost;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit cmbSupplier;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit cmbCategory;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtThirdRatio;
        private DevExpress.XtraEditors.TextEdit txtThirdUOM;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtRatio;
        private DevExpress.XtraEditors.TextEdit txtSecondUoM;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtUoM;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatusProduct;
        private DevExpress.XtraEditors.LabelControl lblPI;
        private DevExpress.XtraEditors.LabelControl lblMSDS;
        private DevExpress.XtraEditors.SimpleButton btnPI;
        private DevExpress.XtraEditors.SimpleButton btnMSDS;
        private DevExpress.XtraEditors.TextEdit txtPI;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtMSDS;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        public DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtMaxStock;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtMinStock;
    }
}