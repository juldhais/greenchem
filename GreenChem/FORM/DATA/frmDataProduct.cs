﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Drawing;
using System.Diagnostics;
using System.IO;

namespace GreenChem.MenuData
{
    public partial class frmDataProduct : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataProduct()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Product");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Product");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Product");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Product");
            btnEditStatusProduct.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Status Product");
            cmbStatusProduct.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Inactive Product");
            btnUpdatePL.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Update Price List");

            if (System.IO.File.Exists("layout_product.xml"))
                gridView.RestoreLayoutFromXml("layout_product.xml");

            bool pricelist = db.CheckUserRole(frmUtama._iduseraccount.Value, "Price List");
            colSalesPrice.Visible = pricelist;
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.Products
                            where x.Active == true
                            && x.StatusProduct == cmbStatusProduct.Text
                            orderby x.StatusProduct, x.Name
                            select new
                            {
                                x.id,
                                x.Code,
                                x.Name,
                                Category = x.Category.Name,
                                Supplier = x.Supplier.Name,
                                x.UoM,
                                x.Cost,
                                x.SalesPrice,
                                x.Stock,
                                x.Stock2,
                                x.Stock3,
                                x.TransitStock,
                                x.Remarks,
                                x.Status,
                                x.StatusProduct,
                                x.StokMin,
                                x.StokMax,
                                x.DateModified,
                                UserModified = x.UserModified != null ? db.UserAccounts.First(a => a.id == x.UserModified).Name : "",
                                x.PLDateModified,
                                PLUserModified = x.PLUserModified != null ? db.UserAccounts.First(a => a.id == x.PLUserModified).Name : ""
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Code == txtSearch.Text || x.Name.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewProduct form = new frmNewProduct();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditProduct form = new frmEditProduct(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this product : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    Product product = db.Products.First(x => x.id == id);
                    product.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Product deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.Products
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.Code,
                                x.Name,
                                Category = x.Category.Name,
                                Supplier = x.Supplier.Name,
                                x.UoM,
                                x.Cost,
                                x.SalesPrice,
                                x.Stock,
                                x.Remarks,
                                x.Status
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Code == txtSearch.Text || x.Name.Contains(txtSearch.Text));

                rptProduct report = new rptProduct();
                report.DataSource = query;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnEditStatusProduct_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditStatusProduct form = new frmEditStatusProduct(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (gridView.GetRowCellValue(e.RowHandle, "StatusProduct").Safe() == "Inactive")
                e.Appearance.ForeColor = Color.DarkRed;
            else
            {
                try
                {
                    decimal stokmin = gridView.GetRowCellValue(e.RowHandle, "StokMin").ToDecimal();
                    decimal stokmax = gridView.GetRowCellValue(e.RowHandle, "StokMax").ToDecimal();
                    decimal stok = gridView.GetRowCellValue(e.RowHandle, "Stock").ToDecimal();
                    if (stok < stokmin && stokmin > 0)
                        e.Appearance.ForeColor = Color.DarkBlue;
                    if (stok > stokmax && stokmax > 0)
                        e.Appearance.ForeColor = Color.Navy;
                }
                catch { }
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditProduct form = new frmEditProduct(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.btnSave.Visible = false;
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbStatusProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnUpdatePL_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmUpdatePriceList form = new frmUpdatePriceList(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_product.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnDownloadMSDS_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                var barang = db.Products.First(x => x.id == id);
                string msds = barang.AttachemntMSDS;

                Cursor.Current = Cursors.WaitCursor;
                string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_msds" + Path.GetExtension(msds);
                Sync.DownloadFromDatabase(msds, localfile);

                Process process = new Process();
                process.StartInfo.FileName = localfile;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnDownloadPI_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                var barang = db.Products.First(x => x.id == id);
                string pi = barang.AttachmentPI;

                Cursor.Current = Cursors.WaitCursor;
                string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_pi" + Path.GetExtension(pi);
                //Sync.DownloadFromDatabase(txtPI.Text, localfile);
                Sync.DownloadFromDatabase(pi, localfile);

                Process process = new Process();
                process.StartInfo.FileName = localfile;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}