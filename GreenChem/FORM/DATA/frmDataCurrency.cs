﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmDataCurrency : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataCurrency()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            currencyBindingSource.DataSource = db.Currencies;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetRowCellValue(e.RowHandle, "Active", true);
        }
    }
}