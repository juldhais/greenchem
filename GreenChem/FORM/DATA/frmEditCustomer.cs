﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmEditCustomer : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditCustomer(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            var customer = db.Customers.First(x => x.id == _id);
            txtCode.Text = customer.Code;
            txtName.Text = customer.Name;
            cmbDepartment.EditValue = customer.idDepartment;
            txtAddress.Text = customer.Address;
            txtShipAddress.Text = customer.ShipAddress;
            txtPhone.Text = customer.Phone;
            txtRemarks.Text = customer.Remarks;
            txtContactPerson.Text = customer.ContactPerson;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                Customer customer = db.Customers.First(x => x.id == _id);
                customer.Code = txtCode.Text;
                customer.Name = txtName.Text;
                customer.idDepartment = (int?)cmbDepartment.EditValue;
                customer.Address = txtAddress.Text;
                customer.ShipAddress = txtShipAddress.Text;
                customer.Phone = txtPhone.Text;
                customer.Remarks = txtRemarks.Text;
                customer.ContactPerson = txtContactPerson.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}