﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmUpdatePriceList : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmUpdatePriceList(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            var product = db.Products.First(x => x.id == _id);
            txtCode.Text = product.Code;
            txtName.Text = product.Name;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                db = new GreenChemDataContext();
                Product product = db.Products.First(x => x.id == _id);
                product.Cost = txtCost.EditValue.ToDecimal();
                product.PLDateModified = DateTime.Now;
                product.PLUserModified = frmUtama._iduseraccount;
                db.SubmitChanges();

                XtraMessageBox.Show("Price List updated.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}