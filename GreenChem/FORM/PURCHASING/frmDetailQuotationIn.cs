﻿using System;
using System.Collections.Generic;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmDetailQuotationIn : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDetailQuotationIn(List<TempDetail> datasource)
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbProduct.DataSource = db.Products.GetName();
            tempDetailBindingSource.DataSource = datasource;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GetTotal()
        {
            try
            {
                var details = tempDetailBindingSource.Cast<TempDetail>();
                txtTotal.EditValue = details.Sum(x => x.Subtotal);
            }
            catch { }
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                db = new GreenChemDataContext();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
                gridView.SetFocusedRowCellValue("Price", product.SalesPrice);
            }
            else if (e.Column.FieldName == "Quantity" || e.Column.FieldName == "Price" || e.Column.FieldName == "Discount")
            {
                decimal quantity = gridView.GetFocusedRowCellValue("Quantity").ToDecimal();
                decimal price = gridView.GetFocusedRowCellValue("Price").ToDecimal();
                decimal discount = gridView.GetFocusedRowCellValue("Discount").ToDecimal();
                decimal subtotal = (quantity * price) * (100 - discount) / 100;
                gridView.SetFocusedRowCellValue("Subtotal", subtotal);
                GetTotal();
            }
        }
    }
}