﻿using System;
using System.Collections.Generic;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmViewQuotationIn : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmViewQuotationIn(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbProduct.DataSource = db.Products.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            var quotationin = db.QuotationIns.First(x => x.id == _id);
            txtTransactionNumber.Text = quotationin.TransactionNumber;
            cmbDate.DateTime = quotationin.TransactionDate;
            cmbSupplier.EditValue = quotationin.idSupplier;
            txtRemarks.Text = quotationin.Remarks;

            //details
            var details = db.DetailQuotationIns.Where(x => x.idQuotationIn == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.Remarks = item.ProductName;
                temp.idProduct = item.idProduct;
                temp.Quantity = item.Quantity;
                if (item.idProduct != null) temp.UoM = db.Products.First(x => x.id == item.idProduct).UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                listdetail.Add(temp);
            }

            gridControl.DataSource = listdetail;

            if (listdetail.Any())
                txtTotal.EditValue = listdetail.Sum(x => (decimal?)x.Subtotal).ToDecimal();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}