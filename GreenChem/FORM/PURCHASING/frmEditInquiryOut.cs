﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditInquiryOut : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        GreenChemDataContext db;

        public frmEditInquiryOut(int? id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            lblRefresh_Click(null, null);

            var inquiry = db.InquiryOuts.First(x => x.id == _id);
            txtTransactionNumber.Text = inquiry.TransactionNumber;
            cmbDate.DateTime = inquiry.TransactionDate;
            cmbDepartment.EditValue = inquiry.idDepartment;
            cmbSubDepartment.EditValue = inquiry.idSubDepartment;
            cmbSupplier.EditValue = inquiry.idSupplier;
            txtRemarks.Text = inquiry.Remarks;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                InquiryOut inquiry = db.InquiryOuts.First(x => x.id == _id);
                if (cmbDepartment.EditValue.IsNotEmpty()) inquiry.idDepartment = cmbDepartment.EditValue.ToInteger();
                if (cmbSubDepartment.EditValue.IsNotEmpty()) inquiry.idSubDepartment = cmbSubDepartment.EditValue.ToInteger();
                if (cmbSupplier.EditValue.IsNotEmpty()) inquiry.idSupplier = cmbSupplier.EditValue.ToInteger();
                inquiry.TransactionNumber = txtTransactionNumber.Text;
                inquiry.TransactionDate = cmbDate.DateTime;
                inquiry.idUserAccount = frmUtama._iduseraccount;
                inquiry.Remarks = txtRemarks.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}