﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmInquiryOut : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmInquiryOut()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Inquiry Out");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Inquiry Out");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Inquiry Out");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.InquiryOuts
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Supplier = x.Supplier.Name,
                                Department = x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                x.Remarks
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewInquiryOut form = new frmNewInquiryOut();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditInquiryOut form = new frmEditInquiryOut(id);
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                InquiryOut inquiry = db.InquiryOuts.First(x => x.id == id);
                db.InquiryOuts.DeleteOnSubmit(inquiry);
                db.SubmitChanges();
                RefreshData();
                XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}