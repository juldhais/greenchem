﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditQuotationIn : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmEditQuotationIn(int? id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            lblRefresh_Click(null, null);

            var quotation = db.QuotationIns.First(x => x.id == _id);
            txtTransactionNumber.Text = quotation.TransactionNumber;
            cmbDate.DateTime = quotation.TransactionDate;
            cmbSupplier.EditValue = quotation.idSupplier;
            txtName.Text = quotation.SupplierName;
            txtRemarks.Text = quotation.Remarks;

            //details
            var details = db.DetailQuotationIns.Where(x => x.idQuotationIn == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.idProduct = item.idProduct;
                temp.Quantity = item.Quantity;
                if (item.idProduct != null) temp.UoM = db.Products.First(x => x.id == item.idProduct).UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                temp.Remarks = item.ProductName;
                listdetail.Add(temp);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                QuotationIn quotation = db.QuotationIns.First(x => x.id == _id);
                if (frmUtama._iddepartment != null) quotation.idDepartment = frmUtama._iddepartment;
                if (frmUtama._idsubdepartment != null) quotation.idSubDepartment = frmUtama._idsubdepartment;
                if (cmbSupplier.EditValue.IsNotEmpty()) quotation.idSupplier = cmbSupplier.EditValue.ToInteger();
                quotation.TransactionNumber = txtTransactionNumber.Text;
                quotation.TransactionDate = cmbDate.DateTime;
                quotation.idUserAccount = frmUtama._iduseraccount;
                quotation.Remarks = txtRemarks.Text;
                quotation.SupplierName = txtName.Text;
                db.SubmitChanges();

                //details
                db.ExecuteCommand("DELETE FROM dbo.DetailQuotationIn WHERE idQuotationIn = " + quotation.id);
                foreach (var item in listdetail)
                {
                    if (item.Quantity != 0)
                    {
                        DetailQuotationIn detail = new DetailQuotationIn();
                        detail.idProduct = item.idProduct;
                        detail.idQuotationIn = quotation.id;
                        detail.Quantity = item.Quantity;
                        detail.Price = item.Price;
                        detail.Discount = item.Discount;
                        detail.Subtotal = item.Subtotal;
                        detail.ProductName = item.Remarks;
                        db.DetailQuotationIns.InsertOnSubmit(detail);
                    }
                }
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            db = new GreenChemDataContext();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailQuotationIn form = new frmDetailQuotationIn(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}