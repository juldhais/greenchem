﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditPOtoSupplierRegular : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditPOtoSupplierRegular(int id)
        {
            InitializeComponent();

            _id = id;

            lblRefresh_Click(null, null);
            var po = db.POtoSupplierRegulars.First(x => x.id == _id);
            txtNomorPO.Text = po.NomorPO;
            txtNomorPB.Text = po.NomorPB;
            cmbDate.DateTime = po.Date.Value;
            txtRFQ.Text = po.RFQ;
            cmbSupplier.EditValue = po.idSupplier;
            txtKurs.Text = po.Kurs;
            txtTerms.Text = po.Terms;
            txtRemarks.Text = po.Remarks;
            txtShippingAddress.Text = po.ShippingAddress;
            txtBillingAddress.Text = po.BillingAddress;
            txtSubtotal.EditValue = po.Subtotal;
            txtTaxPercent.EditValue = po.PPNPercent;
            txtTax.EditValue = po.PPNAmount;
            txtTotal.EditValue = po.Total;

            foreach (var item in po.DetailPOtoSupplierRegulars.Where(x => x.idBahanBaku != null))
            {
                DetailPOtoSupplierRegular detail = new DetailPOtoSupplierRegular();
                detail.idBahanBaku = item.idBahanBaku;
                detail.Quantity = item.Quantity;
                detail.UoM = item.UoM;
                detail.Price = item.Price;
                detail.Subtotal = item.Subtotal;
                detailPOtoSupplierRegularBindingSource.Add(detail);
            }

            foreach (var item in po.DetailPOtoSupplierRegulars.Where(x => x.idBahanPenolong != null))
            {
                DetailPOtoSupplierRegular detail = new DetailPOtoSupplierRegular();
                detail.idBahanPenolong = item.idBahanPenolong;
                detail.Quantity = item.Quantity;
                detail.UoM = item.UoM;
                detail.Price = item.Price;
                detail.Subtotal = item.Subtotal;
                detailPOtoSupplierRegularBindingSource2.Add(detail);
            }
        }

        private void GetTotal()
        {
            try
            {
                var details = detailPOtoSupplierRegularBindingSource.Cast<DetailPOtoSupplierRegular>();
                var details2 = detailPOtoSupplierRegularBindingSource2.Cast<DetailPOtoSupplierRegular>();
                txtSubtotal.EditValue = details.Sum(x => (decimal?)x.Subtotal).ToDecimal() + details2.Sum(x => (decimal?)x.Subtotal).ToDecimal();
                txtTax.EditValue = txtSubtotal.EditValue.ToDecimal() * txtTaxPercent.EditValue.ToDecimal() / 100;
                txtTotal.EditValue = txtSubtotal.EditValue.ToDecimal() + txtTax.EditValue.ToDecimal();
            }
            catch { }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
                cmbBahanBaku.DataSource = db.BahanBakus.GetName();
                cmbBahanPenolong.DataSource = db.BahanPenolongs.GetName();
                GetTotal();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbSupplier_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var supplier = db.Suppliers.First(x => x.id == cmbSupplier.EditValue.ToInteger());
                txtUP.Text = supplier.Person;
                txtPhone.Text = supplier.Phone;
                txtFax.Text = supplier.Fax;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Quantity" || e.Column.FieldName == "Price")
            {
                decimal quantity = gridView.GetFocusedRowCellValue("Quantity").ToDecimal();
                decimal price = gridView.GetFocusedRowCellValue("Price").ToDecimal();
                decimal subtotal = quantity * price;
                gridView.SetFocusedRowCellValue("Subtotal", subtotal);
                GetTotal();
            }
        }

        private void gridView2_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "Quantity" || e.Column.FieldName == "Price")
            {
                decimal quantity = gridView2.GetFocusedRowCellValue("Quantity").ToDecimal();
                decimal price = gridView2.GetFocusedRowCellValue("Price").ToDecimal();
                decimal subtotal = quantity * price;
                gridView2.SetFocusedRowCellValue("Subtotal", subtotal);
                GetTotal();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                //delete detaillama
                db.ExecuteCommand("DELETE FROM dbo.DetailPOtoSupplierRegular WHERE idPOtoSupplierRegular = " + _id);

                GetTotal();

                POtoSupplierRegular po = db.POtoSupplierRegulars.First(x => x.id == _id);
                po.idUserAccount = frmUtama._iduseraccount;
                po.idSupplier = (int?)cmbSupplier.EditValue;
                po.NomorPO = txtNomorPO.Text;
                po.NomorPB = txtNomorPB.Text;
                po.Date = cmbDate.DateTime;
                po.RFQ = txtRFQ.Text;
                po.Kurs = txtKurs.Text;
                po.Terms = txtTerms.Text;
                po.Remarks = txtRemarks.Text;
                po.ShippingAddress = txtShippingAddress.Text;
                po.BillingAddress = txtBillingAddress.Text;
                po.Subtotal = txtSubtotal.EditValue.ToDecimal();
                po.PPNPercent = txtTaxPercent.EditValue.ToDecimal();
                po.PPNAmount = txtTax.EditValue.ToDecimal();
                po.Total = txtTotal.EditValue.ToDecimal();

                foreach (DetailPOtoSupplierRegular item in detailPOtoSupplierRegularBindingSource)
                {
                    if (item.idBahanBaku.IsEmpty()) continue;
                    DetailPOtoSupplierRegular detail = new DetailPOtoSupplierRegular();
                    detail.idBahanBaku = item.idBahanBaku;
                    detail.Quantity = item.Quantity;
                    detail.UoM = item.UoM;
                    detail.Price = item.Price;
                    detail.Subtotal = item.Subtotal;
                    po.DetailPOtoSupplierRegulars.Add(detail);
                }

                foreach (DetailPOtoSupplierRegular item in detailPOtoSupplierRegularBindingSource2)
                {
                    if (item.idBahanPenolong.IsEmpty()) continue;
                    DetailPOtoSupplierRegular detail = new DetailPOtoSupplierRegular();
                    detail.idBahanPenolong = item.idBahanPenolong;
                    detail.Quantity = item.Quantity;
                    detail.UoM = item.UoM;
                    detail.Price = item.Price;
                    detail.Subtotal = item.Subtotal;
                    po.DetailPOtoSupplierRegulars.Add(detail);
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
                Close();
        }

        private void txtTaxPercent_EditValueChanged(object sender, EventArgs e)
        {
            GetTotal();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridView.DeleteSelectedRows();
        }

    }
}