﻿namespace GreenChem.MenuTransaction
{
    partial class frmNewPOtoSupplierExpedisi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtDONumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtJatuhTempo = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblRefresh = new DevExpress.XtraEditors.LabelControl();
            this.cmbDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtNomorPO = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.cmbTo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtPhone = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtPIC = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txtSendToAddress = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtSendTo = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.chkLainLain = new DevExpress.XtraEditors.CheckEdit();
            this.chkTrucking = new DevExpress.XtraEditors.CheckEdit();
            this.chkKapalLambat = new DevExpress.XtraEditors.CheckEdit();
            this.chkKapalCepat = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.txtBiayaHandlingIn = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtBiayaPalletized = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtUnitPrice = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalBiayaExpedisi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtVehicle = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtAsuransi = new DevExpress.XtraEditors.TextEdit();
            this.txtAsuransiPercent = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtTotalBeratDimensi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtTotal = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtTax = new DevExpress.XtraEditors.TextEdit();
            this.txtTaxPercent = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtTotalNilaiBarang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.detailPOtoSupplierExpedisiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colidPOtoSupplierExpedisi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNamaBarang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNilaiBarang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtNumber = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBeratDimensi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPOtoSupplierExpedisi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmbBahanBaku = new DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit();
            this.repositoryItemSearchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJatuhTempo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomorPO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSendToAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSendTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLainLain.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTrucking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKapalLambat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKapalCepat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBiayaHandlingIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBiayaPalletized.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBiayaExpedisi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVehicle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsuransi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsuransiPercent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBeratDimensi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxPercent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNilaiBarang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailPOtoSupplierExpedisiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBahanBaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtDONumber);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtJatuhTempo);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.lblRefresh);
            this.panelControl1.Controls.Add(this.cmbDate);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtNomorPO);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 75);
            this.panelControl1.TabIndex = 0;
            // 
            // txtDONumber
            // 
            this.txtDONumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDONumber.Location = new System.Drawing.Point(472, 42);
            this.txtDONumber.Name = "txtDONumber";
            this.txtDONumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDONumber.Properties.Appearance.Options.UseFont = true;
            this.txtDONumber.Size = new System.Drawing.Size(300, 24);
            this.txtDONumber.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(395, 45);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(71, 17);
            this.labelControl3.TabIndex = 34;
            this.labelControl3.Text = "DO Number";
            // 
            // txtJatuhTempo
            // 
            this.txtJatuhTempo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtJatuhTempo.Location = new System.Drawing.Point(472, 12);
            this.txtJatuhTempo.Name = "txtJatuhTempo";
            this.txtJatuhTempo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtJatuhTempo.Properties.Appearance.Options.UseFont = true;
            this.txtJatuhTempo.Size = new System.Drawing.Size(300, 24);
            this.txtJatuhTempo.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(391, 15);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(75, 17);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Jatuh Tempo";
            // 
            // lblRefresh
            // 
            this.lblRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblRefresh.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRefresh.Location = new System.Drawing.Point(244, 15);
            this.lblRefresh.Name = "lblRefresh";
            this.lblRefresh.Size = new System.Drawing.Size(41, 17);
            this.lblRefresh.TabIndex = 2;
            this.lblRefresh.Text = "refresh";
            this.lblRefresh.Click += new System.EventHandler(this.lblRefresh_Click);
            // 
            // cmbDate
            // 
            this.cmbDate.EditValue = null;
            this.cmbDate.Location = new System.Drawing.Point(88, 42);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDate.Properties.Appearance.Options.UseFont = true;
            this.cmbDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDate.Size = new System.Drawing.Size(150, 24);
            this.cmbDate.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(55, 45);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 17);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Date";
            // 
            // txtNomorPO
            // 
            this.txtNomorPO.Location = new System.Drawing.Point(88, 12);
            this.txtNomorPO.Name = "txtNomorPO";
            this.txtNomorPO.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNomorPO.Properties.Appearance.Options.UseFont = true;
            this.txtNomorPO.Size = new System.Drawing.Size(150, 24);
            this.txtNomorPO.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(19, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(63, 17);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Nomor PO";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.cmbTo);
            this.panelControl2.Controls.Add(this.txtPhone);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.txtPIC);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.txtAddress);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 75);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(784, 128);
            this.panelControl2.TabIndex = 1;
            // 
            // cmbTo
            // 
            this.cmbTo.Location = new System.Drawing.Point(70, 7);
            this.cmbTo.Name = "cmbTo";
            this.cmbTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTo.Properties.Appearance.Options.UseFont = true;
            this.cmbTo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTo.Size = new System.Drawing.Size(702, 23);
            this.cmbTo.TabIndex = 0;
            this.cmbTo.SelectedIndexChanged += new System.EventHandler(this.cmbTo_SelectedIndexChanged);
            // 
            // txtPhone
            // 
            this.txtPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPhone.Location = new System.Drawing.Point(70, 96);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhone.Properties.Appearance.Options.UseFont = true;
            this.txtPhone.Size = new System.Drawing.Size(702, 24);
            this.txtPhone.TabIndex = 3;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(28, 99);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(36, 17);
            this.labelControl9.TabIndex = 37;
            this.labelControl9.Text = "Phone";
            // 
            // txtPIC
            // 
            this.txtPIC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPIC.Location = new System.Drawing.Point(70, 66);
            this.txtPIC.Name = "txtPIC";
            this.txtPIC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPIC.Properties.Appearance.Options.UseFont = true;
            this.txtPIC.Size = new System.Drawing.Size(702, 24);
            this.txtPIC.TabIndex = 2;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(42, 69);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(18, 17);
            this.labelControl8.TabIndex = 35;
            this.labelControl8.Text = "PIC";
            // 
            // txtAddress
            // 
            this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAddress.Location = new System.Drawing.Point(70, 36);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(702, 24);
            this.txtAddress.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(16, 39);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(48, 17);
            this.labelControl7.TabIndex = 33;
            this.labelControl7.Text = "Address";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(49, 9);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(15, 17);
            this.labelControl5.TabIndex = 31;
            this.labelControl5.Text = "To";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txtSendToAddress);
            this.panelControl3.Controls.Add(this.labelControl10);
            this.panelControl3.Controls.Add(this.txtSendTo);
            this.panelControl3.Controls.Add(this.labelControl11);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(0, 203);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(784, 65);
            this.panelControl3.TabIndex = 2;
            // 
            // txtSendToAddress
            // 
            this.txtSendToAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSendToAddress.Location = new System.Drawing.Point(70, 36);
            this.txtSendToAddress.Name = "txtSendToAddress";
            this.txtSendToAddress.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSendToAddress.Properties.Appearance.Options.UseFont = true;
            this.txtSendToAddress.Size = new System.Drawing.Size(702, 24);
            this.txtSendToAddress.TabIndex = 1;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(16, 39);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(48, 17);
            this.labelControl10.TabIndex = 33;
            this.labelControl10.Text = "Address";
            // 
            // txtSendTo
            // 
            this.txtSendTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSendTo.Location = new System.Drawing.Point(70, 6);
            this.txtSendTo.Name = "txtSendTo";
            this.txtSendTo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSendTo.Properties.Appearance.Options.UseFont = true;
            this.txtSendTo.Size = new System.Drawing.Size(702, 24);
            this.txtSendTo.TabIndex = 0;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(16, 9);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(48, 17);
            this.labelControl11.TabIndex = 31;
            this.labelControl11.Text = "Send To";
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.chkLainLain);
            this.panelControl4.Controls.Add(this.chkTrucking);
            this.panelControl4.Controls.Add(this.chkKapalLambat);
            this.panelControl4.Controls.Add(this.chkKapalCepat);
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(0, 268);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(784, 30);
            this.panelControl4.TabIndex = 4;
            // 
            // chkLainLain
            // 
            this.chkLainLain.Location = new System.Drawing.Point(472, 6);
            this.chkLainLain.Name = "chkLainLain";
            this.chkLainLain.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkLainLain.Properties.Appearance.Options.UseFont = true;
            this.chkLainLain.Properties.Caption = "Lain-Lain";
            this.chkLainLain.Size = new System.Drawing.Size(117, 21);
            this.chkLainLain.TabIndex = 36;
            // 
            // chkTrucking
            // 
            this.chkTrucking.Location = new System.Drawing.Point(367, 6);
            this.chkTrucking.Name = "chkTrucking";
            this.chkTrucking.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkTrucking.Properties.Appearance.Options.UseFont = true;
            this.chkTrucking.Properties.Caption = "Trucking";
            this.chkTrucking.Size = new System.Drawing.Size(99, 21);
            this.chkTrucking.TabIndex = 37;
            // 
            // chkKapalLambat
            // 
            this.chkKapalLambat.Location = new System.Drawing.Point(244, 6);
            this.chkKapalLambat.Name = "chkKapalLambat";
            this.chkKapalLambat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkKapalLambat.Properties.Appearance.Options.UseFont = true;
            this.chkKapalLambat.Properties.Caption = "Kapal Lambat";
            this.chkKapalLambat.Size = new System.Drawing.Size(117, 21);
            this.chkKapalLambat.TabIndex = 36;
            // 
            // chkKapalCepat
            // 
            this.chkKapalCepat.Location = new System.Drawing.Point(121, 6);
            this.chkKapalCepat.Name = "chkKapalCepat";
            this.chkKapalCepat.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkKapalCepat.Properties.Appearance.Options.UseFont = true;
            this.chkKapalCepat.Properties.Caption = "Kapal Cepat";
            this.chkKapalCepat.Size = new System.Drawing.Size(117, 21);
            this.chkKapalCepat.TabIndex = 35;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(11, 7);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(86, 17);
            this.labelControl4.TabIndex = 34;
            this.labelControl4.Text = "Transportation";
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.txtBiayaHandlingIn);
            this.panelControl5.Controls.Add(this.labelControl21);
            this.panelControl5.Controls.Add(this.txtBiayaPalletized);
            this.panelControl5.Controls.Add(this.labelControl20);
            this.panelControl5.Controls.Add(this.txtUnitPrice);
            this.panelControl5.Controls.Add(this.txtTotalBiayaExpedisi);
            this.panelControl5.Controls.Add(this.labelControl19);
            this.panelControl5.Controls.Add(this.txtVehicle);
            this.panelControl5.Controls.Add(this.labelControl17);
            this.panelControl5.Controls.Add(this.labelControl14);
            this.panelControl5.Controls.Add(this.txtAsuransi);
            this.panelControl5.Controls.Add(this.txtAsuransiPercent);
            this.panelControl5.Controls.Add(this.labelControl13);
            this.panelControl5.Controls.Add(this.txtTotalBeratDimensi);
            this.panelControl5.Controls.Add(this.labelControl12);
            this.panelControl5.Controls.Add(this.btnCancel);
            this.panelControl5.Controls.Add(this.btnSave);
            this.panelControl5.Controls.Add(this.txtTotal);
            this.panelControl5.Controls.Add(this.labelControl15);
            this.panelControl5.Controls.Add(this.txtTax);
            this.panelControl5.Controls.Add(this.txtTaxPercent);
            this.panelControl5.Controls.Add(this.labelControl16);
            this.panelControl5.Controls.Add(this.txtTotalNilaiBarang);
            this.panelControl5.Controls.Add(this.labelControl18);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl5.Location = new System.Drawing.Point(0, 378);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(784, 234);
            this.panelControl5.TabIndex = 3;
            // 
            // txtBiayaHandlingIn
            // 
            this.txtBiayaHandlingIn.Location = new System.Drawing.Point(419, 131);
            this.txtBiayaHandlingIn.Name = "txtBiayaHandlingIn";
            this.txtBiayaHandlingIn.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtBiayaHandlingIn.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBiayaHandlingIn.Properties.Appearance.Options.UseBackColor = true;
            this.txtBiayaHandlingIn.Properties.Appearance.Options.UseFont = true;
            this.txtBiayaHandlingIn.Properties.Appearance.Options.UseTextOptions = true;
            this.txtBiayaHandlingIn.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtBiayaHandlingIn.Properties.DisplayFormat.FormatString = "n2";
            this.txtBiayaHandlingIn.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBiayaHandlingIn.Properties.EditFormat.FormatString = "n2";
            this.txtBiayaHandlingIn.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBiayaHandlingIn.Properties.Mask.EditMask = "n2";
            this.txtBiayaHandlingIn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtBiayaHandlingIn.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtBiayaHandlingIn.Size = new System.Drawing.Size(160, 24);
            this.txtBiayaHandlingIn.TabIndex = 10;
            this.txtBiayaHandlingIn.EditValueChanged += new System.EventHandler(this.CalculateTotal);
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(287, 134);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(126, 17);
            this.labelControl21.TabIndex = 43;
            this.labelControl21.Text = "Biaya Handling In/Out";
            // 
            // txtBiayaPalletized
            // 
            this.txtBiayaPalletized.Location = new System.Drawing.Point(419, 101);
            this.txtBiayaPalletized.Name = "txtBiayaPalletized";
            this.txtBiayaPalletized.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtBiayaPalletized.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBiayaPalletized.Properties.Appearance.Options.UseBackColor = true;
            this.txtBiayaPalletized.Properties.Appearance.Options.UseFont = true;
            this.txtBiayaPalletized.Properties.Appearance.Options.UseTextOptions = true;
            this.txtBiayaPalletized.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtBiayaPalletized.Properties.DisplayFormat.FormatString = "n2";
            this.txtBiayaPalletized.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBiayaPalletized.Properties.EditFormat.FormatString = "n2";
            this.txtBiayaPalletized.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtBiayaPalletized.Properties.Mask.EditMask = "n2";
            this.txtBiayaPalletized.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtBiayaPalletized.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtBiayaPalletized.Size = new System.Drawing.Size(160, 24);
            this.txtBiayaPalletized.TabIndex = 9;
            this.txtBiayaPalletized.EditValueChanged += new System.EventHandler(this.CalculateTotal);
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(324, 104);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(89, 17);
            this.labelControl20.TabIndex = 41;
            this.labelControl20.Text = "Biaya Palletized";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(122, 36);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtUnitPrice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtUnitPrice.Properties.Appearance.Options.UseBackColor = true;
            this.txtUnitPrice.Properties.Appearance.Options.UseFont = true;
            this.txtUnitPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.txtUnitPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtUnitPrice.Properties.DisplayFormat.FormatString = "n2";
            this.txtUnitPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtUnitPrice.Properties.EditFormat.FormatString = "n2";
            this.txtUnitPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtUnitPrice.Properties.Mask.EditMask = "n2";
            this.txtUnitPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUnitPrice.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtUnitPrice.Size = new System.Drawing.Size(160, 24);
            this.txtUnitPrice.TabIndex = 2;
            this.txtUnitPrice.EditValueChanged += new System.EventHandler(this.CalculateTotal);
            // 
            // txtTotalBiayaExpedisi
            // 
            this.txtTotalBiayaExpedisi.Location = new System.Drawing.Point(122, 66);
            this.txtTotalBiayaExpedisi.Name = "txtTotalBiayaExpedisi";
            this.txtTotalBiayaExpedisi.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalBiayaExpedisi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTotalBiayaExpedisi.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalBiayaExpedisi.Properties.Appearance.Options.UseFont = true;
            this.txtTotalBiayaExpedisi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalBiayaExpedisi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalBiayaExpedisi.Properties.DisplayFormat.FormatString = "n2";
            this.txtTotalBiayaExpedisi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalBiayaExpedisi.Properties.EditFormat.FormatString = "n2";
            this.txtTotalBiayaExpedisi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalBiayaExpedisi.Properties.Mask.EditMask = "n2";
            this.txtTotalBiayaExpedisi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalBiayaExpedisi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalBiayaExpedisi.Size = new System.Drawing.Size(160, 24);
            this.txtTotalBiayaExpedisi.TabIndex = 4;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Location = new System.Drawing.Point(25, 69);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(91, 17);
            this.labelControl19.TabIndex = 38;
            this.labelControl19.Text = "Total Biaya Exp.";
            // 
            // txtVehicle
            // 
            this.txtVehicle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVehicle.Location = new System.Drawing.Point(419, 36);
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtVehicle.Properties.Appearance.Options.UseFont = true;
            this.txtVehicle.Size = new System.Drawing.Size(353, 24);
            this.txtVehicle.TabIndex = 3;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(329, 39);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(84, 17);
            this.labelControl17.TabIndex = 36;
            this.labelControl17.Text = "Contract Num.";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(61, 39);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(55, 17);
            this.labelControl14.TabIndex = 26;
            this.labelControl14.Text = "Unit Price";
            // 
            // txtAsuransi
            // 
            this.txtAsuransi.Location = new System.Drawing.Point(173, 131);
            this.txtAsuransi.Name = "txtAsuransi";
            this.txtAsuransi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAsuransi.Properties.Appearance.Options.UseFont = true;
            this.txtAsuransi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtAsuransi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtAsuransi.Properties.DisplayFormat.FormatString = "n2";
            this.txtAsuransi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtAsuransi.Properties.EditFormat.FormatString = "n2";
            this.txtAsuransi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtAsuransi.Properties.Mask.EditMask = "n2";
            this.txtAsuransi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtAsuransi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAsuransi.Size = new System.Drawing.Size(108, 24);
            this.txtAsuransi.TabIndex = 8;
            this.txtAsuransi.EditValueChanged += new System.EventHandler(this.CalculateTotal);
            // 
            // txtAsuransiPercent
            // 
            this.txtAsuransiPercent.EditValue = "0,2";
            this.txtAsuransiPercent.Location = new System.Drawing.Point(122, 131);
            this.txtAsuransiPercent.Name = "txtAsuransiPercent";
            this.txtAsuransiPercent.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAsuransiPercent.Properties.Appearance.Options.UseFont = true;
            this.txtAsuransiPercent.Properties.Appearance.Options.UseTextOptions = true;
            this.txtAsuransiPercent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtAsuransiPercent.Properties.DisplayFormat.FormatString = "P2";
            this.txtAsuransiPercent.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtAsuransiPercent.Properties.EditFormat.FormatString = "P2";
            this.txtAsuransiPercent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtAsuransiPercent.Properties.Mask.EditMask = "P2";
            this.txtAsuransiPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtAsuransiPercent.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtAsuransiPercent.Size = new System.Drawing.Size(50, 24);
            this.txtAsuransiPercent.TabIndex = 7;
            this.txtAsuransiPercent.EditValueChanged += new System.EventHandler(this.txtAsuransiPercent_EditValueChanged);
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(67, 134);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(49, 17);
            this.labelControl13.TabIndex = 23;
            this.labelControl13.Text = "Asuransi";
            // 
            // txtTotalBeratDimensi
            // 
            this.txtTotalBeratDimensi.Location = new System.Drawing.Point(419, 6);
            this.txtTotalBeratDimensi.Name = "txtTotalBeratDimensi";
            this.txtTotalBeratDimensi.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalBeratDimensi.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTotalBeratDimensi.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalBeratDimensi.Properties.Appearance.Options.UseFont = true;
            this.txtTotalBeratDimensi.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalBeratDimensi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalBeratDimensi.Properties.DisplayFormat.FormatString = "n2";
            this.txtTotalBeratDimensi.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalBeratDimensi.Properties.EditFormat.FormatString = "n2";
            this.txtTotalBeratDimensi.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalBeratDimensi.Properties.Mask.EditMask = "n2";
            this.txtTotalBeratDimensi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalBeratDimensi.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalBeratDimensi.Properties.ReadOnly = true;
            this.txtTotalBeratDimensi.Size = new System.Drawing.Size(160, 24);
            this.txtTotalBeratDimensi.TabIndex = 1;
            this.txtTotalBeratDimensi.EditValueChanged += new System.EventHandler(this.CalculateTotal);
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(299, 9);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(114, 17);
            this.labelControl12.TabIndex = 21;
            this.labelControl12.Text = "Total Berat/Dimensi";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(692, 177);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 45);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(561, 177);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(125, 45);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.EditValue = "";
            this.txtTotal.Location = new System.Drawing.Point(123, 166);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotal.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.txtTotal.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotal.Properties.Appearance.Options.UseFont = true;
            this.txtTotal.Properties.Appearance.Options.UseForeColor = true;
            this.txtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotal.Properties.DisplayFormat.FormatString = "n2";
            this.txtTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotal.Properties.EditFormat.FormatString = "n2";
            this.txtTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotal.Properties.Mask.EditMask = "n2";
            this.txtTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotal.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotal.Properties.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(158, 28);
            this.txtTotal.TabIndex = 11;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelControl15.Location = new System.Drawing.Point(68, 169);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(49, 21);
            this.labelControl15.TabIndex = 19;
            this.labelControl15.Text = "TOTAL";
            // 
            // txtTax
            // 
            this.txtTax.Location = new System.Drawing.Point(173, 101);
            this.txtTax.Name = "txtTax";
            this.txtTax.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTax.Properties.Appearance.Options.UseFont = true;
            this.txtTax.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTax.Properties.DisplayFormat.FormatString = "n2";
            this.txtTax.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTax.Properties.EditFormat.FormatString = "n2";
            this.txtTax.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTax.Properties.Mask.EditMask = "n2";
            this.txtTax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTax.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTax.Size = new System.Drawing.Size(108, 24);
            this.txtTax.TabIndex = 6;
            this.txtTax.EditValueChanged += new System.EventHandler(this.CalculateTotal);
            // 
            // txtTaxPercent
            // 
            this.txtTaxPercent.EditValue = "1";
            this.txtTaxPercent.Location = new System.Drawing.Point(122, 101);
            this.txtTaxPercent.Name = "txtTaxPercent";
            this.txtTaxPercent.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTaxPercent.Properties.Appearance.Options.UseFont = true;
            this.txtTaxPercent.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTaxPercent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTaxPercent.Properties.DisplayFormat.FormatString = "P0";
            this.txtTaxPercent.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTaxPercent.Properties.EditFormat.FormatString = "P0";
            this.txtTaxPercent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTaxPercent.Properties.Mask.EditMask = "P0";
            this.txtTaxPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTaxPercent.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTaxPercent.Size = new System.Drawing.Size(50, 24);
            this.txtTaxPercent.TabIndex = 5;
            this.txtTaxPercent.EditValueChanged += new System.EventHandler(this.txtTaxPercent_EditValueChanged);
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(92, 104);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(24, 17);
            this.labelControl16.TabIndex = 16;
            this.labelControl16.Text = "PPN";
            // 
            // txtTotalNilaiBarang
            // 
            this.txtTotalNilaiBarang.Location = new System.Drawing.Point(122, 6);
            this.txtTotalNilaiBarang.Name = "txtTotalNilaiBarang";
            this.txtTotalNilaiBarang.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalNilaiBarang.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTotalNilaiBarang.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalNilaiBarang.Properties.Appearance.Options.UseFont = true;
            this.txtTotalNilaiBarang.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalNilaiBarang.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalNilaiBarang.Properties.DisplayFormat.FormatString = "n2";
            this.txtTotalNilaiBarang.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalNilaiBarang.Properties.EditFormat.FormatString = "n2";
            this.txtTotalNilaiBarang.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalNilaiBarang.Properties.Mask.EditMask = "n2";
            this.txtTotalNilaiBarang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalNilaiBarang.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtTotalNilaiBarang.Properties.ReadOnly = true;
            this.txtTotalNilaiBarang.Size = new System.Drawing.Size(160, 24);
            this.txtTotalNilaiBarang.TabIndex = 0;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(12, 9);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(104, 17);
            this.labelControl18.TabIndex = 12;
            this.labelControl18.Text = "Total Nilai Barang";
            // 
            // gridControl
            // 
            this.gridControl.DataSource = this.detailPOtoSupplierExpedisiBindingSource;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 298);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cmbBahanBaku,
            this.txtNumber});
            this.gridControl.Size = new System.Drawing.Size(784, 80);
            this.gridControl.TabIndex = 6;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // detailPOtoSupplierExpedisiBindingSource
            // 
            this.detailPOtoSupplierExpedisiBindingSource.DataSource = typeof(GreenChem.DetailPOtoSupplierExpedisi);
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colidPOtoSupplierExpedisi,
            this.colNamaBarang,
            this.colNilaiBarang,
            this.colBeratDimensi,
            this.colPOtoSupplierExpedisi});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsCustomization.AllowGroup = false;
            this.gridView.OptionsDetail.ShowDetailTabs = false;
            this.gridView.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.RowHeight = 23;
            this.gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView_CellValueChanged);
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colidPOtoSupplierExpedisi
            // 
            this.colidPOtoSupplierExpedisi.FieldName = "idPOtoSupplierExpedisi";
            this.colidPOtoSupplierExpedisi.Name = "colidPOtoSupplierExpedisi";
            // 
            // colNamaBarang
            // 
            this.colNamaBarang.FieldName = "NamaBarang";
            this.colNamaBarang.Name = "colNamaBarang";
            this.colNamaBarang.Visible = true;
            this.colNamaBarang.VisibleIndex = 0;
            this.colNamaBarang.Width = 604;
            // 
            // colNilaiBarang
            // 
            this.colNilaiBarang.ColumnEdit = this.txtNumber;
            this.colNilaiBarang.DisplayFormat.FormatString = "n2";
            this.colNilaiBarang.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNilaiBarang.FieldName = "NilaiBarang";
            this.colNilaiBarang.Name = "colNilaiBarang";
            this.colNilaiBarang.Visible = true;
            this.colNilaiBarang.VisibleIndex = 1;
            this.colNilaiBarang.Width = 279;
            // 
            // txtNumber
            // 
            this.txtNumber.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNumber.Appearance.Options.UseFont = true;
            this.txtNumber.AutoHeight = false;
            this.txtNumber.DisplayFormat.FormatString = "n2";
            this.txtNumber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtNumber.EditFormat.FormatString = "n2";
            this.txtNumber.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtNumber.Mask.EditMask = "n2";
            this.txtNumber.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtNumber.Name = "txtNumber";
            // 
            // colBeratDimensi
            // 
            this.colBeratDimensi.ColumnEdit = this.txtNumber;
            this.colBeratDimensi.DisplayFormat.FormatString = "n2";
            this.colBeratDimensi.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBeratDimensi.FieldName = "BeratDimensi";
            this.colBeratDimensi.Name = "colBeratDimensi";
            this.colBeratDimensi.Visible = true;
            this.colBeratDimensi.VisibleIndex = 2;
            this.colBeratDimensi.Width = 281;
            // 
            // colPOtoSupplierExpedisi
            // 
            this.colPOtoSupplierExpedisi.FieldName = "POtoSupplierExpedisi";
            this.colPOtoSupplierExpedisi.Name = "colPOtoSupplierExpedisi";
            // 
            // cmbBahanBaku
            // 
            this.cmbBahanBaku.AutoHeight = false;
            this.cmbBahanBaku.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBahanBaku.DisplayMember = "Name";
            this.cmbBahanBaku.Name = "cmbBahanBaku";
            this.cmbBahanBaku.NullText = "";
            this.cmbBahanBaku.ValueMember = "id";
            this.cmbBahanBaku.View = this.repositoryItemSearchLookUpEdit1View;
            // 
            // repositoryItemSearchLookUpEdit1View
            // 
            this.repositoryItemSearchLookUpEdit1View.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.repositoryItemSearchLookUpEdit1View.Appearance.HeaderPanel.Options.UseFont = true;
            this.repositoryItemSearchLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.repositoryItemSearchLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemSearchLookUpEdit1View.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.repositoryItemSearchLookUpEdit1View.Appearance.Row.Options.UseFont = true;
            this.repositoryItemSearchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.repositoryItemSearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemSearchLookUpEdit1View.Name = "repositoryItemSearchLookUpEdit1View";
            this.repositoryItemSearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemSearchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemSearchLookUpEdit1View.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 336;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 844;
            // 
            // frmNewPOtoSupplierExpedisi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 612);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmNewPOtoSupplierExpedisi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New PO to Supplier Expedisi";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJatuhTempo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomorPO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSendToAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSendTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkLainLain.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTrucking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKapalLambat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkKapalCepat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBiayaHandlingIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBiayaPalletized.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBiayaExpedisi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVehicle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsuransi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAsuransiPercent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalBeratDimensi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxPercent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNilaiBarang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailPOtoSupplierExpedisiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBahanBaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSearchLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblRefresh;
        private DevExpress.XtraEditors.DateEdit cmbDate;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtNomorPO;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.MemoEdit txtJatuhTempo;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.MemoEdit txtPhone;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.MemoEdit txtPIC;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit txtAddress;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.MemoEdit txtSendToAddress;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.MemoEdit txtSendTo;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtDONumber;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.CheckEdit chkLainLain;
        private DevExpress.XtraEditors.CheckEdit chkTrucking;
        private DevExpress.XtraEditors.CheckEdit chkKapalLambat;
        private DevExpress.XtraEditors.CheckEdit chkKapalCepat;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtTotal;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtTax;
        private DevExpress.XtraEditors.TextEdit txtTaxPercent;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtTotalNilaiBarang;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit cmbBahanBaku;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemSearchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit txtNumber;
        private DevExpress.XtraEditors.TextEdit txtBiayaHandlingIn;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtBiayaPalletized;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtUnitPrice;
        private DevExpress.XtraEditors.TextEdit txtTotalBiayaExpedisi;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtVehicle;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtAsuransi;
        private DevExpress.XtraEditors.TextEdit txtAsuransiPercent;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtTotalBeratDimensi;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private System.Windows.Forms.BindingSource detailPOtoSupplierExpedisiBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colidPOtoSupplierExpedisi;
        private DevExpress.XtraGrid.Columns.GridColumn colNamaBarang;
        private DevExpress.XtraGrid.Columns.GridColumn colNilaiBarang;
        private DevExpress.XtraGrid.Columns.GridColumn colBeratDimensi;
        private DevExpress.XtraGrid.Columns.GridColumn colPOtoSupplierExpedisi;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTo;
    }
}