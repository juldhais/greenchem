﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmPOtoSupplierExpedisi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPOtoSupplierExpedisi()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Today.AddYears(-1);
            cmbDateEnd.DateTime = DateTime.Today.AddYears(1);
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New PO to Supplier Expedisi");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit PO to Supplier Expedisi");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete PO to Supplier Expedisi");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print PO to Supplier Expedisi");
            btnApprove.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve PO to Supplier Expedisi");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.POtoSupplierExpedisis
                            where x.Date.Date >= cmbDateStart.DateTime.Date && x.Date.Date <= cmbDateEnd.DateTime.Date
                            select new
                            {
                                x.id,
                                x.idUserAccount,
                                x.NomorPO,
                                x.Date,
                                x.JatuhTempo,
                                x.Untuk,
                                x.Address,
                                x.PIC,
                                x.Phone,
                                x.SendTo,
                                x.SendToAddress,
                                x.DONumber,
                                x.Transportation,
                                x.Subtotal,
                                x.BeratDimensi,
                                x.UnitPrice,
                                x.TotalBiayaExpedisi,
                                x.PPNPercent,
                                x.PPNAmount,
                                x.AsuransiPercent,
                                x.AsuransiAmount,
                                x.BiayaPalletized,
                                x.BiayaHandlingIn,
                                x.Total,
                                x.Vehicle,
                                x.DateApproved,
                                ApprovedBy = x.ApprovedBy != null ? db.UserAccounts.First(a => a.id == x.ApprovedBy).Name : ""
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewPOtoSupplierExpedisi form = new frmNewPOtoSupplierExpedisi();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditPOtoSupplierExpedisi form = new frmEditPOtoSupplierExpedisi(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.ShowDialog();
                lblRefresh_Click(null, null);
                Cursor.Current = Cursors.Default;
            }
            catch { }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    db.ExecuteCommand("DELETE FROM dbo.DetailPOtoSupplierExpedisi WHERE idPOtoSupplierExpedisi = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.POtoSupplierExpedisi WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblRefresh_Click(null, null);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                db = new GreenChemDataContext();
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                List<TempPOtoSupplierExpedisi> list = new List<TempPOtoSupplierExpedisi>();

                var po = db.POtoSupplierExpedisis.First(x => x.id == id);
                if (po.Approved == true)
                {
                    foreach (var item in po.DetailPOtoSupplierExpedisis)
                    {
                        TempPOtoSupplierExpedisi temp = new TempPOtoSupplierExpedisi();
                        temp.NomorPO = po.NomorPO;
                        temp.Tanggal = po.Date;
                        temp.JatuhTempo = po.JatuhTempo;
                        temp.DONumber = po.DONumber;
                        temp.To = po.Untuk;
                        temp.Address = po.Address;
                        temp.PIC = po.PIC;
                        temp.Phone = po.Phone;
                        temp.SendTo = po.SendTo;
                        temp.SendToAddress = po.SendToAddress;
                        if (po.Transportation.Contains("[Kapal Cepat]")) temp.KapalCepat = true;
                        if (po.Transportation.Contains("[Kapal Lambat]")) temp.KapalLambat = true;
                        if (po.Transportation.Contains("[Trucking]")) temp.Trucking = true;
                        if (po.Transportation.Contains("[Lain-Lain]")) temp.LainLain = true;
                        temp.NamaBarang = item.NamaBarang;
                        temp.NilaiBarang = item.NilaiBarang;

                        temp.BeratDimensi = item.BeratDimensi;

                        temp.TotalNilaiBarang = po.Subtotal;
                        temp.TotalBeratDimensi = po.BeratDimensi;
                        temp.UnitPrice = po.UnitPrice;
                        temp.TotalBiayaExpedisi = po.TotalBiayaExpedisi;
                        temp.PPNPercent = po.PPNPercent;
                        temp.PPNAmount = po.PPNAmount;
                        temp.AsuransiPercent = po.AsuransiPercent;
                        temp.AsuransiAmount = po.AsuransiAmount;
                        temp.BiayaPalletized = po.BiayaPalletized;
                        temp.BiayaHandlingIn = po.BiayaHandlingIn;
                        temp.Total = po.Total;
                        if (po.idUserAccount.HasValue) temp.UserName = db.UserAccounts.First(x => x.id == po.idUserAccount).Name;

                        list.Add(temp);
                    }

                    rptPOtoSupplierExpedisi report = new rptPOtoSupplierExpedisi();
                    if (File.Exists("POtoSupplierExpedisi.repx")) report.LoadLayout("POtoSupplierExpedisi.repx");
                    report.DataSource = list;
                    report.ShowPreview();
                    report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
                }
                else
                {
                    XtraMessageBox.Show("PO is not approved yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("NomorPO").Safe();

                db = new GreenChemDataContext();
                POtoSupplierExpedisi po = db.POtoSupplierExpedisis.First(x => x.id == id);

                if (po.Approved != true)
                {
                    DialogResult = XtraMessageBox.Show("Approve this PO : " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (DialogResult == DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        po.Approved = true;
                        po.ApprovedBy = frmUtama._iduseraccount;
                        po.DateApproved = DateTime.Now;
                        db.SubmitChanges();
                        lblRefresh_Click(null, null);

                        XtraMessageBox.Show("PO has been approved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    XtraMessageBox.Show("PO is approved already.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditPOtoSupplierExpedisi form = new frmEditPOtoSupplierExpedisi(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.btnSave.Enabled = false;
                form.ShowDialog();
                lblRefresh_Click(null, null);
                Cursor.Current = Cursors.Default;
            }
            catch { }
        }
    }
}