﻿namespace GreenChem.MenuTransaction
{
    partial class frmPOtoSupplierRegular
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomorPO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomorPB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRFQ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colidSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKurs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShippingAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubtotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPPNPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPPNAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnView = new DevExpress.XtraEditors.SimpleButton();
            this.btnApprove = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDateStart = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDateEnd = new DevExpress.XtraEditors.DateEdit();
            this.lblRefresh = new DevExpress.XtraEditors.LabelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateEnd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 40);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(984, 522);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colNomorPO,
            this.colNomorPB,
            this.colDate,
            this.colRFQ,
            this.colidSupplier,
            this.colKurs,
            this.colTerms,
            this.colRemarks,
            this.colShippingAddress,
            this.colBillingAddress,
            this.colSubtotal,
            this.colPPNPercent,
            this.colPPNAmount,
            this.colTotal,
            this.colDateApproved,
            this.colApprovedBy});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.RowHeight = 23;
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colNomorPO
            // 
            this.colNomorPO.FieldName = "NomorPO";
            this.colNomorPO.Name = "colNomorPO";
            this.colNomorPO.Visible = true;
            this.colNomorPO.VisibleIndex = 0;
            this.colNomorPO.Width = 90;
            // 
            // colNomorPB
            // 
            this.colNomorPB.FieldName = "NomorPB";
            this.colNomorPB.Name = "colNomorPB";
            this.colNomorPB.Visible = true;
            this.colNomorPB.VisibleIndex = 1;
            this.colNomorPB.Width = 90;
            // 
            // colDate
            // 
            this.colDate.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDate.FieldName = "Date";
            this.colDate.Name = "colDate";
            this.colDate.Visible = true;
            this.colDate.VisibleIndex = 2;
            this.colDate.Width = 90;
            // 
            // colRFQ
            // 
            this.colRFQ.FieldName = "RFQ";
            this.colRFQ.Name = "colRFQ";
            this.colRFQ.Visible = true;
            this.colRFQ.VisibleIndex = 3;
            this.colRFQ.Width = 150;
            // 
            // colidSupplier
            // 
            this.colidSupplier.Caption = "Supplier";
            this.colidSupplier.FieldName = "Supplier";
            this.colidSupplier.Name = "colidSupplier";
            this.colidSupplier.Visible = true;
            this.colidSupplier.VisibleIndex = 4;
            this.colidSupplier.Width = 250;
            // 
            // colKurs
            // 
            this.colKurs.FieldName = "Kurs";
            this.colKurs.Name = "colKurs";
            this.colKurs.Visible = true;
            this.colKurs.VisibleIndex = 5;
            this.colKurs.Width = 90;
            // 
            // colTerms
            // 
            this.colTerms.FieldName = "Terms";
            this.colTerms.Name = "colTerms";
            this.colTerms.Visible = true;
            this.colTerms.VisibleIndex = 6;
            this.colTerms.Width = 90;
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 7;
            this.colRemarks.Width = 200;
            // 
            // colShippingAddress
            // 
            this.colShippingAddress.Caption = "Shipping Address";
            this.colShippingAddress.FieldName = "ShippingAddress";
            this.colShippingAddress.Name = "colShippingAddress";
            this.colShippingAddress.Visible = true;
            this.colShippingAddress.VisibleIndex = 8;
            this.colShippingAddress.Width = 250;
            // 
            // colBillingAddress
            // 
            this.colBillingAddress.FieldName = "BillingAddress";
            this.colBillingAddress.Name = "colBillingAddress";
            this.colBillingAddress.Visible = true;
            this.colBillingAddress.VisibleIndex = 9;
            this.colBillingAddress.Width = 250;
            // 
            // colSubtotal
            // 
            this.colSubtotal.DisplayFormat.FormatString = "n0";
            this.colSubtotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSubtotal.FieldName = "Subtotal";
            this.colSubtotal.Name = "colSubtotal";
            this.colSubtotal.Visible = true;
            this.colSubtotal.VisibleIndex = 10;
            this.colSubtotal.Width = 90;
            // 
            // colPPNPercent
            // 
            this.colPPNPercent.DisplayFormat.FormatString = "n2";
            this.colPPNPercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPPNPercent.FieldName = "PPNPercent";
            this.colPPNPercent.Name = "colPPNPercent";
            this.colPPNPercent.Visible = true;
            this.colPPNPercent.VisibleIndex = 11;
            this.colPPNPercent.Width = 70;
            // 
            // colPPNAmount
            // 
            this.colPPNAmount.DisplayFormat.FormatString = "n0";
            this.colPPNAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPPNAmount.FieldName = "PPNAmount";
            this.colPPNAmount.Name = "colPPNAmount";
            this.colPPNAmount.Visible = true;
            this.colPPNAmount.VisibleIndex = 12;
            this.colPPNAmount.Width = 90;
            // 
            // colTotal
            // 
            this.colTotal.DisplayFormat.FormatString = "n0";
            this.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 13;
            this.colTotal.Width = 100;
            // 
            // colDateApproved
            // 
            this.colDateApproved.FieldName = "DateApproved";
            this.colDateApproved.Name = "colDateApproved";
            this.colDateApproved.Visible = true;
            this.colDateApproved.VisibleIndex = 14;
            this.colDateApproved.Width = 100;
            // 
            // colApprovedBy
            // 
            this.colApprovedBy.FieldName = "ApprovedBy";
            this.colApprovedBy.Name = "colApprovedBy";
            this.colApprovedBy.Visible = true;
            this.colApprovedBy.VisibleIndex = 15;
            this.colApprovedBy.Width = 100;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnView);
            this.panelControl1.Controls.Add(this.btnApprove);
            this.panelControl1.Controls.Add(this.btnPrint);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cmbDateStart);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.cmbDateEnd);
            this.panelControl1.Controls.Add(this.lblRefresh);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 40);
            this.panelControl1.TabIndex = 0;
            // 
            // btnView
            // 
            this.btnView.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnView.Appearance.Options.UseFont = true;
            this.btnView.Location = new System.Drawing.Point(293, 5);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(90, 30);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "&View";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnApprove.Appearance.Options.UseFont = true;
            this.btnApprove.Location = new System.Drawing.Point(485, 5);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Size = new System.Drawing.Size(90, 30);
            this.btnApprove.TabIndex = 5;
            this.btnApprove.Text = "Approve";
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.Location = new System.Drawing.Point(389, 5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(90, 30);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "&Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(767, 12);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(12, 17);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "to";
            this.labelControl2.Visible = false;
            // 
            // cmbDateStart
            // 
            this.cmbDateStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDateStart.EditValue = null;
            this.cmbDateStart.Location = new System.Drawing.Point(621, 9);
            this.cmbDateStart.Name = "cmbDateStart";
            this.cmbDateStart.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDateStart.Properties.Appearance.Options.UseFont = true;
            this.cmbDateStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDateStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDateStart.Size = new System.Drawing.Size(140, 24);
            this.cmbDateStart.TabIndex = 6;
            this.cmbDateStart.Visible = false;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(588, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(28, 17);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "from";
            this.labelControl3.Visible = false;
            // 
            // cmbDateEnd
            // 
            this.cmbDateEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDateEnd.EditValue = null;
            this.cmbDateEnd.Location = new System.Drawing.Point(785, 9);
            this.cmbDateEnd.Name = "cmbDateEnd";
            this.cmbDateEnd.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDateEnd.Properties.Appearance.Options.UseFont = true;
            this.cmbDateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDateEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDateEnd.Size = new System.Drawing.Size(140, 24);
            this.cmbDateEnd.TabIndex = 7;
            this.cmbDateEnd.Visible = false;
            // 
            // lblRefresh
            // 
            this.lblRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblRefresh.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRefresh.Location = new System.Drawing.Point(931, 12);
            this.lblRefresh.Name = "lblRefresh";
            this.lblRefresh.Size = new System.Drawing.Size(41, 17);
            this.lblRefresh.TabIndex = 9;
            this.lblRefresh.Text = "refresh";
            this.lblRefresh.Visible = false;
            this.lblRefresh.Click += new System.EventHandler(this.lblRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(197, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(101, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(5, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmPOtoSupplierRegular
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmPOtoSupplierRegular";
            this.Text = "PO to Supplier Regular";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDateEnd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblRefresh;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colNomorPO;
        private DevExpress.XtraGrid.Columns.GridColumn colNomorPB;
        private DevExpress.XtraGrid.Columns.GridColumn colDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRFQ;
        private DevExpress.XtraGrid.Columns.GridColumn colidSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colKurs;
        private DevExpress.XtraGrid.Columns.GridColumn colTerms;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colShippingAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colSubtotal;
        private DevExpress.XtraGrid.Columns.GridColumn colPPNPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colPPNAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotal;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit cmbDateStart;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit cmbDateEnd;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private DevExpress.XtraEditors.SimpleButton btnApprove;
        private DevExpress.XtraGrid.Columns.GridColumn colDateApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedBy;
        private DevExpress.XtraEditors.SimpleButton btnView;
    }
}