﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Linq;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewQuotationIn : DevExpress.XtraEditors.XtraForm
    {
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmNewQuotationIn()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            RefreshData();
        }

        private void RefreshData()
        {
            db = new GreenChemDataContext();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            txtTransactionNumber.Text = db.GetQuotationInNumber();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (db.QuotationIns.Any(x => x.TransactionNumber == txtTransactionNumber.Text))
                {
                    XtraMessageBox.Show("Transaction Number is already exist. System will change the number to the newest number.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTransactionNumber.Text = db.GetQuotationInNumber();
                    txtTransactionNumber.Focus();
                    return;
                }

                QuotationIn quotation = new QuotationIn();
                if (frmUtama._iddepartment != null) quotation.idDepartment = frmUtama._iddepartment;
                if (frmUtama._idsubdepartment != null) quotation.idSubDepartment = frmUtama._idsubdepartment;
                if (cmbSupplier.EditValue.IsNotEmpty()) quotation.idSupplier = cmbSupplier.EditValue.ToInteger();

                quotation.TransactionNumber = txtTransactionNumber.Text;
                quotation.TransactionDate = cmbDate.DateTime;
                quotation.idUserAccount = frmUtama._iduseraccount;
                quotation.Remarks = txtRemarks.Text;
                quotation.SupplierName = txtName.Text;

                db.QuotationIns.InsertOnSubmit(quotation);
                db.SubmitChanges();

                //details
                foreach (var item in listdetail)
                {
                    if (item.Quantity != 0)
                    {
                        DetailQuotationIn detail = new DetailQuotationIn();
                        detail.idProduct = item.idProduct;
                        detail.idQuotationIn = quotation.id;
                        detail.Quantity = item.Quantity;
                        detail.Price = item.Price;
                        detail.Discount = item.Discount;
                        detail.Subtotal = item.Subtotal;
                        detail.ProductName = item.Remarks;
                        db.DetailQuotationIns.InsertOnSubmit(detail);
                    }
                }
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailQuotationIn form = new frmDetailQuotationIn(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}