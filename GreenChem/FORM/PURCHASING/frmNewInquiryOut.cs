﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Linq;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewInquiryOut : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewInquiryOut()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            RefreshData();
        }

        private void RefreshData()
        {
            db = new GreenChemDataContext();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            txtTransactionNumber.Text = db.GetInquiryOutNumber();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();

                    if (db.InquiryOuts.Any(x => x.TransactionNumber == txtTransactionNumber.Text))
                    {
                        XtraMessageBox.Show("Transaction Number is already exist. System will change the number to the newest number.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtTransactionNumber.Text = db.GetInquiryOutNumber();
                        return;
                    }

                    InquiryOut inquiry = new InquiryOut();
                    if (cmbDepartment.EditValue.IsNotEmpty()) inquiry.idDepartment = cmbDepartment.EditValue.ToInteger();
                    if (cmbSubDepartment.EditValue.IsNotEmpty()) inquiry.idSubDepartment = cmbSubDepartment.EditValue.ToInteger();
                    if (cmbSupplier.EditValue.IsNotEmpty()) inquiry.idSupplier = cmbSupplier.EditValue.ToInteger();
                   
                    
                    inquiry.TransactionNumber = txtTransactionNumber.Text;
                    inquiry.TransactionDate = cmbDate.DateTime;
                    inquiry.idUserAccount = frmUtama._iduseraccount;
                    inquiry.Remarks = txtRemarks.Text;

                    db.InquiryOuts.InsertOnSubmit(inquiry);
                    db.SubmitChanges();

                    XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}