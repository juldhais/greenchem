﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;
using System.IO;

namespace GreenChem.MenuTransaction
{
    public partial class frmPOtoSupplierRegular : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPOtoSupplierRegular()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDateStart.DateTime = DateTime.Today.AddYears(-1);
            cmbDateEnd.DateTime = DateTime.Today.AddYears(1);
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New PO to Supplier Regular");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit PO to Supplier Regular");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete PO to Supplier Regular");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print PO to Supplier Regular");
            btnApprove.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve PO to Supplier Regular");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.POtoSupplierRegulars
                            where x.Date.Value.Date >= cmbDateStart.DateTime.Date && x.Date.Value.Date <= cmbDateEnd.DateTime.Date
                            select new
                            {
                                x.id,
                                x.NomorPO,
                                x.NomorPB,
                                x.Date,
                                x.RFQ,
                                Supplier = db.Suppliers.First(a => a.id == x.idSupplier).Name,
                                x.Kurs,
                                x.Terms,
                                x.Remarks,
                                x.ShippingAddress,
                                x.BillingAddress,
                                x.Subtotal,
                                x.PPNPercent,
                                x.PPNAmount,
                                x.Total,
                                x.DateApproved,
                                ApprovedBy = x.ApprovedBy != null ? db.UserAccounts.First(a => a.id == x.ApprovedBy).Name : ""
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewPOtoSupplierRegular form = new frmNewPOtoSupplierRegular();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditPOtoSupplierRegular form = new frmEditPOtoSupplierRegular(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.ShowDialog();
                lblRefresh_Click(null, null);
                Cursor.Current = Cursors.Default;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    db.ExecuteCommand("DELETE FROM dbo.DetailPOtoSupplierRegular WHERE idPOtoSupplierRegular = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.POtoSupplierRegular WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblRefresh_Click(null, null);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                db = new GreenChemDataContext();
                var po = db.POtoSupplierRegulars.First(x => x.id == id);
                if (po.Approved == true)
                {
                    var supplier = db.Suppliers.First(x => x.id == po.idSupplier);

                    List<TempPOtoSupplierRegular> list = new List<TempPOtoSupplierRegular>();
                    foreach (var item in po.DetailPOtoSupplierRegulars)
                    {
                        TempPOtoSupplierRegular temp = new TempPOtoSupplierRegular();
                        temp.NomorPO = po.NomorPO;
                        temp.NomorPB = po.NomorPB;
                        temp.Date = po.Date.Value;
                        temp.RFQ = po.RFQ;
                        temp.Supplier = supplier.Name;
                        temp.UP = supplier.Person;
                        temp.Phone = supplier.Phone;
                        temp.Fax = supplier.Fax;
                        temp.Kurs = po.Kurs;
                        temp.Terms = po.Terms;
                        temp.Remarks = po.Remarks;
                        temp.ShippingAddress = po.ShippingAddress;
                        temp.BillingAddress = po.BillingAddress;
                        temp.TaxPercent = po.PPNPercent;
                        temp.TaxAmount = po.PPNAmount;
                        temp.Total = po.Total;
                        if (item.idBahanBaku != null) temp.BahanBaku = item.BahanBaku.Nama;
                        if (item.idBahanPenolong != null) temp.BahanBaku = db.BahanPenolongs.First(x => x.id == item.idBahanPenolong).Nama;
                        temp.Quantity = item.Quantity;
                        temp.UoM = item.UoM;
                        temp.Price = item.Price;
                        temp.Subtotal = item.Subtotal;
                        if (po.idUserAccount.HasValue) temp.UserName = db.UserAccounts.First(x => x.id == po.idUserAccount).Name;
                        list.Add(temp);
                    }

                    rptPOtoSupplierRegular report = new rptPOtoSupplierRegular();
                    if (File.Exists("POtoSupplierRegular.repx")) report.LoadLayout("POtoSupplierRegular.repx");
                    report.DataSource = list;
                    report.ShowPreview();
                    report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
                }
                else
                {
                    XtraMessageBox.Show("PO is not approved yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("NomorPO").Safe();

                db = new GreenChemDataContext();
                POtoSupplierRegular po = db.POtoSupplierRegulars.First(x => x.id == id);

                if (po.Approved != true)
                {
                    DialogResult = XtraMessageBox.Show("Approve this PO : " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (DialogResult == DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        po.Approved = true;
                        po.ApprovedBy = frmUtama._iduseraccount;
                        po.DateApproved = DateTime.Now;
                        db.SubmitChanges();
                        lblRefresh_Click(null, null);

                        XtraMessageBox.Show("PO has been approved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    XtraMessageBox.Show("PO is approved already.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditPOtoSupplierRegular form = new frmEditPOtoSupplierRegular(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.btnSave.Enabled = false;
                form.ShowDialog();
                lblRefresh_Click(null, null);
                Cursor.Current = Cursors.Default;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}