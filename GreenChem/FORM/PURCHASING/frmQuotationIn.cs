﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmQuotationIn : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmQuotationIn()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Quotation In");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Quotation In");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Quotation In");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.QuotationIns
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Supplier = x.Supplier.Name,
                                x.SupplierName,
                                Department = x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                x.Remarks
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewQuotationIn form = new frmNewQuotationIn();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditQuotationIn form = new frmEditQuotationIn(id);
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                QuotationIn inquiry = db.QuotationIns.First(x => x.id == id);
                db.QuotationIns.DeleteOnSubmit(inquiry);
                db.SubmitChanges();
                RefreshData();
                XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmViewQuotationIn form = new frmViewQuotationIn(id);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}