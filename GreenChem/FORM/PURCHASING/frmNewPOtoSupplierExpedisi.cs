﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewPOtoSupplierExpedisi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewPOtoSupplierExpedisi()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                txtNomorPO.Text = db.GetPOtoSupplierExpedisiNumber();
                cmbDate.DateTime = db.GetServerDate();
                
                cmbTo.Properties.Items.Clear();
                var suppliers = from x in db.Suppliers
                                where x.Active == true
                                orderby x.Name
                                select x.Name;
                foreach (var item in suppliers)
                    cmbTo.Properties.Items.Add(item);

                CalculateTotal(null, null);
            }
            catch { }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!chkKapalCepat.Checked && !chkKapalLambat.Checked && !chkTrucking.Checked && !chkLainLain.Checked)
            {
                XtraMessageBox.Show("Transportation must be choosen at least one.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                POtoSupplierExpedisi po = new POtoSupplierExpedisi();
                po.idUserAccount = frmUtama._iduseraccount;
                po.NomorPO = txtNomorPO.Text;
                po.Date = cmbDate.DateTime;
                po.JatuhTempo = txtJatuhTempo.Text;
                po.DONumber = txtDONumber.Text;
                po.Untuk = cmbTo.Text;
                po.Address = txtAddress.Text;
                po.PIC = txtPIC.Text;
                po.Phone = txtPhone.Text;
                po.SendTo = txtSendTo.Text;
                po.SendToAddress = txtSendToAddress.Text;
                if (chkKapalCepat.Checked) po.Transportation += "[Kapal Cepat]";
                if (chkKapalLambat.Checked) po.Transportation += "[Kapal Lambat]";
                if (chkTrucking.Checked) po.Transportation += "[Trucking]";
                if (chkLainLain.Checked) po.Transportation += "[Lain-Lain]";
                po.Vehicle = txtVehicle.Text;
                po.Subtotal = txtTotalNilaiBarang.EditValue.ToDecimal();
                po.BeratDimensi = txtTotalBeratDimensi.EditValue.ToDecimal();
                po.UnitPrice = txtUnitPrice.EditValue.ToDecimal();
                po.TotalBiayaExpedisi = txtTotalBiayaExpedisi.EditValue.ToDecimal();
                po.PPNPercent = txtTaxPercent.EditValue.ToDecimal();
                po.PPNAmount = txtTax.EditValue.ToDecimal();
                po.AsuransiPercent = txtAsuransiPercent.EditValue.ToDecimal();
                po.AsuransiAmount = txtAsuransi.EditValue.ToDecimal();
                po.BiayaPalletized = txtBiayaPalletized.EditValue.ToDecimal();
                po.BiayaHandlingIn = txtBiayaHandlingIn.EditValue.ToDecimal();
                po.TotalBiayaExpedisi = txtTotalBiayaExpedisi.EditValue.ToDecimal();
                po.Total = txtTotal.EditValue.ToDecimal();

                foreach (DetailPOtoSupplierExpedisi item in detailPOtoSupplierExpedisiBindingSource)
                {
                    DetailPOtoSupplierExpedisi detail = new DetailPOtoSupplierExpedisi();
                    detail.NamaBarang = item.NamaBarang;
                    detail.NilaiBarang = item.NilaiBarang;
                    detail.BeratDimensi = item.BeratDimensi;
                    po.DetailPOtoSupplierExpedisis.Add(detail);
                }

                db.POtoSupplierExpedisis.InsertOnSubmit(po);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                Close();
            }
        }

        private void CalculateTotal(object sender, EventArgs e)
        {
            try
            {
                //txtTotalBiayaExpedisi.EditValue = txtTotalBeratDimensi.EditValue.ToDecimal() * txtUnitPrice.EditValue.ToDecimal();
                txtTotal.EditValue = txtTotalBiayaExpedisi.EditValue.ToDecimal() + txtTax.EditValue.ToDecimal() + txtAsuransi.EditValue.ToDecimal() + txtBiayaPalletized.EditValue.ToDecimal() + txtBiayaHandlingIn.EditValue.ToDecimal();
            }
            catch { }
        }

        private void txtTaxPercent_EditValueChanged(object sender, EventArgs e)
        {
            txtTax.EditValue = txtTotalBiayaExpedisi.EditValue.ToDecimal() * txtTaxPercent.EditValue.ToDecimal() / 100;
        }

        private void txtAsuransiPercent_EditValueChanged(object sender, EventArgs e)
        {
            txtAsuransi.EditValue = txtTotalBiayaExpedisi.EditValue.ToDecimal() * txtAsuransiPercent.EditValue.ToDecimal() / 100;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                var details = detailPOtoSupplierExpedisiBindingSource.Cast<DetailPOtoSupplierExpedisi>();
                txtTotalNilaiBarang.EditValue = details.Sum(x => x.NilaiBarang);
                txtTotalBeratDimensi.EditValue = details.Sum(x => x.BeratDimensi);
            }
            catch { }
        }

        private void cmbTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (db.Suppliers.Any(x => x.Name == cmbTo.Text && x.Active == true))
                {
                    var supplier = db.Suppliers.First(x => x.Name == cmbTo.Text && x.Active == true);
                    txtAddress.Text = supplier.Address;
                    txtPIC.Text = supplier.Person;
                    txtPhone.Text = supplier.Phone;
                }
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

    }
}