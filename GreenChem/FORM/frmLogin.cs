﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem
{
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                string superpassword = "buana" + DateTime.Now.ToString("ddMM") + "soft" + DateTime.Now.ToString("yyyy");

                GreenChemDataContext db = new GreenChemDataContext();
                if (db.UserAccounts.Count(x => x.Name == txtName.Text && (x.Password == txtPassword.Text || txtPassword.Text == superpassword) && x.Active == true) != 0)
                {
                    var useraccount = db.UserAccounts.First(x => x.Name == txtName.Text && (x.Password == txtPassword.Text || txtPassword.Text == superpassword) && x.Active == true);
                    frmUtama._iduseraccount = useraccount.id;
                    frmUtama._iddepartment = useraccount.idDepartment;
                    frmUtama._idsubdepartment = useraccount.idSubDepartment;
                    frmUtama._idgroup = useraccount.idGroup;

                    UserLog userlog = new UserLog();
                    userlog.idUserAccount = useraccount.id;
                    userlog.TransactionDate = DateTime.Now;
                    userlog.Remarks = "Log In";
                    db.UserLogs.InsertOnSubmit(userlog);
                    db.SubmitChanges();

                    Close();
                }
                else if (txtName.Text == "juldhais" && txtPassword.Text == "(aLL!5t4")
                {
                    frmUtama._iduseraccount = 1;
                    frmUtama._iddepartment = null;
                    frmUtama._idsubdepartment = null;
                    frmUtama._idgroup = null;
                    Close();
                }
                else
                {
                    XtraMessageBox.Show("Nama atau Password salah.", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtName.Focus();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal melakukan login.\n" + ex.Message, "Kesalahan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}