﻿namespace GreenChem.MenuTransaction2
{
    partial class frmEditPengembanganProduk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmbTanggalPermintaan = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtNoPRD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtJudul = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTanggalApproval = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDueDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTanggalSelesai = new DevExpress.XtraEditors.DateEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtLamaRiset = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPRD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJudul.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaRiset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmbStatus);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtLamaRiset);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.cmbTanggalSelesai);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.cmbDueDate);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.cmbTanggalApproval);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtJudul);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtNoPRD);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cmbTanggalPermintaan);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(570, 265);
            this.panelControl1.TabIndex = 0;
            // 
            // cmbTanggalPermintaan
            // 
            this.cmbTanggalPermintaan.EditValue = null;
            this.cmbTanggalPermintaan.Location = new System.Drawing.Point(114, 77);
            this.cmbTanggalPermintaan.Name = "cmbTanggalPermintaan";
            this.cmbTanggalPermintaan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalPermintaan.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalPermintaan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalPermintaan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalPermintaan.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalPermintaan.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(18, 80);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 17);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Tgl. Permintaan";
            // 
            // txtNoPRD
            // 
            this.txtNoPRD.Location = new System.Drawing.Point(114, 17);
            this.txtNoPRD.Name = "txtNoPRD";
            this.txtNoPRD.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNoPRD.Properties.Appearance.Options.UseFont = true;
            this.txtNoPRD.Size = new System.Drawing.Size(200, 24);
            this.txtNoPRD.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(18, 20);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(49, 17);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "No. PRD";
            // 
            // txtJudul
            // 
            this.txtJudul.Location = new System.Drawing.Point(114, 47);
            this.txtJudul.Name = "txtJudul";
            this.txtJudul.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtJudul.Properties.Appearance.Options.UseFont = true;
            this.txtJudul.Size = new System.Drawing.Size(433, 24);
            this.txtJudul.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(18, 50);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(73, 17);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Judul/Tujuan";
            // 
            // cmbTanggalApproval
            // 
            this.cmbTanggalApproval.EditValue = null;
            this.cmbTanggalApproval.Location = new System.Drawing.Point(114, 107);
            this.cmbTanggalApproval.Name = "cmbTanggalApproval";
            this.cmbTanggalApproval.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalApproval.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalApproval.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalApproval.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalApproval.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalApproval.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(18, 110);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(78, 17);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Tgl. Approval";
            // 
            // cmbDueDate
            // 
            this.cmbDueDate.EditValue = null;
            this.cmbDueDate.Location = new System.Drawing.Point(114, 137);
            this.cmbDueDate.Name = "cmbDueDate";
            this.cmbDueDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDueDate.Properties.Appearance.Options.UseFont = true;
            this.cmbDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDueDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDueDate.Size = new System.Drawing.Size(150, 24);
            this.cmbDueDate.TabIndex = 4;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(18, 140);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(54, 17);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "Due Date";
            // 
            // cmbTanggalSelesai
            // 
            this.cmbTanggalSelesai.EditValue = null;
            this.cmbTanggalSelesai.Location = new System.Drawing.Point(114, 167);
            this.cmbTanggalSelesai.Name = "cmbTanggalSelesai";
            this.cmbTanggalSelesai.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalSelesai.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalSelesai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalSelesai.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalSelesai.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalSelesai.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(18, 170);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(65, 17);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Tgl. Selesai";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(18, 200);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(63, 17);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Lama Riset";
            // 
            // txtLamaRiset
            // 
            this.txtLamaRiset.Location = new System.Drawing.Point(114, 197);
            this.txtLamaRiset.Name = "txtLamaRiset";
            this.txtLamaRiset.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLamaRiset.Properties.Appearance.Options.UseFont = true;
            this.txtLamaRiset.Properties.Mask.EditMask = "n0";
            this.txtLamaRiset.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtLamaRiset.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtLamaRiset.Size = new System.Drawing.Size(80, 24);
            this.txtLamaRiset.TabIndex = 6;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(200, 200);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(22, 17);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "hari";
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "Open";
            this.cmbStatus.Enabled = false;
            this.cmbStatus.Location = new System.Drawing.Point(114, 227);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "Open",
            "Closed"});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(150, 23);
            this.cmbStatus.TabIndex = 7;
            this.cmbStatus.TabStop = false;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(18, 229);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(35, 17);
            this.labelControl9.TabIndex = 20;
            this.labelControl9.Text = "Status";
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(502, 283);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(396, 283);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmEditPengembanganProduk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 327);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditPengembanganProduk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Pengembangan Produk";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPRD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJudul.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaRiset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit cmbTanggalPermintaan;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtNoPRD;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtJudul;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit cmbTanggalApproval;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DateEdit cmbDueDate;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.DateEdit cmbTanggalSelesai;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtLamaRiset;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        public DevExpress.XtraEditors.SimpleButton btnSave;
    }
}