﻿namespace GreenChem.MenuTransaction
{
    partial class frmLabService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.labServiceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanggalPermintaan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanggalApproval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetLama = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanggalSelesai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLamaRiset = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermintaanLabService = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasilLabService = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.lblRefresh = new DevExpress.XtraEditors.LabelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labServiceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.DataSource = this.labServiceBindingSource;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 40);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(984, 522);
            this.gridControl.TabIndex = 9;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // labServiceBindingSource
            // 
            this.labServiceBindingSource.DataSource = typeof(GreenChem.LabService);
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colTransactionNumber,
            this.colSE,
            this.colTanggalPermintaan,
            this.colTanggalApproval,
            this.colTargetLama,
            this.colTanggalSelesai,
            this.colLamaRiset,
            this.colPermintaanLabService,
            this.colHasilLabService,
            this.colStatus});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.RowHeight = 23;
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colTransactionNumber
            // 
            this.colTransactionNumber.Caption = "No. Lab Service";
            this.colTransactionNumber.FieldName = "TransactionNumber";
            this.colTransactionNumber.Name = "colTransactionNumber";
            this.colTransactionNumber.Visible = true;
            this.colTransactionNumber.VisibleIndex = 0;
            this.colTransactionNumber.Width = 125;
            // 
            // colSE
            // 
            this.colSE.FieldName = "SE";
            this.colSE.Name = "colSE";
            this.colSE.Visible = true;
            this.colSE.VisibleIndex = 1;
            this.colSE.Width = 90;
            // 
            // colTanggalPermintaan
            // 
            this.colTanggalPermintaan.Caption = "Tgl. Permintaan";
            this.colTanggalPermintaan.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colTanggalPermintaan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTanggalPermintaan.FieldName = "TanggalPermintaan";
            this.colTanggalPermintaan.Name = "colTanggalPermintaan";
            this.colTanggalPermintaan.Visible = true;
            this.colTanggalPermintaan.VisibleIndex = 2;
            this.colTanggalPermintaan.Width = 110;
            // 
            // colTanggalApproval
            // 
            this.colTanggalApproval.Caption = "Tgl. Approval";
            this.colTanggalApproval.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colTanggalApproval.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTanggalApproval.FieldName = "TanggalApproval";
            this.colTanggalApproval.Name = "colTanggalApproval";
            this.colTanggalApproval.Visible = true;
            this.colTanggalApproval.VisibleIndex = 3;
            this.colTanggalApproval.Width = 110;
            // 
            // colTargetLama
            // 
            this.colTargetLama.FieldName = "TargetLama";
            this.colTargetLama.Name = "colTargetLama";
            this.colTargetLama.Visible = true;
            this.colTargetLama.VisibleIndex = 4;
            this.colTargetLama.Width = 125;
            // 
            // colTanggalSelesai
            // 
            this.colTanggalSelesai.Caption = "Tgl. Selesai";
            this.colTanggalSelesai.DisplayFormat.FormatString = "dd/MM/yyyy";
            this.colTanggalSelesai.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTanggalSelesai.FieldName = "TanggalSelesai";
            this.colTanggalSelesai.Name = "colTanggalSelesai";
            this.colTanggalSelesai.Visible = true;
            this.colTanggalSelesai.VisibleIndex = 5;
            this.colTanggalSelesai.Width = 110;
            // 
            // colLamaRiset
            // 
            this.colLamaRiset.FieldName = "LamaRiset";
            this.colLamaRiset.Name = "colLamaRiset";
            this.colLamaRiset.Visible = true;
            this.colLamaRiset.VisibleIndex = 6;
            this.colLamaRiset.Width = 125;
            // 
            // colPermintaanLabService
            // 
            this.colPermintaanLabService.Caption = "Permintaan Lab Service";
            this.colPermintaanLabService.FieldName = "PermintaanLabService";
            this.colPermintaanLabService.Name = "colPermintaanLabService";
            this.colPermintaanLabService.Visible = true;
            this.colPermintaanLabService.VisibleIndex = 7;
            this.colPermintaanLabService.Width = 400;
            // 
            // colHasilLabService
            // 
            this.colHasilLabService.FieldName = "HasilLabService";
            this.colHasilLabService.Name = "colHasilLabService";
            this.colHasilLabService.Visible = true;
            this.colHasilLabService.VisibleIndex = 8;
            this.colHasilLabService.Width = 300;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 9;
            this.colStatus.Width = 125;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnPrint);
            this.panelControl1.Controls.Add(this.lblRefresh);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 40);
            this.panelControl1.TabIndex = 8;
            // 
            // btnPrint
            // 
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.Location = new System.Drawing.Point(303, 5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(90, 30);
            this.btnPrint.TabIndex = 11;
            this.btnPrint.Text = "&Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // lblRefresh
            // 
            this.lblRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblRefresh.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRefresh.Location = new System.Drawing.Point(931, 12);
            this.lblRefresh.Name = "lblRefresh";
            this.lblRefresh.Size = new System.Drawing.Size(41, 17);
            this.lblRefresh.TabIndex = 9;
            this.lblRefresh.Text = "refresh";
            this.lblRefresh.Click += new System.EventHandler(this.lblRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(197, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(101, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(5, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmLabService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmLabService";
            this.Text = "Lab Service";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labServiceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblRefresh;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private System.Windows.Forms.BindingSource labServiceBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSE;
        private DevExpress.XtraGrid.Columns.GridColumn colTanggalPermintaan;
        private DevExpress.XtraGrid.Columns.GridColumn colTanggalApproval;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetLama;
        private DevExpress.XtraGrid.Columns.GridColumn colTanggalSelesai;
        private DevExpress.XtraGrid.Columns.GridColumn colLamaRiset;
        private DevExpress.XtraGrid.Columns.GridColumn colPermintaanLabService;
        private DevExpress.XtraGrid.Columns.GridColumn colHasilLabService;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
    }
}