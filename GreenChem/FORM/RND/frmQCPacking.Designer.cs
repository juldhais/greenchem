﻿namespace GreenChem.MenuTransaction
{
    partial class frmQCPacking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanggalPelaksanaan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMulaiPacking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelesaiPacking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLamaPacking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJumlah = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUoM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBatchNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanggalPengiriman = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPanjangStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPanjangAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLebarStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLebarAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTinggiStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTinggiAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKemasanStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKemasanAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabelingMSDSStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabelingMSDSAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlatPendukungStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlatPendukungAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaletStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaletAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSyaratKhususStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSyaratKhususAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKesimpulan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeterangan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnView = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaletAktual = new DevExpress.XtraEditors.SimpleButton();
            this.btnKesimpulan = new DevExpress.XtraEditors.SimpleButton();
            this.lblRefresh = new DevExpress.XtraEditors.LabelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 40);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(984, 522);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colTanggalPelaksanaan,
            this.colMulaiPacking,
            this.colSelesaiPacking,
            this.colLamaPacking,
            this.colProduct,
            this.colCustomer,
            this.colJumlah,
            this.colUoM,
            this.colRatio,
            this.colBatchNo,
            this.colTanggalPengiriman,
            this.colPanjangStandard,
            this.colPanjangAktual,
            this.colLebarStandard,
            this.colLebarAktual,
            this.colTinggiStandard,
            this.colTinggiAktual,
            this.colKemasanStandard,
            this.colKemasanAktual,
            this.colLabelingMSDSStandard,
            this.colLabelingMSDSAktual,
            this.colAlatPendukungStandard,
            this.colAlatPendukungAktual,
            this.colPaletStandard,
            this.colPaletAktual,
            this.colSyaratKhususStandard,
            this.colSyaratKhususAktual,
            this.colKesimpulan,
            this.colKeterangan});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.RowHeight = 23;
            this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colTanggalPelaksanaan
            // 
            this.colTanggalPelaksanaan.Caption = "Tgl. Pelaksanaan";
            this.colTanggalPelaksanaan.FieldName = "TanggalPelaksanaan";
            this.colTanggalPelaksanaan.Name = "colTanggalPelaksanaan";
            this.colTanggalPelaksanaan.Visible = true;
            this.colTanggalPelaksanaan.VisibleIndex = 0;
            this.colTanggalPelaksanaan.Width = 110;
            // 
            // colMulaiPacking
            // 
            this.colMulaiPacking.FieldName = "MulaiPacking";
            this.colMulaiPacking.Name = "colMulaiPacking";
            this.colMulaiPacking.Visible = true;
            this.colMulaiPacking.VisibleIndex = 1;
            this.colMulaiPacking.Width = 100;
            // 
            // colSelesaiPacking
            // 
            this.colSelesaiPacking.FieldName = "SelesaiPacking";
            this.colSelesaiPacking.Name = "colSelesaiPacking";
            this.colSelesaiPacking.Visible = true;
            this.colSelesaiPacking.VisibleIndex = 2;
            this.colSelesaiPacking.Width = 100;
            // 
            // colLamaPacking
            // 
            this.colLamaPacking.FieldName = "LamaPacking";
            this.colLamaPacking.Name = "colLamaPacking";
            this.colLamaPacking.Visible = true;
            this.colLamaPacking.VisibleIndex = 3;
            this.colLamaPacking.Width = 100;
            // 
            // colProduct
            // 
            this.colProduct.Caption = "Product";
            this.colProduct.FieldName = "Product";
            this.colProduct.Name = "colProduct";
            this.colProduct.Visible = true;
            this.colProduct.VisibleIndex = 4;
            this.colProduct.Width = 200;
            // 
            // colCustomer
            // 
            this.colCustomer.Caption = "Customer";
            this.colCustomer.FieldName = "Customer";
            this.colCustomer.Name = "colCustomer";
            this.colCustomer.Visible = true;
            this.colCustomer.VisibleIndex = 5;
            this.colCustomer.Width = 200;
            // 
            // colJumlah
            // 
            this.colJumlah.DisplayFormat.FormatString = "#,#0.####";
            this.colJumlah.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJumlah.FieldName = "Jumlah";
            this.colJumlah.Name = "colJumlah";
            this.colJumlah.Visible = true;
            this.colJumlah.VisibleIndex = 6;
            this.colJumlah.Width = 100;
            // 
            // colUoM
            // 
            this.colUoM.Caption = "UoM";
            this.colUoM.FieldName = "UoM";
            this.colUoM.Name = "colUoM";
            this.colUoM.Visible = true;
            this.colUoM.VisibleIndex = 7;
            this.colUoM.Width = 100;
            // 
            // colRatio
            // 
            this.colRatio.FieldName = "Ratio";
            this.colRatio.Name = "colRatio";
            this.colRatio.Width = 150;
            // 
            // colBatchNo
            // 
            this.colBatchNo.FieldName = "BatchNo";
            this.colBatchNo.Name = "colBatchNo";
            this.colBatchNo.Visible = true;
            this.colBatchNo.VisibleIndex = 8;
            this.colBatchNo.Width = 150;
            // 
            // colTanggalPengiriman
            // 
            this.colTanggalPengiriman.FieldName = "TanggalPengiriman";
            this.colTanggalPengiriman.Name = "colTanggalPengiriman";
            this.colTanggalPengiriman.Visible = true;
            this.colTanggalPengiriman.VisibleIndex = 9;
            this.colTanggalPengiriman.Width = 150;
            // 
            // colPanjangStandard
            // 
            this.colPanjangStandard.FieldName = "PanjangStandard";
            this.colPanjangStandard.Name = "colPanjangStandard";
            this.colPanjangStandard.Visible = true;
            this.colPanjangStandard.VisibleIndex = 10;
            this.colPanjangStandard.Width = 150;
            // 
            // colPanjangAktual
            // 
            this.colPanjangAktual.FieldName = "PanjangAktual";
            this.colPanjangAktual.Name = "colPanjangAktual";
            this.colPanjangAktual.Visible = true;
            this.colPanjangAktual.VisibleIndex = 11;
            this.colPanjangAktual.Width = 150;
            // 
            // colLebarStandard
            // 
            this.colLebarStandard.FieldName = "LebarStandard";
            this.colLebarStandard.Name = "colLebarStandard";
            this.colLebarStandard.Visible = true;
            this.colLebarStandard.VisibleIndex = 12;
            this.colLebarStandard.Width = 150;
            // 
            // colLebarAktual
            // 
            this.colLebarAktual.FieldName = "LebarAktual";
            this.colLebarAktual.Name = "colLebarAktual";
            this.colLebarAktual.Visible = true;
            this.colLebarAktual.VisibleIndex = 13;
            this.colLebarAktual.Width = 150;
            // 
            // colTinggiStandard
            // 
            this.colTinggiStandard.FieldName = "TinggiStandard";
            this.colTinggiStandard.Name = "colTinggiStandard";
            this.colTinggiStandard.Visible = true;
            this.colTinggiStandard.VisibleIndex = 14;
            this.colTinggiStandard.Width = 150;
            // 
            // colTinggiAktual
            // 
            this.colTinggiAktual.FieldName = "TinggiAktual";
            this.colTinggiAktual.Name = "colTinggiAktual";
            this.colTinggiAktual.Visible = true;
            this.colTinggiAktual.VisibleIndex = 15;
            this.colTinggiAktual.Width = 150;
            // 
            // colKemasanStandard
            // 
            this.colKemasanStandard.FieldName = "KemasanStandard";
            this.colKemasanStandard.Name = "colKemasanStandard";
            this.colKemasanStandard.Visible = true;
            this.colKemasanStandard.VisibleIndex = 16;
            this.colKemasanStandard.Width = 150;
            // 
            // colKemasanAktual
            // 
            this.colKemasanAktual.FieldName = "KemasanAktual";
            this.colKemasanAktual.Name = "colKemasanAktual";
            this.colKemasanAktual.Visible = true;
            this.colKemasanAktual.VisibleIndex = 17;
            this.colKemasanAktual.Width = 150;
            // 
            // colLabelingMSDSStandard
            // 
            this.colLabelingMSDSStandard.FieldName = "LabelingMSDSStandard";
            this.colLabelingMSDSStandard.Name = "colLabelingMSDSStandard";
            this.colLabelingMSDSStandard.Visible = true;
            this.colLabelingMSDSStandard.VisibleIndex = 18;
            this.colLabelingMSDSStandard.Width = 150;
            // 
            // colLabelingMSDSAktual
            // 
            this.colLabelingMSDSAktual.FieldName = "LabelingMSDSAktual";
            this.colLabelingMSDSAktual.Name = "colLabelingMSDSAktual";
            this.colLabelingMSDSAktual.Visible = true;
            this.colLabelingMSDSAktual.VisibleIndex = 19;
            this.colLabelingMSDSAktual.Width = 150;
            // 
            // colAlatPendukungStandard
            // 
            this.colAlatPendukungStandard.FieldName = "AlatPendukungStandard";
            this.colAlatPendukungStandard.Name = "colAlatPendukungStandard";
            this.colAlatPendukungStandard.Visible = true;
            this.colAlatPendukungStandard.VisibleIndex = 20;
            this.colAlatPendukungStandard.Width = 150;
            // 
            // colAlatPendukungAktual
            // 
            this.colAlatPendukungAktual.FieldName = "AlatPendukungAktual";
            this.colAlatPendukungAktual.Name = "colAlatPendukungAktual";
            this.colAlatPendukungAktual.Visible = true;
            this.colAlatPendukungAktual.VisibleIndex = 21;
            this.colAlatPendukungAktual.Width = 150;
            // 
            // colPaletStandard
            // 
            this.colPaletStandard.FieldName = "PaletStandard";
            this.colPaletStandard.Name = "colPaletStandard";
            this.colPaletStandard.Visible = true;
            this.colPaletStandard.VisibleIndex = 22;
            this.colPaletStandard.Width = 150;
            // 
            // colPaletAktual
            // 
            this.colPaletAktual.FieldName = "PaletAktual";
            this.colPaletAktual.Name = "colPaletAktual";
            this.colPaletAktual.Visible = true;
            this.colPaletAktual.VisibleIndex = 23;
            this.colPaletAktual.Width = 150;
            // 
            // colSyaratKhususStandard
            // 
            this.colSyaratKhususStandard.FieldName = "SyaratKhususStandard";
            this.colSyaratKhususStandard.Name = "colSyaratKhususStandard";
            this.colSyaratKhususStandard.Visible = true;
            this.colSyaratKhususStandard.VisibleIndex = 24;
            this.colSyaratKhususStandard.Width = 150;
            // 
            // colSyaratKhususAktual
            // 
            this.colSyaratKhususAktual.FieldName = "SyaratKhususAktual";
            this.colSyaratKhususAktual.Name = "colSyaratKhususAktual";
            this.colSyaratKhususAktual.Visible = true;
            this.colSyaratKhususAktual.VisibleIndex = 25;
            this.colSyaratKhususAktual.Width = 150;
            // 
            // colKesimpulan
            // 
            this.colKesimpulan.FieldName = "Kesimpulan";
            this.colKesimpulan.Name = "colKesimpulan";
            this.colKesimpulan.Visible = true;
            this.colKesimpulan.VisibleIndex = 26;
            this.colKesimpulan.Width = 150;
            // 
            // colKeterangan
            // 
            this.colKeterangan.FieldName = "Keterangan";
            this.colKeterangan.Name = "colKeterangan";
            this.colKeterangan.Visible = true;
            this.colKeterangan.VisibleIndex = 27;
            this.colKeterangan.Width = 150;
            // 
            // btnPrint
            // 
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.Location = new System.Drawing.Point(303, 5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(90, 30);
            this.btnPrint.TabIndex = 12;
            this.btnPrint.Text = "&Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnView);
            this.panelControl1.Controls.Add(this.btnPaletAktual);
            this.panelControl1.Controls.Add(this.btnKesimpulan);
            this.panelControl1.Controls.Add(this.btnPrint);
            this.panelControl1.Controls.Add(this.lblRefresh);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 40);
            this.panelControl1.TabIndex = 0;
            // 
            // btnView
            // 
            this.btnView.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnView.Appearance.Options.UseFont = true;
            this.btnView.Location = new System.Drawing.Point(666, 5);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(90, 30);
            this.btnView.TabIndex = 19;
            this.btnView.Text = "&View";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnPaletAktual
            // 
            this.btnPaletAktual.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPaletAktual.Appearance.Options.UseFont = true;
            this.btnPaletAktual.Location = new System.Drawing.Point(535, 5);
            this.btnPaletAktual.Name = "btnPaletAktual";
            this.btnPaletAktual.Size = new System.Drawing.Size(120, 30);
            this.btnPaletAktual.TabIndex = 18;
            this.btnPaletAktual.Text = "&Palet Aktual";
            this.btnPaletAktual.Click += new System.EventHandler(this.btnPaletAktual_Click);
            // 
            // btnKesimpulan
            // 
            this.btnKesimpulan.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnKesimpulan.Appearance.Options.UseFont = true;
            this.btnKesimpulan.Location = new System.Drawing.Point(409, 5);
            this.btnKesimpulan.Name = "btnKesimpulan";
            this.btnKesimpulan.Size = new System.Drawing.Size(120, 30);
            this.btnKesimpulan.TabIndex = 17;
            this.btnKesimpulan.Text = "&Kesimpulan";
            this.btnKesimpulan.Click += new System.EventHandler(this.btnKesimpulan_Click);
            // 
            // lblRefresh
            // 
            this.lblRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblRefresh.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRefresh.Location = new System.Drawing.Point(931, 12);
            this.lblRefresh.Name = "lblRefresh";
            this.lblRefresh.Size = new System.Drawing.Size(41, 17);
            this.lblRefresh.TabIndex = 9;
            this.lblRefresh.Text = "refresh";
            this.lblRefresh.Click += new System.EventHandler(this.lblRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(197, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(101, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(5, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmQCPacking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmQCPacking";
            this.Text = "QC Packing";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colTanggalPelaksanaan;
        private DevExpress.XtraGrid.Columns.GridColumn colMulaiPacking;
        private DevExpress.XtraGrid.Columns.GridColumn colSelesaiPacking;
        private DevExpress.XtraGrid.Columns.GridColumn colLamaPacking;
        private DevExpress.XtraGrid.Columns.GridColumn colProduct;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colJumlah;
        private DevExpress.XtraGrid.Columns.GridColumn colUoM;
        private DevExpress.XtraGrid.Columns.GridColumn colRatio;
        private DevExpress.XtraGrid.Columns.GridColumn colBatchNo;
        private DevExpress.XtraGrid.Columns.GridColumn colTanggalPengiriman;
        private DevExpress.XtraGrid.Columns.GridColumn colPanjangStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colPanjangAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colLebarStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colLebarAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colTinggiStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colTinggiAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colKemasanStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colKemasanAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colLabelingMSDSStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colLabelingMSDSAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colAlatPendukungStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colAlatPendukungAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colPaletStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colPaletAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colSyaratKhususStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colSyaratKhususAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colKesimpulan;
        private DevExpress.XtraGrid.Columns.GridColumn colKeterangan;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblRefresh;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraEditors.SimpleButton btnPaletAktual;
        private DevExpress.XtraEditors.SimpleButton btnKesimpulan;
        private DevExpress.XtraEditors.SimpleButton btnView;
    }
}