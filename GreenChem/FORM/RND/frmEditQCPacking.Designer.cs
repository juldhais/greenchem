﻿namespace GreenChem.MenuTransaction
{
    partial class frmEditQCPacking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtKeterangan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmbTanggalPengiriman = new DevExpress.XtraEditors.DateEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtBatchNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbUoM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtQty = new DevExpress.XtraEditors.TextEdit();
            this.cmbCustomer = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cmbProduct = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtLamaPacking = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtSelesaiPacking = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtMulaiPacking = new DevExpress.XtraEditors.TimeEdit();
            this.cmbTanggalPelaksanaan = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtTinggiAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtSyaratKhususAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtTinggiStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtSyaratKhususStandard = new DevExpress.XtraEditors.TextEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txtPaletStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtAlatPendukungAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtAlatPendukungStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtLabellingMSDSAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtLabellingMSDSStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtKemasanAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtKemasanStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.cmbKesimpulan = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbPaletAktual = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.txtLebarStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtPanjangAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtLebarAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtPanjangStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPengiriman.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPengiriman.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUoM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSelesaiPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMulaiPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPelaksanaan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPelaksanaan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyaratKhususAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyaratKhususStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaletStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlatPendukungAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlatPendukungStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLabellingMSDSAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLabellingMSDSStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKemasanAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKemasanStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKesimpulan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPaletAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(28, 79);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(93, 17);
            this.labelControl14.TabIndex = 12;
            this.labelControl14.Text = "Tinggi Standard";
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.Location = new System.Drawing.Point(87, 369);
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKeterangan.Properties.Appearance.Options.UseFont = true;
            this.txtKeterangan.Size = new System.Drawing.Size(321, 24);
            this.txtKeterangan.TabIndex = 29;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl26.Location = new System.Drawing.Point(14, 372);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(67, 17);
            this.labelControl26.TabIndex = 28;
            this.labelControl26.Text = "Keterangan";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmbTanggalPengiriman);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.txtBatchNo);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.cmbUoM);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtQty);
            this.panelControl1.Controls.Add(this.cmbCustomer);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cmbProduct);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtLamaPacking);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtSelesaiPacking);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtMulaiPacking);
            this.panelControl1.Controls.Add(this.cmbTanggalPelaksanaan);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(442, 292);
            this.panelControl1.TabIndex = 0;
            // 
            // cmbTanggalPengiriman
            // 
            this.cmbTanggalPengiriman.EditValue = null;
            this.cmbTanggalPengiriman.Location = new System.Drawing.Point(117, 254);
            this.cmbTanggalPengiriman.Name = "cmbTanggalPengiriman";
            this.cmbTanggalPengiriman.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalPengiriman.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalPengiriman.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalPengiriman.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalPengiriman.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalPengiriman.TabIndex = 29;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(21, 257);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(90, 17);
            this.labelControl9.TabIndex = 28;
            this.labelControl9.Text = "Tgl. Pengiriman";
            // 
            // txtBatchNo
            // 
            this.txtBatchNo.Location = new System.Drawing.Point(117, 224);
            this.txtBatchNo.Name = "txtBatchNo";
            this.txtBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBatchNo.Properties.Appearance.Options.UseFont = true;
            this.txtBatchNo.Size = new System.Drawing.Size(315, 24);
            this.txtBatchNo.TabIndex = 27;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(58, 227);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(56, 17);
            this.labelControl8.TabIndex = 26;
            this.labelControl8.Text = "Batch No.";
            // 
            // cmbUoM
            // 
            this.cmbUoM.Location = new System.Drawing.Point(183, 194);
            this.cmbUoM.Name = "cmbUoM";
            this.cmbUoM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbUoM.Properties.Appearance.Options.UseFont = true;
            this.cmbUoM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUoM.Size = new System.Drawing.Size(100, 24);
            this.cmbUoM.TabIndex = 25;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(71, 197);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 17);
            this.labelControl3.TabIndex = 23;
            this.labelControl3.Text = "Jumlah";
            // 
            // txtQty
            // 
            this.txtQty.EditValue = "";
            this.txtQty.Location = new System.Drawing.Point(117, 194);
            this.txtQty.Name = "txtQty";
            this.txtQty.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQty.Properties.Appearance.Options.UseFont = true;
            this.txtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.txtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtQty.Properties.DisplayFormat.FormatString = "n2";
            this.txtQty.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQty.Properties.EditFormat.FormatString = "n2";
            this.txtQty.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQty.Properties.Mask.EditMask = "n2";
            this.txtQty.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtQty.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtQty.Size = new System.Drawing.Size(60, 24);
            this.txtQty.TabIndex = 24;
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.EditValue = "";
            this.cmbCustomer.Location = new System.Drawing.Point(117, 164);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbCustomer.Properties.Appearance.Options.UseFont = true;
            this.cmbCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCustomer.Properties.DisplayMember = "Name";
            this.cmbCustomer.Properties.NullText = "";
            this.cmbCustomer.Properties.ValueMember = "id";
            this.cmbCustomer.Properties.View = this.gridView1;
            this.cmbCustomer.Size = new System.Drawing.Size(315, 24);
            this.cmbCustomer.TabIndex = 9;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 23;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "id";
            this.gridColumn7.FieldName = "id";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Code";
            this.gridColumn8.FieldName = "Code";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 120;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Name";
            this.gridColumn9.FieldName = "Name";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 264;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(55, 167);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(56, 17);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Customer";
            // 
            // cmbProduct
            // 
            this.cmbProduct.EditValue = "";
            this.cmbProduct.Location = new System.Drawing.Point(117, 134);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbProduct.Properties.Appearance.Options.UseFont = true;
            this.cmbProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbProduct.Properties.DisplayMember = "Name";
            this.cmbProduct.Properties.NullText = "";
            this.cmbProduct.Properties.ValueMember = "id";
            this.cmbProduct.Properties.View = this.gridView2;
            this.cmbProduct.Size = new System.Drawing.Size(315, 24);
            this.cmbProduct.TabIndex = 22;
            this.cmbProduct.EditValueChanged += new System.EventHandler(this.cmbProduct_EditValueChanged);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 264;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(66, 137);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(45, 17);
            this.labelControl5.TabIndex = 21;
            this.labelControl5.Text = "Product";
            // 
            // txtLamaPacking
            // 
            this.txtLamaPacking.Location = new System.Drawing.Point(117, 104);
            this.txtLamaPacking.Name = "txtLamaPacking";
            this.txtLamaPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLamaPacking.Properties.Appearance.Options.UseFont = true;
            this.txtLamaPacking.Size = new System.Drawing.Size(166, 24);
            this.txtLamaPacking.TabIndex = 20;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(32, 107);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(79, 17);
            this.labelControl7.TabIndex = 19;
            this.labelControl7.Text = "Lama Packing";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(23, 77);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(88, 17);
            this.labelControl6.TabIndex = 17;
            this.labelControl6.Text = "Selesai Packing";
            // 
            // txtSelesaiPacking
            // 
            this.txtSelesaiPacking.EditValue = new System.DateTime(2015, 5, 20, 0, 0, 0, 0);
            this.txtSelesaiPacking.Location = new System.Drawing.Point(117, 74);
            this.txtSelesaiPacking.Name = "txtSelesaiPacking";
            this.txtSelesaiPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSelesaiPacking.Properties.Appearance.Options.UseFont = true;
            this.txtSelesaiPacking.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtSelesaiPacking.Properties.DisplayFormat.FormatString = "HH:mm";
            this.txtSelesaiPacking.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtSelesaiPacking.Properties.EditFormat.FormatString = "HH:mm";
            this.txtSelesaiPacking.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtSelesaiPacking.Properties.Mask.EditMask = "t";
            this.txtSelesaiPacking.Size = new System.Drawing.Size(90, 24);
            this.txtSelesaiPacking.TabIndex = 18;
            this.txtSelesaiPacking.EditValueChanged += new System.EventHandler(this.txtSelesaiPacking_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(31, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(80, 17);
            this.labelControl4.TabIndex = 15;
            this.labelControl4.Text = "Mulai Packing";
            // 
            // txtMulaiPacking
            // 
            this.txtMulaiPacking.EditValue = new System.DateTime(2015, 5, 20, 0, 0, 0, 0);
            this.txtMulaiPacking.Location = new System.Drawing.Point(117, 44);
            this.txtMulaiPacking.Name = "txtMulaiPacking";
            this.txtMulaiPacking.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMulaiPacking.Properties.Appearance.Options.UseFont = true;
            this.txtMulaiPacking.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMulaiPacking.Properties.DisplayFormat.FormatString = "HH:mm";
            this.txtMulaiPacking.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtMulaiPacking.Properties.EditFormat.FormatString = "HH:mm";
            this.txtMulaiPacking.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtMulaiPacking.Properties.Mask.EditMask = "t";
            this.txtMulaiPacking.Size = new System.Drawing.Size(90, 24);
            this.txtMulaiPacking.TabIndex = 16;
            // 
            // cmbTanggalPelaksanaan
            // 
            this.cmbTanggalPelaksanaan.EditValue = null;
            this.cmbTanggalPelaksanaan.Location = new System.Drawing.Point(117, 14);
            this.cmbTanggalPelaksanaan.Name = "cmbTanggalPelaksanaan";
            this.cmbTanggalPelaksanaan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalPelaksanaan.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalPelaksanaan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalPelaksanaan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalPelaksanaan.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalPelaksanaan.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(15, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(96, 17);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Tgl. Pelaksanaan";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl27.Location = new System.Drawing.Point(14, 342);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(67, 17);
            this.labelControl27.TabIndex = 26;
            this.labelControl27.Text = "Kesimpulan";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Location = new System.Drawing.Point(258, 79);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(35, 17);
            this.labelControl15.TabIndex = 14;
            this.labelControl15.Text = "Aktual";
            // 
            // txtTinggiAktual
            // 
            this.txtTinggiAktual.Location = new System.Drawing.Point(299, 76);
            this.txtTinggiAktual.Name = "txtTinggiAktual";
            this.txtTinggiAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTinggiAktual.Properties.Appearance.Options.UseFont = true;
            this.txtTinggiAktual.Size = new System.Drawing.Size(120, 24);
            this.txtTinggiAktual.TabIndex = 15;
            // 
            // txtSyaratKhususAktual
            // 
            this.txtSyaratKhususAktual.Location = new System.Drawing.Point(158, 304);
            this.txtSyaratKhususAktual.Name = "txtSyaratKhususAktual";
            this.txtSyaratKhususAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSyaratKhususAktual.Properties.Appearance.Options.UseFont = true;
            this.txtSyaratKhususAktual.Size = new System.Drawing.Size(250, 24);
            this.txtSyaratKhususAktual.TabIndex = 25;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl24.Location = new System.Drawing.Point(32, 307);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(120, 17);
            this.labelControl24.TabIndex = 24;
            this.labelControl24.Text = "Syarat Khusus Aktual";
            // 
            // txtTinggiStandard
            // 
            this.txtTinggiStandard.Location = new System.Drawing.Point(127, 76);
            this.txtTinggiStandard.Name = "txtTinggiStandard";
            this.txtTinggiStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTinggiStandard.Properties.Appearance.Options.UseFont = true;
            this.txtTinggiStandard.Size = new System.Drawing.Size(120, 24);
            this.txtTinggiStandard.TabIndex = 13;
            // 
            // txtSyaratKhususStandard
            // 
            this.txtSyaratKhususStandard.Location = new System.Drawing.Point(158, 274);
            this.txtSyaratKhususStandard.Name = "txtSyaratKhususStandard";
            this.txtSyaratKhususStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSyaratKhususStandard.Properties.Appearance.Options.UseFont = true;
            this.txtSyaratKhususStandard.Size = new System.Drawing.Size(250, 24);
            this.txtSyaratKhususStandard.TabIndex = 23;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txtKeterangan);
            this.panelControl3.Controls.Add(this.labelControl26);
            this.panelControl3.Controls.Add(this.labelControl27);
            this.panelControl3.Controls.Add(this.txtSyaratKhususAktual);
            this.panelControl3.Controls.Add(this.labelControl24);
            this.panelControl3.Controls.Add(this.txtSyaratKhususStandard);
            this.panelControl3.Controls.Add(this.labelControl25);
            this.panelControl3.Controls.Add(this.labelControl22);
            this.panelControl3.Controls.Add(this.txtPaletStandard);
            this.panelControl3.Controls.Add(this.labelControl23);
            this.panelControl3.Controls.Add(this.txtAlatPendukungAktual);
            this.panelControl3.Controls.Add(this.labelControl20);
            this.panelControl3.Controls.Add(this.txtAlatPendukungStandard);
            this.panelControl3.Controls.Add(this.labelControl21);
            this.panelControl3.Controls.Add(this.txtLabellingMSDSAktual);
            this.panelControl3.Controls.Add(this.labelControl18);
            this.panelControl3.Controls.Add(this.txtLabellingMSDSStandard);
            this.panelControl3.Controls.Add(this.labelControl19);
            this.panelControl3.Controls.Add(this.txtKemasanAktual);
            this.panelControl3.Controls.Add(this.labelControl17);
            this.panelControl3.Controls.Add(this.txtKemasanStandard);
            this.panelControl3.Controls.Add(this.labelControl16);
            this.panelControl3.Controls.Add(this.cmbKesimpulan);
            this.panelControl3.Controls.Add(this.cmbPaletAktual);
            this.panelControl3.Location = new System.Drawing.Point(460, 12);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(422, 413);
            this.panelControl3.TabIndex = 2;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl25.Location = new System.Drawing.Point(14, 277);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(138, 17);
            this.labelControl25.TabIndex = 22;
            this.labelControl25.Text = "Syarat Khusus Standard";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl22.Location = new System.Drawing.Point(85, 242);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(67, 17);
            this.labelControl22.TabIndex = 20;
            this.labelControl22.Text = "Palet Aktual";
            // 
            // txtPaletStandard
            // 
            this.txtPaletStandard.Location = new System.Drawing.Point(158, 209);
            this.txtPaletStandard.Name = "txtPaletStandard";
            this.txtPaletStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPaletStandard.Properties.Appearance.Options.UseFont = true;
            this.txtPaletStandard.Size = new System.Drawing.Size(250, 24);
            this.txtPaletStandard.TabIndex = 19;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl23.Location = new System.Drawing.Point(67, 212);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(85, 17);
            this.labelControl23.TabIndex = 18;
            this.labelControl23.Text = "Palet Standard";
            // 
            // txtAlatPendukungAktual
            // 
            this.txtAlatPendukungAktual.Location = new System.Drawing.Point(158, 174);
            this.txtAlatPendukungAktual.Name = "txtAlatPendukungAktual";
            this.txtAlatPendukungAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAlatPendukungAktual.Properties.Appearance.Options.UseFont = true;
            this.txtAlatPendukungAktual.Size = new System.Drawing.Size(250, 24);
            this.txtAlatPendukungAktual.TabIndex = 17;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(23, 177);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(129, 17);
            this.labelControl20.TabIndex = 16;
            this.labelControl20.Text = "Alat Pendukung Aktual";
            // 
            // txtAlatPendukungStandard
            // 
            this.txtAlatPendukungStandard.Location = new System.Drawing.Point(158, 144);
            this.txtAlatPendukungStandard.Name = "txtAlatPendukungStandard";
            this.txtAlatPendukungStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAlatPendukungStandard.Properties.Appearance.Options.UseFont = true;
            this.txtAlatPendukungStandard.Size = new System.Drawing.Size(250, 24);
            this.txtAlatPendukungStandard.TabIndex = 15;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(5, 147);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(147, 17);
            this.labelControl21.TabIndex = 14;
            this.labelControl21.Text = "Alat Pendukung Standard";
            // 
            // txtLabellingMSDSAktual
            // 
            this.txtLabellingMSDSAktual.Location = new System.Drawing.Point(158, 109);
            this.txtLabellingMSDSAktual.Name = "txtLabellingMSDSAktual";
            this.txtLabellingMSDSAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLabellingMSDSAktual.Properties.Appearance.Options.UseFont = true;
            this.txtLabellingMSDSAktual.Size = new System.Drawing.Size(250, 24);
            this.txtLabellingMSDSAktual.TabIndex = 13;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(22, 112);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(130, 17);
            this.labelControl18.TabIndex = 12;
            this.labelControl18.Text = "Labelling MSDS Aktual";
            // 
            // txtLabellingMSDSStandard
            // 
            this.txtLabellingMSDSStandard.Location = new System.Drawing.Point(158, 79);
            this.txtLabellingMSDSStandard.Name = "txtLabellingMSDSStandard";
            this.txtLabellingMSDSStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLabellingMSDSStandard.Properties.Appearance.Options.UseFont = true;
            this.txtLabellingMSDSStandard.Size = new System.Drawing.Size(250, 24);
            this.txtLabellingMSDSStandard.TabIndex = 11;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Location = new System.Drawing.Point(4, 82);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(148, 17);
            this.labelControl19.TabIndex = 10;
            this.labelControl19.Text = "Labelling MSDS Standard";
            // 
            // txtKemasanAktual
            // 
            this.txtKemasanAktual.Location = new System.Drawing.Point(158, 44);
            this.txtKemasanAktual.Name = "txtKemasanAktual";
            this.txtKemasanAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKemasanAktual.Properties.Appearance.Options.UseFont = true;
            this.txtKemasanAktual.Size = new System.Drawing.Size(250, 24);
            this.txtKemasanAktual.TabIndex = 9;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(60, 47);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(92, 17);
            this.labelControl17.TabIndex = 8;
            this.labelControl17.Text = "Kemasan Aktual";
            // 
            // txtKemasanStandard
            // 
            this.txtKemasanStandard.Location = new System.Drawing.Point(158, 14);
            this.txtKemasanStandard.Name = "txtKemasanStandard";
            this.txtKemasanStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKemasanStandard.Properties.Appearance.Options.UseFont = true;
            this.txtKemasanStandard.Size = new System.Drawing.Size(250, 24);
            this.txtKemasanStandard.TabIndex = 7;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(42, 17);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(110, 17);
            this.labelControl16.TabIndex = 6;
            this.labelControl16.Text = "Kemasan Standard";
            // 
            // cmbKesimpulan
            // 
            this.cmbKesimpulan.Location = new System.Drawing.Point(87, 339);
            this.cmbKesimpulan.Name = "cmbKesimpulan";
            this.cmbKesimpulan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbKesimpulan.Properties.Appearance.Options.UseFont = true;
            this.cmbKesimpulan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbKesimpulan.Size = new System.Drawing.Size(321, 24);
            this.cmbKesimpulan.TabIndex = 27;
            // 
            // cmbPaletAktual
            // 
            this.cmbPaletAktual.Location = new System.Drawing.Point(158, 239);
            this.cmbPaletAktual.Name = "cmbPaletAktual";
            this.cmbPaletAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbPaletAktual.Properties.Appearance.Options.UseFont = true;
            this.cmbPaletAktual.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPaletAktual.Size = new System.Drawing.Size(250, 24);
            this.cmbPaletAktual.TabIndex = 21;
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(802, 431);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtLebarStandard
            // 
            this.txtLebarStandard.Location = new System.Drawing.Point(127, 46);
            this.txtLebarStandard.Name = "txtLebarStandard";
            this.txtLebarStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLebarStandard.Properties.Appearance.Options.UseFont = true;
            this.txtLebarStandard.Size = new System.Drawing.Size(120, 24);
            this.txtLebarStandard.TabIndex = 9;
            // 
            // txtPanjangAktual
            // 
            this.txtPanjangAktual.Location = new System.Drawing.Point(299, 16);
            this.txtPanjangAktual.Name = "txtPanjangAktual";
            this.txtPanjangAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPanjangAktual.Properties.Appearance.Options.UseFont = true;
            this.txtPanjangAktual.Size = new System.Drawing.Size(120, 24);
            this.txtPanjangAktual.TabIndex = 7;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(31, 49);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(90, 17);
            this.labelControl12.TabIndex = 8;
            this.labelControl12.Text = "Lebar Standard";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(258, 49);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(35, 17);
            this.labelControl13.TabIndex = 10;
            this.labelControl13.Text = "Aktual";
            // 
            // txtLebarAktual
            // 
            this.txtLebarAktual.Location = new System.Drawing.Point(299, 46);
            this.txtLebarAktual.Name = "txtLebarAktual";
            this.txtLebarAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLebarAktual.Properties.Appearance.Options.UseFont = true;
            this.txtLebarAktual.Size = new System.Drawing.Size(120, 24);
            this.txtLebarAktual.TabIndex = 11;
            // 
            // txtPanjangStandard
            // 
            this.txtPanjangStandard.Location = new System.Drawing.Point(127, 16);
            this.txtPanjangStandard.Name = "txtPanjangStandard";
            this.txtPanjangStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPanjangStandard.Properties.Appearance.Options.UseFont = true;
            this.txtPanjangStandard.Size = new System.Drawing.Size(120, 24);
            this.txtPanjangStandard.TabIndex = 5;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(18, 19);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(103, 17);
            this.labelControl10.TabIndex = 4;
            this.labelControl10.Text = "Panjang Standard";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(258, 19);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(35, 17);
            this.labelControl11.TabIndex = 6;
            this.labelControl11.Text = "Aktual";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.txtTinggiStandard);
            this.panelControl2.Controls.Add(this.labelControl14);
            this.panelControl2.Controls.Add(this.labelControl15);
            this.panelControl2.Controls.Add(this.txtTinggiAktual);
            this.panelControl2.Controls.Add(this.txtLebarStandard);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.txtLebarAktual);
            this.panelControl2.Controls.Add(this.txtPanjangStandard);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.txtPanjangAktual);
            this.panelControl2.Location = new System.Drawing.Point(12, 310);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(442, 115);
            this.panelControl2.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(696, 431);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmEditQCPacking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 472);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditQCPacking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit QC Packing";
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPengiriman.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPengiriman.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUoM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSelesaiPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMulaiPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPelaksanaan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPelaksanaan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyaratKhususAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyaratKhususStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaletStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlatPendukungAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlatPendukungStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLabellingMSDSAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLabellingMSDSStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKemasanAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKemasanStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKesimpulan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPaletAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtKeterangan;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit cmbTanggalPengiriman;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtBatchNo;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit cmbUoM;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtQty;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbCustomer;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtLamaPacking;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TimeEdit txtSelesaiPacking;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TimeEdit txtMulaiPacking;
        private DevExpress.XtraEditors.DateEdit cmbTanggalPelaksanaan;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtTinggiAktual;
        private DevExpress.XtraEditors.TextEdit txtSyaratKhususAktual;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtTinggiStandard;
        private DevExpress.XtraEditors.TextEdit txtSyaratKhususStandard;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtPaletStandard;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit txtAlatPendukungAktual;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtAlatPendukungStandard;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtLabellingMSDSAktual;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtLabellingMSDSStandard;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtKemasanAktual;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtKemasanStandard;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.TextEdit txtLebarStandard;
        private DevExpress.XtraEditors.TextEdit txtPanjangAktual;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtLebarAktual;
        private DevExpress.XtraEditors.TextEdit txtPanjangStandard;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbKesimpulan;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPaletAktual;
        public DevExpress.XtraEditors.SimpleButton btnSave;
    }
}