﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmQCKemasan : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmQCKemasan()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New QC Kemasan");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit QC Kemasan");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete QC Kemasan");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print QC Kemasan");

            try
            {
                if (System.IO.File.Exists("layout_qckemasan.xml"))
                    gridView.RestoreLayoutFromXml("layout_qckemasan.xml");
            }
            catch { }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.QCKemasans
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TanggalTerima,
                                Product = db.Products.First(a => a.id == x.idProduct).Name,
                                x.Quantity,
                                x.UoM,
                                x.WaktuMulaiQC,
                                x.WaktuSelesaiQC,
                                x.LamaQC,
                                x.DOCISO,
                                x.PanjangStandard,
                                x.PanjangAktual,
                                x.LebarStandard,
                                x.LebarAktual,
                                x.TinggiStandard,
                                x.TinggiAktual,
                                x.DiameterStandard,
                                x.DiameterAktual,
                                x.FisikStandard,
                                x.FisikAktual,
                                x.FisikDalamStandard,
                                x.FisikDalamAktual,
                                x.FisikTutupStandard,
                                x.FisikTutupAktual,
                                x.DiamterTutupStandard,
                                x.DiameterTutupAktual,
                                x.WarnaTutupStandard,
                                x.WarnaTutupAktual,
                                x.WarnaKemasanStandard,
                                x.WarnaKemasanAktual,
                                x.KranStandard,
                                x.KranAktual,
                                x.WarnaProduk,
                                x.Kesimpulan,
                                x.Keterangan
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewQCKemasan form = new frmNewQCKemasan();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditQCKemasan form = new frmEditQCKemasan(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                DialogResult = XtraMessageBox.Show("Delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    QCBarangJadi qc = db.QCBarangJadis.First(x => x.id == id);
                    db.QCBarangJadis.DeleteOnSubmit(qc);
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                gridControl.ShowPrintPreview();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnKesimpulan_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmKesimpulanQC form = new frmKesimpulanQC();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditQCKemasan form = new frmEditQCKemasan(id);
                form.btnSave.Visible = false;
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_qckemasan.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}