﻿namespace GreenChem.MenuTransaction
{
    partial class frmEditQCKemasan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDiameterStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtFisikStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtWarnaProduk = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.txtPanjangAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtTinggiStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtDiameterAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.txtKranStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtFisikAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtKranAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtWarnaKemasanStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtWarnaKemasanAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtWarnaTutupStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtWarnaTutupAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtDiameterTutupStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtDiameterTutupAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtFisikTutupStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtFisikTutupAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtFisikDalamStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtFisikDalamAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtKeterangan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.cmbKesimpulan = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtTinggiAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtLebarStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtLebarAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtPanjangStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDOCISO = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbUoM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtQty = new DevExpress.XtraEditors.TextEdit();
            this.cmbProduct = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtLamaQC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtSelesaiQC = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtMulaiQC = new DevExpress.XtraEditors.TimeEdit();
            this.cmbTanggalTerima = new DevExpress.XtraEditors.DateEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKranStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKranAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaKemasanStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaKemasanAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaTutupStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaTutupAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterTutupStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterTutupAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikTutupStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikTutupAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikDalamStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikDalamAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKesimpulan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOCISO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUoM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaQC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSelesaiQC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMulaiQC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalTerima.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalTerima.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDiameterStandard
            // 
            this.txtDiameterStandard.Location = new System.Drawing.Point(167, 14);
            this.txtDiameterStandard.Name = "txtDiameterStandard";
            this.txtDiameterStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDiameterStandard.Properties.Appearance.Options.UseFont = true;
            this.txtDiameterStandard.Size = new System.Drawing.Size(120, 24);
            this.txtDiameterStandard.TabIndex = 1;
            // 
            // txtFisikStandard
            // 
            this.txtFisikStandard.Location = new System.Drawing.Point(167, 44);
            this.txtFisikStandard.Name = "txtFisikStandard";
            this.txtFisikStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFisikStandard.Properties.Appearance.Options.UseFont = true;
            this.txtFisikStandard.Size = new System.Drawing.Size(120, 24);
            this.txtFisikStandard.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(51, 17);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(110, 17);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Diameter Standard";
            // 
            // txtWarnaProduk
            // 
            this.txtWarnaProduk.Location = new System.Drawing.Point(103, 254);
            this.txtWarnaProduk.Name = "txtWarnaProduk";
            this.txtWarnaProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWarnaProduk.Properties.Appearance.Options.UseFont = true;
            this.txtWarnaProduk.Size = new System.Drawing.Size(356, 24);
            this.txtWarnaProduk.TabIndex = 32;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(298, 17);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(35, 17);
            this.labelControl9.TabIndex = 2;
            this.labelControl9.Text = "Aktual";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl28.Location = new System.Drawing.Point(80, 47);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(81, 17);
            this.labelControl28.TabIndex = 4;
            this.labelControl28.Text = "Fisik Standard";
            // 
            // txtPanjangAktual
            // 
            this.txtPanjangAktual.Location = new System.Drawing.Point(295, 15);
            this.txtPanjangAktual.Name = "txtPanjangAktual";
            this.txtPanjangAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPanjangAktual.Properties.Appearance.Options.UseFont = true;
            this.txtPanjangAktual.Size = new System.Drawing.Size(120, 24);
            this.txtPanjangAktual.TabIndex = 3;
            // 
            // txtTinggiStandard
            // 
            this.txtTinggiStandard.Location = new System.Drawing.Point(123, 75);
            this.txtTinggiStandard.Name = "txtTinggiStandard";
            this.txtTinggiStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTinggiStandard.Properties.Appearance.Options.UseFont = true;
            this.txtTinggiStandard.Size = new System.Drawing.Size(120, 24);
            this.txtTinggiStandard.TabIndex = 9;
            // 
            // txtDiameterAktual
            // 
            this.txtDiameterAktual.Location = new System.Drawing.Point(339, 14);
            this.txtDiameterAktual.Name = "txtDiameterAktual";
            this.txtDiameterAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDiameterAktual.Properties.Appearance.Options.UseFont = true;
            this.txtDiameterAktual.Size = new System.Drawing.Size(120, 24);
            this.txtDiameterAktual.TabIndex = 3;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(24, 78);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(93, 17);
            this.labelControl14.TabIndex = 8;
            this.labelControl14.Text = "Tinggi Standard";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txtDiameterStandard);
            this.panelControl3.Controls.Add(this.txtFisikStandard);
            this.panelControl3.Controls.Add(this.labelControl2);
            this.panelControl3.Controls.Add(this.txtWarnaProduk);
            this.panelControl3.Controls.Add(this.labelControl9);
            this.panelControl3.Controls.Add(this.labelControl28);
            this.panelControl3.Controls.Add(this.txtDiameterAktual);
            this.panelControl3.Controls.Add(this.labelControl32);
            this.panelControl3.Controls.Add(this.labelControl29);
            this.panelControl3.Controls.Add(this.txtKranStandard);
            this.panelControl3.Controls.Add(this.txtFisikAktual);
            this.panelControl3.Controls.Add(this.labelControl30);
            this.panelControl3.Controls.Add(this.labelControl31);
            this.panelControl3.Controls.Add(this.txtKranAktual);
            this.panelControl3.Controls.Add(this.txtWarnaKemasanStandard);
            this.panelControl3.Controls.Add(this.labelControl24);
            this.panelControl3.Controls.Add(this.labelControl25);
            this.panelControl3.Controls.Add(this.txtWarnaKemasanAktual);
            this.panelControl3.Controls.Add(this.txtWarnaTutupStandard);
            this.panelControl3.Controls.Add(this.labelControl22);
            this.panelControl3.Controls.Add(this.labelControl23);
            this.panelControl3.Controls.Add(this.txtWarnaTutupAktual);
            this.panelControl3.Controls.Add(this.txtDiameterTutupStandard);
            this.panelControl3.Controls.Add(this.labelControl20);
            this.panelControl3.Controls.Add(this.labelControl21);
            this.panelControl3.Controls.Add(this.txtDiameterTutupAktual);
            this.panelControl3.Controls.Add(this.txtFisikTutupStandard);
            this.panelControl3.Controls.Add(this.labelControl18);
            this.panelControl3.Controls.Add(this.labelControl19);
            this.panelControl3.Controls.Add(this.txtFisikTutupAktual);
            this.panelControl3.Controls.Add(this.txtFisikDalamStandard);
            this.panelControl3.Controls.Add(this.labelControl16);
            this.panelControl3.Controls.Add(this.labelControl17);
            this.panelControl3.Controls.Add(this.txtFisikDalamAktual);
            this.panelControl3.Controls.Add(this.txtKeterangan);
            this.panelControl3.Controls.Add(this.labelControl26);
            this.panelControl3.Controls.Add(this.labelControl27);
            this.panelControl3.Controls.Add(this.cmbKesimpulan);
            this.panelControl3.Location = new System.Drawing.Point(447, 12);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(472, 352);
            this.panelControl3.TabIndex = 2;
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl32.Location = new System.Drawing.Point(14, 257);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(83, 17);
            this.labelControl32.TabIndex = 31;
            this.labelControl32.Text = "Warna Produk";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl29.Location = new System.Drawing.Point(298, 47);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(35, 17);
            this.labelControl29.TabIndex = 6;
            this.labelControl29.Text = "Aktual";
            // 
            // txtKranStandard
            // 
            this.txtKranStandard.Location = new System.Drawing.Point(167, 224);
            this.txtKranStandard.Name = "txtKranStandard";
            this.txtKranStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKranStandard.Properties.Appearance.Options.UseFont = true;
            this.txtKranStandard.Size = new System.Drawing.Size(120, 24);
            this.txtKranStandard.TabIndex = 29;
            // 
            // txtFisikAktual
            // 
            this.txtFisikAktual.Location = new System.Drawing.Point(339, 44);
            this.txtFisikAktual.Name = "txtFisikAktual";
            this.txtFisikAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFisikAktual.Properties.Appearance.Options.UseFont = true;
            this.txtFisikAktual.Size = new System.Drawing.Size(120, 24);
            this.txtFisikAktual.TabIndex = 7;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl30.Location = new System.Drawing.Point(77, 227);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(84, 17);
            this.labelControl30.TabIndex = 28;
            this.labelControl30.Text = "Kran Standard";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl31.Location = new System.Drawing.Point(298, 227);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(35, 17);
            this.labelControl31.TabIndex = 52;
            this.labelControl31.Text = "Aktual";
            // 
            // txtKranAktual
            // 
            this.txtKranAktual.Location = new System.Drawing.Point(339, 224);
            this.txtKranAktual.Name = "txtKranAktual";
            this.txtKranAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKranAktual.Properties.Appearance.Options.UseFont = true;
            this.txtKranAktual.Size = new System.Drawing.Size(120, 24);
            this.txtKranAktual.TabIndex = 30;
            // 
            // txtWarnaKemasanStandard
            // 
            this.txtWarnaKemasanStandard.Location = new System.Drawing.Point(167, 194);
            this.txtWarnaKemasanStandard.Name = "txtWarnaKemasanStandard";
            this.txtWarnaKemasanStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWarnaKemasanStandard.Properties.Appearance.Options.UseFont = true;
            this.txtWarnaKemasanStandard.Size = new System.Drawing.Size(120, 24);
            this.txtWarnaKemasanStandard.TabIndex = 25;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl24.Location = new System.Drawing.Point(9, 197);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(152, 17);
            this.labelControl24.TabIndex = 24;
            this.labelControl24.Text = "Warna Kemasan Standard";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl25.Location = new System.Drawing.Point(298, 197);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(35, 17);
            this.labelControl25.TabIndex = 26;
            this.labelControl25.Text = "Aktual";
            // 
            // txtWarnaKemasanAktual
            // 
            this.txtWarnaKemasanAktual.Location = new System.Drawing.Point(339, 194);
            this.txtWarnaKemasanAktual.Name = "txtWarnaKemasanAktual";
            this.txtWarnaKemasanAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWarnaKemasanAktual.Properties.Appearance.Options.UseFont = true;
            this.txtWarnaKemasanAktual.Size = new System.Drawing.Size(120, 24);
            this.txtWarnaKemasanAktual.TabIndex = 27;
            // 
            // txtWarnaTutupStandard
            // 
            this.txtWarnaTutupStandard.Location = new System.Drawing.Point(167, 164);
            this.txtWarnaTutupStandard.Name = "txtWarnaTutupStandard";
            this.txtWarnaTutupStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWarnaTutupStandard.Properties.Appearance.Options.UseFont = true;
            this.txtWarnaTutupStandard.Size = new System.Drawing.Size(120, 24);
            this.txtWarnaTutupStandard.TabIndex = 21;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl22.Location = new System.Drawing.Point(29, 167);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(132, 17);
            this.labelControl22.TabIndex = 20;
            this.labelControl22.Text = "Warna Tutup Standard";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl23.Location = new System.Drawing.Point(298, 167);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(35, 17);
            this.labelControl23.TabIndex = 22;
            this.labelControl23.Text = "Aktual";
            // 
            // txtWarnaTutupAktual
            // 
            this.txtWarnaTutupAktual.Location = new System.Drawing.Point(339, 164);
            this.txtWarnaTutupAktual.Name = "txtWarnaTutupAktual";
            this.txtWarnaTutupAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWarnaTutupAktual.Properties.Appearance.Options.UseFont = true;
            this.txtWarnaTutupAktual.Size = new System.Drawing.Size(120, 24);
            this.txtWarnaTutupAktual.TabIndex = 23;
            // 
            // txtDiameterTutupStandard
            // 
            this.txtDiameterTutupStandard.Location = new System.Drawing.Point(167, 134);
            this.txtDiameterTutupStandard.Name = "txtDiameterTutupStandard";
            this.txtDiameterTutupStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDiameterTutupStandard.Properties.Appearance.Options.UseFont = true;
            this.txtDiameterTutupStandard.Size = new System.Drawing.Size(120, 24);
            this.txtDiameterTutupStandard.TabIndex = 39;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(14, 137);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(147, 17);
            this.labelControl20.TabIndex = 17;
            this.labelControl20.Text = "Diameter Tutup Standard";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(298, 137);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(35, 17);
            this.labelControl21.TabIndex = 18;
            this.labelControl21.Text = "Aktual";
            // 
            // txtDiameterTutupAktual
            // 
            this.txtDiameterTutupAktual.Location = new System.Drawing.Point(339, 134);
            this.txtDiameterTutupAktual.Name = "txtDiameterTutupAktual";
            this.txtDiameterTutupAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDiameterTutupAktual.Properties.Appearance.Options.UseFont = true;
            this.txtDiameterTutupAktual.Size = new System.Drawing.Size(120, 24);
            this.txtDiameterTutupAktual.TabIndex = 19;
            // 
            // txtFisikTutupStandard
            // 
            this.txtFisikTutupStandard.Location = new System.Drawing.Point(167, 104);
            this.txtFisikTutupStandard.Name = "txtFisikTutupStandard";
            this.txtFisikTutupStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFisikTutupStandard.Properties.Appearance.Options.UseFont = true;
            this.txtFisikTutupStandard.Size = new System.Drawing.Size(120, 24);
            this.txtFisikTutupStandard.TabIndex = 13;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(43, 107);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(118, 17);
            this.labelControl18.TabIndex = 12;
            this.labelControl18.Text = "Fisik Tutup Standard";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Location = new System.Drawing.Point(298, 107);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(35, 17);
            this.labelControl19.TabIndex = 14;
            this.labelControl19.Text = "Aktual";
            // 
            // txtFisikTutupAktual
            // 
            this.txtFisikTutupAktual.Location = new System.Drawing.Point(339, 104);
            this.txtFisikTutupAktual.Name = "txtFisikTutupAktual";
            this.txtFisikTutupAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFisikTutupAktual.Properties.Appearance.Options.UseFont = true;
            this.txtFisikTutupAktual.Size = new System.Drawing.Size(120, 24);
            this.txtFisikTutupAktual.TabIndex = 15;
            // 
            // txtFisikDalamStandard
            // 
            this.txtFisikDalamStandard.Location = new System.Drawing.Point(167, 74);
            this.txtFisikDalamStandard.Name = "txtFisikDalamStandard";
            this.txtFisikDalamStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFisikDalamStandard.Properties.Appearance.Options.UseFont = true;
            this.txtFisikDalamStandard.Size = new System.Drawing.Size(120, 24);
            this.txtFisikDalamStandard.TabIndex = 9;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(39, 77);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(122, 17);
            this.labelControl16.TabIndex = 8;
            this.labelControl16.Text = "Fisik Dalam Standard";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(298, 77);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(35, 17);
            this.labelControl17.TabIndex = 10;
            this.labelControl17.Text = "Aktual";
            // 
            // txtFisikDalamAktual
            // 
            this.txtFisikDalamAktual.Location = new System.Drawing.Point(339, 74);
            this.txtFisikDalamAktual.Name = "txtFisikDalamAktual";
            this.txtFisikDalamAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFisikDalamAktual.Properties.Appearance.Options.UseFont = true;
            this.txtFisikDalamAktual.Size = new System.Drawing.Size(120, 24);
            this.txtFisikDalamAktual.TabIndex = 11;
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.Location = new System.Drawing.Point(103, 314);
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKeterangan.Properties.Appearance.Options.UseFont = true;
            this.txtKeterangan.Size = new System.Drawing.Size(356, 24);
            this.txtKeterangan.TabIndex = 36;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl26.Location = new System.Drawing.Point(29, 318);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(67, 17);
            this.labelControl26.TabIndex = 35;
            this.labelControl26.Text = "Keterangan";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl27.Location = new System.Drawing.Point(30, 287);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(67, 17);
            this.labelControl27.TabIndex = 33;
            this.labelControl27.Text = "Kesimpulan";
            // 
            // cmbKesimpulan
            // 
            this.cmbKesimpulan.Location = new System.Drawing.Point(103, 284);
            this.cmbKesimpulan.Name = "cmbKesimpulan";
            this.cmbKesimpulan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbKesimpulan.Properties.Appearance.Options.UseFont = true;
            this.cmbKesimpulan.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbKesimpulan.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbKesimpulan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbKesimpulan.Size = new System.Drawing.Size(356, 24);
            this.cmbKesimpulan.TabIndex = 34;
            // 
            // txtTinggiAktual
            // 
            this.txtTinggiAktual.Location = new System.Drawing.Point(295, 75);
            this.txtTinggiAktual.Name = "txtTinggiAktual";
            this.txtTinggiAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTinggiAktual.Properties.Appearance.Options.UseFont = true;
            this.txtTinggiAktual.Size = new System.Drawing.Size(120, 24);
            this.txtTinggiAktual.TabIndex = 11;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Location = new System.Drawing.Point(254, 78);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(35, 17);
            this.labelControl15.TabIndex = 10;
            this.labelControl15.Text = "Aktual";
            // 
            // txtLebarStandard
            // 
            this.txtLebarStandard.Location = new System.Drawing.Point(123, 45);
            this.txtLebarStandard.Name = "txtLebarStandard";
            this.txtLebarStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLebarStandard.Properties.Appearance.Options.UseFont = true;
            this.txtLebarStandard.Size = new System.Drawing.Size(120, 24);
            this.txtLebarStandard.TabIndex = 5;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(27, 48);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(90, 17);
            this.labelControl12.TabIndex = 4;
            this.labelControl12.Text = "Lebar Standard";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.txtTinggiStandard);
            this.panelControl2.Controls.Add(this.labelControl14);
            this.panelControl2.Controls.Add(this.labelControl15);
            this.panelControl2.Controls.Add(this.txtTinggiAktual);
            this.panelControl2.Controls.Add(this.txtLebarStandard);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.txtLebarAktual);
            this.panelControl2.Controls.Add(this.txtPanjangStandard);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.txtPanjangAktual);
            this.panelControl2.Location = new System.Drawing.Point(12, 251);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(429, 113);
            this.panelControl2.TabIndex = 1;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(254, 48);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(35, 17);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "Aktual";
            // 
            // txtLebarAktual
            // 
            this.txtLebarAktual.Location = new System.Drawing.Point(295, 45);
            this.txtLebarAktual.Name = "txtLebarAktual";
            this.txtLebarAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLebarAktual.Properties.Appearance.Options.UseFont = true;
            this.txtLebarAktual.Size = new System.Drawing.Size(120, 24);
            this.txtLebarAktual.TabIndex = 7;
            // 
            // txtPanjangStandard
            // 
            this.txtPanjangStandard.Location = new System.Drawing.Point(123, 15);
            this.txtPanjangStandard.Name = "txtPanjangStandard";
            this.txtPanjangStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPanjangStandard.Properties.Appearance.Options.UseFont = true;
            this.txtPanjangStandard.Size = new System.Drawing.Size(120, 24);
            this.txtPanjangStandard.TabIndex = 1;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(14, 18);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(103, 17);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "Panjang Standard";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(254, 18);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(35, 17);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "Aktual";
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(852, 370);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(14, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tgl. Terima";
            // 
            // txtDOCISO
            // 
            this.txtDOCISO.Location = new System.Drawing.Point(85, 194);
            this.txtDOCISO.Name = "txtDOCISO";
            this.txtDOCISO.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDOCISO.Properties.Appearance.Options.UseFont = true;
            this.txtDOCISO.Size = new System.Drawing.Size(315, 24);
            this.txtDOCISO.TabIndex = 14;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtDOCISO);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.cmbUoM);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtQty);
            this.panelControl1.Controls.Add(this.cmbProduct);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtLamaQC);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtSelesaiQC);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtMulaiQC);
            this.panelControl1.Controls.Add(this.cmbTanggalTerima);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(429, 233);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(28, 197);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(51, 17);
            this.labelControl8.TabIndex = 13;
            this.labelControl8.Text = "DOC ISO";
            // 
            // cmbUoM
            // 
            this.cmbUoM.Location = new System.Drawing.Point(151, 164);
            this.cmbUoM.Name = "cmbUoM";
            this.cmbUoM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbUoM.Properties.Appearance.Options.UseFont = true;
            this.cmbUoM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUoM.Size = new System.Drawing.Size(100, 24);
            this.cmbUoM.TabIndex = 12;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(39, 167);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 17);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "Jumlah";
            // 
            // txtQty
            // 
            this.txtQty.EditValue = "";
            this.txtQty.Location = new System.Drawing.Point(85, 164);
            this.txtQty.Name = "txtQty";
            this.txtQty.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQty.Properties.Appearance.Options.UseFont = true;
            this.txtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.txtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtQty.Properties.DisplayFormat.FormatString = "n2";
            this.txtQty.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQty.Properties.EditFormat.FormatString = "n2";
            this.txtQty.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQty.Properties.Mask.EditMask = "n2";
            this.txtQty.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtQty.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtQty.Size = new System.Drawing.Size(60, 24);
            this.txtQty.TabIndex = 11;
            // 
            // cmbProduct
            // 
            this.cmbProduct.EditValue = "";
            this.cmbProduct.Location = new System.Drawing.Point(85, 134);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbProduct.Properties.Appearance.Options.UseFont = true;
            this.cmbProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbProduct.Properties.DisplayMember = "Name";
            this.cmbProduct.Properties.NullText = "";
            this.cmbProduct.Properties.ValueMember = "id";
            this.cmbProduct.Properties.View = this.gridView2;
            this.cmbProduct.Size = new System.Drawing.Size(315, 24);
            this.cmbProduct.TabIndex = 9;
            this.cmbProduct.EditValueChanged += new System.EventHandler(this.cmbProduct_EditValueChanged);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 264;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(34, 137);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(45, 17);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Product";
            // 
            // txtLamaQC
            // 
            this.txtLamaQC.Location = new System.Drawing.Point(85, 104);
            this.txtLamaQC.Name = "txtLamaQC";
            this.txtLamaQC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLamaQC.Properties.Appearance.Options.UseFont = true;
            this.txtLamaQC.Size = new System.Drawing.Size(166, 24);
            this.txtLamaQC.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(26, 107);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(53, 17);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "Lama QC";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(17, 77);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(62, 17);
            this.labelControl6.TabIndex = 4;
            this.labelControl6.Text = "Selesai QC";
            // 
            // txtSelesaiQC
            // 
            this.txtSelesaiQC.EditValue = new System.DateTime(2015, 5, 20, 0, 0, 0, 0);
            this.txtSelesaiQC.Location = new System.Drawing.Point(85, 74);
            this.txtSelesaiQC.Name = "txtSelesaiQC";
            this.txtSelesaiQC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSelesaiQC.Properties.Appearance.Options.UseFont = true;
            this.txtSelesaiQC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtSelesaiQC.Properties.DisplayFormat.FormatString = "HH:mm";
            this.txtSelesaiQC.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtSelesaiQC.Properties.EditFormat.FormatString = "HH:mm";
            this.txtSelesaiQC.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtSelesaiQC.Properties.Mask.EditMask = "t";
            this.txtSelesaiQC.Size = new System.Drawing.Size(90, 24);
            this.txtSelesaiQC.TabIndex = 5;
            this.txtSelesaiQC.EditValueChanged += new System.EventHandler(this.txtMulaiQC_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(25, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(54, 17);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Mulai QC";
            // 
            // txtMulaiQC
            // 
            this.txtMulaiQC.EditValue = new System.DateTime(2015, 5, 20, 0, 0, 0, 0);
            this.txtMulaiQC.Location = new System.Drawing.Point(85, 44);
            this.txtMulaiQC.Name = "txtMulaiQC";
            this.txtMulaiQC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtMulaiQC.Properties.Appearance.Options.UseFont = true;
            this.txtMulaiQC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtMulaiQC.Properties.DisplayFormat.FormatString = "HH:mm";
            this.txtMulaiQC.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtMulaiQC.Properties.EditFormat.FormatString = "HH:mm";
            this.txtMulaiQC.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtMulaiQC.Properties.Mask.EditMask = "t";
            this.txtMulaiQC.Size = new System.Drawing.Size(90, 24);
            this.txtMulaiQC.TabIndex = 3;
            this.txtMulaiQC.EditValueChanged += new System.EventHandler(this.txtMulaiQC_EditValueChanged);
            // 
            // cmbTanggalTerima
            // 
            this.cmbTanggalTerima.EditValue = null;
            this.cmbTanggalTerima.Location = new System.Drawing.Point(85, 14);
            this.cmbTanggalTerima.Name = "cmbTanggalTerima";
            this.cmbTanggalTerima.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalTerima.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalTerima.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalTerima.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalTerima.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalTerima.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(746, 370);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmEditQCKemasan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 412);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditQCKemasan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit QC Kemasan";
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKranStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKranAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaKemasanStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaKemasanAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaTutupStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWarnaTutupAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterTutupStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiameterTutupAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikTutupStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikTutupAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikDalamStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFisikDalamAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKesimpulan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTinggiAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLebarAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPanjangStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOCISO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUoM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaQC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSelesaiQC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMulaiQC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalTerima.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalTerima.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtDiameterStandard;
        private DevExpress.XtraEditors.TextEdit txtFisikStandard;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtWarnaProduk;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit txtPanjangAktual;
        private DevExpress.XtraEditors.TextEdit txtTinggiStandard;
        private DevExpress.XtraEditors.TextEdit txtDiameterAktual;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.TextEdit txtKranStandard;
        private DevExpress.XtraEditors.TextEdit txtFisikAktual;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit txtKranAktual;
        private DevExpress.XtraEditors.TextEdit txtWarnaKemasanStandard;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtWarnaKemasanAktual;
        private DevExpress.XtraEditors.TextEdit txtWarnaTutupStandard;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit txtWarnaTutupAktual;
        private DevExpress.XtraEditors.TextEdit txtDiameterTutupStandard;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtDiameterTutupAktual;
        private DevExpress.XtraEditors.TextEdit txtFisikTutupStandard;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtFisikTutupAktual;
        private DevExpress.XtraEditors.TextEdit txtFisikDalamStandard;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtFisikDalamAktual;
        private DevExpress.XtraEditors.TextEdit txtKeterangan;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit txtTinggiAktual;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtLebarStandard;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtLebarAktual;
        private DevExpress.XtraEditors.TextEdit txtPanjangStandard;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtDOCISO;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit cmbUoM;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtQty;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtLamaQC;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TimeEdit txtSelesaiQC;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TimeEdit txtMulaiQC;
        private DevExpress.XtraEditors.DateEdit cmbTanggalTerima;
        private DevExpress.XtraEditors.ComboBoxEdit cmbKesimpulan;
        public DevExpress.XtraEditors.SimpleButton btnSave;
    }
}