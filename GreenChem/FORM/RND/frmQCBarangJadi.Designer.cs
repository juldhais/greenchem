﻿namespace GreenChem.MenuTransaction
{
    partial class frmQCBarangJadi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanggalMasuk = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBatchNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJumlahMasuk = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUoM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaktuTerimaQC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaktuSelesaiQC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLamaQC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCISO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPenampilanStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPenampilanAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBauStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBauAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBeratJenisStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBeratJenisAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colpHStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colphAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKelarutanStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKelarutanAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSolidContentStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSolidContent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKesimpulan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPemusnahan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnView = new DevExpress.XtraEditors.SimpleButton();
            this.btnStatus = new DevExpress.XtraEditors.SimpleButton();
            this.btnKesimpulan = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.lblRefresh = new DevExpress.XtraEditors.LabelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 40);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(984, 522);
            this.gridControl.TabIndex = 11;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colTanggalMasuk,
            this.colProduct,
            this.colBatchNo,
            this.colJumlahMasuk,
            this.colUoM,
            this.colRatio,
            this.colWaktuTerimaQC,
            this.colWaktuSelesaiQC,
            this.colLamaQC,
            this.colDOCISO,
            this.colPenampilanStandard,
            this.colPenampilanAktual,
            this.colBauStandard,
            this.colBauAktual,
            this.colBeratJenisStandard,
            this.colBeratJenisAktual,
            this.colpHStandard,
            this.colphAktual,
            this.colKelarutanStandard,
            this.colKelarutanAktual,
            this.colSolidContentStandard,
            this.colSolidContent,
            this.colKesimpulan,
            this.colExpired,
            this.colCOA,
            this.colPemusnahan,
            this.colStatus,
            this.gridColumn1});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.RowHeight = 23;
            this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colTanggalMasuk
            // 
            this.colTanggalMasuk.Caption = "Tgl Masuk";
            this.colTanggalMasuk.FieldName = "TanggalMasuk";
            this.colTanggalMasuk.Name = "colTanggalMasuk";
            this.colTanggalMasuk.Visible = true;
            this.colTanggalMasuk.VisibleIndex = 0;
            this.colTanggalMasuk.Width = 100;
            // 
            // colProduct
            // 
            this.colProduct.Caption = "Product";
            this.colProduct.FieldName = "Product";
            this.colProduct.Name = "colProduct";
            this.colProduct.Visible = true;
            this.colProduct.VisibleIndex = 1;
            this.colProduct.Width = 250;
            // 
            // colBatchNo
            // 
            this.colBatchNo.FieldName = "BatchNo";
            this.colBatchNo.Name = "colBatchNo";
            this.colBatchNo.Visible = true;
            this.colBatchNo.VisibleIndex = 2;
            this.colBatchNo.Width = 200;
            // 
            // colJumlahMasuk
            // 
            this.colJumlahMasuk.Caption = "Qty";
            this.colJumlahMasuk.DisplayFormat.FormatString = "#,#0.####";
            this.colJumlahMasuk.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colJumlahMasuk.FieldName = "JumlahMasuk";
            this.colJumlahMasuk.Name = "colJumlahMasuk";
            this.colJumlahMasuk.Visible = true;
            this.colJumlahMasuk.VisibleIndex = 3;
            this.colJumlahMasuk.Width = 50;
            // 
            // colUoM
            // 
            this.colUoM.Caption = "UoM";
            this.colUoM.FieldName = "UoM";
            this.colUoM.Name = "colUoM";
            this.colUoM.Visible = true;
            this.colUoM.VisibleIndex = 4;
            this.colUoM.Width = 50;
            // 
            // colRatio
            // 
            this.colRatio.FieldName = "Ratio";
            this.colRatio.Name = "colRatio";
            // 
            // colWaktuTerimaQC
            // 
            this.colWaktuTerimaQC.Caption = "Waktu Terima";
            this.colWaktuTerimaQC.FieldName = "WaktuTerimaQC";
            this.colWaktuTerimaQC.Name = "colWaktuTerimaQC";
            this.colWaktuTerimaQC.Visible = true;
            this.colWaktuTerimaQC.VisibleIndex = 5;
            this.colWaktuTerimaQC.Width = 100;
            // 
            // colWaktuSelesaiQC
            // 
            this.colWaktuSelesaiQC.Caption = "Waktu Selesai";
            this.colWaktuSelesaiQC.FieldName = "WaktuSelesaiQC";
            this.colWaktuSelesaiQC.Name = "colWaktuSelesaiQC";
            this.colWaktuSelesaiQC.Visible = true;
            this.colWaktuSelesaiQC.VisibleIndex = 6;
            this.colWaktuSelesaiQC.Width = 100;
            // 
            // colLamaQC
            // 
            this.colLamaQC.Caption = "Lama QC";
            this.colLamaQC.FieldName = "LamaQC";
            this.colLamaQC.Name = "colLamaQC";
            this.colLamaQC.Visible = true;
            this.colLamaQC.VisibleIndex = 7;
            this.colLamaQC.Width = 100;
            // 
            // colDOCISO
            // 
            this.colDOCISO.Caption = "DOC ISO";
            this.colDOCISO.FieldName = "DOCISO";
            this.colDOCISO.Name = "colDOCISO";
            this.colDOCISO.Visible = true;
            this.colDOCISO.VisibleIndex = 8;
            this.colDOCISO.Width = 150;
            // 
            // colPenampilanStandard
            // 
            this.colPenampilanStandard.FieldName = "PenampilanStandard";
            this.colPenampilanStandard.Name = "colPenampilanStandard";
            this.colPenampilanStandard.Width = 150;
            // 
            // colPenampilanAktual
            // 
            this.colPenampilanAktual.FieldName = "PenampilanAktual";
            this.colPenampilanAktual.Name = "colPenampilanAktual";
            this.colPenampilanAktual.Visible = true;
            this.colPenampilanAktual.VisibleIndex = 9;
            this.colPenampilanAktual.Width = 150;
            // 
            // colBauStandard
            // 
            this.colBauStandard.FieldName = "BauStandard";
            this.colBauStandard.Name = "colBauStandard";
            this.colBauStandard.Visible = true;
            this.colBauStandard.VisibleIndex = 10;
            this.colBauStandard.Width = 150;
            // 
            // colBauAktual
            // 
            this.colBauAktual.FieldName = "BauAktual";
            this.colBauAktual.Name = "colBauAktual";
            this.colBauAktual.Visible = true;
            this.colBauAktual.VisibleIndex = 11;
            this.colBauAktual.Width = 150;
            // 
            // colBeratJenisStandard
            // 
            this.colBeratJenisStandard.FieldName = "BeratJenisStandard";
            this.colBeratJenisStandard.Name = "colBeratJenisStandard";
            this.colBeratJenisStandard.Visible = true;
            this.colBeratJenisStandard.VisibleIndex = 12;
            this.colBeratJenisStandard.Width = 150;
            // 
            // colBeratJenisAktual
            // 
            this.colBeratJenisAktual.FieldName = "BeratJenisAktual";
            this.colBeratJenisAktual.Name = "colBeratJenisAktual";
            this.colBeratJenisAktual.Visible = true;
            this.colBeratJenisAktual.VisibleIndex = 13;
            this.colBeratJenisAktual.Width = 150;
            // 
            // colpHStandard
            // 
            this.colpHStandard.FieldName = "pHStandard";
            this.colpHStandard.Name = "colpHStandard";
            this.colpHStandard.Visible = true;
            this.colpHStandard.VisibleIndex = 14;
            this.colpHStandard.Width = 150;
            // 
            // colphAktual
            // 
            this.colphAktual.FieldName = "phAktual";
            this.colphAktual.Name = "colphAktual";
            this.colphAktual.Visible = true;
            this.colphAktual.VisibleIndex = 15;
            this.colphAktual.Width = 150;
            // 
            // colKelarutanStandard
            // 
            this.colKelarutanStandard.FieldName = "KelarutanStandard";
            this.colKelarutanStandard.Name = "colKelarutanStandard";
            this.colKelarutanStandard.Visible = true;
            this.colKelarutanStandard.VisibleIndex = 16;
            this.colKelarutanStandard.Width = 150;
            // 
            // colKelarutanAktual
            // 
            this.colKelarutanAktual.FieldName = "KelarutanAktual";
            this.colKelarutanAktual.Name = "colKelarutanAktual";
            this.colKelarutanAktual.Visible = true;
            this.colKelarutanAktual.VisibleIndex = 17;
            this.colKelarutanAktual.Width = 150;
            // 
            // colSolidContentStandard
            // 
            this.colSolidContentStandard.FieldName = "SolidContentStandard";
            this.colSolidContentStandard.Name = "colSolidContentStandard";
            this.colSolidContentStandard.Visible = true;
            this.colSolidContentStandard.VisibleIndex = 18;
            this.colSolidContentStandard.Width = 150;
            // 
            // colSolidContent
            // 
            this.colSolidContent.FieldName = "SolidContent";
            this.colSolidContent.Name = "colSolidContent";
            this.colSolidContent.Width = 150;
            // 
            // colKesimpulan
            // 
            this.colKesimpulan.FieldName = "Kesimpulan";
            this.colKesimpulan.Name = "colKesimpulan";
            this.colKesimpulan.Visible = true;
            this.colKesimpulan.VisibleIndex = 19;
            this.colKesimpulan.Width = 150;
            // 
            // colExpired
            // 
            this.colExpired.FieldName = "Expired";
            this.colExpired.Name = "colExpired";
            this.colExpired.Visible = true;
            this.colExpired.VisibleIndex = 20;
            this.colExpired.Width = 150;
            // 
            // colCOA
            // 
            this.colCOA.FieldName = "COA";
            this.colCOA.Name = "colCOA";
            this.colCOA.Visible = true;
            this.colCOA.VisibleIndex = 21;
            this.colCOA.Width = 150;
            // 
            // colPemusnahan
            // 
            this.colPemusnahan.FieldName = "Pemusnahan";
            this.colPemusnahan.Name = "colPemusnahan";
            this.colPemusnahan.Visible = true;
            this.colPemusnahan.VisibleIndex = 22;
            this.colPemusnahan.Width = 150;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 23;
            this.colStatus.Width = 150;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remarks";
            this.gridColumn1.FieldName = "Remarks";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 24;
            this.gridColumn1.Width = 200;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnView);
            this.panelControl1.Controls.Add(this.btnStatus);
            this.panelControl1.Controls.Add(this.btnKesimpulan);
            this.panelControl1.Controls.Add(this.btnPrint);
            this.panelControl1.Controls.Add(this.lblRefresh);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 40);
            this.panelControl1.TabIndex = 10;
            // 
            // btnView
            // 
            this.btnView.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnView.Appearance.Options.UseFont = true;
            this.btnView.Location = new System.Drawing.Point(661, 5);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(90, 30);
            this.btnView.TabIndex = 18;
            this.btnView.Text = "&View";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnStatus
            // 
            this.btnStatus.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnStatus.Appearance.Options.UseFont = true;
            this.btnStatus.Location = new System.Drawing.Point(530, 5);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(120, 30);
            this.btnStatus.TabIndex = 14;
            this.btnStatus.Text = "&Status";
            this.btnStatus.Click += new System.EventHandler(this.btnStatus_Click);
            // 
            // btnKesimpulan
            // 
            this.btnKesimpulan.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnKesimpulan.Appearance.Options.UseFont = true;
            this.btnKesimpulan.Location = new System.Drawing.Point(404, 5);
            this.btnKesimpulan.Name = "btnKesimpulan";
            this.btnKesimpulan.Size = new System.Drawing.Size(120, 30);
            this.btnKesimpulan.TabIndex = 13;
            this.btnKesimpulan.Text = "&Kesimpulan";
            this.btnKesimpulan.Click += new System.EventHandler(this.btnKesimpulan_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.Location = new System.Drawing.Point(298, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(90, 30);
            this.btnPrint.TabIndex = 12;
            this.btnPrint.Text = "&Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // lblRefresh
            // 
            this.lblRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblRefresh.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRefresh.Location = new System.Drawing.Point(931, 12);
            this.lblRefresh.Name = "lblRefresh";
            this.lblRefresh.Size = new System.Drawing.Size(41, 17);
            this.lblRefresh.TabIndex = 9;
            this.lblRefresh.Text = "refresh";
            this.lblRefresh.Click += new System.EventHandler(this.lblRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(197, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(101, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(5, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmQCBarangJadi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmQCBarangJadi";
            this.Text = "QC Barang Jadi";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblRefresh;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colTanggalMasuk;
        private DevExpress.XtraGrid.Columns.GridColumn colProduct;
        private DevExpress.XtraGrid.Columns.GridColumn colBatchNo;
        private DevExpress.XtraGrid.Columns.GridColumn colJumlahMasuk;
        private DevExpress.XtraGrid.Columns.GridColumn colUoM;
        private DevExpress.XtraGrid.Columns.GridColumn colRatio;
        private DevExpress.XtraGrid.Columns.GridColumn colWaktuTerimaQC;
        private DevExpress.XtraGrid.Columns.GridColumn colWaktuSelesaiQC;
        private DevExpress.XtraGrid.Columns.GridColumn colLamaQC;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCISO;
        private DevExpress.XtraGrid.Columns.GridColumn colPenampilanStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colPenampilanAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colBauStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colBauAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colBeratJenisStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colBeratJenisAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colpHStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colphAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colKelarutanStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colKelarutanAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colSolidContentStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colSolidContent;
        private DevExpress.XtraGrid.Columns.GridColumn colKesimpulan;
        private DevExpress.XtraGrid.Columns.GridColumn colExpired;
        private DevExpress.XtraGrid.Columns.GridColumn colCOA;
        private DevExpress.XtraGrid.Columns.GridColumn colPemusnahan;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private DevExpress.XtraEditors.SimpleButton btnStatus;
        private DevExpress.XtraEditors.SimpleButton btnKesimpulan;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SimpleButton btnView;
    }
}