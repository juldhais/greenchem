﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmQCPacking : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmQCPacking()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New QC Packing");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit QC Packing");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete QC Packing");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print QC Packing");

            try
            {
                if (System.IO.File.Exists("layout_qcpacking.xml"))
                    gridView.RestoreLayoutFromXml("layout_qcpacking.xml");
            }
            catch { }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewQCPacking form = new frmNewQCPacking();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditQCPacking form = new frmEditQCPacking(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = XtraMessageBox.Show("Delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    QCPacking qc = db.QCPackings.First(x => x.id == id);
                    db.QCPackings.DeleteOnSubmit(qc);
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);

                    XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                gridControl.ShowPrintPreview();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.QCPackings
                            select new
                            {
                                x.id,
                                x.TanggalPelaksanaan,
                                x.MulaiPacking,
                                x.SelesaiPacking,
                                x.LamaPacking,
                                Product = db.Products.First(a => a.id == x.idProduct).Name,
                                Customer = db.Customers.First(a => a.id == x.idCustomer).Name,
                                x.Jumlah,
                                x.UoM,
                                x.Ratio,
                                x.BatchNo,
                                x.TanggalPengiriman,
                                x.PanjangStandard,
                                x.PanjangAktual,
                                x.LebarStandard,
                                x.LebarAktual,
                                x.TinggiStandard,
                                x.TinggiAktual,
                                x.KemasanStandard,
                                x.KemasanAktual,
                                x.LabelingMSDSStandard,
                                x.LabelingMSDSAktual,
                                x.AlatPendukungStandard,
                                x.AlatPendukungAktual,
                                x.PaletStandard,
                                x.PaletAktual,
                                x.SyaratKhususStandard,
                                x.SyaratKhususAktual,
                                x.Kesimpulan,
                                x.Keterangan
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnKesimpulan_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmKesimpulanQC form = new frmKesimpulanQC();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void btnPaletAktual_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmPaletAktualQC form = new frmPaletAktualQC();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditQCPacking form = new frmEditQCPacking(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.btnSave.Visible = false;
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_qcpacking.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}