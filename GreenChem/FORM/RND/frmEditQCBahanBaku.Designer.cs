﻿namespace GreenChem.MenuTransaction
{
    partial class frmEditQCBahanBaku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbExpired = new DevExpress.XtraEditors.DateEdit();
            this.txtPemusnahan = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txtPenampilanStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtPenampilanAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtBauStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtBauAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtSolidContent = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtSolidContentStandard = new DevExpress.XtraEditors.TextEdit();
            this.cmbProduct = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtCOA = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtKelarutanAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTanggalMasuk = new DevExpress.XtraEditors.DateEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSupplier = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtKelarutanStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtPHAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtPHStandard = new DevExpress.XtraEditors.TextEdit();
            this.txtBatchNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtBeratJenisAktual = new DevExpress.XtraEditors.TextEdit();
            this.txtBeratJenisStandard = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtQty = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.cmbKesimpulan = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtDOCISO = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtLamaQC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtWaktuSelesai = new DevExpress.XtraEditors.TimeEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtWaktuTerima = new DevExpress.XtraEditors.TimeEdit();
            this.cmbUoM = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpired.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpired.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPemusnahan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenampilanStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenampilanAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBauStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBauAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSolidContent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSolidContentStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCOA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKelarutanAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalMasuk.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalMasuk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKelarutanStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPHAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPHStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeratJenisAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeratJenisStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKesimpulan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOCISO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaQC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuSelesai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuTerima.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUoM.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbExpired
            // 
            this.cmbExpired.EditValue = null;
            this.cmbExpired.Location = new System.Drawing.Point(152, 281);
            this.cmbExpired.Name = "cmbExpired";
            this.cmbExpired.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbExpired.Properties.Appearance.Options.UseFont = true;
            this.cmbExpired.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbExpired.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbExpired.Size = new System.Drawing.Size(150, 24);
            this.cmbExpired.TabIndex = 19;
            // 
            // txtPemusnahan
            // 
            this.txtPemusnahan.Location = new System.Drawing.Point(152, 341);
            this.txtPemusnahan.Name = "txtPemusnahan";
            this.txtPemusnahan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPemusnahan.Properties.Appearance.Options.UseFont = true;
            this.txtPemusnahan.Size = new System.Drawing.Size(271, 24);
            this.txtPemusnahan.TabIndex = 23;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl23.Location = new System.Drawing.Point(111, 374);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(35, 17);
            this.labelControl23.TabIndex = 24;
            this.labelControl23.Text = "Status";
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(696, 451);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(802, 451);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl25.Location = new System.Drawing.Point(102, 284);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(44, 17);
            this.labelControl25.TabIndex = 18;
            this.labelControl25.Text = "Expired";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txtPenampilanStandard);
            this.panelControl3.Controls.Add(this.labelControl9);
            this.panelControl3.Controls.Add(this.labelControl10);
            this.panelControl3.Controls.Add(this.txtPenampilanAktual);
            this.panelControl3.Controls.Add(this.labelControl12);
            this.panelControl3.Controls.Add(this.labelControl11);
            this.panelControl3.Controls.Add(this.txtBauStandard);
            this.panelControl3.Controls.Add(this.txtBauAktual);
            this.panelControl3.Location = new System.Drawing.Point(12, 308);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(427, 137);
            this.panelControl3.TabIndex = 1;
            // 
            // txtPenampilanStandard
            // 
            this.txtPenampilanStandard.Location = new System.Drawing.Point(143, 12);
            this.txtPenampilanStandard.Name = "txtPenampilanStandard";
            this.txtPenampilanStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPenampilanStandard.Properties.Appearance.Options.UseFont = true;
            this.txtPenampilanStandard.Size = new System.Drawing.Size(271, 24);
            this.txtPenampilanStandard.TabIndex = 1;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(13, 15);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(124, 17);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "Penampilan Standard";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(31, 45);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(106, 17);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "Penampilan Aktual";
            // 
            // txtPenampilanAktual
            // 
            this.txtPenampilanAktual.Location = new System.Drawing.Point(143, 42);
            this.txtPenampilanAktual.Name = "txtPenampilanAktual";
            this.txtPenampilanAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPenampilanAktual.Properties.Appearance.Options.UseFont = true;
            this.txtPenampilanAktual.Size = new System.Drawing.Size(271, 24);
            this.txtPenampilanAktual.TabIndex = 3;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(59, 75);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(78, 17);
            this.labelControl12.TabIndex = 4;
            this.labelControl12.Text = "Bau Standard";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(77, 105);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(60, 17);
            this.labelControl11.TabIndex = 6;
            this.labelControl11.Text = "Bau Aktual";
            // 
            // txtBauStandard
            // 
            this.txtBauStandard.Location = new System.Drawing.Point(143, 72);
            this.txtBauStandard.Name = "txtBauStandard";
            this.txtBauStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBauStandard.Properties.Appearance.Options.UseFont = true;
            this.txtBauStandard.Size = new System.Drawing.Size(271, 24);
            this.txtBauStandard.TabIndex = 5;
            // 
            // txtBauAktual
            // 
            this.txtBauAktual.Location = new System.Drawing.Point(143, 102);
            this.txtBauAktual.Name = "txtBauAktual";
            this.txtBauAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBauAktual.Properties.Appearance.Options.UseFont = true;
            this.txtBauAktual.Size = new System.Drawing.Size(271, 24);
            this.txtBauAktual.TabIndex = 7;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl24.Location = new System.Drawing.Point(73, 344);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(73, 17);
            this.labelControl24.TabIndex = 22;
            this.labelControl24.Text = "Pemusnahan";
            // 
            // txtSolidContent
            // 
            this.txtSolidContent.Location = new System.Drawing.Point(152, 221);
            this.txtSolidContent.Name = "txtSolidContent";
            this.txtSolidContent.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSolidContent.Properties.Appearance.Options.UseFont = true;
            this.txtSolidContent.Size = new System.Drawing.Size(271, 24);
            this.txtSolidContent.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(30, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tgl. Masuk";
            // 
            // txtSolidContentStandard
            // 
            this.txtSolidContentStandard.Location = new System.Drawing.Point(152, 191);
            this.txtSolidContentStandard.Name = "txtSolidContentStandard";
            this.txtSolidContentStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSolidContentStandard.Properties.Appearance.Options.UseFont = true;
            this.txtSolidContentStandard.Size = new System.Drawing.Size(271, 24);
            this.txtSolidContentStandard.TabIndex = 13;
            // 
            // cmbProduct
            // 
            this.cmbProduct.EditValue = "";
            this.cmbProduct.Location = new System.Drawing.Point(99, 43);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbProduct.Properties.Appearance.Options.UseFont = true;
            this.cmbProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbProduct.Properties.DisplayMember = "Name";
            this.cmbProduct.Properties.NullText = "";
            this.cmbProduct.Properties.ValueMember = "id";
            this.cmbProduct.Properties.View = this.gridView2;
            this.cmbProduct.Size = new System.Drawing.Size(315, 24);
            this.cmbProduct.TabIndex = 3;
            this.cmbProduct.EditValueChanged += new System.EventHandler(this.cmbProduct_EditValueChanged);
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 264;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(120, 314);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(26, 17);
            this.labelControl21.TabIndex = 20;
            this.labelControl21.Text = "COA";
            // 
            // txtCOA
            // 
            this.txtCOA.Location = new System.Drawing.Point(152, 311);
            this.txtCOA.Name = "txtCOA";
            this.txtCOA.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCOA.Properties.Appearance.Options.UseFont = true;
            this.txtCOA.Size = new System.Drawing.Size(271, 24);
            this.txtCOA.TabIndex = 21;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(40, 76);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(56, 17);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Batch No.";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Location = new System.Drawing.Point(68, 224);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(78, 17);
            this.labelControl19.TabIndex = 14;
            this.labelControl19.Text = "Solid Content";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(11, 194);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(135, 17);
            this.labelControl20.TabIndex = 12;
            this.labelControl20.Text = "Solid Content Standard";
            // 
            // txtKelarutanAktual
            // 
            this.txtKelarutanAktual.Location = new System.Drawing.Point(152, 161);
            this.txtKelarutanAktual.Name = "txtKelarutanAktual";
            this.txtKelarutanAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKelarutanAktual.Properties.Appearance.Options.UseFont = true;
            this.txtKelarutanAktual.Size = new System.Drawing.Size(271, 24);
            this.txtKelarutanAktual.TabIndex = 11;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(48, 46);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(45, 17);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Product";
            // 
            // cmbTanggalMasuk
            // 
            this.cmbTanggalMasuk.EditValue = null;
            this.cmbTanggalMasuk.Location = new System.Drawing.Point(99, 13);
            this.cmbTanggalMasuk.Name = "cmbTanggalMasuk";
            this.cmbTanggalMasuk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalMasuk.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalMasuk.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalMasuk.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalMasuk.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalMasuk.TabIndex = 1;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl22.Location = new System.Drawing.Point(79, 254);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(67, 17);
            this.labelControl22.TabIndex = 16;
            this.labelControl22.Text = "Kesimpulan";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSupplier.EditValue = "";
            this.cmbSupplier.Location = new System.Drawing.Point(99, 103);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbSupplier.Properties.Appearance.Options.UseFont = true;
            this.cmbSupplier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSupplier.Properties.DisplayMember = "Name";
            this.cmbSupplier.Properties.NullText = "";
            this.cmbSupplier.Properties.ValueMember = "id";
            this.cmbSupplier.Properties.View = this.gridView1;
            this.cmbSupplier.Size = new System.Drawing.Size(315, 24);
            this.cmbSupplier.TabIndex = 7;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 23;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "id";
            this.gridColumn7.FieldName = "id";
            this.gridColumn7.Name = "gridColumn7";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Code";
            this.gridColumn8.FieldName = "Code";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 120;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Name";
            this.gridColumn9.FieldName = "Name";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 264;
            // 
            // txtKelarutanStandard
            // 
            this.txtKelarutanStandard.Location = new System.Drawing.Point(152, 131);
            this.txtKelarutanStandard.Name = "txtKelarutanStandard";
            this.txtKelarutanStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKelarutanStandard.Properties.Appearance.Options.UseFont = true;
            this.txtKelarutanStandard.Size = new System.Drawing.Size(271, 24);
            this.txtKelarutanStandard.TabIndex = 9;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(52, 164);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(94, 17);
            this.labelControl17.TabIndex = 10;
            this.labelControl17.Text = "Kelarutan Aktual";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(34, 134);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(112, 17);
            this.labelControl18.TabIndex = 8;
            this.labelControl18.Text = "Kelarutan Standard";
            // 
            // txtPHAktual
            // 
            this.txtPHAktual.Location = new System.Drawing.Point(152, 101);
            this.txtPHAktual.Name = "txtPHAktual";
            this.txtPHAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPHAktual.Properties.Appearance.Options.UseFont = true;
            this.txtPHAktual.Size = new System.Drawing.Size(271, 24);
            this.txtPHAktual.TabIndex = 7;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Location = new System.Drawing.Point(90, 104);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(56, 17);
            this.labelControl15.TabIndex = 6;
            this.labelControl15.Text = "pH Aktual";
            // 
            // txtPHStandard
            // 
            this.txtPHStandard.Location = new System.Drawing.Point(152, 71);
            this.txtPHStandard.Name = "txtPHStandard";
            this.txtPHStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPHStandard.Properties.Appearance.Options.UseFont = true;
            this.txtPHStandard.Size = new System.Drawing.Size(271, 24);
            this.txtPHStandard.TabIndex = 5;
            // 
            // txtBatchNo
            // 
            this.txtBatchNo.Location = new System.Drawing.Point(99, 73);
            this.txtBatchNo.Name = "txtBatchNo";
            this.txtBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBatchNo.Properties.Appearance.Options.UseFont = true;
            this.txtBatchNo.Size = new System.Drawing.Size(315, 24);
            this.txtBatchNo.TabIndex = 5;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(72, 74);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(74, 17);
            this.labelControl16.TabIndex = 4;
            this.labelControl16.Text = "pH Standard";
            // 
            // txtBeratJenisAktual
            // 
            this.txtBeratJenisAktual.Location = new System.Drawing.Point(152, 41);
            this.txtBeratJenisAktual.Name = "txtBeratJenisAktual";
            this.txtBeratJenisAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBeratJenisAktual.Properties.Appearance.Options.UseFont = true;
            this.txtBeratJenisAktual.Size = new System.Drawing.Size(271, 24);
            this.txtBeratJenisAktual.TabIndex = 3;
            // 
            // txtBeratJenisStandard
            // 
            this.txtBeratJenisStandard.Location = new System.Drawing.Point(152, 11);
            this.txtBeratJenisStandard.Name = "txtBeratJenisStandard";
            this.txtBeratJenisStandard.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBeratJenisStandard.Properties.Appearance.Options.UseFont = true;
            this.txtBeratJenisStandard.Size = new System.Drawing.Size(271, 24);
            this.txtBeratJenisStandard.TabIndex = 1;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(45, 44);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(101, 17);
            this.labelControl13.TabIndex = 2;
            this.labelControl13.Text = "Berat Jenis Aktual";
            // 
            // txtQty
            // 
            this.txtQty.EditValue = "";
            this.txtQty.Location = new System.Drawing.Point(99, 133);
            this.txtQty.Name = "txtQty";
            this.txtQty.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQty.Properties.Appearance.Options.UseFont = true;
            this.txtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.txtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtQty.Properties.DisplayFormat.FormatString = "n2";
            this.txtQty.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQty.Properties.EditFormat.FormatString = "n2";
            this.txtQty.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtQty.Properties.Mask.EditMask = "n2";
            this.txtQty.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtQty.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtQty.Size = new System.Drawing.Size(60, 24);
            this.txtQty.TabIndex = 9;
            // 
            // labelControl26
            // 
            this.labelControl26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl26.Location = new System.Drawing.Point(45, 106);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(48, 17);
            this.labelControl26.TabIndex = 6;
            this.labelControl26.Text = "Supplier";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.cmbExpired);
            this.panelControl2.Controls.Add(this.txtPemusnahan);
            this.panelControl2.Controls.Add(this.labelControl23);
            this.panelControl2.Controls.Add(this.labelControl25);
            this.panelControl2.Controls.Add(this.labelControl24);
            this.panelControl2.Controls.Add(this.txtSolidContent);
            this.panelControl2.Controls.Add(this.txtSolidContentStandard);
            this.panelControl2.Controls.Add(this.labelControl21);
            this.panelControl2.Controls.Add(this.txtCOA);
            this.panelControl2.Controls.Add(this.labelControl19);
            this.panelControl2.Controls.Add(this.labelControl20);
            this.panelControl2.Controls.Add(this.txtKelarutanAktual);
            this.panelControl2.Controls.Add(this.labelControl22);
            this.panelControl2.Controls.Add(this.txtKelarutanStandard);
            this.panelControl2.Controls.Add(this.labelControl17);
            this.panelControl2.Controls.Add(this.labelControl18);
            this.panelControl2.Controls.Add(this.txtPHAktual);
            this.panelControl2.Controls.Add(this.txtPHStandard);
            this.panelControl2.Controls.Add(this.labelControl15);
            this.panelControl2.Controls.Add(this.labelControl16);
            this.panelControl2.Controls.Add(this.txtBeratJenisAktual);
            this.panelControl2.Controls.Add(this.txtBeratJenisStandard);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.labelControl14);
            this.panelControl2.Controls.Add(this.cmbKesimpulan);
            this.panelControl2.Controls.Add(this.cmbStatus);
            this.panelControl2.Location = new System.Drawing.Point(445, 14);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(437, 431);
            this.panelControl2.TabIndex = 2;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(27, 14);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(119, 17);
            this.labelControl14.TabIndex = 0;
            this.labelControl14.Text = "Berat Jenis Standard";
            // 
            // cmbKesimpulan
            // 
            this.cmbKesimpulan.Location = new System.Drawing.Point(152, 251);
            this.cmbKesimpulan.Name = "cmbKesimpulan";
            this.cmbKesimpulan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbKesimpulan.Properties.Appearance.Options.UseFont = true;
            this.cmbKesimpulan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbKesimpulan.Size = new System.Drawing.Size(271, 24);
            this.cmbKesimpulan.TabIndex = 17;
            // 
            // cmbStatus
            // 
            this.cmbStatus.Location = new System.Drawing.Point(152, 371);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Size = new System.Drawing.Size(271, 24);
            this.cmbStatus.TabIndex = 25;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.cmbSupplier);
            this.panelControl1.Controls.Add(this.labelControl26);
            this.panelControl1.Controls.Add(this.txtDOCISO);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtLamaQC);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtWaktuSelesai);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtWaktuTerima);
            this.panelControl1.Controls.Add(this.cmbUoM);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtQty);
            this.panelControl1.Controls.Add(this.txtBatchNo);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.cmbProduct);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.cmbTanggalMasuk);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(427, 290);
            this.panelControl1.TabIndex = 0;
            // 
            // txtDOCISO
            // 
            this.txtDOCISO.Location = new System.Drawing.Point(99, 254);
            this.txtDOCISO.Name = "txtDOCISO";
            this.txtDOCISO.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDOCISO.Properties.Appearance.Options.UseFont = true;
            this.txtDOCISO.Size = new System.Drawing.Size(315, 24);
            this.txtDOCISO.TabIndex = 18;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(42, 257);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(51, 17);
            this.labelControl8.TabIndex = 17;
            this.labelControl8.Text = "DOC ISO";
            // 
            // txtLamaQC
            // 
            this.txtLamaQC.Location = new System.Drawing.Point(99, 224);
            this.txtLamaQC.Name = "txtLamaQC";
            this.txtLamaQC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLamaQC.Properties.Appearance.Options.UseFont = true;
            this.txtLamaQC.Size = new System.Drawing.Size(166, 24);
            this.txtLamaQC.TabIndex = 16;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(40, 227);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(53, 17);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Lama QC";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(13, 197);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(80, 17);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "Waktu Selesai";
            // 
            // txtWaktuSelesai
            // 
            this.txtWaktuSelesai.EditValue = new System.DateTime(2015, 5, 20, 0, 0, 0, 0);
            this.txtWaktuSelesai.Location = new System.Drawing.Point(99, 194);
            this.txtWaktuSelesai.Name = "txtWaktuSelesai";
            this.txtWaktuSelesai.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWaktuSelesai.Properties.Appearance.Options.UseFont = true;
            this.txtWaktuSelesai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtWaktuSelesai.Properties.DisplayFormat.FormatString = "HH:mm";
            this.txtWaktuSelesai.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtWaktuSelesai.Properties.EditFormat.FormatString = "HH:mm";
            this.txtWaktuSelesai.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtWaktuSelesai.Properties.Mask.EditMask = "t";
            this.txtWaktuSelesai.Size = new System.Drawing.Size(90, 24);
            this.txtWaktuSelesai.TabIndex = 14;
            this.txtWaktuSelesai.EditValueChanged += new System.EventHandler(this.txtWaktuSelesai_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(13, 167);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(80, 17);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "Waktu Terima";
            // 
            // txtWaktuTerima
            // 
            this.txtWaktuTerima.EditValue = new System.DateTime(2015, 5, 20, 0, 0, 0, 0);
            this.txtWaktuTerima.Location = new System.Drawing.Point(99, 164);
            this.txtWaktuTerima.Name = "txtWaktuTerima";
            this.txtWaktuTerima.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWaktuTerima.Properties.Appearance.Options.UseFont = true;
            this.txtWaktuTerima.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtWaktuTerima.Properties.DisplayFormat.FormatString = "HH:mm";
            this.txtWaktuTerima.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtWaktuTerima.Properties.EditFormat.FormatString = "HH:mm";
            this.txtWaktuTerima.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtWaktuTerima.Properties.Mask.EditMask = "t";
            this.txtWaktuTerima.Size = new System.Drawing.Size(90, 24);
            this.txtWaktuTerima.TabIndex = 12;
            // 
            // cmbUoM
            // 
            this.cmbUoM.Location = new System.Drawing.Point(165, 133);
            this.cmbUoM.Name = "cmbUoM";
            this.cmbUoM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbUoM.Properties.Appearance.Options.UseFont = true;
            this.cmbUoM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUoM.Size = new System.Drawing.Size(100, 24);
            this.cmbUoM.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(11, 136);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(82, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Jumlah Masuk";
            // 
            // frmEditQCBahanBaku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 492);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditQCBahanBaku";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit QC Bahan Baku";
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpired.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExpired.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPemusnahan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenampilanStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenampilanAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBauStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBauAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSolidContent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSolidContentStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCOA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKelarutanAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalMasuk.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalMasuk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKelarutanStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPHAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPHStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeratJenisAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeratJenisStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbKesimpulan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDOCISO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaQC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuSelesai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuTerima.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUoM.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit cmbExpired;
        private DevExpress.XtraEditors.TextEdit txtPemusnahan;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txtPenampilanStandard;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtPenampilanAktual;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtBauStandard;
        private DevExpress.XtraEditors.TextEdit txtBauAktual;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.TextEdit txtSolidContent;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSolidContentStandard;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtCOA;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtKelarutanAktual;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.DateEdit cmbTanggalMasuk;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbSupplier;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit txtKelarutanStandard;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtPHAktual;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtPHStandard;
        private DevExpress.XtraEditors.TextEdit txtBatchNo;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtBeratJenisAktual;
        private DevExpress.XtraEditors.TextEdit txtBeratJenisStandard;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtQty;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtDOCISO;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtLamaQC;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TimeEdit txtWaktuSelesai;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TimeEdit txtWaktuTerima;
        private DevExpress.XtraEditors.ComboBoxEdit cmbUoM;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit cmbKesimpulan;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        public DevExpress.XtraEditors.SimpleButton btnSave;
    }
}