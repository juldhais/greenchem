﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmQCBarangJadi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmQCBarangJadi()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New QC Barang Jadi");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit QC Barang Jadi");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete QC Barang Jadi");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print QC Barang Jadi");
            btnKesimpulan.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Kesimpulan QC Barang Jadi");
            btnStatus.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Status QC Barang Jadi");

            try
            {
                if (System.IO.File.Exists("layout_qcbarangjadi.xml"))
                gridView.RestoreLayoutFromXml("layout_qcbarangjadi.xml");
            }
            catch { }

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewQCBarangJadi form = new frmNewQCBarangJadi();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditQCBarangJadi form = new frmEditQCBarangJadi(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                DialogResult = XtraMessageBox.Show("Delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    QCBarangJadi qc = db.QCBarangJadis.First(x => x.id == id);
                    db.QCBarangJadis.DeleteOnSubmit(qc);
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                gridControl.ShowPrintPreview();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.QCBarangJadis
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TanggalMasuk,
                                Product = db.Products.First(a => a.id == x.idProduct).Name,
                                x.BatchNo,
                                x.JumlahMasuk,
                                x.UoM,
                                x.Ratio,
                                x.WaktuTerimaQC,
                                x.WaktuSelesaiQC,
                                x.LamaQC,
                                x.DOCISO,
                                //x.PenampilanStandard,
                                x.PenampilanAktual,
                                x.BauStandard,
                                x.BauAktual,
                                x.BeratJenisStandard,
                                x.BeratJenisAktual,
                                x.pHStandard,
                                x.phAktual,
                                x.KelarutanStandard,
                                x.KelarutanAktual,
                                x.SolidContentStandard,
                                //x.SolidContent,
                                x.Kesimpulan,
                                x.Expired,
                                x.COA,
                                x.Pemusnahan,
                                x.Status,
                                x.Remarks
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnKesimpulan_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmKesimpulanQC form = new frmKesimpulanQC();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmStatusQC form = new frmStatusQC();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditQCBarangJadi form = new frmEditQCBarangJadi(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.btnSave.Visible = false;
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_qcbarangjadi.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}