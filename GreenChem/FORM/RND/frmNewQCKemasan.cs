﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewQCKemasan : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewQCKemasan()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbProduct.Properties.DataSource = db.Products.GetName();
            cmbTanggalTerima.DateTime = DateTime.Now;
            foreach (var item in db.KesimpulanQCs)
                cmbKesimpulan.Properties.Items.Add(item.Name);
        }

        private void txtMulaiQC_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                TimeSpan lamaqc = txtSelesaiQC.Time - txtMulaiQC.Time;
                txtLamaQC.Text = string.Empty;
                if (lamaqc.Hours != 0) txtLamaQC.Text = lamaqc.Hours + " jam  ";
                if (lamaqc.Minutes != 0) txtLamaQC.Text += lamaqc.Minutes + " menit";
            }
            catch { }
        }

        private void cmbProduct_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                var product = db.Products.First(x => x.id == cmbProduct.EditValue.ToInteger());
                cmbUoM.Properties.Items.Clear();
                if (product.UoM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.UoM);
                if (product.SecondUoM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.SecondUoM);
                if (product.ThirdUOM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.ThirdUOM);
                cmbUoM.Text = product.UoM;
            }
            catch { }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                QCKemasan qc = new QCKemasan();
                qc.TanggalTerima = cmbTanggalTerima.DateTime;
                qc.WaktuMulaiQC = (DateTime?)txtMulaiQC.EditValue;
                qc.WaktuSelesaiQC = (DateTime?)txtSelesaiQC.EditValue;
                qc.LamaQC = txtLamaQC.Text;
                qc.idProduct = (int?)cmbProduct.EditValue;
                qc.Quantity = txtQty.EditValue.ToDecimal();
                qc.UoM = cmbUoM.Text;

                var product = db.Products.First(x => x.id == qc.idProduct);
                if (qc.UoM == product.UoM) qc.Ratio = 1;
                else if (qc.UoM == product.SecondUoM) qc.Ratio = product.Ratio.ToDecimal();
                else if (qc.UoM == product.ThirdUOM) qc.Ratio = product.Ratio.ToDecimal();
                else qc.Ratio = 1;

                qc.DOCISO = txtDOCISO.Text;

                qc.PanjangStandard = txtPanjangStandard.Text;
                qc.PanjangAktual = txtPanjangAktual.Text;
                qc.LebarStandard = txtLebarStandard.Text;
                qc.LebarAktual = txtLebarAktual.Text;
                qc.TinggiStandard = txtTinggiStandard.Text;
                qc.TinggiAktual = txtTinggiAktual.Text;
                qc.DiameterStandard = txtDiameterStandard.Text;
                qc.DiameterAktual = txtDiameterAktual.Text;
                qc.FisikStandard = txtFisikStandard.Text;
                qc.FisikAktual = txtFisikAktual.Text;
                qc.FisikDalamStandard = txtFisikDalamStandard.Text;
                qc.FisikDalamAktual = txtFisikDalamAktual.Text;
                qc.FisikTutupStandard = txtFisikTutupStandard.Text;
                qc.FisikTutupAktual = txtFisikTutupAktual.Text;
                qc.DiamterTutupStandard = txtDiameterTutupStandard.Text;
                qc.DiameterTutupAktual = txtDiameterTutupAktual.Text;
                qc.WarnaTutupStandard = txtWarnaTutupStandard.Text;
                qc.WarnaTutupAktual = txtWarnaTutupAktual.Text;
                qc.WarnaKemasanStandard = txtWarnaKemasanStandard.Text;
                qc.WarnaKemasanAktual = txtWarnaKemasanAktual.Text;
                qc.KranStandard = txtKranStandard.Text;
                qc.KranAktual = txtKranAktual.Text;
                qc.WarnaProduk = txtWarnaProduk.Text;
                qc.Kesimpulan = cmbKesimpulan.Text;
                qc.Keterangan = txtKeterangan.Text;

                db.QCKemasans.InsertOnSubmit(qc);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}