﻿namespace GreenChem.MenuTransaction
{
    partial class frmQCKemasan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTanggalTerima = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colidProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUoM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaktuMulaiQC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWaktuSelesaiQC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLamaQC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDOCISO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPanjangStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPanjangAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLebarStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLebarAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTinggiStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTinggiAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiameterStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiameterAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFisikStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFisikAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFisikDalamStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFisikDalamAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFisikTutupStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFisikTutupAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiamterTutupStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiameterTutupAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarnaTutupStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarnaTutupAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarnaKemasanStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarnaKemasanAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKranStandard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKranAktual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarnaProduk = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKesimpulan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeterangan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnView = new DevExpress.XtraEditors.SimpleButton();
            this.btnKesimpulan = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.lblRefresh = new DevExpress.XtraEditors.LabelControl();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 40);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(984, 522);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colTanggalTerima,
            this.colidProduct,
            this.colQuantity,
            this.colUoM,
            this.colRatio,
            this.colWaktuMulaiQC,
            this.colWaktuSelesaiQC,
            this.colLamaQC,
            this.colDOCISO,
            this.colPanjangStandard,
            this.colPanjangAktual,
            this.colLebarStandard,
            this.colLebarAktual,
            this.colTinggiStandard,
            this.colTinggiAktual,
            this.colDiameterStandard,
            this.colDiameterAktual,
            this.colFisikStandard,
            this.colFisikAktual,
            this.colFisikDalamStandard,
            this.colFisikDalamAktual,
            this.colFisikTutupStandard,
            this.colFisikTutupAktual,
            this.colDiamterTutupStandard,
            this.colDiameterTutupAktual,
            this.colWarnaTutupStandard,
            this.colWarnaTutupAktual,
            this.colWarnaKemasanStandard,
            this.colWarnaKemasanAktual,
            this.colKranStandard,
            this.colKranAktual,
            this.colWarnaProduk,
            this.colKesimpulan,
            this.colKeterangan});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.RowHeight = 23;
            this.gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridView_KeyDown);
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colTanggalTerima
            // 
            this.colTanggalTerima.FieldName = "TanggalTerima";
            this.colTanggalTerima.Name = "colTanggalTerima";
            this.colTanggalTerima.Visible = true;
            this.colTanggalTerima.VisibleIndex = 0;
            this.colTanggalTerima.Width = 120;
            // 
            // colidProduct
            // 
            this.colidProduct.Caption = "Product";
            this.colidProduct.FieldName = "Product";
            this.colidProduct.Name = "colidProduct";
            this.colidProduct.Visible = true;
            this.colidProduct.VisibleIndex = 1;
            this.colidProduct.Width = 200;
            // 
            // colQuantity
            // 
            this.colQuantity.DisplayFormat.FormatString = "#,#0.####";
            this.colQuantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 2;
            this.colQuantity.Width = 90;
            // 
            // colUoM
            // 
            this.colUoM.Caption = "UoM";
            this.colUoM.FieldName = "UoM";
            this.colUoM.Name = "colUoM";
            this.colUoM.Visible = true;
            this.colUoM.VisibleIndex = 3;
            this.colUoM.Width = 80;
            // 
            // colRatio
            // 
            this.colRatio.FieldName = "Ratio";
            this.colRatio.Name = "colRatio";
            this.colRatio.Width = 80;
            // 
            // colWaktuMulaiQC
            // 
            this.colWaktuMulaiQC.FieldName = "WaktuMulaiQC";
            this.colWaktuMulaiQC.Name = "colWaktuMulaiQC";
            this.colWaktuMulaiQC.Visible = true;
            this.colWaktuMulaiQC.VisibleIndex = 4;
            this.colWaktuMulaiQC.Width = 150;
            // 
            // colWaktuSelesaiQC
            // 
            this.colWaktuSelesaiQC.FieldName = "WaktuSelesaiQC";
            this.colWaktuSelesaiQC.Name = "colWaktuSelesaiQC";
            this.colWaktuSelesaiQC.Visible = true;
            this.colWaktuSelesaiQC.VisibleIndex = 5;
            this.colWaktuSelesaiQC.Width = 150;
            // 
            // colLamaQC
            // 
            this.colLamaQC.FieldName = "LamaQC";
            this.colLamaQC.Name = "colLamaQC";
            this.colLamaQC.Visible = true;
            this.colLamaQC.VisibleIndex = 6;
            this.colLamaQC.Width = 150;
            // 
            // colDOCISO
            // 
            this.colDOCISO.FieldName = "DOCISO";
            this.colDOCISO.Name = "colDOCISO";
            this.colDOCISO.Visible = true;
            this.colDOCISO.VisibleIndex = 7;
            this.colDOCISO.Width = 150;
            // 
            // colPanjangStandard
            // 
            this.colPanjangStandard.FieldName = "PanjangStandard";
            this.colPanjangStandard.Name = "colPanjangStandard";
            this.colPanjangStandard.Visible = true;
            this.colPanjangStandard.VisibleIndex = 8;
            this.colPanjangStandard.Width = 150;
            // 
            // colPanjangAktual
            // 
            this.colPanjangAktual.FieldName = "PanjangAktual";
            this.colPanjangAktual.Name = "colPanjangAktual";
            this.colPanjangAktual.Visible = true;
            this.colPanjangAktual.VisibleIndex = 9;
            this.colPanjangAktual.Width = 150;
            // 
            // colLebarStandard
            // 
            this.colLebarStandard.FieldName = "LebarStandard";
            this.colLebarStandard.Name = "colLebarStandard";
            this.colLebarStandard.Visible = true;
            this.colLebarStandard.VisibleIndex = 10;
            this.colLebarStandard.Width = 150;
            // 
            // colLebarAktual
            // 
            this.colLebarAktual.FieldName = "LebarAktual";
            this.colLebarAktual.Name = "colLebarAktual";
            this.colLebarAktual.Visible = true;
            this.colLebarAktual.VisibleIndex = 11;
            this.colLebarAktual.Width = 150;
            // 
            // colTinggiStandard
            // 
            this.colTinggiStandard.FieldName = "TinggiStandard";
            this.colTinggiStandard.Name = "colTinggiStandard";
            this.colTinggiStandard.Visible = true;
            this.colTinggiStandard.VisibleIndex = 12;
            this.colTinggiStandard.Width = 150;
            // 
            // colTinggiAktual
            // 
            this.colTinggiAktual.FieldName = "TinggiAktual";
            this.colTinggiAktual.Name = "colTinggiAktual";
            this.colTinggiAktual.Visible = true;
            this.colTinggiAktual.VisibleIndex = 13;
            this.colTinggiAktual.Width = 150;
            // 
            // colDiameterStandard
            // 
            this.colDiameterStandard.FieldName = "DiameterStandard";
            this.colDiameterStandard.Name = "colDiameterStandard";
            this.colDiameterStandard.Visible = true;
            this.colDiameterStandard.VisibleIndex = 14;
            this.colDiameterStandard.Width = 150;
            // 
            // colDiameterAktual
            // 
            this.colDiameterAktual.FieldName = "DiameterAktual";
            this.colDiameterAktual.Name = "colDiameterAktual";
            this.colDiameterAktual.Visible = true;
            this.colDiameterAktual.VisibleIndex = 15;
            this.colDiameterAktual.Width = 150;
            // 
            // colFisikStandard
            // 
            this.colFisikStandard.FieldName = "FisikStandard";
            this.colFisikStandard.Name = "colFisikStandard";
            this.colFisikStandard.Visible = true;
            this.colFisikStandard.VisibleIndex = 16;
            this.colFisikStandard.Width = 150;
            // 
            // colFisikAktual
            // 
            this.colFisikAktual.FieldName = "FisikAktual";
            this.colFisikAktual.Name = "colFisikAktual";
            this.colFisikAktual.Visible = true;
            this.colFisikAktual.VisibleIndex = 17;
            this.colFisikAktual.Width = 150;
            // 
            // colFisikDalamStandard
            // 
            this.colFisikDalamStandard.FieldName = "FisikDalamStandard";
            this.colFisikDalamStandard.Name = "colFisikDalamStandard";
            this.colFisikDalamStandard.Visible = true;
            this.colFisikDalamStandard.VisibleIndex = 18;
            this.colFisikDalamStandard.Width = 150;
            // 
            // colFisikDalamAktual
            // 
            this.colFisikDalamAktual.FieldName = "FisikDalamAktual";
            this.colFisikDalamAktual.Name = "colFisikDalamAktual";
            this.colFisikDalamAktual.Visible = true;
            this.colFisikDalamAktual.VisibleIndex = 19;
            this.colFisikDalamAktual.Width = 150;
            // 
            // colFisikTutupStandard
            // 
            this.colFisikTutupStandard.FieldName = "FisikTutupStandard";
            this.colFisikTutupStandard.Name = "colFisikTutupStandard";
            this.colFisikTutupStandard.Visible = true;
            this.colFisikTutupStandard.VisibleIndex = 20;
            this.colFisikTutupStandard.Width = 150;
            // 
            // colFisikTutupAktual
            // 
            this.colFisikTutupAktual.FieldName = "FisikTutupAktual";
            this.colFisikTutupAktual.Name = "colFisikTutupAktual";
            this.colFisikTutupAktual.Visible = true;
            this.colFisikTutupAktual.VisibleIndex = 21;
            this.colFisikTutupAktual.Width = 150;
            // 
            // colDiamterTutupStandard
            // 
            this.colDiamterTutupStandard.FieldName = "DiamterTutupStandard";
            this.colDiamterTutupStandard.Name = "colDiamterTutupStandard";
            this.colDiamterTutupStandard.Visible = true;
            this.colDiamterTutupStandard.VisibleIndex = 22;
            this.colDiamterTutupStandard.Width = 150;
            // 
            // colDiameterTutupAktual
            // 
            this.colDiameterTutupAktual.FieldName = "DiameterTutupAktual";
            this.colDiameterTutupAktual.Name = "colDiameterTutupAktual";
            this.colDiameterTutupAktual.Visible = true;
            this.colDiameterTutupAktual.VisibleIndex = 23;
            this.colDiameterTutupAktual.Width = 150;
            // 
            // colWarnaTutupStandard
            // 
            this.colWarnaTutupStandard.FieldName = "WarnaTutupStandard";
            this.colWarnaTutupStandard.Name = "colWarnaTutupStandard";
            this.colWarnaTutupStandard.Visible = true;
            this.colWarnaTutupStandard.VisibleIndex = 24;
            this.colWarnaTutupStandard.Width = 150;
            // 
            // colWarnaTutupAktual
            // 
            this.colWarnaTutupAktual.FieldName = "WarnaTutupAktual";
            this.colWarnaTutupAktual.Name = "colWarnaTutupAktual";
            this.colWarnaTutupAktual.Visible = true;
            this.colWarnaTutupAktual.VisibleIndex = 25;
            this.colWarnaTutupAktual.Width = 150;
            // 
            // colWarnaKemasanStandard
            // 
            this.colWarnaKemasanStandard.FieldName = "WarnaKemasanStandard";
            this.colWarnaKemasanStandard.Name = "colWarnaKemasanStandard";
            this.colWarnaKemasanStandard.Visible = true;
            this.colWarnaKemasanStandard.VisibleIndex = 26;
            this.colWarnaKemasanStandard.Width = 150;
            // 
            // colWarnaKemasanAktual
            // 
            this.colWarnaKemasanAktual.FieldName = "WarnaKemasanAktual";
            this.colWarnaKemasanAktual.Name = "colWarnaKemasanAktual";
            this.colWarnaKemasanAktual.Visible = true;
            this.colWarnaKemasanAktual.VisibleIndex = 27;
            this.colWarnaKemasanAktual.Width = 150;
            // 
            // colKranStandard
            // 
            this.colKranStandard.FieldName = "KranStandard";
            this.colKranStandard.Name = "colKranStandard";
            this.colKranStandard.Visible = true;
            this.colKranStandard.VisibleIndex = 28;
            this.colKranStandard.Width = 150;
            // 
            // colKranAktual
            // 
            this.colKranAktual.FieldName = "KranAktual";
            this.colKranAktual.Name = "colKranAktual";
            this.colKranAktual.Visible = true;
            this.colKranAktual.VisibleIndex = 29;
            this.colKranAktual.Width = 150;
            // 
            // colWarnaProduk
            // 
            this.colWarnaProduk.FieldName = "WarnaProduk";
            this.colWarnaProduk.Name = "colWarnaProduk";
            this.colWarnaProduk.Visible = true;
            this.colWarnaProduk.VisibleIndex = 30;
            this.colWarnaProduk.Width = 150;
            // 
            // colKesimpulan
            // 
            this.colKesimpulan.FieldName = "Kesimpulan";
            this.colKesimpulan.Name = "colKesimpulan";
            this.colKesimpulan.Visible = true;
            this.colKesimpulan.VisibleIndex = 31;
            this.colKesimpulan.Width = 150;
            // 
            // colKeterangan
            // 
            this.colKeterangan.FieldName = "Keterangan";
            this.colKeterangan.Name = "colKeterangan";
            this.colKeterangan.Visible = true;
            this.colKeterangan.VisibleIndex = 32;
            this.colKeterangan.Width = 150;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnView);
            this.panelControl1.Controls.Add(this.btnKesimpulan);
            this.panelControl1.Controls.Add(this.btnPrint);
            this.panelControl1.Controls.Add(this.lblRefresh);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 40);
            this.panelControl1.TabIndex = 0;
            // 
            // btnView
            // 
            this.btnView.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnView.Appearance.Options.UseFont = true;
            this.btnView.Location = new System.Drawing.Point(535, 5);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(90, 30);
            this.btnView.TabIndex = 14;
            this.btnView.Text = "&View";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnKesimpulan
            // 
            this.btnKesimpulan.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnKesimpulan.Appearance.Options.UseFont = true;
            this.btnKesimpulan.Location = new System.Drawing.Point(404, 5);
            this.btnKesimpulan.Name = "btnKesimpulan";
            this.btnKesimpulan.Size = new System.Drawing.Size(120, 30);
            this.btnKesimpulan.TabIndex = 13;
            this.btnKesimpulan.Text = "&Kesimpulan";
            this.btnKesimpulan.Click += new System.EventHandler(this.btnKesimpulan_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.Location = new System.Drawing.Point(303, 5);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(90, 30);
            this.btnPrint.TabIndex = 12;
            this.btnPrint.Text = "&Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // lblRefresh
            // 
            this.lblRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblRefresh.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRefresh.Location = new System.Drawing.Point(931, 12);
            this.lblRefresh.Name = "lblRefresh";
            this.lblRefresh.Size = new System.Drawing.Size(41, 17);
            this.lblRefresh.TabIndex = 9;
            this.lblRefresh.Text = "refresh";
            this.lblRefresh.Click += new System.EventHandler(this.lblRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(197, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(101, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(5, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmQCKemasan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmQCKemasan";
            this.Text = "QC Kemasan";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private DevExpress.XtraEditors.LabelControl lblRefresh;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colTanggalTerima;
        private DevExpress.XtraGrid.Columns.GridColumn colidProduct;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colUoM;
        private DevExpress.XtraGrid.Columns.GridColumn colRatio;
        private DevExpress.XtraGrid.Columns.GridColumn colWaktuMulaiQC;
        private DevExpress.XtraGrid.Columns.GridColumn colWaktuSelesaiQC;
        private DevExpress.XtraGrid.Columns.GridColumn colLamaQC;
        private DevExpress.XtraGrid.Columns.GridColumn colDOCISO;
        private DevExpress.XtraGrid.Columns.GridColumn colPanjangStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colPanjangAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colLebarStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colLebarAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colTinggiStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colTinggiAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colDiameterStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colDiameterAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colFisikStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colFisikAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colFisikDalamStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colFisikDalamAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colFisikTutupStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colFisikTutupAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colDiamterTutupStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colDiameterTutupAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colWarnaTutupStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colWarnaTutupAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colWarnaKemasanStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colWarnaKemasanAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colKranStandard;
        private DevExpress.XtraGrid.Columns.GridColumn colKranAktual;
        private DevExpress.XtraGrid.Columns.GridColumn colWarnaProduk;
        private DevExpress.XtraGrid.Columns.GridColumn colKesimpulan;
        private DevExpress.XtraGrid.Columns.GridColumn colKeterangan;
        private DevExpress.XtraEditors.SimpleButton btnKesimpulan;
        private DevExpress.XtraEditors.SimpleButton btnView;
    }
}