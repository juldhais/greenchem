﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmEditPengembanganProduk : DevExpress.XtraEditors.XtraForm
    {
        int? _id;
        GreenChemDataContext db;

        public frmEditPengembanganProduk(int? id = null)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            if (_id == null) txtNoPRD.Text = db.GetNomorPengembanganProduk();
            else
            {
                var peng = db.PengembanganProduks.First(x => x.id == _id);
                txtNoPRD.Text = peng.NoPRD;
                txtJudul.Text = peng.Judul;
                cmbTanggalPermintaan.EditValue = peng.TanggalPermintaan;
                cmbTanggalApproval.EditValue = peng.TanggalApproval;
                cmbTanggalSelesai.EditValue = peng.TanggalSelesai;
                cmbDueDate.EditValue = peng.DueDate;
                txtLamaRiset.EditValue = peng.LamaRiset;
                cmbStatus.Text = peng.Status;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                PengembanganProduk peng;
                if (_id == null) peng = new PengembanganProduk();
                else peng = db.PengembanganProduks.First(x => x.id == _id);

                peng.NoPRD = txtNoPRD.Text;
                peng.Judul = txtJudul.Text;
                peng.TanggalPermintaan = cmbTanggalPermintaan.EditValue.ToNullDateTime();
                peng.TanggalApproval = cmbTanggalApproval.EditValue.ToNullDateTime();
                peng.TanggalSelesai = cmbTanggalSelesai.EditValue.ToNullDateTime(); ;
                peng.DueDate = cmbDueDate.EditValue.ToNullDateTime();
                peng.LamaRiset = txtLamaRiset.EditValue.ToInteger();
                peng.Status = cmbStatus.Text;

                if (_id == null) db.PengembanganProduks.InsertOnSubmit(peng);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}