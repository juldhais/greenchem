﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Diagnostics;
using System.IO;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewLabService : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewLabService()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbTanggalPermintaan.DateTime = db.GetServerDate();
            GenerateCode();
        }

        private void GenerateCode()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var query = from x in db.LabServices
                            where x.TanggalPermintaan.Year == cmbTanggalPermintaan.DateTime.Year
                            orderby x.TransactionNumber descending
                            select x.TransactionNumber;

                if (query.Any())
                {
                    int newnumber = query.First().Substring(0, 3).ToInteger() + 1;
                    txtTransactionNumber.Text = newnumber.ToString("000") + "/" + cmbTanggalPermintaan.DateTime.Year;
                }
                else txtTransactionNumber.Text = "001/" + cmbTanggalPermintaan.DateTime.Year;
            }
            catch
            {
                txtTransactionNumber.Text = "001/" + cmbTanggalPermintaan.DateTime.Year;
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                LabService labservice = new LabService();
                labservice.TransactionNumber = txtTransactionNumber.Text;
                labservice.SE = txtSE.Text;
                labservice.TanggalPermintaan = cmbTanggalPermintaan.DateTime;
                labservice.TanggalApproval = (DateTime?)cmbTanggalApproval.EditValue;
                labservice.TargetLama = txtTargetLama.Text;
                labservice.TanggalSelesai = (DateTime?)cmbTanggalSelesai.EditValue;
                labservice.LamaRiset = txtLamaRisetAktual.Text;
                labservice.PermintaanLabService = txtPermintaanLabService.Text;
                labservice.HasilLabService = txtHasilLabService.Text;
                labservice.Status = cmbStatus.Text;
                db.LabServices.InsertOnSubmit(labservice);
                db.SubmitChanges();

                //upload
                try
                {
                    if (txtAttachment.Text.IsNotEmpty() && !txtAttachment.Text.Contains("product_msds"))
                    {
                        string remotefile = "labservice_" + labservice.TransactionNumber + "_" + labservice.id + Path.GetExtension(txtAttachment.Text);
                        labservice.Attachment = remotefile;
                        Sync.UploadToDatabase(remotefile, txtAttachment.Text);
                    }

                    db.SubmitChanges();
                }
                catch { }

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbTanggalSelesai_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbTanggalApproval.EditValue.IsNotEmpty())
                {
                    TimeSpan durasi = cmbTanggalSelesai.DateTime.Date - cmbTanggalApproval.DateTime.Date;
                    txtLamaRisetAktual.Text = durasi.Days + " hari";
                }
                else txtLamaRisetAktual.Text = "";
            }
            catch
            {
                txtLamaRisetAktual.Text = string.Empty;
            }
        }

        private void btnAttachment_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtAttachment.Text = openFileDialog.FileName;
        }

        private void lblViewAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAttachment.Text.IsEmpty()) return;

                Cursor.Current = Cursors.WaitCursor;
                string localfile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\temp_labservice" + Path.GetExtension(txtAttachment.Text);
                //Sync.DownloadFromDatabase(txtMSDS.Text, localfile);
                Sync.DownloadFromDatabase(txtAttachment.Text, localfile);

                Process process = new Process();
                process.StartInfo.FileName = localfile;
                process.StartInfo.UseShellExecute = true;
                process.Start();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}