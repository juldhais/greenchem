﻿namespace GreenChem.MenuTransaction
{
    partial class frmNewLabService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtHasilLabService = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtPermintaanLabService = new DevExpress.XtraEditors.MemoEdit();
            this.txtLamaRisetAktual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTanggalSelesai = new DevExpress.XtraEditors.DateEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTargetLama = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTanggalApproval = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTanggalPermintaan = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtSE = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTransactionNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.lblViewAttachment = new DevExpress.XtraEditors.LabelControl();
            this.btnAttachment = new DevExpress.XtraEditors.SimpleButton();
            this.txtAttachment = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasilLabService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermintaanLabService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaRisetAktual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTargetLama.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachment.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblViewAttachment);
            this.panelControl1.Controls.Add(this.btnAttachment);
            this.panelControl1.Controls.Add(this.txtAttachment);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.cmbStatus);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.txtHasilLabService);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtPermintaanLabService);
            this.panelControl1.Controls.Add(this.txtLamaRisetAktual);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.cmbTanggalSelesai);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtTargetLama);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.cmbTanggalApproval);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.cmbTanggalPermintaan);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtSE);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtTransactionNumber);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 372);
            this.panelControl1.TabIndex = 0;
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "OK";
            this.cmbStatus.Location = new System.Drawing.Point(121, 301);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "OK",
            " "});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(150, 23);
            this.cmbStatus.TabIndex = 19;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(13, 303);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(35, 17);
            this.labelControl10.TabIndex = 18;
            this.labelControl10.Text = "Status";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(13, 250);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(102, 20);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Hasil Lab Service";
            // 
            // txtHasilLabService
            // 
            this.txtHasilLabService.Location = new System.Drawing.Point(121, 248);
            this.txtHasilLabService.Name = "txtHasilLabService";
            this.txtHasilLabService.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtHasilLabService.Properties.Appearance.Options.UseFont = true;
            this.txtHasilLabService.Size = new System.Drawing.Size(334, 47);
            this.txtHasilLabService.TabIndex = 17;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(13, 192);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(102, 41);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "Permintaan Lab Service";
            // 
            // txtPermintaanLabService
            // 
            this.txtPermintaanLabService.Location = new System.Drawing.Point(121, 195);
            this.txtPermintaanLabService.Name = "txtPermintaanLabService";
            this.txtPermintaanLabService.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPermintaanLabService.Properties.Appearance.Options.UseFont = true;
            this.txtPermintaanLabService.Size = new System.Drawing.Size(334, 47);
            this.txtPermintaanLabService.TabIndex = 15;
            // 
            // txtLamaRisetAktual
            // 
            this.txtLamaRisetAktual.Location = new System.Drawing.Point(121, 165);
            this.txtLamaRisetAktual.Name = "txtLamaRisetAktual";
            this.txtLamaRisetAktual.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtLamaRisetAktual.Properties.Appearance.Options.UseFont = true;
            this.txtLamaRisetAktual.Size = new System.Drawing.Size(334, 24);
            this.txtLamaRisetAktual.TabIndex = 13;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(13, 168);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(102, 17);
            this.labelControl7.TabIndex = 12;
            this.labelControl7.Text = "Lama Riset Aktual";
            // 
            // cmbTanggalSelesai
            // 
            this.cmbTanggalSelesai.EditValue = null;
            this.cmbTanggalSelesai.Location = new System.Drawing.Point(121, 135);
            this.cmbTanggalSelesai.Name = "cmbTanggalSelesai";
            this.cmbTanggalSelesai.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalSelesai.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalSelesai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalSelesai.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalSelesai.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalSelesai.TabIndex = 11;
            this.cmbTanggalSelesai.EditValueChanged += new System.EventHandler(this.cmbTanggalSelesai_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(13, 138);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(65, 17);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Tgl. Selesai";
            // 
            // txtTargetLama
            // 
            this.txtTargetLama.Location = new System.Drawing.Point(121, 105);
            this.txtTargetLama.Name = "txtTargetLama";
            this.txtTargetLama.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTargetLama.Properties.Appearance.Options.UseFont = true;
            this.txtTargetLama.Size = new System.Drawing.Size(334, 24);
            this.txtTargetLama.TabIndex = 9;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(13, 108);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(73, 17);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Target Lama";
            // 
            // cmbTanggalApproval
            // 
            this.cmbTanggalApproval.EditValue = null;
            this.cmbTanggalApproval.Location = new System.Drawing.Point(121, 75);
            this.cmbTanggalApproval.Name = "cmbTanggalApproval";
            this.cmbTanggalApproval.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalApproval.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalApproval.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalApproval.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalApproval.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalApproval.TabIndex = 7;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(13, 78);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(78, 17);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Tgl. Approval";
            // 
            // cmbTanggalPermintaan
            // 
            this.cmbTanggalPermintaan.EditValue = null;
            this.cmbTanggalPermintaan.Location = new System.Drawing.Point(121, 45);
            this.cmbTanggalPermintaan.Name = "cmbTanggalPermintaan";
            this.cmbTanggalPermintaan.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbTanggalPermintaan.Properties.Appearance.Options.UseFont = true;
            this.cmbTanggalPermintaan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTanggalPermintaan.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbTanggalPermintaan.Size = new System.Drawing.Size(150, 24);
            this.cmbTanggalPermintaan.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(13, 48);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 17);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Tgl. Permintaan";
            // 
            // txtSE
            // 
            this.txtSE.Location = new System.Drawing.Point(305, 15);
            this.txtSE.Name = "txtSE";
            this.txtSE.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSE.Properties.Appearance.Options.UseFont = true;
            this.txtSE.Size = new System.Drawing.Size(150, 24);
            this.txtSE.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(285, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(14, 17);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "SE";
            // 
            // txtTransactionNumber
            // 
            this.txtTransactionNumber.Location = new System.Drawing.Point(121, 15);
            this.txtTransactionNumber.Name = "txtTransactionNumber";
            this.txtTransactionNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTransactionNumber.Properties.Appearance.Options.UseFont = true;
            this.txtTransactionNumber.Size = new System.Drawing.Size(150, 24);
            this.txtTransactionNumber.TabIndex = 1;
            this.txtTransactionNumber.TabStop = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(13, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(91, 17);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "No. Lab Service";
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(296, 390);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(402, 390);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblViewAttachment
            // 
            this.lblViewAttachment.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Underline);
            this.lblViewAttachment.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.lblViewAttachment.Location = new System.Drawing.Point(428, 333);
            this.lblViewAttachment.Name = "lblViewAttachment";
            this.lblViewAttachment.Size = new System.Drawing.Size(27, 17);
            this.lblViewAttachment.TabIndex = 43;
            this.lblViewAttachment.Text = "View";
            this.lblViewAttachment.Click += new System.EventHandler(this.lblViewAttachment_Click);
            // 
            // btnAttachment
            // 
            this.btnAttachment.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnAttachment.Appearance.Options.UseFont = true;
            this.btnAttachment.Location = new System.Drawing.Point(362, 329);
            this.btnAttachment.Name = "btnAttachment";
            this.btnAttachment.Size = new System.Drawing.Size(60, 25);
            this.btnAttachment.TabIndex = 42;
            this.btnAttachment.Text = "...";
            this.btnAttachment.Click += new System.EventHandler(this.btnAttachment_Click);
            // 
            // txtAttachment
            // 
            this.txtAttachment.Location = new System.Drawing.Point(121, 330);
            this.txtAttachment.Name = "txtAttachment";
            this.txtAttachment.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAttachment.Properties.Appearance.Options.UseFont = true;
            this.txtAttachment.Size = new System.Drawing.Size(235, 24);
            this.txtAttachment.TabIndex = 41;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(13, 333);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(65, 17);
            this.labelControl18.TabIndex = 40;
            this.labelControl18.Text = "Attachment";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "All Files|*.*";
            this.openFileDialog.Title = "Open File";
            // 
            // frmNewLabService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 432);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewLabService";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Lab Service";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHasilLabService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermintaanLabService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLamaRisetAktual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalSelesai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTargetLama.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalApproval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTanggalPermintaan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAttachment.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtTransactionNumber;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtSE;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtLamaRisetAktual;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.DateEdit cmbTanggalSelesai;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtTargetLama;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.DateEdit cmbTanggalApproval;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DateEdit cmbTanggalPermintaan;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.MemoEdit txtHasilLabService;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit txtPermintaanLabService;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.LabelControl lblViewAttachment;
        private DevExpress.XtraEditors.SimpleButton btnAttachment;
        private DevExpress.XtraEditors.TextEdit txtAttachment;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}