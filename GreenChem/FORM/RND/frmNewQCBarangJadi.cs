﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewQCBarangJadi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewQCBarangJadi()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbProduct.Properties.DataSource = db.Products.GetName();
            cmbTanggalMasuk.DateTime = DateTime.Now;
            foreach (var item in db.KesimpulanQCs)
                cmbKesimpulan.Properties.Items.Add(item.Name);

            foreach (var item in db.StatusQCs)
                cmbStatus.Properties.Items.Add(item.Name);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                QCBarangJadi qc = new QCBarangJadi();

                qc.TanggalMasuk = cmbTanggalMasuk.DateTime;
                qc.idProduct = (int?)cmbProduct.EditValue;
                qc.BatchNo = txtBatchNo.Text;
                qc.JumlahMasuk = txtQty.EditValue.ToDecimal();
                qc.UoM = cmbUoM.Text;
                
                var product = db.Products.First(x => x.id == qc.idProduct);
                if (qc.UoM == product.UoM) qc.Ratio = 1;
                else if (qc.UoM == product.SecondUoM) qc.Ratio = product.Ratio.ToDecimal();
                else if (qc.UoM == product.ThirdUOM) qc.Ratio = product.Ratio.ToDecimal();
                else qc.Ratio = 1;

                qc.WaktuTerimaQC = (DateTime?)txtWaktuTerima.EditValue;
                qc.WaktuSelesaiQC = (DateTime?)txtWaktuSelesai.EditValue;
                qc.LamaQC = txtLamaQC.Text;
                qc.DOCISO = txtDOCISO.Text;
                qc.PenampilanStandard = txtPenampilanStandard.Text;
                qc.PenampilanAktual = txtPenampilanAktual.Text;
                qc.BauStandard = txtBauStandard.Text;
                qc.BauAktual = txtBauAktual.Text;
                qc.BeratJenisStandard = txtBeratJenisStandard.Text;
                qc.BeratJenisAktual = txtBeratJenisAktual.Text;
                qc.pHStandard = txtPHStandard.Text;
                qc.phAktual = txtPHAktual.Text;
                qc.KelarutanStandard = txtKelarutanStandard.Text;
                qc.KelarutanAktual = txtKelarutanAktual.Text;
                qc.SolidContentStandard = txtSolidContentStandard.Text;
                qc.SolidContent = txtSolidContent.Text;
                qc.Kesimpulan = cmbKesimpulan.Text;
                qc.Expired = (DateTime?)cmbExpired.EditValue;
                qc.COA = txtCOA.Text;
                qc.Pemusnahan = txtPemusnahan.Text;
                qc.Status = cmbStatus.Text;
                qc.Remarks = txtRemarks.Text;

                db.QCBarangJadis.InsertOnSubmit(qc);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbProduct_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();  
                var product = db.Products.First(x => x.id == cmbProduct.EditValue.ToInteger());
                cmbUoM.Properties.Items.Clear();
                if (product.UoM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.UoM);
                if (product.SecondUoM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.SecondUoM);
                if (product.ThirdUOM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.ThirdUOM);
                cmbUoM.Text = product.UoM;
            }
            catch { }
        }

        private void txtWaktuSelesai_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                TimeSpan lamaqc = txtWaktuSelesai.Time - txtWaktuTerima.Time;
                txtLamaQC.Text = string.Empty;
                if (lamaqc.Hours != 0) txtLamaQC.Text = lamaqc.Hours + " jam  ";
                if (lamaqc.Minutes != 0) txtLamaQC.Text += lamaqc.Minutes + " menit";
            }
            catch { }
        }
    }
}