﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewQCPacking : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewQCPacking()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbProduct.Properties.DataSource = db.Products.GetName();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();

            foreach (var item in db.KesimpulanQCs)
                cmbKesimpulan.Properties.Items.Add(item.Name);

            foreach (var item in db.PaletAktualQCs)
                cmbPaletAktual.Properties.Items.Add(item.Name);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                QCPacking qc = new QCPacking();
                qc.TanggalPelaksanaan = cmbTanggalPelaksanaan.DateTime;
                qc.MulaiPacking = txtMulaiPacking.Time;
                qc.SelesaiPacking = txtSelesaiPacking.Time;
                qc.LamaPacking = txtLamaPacking.Text;
                qc.idProduct = (int?)cmbProduct.EditValue;
                qc.idCustomer = (int?)cmbCustomer.EditValue;
                qc.Jumlah = txtQty.EditValue.ToDecimal();
                qc.UoM = cmbUoM.Text;

                var product = db.Products.First(x => x.id == qc.idProduct);
                if (qc.UoM == product.UoM) qc.Ratio = 1;
                else if (qc.UoM == product.SecondUoM) qc.Ratio = product.Ratio.ToDecimal();
                else if (qc.UoM == product.ThirdUOM) qc.Ratio = product.Ratio.ToDecimal();
                else qc.Ratio = 1;

                qc.BatchNo = txtBatchNo.Text;
                qc.TanggalPengiriman = (DateTime?)cmbTanggalPengiriman.EditValue;
                
                qc.PanjangStandard = txtPanjangStandard.Text;
                qc.PanjangAktual = txtPanjangAktual.Text;
                qc.LebarStandard = txtLebarStandard.Text;
                qc.LebarAktual = txtLebarAktual.Text;
                qc.TinggiStandard = txtTinggiStandard.Text;
                qc.TinggiAktual = txtTinggiAktual.Text;
                qc.KemasanStandard = txtKemasanStandard.Text;
                qc.KemasanAktual = txtKemasanAktual.Text;
                qc.LabelingMSDSStandard = txtLabellingMSDSStandard.Text;
                qc.LabelingMSDSAktual = txtLabellingMSDSAktual.Text;
                qc.AlatPendukungStandard = txtAlatPendukungStandard.Text;
                qc.AlatPendukungAktual = txtAlatPendukungAktual.Text;
                qc.PaletStandard = txtPaletStandard.Text;
                qc.PaletAktual = cmbPaletAktual.Text;
                qc.SyaratKhususStandard = txtSyaratKhususStandard.Text;
                qc.SyaratKhususAktual = txtSyaratKhususAktual.Text;
                qc.Kesimpulan = cmbKesimpulan.Text;
                qc.Keterangan = txtKeterangan.Text;
                db.QCPackings.InsertOnSubmit(qc);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtSelesaiPacking_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                TimeSpan lamapacking = txtSelesaiPacking.Time - txtMulaiPacking.Time;
                txtLamaPacking.Text = string.Empty;
                if (lamapacking.Hours != 0) txtLamaPacking.Text = lamapacking.Hours + " jam  ";
                if (lamapacking.Minutes != 0) txtLamaPacking.Text += lamapacking.Minutes + " menit";
            }
            catch { }
        }

        private void cmbProduct_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                var product = db.Products.First(x => x.id == cmbProduct.EditValue.ToInteger());
                cmbUoM.Properties.Items.Clear();
                if (product.UoM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.UoM);
                if (product.SecondUoM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.SecondUoM);
                if (product.ThirdUOM.IsNotEmpty()) cmbUoM.Properties.Items.Add(product.ThirdUOM);
                cmbUoM.Text = product.UoM;
            }
            catch { }
        }
    }
}