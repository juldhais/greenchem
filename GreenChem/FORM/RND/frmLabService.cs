﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;
using System.IO;
using GreenChem.MenuReport;

namespace GreenChem.MenuTransaction
{
    public partial class frmLabService : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmLabService()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Lab Service");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Lab Service");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Lab Service");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Lab Service");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.LabServices
                            orderby x.TransactionNumber
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.SE,
                                x.TanggalPermintaan,
                                x.TanggalApproval,
                                x.TargetLama,
                                x.TanggalSelesai,
                                x.LamaRiset,
                                x.PermintaanLabService,
                                x.HasilLabService,
                                x.Status
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewLabService form = new frmNewLabService();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditLabService form = new frmEditLabService(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                DialogResult = XtraMessageBox.Show("Delete this data " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    LabService labservice = db.LabServices.First(x => x.id == id);
                    db.LabServices.DeleteOnSubmit(labservice);
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                List<LabService> list = new List<LabService>();
                for (int i = 0; i < gridView.RowCount; i++)
                {
                    LabService labservice = new LabService();
                    labservice.TransactionNumber = gridView.GetRowCellValue(i, "TransactionNumber").Safe();
                    labservice.SE = gridView.GetRowCellValue(i, "SE").Safe();
                    labservice.TanggalPermintaan = (DateTime)gridView.GetRowCellValue(i, "TanggalPermintaan");
                    labservice.TanggalApproval = (DateTime?)gridView.GetRowCellValue(i, "TanggalApproval");
                    labservice.TargetLama = gridView.GetRowCellValue(i, "TargetLama").Safe();
                    labservice.TanggalSelesai = (DateTime?)gridView.GetRowCellValue(i, "TanggalSelesai");
                    labservice.LamaRiset = gridView.GetRowCellValue(i, "LamaRiset").Safe();
                    labservice.PermintaanLabService = gridView.GetRowCellValue(i, "PermintaanLabService").Safe();
                    labservice.HasilLabService = gridView.GetRowCellValue(i, "HasilLabService").Safe();
                    labservice.Status = gridView.GetRowCellValue(i, "Status").Safe();
                    list.Add(labservice);
                }

                rptLabService report = new rptLabService();
                if (File.Exists("LabService.repx")) report.LoadLayout("LabService.repx");
                report.DataSource = list;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}