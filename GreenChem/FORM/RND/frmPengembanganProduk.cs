﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmPengembanganProduk : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPengembanganProduk()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Pengembangan Produk");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Pengembangan Produk");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Pengembangan Produk");
            btnView.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "View Pengembangan Produk");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Pengembangan Produk");
            btnClose.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Close Pengembangan Produk");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.PengembanganProduks
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.NoPRD,
                                x.Judul,
                                x.TanggalPermintaan,
                                x.TanggalApproval,
                                x.TanggalSelesai,
                                x.DueDate,
                                x.LamaRiset,
                                x.Status
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditPengembanganProduk form = new frmEditPengembanganProduk();
                form.ShowDialog();
                RefreshData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditPengembanganProduk form = new frmEditPengembanganProduk(id);
                form.ShowDialog();
                RefreshData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string judul = gridView.GetFocusedRowCellValue("Judul").Safe();

                DialogResult = XtraMessageBox.Show("Hapus pengembangan produk ini: " + judul + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;

                    PengembanganProduk delete = db.PengembanganProduks.First(x => x.id == id);
                    db.PengembanganProduks.DeleteOnSubmit(delete);
                    db.SubmitChanges();

                    RefreshData();

                    XtraMessageBox.Show("Data berhasil dihapus.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditPengembanganProduk form = new frmEditPengembanganProduk(id);
                form.btnSave.Visible = false;
                form.ShowDialog();
                RefreshData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                gridView.ShowPrintPreview();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string judul = gridView.GetFocusedRowCellValue("Judul").Safe();

                DialogResult = XtraMessageBox.Show("Close pengembangan produk ini : " + judul + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();

                    PengembanganProduk produk = db.PengembanganProduks.First(x => x.id == id);
                    produk.Status = "Closed";
                    db.SubmitChanges();

                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}