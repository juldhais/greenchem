﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmStatusQC : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmStatusQC()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            statusQCBindingSource.DataSource = db.StatusQCs;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}