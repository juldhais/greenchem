﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmStockAdjustment : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmStockAdjustment()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                txtTransactionNumber.Text = db.GetStockAdjustmentNumber(cmbDate.DateTime.Year, cmbDate.DateTime.Month);
                cmbProduct.DataSource = db.Products.GetName();
                GetTotal();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void GetTotal()
        {
            try
            {
                var details = tempAdjustStockBindingSource.Cast<TempAdjustStock>();
                txtTotal.EditValue = details.Sum(x => x.Subtotal);
            }
            catch { }
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                db = new GreenChemDataContext();
                var product = (from x in db.Products
                              where x.id == id
                              select new
                              {
                                  x.UoM,
                                  x.Cost
                              }).First();

                gridView.SetFocusedRowCellValue("UoM", product.UoM);
                gridView.SetFocusedRowCellValue("Cost", product.Cost);
                              
            }
            else if (e.Column.FieldName == "Quantity" || e.Column.FieldName == "Cost")
            {
                decimal quantity = gridView.GetFocusedRowCellValue("Quantity").ToDecimal();
                decimal cost = gridView.GetFocusedRowCellValue("Cost").ToDecimal();
                decimal subtotal = quantity * cost;
                gridView.SetFocusedRowCellValue("Subtotal", subtotal);
                GetTotal();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tempAdjustStockBindingSource.Count == 0)
            {
                XtraMessageBox.Show("Stock adjustment can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                gridControl.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                txtTransactionNumber.Text = db.GetStockAdjustmentNumber(cmbDate.DateTime.Year, cmbDate.DateTime.Month);

                foreach (TempAdjustStock item in tempAdjustStockBindingSource)
                {
                    AdjustStock adjust = new AdjustStock();
                    adjust.TransactionNumber = txtTransactionNumber.Text;
                    adjust.TransactionDate = cmbDate.DateTime;
                    adjust.idUserAccount = frmUtama._iduseraccount;
                    adjust.idProduct = item.idProduct;
                    adjust.Quantity = item.Quantity;
                    adjust.Cost = item.Cost;
                    adjust.Subtotal = item.Subtotal;
                    adjust.Remarks = item.Remarks;
                    db.AdjustStocks.InsertOnSubmit(adjust);

                    Product product = db.Products.First(x => x.id == item.idProduct);
                    product.Stock += item.Quantity;
                }

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}