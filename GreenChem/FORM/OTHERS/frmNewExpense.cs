﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewExpense : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewExpense()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            cmbSalesman.Properties.DataSource = db.Salesmans.GetName();
            txtTransactionNumber.Text = db.GetExpenseNumber();
            cmbDate.DateTime = DateTime.Now;
            cmbDepartment.EditValue = frmUtama._iddepartment;
            cmbSubDepartment.EditValue = frmUtama._idsubdepartment;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbDepartment.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Department can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbDepartment.Focus();
                return;
            }
            if (cmbSalesman.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Employee can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbSalesman.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }
            if (txtAmount.EditValue.IsZero())
            {
                XtraMessageBox.Show("Amount can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtAmount.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                Expense expense = new Expense();
                expense.TransactionNumber = txtTransactionNumber.Text;
                expense.TransactionDate = cmbDate.DateTime;
                expense.idUserAccount = frmUtama._iduseraccount;
                if (cmbDepartment.EditValue.IsNotEmpty()) expense.idDepartment = cmbDepartment.EditValue.ToInteger();
                if (cmbSubDepartment.EditValue.IsNotEmpty()) expense.idSubDepartment = cmbSubDepartment.EditValue.ToInteger();
                if (cmbSalesman.EditValue.IsNotEmpty()) expense.idSalesman = cmbSalesman.EditValue.ToInteger();
                expense.Remarks = txtRemarks.Text;
                expense.Amount = txtAmount.EditValue.ToDecimal();
                db.Expenses.InsertOnSubmit(expense);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTransactionNumber.Text = db.GetExpenseNumber();
                cmbDate.DateTime = DateTime.Now;
                txtRemarks.Text = string.Empty;
                txtAmount.EditValue = 0;
                cmbDate.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbDate_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                txtTransactionNumber.Text = db.GetExpenseNumber();
            }
            catch { }
        }

        private void btnSaveAndExit_Click(object sender, EventArgs e)
        {
            btnSave_Click(null, null);
            Close();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}