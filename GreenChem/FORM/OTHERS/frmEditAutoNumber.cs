﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditAutoNumber : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        GreenChemDataContext db;

        public frmEditAutoNumber(int? id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            lblRefresh_Click(null, null);
            var auto = db.AutoNumbers.First(x => x.id == _id);
            txtTransactionNumber.Text = auto.TransactionNumber;
            cmbDate.DateTime = auto.TransactionDate;
            cmbSupplier.EditValue = auto.idSupplier;
            cmbStatus.Text = auto.Status;
            txtRemarks.Text = auto.Remarks;
            cmbETA.EditValue = auto.ETA;
            cmbDeliveryStatus.Text = auto.DeliveryStatus;
            cmbDateDelivered.EditValue = auto.DateDelivered;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbSupplier.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Supplier can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbSupplier.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                AutoNumber auto = db.AutoNumbers.First(x => x.id == _id);
                auto.idDepartment = frmUtama._iddepartment;
                auto.idSubDepartment = frmUtama._idsubdepartment;
                if (cmbSupplier.EditValue.IsNotEmpty()) auto.idSupplier = cmbSupplier.EditValue.ToInteger();
                txtTransactionNumber.Text = txtTransactionNumber.Text;
                auto.TransactionDate = cmbDate.DateTime;
                auto.idUserAccount = frmUtama._iduseraccount;
                auto.Remarks = txtRemarks.Text;
                auto.Status = cmbStatus.Text;
                auto.ETA = (DateTime?)cmbETA.EditValue;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            db = new GreenChemDataContext();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}