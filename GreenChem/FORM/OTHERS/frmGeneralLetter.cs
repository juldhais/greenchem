﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmGeneralLetter : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmGeneralLetter()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New External Letter");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit External Letter");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete External Letter");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.GeneralLetters
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Department =x.Department.Name,
                                UserAccount = x.UserAccount.Name,
                                x.FileName,
                                x.Remarks
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewGeneralLetter form = new frmNewGeneralLetter();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditGeneralLetter form = new frmEditGeneralLetter(id);
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    GeneralLetter general = db.GeneralLetters.First(x => x.id == id);
                    db.GeneralLetters.DeleteOnSubmit(general);
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);
                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


                Cursor.Current = Cursors.Default;
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                string folder = db.GetConfiguration("AttachmentFolder");
                string filename = folder + "//" + gridView.GetFocusedRowCellValue("FileName").Safe();
                System.Diagnostics.Process.Start(filename);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

    }
}