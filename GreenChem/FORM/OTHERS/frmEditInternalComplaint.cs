﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditInternalComplaint : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditInternalComplaint(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            var ic = db.InternalComplaints.First(x => x.id == _id);
            txtTransactionNumber.Text = ic.TransactionNumber;
            cmbDate.DateTime = ic.TransactionDate;
            txtDescription.Text = ic.Description;
            txtDeposition.Text = ic.Deposition;
            //cmbDueDate.EditValue = ic.DueDate;
            txtAction.Text = ic.Action;
            cmbClosingDate.EditValue = ic.ClosingDate;
            cmbStatus.Text = ic.Status;
            txtSuggestion.Text = ic.Suggestion;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDescription.Text.IsEmpty())
                {
                    XtraMessageBox.Show("Description can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtDescription.Focus();
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                InternalComplaint ic = db.InternalComplaints.First(x => x.id == _id);
                ic.idUserAccount = frmUtama._iduseraccount;
                ic.TransactionNumber = txtTransactionNumber.Text;
                ic.TransactionDate = cmbDate.DateTime;
                ic.Description = txtDescription.Text;
                ic.Deposition = txtDeposition.Text;
                //ic.DueDate = (DateTime?)cmbDueDate.EditValue;
                ic.Action = txtAction.Text;
                ic.ClosingDate = (DateTime?)cmbClosingDate.EditValue;
                ic.Status = cmbStatus.Text;
                ic.Suggestion = txtSuggestion.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}