﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewGeneralLetter : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewGeneralLetter()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            db = new GreenChemDataContext();
            txtTransactionNumber.Text = db.GetGeneralLetterNumber();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbDepartment.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Department can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbDepartment.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (db.GeneralLetters.Any(x => x.TransactionNumber == txtTransactionNumber.Text))
                {
                    XtraMessageBox.Show("Transaction Number is already exist. System will change the number to the newest number.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTransactionNumber.Text = db.GetGeneralLetterNumber();
                    txtTransactionNumber.Focus();
                    return;
                }

                GeneralLetter general = new GeneralLetter();
                if (cmbDepartment.EditValue.IsNotEmpty()) general.idDepartment = cmbDepartment.EditValue.ToInteger();
                general.idUserAccount = frmUtama._iduseraccount;

                general.TransactionNumber = txtTransactionNumber.Text;
                general.TransactionDate = cmbDate.DateTime;
                general.Remarks = txtRemarks.Text;
                general.FileName = txtFileName.Text;
                db.GeneralLetters.InsertOnSubmit(general);

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}