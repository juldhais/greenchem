﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditExpense : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditExpense(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            cmbSalesman.Properties.DataSource = db.Salesmans.GetName();

            var expense = db.Expenses.First(x => x.id == _id);
            txtTransactionNumber.Text = expense.TransactionNumber;
            cmbDate.DateTime = expense.TransactionDate;
            cmbDepartment.EditValue = expense.idDepartment;
            cmbSubDepartment.EditValue = expense.idSubDepartment;
            cmbSalesman.EditValue = expense.idSalesman;
            txtRemarks.Text = expense.Remarks;
            txtAmount.EditValue = expense.Amount;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbDepartment.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Departement can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbDepartment.Focus();
                return;
            }
            if (cmbSalesman.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Employee can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbSalesman.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }
            if (txtAmount.EditValue.IsZero())
            {
                XtraMessageBox.Show("Amount can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtAmount.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                Expense expense = db.Expenses.First(x => x.id == _id);
                expense.TransactionNumber = txtTransactionNumber.Text;
                expense.TransactionDate = cmbDate.DateTime;
                expense.idUserAccount = frmUtama._iduseraccount;
                if (cmbDepartment.EditValue.IsNotEmpty()) expense.idDepartment = cmbDepartment.EditValue.ToInteger();
                if (cmbSubDepartment.EditValue.IsNotEmpty()) expense.idSubDepartment = cmbSubDepartment.EditValue.ToInteger();
                if (cmbSalesman.EditValue.IsNotEmpty()) expense.idSalesman = cmbSalesman.EditValue.ToInteger();
                expense.Remarks = txtRemarks.Text;
                expense.Amount = txtAmount.EditValue.ToDecimal();
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}