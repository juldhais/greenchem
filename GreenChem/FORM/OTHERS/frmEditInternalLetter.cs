﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditInternalLetter : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditInternalLetter(int id)
        {
            InitializeComponent();

            this._id = id;

            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();

            var general = db.InternalLetters.First(x => x.id == _id);
            txtTransactionNumber.Text = general.TransactionNumber;
            cmbDate.DateTime = general.TransactionDate;
            cmbDepartment.EditValue = general.idDepartment;
            txtFileName.Text = general.FileName;
            txtRemarks.Text = general.Remarks;
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbDepartment.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Department can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbDepartment.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                InternalLetter intern = db.InternalLetters.First(x => x.id == _id);
                intern.idDepartment = (int?)cmbDepartment.EditValue;
                intern.idUserAccount = frmUtama._iduseraccount;
                intern.TransactionDate = cmbDate.DateTime;
                intern.Remarks = txtRemarks.Text;
                intern.FileName = txtFileName.Text;

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}