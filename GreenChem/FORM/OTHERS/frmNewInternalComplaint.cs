﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewInternalComplaint : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewInternalComplaint()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            txtTransactionNumber.Text = db.GetInternalComplaintNumber();
            cmbDate.DateTime = db.GetServerDate();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDescription.Text.IsEmpty())
                {
                    XtraMessageBox.Show("Description can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtDescription.Focus();
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                InternalComplaint ic = new InternalComplaint();
                ic.idUserAccount = frmUtama._iduseraccount;
                ic.TransactionNumber = txtTransactionNumber.Text;
                ic.TransactionDate = cmbDate.DateTime;
                ic.Description = txtDescription.Text;
                ic.Deposition = txtDeposition.Text;
                //ic.DueDate = (DateTime?)cmbDueDate.EditValue;
                ic.Action = txtAction.Text;
                ic.ClosingDate = (DateTime?)cmbClosingDate.EditValue;
                ic.Status = cmbStatus.Text;
                ic.Suggestion = txtSuggestion.Text;
                db.InternalComplaints.InsertOnSubmit(ic);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}