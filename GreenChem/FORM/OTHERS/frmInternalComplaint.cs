﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmInternalComplaint : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmInternalComplaint()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Today.AddYears(-1);
            cmbDateEnd.DateTime = DateTime.Today.AddYears(1);
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Internal Complaint");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Internal Complaint");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Internal Complaint");
            btnSolve.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Solve Internal Complaint");
            btnSetDeposition.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Set Deposition Internal Complaint");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.InternalComplaints
                            where x.TransactionDate.Date >= cmbDateStart.DateTime.Date && x.TransactionDate.Date <= cmbDateEnd.DateTime.Date
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                x.Description,
                                x.Deposition,
                                x.DueDate,
                                x.Action,
                                x.ClosingDate,
                                x.Status,
                                x.Suggestion
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewInternalComplaint form = new frmNewInternalComplaint();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditInternalComplaint form = new frmEditInternalComplaint(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    db.ExecuteCommand("DELETE FROM dbo.InternalComplaint WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblRefresh_Click(null, null);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string action = gridView.GetFocusedRowCellValue("Action").Safe();
                if (action.IsEmpty())
                {
                    frmSolveInternalComplaint form = new frmSolveInternalComplaint(id);
                    form.ShowDialog();
                    lblRefresh_Click(null, null);
                }
                else
                {
                    XtraMessageBox.Show("Complaint is already solved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnSetDeposition_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string deposition = gridView.GetFocusedRowCellValue("Deposition").Safe();
                if (deposition.IsEmpty())
                {
                    frmSetDepositionInternalComplaint form = new frmSetDepositionInternalComplaint(id);
                    form.ShowDialog();
                    lblRefresh_Click(null, null);
                }
                else
                {
                    XtraMessageBox.Show("Deposition is already set.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}