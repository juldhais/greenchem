﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmAutoNumber : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmAutoNumber()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New PO to Supplier");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit PO to Supplier");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete PO to Supplier");
            btnChangeDeliveryStatus.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Change Delivery Status");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.AutoNumbers
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.idDepartment,
                                x.idSubDepartment,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Supplier = x.Supplier.Name,
                                Department = x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                UserAccount = x.UserAccount.Name,
                                x.Remarks,
                                x.ETA,
                                x.DeliveryStatus,
                                x.DateDelivered
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewAutoNumber form = new frmNewAutoNumber();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditAutoNumber form = new frmEditAutoNumber(id);
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                AutoNumber auto = db.AutoNumbers.First(x => x.id == id);
                db.AutoNumbers.DeleteOnSubmit(auto);
                db.SubmitChanges();
                RefreshData();
                XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnChangeDeliveryStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string deliverystatus = gridView.GetFocusedRowCellValue("DeliveryStatus").Safe();
                if (deliverystatus != "CANCEL" && (deliverystatus != "RECEIVED" || deliverystatus != "DELIVERED"))
                {
                    frmChangeDeliveryStatus form = new frmChangeDeliveryStatus(id);
                    form.ShowDialog();
                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}