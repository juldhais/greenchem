﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmSetDepositionInternalComplaint : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmSetDepositionInternalComplaint(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            var complaint = db.InternalComplaints.First(x => x.id == _id);
            txtTransactionNumber.Text = complaint.TransactionNumber;
            txtDeposition.Text = complaint.Deposition;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                InternalComplaint ic = db.InternalComplaints.First(x => x.id == _id);
                ic.Deposition = txtDeposition.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}