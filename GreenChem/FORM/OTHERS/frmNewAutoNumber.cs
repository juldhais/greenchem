﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Linq;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewAutoNumber : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewAutoNumber()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDate.DateTime = DateTime.Now;
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            txtTransactionNumber.Text = db.GetAutoNumber();
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbSupplier.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Supplier can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbSupplier.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (db.AutoNumbers.Any(x => x.TransactionNumber == txtTransactionNumber.Text))
                {
                    XtraMessageBox.Show("Transaction Number is already exist. System will change the number to the newest number.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTransactionNumber.Text = db.GetAutoNumber();
                    return;
                }

                AutoNumber auto = new AutoNumber();
                auto.idDepartment = frmUtama._iddepartment;
                auto.idSubDepartment = frmUtama._idsubdepartment;
                if (cmbSupplier.EditValue.IsNotEmpty()) auto.idSupplier = cmbSupplier.EditValue.ToInteger();

                auto.TransactionNumber = txtTransactionNumber.Text;
                auto.TransactionDate = cmbDate.DateTime;
                auto.idUserAccount = frmUtama._iduseraccount;
                auto.Remarks = txtRemarks.Text;
                auto.Status = cmbStatus.Text;
                auto.ETA = (DateTime?)cmbETA.EditValue;
                auto.DeliveryStatus = cmbDeliveryStatus.Text;
                db.AutoNumbers.InsertOnSubmit(auto);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            txtTransactionNumber.Text = db.GetAutoNumber();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStatus.Text == "Expedisi") txtTransactionNumber.Text = db.GetPOEXNumber();
            else txtTransactionNumber.Text = db.GetAutoNumber();
        }
    }
}