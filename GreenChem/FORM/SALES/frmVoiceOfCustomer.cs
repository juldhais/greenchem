﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem
{
    public partial class frmVoiceOfCustomer : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmVoiceOfCustomer()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Now.AddDays(-365);
            cmbDateEnd.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Customer Complaint");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Customer Complaint");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Customer Complaint");
            btnSolve.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Solve Customer Complaint");
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewVoiceOfCustomer form = new frmNewVoiceOfCustomer();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditVoiceOfCustomer form = new frmEditVoiceOfCustomer(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    db.ExecuteCommand("DELETE FROM dbo.CustomerComplaint WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblRefresh_Click(null, null);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.CustomerComplaints
                            where x.TransactionDate.Date >= cmbDateStart.DateTime.Date && x.TransactionDate.Date <= cmbDateEnd.DateTime.Date
                            select new
                            {
                                x.id,
                                x.TransactionDate,
                                Customer = x.Customer.Name,
                                x.ComplaintDescription,
                                x.DepositionTo,
                                x.DueDate,
                                x.Category,
                                x.Action,
                                x.ClosingDate,
                                x.Status
                            };

                gridControl.DataSource = query;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            string status = gridView.GetFocusedRowCellValue("Status").Safe();
            if (status == "Open")
            {
                frmSolveVoiceOfCustomer form = new frmSolveVoiceOfCustomer(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            else XtraMessageBox.Show("Complaint is already solved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            Cursor.Current = Cursors.Default;
        }

        private void btnSetDeposition_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditVoiceOfCustomer form = new frmEditVoiceOfCustomer(id);
                form.txtDepositionTo.Enabled = true;
                form.cmbDate.Enabled = false;
                form.cmbCustomer.Enabled = false;
                form.txtDescription.Enabled = false;
                form.cmbDueDate.Enabled = false;
                form.txtCategory.Enabled = false;
                form.txtAction.Enabled = false;
                form.cmbClosingDate.Enabled = false;
                form.cmbStatus.Enabled = false;
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditVoiceOfCustomer form = new frmEditVoiceOfCustomer(id);
                form.btnSave.Enabled = false;
                form.ShowDialog();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}