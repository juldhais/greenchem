﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmNewProject : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewProject()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDate.DateTime = db.GetServerDate();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            cmbName.DataSource = db.UserAccounts.GetName();
            cmbProjectCategory.Properties.DataSource = from x in db.ProjectCategories
                                                       where x.Active == true
                                                       select new
                                                       {
                                                           x.id,
                                                           x.Name
                                                       };

            cmbParent.Properties.DataSource = from x in db.ProjectLists
                                              where x.TransactionDate.Date >= DateTime.Now.AddYears(-1)
                                              select new
                                              {
                                                  x.id,
                                                  x.TransactionDate,
                                                  Category = x.ProjectCategory.Name,
                                                  Customer = x.Customer.Name
                                              };

            TempId user = new TempId();
            user.id = frmUtama._iduseraccount;
            tempIdBindingSource.Add(user);
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridView.DeleteSelectedRows();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                ProjectList project = new ProjectList();
                if (cmbCustomer.EditValue.IsNotEmpty()) project.idCustomer = cmbCustomer.EditValue.ToInteger();
                project.idUserAccount = frmUtama._iduseraccount;
                if (cmbCustomer.EditValue.IsNotEmpty()) project.idProjectCategory = cmbProjectCategory.EditValue.ToInteger();
                if (cmbParent.EditValue.IsNotEmpty()) project.idParent = cmbParent.EditValue.ToNullInteger();
                project.TransactionDate = cmbDate.DateTime;
                project.Description = txtDescription.Text;
                project.Status = cmbStatus.Text;

                foreach (TempId item in tempIdBindingSource)
                {
                    ProjectUserAccount user = new ProjectUserAccount();
                    user.idUserAccount = item.id;
                    project.ProjectUserAccounts.Add(user);
                }

                db.ProjectLists.InsertOnSubmit(project);
                db.SubmitChanges();

                XtraMessageBox.Show("Project saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}