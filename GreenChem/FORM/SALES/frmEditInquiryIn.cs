﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditInquiryIn : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmEditInquiryIn(int? id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            lblRefresh_Click(null, null);

            var inquiry = db.InquiryIns.First(x => x.id == _id);
            txtTransactionNumber.Text = inquiry.TransactionNumber;
            cmbDate.DateTime = inquiry.TransactionDate;
            cmbDepartment.EditValue = inquiry.idDepartment;
            cmbSubDepartment.EditValue = inquiry.idSubDepartment;
            cmbCustomer.EditValue = inquiry.idCustomer;
            txtRemarks.Text = inquiry.Remarks;

            //details
            var details = db.DetailInquiryIns.Where(x => x.idInquiryIn == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.idProduct = item.idProduct;
                temp.Quantity = item.Quantity;
                if (item.idProduct != null) temp.UoM = db.Products.First(x => x.id == item.idProduct).UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                temp.Remarks = item.ProductName;
                listdetail.Add(temp);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                InquiryIn inquiry = db.InquiryIns.First(x => x.id == _id);
                if (cmbDepartment.EditValue.IsNotEmpty()) inquiry.idDepartment = cmbDepartment.EditValue.ToInteger();
                if (cmbSubDepartment.EditValue.IsNotEmpty()) inquiry.idSubDepartment = cmbSubDepartment.EditValue.ToInteger();
                if (cmbCustomer.EditValue.IsNotEmpty()) inquiry.idCustomer = cmbCustomer.EditValue.ToInteger();
                inquiry.TransactionNumber = txtTransactionNumber.Text;
                inquiry.TransactionDate = cmbDate.DateTime;
                inquiry.idUserAccount = frmUtama._iduseraccount;
                inquiry.Remarks = txtRemarks.Text;
                db.SubmitChanges();

                //details
                db.ExecuteCommand("DELETE FROM dbo.DetailInquiryIn WHERE idQuotationIn = " + inquiry.id);
                foreach (var item in listdetail)
                {
                    if (item.Quantity != 0)
                    {
                        DetailQuotationIn detail = new DetailQuotationIn();
                        detail.idProduct = item.idProduct;
                        detail.idQuotationIn = inquiry.id;
                        detail.Quantity = item.Quantity;
                        detail.Price = item.Price;
                        detail.Discount = item.Discount;
                        detail.Subtotal = item.Subtotal;
                        detail.ProductName = item.Remarks;
                        db.DetailQuotationIns.InsertOnSubmit(detail);
                    }
                }
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            db = new GreenChemDataContext();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailQuotationIn form = new frmDetailQuotationIn(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}