﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmQuotationOut : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmQuotationOut()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Quotation Out");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Quotation Out");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Quotation Out");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.QuotationOuts
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Customer = x.Customer.Name,
                                x.CustomerText,
                                Department = x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                x.Remarks
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewQuotationOut form = new frmNewQuotationOut();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditQuotationOut form = new frmEditQuotationOut(id);
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                QuotationOut inquiry = db.QuotationOuts.First(x => x.id == id);
                db.QuotationOuts.DeleteOnSubmit(inquiry);
                db.SubmitChanges();
                RefreshData();
                XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmViewQuotationOut form = new frmViewQuotationOut(id);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}