﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem
{
    public partial class frmNewVoiceOfCustomer : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewVoiceOfCustomer()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDate.DateTime = DateTime.Now;
            cmbCustomer.Properties.DataSource = db.Customers.GetName();

            txtDepositionTo.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Allow Edit Deposition");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtDescription.Text.IsEmpty())
            {
                XtraMessageBox.Show("Description can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDescription.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                CustomerComplaint complaint = new CustomerComplaint();
                if (cmbCustomer.EditValue.IsNotEmpty()) complaint.idCustomer = cmbCustomer.EditValue.ToInteger();
                complaint.idUserAccount = frmUtama._iduseraccount;
                complaint.TransactionDate = cmbDate.DateTime;
                complaint.ComplaintDescription = txtDescription.Text;
                complaint.DepositionTo = txtDepositionTo.Text;
                if (cmbDueDate.EditValue.IsNotEmpty()) complaint.DueDate = cmbDueDate.DateTime;
                complaint.Category = txtCategory.Text;
                complaint.Action = txtAction.Text;
                if (cmbClosingDate.EditValue.IsNotEmpty()) complaint.ClosingDate = cmbClosingDate.DateTime;
                complaint.Status = cmbStatus.Text;
                complaint.PIC = txtPIC.Text;
                complaint.NoBatch = txtNoBatch.Text;
                complaint.VoiceDetail = txtVoiceDetail.Text;
                db.CustomerComplaints.InsertOnSubmit(complaint);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}