﻿namespace GreenChem
{
    partial class frmEditVoiceOfCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cmbClosingDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtAction = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtCategory = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDueDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtDepositionTo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtDescription = new DevExpress.XtraEditors.MemoEdit();
            this.cmbCustomer = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtVoiceDetail = new DevExpress.XtraEditors.MemoEdit();
            this.txtNoBatch = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtPIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepositionTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoiceDetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBatch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(402, 449);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(296, 449);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl12);
            this.panelControl1.Controls.Add(this.txtVoiceDetail);
            this.panelControl1.Controls.Add(this.txtNoBatch);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.txtPIC);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.cmbStatus);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.cmbClosingDate);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.txtAction);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtCategory);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.cmbDueDate);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtDepositionTo);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtDescription);
            this.panelControl1.Controls.Add(this.cmbCustomer);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.cmbDate);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 431);
            this.panelControl1.TabIndex = 0;
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "Open";
            this.cmbStatus.Enabled = false;
            this.cmbStatus.Location = new System.Drawing.Point(91, 277);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "Open",
            "Closed"});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(150, 23);
            this.cmbStatus.TabIndex = 17;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(50, 279);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(35, 17);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Status";
            // 
            // cmbClosingDate
            // 
            this.cmbClosingDate.EditValue = null;
            this.cmbClosingDate.Enabled = false;
            this.cmbClosingDate.Location = new System.Drawing.Point(91, 247);
            this.cmbClosingDate.Name = "cmbClosingDate";
            this.cmbClosingDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbClosingDate.Properties.Appearance.Options.UseFont = true;
            this.cmbClosingDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbClosingDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbClosingDate.Size = new System.Drawing.Size(150, 24);
            this.cmbClosingDate.TabIndex = 15;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(11, 250);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(74, 17);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Closing Date";
            // 
            // txtAction
            // 
            this.txtAction.Enabled = false;
            this.txtAction.Location = new System.Drawing.Point(91, 217);
            this.txtAction.Name = "txtAction";
            this.txtAction.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAction.Properties.Appearance.Options.UseFont = true;
            this.txtAction.Size = new System.Drawing.Size(370, 24);
            this.txtAction.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(49, 220);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(36, 17);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Action";
            // 
            // txtCategory
            // 
            this.txtCategory.Location = new System.Drawing.Point(91, 187);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCategory.Properties.Appearance.Options.UseFont = true;
            this.txtCategory.Size = new System.Drawing.Size(370, 24);
            this.txtCategory.TabIndex = 11;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(32, 190);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(53, 17);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Category";
            // 
            // cmbDueDate
            // 
            this.cmbDueDate.EditValue = null;
            this.cmbDueDate.Location = new System.Drawing.Point(91, 157);
            this.cmbDueDate.Name = "cmbDueDate";
            this.cmbDueDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDueDate.Properties.Appearance.Options.UseFont = true;
            this.cmbDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDueDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDueDate.Size = new System.Drawing.Size(150, 24);
            this.cmbDueDate.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(31, 160);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Due Date";
            // 
            // txtDepositionTo
            // 
            this.txtDepositionTo.Location = new System.Drawing.Point(91, 127);
            this.txtDepositionTo.Name = "txtDepositionTo";
            this.txtDepositionTo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDepositionTo.Properties.Appearance.Options.UseFont = true;
            this.txtDepositionTo.Size = new System.Drawing.Size(370, 24);
            this.txtDepositionTo.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(5, 130);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(82, 17);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Deposition To";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(19, 77);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(66, 17);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "Description";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(91, 74);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDescription.Properties.Appearance.Options.UseFont = true;
            this.txtDescription.Size = new System.Drawing.Size(370, 47);
            this.txtDescription.TabIndex = 5;
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.EditValue = "";
            this.cmbCustomer.Location = new System.Drawing.Point(91, 44);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbCustomer.Properties.Appearance.Options.UseFont = true;
            this.cmbCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCustomer.Properties.DisplayMember = "Name";
            this.cmbCustomer.Properties.NullText = "";
            this.cmbCustomer.Properties.ValueMember = "id";
            this.cmbCustomer.Properties.View = this.searchLookUpEdit1View;
            this.cmbCustomer.Size = new System.Drawing.Size(370, 24);
            this.cmbCustomer.TabIndex = 3;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseFont = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.searchLookUpEdit1View.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.Row.Options.UseFont = true;
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.searchLookUpEdit1View.RowHeight = 23;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(29, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(56, 17);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Customer";
            // 
            // cmbDate
            // 
            this.cmbDate.EditValue = null;
            this.cmbDate.Location = new System.Drawing.Point(91, 14);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDate.Properties.Appearance.Options.UseFont = true;
            this.cmbDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDate.Size = new System.Drawing.Size(150, 24);
            this.cmbDate.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(58, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Date";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(16, 369);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(69, 17);
            this.labelControl12.TabIndex = 28;
            this.labelControl12.Text = "Voice Detail";
            // 
            // txtVoiceDetail
            // 
            this.txtVoiceDetail.Enabled = false;
            this.txtVoiceDetail.Location = new System.Drawing.Point(91, 366);
            this.txtVoiceDetail.Name = "txtVoiceDetail";
            this.txtVoiceDetail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtVoiceDetail.Properties.Appearance.Options.UseFont = true;
            this.txtVoiceDetail.Size = new System.Drawing.Size(370, 47);
            this.txtVoiceDetail.TabIndex = 29;
            // 
            // txtNoBatch
            // 
            this.txtNoBatch.Enabled = false;
            this.txtNoBatch.Location = new System.Drawing.Point(91, 336);
            this.txtNoBatch.Name = "txtNoBatch";
            this.txtNoBatch.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNoBatch.Properties.Appearance.Options.UseFont = true;
            this.txtNoBatch.Size = new System.Drawing.Size(370, 24);
            this.txtNoBatch.TabIndex = 27;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Enabled = false;
            this.labelControl11.Location = new System.Drawing.Point(29, 339);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(56, 17);
            this.labelControl11.TabIndex = 26;
            this.labelControl11.Text = "No. Batch";
            // 
            // txtPIC
            // 
            this.txtPIC.Enabled = false;
            this.txtPIC.Location = new System.Drawing.Point(91, 306);
            this.txtPIC.Name = "txtPIC";
            this.txtPIC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPIC.Properties.Appearance.Options.UseFont = true;
            this.txtPIC.Size = new System.Drawing.Size(370, 24);
            this.txtPIC.TabIndex = 25;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Enabled = false;
            this.labelControl10.Location = new System.Drawing.Point(67, 309);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(18, 17);
            this.labelControl10.TabIndex = 24;
            this.labelControl10.Text = "PIC";
            // 
            // frmEditVoiceOfCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 491);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditVoiceOfCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Voice of Customer";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepositionTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoiceDetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBatch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        public DevExpress.XtraEditors.DateEdit cmbClosingDate;
        public DevExpress.XtraEditors.TextEdit txtAction;
        public DevExpress.XtraEditors.TextEdit txtCategory;
        public DevExpress.XtraEditors.DateEdit cmbDueDate;
        public DevExpress.XtraEditors.MemoEdit txtDescription;
        public DevExpress.XtraEditors.SearchLookUpEdit cmbCustomer;
        public DevExpress.XtraEditors.DateEdit cmbDate;
        public DevExpress.XtraEditors.SimpleButton btnSave;
        public DevExpress.XtraEditors.TextEdit txtDepositionTo;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.MemoEdit txtVoiceDetail;
        private DevExpress.XtraEditors.TextEdit txtNoBatch;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtPIC;
        private DevExpress.XtraEditors.LabelControl labelControl10;
    }
}