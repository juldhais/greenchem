﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmPPB : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPPB()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New PPB");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit PPB");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete PPB");
            btnSetPONumber.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Set PO Number PPB");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.PPBs
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                x.PONumber,
                                Product = x.Product.Name,
                                x.Quantity,
                                x.Packaging,
                                x.Remarks,
                                Customer = x.Customer.Name
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewPPB form = new frmNewPPB();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditPPB form = new frmEditPPB(id);
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    PPB ppb = db.PPBs.First(x => x.id == id);
                    db.PPBs.DeleteOnSubmit(ppb);
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);
                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;

            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmViewPPB form = new frmViewPPB(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnSetPONumber_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmSetPONumber form = new frmSetPONumber(gridView.GetFocusedRowCellValue("PONumber").Safe());
                form.ShowDialog();

                if (frmSetPONumber._number.IsNotEmpty())
                {
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    PPB ppb = db.PPBs.First(x => x.id == id);
                    ppb.PONumber = frmSetPONumber._number;
                    db.SubmitChanges();

                    XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblRefresh_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}