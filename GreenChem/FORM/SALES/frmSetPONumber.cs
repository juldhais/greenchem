﻿using System;

namespace GreenChem.MenuTransaction
{
    public partial class frmSetPONumber : DevExpress.XtraEditors.XtraForm
    {
        public static string _number;

        public frmSetPONumber(string number)
        {
            InitializeComponent();

            _number = number;
            txtPONumber.Text = _number;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _number = txtPONumber.Text;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _number = string.Empty;
            Close();
        }
    }
}