﻿using System;
using JulFunctions;
using System.Linq;

namespace GreenChem.MenuTransaction
{
    public partial class frmCreateInvoice : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;
        decimal _oldtotal = 0;
        public static DateTime? date = null;
        public static decimal rate = 1;
        public static string invoicenumber = string.Empty;

        public frmCreateInvoice(int id)
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            txtRate.EditValue = 1;

            db = new GreenChemDataContext();
            var sales = db.SalesOrders.First(x => x.id == id);
            txtTransactionNumber.Text = sales.TransactionNumber;
            txtCustomer.Text = sales.Customer.Name;
            _oldtotal = sales.TotalDelivered;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            invoicenumber = txtInvoiceNumber.Text;
            date = cmbDate.DateTime;
            rate = txtRate.EditValue.ToDecimal();
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            date = null;
            rate = 1;
            Close();
        }

        private void txtRate_EditValueChanged(object sender, EventArgs e)
        {
            txtTotal.EditValue = _oldtotal * txtRate.EditValue.ToDecimal();
        }
    }
}