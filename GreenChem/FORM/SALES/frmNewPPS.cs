﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewPPS : DevExpress.XtraEditors.XtraForm
    {
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmNewPPS()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDate.DateTime = DateTime.Now;
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            txtTransactionNumber.Text = db.GetPPSNumber();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                PPS pps = new PPS();

                if (cmbCustomer.EditValue.IsNotEmpty()) pps.idCustomer = cmbCustomer.EditValue.ToInteger();
                pps.idUserAccount = frmUtama._iduseraccount;
                pps.idDepartment = frmUtama._iddepartment;
                pps.idSubDepartment = frmUtama._idsubdepartment;
                pps.TransactionNumber = txtTransactionNumber.Text;
                pps.TransactionDate = cmbDate.DateTime;
                pps.Division = txtDivision.Text;
                pps.Remarks = txtRemarks.Text;
                if (cmbTanggalPenerimaan.EditValue != null) pps.TanggalPenerimaan = cmbTanggalPenerimaan.DateTime;

                foreach (var item in listdetail)
                {
                    DetailPPS detail = new DetailPPS();
                    //detail.idProduct = item.idProduct;
                    detail.ProductName = item.ProductName;
                    detail.UoM = item.UoM;
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;
                    detail.Discount = item.Discount;
                    detail.Subtotal = item.Subtotal;
                    pps.DetailPPS.Add(detail);
                }

                db.PPS.InsertOnSubmit(pps);
                db.SubmitChanges();

                XtraMessageBox.Show("Data save successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailPPS form = new frmDetailPPS(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}