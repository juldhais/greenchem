﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem
{
    public partial class frmEditVoiceOfCustomer : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditVoiceOfCustomer(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();

            var complaint = db.CustomerComplaints.First(x => x.id == _id);
            cmbDate.DateTime = complaint.TransactionDate;
            cmbCustomer.EditValue = complaint.idCustomer;
            txtDescription.Text = complaint.ComplaintDescription;
            txtDepositionTo.Text = complaint.DepositionTo;
            cmbDueDate.EditValue = complaint.DueDate;
            txtCategory.Text = complaint.Category;
            txtAction.Text = complaint.Action;
            cmbClosingDate.EditValue = complaint.ClosingDate;
            cmbStatus.Text = complaint.Status;
            txtPIC.Text = complaint.PIC;
            txtNoBatch.Text = complaint.NoBatch;
            txtVoiceDetail.Text = complaint.VoiceDetail;

            txtDepositionTo.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Allow Edit Deposition");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtDescription.Text.IsEmpty())
            {
                XtraMessageBox.Show("Description can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDescription.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                CustomerComplaint complaint = db.CustomerComplaints.First(x => x.id == _id);
                if (cmbCustomer.EditValue.IsNotEmpty()) complaint.idCustomer = cmbCustomer.EditValue.ToInteger();
                complaint.idUserAccount = frmUtama._iduseraccount;
                complaint.TransactionDate = cmbDate.DateTime;
                complaint.ComplaintDescription = txtDescription.Text;
                complaint.DepositionTo = txtDepositionTo.Text;
                if (cmbDueDate.EditValue.IsNotEmpty()) complaint.DueDate = cmbDueDate.DateTime;
                complaint.Category = txtCategory.Text;
                complaint.Action = txtAction.Text;
                if (cmbClosingDate.EditValue.IsNotEmpty()) complaint.ClosingDate = cmbClosingDate.DateTime;
                complaint.Status = cmbStatus.Text;
                complaint.PIC = txtPIC.Text;
                complaint.NoBatch = txtNoBatch.Text;
                complaint.VoiceDetail = txtVoiceDetail.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}