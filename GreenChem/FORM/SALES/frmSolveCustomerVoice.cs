﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmSolveCustomerVoice : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmSolveCustomerVoice(int id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            //cmbCustomer.Properties.DataSource = db.Customers.GetName();

            var voice = db.CustomerVoices.First(x => x.id == _id);
            //cmbCustomer.EditValue = voice.idCustomer;
            txtTransactionNumber.Text = voice.TransactionNumber;
            cmbDate.DateTime = voice.TransactionDate;
            txtDescription.Text = voice.ComplaintDescription;
            txtDepositionTo.Text = voice.DepositionTo;
            cmbDueDate.EditValue = voice.DueDate;
            txtCategory.Text = voice.Category;
            txtAction.Text = voice.Action;
            cmbClosingDate.EditValue = voice.ClosingDate;
            cmbStatus.Text = "Closed";
            cmbCustomerSupport.Text = voice.CustomerSupport;
            cmbTime.Text = voice.Time;
            cmbQuality.Text = voice.Quality;
            cmbTechnicalSupport.Text = voice.TechnicalSupport;
            txtSuggestion.Text = voice.Suggestion;
            txtPIC.Text = voice.PIC;

            //var customer = db.Customers.First(x => x.id == cmbCustomer.EditValue.ToInteger());
            if (voice.CustomerName.IsNotEmpty())
            {
                txtCustomer.Text = voice.CustomerName;
                txtPhone.Text = voice.CustomerPhone;
                txtFax.Text = voice.CustomerFax;
                txtEmail.Text = voice.CustomerEmail;
            }
            else if (voice.idCustomer != null)
            {
                txtCustomer.Text = voice.Customer.Name;
                txtPhone.Text = voice.Customer.Phone;
                txtFax.Text = voice.Customer.Fax;
                txtEmail.Text = voice.Customer.Email;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                CustomerVoice voice = db.CustomerVoices.First(x => x.id == _id);
                voice.Action = txtAction.Text;
                voice.ClosingDate = cmbClosingDate.DateTime;
                voice.Status = cmbStatus.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}