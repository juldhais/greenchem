﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction2
{
    public partial class frmProjectCategory : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmProjectCategory()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            projectCategoryBindingSource.DataSource = db.ProjectCategories.Where(x => x.Active == true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetRowCellValue(e.RowHandle, "Active", true);
        }
    }
}