﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Linq;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewInquiryIn : DevExpress.XtraEditors.XtraForm
    {
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmNewInquiryIn()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            RefreshData();
        }

        private void RefreshData()
        {
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            txtTransactionNumber.Text = db.GetInquiryInNumber();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();

                    if (db.InquiryIns.Any(x => x.TransactionNumber == txtTransactionNumber.Text))
                    {
                        XtraMessageBox.Show("Transaction Number is already exist. System will change the number to the newest number.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtTransactionNumber.Text = db.GetInquiryInNumber();
                        txtTransactionNumber.Focus();
                        return;
                    }

                    InquiryIn inquiry = new InquiryIn();
                    if (cmbDepartment.EditValue.IsNotEmpty()) inquiry.idDepartment = cmbDepartment.EditValue.ToInteger();
                    if (cmbSubDepartment.EditValue.IsNotEmpty()) inquiry.idSubDepartment = cmbSubDepartment.EditValue.ToInteger();
                    if (cmbCustomer.EditValue.IsNotEmpty()) inquiry.idCustomer = cmbCustomer.EditValue.ToInteger();
                    
                    inquiry.TransactionNumber = txtTransactionNumber.Text;
                    inquiry.TransactionDate = cmbDate.DateTime;
                    inquiry.idUserAccount = frmUtama._iduseraccount;
                    inquiry.Remarks = txtRemarks.Text;

                    db.InquiryIns.InsertOnSubmit(inquiry);
                    db.SubmitChanges();

                    //details
                    foreach (var item in listdetail)
                    {
                        if (item.Quantity != 0)
                        {
                            DetailInquiryIn detail = new DetailInquiryIn();
                            detail.idProduct = item.idProduct;
                            detail.idInquiryIn = inquiry.id;
                            detail.Quantity = item.Quantity;
                            detail.Price = item.Price;
                            detail.Discount = item.Discount;
                            detail.Subtotal = item.Subtotal;
                            detail.ProductName = item.Remarks;
                            db.DetailInquiryIns.InsertOnSubmit(detail);
                        }
                    }
                    db.SubmitChanges();

                    XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailInquiryIn form = new frmDetailInquiryIn(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}