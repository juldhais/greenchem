﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmEditProject : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditProject(int id)
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            cmbName.DataSource = db.UserAccounts.GetName();
            cmbProjectCategory.Properties.DataSource = from x in db.ProjectCategories
                                            where x.Active == true
                                            select new
                                            {
                                                x.id,
                                                x.Name
                                            };

            cmbParent.Properties.DataSource = from x in db.ProjectLists
                                              where x.TransactionDate.Date >= DateTime.Now.AddYears(-1)
                                              select new
                                              {
                                                  x.id,
                                                  x.TransactionDate,
                                                  Category = x.ProjectCategory.Name,
                                                  Customer = x.Customer.Name
                                              };

            _id = id;
            var project = db.ProjectLists.First(x => x.id == _id);

            if (!project.ProjectUserAccounts.Any(x => x.idUserAccount == frmUtama._iduseraccount))
            {
                XtraMessageBox.Show("You are not allowed to modify this project.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnSave.Enabled = false;
            }

            cmbDate.DateTime = project.TransactionDate;
            cmbProjectCategory.EditValue = project.idProjectCategory;
            cmbCustomer.EditValue = project.idCustomer;
            txtDescription.Text = project.Description;
            cmbStatus.Text = project.Status;
            cmbParent.EditValue = project.idParent;

            foreach (var item in project.ProjectUserAccounts)
            {
                TempId temp = new TempId();
                temp.id = item.idUserAccount;
                tempIdBindingSource.Add(temp);
            }

            gridControl.RefreshDataSource();
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridView.DeleteSelectedRows();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                db.ExecuteCommand("DELETE FROM dbo.ProjectUserAccount WHERE idProjectList = " + _id);

                ProjectList project = db.ProjectLists.First(x => x.id == _id);
                if (cmbCustomer.EditValue.IsNotEmpty()) project.idCustomer = cmbCustomer.EditValue.ToInteger();
                if (cmbCustomer.EditValue.IsNotEmpty()) project.idProjectCategory = cmbProjectCategory.EditValue.ToInteger();
                if (cmbParent.EditValue.IsNotEmpty()) project.idParent = cmbParent.EditValue.ToNullInteger();
                project.TransactionDate = cmbDate.DateTime;
                project.Description = txtDescription.Text;
                project.Status = cmbStatus.Text;

                foreach (TempId item in tempIdBindingSource)
                {
                    ProjectUserAccount user = new ProjectUserAccount();
                    user.idUserAccount = item.id;
                    project.ProjectUserAccounts.Add(user);
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Project saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnViewChild_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmViewChildProject form = new frmViewChildProject(_id);
                form.Show();
                form.BringToFront();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}