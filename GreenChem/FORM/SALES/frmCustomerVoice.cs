﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmCustomerVoice : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmCustomerVoice()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Now.AddYears(-1);
            cmbDateEnd.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Customer Voice");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Customer Voice");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Customer Voice");
            btnSolve.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Solve Customer Voice");
            btnSetDeposition.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Set Deposition");
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewCustomerVoice form = new frmNewCustomerVoice();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditCustomerVoice form = new frmEditCustomerVoice(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    db.ExecuteCommand("DELETE FROM dbo.CustomerVoice WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblRefresh_Click(null, null);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            string status = gridView.GetFocusedRowCellValue("Status").Safe();
            if (status == "Open")
            {
                frmSolveCustomerVoice form = new frmSolveCustomerVoice(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            else XtraMessageBox.Show("Complaint is already solved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            Cursor.Current = Cursors.Default;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.CustomerVoices
                            where x.TransactionDate.Date >= cmbDateStart.DateTime.Date && x.TransactionDate.Date <= cmbDateEnd.DateTime.Date
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Customer = x.CustomerName != null ? x.CustomerName : x.Customer.Name,
                                x.ComplaintDescription,
                                x.DepositionTo,
                                x.DueDate,
                                x.Category,
                                x.Action,
                                x.ClosingDate,
                                x.Status,
                                x.CustomerSupport,
                                x.Time,
                                x.Quality,
                                x.TechnicalSupport,
                                x.Suggestion,
                                x.PIC
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnSetDeposition_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string deposition = gridView.GetFocusedRowCellValue("DepositionTo").Safe();
                if (deposition.IsEmpty())
                {
                    frmSetDeposition form = new frmSetDeposition(id);
                    form.ShowDialog();
                    lblRefresh_Click(null, null);
                }
                else
                {
                    XtraMessageBox.Show("Deposition is already set.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}