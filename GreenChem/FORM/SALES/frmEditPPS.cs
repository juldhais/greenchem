﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditPPS : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmEditPPS(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            var pps = db.PPS.First(x => x.id == _id);
            txtTransactionNumber.Text = pps.TransactionNumber;
            cmbDate.DateTime = pps.TransactionDate;
            txtDivision.Text = pps.Division;
            cmbCustomer.EditValue = pps.idCustomer;
            txtRemarks.Text = pps.Remarks;
            cmbTanggalPenerimaan.EditValue = pps.TanggalPenerimaan;

            //details
            var details = db.DetailPPS.Where(x => x.idPPS == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                //temp.idProduct = item.idProduct;
                temp.ProductName = item.ProductName;
                temp.UoM = item.UoM;
                temp.Quantity = item.Quantity;
                temp.UoM = item.UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                listdetail.Add(temp);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                db.ExecuteCommand("DELETE FROM dbo.DetailPPS WHERE idPPS = " + _id);

                PPS pps = db.PPS.First(x => x.id == _id);

                if (cmbCustomer.EditValue.IsNotEmpty()) pps.idCustomer = cmbCustomer.EditValue.ToInteger();
                pps.idUserAccount = frmUtama._iduseraccount;
                pps.idDepartment = frmUtama._iddepartment;
                pps.idSubDepartment = frmUtama._idsubdepartment;
                pps.TransactionNumber = txtTransactionNumber.Text;
                pps.TransactionDate = cmbDate.DateTime;
                pps.Division = txtDivision.Text;
                pps.Remarks = txtRemarks.Text;
                if (cmbTanggalPenerimaan.EditValue != null) pps.TanggalPenerimaan = cmbTanggalPenerimaan.DateTime;

                foreach (var item in listdetail)
                {
                    DetailPPS detail = new DetailPPS();
                    //detail.idProduct = item.idProduct;
                    detail.ProductName = item.ProductName;
                    detail.UoM = item.UoM;
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;
                    detail.Discount = item.Discount;
                    detail.Subtotal = item.Subtotal;
                    pps.DetailPPS.Add(detail);
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data save successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailPPS form = new frmDetailPPS(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}