﻿using System;

namespace GreenChem.MenuTransaction
{
    public partial class frmChooseDate : DevExpress.XtraEditors.XtraForm
    {
        public static DateTime? date= null;

        public frmChooseDate()
        {
            
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            date = cmbDate.DateTime;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            date = null;
            Close();
        }
    }
}