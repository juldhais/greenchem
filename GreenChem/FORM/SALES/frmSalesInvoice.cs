﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmSalesInvoice : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmSalesInvoice()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Now.AddDays(-365);
            cmbDateEnd.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);

            btnCreateInvoice.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Create Invoice");
            btnCancelInvoice.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Cancel Invoice");
            btnPrintInvoice.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Invoice");
            btnPayInvoice.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Pay Invoice");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.SalesOrders
                            where x.Status == "DELIVERED" || x.Status == "INVOICED" || x.Status == "VOID" || x.Status == "PAID"
                            && x.DateDelivered.Value.Date >= cmbDateStart.DateTime.Date && x.DateDelivered.Value.Date <= cmbDateEnd.DateTime.Date
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.DateDelivered,
                                x.InvoiceNumber,
                                x.DateInvoice,
                                x.DatePaid,
                                Customer = x.Customer.Name,
                                x.Status,
                                x.Remarks,
                                x.TotalDelivered,
                                TotalIDR = x.TotalDelivered * x.Rate,
                                Currency = x.idCurrency != null ? db.Currencies.First(a => a.id == x.idCurrency).Symbol : "",
                                x.Rate
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnCreateInvoice_Click(object sender, EventArgs e)
        {
            if (gridView.GetFocusedRowCellValue("Status").Safe() != "DELIVERED")
            {
                XtraMessageBox.Show("Can't create invoice!.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                gridControl.Focus();
                return;
            }

            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmCreateInvoice form = new frmCreateInvoice(id);
                form.ShowDialog();
                if (frmCreateInvoice.date == null) return;

                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                DialogResult = XtraMessageBox.Show("Create invoice for this Sales Order : " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    SalesOrder salesorder = db.SalesOrders.First(x => x.id == id);
                    salesorder.InvoiceNumber = frmCreateInvoice.invoicenumber;
                    salesorder.DateInvoice = frmCreateInvoice.date;
                    salesorder.Rate = frmCreateInvoice.rate;
                    salesorder.Status = "INVOICED";
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);
                    Printing.SalesInvoice(id);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancelInvoice_Click(object sender, EventArgs e)
        {
            if (gridView.GetFocusedRowCellValue("Status").Safe() != "INVOICED")
            {
                XtraMessageBox.Show("This transaction is not invoiced yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                gridControl.Focus();
                return;
            }

            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to cancel this invoice : " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    SalesOrder salesorder = db.SalesOrders.First(x => x.id == id);
                    salesorder.DateInvoice = null;
                    salesorder.Status = "VOID";
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPrintInvoice_Click(object sender, EventArgs e)
        {
            if (gridView.GetFocusedRowCellValue("Status").Safe() == "INVOICED" || gridView.GetFocusedRowCellValue("Status").Safe() == "PAID")
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                Printing.SalesInvoice(id);
            }
        }

        private void btnPayInvoice_Click(object sender, EventArgs e)
        {
            if (gridView.GetFocusedRowCellValue("Status").Safe() == "INVOICED")
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmPayInvoice form = new frmPayInvoice(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
                Cursor.Current = Cursors.Default;
            }
            else if (gridView.GetFocusedRowCellValue("Status").Safe() == "PAID")
            {
                XtraMessageBox.Show("Sales Order is already paid.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                XtraMessageBox.Show("Sales Order is not invoiced yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}