﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmCustomerSatisfaction : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmCustomerSatisfaction()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Now.AddDays(-365);
            cmbDateEnd.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Customer Voice");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Customer Voice");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Customer Voice");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.CustomerSatisfactions
                            where x.TransactionDate.Date >= cmbDateStart.DateTime.Date && x.TransactionDate.Date <= cmbDateEnd.DateTime.Date
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Customer = x.Customer.Name,
                                x.CustomerSupport,
                                x.Time,
                                x.Quality,
                                x.TechnicalSupport,
                                x.Suggestion,
                                x.PIC
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewCustomerSatisfaction form = new frmNewCustomerSatisfaction();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditCustomerSatisfaction form = new frmEditCustomerSatisfaction(id);
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this customer satisfaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    db.ExecuteCommand("DELETE FROM dbo.CustomerSatisfaction WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lblRefresh_Click(null, null);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}