﻿using System;
using System.Collections.Generic;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmViewQuotationOut : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmViewQuotationOut(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbProduct.DataSource = db.Products.GetName();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            var quotationout = db.QuotationOuts.First(x => x.id == _id);
            txtTransactionNumber.Text = quotationout.TransactionNumber;
            cmbDate.DateTime = quotationout.TransactionDate;
            cmbCustomer.EditValue = quotationout.idCustomer;
            txtRemarks.Text = quotationout.Remarks;

            //details
            var details = db.DetailQuotationOuts.Where(x => x.idQuotationOut == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.idProduct = item.idProduct;
                temp.Quantity = item.Quantity;
                temp.UoM = db.Products.First(x => x.id == item.idProduct).UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                listdetail.Add(temp);
            }

            gridControl.DataSource = listdetail;

            if (listdetail.Any())
                txtTotal.EditValue = listdetail.Sum(x => (decimal?)x.Subtotal).ToDecimal();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}