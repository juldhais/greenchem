﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmSetDeposition : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmSetDeposition(int id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            var custvoice = db.CustomerVoices.First(x => x.id == _id);
            txtTransactionNumber.Text = custvoice.TransactionNumber;
            txtDepositionTo.Text = custvoice.DepositionTo;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                CustomerVoice custvoice = db.CustomerVoices.First(x => x.id == _id);
                custvoice.DepositionTo = txtDepositionTo.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}