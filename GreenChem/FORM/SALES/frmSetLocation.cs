﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmSetLocation : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        GreenChemDataContext db;

        public frmSetLocation(int? id = null, string text = "")
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            cmbLocation.Properties.DataSource = db.Locations.GetName();
            cmbLocation.EditValue = db.SalesOrders.First(x => x.id == _id).idLocation;
            lblText.Text = text;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                SalesOrder salesorder = db.SalesOrders.First(x => x.id == _id);
                salesorder.idLocation = (int?)cmbLocation.EditValue;
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}