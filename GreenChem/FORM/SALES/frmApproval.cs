﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmApproval : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmApproval()
        {
            InitializeComponent();
            RefreshData();
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.Approvals
                            where x.Approved == false
                            select new
                            {
                                x.id,
                                x.idTransaction,
                                x.Module
                            };

                tempApprovalBindingSource.Clear();
                foreach (var item in query)
                {
                    TempApproval temp = new TempApproval();
                    temp.id = item.id;
                    temp.idTransaction = item.idTransaction;
                    temp.Module = item.Module;
                    temp.Approved = false;

                    if (temp.Module == "Sales Order")
                    {
                        var salesorder = db.SalesOrders.First(x => x.id == item.idTransaction);
                        temp.TransactionNumber = salesorder.TransactionNumber;
                        temp.TransactionDate = salesorder.DateOrder;
                        temp.Remarks = salesorder.Customer.Name;
                        temp.Amount = salesorder.TotalOrder;
                    }

                    tempApprovalBindingSource.Add(temp);
                }

                gridControl.RefreshDataSource();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                foreach (TempApproval item in tempApprovalBindingSource)
                {
                    if (item.Approved)
                    {
                        Approval approval = db.Approvals.First(x => x.id == item.id);
                        approval.Approved = true;
                        approval.idUserAccount = frmUtama._iduseraccount;
                        approval.DateApproved = DateTime.Now;
                    }
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}