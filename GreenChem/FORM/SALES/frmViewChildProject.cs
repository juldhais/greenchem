﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmViewChildProject : DevExpress.XtraEditors.XtraForm
    {
        int? _idparent = null;
        GreenChemDataContext db;

        public frmViewChildProject(int? idparent)
        {
            InitializeComponent();

            _idparent = idparent;

            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Project");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Project");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Project");
            btnView.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "View Project");
            btnViewProjectProgress.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "View Project");
            btnCreateProjectProgress.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Create Project Progress");
            btnCloseProject.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Close Project");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.ProjectLists
                            where x.idParent != null  && x.idParent == _idparent
                            orderby x.TransactionDate descending
                            select new
                            {
                                x.idUserAccount,
                                x.id,
                                x.TransactionDate,
                                Customer = x.Customer.Name,
                                ProjectCategory = x.ProjectCategory.Name,
                                x.Description,
                                x.Status,
                                x.ProjectUserAccounts
                            };

                if (!db.CheckUserRole(frmUtama._iduseraccount.Value, "View All Project"))
                    query = query.Where(x => x.ProjectUserAccounts.Any(a => a.idUserAccount == frmUtama._iduseraccount));


                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewProject form = new frmNewProject();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                string status = gridView.GetFocusedRowCellValue("Status").Safe();

                if (status == "Closed")
                {
                    XtraMessageBox.Show("Project is already closed.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditProject form = new frmEditProject(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string description = gridView.GetFocusedRowCellValue("Description").Safe();
                DialogResult = XtraMessageBox.Show("Delete project " + description + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();

                    db.ExecuteCommand("DELETE FROM dbo.ProjectUserAccount WHERE idProjectList = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.ProjectProgress WHERE idProjectList = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.ProjectList WHERE id = " + id);

                    XtraMessageBox.Show("Project deleted.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditProject form = new frmEditProject(id);
                form.btnSave.Enabled = false;
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnCreateProjectProgress_Click(object sender, EventArgs e)
        {
            try
            {
                string description = gridView.GetFocusedRowCellValue("Description").Safe();
                string status = gridView.GetFocusedRowCellValue("Status").Safe();

                if (status == "Closed")
                {
                    XtraMessageBox.Show("Project is already closed.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                DialogResult = XtraMessageBox.Show("Create progress for project : " + description, Text + "?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();

                    if (!db.ProjectUserAccounts.Any(x => x.idProjectList == id && x.idUserAccount == frmUtama._iduseraccount))
                    {
                        XtraMessageBox.Show("You are not allowed to modify this project.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        frmCreateProjectProgress form = new frmCreateProjectProgress(id);
                        form.ShowDialog();
                        RefreshData();
                    }
                }
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnCloseProject_Click(object sender, EventArgs e)
        {
            try
            {
                string description = gridView.GetFocusedRowCellValue("Description").Safe();
                string status = gridView.GetFocusedRowCellValue("Status").Safe();

                if (status == "Closed")
                {
                    XtraMessageBox.Show("Project is already closed.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                DialogResult = XtraMessageBox.Show("Close project : " + description, Text + "?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();

                    if (!db.ProjectUserAccounts.Any(x => x.idProjectList == id && x.idUserAccount == frmUtama._iduseraccount))
                    {
                        XtraMessageBox.Show("You are not allowed to modify this project.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        ProjectList project = db.ProjectLists.First(x => x.id == id);
                        project.Status = "Closed";
                        db.SubmitChanges();

                        XtraMessageBox.Show("Project closed.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        RefreshData();
                    }
                }
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnViewProjectProgress_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmViewProjectProgress form = new frmViewProjectProgress(id);
                form.ShowDialog();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

    }
}