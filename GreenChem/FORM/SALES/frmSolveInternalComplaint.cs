﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmSolveInternalComplaint : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmSolveInternalComplaint(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            var ic = db.InternalComplaints.First(x => x.id == _id);
            txtTransactionNumber.Text = ic.TransactionNumber;
            cmbDate.DateTime = ic.TransactionDate;
            txtDescription.Text = ic.Description;
            txtDeposition.Text = ic.Deposition;
            cmbDueDate.EditValue = ic.DueDate;
            txtAction.Text = ic.Action;
            cmbClosingDate.EditValue = ic.ClosingDate;
            //cmbStatus.Text = ic.Status;
            txtSuggestion.Text = ic.Suggestion;
            if (cmbClosingDate.EditValue == null) cmbClosingDate.DateTime = DateTime.Now;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                InternalComplaint complaint = db.InternalComplaints.First(x => x.id == _id);
                complaint.Action = txtAction.Text;
                complaint.ClosingDate = cmbClosingDate.DateTime;
                complaint.Status = cmbStatus.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}