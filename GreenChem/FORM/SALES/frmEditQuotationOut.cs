﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditQuotationOut : DevExpress.XtraEditors.XtraForm
    {
        int? _id = null;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmEditQuotationOut(int? id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            lblRefresh_Click(null, null);

            var quotation = db.QuotationOuts.First(x => x.id == _id);
            txtTransactionNumber.Text = quotation.TransactionNumber;
            cmbDate.DateTime = quotation.TransactionDate;
            cmbCustomer.EditValue = quotation.idCustomer;
            txtCustomer.Text = quotation.CustomerText;
            txtRemarks.Text = quotation.Remarks;

            //details
            var details = db.DetailQuotationOuts.Where(x => x.idQuotationOut == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.idProduct = item.idProduct;
                temp.Quantity = item.Quantity;
                temp.UoM = db.Products.First(x => x.id == item.idProduct).UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                listdetail.Add(temp);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                QuotationOut quotation = db.QuotationOuts.First(x => x.id == _id);
                if (frmUtama._iddepartment != null) quotation.idDepartment = frmUtama._iddepartment;
                if (frmUtama._idsubdepartment != null) quotation.idSubDepartment = frmUtama._idsubdepartment;
                if (cmbCustomer.EditValue.IsNotEmpty()) quotation.idCustomer = cmbCustomer.EditValue.ToInteger();
                quotation.TransactionNumber = txtTransactionNumber.Text;
                quotation.TransactionDate = cmbDate.DateTime;
                quotation.idUserAccount = frmUtama._iduseraccount;
                quotation.Remarks = txtRemarks.Text;
                quotation.CustomerText = txtCustomer.Text;
                db.SubmitChanges();

                //details
                db.ExecuteCommand("DELETE FROM dbo.DetailQuotationOut WHERE idQuotationOut = " + quotation.id);
                foreach (var item in listdetail)
                {
                    if (item.Quantity != 0)
                    {
                        DetailQuotationOut detail = new DetailQuotationOut();
                        detail.idProduct = item.idProduct;
                        detail.idQuotationOut = quotation.id;
                        detail.Quantity = item.Quantity;
                        detail.Price = item.Price;
                        detail.Discount = item.Discount;
                        detail.Subtotal = item.Subtotal;
                        db.DetailQuotationOuts.InsertOnSubmit(detail);
                    }
                }
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailQuotationOut form = new frmDetailQuotationOut(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}