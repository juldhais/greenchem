﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditPPB : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmEditPPB(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            var ppb = db.PPBs.First(x => x.id == _id);
            txtTransactionNumber.Text = ppb.TransactionNumber;
            cmbDate.DateTime = ppb.TransactionDate;
            txtPONumber.Text = ppb.PONumber;
            cmbCustomer.EditValue = ppb.idCustomer;
            txtRemarks.Text = ppb.Remarks;

            //details
            var details = db.DetailPPBs.Where(x => x.idPPB == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.ProductName = item.ProductName;
                if (temp.ProductName.IsEmpty() && db.Products.Any(x => x.id == item.idProduct))
                    temp.ProductName = item.Product.Name;
                temp.UoM = item.UoM;
                temp.Quantity = item.Quantity;
                temp.UoM = item.UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                listdetail.Add(temp);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                db.ExecuteCommand("DELETE FROM dbo.DetailPPB WHERE idPPB = " + _id);

                PPB ppb = db.PPBs.First(x => x.id == _id);

                if (cmbCustomer.EditValue.IsNotEmpty()) ppb.idCustomer = cmbCustomer.EditValue.ToInteger();
                ppb.idUserAccount = frmUtama._iduseraccount;
                ppb.idDepartment = frmUtama._iddepartment;
                ppb.idSubDepartment = frmUtama._idsubdepartment;
                ppb.TransactionNumber = txtTransactionNumber.Text;
                ppb.TransactionDate = cmbDate.DateTime;
                ppb.PONumber = txtPONumber.Text;
                ppb.Remarks = txtRemarks.Text;

                foreach (var item in listdetail)
                {
                    DetailPPB detail = new DetailPPB();
                    //detail.idProduct = item.idProduct;
                    detail.ProductName = item.ProductName;
                    detail.UoM = item.UoM;
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;
                    detail.Discount = item.Discount;
                    detail.Subtotal = item.Subtotal;
                    ppb.DetailPPBs.Add(detail);
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data save successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailPPB form = new frmDetailPPB(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}