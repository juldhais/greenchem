﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewPPB : DevExpress.XtraEditors.XtraForm
    {
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmNewPPB()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDate.DateTime = DateTime.Now;
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            txtTransactionNumber.Text = db.GetPPBNumber();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                PPB ppb = new PPB();

                if (cmbCustomer.EditValue.IsNotEmpty()) ppb.idCustomer = cmbCustomer.EditValue.ToInteger();
                ppb.idUserAccount = frmUtama._iduseraccount;
                ppb.idDepartment = frmUtama._iddepartment;
                ppb.idSubDepartment = frmUtama._idsubdepartment;
                ppb.TransactionNumber = txtTransactionNumber.Text;
                ppb.TransactionDate = cmbDate.DateTime;
                ppb.PONumber = txtPONumber.Text;
                ppb.Remarks = txtRemarks.Text;

                foreach (var item in listdetail)
                {
                    DetailPPB detail = new DetailPPB();
                    //detail.idProduct = item.idProduct;
                    detail.ProductName = item.ProductName;
                    detail.UoM = item.UoM;
                    detail.Quantity = item.Quantity;
                    detail.Price = item.Price;
                    detail.Discount = item.Discount;
                    detail.Subtotal = item.Subtotal;
                    ppb.DetailPPBs.Add(detail);
                }

                db.PPBs.InsertOnSubmit(ppb);
                db.SubmitChanges();

                XtraMessageBox.Show("Data save successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailPPB form = new frmDetailPPB(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}