﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmSalesOrder : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmSalesOrder()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Now.AddDays(-365);
            cmbDateEnd.DateTime = DateTime.Now;
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Sales Order");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Sales Order");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Sales Order");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Sales Order");
            btnApprove.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve Sales Order");
            btnApprove2.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve2 Sales Order");

            if (System.IO.File.Exists("layout_salesorder.xml"))
                gridView.RestoreLayoutFromXml("layout_salesorder.xml");
        }

        public void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.SalesOrders
                            where x.DateOrder.Date >= cmbDateStart.DateTime.Date && x.DateOrder.Date <= cmbDateEnd.DateTime.Date
                            && x.Status != "WIP"
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.DateOrder,
                                x.Status,
                                Customer = x.Customer.Name,
                                x.Remarks,
                                x.TotalOrder,
                                TotalOrderIDR = x.TotalOrder * x.Rate,
                                Currency = x.idCurrency != null ? db.Currencies.First(a => a.id == x.idCurrency).Symbol : "",
                                x.Rate,
                                x.DateApproved,
                                ApprovedBy = x.ApprovedBy != null ? db.UserAccounts.First(a => a.id == x.ApprovedBy).Name : "",
                                x.DateApproved2,
                                ApprovedBy2 = x.Approved2 != null ? db.UserAccounts.First(a => a.id == x.ApprovedBy2).Name : "",
                                Salesman = x.idSalesman != null ? db.UserAccounts.First(a => a.id == x.idSalesman).Name : "",
                                DPP = x.TotalOrder - x.TaxOrder,
                                PPN = x.TaxOrder
                                
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewSalesOrder form = new frmNewSalesOrder();
            form.MdiParent = Application.OpenForms["frmUtama"];
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEditSalesOrder form = new frmEditSalesOrder(gridView.GetFocusedRowCellValue("id").ToInteger());
            form.MdiParent = Application.OpenForms["frmUtama"];
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    db.ExecuteCommand("DELETE FROM dbo.DetailSalesOrder WHERE idSalesOrder = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.SalesOrder WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RefreshData();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                Printing.SalesOrder(id);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEditSalesOrder form = new frmEditSalesOrder(gridView.GetFocusedRowCellValue("id").ToInteger());
            form.Show();
            form.BringToFront();
            form.btnSave.Visible = false;
            Cursor.Current = Cursors.Default;
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                string status = gridView.GetFocusedRowCellValue("Status").Safe();

                db = new GreenChemDataContext();
                SalesOrder salesorder = db.SalesOrders.First(x => x.id == id);

                if (status == "OPEN" && salesorder.Approved != true)
                {
                    DialogResult = XtraMessageBox.Show("Approve this Sales Order : " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (DialogResult == DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        salesorder.Approved = true;
                        salesorder.ApprovedBy = frmUtama._iduseraccount;
                        salesorder.DateApproved = DateTime.Now;
                        db.SubmitChanges();
                        RefreshData();

                        XtraMessageBox.Show("Sales Order has been approved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    XtraMessageBox.Show("Sales Order is approved already.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnApprove2_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                string status = gridView.GetFocusedRowCellValue("Status").Safe();

                db = new GreenChemDataContext();
                SalesOrder salesorder = db.SalesOrders.First(x => x.id == id);

                if (status == "OPEN" && salesorder.Approved == true && salesorder.Approved2 != true)
                {
                    DialogResult = XtraMessageBox.Show("Approve this Sales Order : " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (DialogResult == DialogResult.Yes)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        salesorder.Approved2 = true;
                        salesorder.ApprovedBy2 = frmUtama._iduseraccount;
                        salesorder.DateApproved2 = DateTime.Now;
                        db.SubmitChanges();
                        RefreshData();

                        XtraMessageBox.Show("Sales Order has been approved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (status == "OPEN" && salesorder.Approved != true)
                {
                    XtraMessageBox.Show("Sales Order is not approved by SE yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    XtraMessageBox.Show("Sales Order is approved already.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_salesorder.xml");

                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }
    }
}