﻿using System;
using System.Collections.Generic;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmViewPPB : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmViewPPB(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbProduct.DataSource = db.Products.GetNameAll();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            var ppb = db.PPBs.First(x => x.id == _id);
            txtTransactionNumber.Text = ppb.TransactionNumber;
            cmbDate.DateTime = ppb.TransactionDate;
            txtPONumber.Text = ppb.PONumber;
            cmbCustomer.EditValue = ppb.idCustomer;
            txtRemarks.Text = ppb.Remarks;

            //details
            var details = db.DetailPPBs.Where(x => x.idPPB == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.idProduct = item.idProduct;
                temp.Quantity = item.Quantity;
                temp.ProductName = item.ProductName;
                if (temp.ProductName.IsEmpty() && db.Products.Any(x => x.id == item.idProduct))
                    temp.ProductName = item.Product.Name;
                temp.UoM = item.UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                listdetail.Add(temp);
            }
            txtTotal.EditValue = listdetail.Sum(x => (decimal?)x.Subtotal).ToDecimal();

            gridControl.DataSource = listdetail;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}