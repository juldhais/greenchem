﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmSetDONumber : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmSetDONumber(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            var so = db.SalesOrders.First(x => x.id == _id);
            txtTransactionNumber.Text = so.TransactionNumber;
            txtCustomer.Text = so.Customer.Name;
            txtDONumber.Text = so.DONumber;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                SalesOrder so = db.SalesOrders.First(x => x.id == _id);
                so.DONumber = txtDONumber.Text;

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Error.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}