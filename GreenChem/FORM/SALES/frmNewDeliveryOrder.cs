﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewDeliveryOrder : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewDeliveryOrder()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);

            if (!db.CheckUserRole(frmUtama._iduseraccount.Value, "Allow Change Location"))
                cmbLocation.Enabled = false;

            if (!db.CheckUserRole(frmUtama._iduseraccount.Value, "Allow Change Status"))
                cmbStatus.Enabled = false;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                cmbNumber.Properties.DataSource = from x in db.SalesOrders
                                                    where x.Approved == true && x.Approved2 == true
                                                    && x.Status == "OPEN"
                                                    orderby x.id descending
                                                    select new
                                                    {
                                                        x.id,
                                                        x.TransactionNumber,
                                                        x.DateOrder,
                                                        Customer = x.Customer.Name,
                                                        x.TotalOrder
                                                    };

                cmbCustomer.Properties.DataSource = db.Customers.GetName();
                cmbProduct.DataSource = db.Products.GetName();
                cmbLocation.Properties.DataSource = db.Locations.GetName();
                cmbExpedition.Properties.DataSource = db.Couriers.GetName();

                GetTotal();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbNumber_EditValueChanged(object sender, EventArgs e)
        {
            if (cmbNumber.EditValue.IsNotEmpty())
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();

                    var salesorder = db.SalesOrders.First(x => x.id == cmbNumber.EditValue.ToInteger());

                    if (salesorder.Approved == false && salesorder.Status == "OPEN")
                    {
                        XtraMessageBox.Show("Sales Order is not approved yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        cmbNumber.EditValue = null;
                        return;
                    }

                    cmbStatus.Text = cmbStatus.Text;
                    cmbCustomer.EditValue = salesorder.idCustomer;
                    cmbLocation.EditValue = salesorder.idLocation;
                    txtRemarks.Text = salesorder.Remarks;
                    txtSubtotal.EditValue = salesorder.SubtotalOrder;
                    txtDiscount.EditValue = salesorder.DiscountOrder;
                    txtCost.EditValue = salesorder.CostOrder;
                    if (salesorder.SubtotalOrder != 0) txtTaxPercent.EditValue = (salesorder.TaxOrder / salesorder.SubtotalOrder) * 100;
                    txtTax.EditValue = salesorder.TaxOrder;
                    txtTotal.EditValue = salesorder.TotalOrder;

                    tempSalesOrderBindingSource.Clear();
                    foreach (var item in salesorder.DetailSalesOrders)
                    {
                        TempSalesOrder detail = new TempSalesOrder();
                        detail.id = item.id;
                        detail.idProduct = item.idProduct;
                        detail.QuantityOrder = item.QuantityOrder;
                        detail.QuantityDelivered = item.QuantityOrder;
                        detail.UoM = item.Product.UoM;
                        detail.PriceDelivered = item.PriceOrder;
                        detail.DiscountDelivered = item.DiscountOrder;
                        detail.SubtotalDelivered = item.SubtotalOrder;
                        detail.Packaging = item.Packaging;
                        tempSalesOrderBindingSource.Add(detail);
                    }

                    GetTotal();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void GetTotal()
        {
            try
            {
                var details = tempSalesOrderBindingSource.Cast<TempSalesOrder>();
                txtSubtotal.EditValue = details.Sum(x => x.SubtotalDelivered);
                decimal subtotaldiscount = txtSubtotal.EditValue.ToDecimal() - txtDiscount.EditValue.ToDecimal();
                txtTax.EditValue = subtotaldiscount * txtTaxPercent.EditValue.ToDecimal() / 100;
                txtTotal.EditValue = subtotaldiscount + txtTax.EditValue.ToDecimal() + txtCost.EditValue.ToDecimal();
            }
            catch { }
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                db = new GreenChemDataContext();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
                gridView.SetFocusedRowCellValue("PriceDelivered", product.SalesPrice);
            }
            else if (e.Column.FieldName == "QuantityDelivered" || e.Column.FieldName == "PriceDelivered" || e.Column.FieldName == "DiscountDelivered")
            {
                decimal quantity = gridView.GetFocusedRowCellValue("QuantityDelivered").ToDecimal();
                decimal price = gridView.GetFocusedRowCellValue("PriceDelivered").ToDecimal();
                decimal discount = gridView.GetFocusedRowCellValue("DiscountDelivered").ToDecimal();

                decimal subtotal = 0;
                if (discount < 100) subtotal = (quantity * price) * (100 - discount) / 100;
                else subtotal = quantity * (price - discount);

                gridView.SetFocusedRowCellValue("SubtotalDelivered", subtotal);
                GetTotal();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbNumber.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Transaction number can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbNumber.Focus();
                return;
            }

            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                SalesOrder salesorder = db.SalesOrders.First(x => x.id == cmbNumber.EditValue.ToInteger());
                salesorder.idUserAccount = frmUtama._iduseraccount;
                if (cmbLocation.EditValue.IsNotEmpty()) salesorder.idLocation = cmbLocation.EditValue.ToInteger();
                if (cmbExpedition.EditValue.IsNotEmpty()) salesorder.idCourier = cmbExpedition.EditValue.ToInteger();
                salesorder.DateDelivered = cmbDate.DateTime;
                salesorder.Status = cmbStatus.Text;
                salesorder.Remarks = txtRemarks.Text;
                salesorder.Remarks2 = txtRemarks2.Text;
                salesorder.SubtotalDelivered = txtSubtotal.EditValue.ToDecimal();
                salesorder.DiscountDelivered = txtDiscount.EditValue.ToDecimal();
                salesorder.CostDelivered = txtCost.EditValue.ToDecimal();
                salesorder.TaxDelivered = txtTax.EditValue.ToDecimal();
                salesorder.TotalDelivered = txtTotal.EditValue.ToDecimal();
                salesorder.Stock = cmbStock.Text;
                salesorder.FOB = txtFOB.Text;
                salesorder.ShipVia = txtShipVia.Text;

                foreach (TempSalesOrder item in tempSalesOrderBindingSource)
                {
                    if (item.idProduct == null) continue;

                    Product product = db.Products.First(x => x.id == item.idProduct);

                    DetailSalesOrder detail = salesorder.DetailSalesOrders.First(x => x.id == item.id);
                    detail.idProduct = item.idProduct;
                    detail.QuantityDelivered = item.QuantityDelivered;
                    detail.PriceDelivered = item.PriceDelivered;
                    detail.DiscountDelivered = item.DiscountDelivered;
                    detail.SubtotalDelivered = item.SubtotalDelivered;
                    detail.Packaging = item.Packaging;
                    detail.NoBatch = item.NoBatch;
                    detail.Cost = product.Cost;

                    product.TransitStock = product.TransitStock.ToDecimal() + item.QuantityDelivered;

                    if (cmbStock.Text == "JKT")
                    {
                        product.Stock -= item.QuantityDelivered;
                        if (product.Stock < 0)
                        {
                            XtraMessageBox.Show("Stock JKT Product : " + product.Name + " tidak mencukupi.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            gridControl.Focus();
                            return;
                        }
                    }
                    else if (cmbStock.Text == "BPP")
                    {
                        product.Stock2 = product.Stock2.ToDecimal() - item.QuantityDelivered;
                        if (product.Stock2 < 0)
                        {
                            XtraMessageBox.Show("Stock BPP Product : " + product.Name + " tidak mencukupi.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            gridControl.Focus();
                            return;
                        }
                    }
                    else if (cmbStock.Text == "BTG")
                    {
                        product.Stock3 = product.Stock3.ToDecimal() - item.QuantityDelivered;
                        if (product.Stock3 < 0)
                        {
                            XtraMessageBox.Show("Stock BTG Product : " + product.Name + " tidak mencukupi.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            gridControl.Focus();
                            return;
                        }
                    }
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //Printing.DeliveryOrder(salesorder.id);
                Close();

                try
                {
                    frmDeliveryOrder form = (frmDeliveryOrder)Application.OpenForms["frmDeliveryOrder"];
                    form.RefreshData();
                }
                catch { }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
                Close();
        }
    }
}