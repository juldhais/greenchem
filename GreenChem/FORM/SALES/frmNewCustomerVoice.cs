﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewCustomerVoice : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewCustomerVoice()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            txtTransactionNumber.Text = db.GetCustomerVoiceNumber();
            cmbDate.DateTime = DateTime.Now;
            //cmbCustomer.Properties.DataSource = db.Customers.GetName();

            //txtDepositionTo.Enabled = db.CheckUserRole(frmUtama._iduseraccount.Value, "Allow Edit Deposition");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCustomer.Focus();
                return;
            }
            if (txtDescription.Text.IsEmpty())
            {
                XtraMessageBox.Show("Description can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDescription.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                CustomerVoice voice = new CustomerVoice();
                //if (cmbCustomer.EditValue.IsNotEmpty()) voice.idCustomer = cmbCustomer.EditValue.ToInteger();
                voice.CustomerName = txtCustomer.Text;
                voice.CustomerPhone = txtPhone.Text;
                voice.CustomerFax = txtFax.Text;
                voice.CustomerEmail = txtEmail.Text;
                voice.idUserAccount = frmUtama._iduseraccount;
                voice.TransactionNumber = txtTransactionNumber.Text;
                voice.TransactionDate = cmbDate.DateTime;
                voice.ComplaintDescription = txtDescription.Text;
                voice.DepositionTo = txtDepositionTo.Text;
                if (cmbDueDate.EditValue.IsNotEmpty()) voice.DueDate = cmbDueDate.DateTime;
                voice.Category = txtCategory.Text;
                voice.Action = txtAction.Text;
                if (cmbClosingDate.EditValue.IsNotEmpty()) voice.ClosingDate = cmbClosingDate.DateTime;
                voice.Status = cmbStatus.Text;
                voice.CustomerSupport = cmbCustomerSupport.Text;
                voice.Time = cmbTime.Text;
                voice.Quality = cmbQuality.Text;
                voice.TechnicalSupport = cmbTechnicalSupport.Text;
                voice.Suggestion = txtSuggestion.Text;
                voice.PIC = txtPIC.Text;

                db.CustomerVoices.InsertOnSubmit(voice);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbCustomer_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                //db = new GreenChemDataContext();
                //var customer = db.Customers.First(x => x.id == cmbCustomer.EditValue.ToInteger());
                //txtPhone.Text = customer.Phone;
                //txtFax.Text = customer.Fax;
                //txtEmail.Text = customer.Email;
            }
            catch { }
        }
    }
}