﻿using System.Linq;

namespace GreenChem.MenuTransaction2
{
    public partial class frmViewProjectProgress : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmViewProjectProgress(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            var query = from x in db.ProjectProgresses
                        where x.idProjectList == _id
                        select new
                        {
                            x.id,
                            x.TransactionDate,
                            UserAccount = x.UserAccount.Name,
                            x.Description,
                            x.Status
                        };

            gridControl.DataSource = query;
        }
    }
}