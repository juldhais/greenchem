﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmPayInvoice : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        decimal _oldtotal = 0;
        GreenChemDataContext db;

        public frmPayInvoice(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            var salesorder = db.SalesOrders.First(x => x.id == _id);
            txtTransactionNumber.Text = salesorder.TransactionNumber;
            cmbDate.DateTime = DateTime.Now;
            txtCustomer.Text = salesorder.Customer.Name;
            txtRate.EditValue = salesorder.Rate;
            txtTotal.EditValue = salesorder.TotalDelivered;
            _oldtotal = salesorder.TotalDelivered;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                SalesOrder salesorder = db.SalesOrders.First(x => x.id == _id);
                salesorder.DatePaid = cmbDate.DateTime;
                salesorder.Status = "PAID";
                salesorder.Rate = txtRate.EditValue.ToDecimal();
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtRate_EditValueChanged(object sender, EventArgs e)
        {
            txtTotal.EditValue = _oldtotal * txtRate.EditValue.ToDecimal();
        }

    }
}