﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmChangeDeliveryStatus : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmChangeDeliveryStatus(int id)
        {
            InitializeComponent();
            _id = id;
            db = new GreenChemDataContext();
            var autonumber = db.AutoNumbers.First(x => x.id == _id);
            txtTransactionNumber.Text = autonumber.TransactionNumber;
            cmbDeliveryStatus.Text = autonumber.DeliveryStatus;
            cmbDateDelivered.EditValue = autonumber.DateDelivered;

            cmbDeliveryStatus_SelectedIndexChanged(null, null);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                AutoNumber autonumber = db.AutoNumbers.First(x => x.id == _id);
                autonumber.DeliveryStatus = cmbDeliveryStatus.Text;
                autonumber.DateDelivered = (DateTime?)cmbDateDelivered.EditValue;

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbDeliveryStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDeliveryStatus.Text == "RECEIVED") cmbDateDelivered.Enabled = true;
            else
            {
                cmbDateDelivered.EditValue = null;
                cmbDateDelivered.Enabled = false;
            }
        }
    }
}