﻿namespace GreenChem.MenuTransaction
{
    partial class frmSolveCustomerVoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txtPIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtSuggestion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTechnicalSupport = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbQuality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbTime = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.cmbCustomerSupport = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cmbClosingDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtAction = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtCategory = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDueDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtDepositionTo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtDescription = new DevExpress.XtraEditors.MemoEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTransactionNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustomer = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuggestion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTechnicalSupport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomerSupport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepositionTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(401, 594);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(295, 594);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.txtPIC);
            this.panelControl3.Controls.Add(this.labelControl14);
            this.panelControl3.Controls.Add(this.txtSuggestion);
            this.panelControl3.Controls.Add(this.labelControl15);
            this.panelControl3.Controls.Add(this.cmbTechnicalSupport);
            this.panelControl3.Controls.Add(this.cmbQuality);
            this.panelControl3.Controls.Add(this.cmbTime);
            this.panelControl3.Controls.Add(this.labelControl16);
            this.panelControl3.Controls.Add(this.labelControl17);
            this.panelControl3.Controls.Add(this.labelControl18);
            this.panelControl3.Controls.Add(this.cmbCustomerSupport);
            this.panelControl3.Controls.Add(this.labelControl19);
            this.panelControl3.Location = new System.Drawing.Point(12, 396);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(469, 192);
            this.panelControl3.TabIndex = 7;
            // 
            // txtPIC
            // 
            this.txtPIC.Enabled = false;
            this.txtPIC.Location = new System.Drawing.Point(79, 160);
            this.txtPIC.Name = "txtPIC";
            this.txtPIC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPIC.Properties.Appearance.Options.UseFont = true;
            this.txtPIC.Size = new System.Drawing.Size(381, 24);
            this.txtPIC.TabIndex = 15;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Enabled = false;
            this.labelControl14.Location = new System.Drawing.Point(55, 163);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(18, 17);
            this.labelControl14.TabIndex = 14;
            this.labelControl14.Text = "PIC";
            // 
            // txtSuggestion
            // 
            this.txtSuggestion.Enabled = false;
            this.txtSuggestion.Location = new System.Drawing.Point(79, 130);
            this.txtSuggestion.Name = "txtSuggestion";
            this.txtSuggestion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSuggestion.Properties.Appearance.Options.UseFont = true;
            this.txtSuggestion.Size = new System.Drawing.Size(381, 24);
            this.txtSuggestion.TabIndex = 13;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Enabled = false;
            this.labelControl15.Location = new System.Drawing.Point(8, 133);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(65, 17);
            this.labelControl15.TabIndex = 12;
            this.labelControl15.Text = "Suggestion";
            // 
            // cmbTechnicalSupport
            // 
            this.cmbTechnicalSupport.EditValue = "Sangat Baik";
            this.cmbTechnicalSupport.Enabled = false;
            this.cmbTechnicalSupport.Location = new System.Drawing.Point(102, 96);
            this.cmbTechnicalSupport.Name = "cmbTechnicalSupport";
            this.cmbTechnicalSupport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTechnicalSupport.Properties.Appearance.Options.UseFont = true;
            this.cmbTechnicalSupport.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTechnicalSupport.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbTechnicalSupport.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTechnicalSupport.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbTechnicalSupport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTechnicalSupport.Size = new System.Drawing.Size(175, 23);
            this.cmbTechnicalSupport.TabIndex = 7;
            // 
            // cmbQuality
            // 
            this.cmbQuality.EditValue = "Sangat Baik";
            this.cmbQuality.Enabled = false;
            this.cmbQuality.Location = new System.Drawing.Point(102, 67);
            this.cmbQuality.Name = "cmbQuality";
            this.cmbQuality.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbQuality.Properties.Appearance.Options.UseFont = true;
            this.cmbQuality.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbQuality.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbQuality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbQuality.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbQuality.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbQuality.Size = new System.Drawing.Size(175, 23);
            this.cmbQuality.TabIndex = 5;
            // 
            // cmbTime
            // 
            this.cmbTime.EditValue = "Sangat Baik";
            this.cmbTime.Enabled = false;
            this.cmbTime.Location = new System.Drawing.Point(102, 38);
            this.cmbTime.Name = "cmbTime";
            this.cmbTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTime.Properties.Appearance.Options.UseFont = true;
            this.cmbTime.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTime.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTime.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbTime.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTime.Size = new System.Drawing.Size(175, 23);
            this.cmbTime.TabIndex = 3;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Enabled = false;
            this.labelControl16.Location = new System.Drawing.Point(15, 98);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(81, 17);
            this.labelControl16.TabIndex = 6;
            this.labelControl16.Text = "Tech. Support";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Enabled = false;
            this.labelControl17.Location = new System.Drawing.Point(56, 69);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(40, 17);
            this.labelControl17.TabIndex = 4;
            this.labelControl17.Text = "Quality";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Enabled = false;
            this.labelControl18.Location = new System.Drawing.Point(68, 40);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(28, 17);
            this.labelControl18.TabIndex = 2;
            this.labelControl18.Text = "Time";
            // 
            // cmbCustomerSupport
            // 
            this.cmbCustomerSupport.EditValue = "Sangat Baik";
            this.cmbCustomerSupport.Enabled = false;
            this.cmbCustomerSupport.Location = new System.Drawing.Point(102, 9);
            this.cmbCustomerSupport.Name = "cmbCustomerSupport";
            this.cmbCustomerSupport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbCustomerSupport.Properties.Appearance.Options.UseFont = true;
            this.cmbCustomerSupport.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbCustomerSupport.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbCustomerSupport.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCustomerSupport.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbCustomerSupport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbCustomerSupport.Size = new System.Drawing.Size(175, 23);
            this.cmbCustomerSupport.TabIndex = 1;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Enabled = false;
            this.labelControl19.Location = new System.Drawing.Point(17, 11);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(79, 17);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "Cust. Support";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.cmbStatus);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.cmbClosingDate);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.txtAction);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.txtCategory);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.cmbDueDate);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.txtDepositionTo);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.txtDescription);
            this.panelControl2.Location = new System.Drawing.Point(12, 175);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(470, 215);
            this.panelControl2.TabIndex = 6;
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "Closed";
            this.cmbStatus.Enabled = false;
            this.cmbStatus.Location = new System.Drawing.Point(315, 184);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "Open",
            "Closed"});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(150, 23);
            this.cmbStatus.TabIndex = 17;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(274, 186);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(35, 17);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Status";
            // 
            // cmbClosingDate
            // 
            this.cmbClosingDate.EditValue = null;
            this.cmbClosingDate.Location = new System.Drawing.Point(95, 183);
            this.cmbClosingDate.Name = "cmbClosingDate";
            this.cmbClosingDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbClosingDate.Properties.Appearance.Options.UseFont = true;
            this.cmbClosingDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbClosingDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbClosingDate.Size = new System.Drawing.Size(150, 24);
            this.cmbClosingDate.TabIndex = 15;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Enabled = false;
            this.labelControl7.Location = new System.Drawing.Point(15, 186);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(74, 17);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "Closing Date";
            // 
            // txtAction
            // 
            this.txtAction.Location = new System.Drawing.Point(95, 153);
            this.txtAction.Name = "txtAction";
            this.txtAction.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAction.Properties.Appearance.Options.UseFont = true;
            this.txtAction.Size = new System.Drawing.Size(370, 24);
            this.txtAction.TabIndex = 13;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Enabled = false;
            this.labelControl8.Location = new System.Drawing.Point(53, 156);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(36, 17);
            this.labelControl8.TabIndex = 12;
            this.labelControl8.Text = "Action";
            // 
            // txtCategory
            // 
            this.txtCategory.Enabled = false;
            this.txtCategory.Location = new System.Drawing.Point(95, 123);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCategory.Properties.Appearance.Options.UseFont = true;
            this.txtCategory.Size = new System.Drawing.Size(370, 24);
            this.txtCategory.TabIndex = 11;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Enabled = false;
            this.labelControl10.Location = new System.Drawing.Point(36, 126);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(53, 17);
            this.labelControl10.TabIndex = 10;
            this.labelControl10.Text = "Category";
            // 
            // cmbDueDate
            // 
            this.cmbDueDate.EditValue = null;
            this.cmbDueDate.Enabled = false;
            this.cmbDueDate.Location = new System.Drawing.Point(95, 93);
            this.cmbDueDate.Name = "cmbDueDate";
            this.cmbDueDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDueDate.Properties.Appearance.Options.UseFont = true;
            this.cmbDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDueDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDueDate.Size = new System.Drawing.Size(150, 24);
            this.cmbDueDate.TabIndex = 9;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Enabled = false;
            this.labelControl11.Location = new System.Drawing.Point(35, 96);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(54, 17);
            this.labelControl11.TabIndex = 8;
            this.labelControl11.Text = "Due Date";
            // 
            // txtDepositionTo
            // 
            this.txtDepositionTo.Enabled = false;
            this.txtDepositionTo.Location = new System.Drawing.Point(95, 63);
            this.txtDepositionTo.Name = "txtDepositionTo";
            this.txtDepositionTo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDepositionTo.Properties.Appearance.Options.UseFont = true;
            this.txtDepositionTo.Size = new System.Drawing.Size(370, 24);
            this.txtDepositionTo.TabIndex = 7;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Enabled = false;
            this.labelControl12.Location = new System.Drawing.Point(9, 66);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(82, 17);
            this.labelControl12.TabIndex = 6;
            this.labelControl12.Text = "Deposition To";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Enabled = false;
            this.labelControl13.Location = new System.Drawing.Point(23, 13);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(66, 17);
            this.labelControl13.TabIndex = 4;
            this.labelControl13.Text = "Description";
            // 
            // txtDescription
            // 
            this.txtDescription.Enabled = false;
            this.txtDescription.Location = new System.Drawing.Point(95, 10);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtDescription.Properties.Appearance.Options.UseFont = true;
            this.txtDescription.Size = new System.Drawing.Size(370, 47);
            this.txtDescription.TabIndex = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtCustomer);
            this.panelControl1.Controls.Add(this.txtEmail);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtFax);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtPhone);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.cmbDate);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtTransactionNumber);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 157);
            this.panelControl1.TabIndex = 5;
            // 
            // txtEmail
            // 
            this.txtEmail.Enabled = false;
            this.txtEmail.Location = new System.Drawing.Point(80, 127);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(381, 24);
            this.txtEmail.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Enabled = false;
            this.labelControl6.Location = new System.Drawing.Point(43, 130);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(31, 17);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Email";
            // 
            // txtFax
            // 
            this.txtFax.Enabled = false;
            this.txtFax.Location = new System.Drawing.Point(80, 97);
            this.txtFax.Name = "txtFax";
            this.txtFax.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFax.Properties.Appearance.Options.UseFont = true;
            this.txtFax.Size = new System.Drawing.Size(381, 24);
            this.txtFax.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Enabled = false;
            this.labelControl3.Location = new System.Drawing.Point(55, 100);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(19, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Fax";
            // 
            // txtPhone
            // 
            this.txtPhone.Enabled = false;
            this.txtPhone.Location = new System.Drawing.Point(80, 67);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhone.Properties.Appearance.Options.UseFont = true;
            this.txtPhone.Size = new System.Drawing.Size(381, 24);
            this.txtPhone.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Enabled = false;
            this.labelControl4.Location = new System.Drawing.Point(38, 70);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 17);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Phone";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Enabled = false;
            this.labelControl5.Location = new System.Drawing.Point(18, 40);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(56, 17);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Customer";
            // 
            // cmbDate
            // 
            this.cmbDate.EditValue = null;
            this.cmbDate.Enabled = false;
            this.cmbDate.Location = new System.Drawing.Point(311, 7);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDate.Properties.Appearance.Options.UseFont = true;
            this.cmbDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDate.Size = new System.Drawing.Size(150, 24);
            this.cmbDate.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Enabled = false;
            this.labelControl1.Location = new System.Drawing.Point(278, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 17);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Date";
            // 
            // txtTransactionNumber
            // 
            this.txtTransactionNumber.Enabled = false;
            this.txtTransactionNumber.Location = new System.Drawing.Point(80, 7);
            this.txtTransactionNumber.Name = "txtTransactionNumber";
            this.txtTransactionNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTransactionNumber.Properties.Appearance.Options.UseFont = true;
            this.txtTransactionNumber.Size = new System.Drawing.Size(180, 24);
            this.txtTransactionNumber.TabIndex = 0;
            this.txtTransactionNumber.TabStop = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Enabled = false;
            this.labelControl2.Location = new System.Drawing.Point(53, 10);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(21, 17);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "No.";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Enabled = false;
            this.txtCustomer.Location = new System.Drawing.Point(80, 37);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCustomer.Properties.Appearance.Options.UseFont = true;
            this.txtCustomer.Size = new System.Drawing.Size(381, 24);
            this.txtCustomer.TabIndex = 2;
            // 
            // frmSolveCustomerVoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 632);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSolveCustomerVoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solve Customer Voice";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuggestion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTechnicalSupport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomerSupport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbClosingDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepositionTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomer.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.TextEdit txtPIC;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtSuggestion;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTechnicalSupport;
        private DevExpress.XtraEditors.ComboBoxEdit cmbQuality;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTime;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.ComboBoxEdit cmbCustomerSupport;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.DateEdit cmbClosingDate;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtAction;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtCategory;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.DateEdit cmbDueDate;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtDepositionTo;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.MemoEdit txtDescription;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtPhone;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.DateEdit cmbDate;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtTransactionNumber;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtCustomer;
    }
}