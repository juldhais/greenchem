﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmInquiryIn : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmInquiryIn()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Inquiry In");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Inquiry In");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Inquiry In");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.InquiryIns
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Customer = x.Customer.Name,
                                Department = x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                x.Remarks
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewInquiryIn form = new frmNewInquiryIn();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditInquiryIn form = new frmEditInquiryIn(id);
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                InquiryIn inquiry = db.InquiryIns.First(x => x.id == id);
                db.InquiryIns.DeleteOnSubmit(inquiry);
                db.SubmitChanges();
                RefreshData();
                XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int id = gridView.GetFocusedRowCellValue("id").ToInteger();
            frmEditInquiryIn form = new frmEditInquiryIn(id);
            form.btnSave.Enabled = false;
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }
    }
}