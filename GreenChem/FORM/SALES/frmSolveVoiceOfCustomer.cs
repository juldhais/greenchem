﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem
{
    public partial class frmSolveVoiceOfCustomer : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmSolveVoiceOfCustomer(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();

            var complaint = db.CustomerComplaints.First(x => x.id == _id);
            cmbDate.DateTime = complaint.TransactionDate;
            cmbCustomer.EditValue = complaint.idCustomer;
            txtDescription.Text = complaint.ComplaintDescription;
            txtDepositionTo.Text = complaint.DepositionTo;
            cmbDueDate.EditValue = complaint.DueDate;
            txtCategory.Text = complaint.Category;
            txtAction.Text = complaint.Action;
            cmbClosingDate.EditValue = DateTime.Now;
            cmbStatus.Text = "Closed";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                CustomerComplaint voice = db.CustomerComplaints.First(x => x.id == _id);
                voice.Action = txtAction.Text;
                voice.ClosingDate = cmbClosingDate.DateTime;
                voice.Status = cmbStatus.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}