﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Linq;
using System.Collections.Generic;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewQuotationOut : DevExpress.XtraEditors.XtraForm
    {
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmNewQuotationOut()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            RefreshData();
        }

        private void RefreshData()
        {
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            txtTransactionNumber.Text = db.GetQuotationOutNumber();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (txtRemarks.Text.IsEmpty())
            {
                XtraMessageBox.Show("Remarks can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (db.QuotationOuts.Any(x => x.TransactionNumber == txtTransactionNumber.Text))
                {
                    XtraMessageBox.Show("Transaction Number is already exist. System will change the number to the newest number.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTransactionNumber.Text = db.GetQuotationOutNumber();
                    txtTransactionNumber.Focus();
                    return;
                }

                QuotationOut quotation = new QuotationOut();
                if (frmUtama._iddepartment != null) quotation.idDepartment = frmUtama._iddepartment;
                if (frmUtama._idsubdepartment != null) quotation.idSubDepartment = frmUtama._idsubdepartment;
                if (cmbCustomer.EditValue.IsNotEmpty()) quotation.idCustomer = cmbCustomer.EditValue.ToInteger();
                quotation.TransactionNumber = txtTransactionNumber.Text;
                quotation.TransactionDate = cmbDate.DateTime;
                quotation.idUserAccount = frmUtama._iduseraccount;
                quotation.Remarks = txtRemarks.Text;
                quotation.CustomerText = txtCustomer.Text;

                db.QuotationOuts.InsertOnSubmit(quotation);
                db.SubmitChanges();


                //details
                foreach (var item in listdetail)
                {
                    if (item.Quantity != 0)
                    {
                        DetailQuotationOut detail = new DetailQuotationOut();
                        detail.idProduct = item.idProduct;
                        detail.idQuotationOut = quotation.id;
                        detail.Quantity = item.Quantity;
                        detail.Price = item.Price;
                        detail.Discount = item.Discount;
                        detail.Subtotal = item.Subtotal;
                        db.DetailQuotationOuts.InsertOnSubmit(detail);
                    }
                }
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmDetailQuotationOut form = new frmDetailQuotationOut(listdetail);
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }
    }
}