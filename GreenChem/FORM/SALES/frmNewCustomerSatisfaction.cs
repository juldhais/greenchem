﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewCustomerSatisfaction : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewCustomerSatisfaction()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbDate.DateTime = DateTime.Now;
            txtTransactionNumber.Text = db.GetCustomerSatisfactionNumber();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                CustomerSatisfaction cs = new CustomerSatisfaction();
                cs.idCustomer = cmbCustomer.EditValue.ToInteger();
                cs.TransactionNumber = txtTransactionNumber.Text;
                cs.TransactionDate = cmbDate.DateTime;
                cs.CustomerSupport = cmbCustomerSupport.Text;
                cs.Time = cmbTime.Text;
                cs.Quality = cmbQuality.Text;
                cs.TechnicalSupport = cmbTechnicalSupport.Text;
                cs.Suggestion = txtSuggestion.Text;
                cs.PIC = txtPIC.Text;
                db.CustomerSatisfactions.InsertOnSubmit(cs);

                Customer customer = db.Customers.First(x => x.id == cs.idCustomer);
                customer.Phone = txtPhone.Text;
                customer.Fax = txtFax.Text;
                customer.Email = txtEmail.Text;

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbCustomer_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                var customer = db.Customers.First(x => x.id == cmbCustomer.EditValue.ToInteger());
                txtPhone.Text = customer.Phone;
                txtFax.Text = customer.Fax;
                txtEmail.Text = customer.Email;
            }
            catch { }
        }

        private void cmbDate_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                txtTransactionNumber.Text = db.GetCustomerSatisfactionNumber();
            }
            catch { }
        }
    }
}