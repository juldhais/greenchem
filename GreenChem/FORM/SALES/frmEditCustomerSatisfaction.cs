﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditCustomerSatisfaction : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditCustomerSatisfaction(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            var cs = db.CustomerSatisfactions.First(x => x.id == _id);
            txtTransactionNumber.Text = cs.TransactionNumber;
            cmbDate.DateTime = cs.TransactionDate;
            cmbCustomer.EditValue = cs.idCustomer;
            txtPhone.Text = cs.Customer.Phone;
            txtFax.Text = cs.Customer.Fax;
            txtEmail.Text = cs.Customer.Email;
            txtSuggestion.Text = cs.Suggestion;
            txtPIC.Text = cs.PIC;

            cmbCustomerSupport.Text = cs.CustomerSupport;
            cmbTime.Text = cs.Time;
            cmbQuality.Text = cs.Quality;
            cmbTechnicalSupport.Text = cs.TechnicalSupport;
        }

        private void cmbCustomer_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                var customer = db.Customers.First(x => x.id == cmbCustomer.EditValue.ToInteger());
                txtPhone.Text = customer.Phone;
                txtFax.Text = customer.Fax;
                txtEmail.Text = customer.Email;
            }
            catch { }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                CustomerSatisfaction cs = db.CustomerSatisfactions.First(x => x.id == _id);
                cs.idCustomer = cmbCustomer.EditValue.ToInteger();
                cs.TransactionNumber = txtTransactionNumber.Text;
                cs.TransactionDate = cmbDate.DateTime;
                cs.CustomerSupport = cmbCustomerSupport.Text;
                cs.Time = cmbTime.Text;
                cs.Quality = cmbQuality.Text;
                cs.TechnicalSupport = cmbTechnicalSupport.Text;
                cs.Suggestion = txtSuggestion.Text;
                cs.PIC = txtPIC.Text;

                Customer customer = db.Customers.First(x => x.id == cs.idCustomer);
                customer.Phone = txtPhone.Text;
                customer.Fax = txtFax.Text;
                customer.Email = txtEmail.Text;

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}