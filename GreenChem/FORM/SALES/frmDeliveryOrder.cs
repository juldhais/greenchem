﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Drawing;

namespace GreenChem.MenuTransaction
{
    public partial class frmDeliveryOrder : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDeliveryOrder()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Now.AddDays(-365);
            cmbDateEnd.DateTime = DateTime.Now;
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Delivery Order");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Delivery Order");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Delivery Order");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Delivery Order");
            btnSetLocation.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Set Location Delivery Order");
            btnSetDONumber.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Set DO Number");
        }

        public void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.SalesOrders
                            where x.DateDelivered != null && x.DateDelivered.Value.Date >= cmbDateStart.DateTime.Date && x.DateDelivered.Value.Date <= cmbDateEnd.DateTime.Date
                            && (x.Status == "DELIVERED" || x.Status == "ON-PROGRESS" || x.Status == "TRANSITED" || x.Status == "INVOICED" || x.Status == "PAID")
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.DONumber,
                                x.DateDelivered,
                                x.ChangeDODate,
                                x.Status,
                                Customer = x.Customer.Name,
                                x.Remarks,
                                Location = x.Location.Name,
                                x.Remarks2,
                                x.DueDate,
                                Difference = x.DueDate.HasValue && x.ChangeDODate.HasValue ? (x.DueDate.Value - x.ChangeDODate.Value).Days : 0
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewDeliveryOrder form = new frmNewDeliveryOrder();
            form.MdiParent = Application.OpenForms["frmUtama"];
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditDeliveryOrder form = new frmEditDeliveryOrder(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.MdiParent = Application.OpenForms["frmUtama"];
                form.Show();
                form.BringToFront();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();

                    //repair stock
                    var details = from x in db.DetailSalesOrders
                                  where x.idSalesOrder == id
                                  select new
                                  {
                                      x.SalesOrder.Stock,
                                      x.idProduct,
                                      x.QuantityDelivered,
                                  };

                    foreach (var item in details)
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        if (item.Stock == "JKT")
                            product.Stock += item.QuantityDelivered;
                        else if (item.Stock == "BPP")
                            product.Stock2 = product.Stock2.ToDecimal() + item.QuantityDelivered;
                    }
                    db.SubmitChanges();

                    db.ExecuteCommand("DELETE FROM dbo.DetailSalesOrder WHERE idDeliveryOrder = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.SalesOrder WHERE id = " + id);

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RefreshData();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                Printing.DeliveryOrder(id);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnSetLocation_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                frmSetLocation form = new frmSetLocation(id, "Set Location for Order # " + number);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            int diff = gridView.GetRowCellValue(e.RowHandle, "Difference").ToInteger();
            if (diff < 0)
                e.Appearance.ForeColor = Color.Red;
            else if (diff > 0) e.Appearance.ForeColor = Color.Blue;
        }

        private void btnSetDONumber_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string donumber = gridView.GetFocusedRowCellValue("DONumber").Safe();
                if (donumber.IsNotEmpty())
                {
                    XtraMessageBox.Show("DO Number is already set.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmSetDONumber form = new frmSetDONumber(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmEditDeliveryOrder form = new frmEditDeliveryOrder(gridView.GetFocusedRowCellValue("id").ToInteger());
                form.MdiParent = Application.OpenForms["frmUtama"];
                form.Show();
                form.BringToFront();
                form.btnSave.Visible = false;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}