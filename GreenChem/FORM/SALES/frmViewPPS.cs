﻿using System;
using System.Collections.Generic;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmViewPPS : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        List<TempDetail> listdetail = new List<TempDetail>();
        GreenChemDataContext db;

        public frmViewPPS(int id)
        {
            InitializeComponent();

            _id = id;
            db = new GreenChemDataContext();
            cmbProduct.DataSource = db.Products.GetNameAll();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            var pps = db.PPS.First(x => x.id == _id);
            txtTransactionNumber.Text = pps.TransactionNumber;
            cmbDate.DateTime = pps.TransactionDate;
            txtDivision.Text = pps.Division;
            cmbCustomer.EditValue = pps.idCustomer;
            txtRemarks.Text = pps.Remarks;
            cmbTanggalPenerimaan.EditValue = pps.TanggalPenerimaan;

            //details
            var details = db.DetailPPS.Where(x => x.idPPS == _id);
            foreach (var item in details)
            {
                TempDetail temp = new TempDetail();
                temp.idProduct = item.idProduct;
                temp.ProductName = item.ProductName;
                if (temp.ProductName.IsEmpty() && db.Products.Any(x => x.id == item.idProduct))
                    temp.ProductName = item.Product.Name;
                temp.Quantity = item.Quantity;
                temp.UoM = item.UoM;
                temp.Price = item.Price;
                temp.Discount = item.Discount;
                temp.Subtotal = item.Subtotal;
                listdetail.Add(temp);
            }
            txtTotal.EditValue = listdetail.Sum(x => (decimal?)x.Subtotal).ToDecimal();

            gridControl.DataSource = listdetail;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}