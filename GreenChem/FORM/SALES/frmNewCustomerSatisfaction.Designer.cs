﻿namespace GreenChem.MenuTransaction
{
    partial class frmNewCustomerSatisfaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbCustomer = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTransactionNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtSuggestion = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.cmbTechnicalSupport = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbQuality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbTime = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbCustomerSupport = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtPIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuggestion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTechnicalSupport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomerSupport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtEmail);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtFax);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtPhone);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.cmbCustomer);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.cmbDate);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtTransactionNumber);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 175);
            this.panelControl1.TabIndex = 0;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(80, 136);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(381, 24);
            this.txtEmail.TabIndex = 11;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(43, 139);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(31, 17);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Email";
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(80, 106);
            this.txtFax.Name = "txtFax";
            this.txtFax.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFax.Properties.Appearance.Options.UseFont = true;
            this.txtFax.Size = new System.Drawing.Size(381, 24);
            this.txtFax.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(55, 109);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(19, 17);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Fax";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(80, 76);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhone.Properties.Appearance.Options.UseFont = true;
            this.txtPhone.Size = new System.Drawing.Size(381, 24);
            this.txtPhone.TabIndex = 7;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(38, 79);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 17);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Phone";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.EditValue = "";
            this.cmbCustomer.Location = new System.Drawing.Point(80, 46);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbCustomer.Properties.Appearance.Options.UseFont = true;
            this.cmbCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCustomer.Properties.DisplayMember = "Name";
            this.cmbCustomer.Properties.NullText = "";
            this.cmbCustomer.Properties.ValueMember = "id";
            this.cmbCustomer.Properties.View = this.searchLookUpEdit1View;
            this.cmbCustomer.Size = new System.Drawing.Size(381, 24);
            this.cmbCustomer.TabIndex = 5;
            this.cmbCustomer.EditValueChanged += new System.EventHandler(this.cmbCustomer_EditValueChanged);
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseFont = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.searchLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.searchLookUpEdit1View.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.searchLookUpEdit1View.Appearance.Row.Options.UseFont = true;
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.searchLookUpEdit1View.RowHeight = 23;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "id";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Code";
            this.gridColumn5.FieldName = "Code";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 120;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Name";
            this.gridColumn6.FieldName = "Name";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 264;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(18, 49);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(56, 17);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Customer";
            // 
            // cmbDate
            // 
            this.cmbDate.EditValue = null;
            this.cmbDate.Location = new System.Drawing.Point(311, 16);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbDate.Properties.Appearance.Options.UseFont = true;
            this.cmbDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbDate.Size = new System.Drawing.Size(150, 24);
            this.cmbDate.TabIndex = 3;
            this.cmbDate.EditValueChanged += new System.EventHandler(this.cmbDate_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(278, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 17);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Date";
            // 
            // txtTransactionNumber
            // 
            this.txtTransactionNumber.Location = new System.Drawing.Point(80, 16);
            this.txtTransactionNumber.Name = "txtTransactionNumber";
            this.txtTransactionNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTransactionNumber.Properties.Appearance.Options.UseFont = true;
            this.txtTransactionNumber.Size = new System.Drawing.Size(180, 24);
            this.txtTransactionNumber.TabIndex = 1;
            this.txtTransactionNumber.TabStop = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(53, 19);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(21, 17);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "No.";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(17, 12);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(79, 17);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Cust. Support";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.txtPIC);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.txtSuggestion);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.cmbTechnicalSupport);
            this.panelControl2.Controls.Add(this.cmbQuality);
            this.panelControl2.Controls.Add(this.cmbTime);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.cmbCustomerSupport);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Location = new System.Drawing.Point(13, 193);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(469, 201);
            this.panelControl2.TabIndex = 1;
            // 
            // txtSuggestion
            // 
            this.txtSuggestion.Location = new System.Drawing.Point(79, 131);
            this.txtSuggestion.Name = "txtSuggestion";
            this.txtSuggestion.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSuggestion.Properties.Appearance.Options.UseFont = true;
            this.txtSuggestion.Size = new System.Drawing.Size(381, 24);
            this.txtSuggestion.TabIndex = 13;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(8, 134);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(65, 17);
            this.labelControl11.TabIndex = 12;
            this.labelControl11.Text = "Suggestion";
            // 
            // cmbTechnicalSupport
            // 
            this.cmbTechnicalSupport.EditValue = "Sangat Baik";
            this.cmbTechnicalSupport.Location = new System.Drawing.Point(102, 97);
            this.cmbTechnicalSupport.Name = "cmbTechnicalSupport";
            this.cmbTechnicalSupport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTechnicalSupport.Properties.Appearance.Options.UseFont = true;
            this.cmbTechnicalSupport.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTechnicalSupport.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbTechnicalSupport.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTechnicalSupport.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbTechnicalSupport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTechnicalSupport.Size = new System.Drawing.Size(175, 23);
            this.cmbTechnicalSupport.TabIndex = 7;
            // 
            // cmbQuality
            // 
            this.cmbQuality.EditValue = "Sangat Baik";
            this.cmbQuality.Location = new System.Drawing.Point(102, 68);
            this.cmbQuality.Name = "cmbQuality";
            this.cmbQuality.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbQuality.Properties.Appearance.Options.UseFont = true;
            this.cmbQuality.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbQuality.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbQuality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbQuality.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbQuality.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbQuality.Size = new System.Drawing.Size(175, 23);
            this.cmbQuality.TabIndex = 5;
            // 
            // cmbTime
            // 
            this.cmbTime.EditValue = "Sangat Baik";
            this.cmbTime.Location = new System.Drawing.Point(102, 39);
            this.cmbTime.Name = "cmbTime";
            this.cmbTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTime.Properties.Appearance.Options.UseFont = true;
            this.cmbTime.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbTime.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbTime.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbTime.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbTime.Size = new System.Drawing.Size(175, 23);
            this.cmbTime.TabIndex = 3;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(15, 99);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(81, 17);
            this.labelControl10.TabIndex = 6;
            this.labelControl10.Text = "Tech. Support";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(56, 70);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(40, 17);
            this.labelControl9.TabIndex = 4;
            this.labelControl9.Text = "Quality";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(68, 41);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(28, 17);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "Time";
            // 
            // cmbCustomerSupport
            // 
            this.cmbCustomerSupport.EditValue = "Sangat Baik";
            this.cmbCustomerSupport.Location = new System.Drawing.Point(102, 10);
            this.cmbCustomerSupport.Name = "cmbCustomerSupport";
            this.cmbCustomerSupport.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbCustomerSupport.Properties.Appearance.Options.UseFont = true;
            this.cmbCustomerSupport.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbCustomerSupport.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbCustomerSupport.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCustomerSupport.Properties.Items.AddRange(new object[] {
            "Sangat Baik",
            "Baik",
            "Cukup",
            "Kurang",
            "Buruk"});
            this.cmbCustomerSupport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbCustomerSupport.Size = new System.Drawing.Size(175, 23);
            this.cmbCustomerSupport.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(402, 400);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(296, 400);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPIC
            // 
            this.txtPIC.Location = new System.Drawing.Point(79, 161);
            this.txtPIC.Name = "txtPIC";
            this.txtPIC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPIC.Properties.Appearance.Options.UseFont = true;
            this.txtPIC.Size = new System.Drawing.Size(381, 24);
            this.txtPIC.TabIndex = 15;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(55, 164);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(18, 17);
            this.labelControl12.TabIndex = 14;
            this.labelControl12.Text = "PIC";
            // 
            // frmNewCustomerSatisfaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 442);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewCustomerSatisfaction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Customer Satisfaction";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuggestion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTechnicalSupport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCustomerSupport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPIC.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit cmbDate;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtTransactionNumber;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbCustomer;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtPhone;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbCustomerSupport;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTechnicalSupport;
        private DevExpress.XtraEditors.ComboBoxEdit cmbQuality;
        private DevExpress.XtraEditors.ComboBoxEdit cmbTime;
        private DevExpress.XtraEditors.TextEdit txtSuggestion;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtPIC;
        private DevExpress.XtraEditors.LabelControl labelControl12;
    }
}