﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewSalesOrder : DevExpress.XtraEditors.XtraForm
    {
        string _pricelevel = "1";
        GreenChemDataContext db;

        public frmNewSalesOrder()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);

            cmbCurrency.EditValue = 1;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbCustomer.Properties.DataSource = db.Customers.GetName();
                cmbSalesman.Properties.DataSource = db.UserAccounts.GetName();
                cmbCurrency.Properties.DataSource = db.Currencies.GetName();
                cmbLocation.Properties.DataSource = db.Locations.GetName();
                cmbProduct.DataSource = db.Products.GetName();
                txtTransactionNumber.Text = db.GetSalesOrderNumber();
                cmbTerms.Properties.DataSource = db.Terms.GetName();

                cmbPackaging.Items.Clear();
                foreach (var item in db.Packagings) cmbPackaging.Items.Add(item.Name);

                GetTotal();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void GetTotal()
        {
            try
            {
                var details = tempSalesOrderBindingSource.Cast<TempSalesOrder>();
                txtSubtotal.EditValue = details.Sum(x => x.SubtotalOrder);
                decimal subtotaldiscount = txtSubtotal.EditValue.ToDecimal() - txtDiscount.EditValue.ToDecimal();
                txtTax.EditValue = subtotaldiscount * txtTaxPercent.EditValue.ToDecimal() / 100;
                txtTotal.EditValue = subtotaldiscount + txtTax.EditValue.ToDecimal() + txtCost.EditValue.ToDecimal();
            }
            catch { }
        }

        private void GetPrice()
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                db = new GreenChemDataContext();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
                decimal? price = product.SalesPrice;

                if (_pricelevel == "5") price = product.SalesPrice5;
                else if (_pricelevel == "4") price = product.SalesPrice4;
                else if (_pricelevel == "3") price = product.SalesPrice3;
                else if (_pricelevel == "2") price = product.SalesPrice2;
                else price = product.SalesPrice;

                if (cmbCurrency.EditValue.IsNotEmpty() && product.idCurrency.IsNotEmpty() && product.idCurrency != cmbCurrency.EditValue.ToInteger())
                {
                    var productcurrency = db.Currencies.First(x => x.id == product.idCurrency);
                    var salescurrency = db.Currencies.First(x => x.id == cmbCurrency.EditValue.ToInteger());
                    price = price * productcurrency.Rate / salescurrency.Rate;
                }

                gridView.SetFocusedRowCellValue("PriceOrder", price);
            }
            catch { }
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                GetPrice();
            }
            else if (e.Column.FieldName == "QuantityOrder" || e.Column.FieldName == "PriceOrder" || e.Column.FieldName == "DiscountOrder")
            {
                decimal quantity = gridView.GetFocusedRowCellValue("QuantityOrder").ToDecimal();
                decimal price = gridView.GetFocusedRowCellValue("PriceOrder").ToDecimal();
                decimal discount = gridView.GetFocusedRowCellValue("DiscountOrder").ToDecimal();
                
                decimal subtotal = 0;
                if (discount < 100) subtotal = (quantity * price) * (100 - discount) / 100;
                else subtotal = quantity * (price - discount);

                gridView.SetFocusedRowCellValue("SubtotalOrder", subtotal);
                GetTotal();
            }
            else if (e.Column.FieldName == "PriceLevel" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                _pricelevel = gridView.GetFocusedRowCellValue("PriceLevel").Safe();
                GetPrice();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (cmbDueDate.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Due Date can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbDueDate.Focus();
                return;
            }
            if (cmbCurrency.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Currency can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCurrency.Focus();
                return;
            }
            if (txtRemarks.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("PO Number can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }
            if (cmbTerms.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Terms can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbTerms.Focus();
                return;
            }
            if (tempSalesOrderBindingSource.Count == 0)
            {
                XtraMessageBox.Show("Product can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                gridControl.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (db.SalesOrders.Any(x => x.TransactionNumber == txtTransactionNumber.Text))
                {
                    XtraMessageBox.Show("Transaction number is already exist. System will change the number to the newest number.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTransactionNumber.Text = db.GetSalesOrderNumber();
                    txtTransactionNumber.Focus();
                    return;
                }

                SalesOrder salesorder = new SalesOrder();
                salesorder.idCustomer = cmbCustomer.EditValue.ToInteger();
                if (cmbCurrency.EditValue.IsNotEmpty())
                {
                    salesorder.idCurrency = cmbCurrency.EditValue.ToInteger();
                    salesorder.Rate = db.Currencies.First(x => x.id == salesorder.idCurrency).Rate;
                }
                else salesorder.Rate = 1;
                if (cmbLocation.EditValue.IsNotEmpty()) salesorder.idLocation = cmbLocation.EditValue.ToInteger();
                salesorder.idUserAccount = frmUtama._iduseraccount;
                if (cmbSalesman.EditValue.IsNotEmpty()) salesorder.idSalesman = cmbSalesman.EditValue.ToNullInteger();
                salesorder.TransactionNumber = txtTransactionNumber.Text;
                salesorder.DateOrder = cmbDate.DateTime;
                if (cmbDueDate.EditValue != null) salesorder.DueDate = cmbDueDate.DateTime;
                salesorder.Status = cmbStatus.Text;
                salesorder.Remarks = txtRemarks.Text;
                salesorder.SubtotalOrder = txtSubtotal.EditValue.ToDecimal();
                salesorder.DiscountOrder = txtDiscount.EditValue.ToDecimal();
                salesorder.CostOrder = txtCost.EditValue.ToDecimal();
                salesorder.TaxOrder = txtTax.EditValue.ToDecimal();
                salesorder.TotalOrder = txtTotal.EditValue.ToDecimal();
                salesorder.Terms = cmbTerms.Text;

                foreach (TempSalesOrder item in tempSalesOrderBindingSource)
                {
                    if (item.idProduct == null) continue;

                    var cost = (from x in db.Products
                                where x.id == item.idProduct
                                select x.Cost).First();

                    DetailSalesOrder detail = new DetailSalesOrder();
                    detail.idProduct = item.idProduct;
                    detail.QuantityOrder = item.QuantityOrder;
                    detail.PriceOrder = item.PriceOrder;
                    detail.DiscountOrder = item.DiscountOrder;
                    detail.SubtotalOrder = item.SubtotalOrder;
                    detail.Cost = cost;
                    detail.Ratio = 1;
                    detail.Packaging = item.Packaging;
                    salesorder.DetailSalesOrders.Add(detail);
                }

                db.SalesOrders.InsertOnSubmit(salesorder);
                db.SubmitChanges();


                //approval
                try
                {
                    Approval approval = new Approval();
                    approval.idTransaction = salesorder.id;
                    approval.Module = "Sales Order";
                    db.Approvals.InsertOnSubmit(approval);
                    db.SubmitChanges();
                }
                catch { }

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //Printing.SalesOrder(salesorder.id);
                Close();

                try
                {
                    frmSalesOrder formso = (frmSalesOrder)Application.OpenForms["frmSalesOrder"];
                    formso.RefreshData();
                }
                catch { }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
                Close();
        }

        private void txtTaxPercent_EditValueChanged(object sender, EventArgs e)
        {
            GetTotal();
        }

        private void txtCost_EditValueChanged(object sender, EventArgs e)
        {
            GetTotal();
        }

        private void txtDiscount_EditValueChanged(object sender, EventArgs e)
        {
            GetTotal();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetFocusedRowCellValue("PriceLevel", _pricelevel);
        }
    }
}