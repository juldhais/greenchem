﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction2
{
    public partial class frmCreateProjectProgress : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmCreateProjectProgress(int id)
        {
            InitializeComponent();
            _id = id;

            db = new GreenChemDataContext();
            cmbDate.DateTime = db.GetServerDate();

            var project = db.ProjectLists.First(x => x.id == _id);
            Text = "Project Progress | Project Date : " + project.TransactionDate.ToShortDateString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                ProjectProgress progress = new ProjectProgress();
                progress.idProjectList = _id;
                progress.idUserAccount = frmUtama._iduseraccount;
                progress.TransactionDate = cmbDate.DateTime;
                progress.Status = cmbStatus.Text;
                progress.Description = txtDescription.Text;
                db.ProjectProgresses.InsertOnSubmit(progress);

                ProjectList project = db.ProjectLists.First(x => x.id == _id);
                progress.Status = cmbStatus.Text;

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}