﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditSalesOrder : DevExpress.XtraEditors.XtraForm
    {
        string _pricelevel = "1";
        GreenChemDataContext db;

        public frmEditSalesOrder(int? id, bool hideprice = false)
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            cmbNumber.EditValue = id;
            lblRefresh_Click(null, null);
            cmbNumber_EditValueChanged(null, null);

            if (hideprice)
            {
                colPriceLevel.Visible = false;
                colPriceOrder.Visible = false;
                colSubtotalOrder.Visible = false;
                colDiscountOrder.Visible = false;
                txtSubtotal.Visible = false;
                txtDiscount.Visible = false;
                txtCost.Visible = false;
                txtTax.Visible = false;
                txtTaxPercent.Visible = false;
                txtTotal.Visible = false;
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                cmbNumber.Properties.DataSource = from x in db.SalesOrders
                                                    where x.DateOrder > cmbDate.DateTime.AddDays(-366)
                                                    orderby x.id descending
                                                    select new
                                                    {
                                                        x.id,
                                                        x.TransactionNumber,
                                                        x.DateOrder,
                                                        Customer = x.Customer.Name,
                                                        x.TotalOrder
                                                    };

                cmbCustomer.Properties.DataSource = db.Customers.GetName();
                cmbSalesman.Properties.DataSource = db.UserAccounts.GetName();
                cmbCurrency.Properties.DataSource = db.Currencies.GetName();
                cmbLocation.Properties.DataSource = db.Locations.GetName();
                cmbProduct.DataSource = db.Products.GetNameAll();
                cmbTerms.Properties.DataSource = db.Terms.GetName();
                    
                cmbPackaging.Items.Clear();
                foreach (var item in db.Packagings) cmbPackaging.Items.Add(item.Name);

                GetTotal();
            }
            catch { }

            Cursor.Current = Cursors.Default;

        }

        private void GetTotal()
        {
            try
            {
                var details = tempSalesOrderBindingSource.Cast<TempSalesOrder>();
                txtSubtotal.EditValue = details.Sum(x => x.SubtotalOrder);
                decimal subtotaldiscount = txtSubtotal.EditValue.ToDecimal() - txtDiscount.EditValue.ToDecimal();
                txtTax.EditValue = subtotaldiscount * txtTaxPercent.EditValue.ToDecimal() / 100;
                txtTotal.EditValue = subtotaldiscount + txtTax.EditValue.ToDecimal() + txtCost.EditValue.ToDecimal();
            }
            catch { }
        }

        private void cmbNumber_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var salesorder = db.SalesOrders.First(x => x.id == cmbNumber.EditValue.ToInteger());

                cmbDate.DateTime = salesorder.DateOrder;
                cmbDueDate.EditValue = salesorder.DueDate;
                cmbStatus.Text = salesorder.Status;
                cmbCustomer.EditValue = salesorder.idCustomer;
                cmbSalesman.EditValue = salesorder.idSalesman;
                cmbCurrency.EditValue = salesorder.idCurrency;
                cmbLocation.EditValue = salesorder.idLocation;
                txtRemarks.Text = salesorder.Remarks;
                txtSubtotal.EditValue = salesorder.SubtotalOrder;
                txtDiscount.EditValue = salesorder.DiscountOrder;
                txtCost.EditValue = salesorder.CostOrder;
                if (salesorder.SubtotalOrder != 0) txtTaxPercent.EditValue = (salesorder.TaxOrder / salesorder.SubtotalOrder) * 100;
                txtTax.EditValue = salesorder.TaxOrder;
                txtTotal.EditValue = salesorder.TotalOrder;
                cmbTerms.EditValue = salesorder.Terms;

                tempSalesOrderBindingSource.Clear();
                foreach (var item in salesorder.DetailSalesOrders)
                {
                    TempSalesOrder detail = new TempSalesOrder();
                    detail.PriceLevel = item.PriceLevel;
                    detail.idProduct = item.idProduct;
                    detail.QuantityOrder = item.QuantityOrder;
                    detail.UoM = item.Product.UoM;
                    detail.PriceOrder = item.PriceOrder;
                    detail.DiscountOrder = item.DiscountOrder;
                    detail.SubtotalOrder = item.SubtotalOrder;
                    detail.Packaging = item.Packaging;
                    tempSalesOrderBindingSource.Add(detail);
                }

                GetTotal();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void GetPrice()
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                db = new GreenChemDataContext();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
                decimal? price = product.SalesPrice;

                if (_pricelevel == "5") price = product.SalesPrice5;
                else if (_pricelevel == "4") price = product.SalesPrice4;
                else if (_pricelevel == "3") price = product.SalesPrice3;
                else if (_pricelevel == "2") price = product.SalesPrice2;
                else price = product.SalesPrice;

                if (cmbCurrency.EditValue.IsNotEmpty() && product.idCurrency.IsNotEmpty() && product.idCurrency != cmbCurrency.EditValue.ToInteger())
                {
                    var productcurrency = db.Currencies.First(x => x.id == product.idCurrency);
                    var salescurrency = db.Currencies.First(x => x.id == cmbCurrency.EditValue.ToInteger());
                    price = price * productcurrency.Rate / salescurrency.Rate;
                }

                gridView.SetFocusedRowCellValue("PriceOrder", price);
            }
            catch { }
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                GetPrice();
            }
            else if (e.Column.FieldName == "QuantityOrder" || e.Column.FieldName == "PriceOrder" || e.Column.FieldName == "DiscountOrder")
            {
                decimal quantity = gridView.GetFocusedRowCellValue("QuantityOrder").ToDecimal();
                decimal price = gridView.GetFocusedRowCellValue("PriceOrder").ToDecimal();
                decimal discount = gridView.GetFocusedRowCellValue("DiscountOrder").ToDecimal();

                decimal subtotal = 0;
                if (discount < 100) subtotal = (quantity * price) * (100 - discount) / 100;
                else subtotal = quantity * (price - discount);

                gridView.SetFocusedRowCellValue("SubtotalOrder", subtotal);
                GetTotal();
            }
            else if (e.Column.FieldName == "PriceLevel" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                _pricelevel = gridView.GetFocusedRowCellValue("PriceLevel").Safe();
                GetPrice();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbNumber.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Transaction number can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbNumber.Focus();
                return;
            }

            if (cmbCustomer.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Customer can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCustomer.Focus();
                return;
            }
            if (cmbDueDate.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Due Date can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbDueDate.Focus();
                return;
            }
            if (cmbCurrency.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Currency can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbCurrency.Focus();
                return;
            }
            if (txtRemarks.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("PO Number can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }
            if (cmbTerms.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Terms can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbTerms.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                db.ExecuteCommand("DELETE FROM dbo.DetailSalesOrder WHERE idSalesOrder = " + cmbNumber.EditValue.ToInteger());

                SalesOrder salesorder = db.SalesOrders.First(x => x.id == cmbNumber.EditValue.ToInteger());
                if (cmbCurrency.EditValue.IsNotEmpty())
                {
                    salesorder.idCurrency = cmbCurrency.EditValue.ToInteger();
                    salesorder.Rate = db.Currencies.First(x => x.id == salesorder.idCurrency).Rate;
                }
                else salesorder.Rate = 1;
                salesorder.idCustomer = cmbCustomer.EditValue.ToInteger();
                if (cmbLocation.EditValue.IsNotEmpty()) salesorder.idLocation =cmbLocation.EditValue.ToInteger();
                salesorder.idUserAccount = frmUtama._iduseraccount;
                if (cmbSalesman.EditValue.IsNotEmpty()) salesorder.idCustomer = cmbSalesman.EditValue.ToNullInteger();
                salesorder.DateOrder = cmbDate.DateTime;
                if (cmbDueDate.EditValue != null) salesorder.DueDate = cmbDueDate.DateTime;
                salesorder.Status = cmbStatus.Text;
                salesorder.Remarks = txtRemarks.Text;
                salesorder.SubtotalOrder = txtSubtotal.EditValue.ToDecimal();
                salesorder.DiscountOrder = txtDiscount.EditValue.ToDecimal();
                salesorder.CostOrder = txtCost.EditValue.ToDecimal();
                salesorder.TaxOrder = txtTax.EditValue.ToDecimal();
                salesorder.TotalOrder = txtTotal.EditValue.ToDecimal();
                salesorder.Terms = cmbTerms.Text;

                foreach (TempSalesOrder item in tempSalesOrderBindingSource)
                {
                    if (item.idProduct == null) continue;

                    var cost = (from x in db.Products
                                where x.id == item.idProduct
                                select x.Cost).First();

                    DetailSalesOrder detail = new DetailSalesOrder();
                    detail.idProduct = item.idProduct;
                    detail.QuantityOrder = item.QuantityOrder;
                    detail.PriceOrder = item.PriceOrder;
                    detail.DiscountOrder = item.DiscountOrder;
                    detail.SubtotalOrder = item.SubtotalOrder;
                    detail.Cost = cost;
                    detail.Packaging = item.Packaging;
                    salesorder.DetailSalesOrders.Add(detail);
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //Printing.SalesOrder(salesorder.id);
                Close();

                try
                {
                    frmSalesOrder formso = (frmSalesOrder)Application.OpenForms["frmSalesOrder"];
                    formso.RefreshData();
                }
                catch { }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
                Close();
        }

        private void txtTaxPercent_EditValueChanged(object sender, EventArgs e)
        {
            GetTotal();
        }

        private void txtCost_EditValueChanged(object sender, EventArgs e)
        {
            GetTotal();
        }

        private void txtDiscount_EditValueChanged(object sender, EventArgs e)
        {
            GetTotal();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetFocusedRowCellValue("PriceLevel", _pricelevel);
        }
    }
}