﻿namespace GreenChem
{
    partial class frmUtama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUtama));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bahanBakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bahanPenolongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierSCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerSatisfactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerComplaintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rFQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quotationOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pPBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logisticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDOLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeTSLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.standardDeliveryTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expeditionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.locationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.permintaanProduksiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penyerahanProduksiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workInProgressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transferStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qCBarangJadiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qCBahanBakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qCPackingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qCKemasanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sampleCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pengembanganProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchasingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pOToSupplierRegularToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pOToSupplierExpeditionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pOToSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inquiryOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quotationInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pIMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.myInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoNumberingReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expenseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userLogReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerSatisfactionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerComplaintReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerVoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pPBReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pPSReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penyerahanProduksiReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pIMReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.othersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.externalLetterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.internalLetterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voiceOfCompanyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.internalComplaintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newInternalComplaintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockAdjustmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.termsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.packagingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currencyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingGudangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.designReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xtraTabbedMdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataToolStripMenuItem,
            this.contactListToolStripMenuItem,
            this.salesToolStripMenuItem,
            this.logisticToolStripMenuItem,
            this.plantToolStripMenuItem,
            this.rDToolStripMenuItem,
            this.purchasingToolStripMenuItem,
            this.hRToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.othersToolStripMenuItem,
            this.settingToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(984, 27);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productToolStripMenuItem,
            this.bahanBakuToolStripMenuItem,
            this.bahanPenolongToolStripMenuItem,
            this.supplierToolStripMenuItem,
            this.supplierSCToolStripMenuItem,
            this.customerToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(50, 23);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.productToolStripMenuItem.Text = "Product";
            this.productToolStripMenuItem.Click += new System.EventHandler(this.productToolStripMenuItem_Click);
            // 
            // bahanBakuToolStripMenuItem
            // 
            this.bahanBakuToolStripMenuItem.Name = "bahanBakuToolStripMenuItem";
            this.bahanBakuToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.bahanBakuToolStripMenuItem.Text = "Bahan Baku";
            this.bahanBakuToolStripMenuItem.Click += new System.EventHandler(this.bahanBakuToolStripMenuItem_Click);
            // 
            // bahanPenolongToolStripMenuItem
            // 
            this.bahanPenolongToolStripMenuItem.Name = "bahanPenolongToolStripMenuItem";
            this.bahanPenolongToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.bahanPenolongToolStripMenuItem.Text = "Bahan Penolong";
            this.bahanPenolongToolStripMenuItem.Click += new System.EventHandler(this.bahanPenolongToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem
            // 
            this.supplierToolStripMenuItem.Name = "supplierToolStripMenuItem";
            this.supplierToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.supplierToolStripMenuItem.Text = "Supplier";
            this.supplierToolStripMenuItem.Click += new System.EventHandler(this.supplierToolStripMenuItem_Click);
            // 
            // supplierSCToolStripMenuItem
            // 
            this.supplierSCToolStripMenuItem.Name = "supplierSCToolStripMenuItem";
            this.supplierSCToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.supplierSCToolStripMenuItem.Text = "Supplier SC";
            this.supplierSCToolStripMenuItem.Click += new System.EventHandler(this.supplierSCToolStripMenuItem_Click);
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.customerToolStripMenuItem.Text = "Customer";
            this.customerToolStripMenuItem.Click += new System.EventHandler(this.customerToolStripMenuItem_Click);
            // 
            // contactListToolStripMenuItem
            // 
            this.contactListToolStripMenuItem.Name = "contactListToolStripMenuItem";
            this.contactListToolStripMenuItem.Size = new System.Drawing.Size(94, 23);
            this.contactListToolStripMenuItem.Text = "Contact List";
            this.contactListToolStripMenuItem.Click += new System.EventHandler(this.contactListToolStripMenuItem_Click);
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesOrderToolStripMenuItem,
            this.deliveryOrderToolStripMenuItem,
            this.salesInvoiceToolStripMenuItem,
            this.customerSatisfactionToolStripMenuItem,
            this.customerComplaintToolStripMenuItem,
            this.projectToolStripMenuItem,
            this.rFQToolStripMenuItem,
            this.quotationOutToolStripMenuItem,
            this.pPBToolStripMenuItem,
            this.pPSToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(51, 23);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // salesOrderToolStripMenuItem
            // 
            this.salesOrderToolStripMenuItem.Name = "salesOrderToolStripMenuItem";
            this.salesOrderToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.salesOrderToolStripMenuItem.Text = "Sales Order";
            this.salesOrderToolStripMenuItem.Click += new System.EventHandler(this.salesOrderToolStripMenuItem_Click);
            // 
            // deliveryOrderToolStripMenuItem
            // 
            this.deliveryOrderToolStripMenuItem.Name = "deliveryOrderToolStripMenuItem";
            this.deliveryOrderToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.deliveryOrderToolStripMenuItem.Text = "Delivery Order";
            this.deliveryOrderToolStripMenuItem.Click += new System.EventHandler(this.deliveryOrderToolStripMenuItem_Click);
            // 
            // salesInvoiceToolStripMenuItem
            // 
            this.salesInvoiceToolStripMenuItem.Name = "salesInvoiceToolStripMenuItem";
            this.salesInvoiceToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.salesInvoiceToolStripMenuItem.Text = "Sales Invoice";
            this.salesInvoiceToolStripMenuItem.Click += new System.EventHandler(this.salesInvoiceToolStripMenuItem_Click);
            // 
            // customerSatisfactionToolStripMenuItem
            // 
            this.customerSatisfactionToolStripMenuItem.Name = "customerSatisfactionToolStripMenuItem";
            this.customerSatisfactionToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.customerSatisfactionToolStripMenuItem.Text = "Customer Satisfaction";
            this.customerSatisfactionToolStripMenuItem.Click += new System.EventHandler(this.customerSatisfactionToolStripMenuItem_Click);
            // 
            // customerComplaintToolStripMenuItem
            // 
            this.customerComplaintToolStripMenuItem.Name = "customerComplaintToolStripMenuItem";
            this.customerComplaintToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.customerComplaintToolStripMenuItem.Text = "Voice of Customer";
            this.customerComplaintToolStripMenuItem.Click += new System.EventHandler(this.customerComplaintToolStripMenuItem_Click);
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.projectToolStripMenuItem.Text = "Project";
            this.projectToolStripMenuItem.Click += new System.EventHandler(this.projectToolStripMenuItem_Click);
            // 
            // rFQToolStripMenuItem
            // 
            this.rFQToolStripMenuItem.Name = "rFQToolStripMenuItem";
            this.rFQToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.rFQToolStripMenuItem.Text = "RFQ";
            this.rFQToolStripMenuItem.Click += new System.EventHandler(this.inquiryInCustomerToolStripMenuItem_Click);
            // 
            // quotationOutToolStripMenuItem
            // 
            this.quotationOutToolStripMenuItem.Name = "quotationOutToolStripMenuItem";
            this.quotationOutToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.quotationOutToolStripMenuItem.Text = "Quotation Out";
            this.quotationOutToolStripMenuItem.Click += new System.EventHandler(this.quotationOutSupplierToolStripMenuItem_Click);
            // 
            // pPBToolStripMenuItem
            // 
            this.pPBToolStripMenuItem.Name = "pPBToolStripMenuItem";
            this.pPBToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.pPBToolStripMenuItem.Text = "PPB";
            this.pPBToolStripMenuItem.Click += new System.EventHandler(this.pPBToolStripMenuItem_Click);
            // 
            // pPSToolStripMenuItem
            // 
            this.pPSToolStripMenuItem.Name = "pPSToolStripMenuItem";
            this.pPSToolStripMenuItem.Size = new System.Drawing.Size(211, 24);
            this.pPSToolStripMenuItem.Text = "PPS";
            this.pPSToolStripMenuItem.Click += new System.EventHandler(this.pPSToolStripMenuItem_Click);
            // 
            // logisticToolStripMenuItem
            // 
            this.logisticToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeDOLocationToolStripMenuItem,
            this.changeTSLocationToolStripMenuItem,
            this.standardDeliveryTimeToolStripMenuItem,
            this.expeditionToolStripMenuItem,
            this.locationToolStripMenuItem});
            this.logisticToolStripMenuItem.Name = "logisticToolStripMenuItem";
            this.logisticToolStripMenuItem.Size = new System.Drawing.Size(67, 23);
            this.logisticToolStripMenuItem.Text = "Logistic";
            // 
            // changeDOLocationToolStripMenuItem
            // 
            this.changeDOLocationToolStripMenuItem.Name = "changeDOLocationToolStripMenuItem";
            this.changeDOLocationToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.changeDOLocationToolStripMenuItem.Text = "Change DO Location";
            this.changeDOLocationToolStripMenuItem.Click += new System.EventHandler(this.changeDOLocationToolStripMenuItem_Click);
            // 
            // changeTSLocationToolStripMenuItem
            // 
            this.changeTSLocationToolStripMenuItem.Name = "changeTSLocationToolStripMenuItem";
            this.changeTSLocationToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.changeTSLocationToolStripMenuItem.Text = "Change TS Location";
            this.changeTSLocationToolStripMenuItem.Click += new System.EventHandler(this.changeTSLocationToolStripMenuItem_Click);
            // 
            // standardDeliveryTimeToolStripMenuItem
            // 
            this.standardDeliveryTimeToolStripMenuItem.Name = "standardDeliveryTimeToolStripMenuItem";
            this.standardDeliveryTimeToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.standardDeliveryTimeToolStripMenuItem.Text = "Standard Delivery Time";
            this.standardDeliveryTimeToolStripMenuItem.Click += new System.EventHandler(this.standardDeliveryTimeToolStripMenuItem_Click);
            // 
            // expeditionToolStripMenuItem
            // 
            this.expeditionToolStripMenuItem.Name = "expeditionToolStripMenuItem";
            this.expeditionToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.expeditionToolStripMenuItem.Text = "Expedition";
            this.expeditionToolStripMenuItem.Click += new System.EventHandler(this.courierToolStripMenuItem_Click);
            // 
            // locationToolStripMenuItem
            // 
            this.locationToolStripMenuItem.Name = "locationToolStripMenuItem";
            this.locationToolStripMenuItem.Size = new System.Drawing.Size(219, 24);
            this.locationToolStripMenuItem.Text = "Location";
            this.locationToolStripMenuItem.Click += new System.EventHandler(this.locationToolStripMenuItem_Click);
            // 
            // plantToolStripMenuItem
            // 
            this.plantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.permintaanProduksiToolStripMenuItem,
            this.penyerahanProduksiToolStripMenuItem,
            this.workInProgressToolStripMenuItem,
            this.transferStockToolStripMenuItem});
            this.plantToolStripMenuItem.Name = "plantToolStripMenuItem";
            this.plantToolStripMenuItem.Size = new System.Drawing.Size(52, 23);
            this.plantToolStripMenuItem.Text = "Plant";
            // 
            // permintaanProduksiToolStripMenuItem
            // 
            this.permintaanProduksiToolStripMenuItem.Name = "permintaanProduksiToolStripMenuItem";
            this.permintaanProduksiToolStripMenuItem.Size = new System.Drawing.Size(207, 24);
            this.permintaanProduksiToolStripMenuItem.Text = "Permintaan Produksi";
            this.permintaanProduksiToolStripMenuItem.Click += new System.EventHandler(this.permintaanProduksiToolStripMenuItem_Click);
            // 
            // penyerahanProduksiToolStripMenuItem
            // 
            this.penyerahanProduksiToolStripMenuItem.Name = "penyerahanProduksiToolStripMenuItem";
            this.penyerahanProduksiToolStripMenuItem.Size = new System.Drawing.Size(207, 24);
            this.penyerahanProduksiToolStripMenuItem.Text = "Penyerahan Produksi";
            this.penyerahanProduksiToolStripMenuItem.Click += new System.EventHandler(this.penyerahanProduksiToolStripMenuItem_Click);
            // 
            // workInProgressToolStripMenuItem
            // 
            this.workInProgressToolStripMenuItem.Name = "workInProgressToolStripMenuItem";
            this.workInProgressToolStripMenuItem.Size = new System.Drawing.Size(207, 24);
            this.workInProgressToolStripMenuItem.Text = "Work In Progress";
            this.workInProgressToolStripMenuItem.Click += new System.EventHandler(this.workInProgressToolStripMenuItem_Click);
            // 
            // transferStockToolStripMenuItem
            // 
            this.transferStockToolStripMenuItem.Name = "transferStockToolStripMenuItem";
            this.transferStockToolStripMenuItem.Size = new System.Drawing.Size(207, 24);
            this.transferStockToolStripMenuItem.Text = "Transfer Stock";
            this.transferStockToolStripMenuItem.Click += new System.EventHandler(this.transferStockToolStripMenuItem_Click);
            // 
            // rDToolStripMenuItem
            // 
            this.rDToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labServiceToolStripMenuItem,
            this.qCBarangJadiToolStripMenuItem,
            this.qCBahanBakuToolStripMenuItem,
            this.qCPackingToolStripMenuItem,
            this.qCKemasanToolStripMenuItem,
            this.sampleCodeToolStripMenuItem,
            this.pengembanganProdukToolStripMenuItem});
            this.rDToolStripMenuItem.Name = "rDToolStripMenuItem";
            this.rDToolStripMenuItem.Size = new System.Drawing.Size(58, 23);
            this.rDToolStripMenuItem.Text = "R && D";
            // 
            // labServiceToolStripMenuItem
            // 
            this.labServiceToolStripMenuItem.Name = "labServiceToolStripMenuItem";
            this.labServiceToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.labServiceToolStripMenuItem.Text = "Lab Service";
            this.labServiceToolStripMenuItem.Click += new System.EventHandler(this.labServiceToolStripMenuItem_Click);
            // 
            // qCBarangJadiToolStripMenuItem
            // 
            this.qCBarangJadiToolStripMenuItem.Name = "qCBarangJadiToolStripMenuItem";
            this.qCBarangJadiToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.qCBarangJadiToolStripMenuItem.Text = "QC Barang Jadi";
            this.qCBarangJadiToolStripMenuItem.Click += new System.EventHandler(this.qCBarangJadiToolStripMenuItem_Click);
            // 
            // qCBahanBakuToolStripMenuItem
            // 
            this.qCBahanBakuToolStripMenuItem.Name = "qCBahanBakuToolStripMenuItem";
            this.qCBahanBakuToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.qCBahanBakuToolStripMenuItem.Text = "QC Bahan Baku";
            this.qCBahanBakuToolStripMenuItem.Click += new System.EventHandler(this.qCBahanBakuToolStripMenuItem_Click);
            // 
            // qCPackingToolStripMenuItem
            // 
            this.qCPackingToolStripMenuItem.Name = "qCPackingToolStripMenuItem";
            this.qCPackingToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.qCPackingToolStripMenuItem.Text = "QC Packing";
            this.qCPackingToolStripMenuItem.Click += new System.EventHandler(this.qCPackingToolStripMenuItem_Click);
            // 
            // qCKemasanToolStripMenuItem
            // 
            this.qCKemasanToolStripMenuItem.Name = "qCKemasanToolStripMenuItem";
            this.qCKemasanToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.qCKemasanToolStripMenuItem.Text = "QC Kemasan";
            this.qCKemasanToolStripMenuItem.Click += new System.EventHandler(this.qCKemasanToolStripMenuItem_Click);
            // 
            // sampleCodeToolStripMenuItem
            // 
            this.sampleCodeToolStripMenuItem.Name = "sampleCodeToolStripMenuItem";
            this.sampleCodeToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.sampleCodeToolStripMenuItem.Text = "Sample Code";
            this.sampleCodeToolStripMenuItem.Click += new System.EventHandler(this.sampleCodeToolStripMenuItem_Click);
            // 
            // pengembanganProdukToolStripMenuItem
            // 
            this.pengembanganProdukToolStripMenuItem.Name = "pengembanganProdukToolStripMenuItem";
            this.pengembanganProdukToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.pengembanganProdukToolStripMenuItem.Text = "Pengembangan Produk";
            this.pengembanganProdukToolStripMenuItem.Click += new System.EventHandler(this.pengembanganProdukToolStripMenuItem_Click);
            // 
            // purchasingToolStripMenuItem
            // 
            this.purchasingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pOToSupplierRegularToolStripMenuItem,
            this.pOToSupplierExpeditionToolStripMenuItem,
            this.pOToSupplierToolStripMenuItem,
            this.inquiryOutToolStripMenuItem,
            this.quotationInToolStripMenuItem});
            this.purchasingToolStripMenuItem.Name = "purchasingToolStripMenuItem";
            this.purchasingToolStripMenuItem.Size = new System.Drawing.Size(88, 23);
            this.purchasingToolStripMenuItem.Text = "Purchasing";
            // 
            // pOToSupplierRegularToolStripMenuItem
            // 
            this.pOToSupplierRegularToolStripMenuItem.Name = "pOToSupplierRegularToolStripMenuItem";
            this.pOToSupplierRegularToolStripMenuItem.Size = new System.Drawing.Size(234, 24);
            this.pOToSupplierRegularToolStripMenuItem.Text = "PO to Supplier Regular";
            this.pOToSupplierRegularToolStripMenuItem.Click += new System.EventHandler(this.pOToSupplierRegularToolStripMenuItem_Click);
            // 
            // pOToSupplierExpeditionToolStripMenuItem
            // 
            this.pOToSupplierExpeditionToolStripMenuItem.Name = "pOToSupplierExpeditionToolStripMenuItem";
            this.pOToSupplierExpeditionToolStripMenuItem.Size = new System.Drawing.Size(234, 24);
            this.pOToSupplierExpeditionToolStripMenuItem.Text = "PO to Supplier Expedition";
            this.pOToSupplierExpeditionToolStripMenuItem.Click += new System.EventHandler(this.pOToSuppplierExpedisiToolStripMenuItem_Click);
            // 
            // pOToSupplierToolStripMenuItem
            // 
            this.pOToSupplierToolStripMenuItem.Name = "pOToSupplierToolStripMenuItem";
            this.pOToSupplierToolStripMenuItem.Size = new System.Drawing.Size(234, 24);
            this.pOToSupplierToolStripMenuItem.Text = "PO to Supplier";
            this.pOToSupplierToolStripMenuItem.Click += new System.EventHandler(this.autoNumberToolStripMenuItem_Click);
            // 
            // inquiryOutToolStripMenuItem
            // 
            this.inquiryOutToolStripMenuItem.Name = "inquiryOutToolStripMenuItem";
            this.inquiryOutToolStripMenuItem.Size = new System.Drawing.Size(234, 24);
            this.inquiryOutToolStripMenuItem.Text = "Inquiry Out";
            this.inquiryOutToolStripMenuItem.Click += new System.EventHandler(this.inquiryOutSupplierToolStripMenuItem_Click);
            // 
            // quotationInToolStripMenuItem
            // 
            this.quotationInToolStripMenuItem.Name = "quotationInToolStripMenuItem";
            this.quotationInToolStripMenuItem.Size = new System.Drawing.Size(234, 24);
            this.quotationInToolStripMenuItem.Text = "Quotation In";
            this.quotationInToolStripMenuItem.Click += new System.EventHandler(this.quotationInCustomerToolStripMenuItem_Click);
            // 
            // hRToolStripMenuItem
            // 
            this.hRToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pIMToolStripMenuItem,
            this.leaveToolStripMenuItem,
            this.toolStripSeparator1,
            this.myInformationToolStripMenuItem});
            this.hRToolStripMenuItem.Name = "hRToolStripMenuItem";
            this.hRToolStripMenuItem.Size = new System.Drawing.Size(39, 23);
            this.hRToolStripMenuItem.Text = "HR";
            // 
            // pIMToolStripMenuItem
            // 
            this.pIMToolStripMenuItem.Name = "pIMToolStripMenuItem";
            this.pIMToolStripMenuItem.Size = new System.Drawing.Size(174, 24);
            this.pIMToolStripMenuItem.Text = "PIM";
            this.pIMToolStripMenuItem.Click += new System.EventHandler(this.pIMToolStripMenuItem_Click);
            // 
            // leaveToolStripMenuItem
            // 
            this.leaveToolStripMenuItem.Name = "leaveToolStripMenuItem";
            this.leaveToolStripMenuItem.Size = new System.Drawing.Size(174, 24);
            this.leaveToolStripMenuItem.Text = "Leave";
            this.leaveToolStripMenuItem.Click += new System.EventHandler(this.leaveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(171, 6);
            // 
            // myInformationToolStripMenuItem
            // 
            this.myInformationToolStripMenuItem.Name = "myInformationToolStripMenuItem";
            this.myInformationToolStripMenuItem.Size = new System.Drawing.Size(174, 24);
            this.myInformationToolStripMenuItem.Text = "My Information";
            this.myInformationToolStripMenuItem.Click += new System.EventHandler(this.myInformationToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesOrderReportToolStripMenuItem,
            this.autoNumberingReportToolStripMenuItem,
            this.expenseReportToolStripMenuItem,
            this.userLogReportToolStripMenuItem,
            this.customerSatisfactionReportToolStripMenuItem,
            this.customerComplaintReportToolStripMenuItem,
            this.customerVoiceReportToolStripMenuItem,
            this.pPBReportToolStripMenuItem,
            this.pPSReportToolStripMenuItem,
            this.penyerahanProduksiReportToolStripMenuItem,
            this.stockReportToolStripMenuItem,
            this.pIMReportToolStripMenuItem});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(62, 23);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // salesOrderReportToolStripMenuItem
            // 
            this.salesOrderReportToolStripMenuItem.Name = "salesOrderReportToolStripMenuItem";
            this.salesOrderReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.salesOrderReportToolStripMenuItem.Text = "Sales Order Report";
            this.salesOrderReportToolStripMenuItem.Click += new System.EventHandler(this.salesOrderReportToolStripMenuItem_Click);
            // 
            // autoNumberingReportToolStripMenuItem
            // 
            this.autoNumberingReportToolStripMenuItem.Name = "autoNumberingReportToolStripMenuItem";
            this.autoNumberingReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.autoNumberingReportToolStripMenuItem.Text = "Auto Numbering Report";
            this.autoNumberingReportToolStripMenuItem.Click += new System.EventHandler(this.autoNumberingReportToolStripMenuItem_Click);
            // 
            // expenseReportToolStripMenuItem
            // 
            this.expenseReportToolStripMenuItem.Name = "expenseReportToolStripMenuItem";
            this.expenseReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.expenseReportToolStripMenuItem.Text = "Expense Report";
            this.expenseReportToolStripMenuItem.Click += new System.EventHandler(this.expenseReportToolStripMenuItem_Click);
            // 
            // userLogReportToolStripMenuItem
            // 
            this.userLogReportToolStripMenuItem.Name = "userLogReportToolStripMenuItem";
            this.userLogReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.userLogReportToolStripMenuItem.Text = "User Log Report";
            this.userLogReportToolStripMenuItem.Click += new System.EventHandler(this.userLogReportToolStripMenuItem_Click);
            // 
            // customerSatisfactionReportToolStripMenuItem
            // 
            this.customerSatisfactionReportToolStripMenuItem.Name = "customerSatisfactionReportToolStripMenuItem";
            this.customerSatisfactionReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.customerSatisfactionReportToolStripMenuItem.Text = "Customer Satisfaction Report";
            this.customerSatisfactionReportToolStripMenuItem.Click += new System.EventHandler(this.customerSatisfactionReportToolStripMenuItem_Click);
            // 
            // customerComplaintReportToolStripMenuItem
            // 
            this.customerComplaintReportToolStripMenuItem.Name = "customerComplaintReportToolStripMenuItem";
            this.customerComplaintReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.customerComplaintReportToolStripMenuItem.Text = "Customer Complaint Report";
            this.customerComplaintReportToolStripMenuItem.Click += new System.EventHandler(this.customerComplaintReportToolStripMenuItem_Click);
            // 
            // customerVoiceReportToolStripMenuItem
            // 
            this.customerVoiceReportToolStripMenuItem.Name = "customerVoiceReportToolStripMenuItem";
            this.customerVoiceReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.customerVoiceReportToolStripMenuItem.Text = "Customer Voice Report";
            this.customerVoiceReportToolStripMenuItem.Click += new System.EventHandler(this.customerVoiceReportToolStripMenuItem_Click);
            // 
            // pPBReportToolStripMenuItem
            // 
            this.pPBReportToolStripMenuItem.Name = "pPBReportToolStripMenuItem";
            this.pPBReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.pPBReportToolStripMenuItem.Text = "PPB Report";
            this.pPBReportToolStripMenuItem.Click += new System.EventHandler(this.pPBReportToolStripMenuItem_Click);
            // 
            // pPSReportToolStripMenuItem
            // 
            this.pPSReportToolStripMenuItem.Name = "pPSReportToolStripMenuItem";
            this.pPSReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.pPSReportToolStripMenuItem.Text = "PPS Report";
            this.pPSReportToolStripMenuItem.Click += new System.EventHandler(this.pPSReportToolStripMenuItem_Click);
            // 
            // penyerahanProduksiReportToolStripMenuItem
            // 
            this.penyerahanProduksiReportToolStripMenuItem.Name = "penyerahanProduksiReportToolStripMenuItem";
            this.penyerahanProduksiReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.penyerahanProduksiReportToolStripMenuItem.Text = "Penyerahan Produksi Report";
            this.penyerahanProduksiReportToolStripMenuItem.Click += new System.EventHandler(this.penyerahanProduksiReportToolStripMenuItem_Click);
            // 
            // stockReportToolStripMenuItem
            // 
            this.stockReportToolStripMenuItem.Name = "stockReportToolStripMenuItem";
            this.stockReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.stockReportToolStripMenuItem.Text = "Stock Report";
            this.stockReportToolStripMenuItem.Click += new System.EventHandler(this.stockReportToolStripMenuItem_Click);
            // 
            // pIMReportToolStripMenuItem
            // 
            this.pIMReportToolStripMenuItem.Name = "pIMReportToolStripMenuItem";
            this.pIMReportToolStripMenuItem.Size = new System.Drawing.Size(256, 24);
            this.pIMReportToolStripMenuItem.Text = "PIM Report";
            this.pIMReportToolStripMenuItem.Click += new System.EventHandler(this.pIMReportToolStripMenuItem_Click);
            // 
            // othersToolStripMenuItem
            // 
            this.othersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.externalLetterToolStripMenuItem,
            this.internalLetterToolStripMenuItem,
            this.voiceOfCompanyToolStripMenuItem,
            this.internalComplaintToolStripMenuItem,
            this.newInternalComplaintToolStripMenuItem,
            this.stockAdjustmentToolStripMenuItem,
            this.expenseToolStripMenuItem,
            this.categoryToolStripMenuItem,
            this.departmentToolStripMenuItem,
            this.employeeToolStripMenuItem,
            this.termsToolStripMenuItem,
            this.packagingToolStripMenuItem,
            this.currencyToolStripMenuItem,
            this.settingGudangToolStripMenuItem,
            this.userAccountToolStripMenuItem});
            this.othersToolStripMenuItem.Name = "othersToolStripMenuItem";
            this.othersToolStripMenuItem.Size = new System.Drawing.Size(63, 23);
            this.othersToolStripMenuItem.Text = "Others";
            // 
            // externalLetterToolStripMenuItem
            // 
            this.externalLetterToolStripMenuItem.Name = "externalLetterToolStripMenuItem";
            this.externalLetterToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.externalLetterToolStripMenuItem.Text = "External Letter";
            this.externalLetterToolStripMenuItem.Click += new System.EventHandler(this.generalLetterToolStripMenuItem_Click);
            // 
            // internalLetterToolStripMenuItem
            // 
            this.internalLetterToolStripMenuItem.Name = "internalLetterToolStripMenuItem";
            this.internalLetterToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.internalLetterToolStripMenuItem.Text = "Internal Letter";
            this.internalLetterToolStripMenuItem.Click += new System.EventHandler(this.internalLetterToolStripMenuItem_Click);
            // 
            // voiceOfCompanyToolStripMenuItem
            // 
            this.voiceOfCompanyToolStripMenuItem.Name = "voiceOfCompanyToolStripMenuItem";
            this.voiceOfCompanyToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.voiceOfCompanyToolStripMenuItem.Text = "Voice of Company";
            this.voiceOfCompanyToolStripMenuItem.Click += new System.EventHandler(this.customerVoiceToolStripMenuItem_Click);
            // 
            // internalComplaintToolStripMenuItem
            // 
            this.internalComplaintToolStripMenuItem.Name = "internalComplaintToolStripMenuItem";
            this.internalComplaintToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.internalComplaintToolStripMenuItem.Text = "Internal Complaint";
            this.internalComplaintToolStripMenuItem.Click += new System.EventHandler(this.internalComplaintToolStripMenuItem_Click);
            // 
            // newInternalComplaintToolStripMenuItem
            // 
            this.newInternalComplaintToolStripMenuItem.Name = "newInternalComplaintToolStripMenuItem";
            this.newInternalComplaintToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.newInternalComplaintToolStripMenuItem.Text = "New Internal Complaint";
            this.newInternalComplaintToolStripMenuItem.Click += new System.EventHandler(this.newInternalComplaintToolStripMenuItem_Click);
            // 
            // stockAdjustmentToolStripMenuItem
            // 
            this.stockAdjustmentToolStripMenuItem.Name = "stockAdjustmentToolStripMenuItem";
            this.stockAdjustmentToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.stockAdjustmentToolStripMenuItem.Text = "Stock Adjustment";
            this.stockAdjustmentToolStripMenuItem.Click += new System.EventHandler(this.stockAdjustmentToolStripMenuItem_Click);
            // 
            // expenseToolStripMenuItem
            // 
            this.expenseToolStripMenuItem.Name = "expenseToolStripMenuItem";
            this.expenseToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.expenseToolStripMenuItem.Text = "Expense";
            this.expenseToolStripMenuItem.Click += new System.EventHandler(this.expenseToolStripMenuItem_Click);
            // 
            // categoryToolStripMenuItem
            // 
            this.categoryToolStripMenuItem.Name = "categoryToolStripMenuItem";
            this.categoryToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.categoryToolStripMenuItem.Text = "Category";
            this.categoryToolStripMenuItem.Click += new System.EventHandler(this.categoryToolStripMenuItem_Click);
            // 
            // departmentToolStripMenuItem
            // 
            this.departmentToolStripMenuItem.Name = "departmentToolStripMenuItem";
            this.departmentToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.departmentToolStripMenuItem.Text = "Department";
            this.departmentToolStripMenuItem.Click += new System.EventHandler(this.departmentToolStripMenuItem_Click);
            // 
            // employeeToolStripMenuItem
            // 
            this.employeeToolStripMenuItem.Name = "employeeToolStripMenuItem";
            this.employeeToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.employeeToolStripMenuItem.Text = "Employee";
            this.employeeToolStripMenuItem.Click += new System.EventHandler(this.salesmanToolStripMenuItem_Click);
            // 
            // termsToolStripMenuItem
            // 
            this.termsToolStripMenuItem.Name = "termsToolStripMenuItem";
            this.termsToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.termsToolStripMenuItem.Text = "Terms";
            this.termsToolStripMenuItem.Click += new System.EventHandler(this.termsToolStripMenuItem_Click);
            // 
            // packagingToolStripMenuItem
            // 
            this.packagingToolStripMenuItem.Name = "packagingToolStripMenuItem";
            this.packagingToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.packagingToolStripMenuItem.Text = "Packaging";
            this.packagingToolStripMenuItem.Click += new System.EventHandler(this.packagingToolStripMenuItem_Click);
            // 
            // currencyToolStripMenuItem
            // 
            this.currencyToolStripMenuItem.Name = "currencyToolStripMenuItem";
            this.currencyToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.currencyToolStripMenuItem.Text = "Currency";
            this.currencyToolStripMenuItem.Click += new System.EventHandler(this.currencyToolStripMenuItem_Click);
            // 
            // settingGudangToolStripMenuItem
            // 
            this.settingGudangToolStripMenuItem.Name = "settingGudangToolStripMenuItem";
            this.settingGudangToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.settingGudangToolStripMenuItem.Text = "Setting Gudang";
            this.settingGudangToolStripMenuItem.Click += new System.EventHandler(this.settingGudangToolStripMenuItem_Click);
            // 
            // userAccountToolStripMenuItem
            // 
            this.userAccountToolStripMenuItem.Name = "userAccountToolStripMenuItem";
            this.userAccountToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.userAccountToolStripMenuItem.Text = "User Account";
            this.userAccountToolStripMenuItem.Click += new System.EventHandler(this.userAccountToolStripMenuItem_Click);
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem,
            this.designReportToolStripMenuItem,
            this.changePasswordToolStripMenuItem,
            this.toolStripSeparator4,
            this.logOutToolStripMenuItem});
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(64, 23);
            this.settingToolStripMenuItem.Text = "Setting";
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // designReportToolStripMenuItem
            // 
            this.designReportToolStripMenuItem.Name = "designReportToolStripMenuItem";
            this.designReportToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.designReportToolStripMenuItem.Text = "Design Report";
            this.designReportToolStripMenuItem.Click += new System.EventHandler(this.designReportToolStripMenuItem_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(184, 6);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // xtraTabbedMdiManager
            // 
            this.xtraTabbedMdiManager.AppearancePage.Header.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.xtraTabbedMdiManager.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabbedMdiManager.MdiParent = this;
            // 
            // frmUtama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "frmUtama";
            this.Text = "GREENSYS  4.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmUtama_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager;
        private System.Windows.Forms.ToolStripMenuItem salesOrderReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactListToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userLogReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoNumberingReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expenseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bahanBakuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bahanPenolongToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerSatisfactionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerComplaintReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pPBReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pPSReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierSCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penyerahanProduksiReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem designReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerVoiceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pIMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logisticToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchasingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem othersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerSatisfactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerComplaintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rFQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quotationOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pPBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeDOLocationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem standardDeliveryTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expeditionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem locationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem permintaanProduksiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penyerahanProduksiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workInProgressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transferStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem labServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qCBarangJadiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qCBahanBakuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qCPackingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qCKemasanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sampleCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pengembanganProdukToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pOToSupplierRegularToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pOToSupplierExpeditionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inquiryOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quotationInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem externalLetterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem internalLetterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voiceOfCompanyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem internalComplaintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newInternalComplaintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockAdjustmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem departmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem termsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem packagingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currencyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userAccountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesInvoiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pOToSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeTSLocationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pIMReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem myInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem settingGudangToolStripMenuItem;
    }
}