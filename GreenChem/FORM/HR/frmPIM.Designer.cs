﻿namespace GreenChem.MenuHR
{
    partial class frmPIM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaritalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNationality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKTP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSIMA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSIMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSIMC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPassport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNPWP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalaryAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccountNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBank = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBranch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayGrade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeaveEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.chkActive = new DevExpress.XtraEditors.CheckEdit();
            this.btnPosition = new DevExpress.XtraEditors.SimpleButton();
            this.btnNationality = new DevExpress.XtraEditors.SimpleButton();
            this.btnPayGrade = new DevExpress.XtraEditors.SimpleButton();
            this.btnView = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 40);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(1084, 522);
            this.gridControl.TabIndex = 5;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colFullName,
            this.colIDNo,
            this.colGender,
            this.colMaritalStatus,
            this.colNationality,
            this.colBirthDate,
            this.colKTP,
            this.colSIMA,
            this.colSIMB,
            this.colSIMC,
            this.colPassport,
            this.colNPWP,
            this.colStatus,
            this.colAddress,
            this.colPhone1,
            this.colPhone2,
            this.colWorkEmail,
            this.colOtherEmail,
            this.colStartDate,
            this.colEndDate,
            this.colSalaryAmount,
            this.colRemarks,
            this.colAccountNumber,
            this.colBank,
            this.colBranch,
            this.colPayGrade,
            this.colLeaveEntitlement,
            this.colPosition});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.RowHeight = 23;
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colFullName
            // 
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 0;
            this.colFullName.Width = 217;
            // 
            // colIDNo
            // 
            this.colIDNo.FieldName = "IDNo";
            this.colIDNo.Name = "colIDNo";
            this.colIDNo.Visible = true;
            this.colIDNo.VisibleIndex = 2;
            this.colIDNo.Width = 197;
            // 
            // colGender
            // 
            this.colGender.FieldName = "Gender";
            this.colGender.Name = "colGender";
            // 
            // colMaritalStatus
            // 
            this.colMaritalStatus.FieldName = "MaritalStatus";
            this.colMaritalStatus.Name = "colMaritalStatus";
            // 
            // colNationality
            // 
            this.colNationality.FieldName = "Nationality";
            this.colNationality.Name = "colNationality";
            // 
            // colBirthDate
            // 
            this.colBirthDate.FieldName = "BirthDate";
            this.colBirthDate.Name = "colBirthDate";
            // 
            // colKTP
            // 
            this.colKTP.FieldName = "KTP";
            this.colKTP.Name = "colKTP";
            // 
            // colSIMA
            // 
            this.colSIMA.FieldName = "SIMA";
            this.colSIMA.Name = "colSIMA";
            // 
            // colSIMB
            // 
            this.colSIMB.FieldName = "SIMB";
            this.colSIMB.Name = "colSIMB";
            // 
            // colSIMC
            // 
            this.colSIMC.FieldName = "SIMC";
            this.colSIMC.Name = "colSIMC";
            // 
            // colPassport
            // 
            this.colPassport.FieldName = "Passport";
            this.colPassport.Name = "colPassport";
            // 
            // colNPWP
            // 
            this.colNPWP.FieldName = "NPWP";
            this.colNPWP.Name = "colNPWP";
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // colAddress
            // 
            this.colAddress.FieldName = "Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.Visible = true;
            this.colAddress.VisibleIndex = 3;
            this.colAddress.Width = 197;
            // 
            // colPhone1
            // 
            this.colPhone1.Caption = "Home Tel.";
            this.colPhone1.FieldName = "Phone1";
            this.colPhone1.Name = "colPhone1";
            this.colPhone1.Visible = true;
            this.colPhone1.VisibleIndex = 4;
            this.colPhone1.Width = 197;
            // 
            // colPhone2
            // 
            this.colPhone2.Caption = "Mobile Num.";
            this.colPhone2.FieldName = "Phone2";
            this.colPhone2.Name = "colPhone2";
            this.colPhone2.Visible = true;
            this.colPhone2.VisibleIndex = 5;
            this.colPhone2.Width = 200;
            // 
            // colWorkEmail
            // 
            this.colWorkEmail.FieldName = "WorkEmail";
            this.colWorkEmail.Name = "colWorkEmail";
            // 
            // colOtherEmail
            // 
            this.colOtherEmail.FieldName = "OtherEmail";
            this.colOtherEmail.Name = "colOtherEmail";
            // 
            // colStartDate
            // 
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            // 
            // colEndDate
            // 
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            // 
            // colSalaryAmount
            // 
            this.colSalaryAmount.FieldName = "SalaryAmount";
            this.colSalaryAmount.Name = "colSalaryAmount";
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            // 
            // colAccountNumber
            // 
            this.colAccountNumber.FieldName = "AccountNumber";
            this.colAccountNumber.Name = "colAccountNumber";
            // 
            // colBank
            // 
            this.colBank.FieldName = "Bank";
            this.colBank.Name = "colBank";
            // 
            // colBranch
            // 
            this.colBranch.FieldName = "Branch";
            this.colBranch.Name = "colBranch";
            // 
            // colPayGrade
            // 
            this.colPayGrade.FieldName = "PayGrade";
            this.colPayGrade.Name = "colPayGrade";
            // 
            // colLeaveEntitlement
            // 
            this.colLeaveEntitlement.Caption = "Leave Ent.";
            this.colLeaveEntitlement.DisplayFormat.FormatString = "n0";
            this.colLeaveEntitlement.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLeaveEntitlement.FieldName = "LeaveEntitlement";
            this.colLeaveEntitlement.Name = "colLeaveEntitlement";
            this.colLeaveEntitlement.Width = 70;
            // 
            // colPosition
            // 
            this.colPosition.Caption = "Position";
            this.colPosition.FieldName = "Position";
            this.colPosition.Name = "colPosition";
            this.colPosition.Visible = true;
            this.colPosition.VisibleIndex = 1;
            this.colPosition.Width = 172;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.chkActive);
            this.panelControl1.Controls.Add(this.btnPosition);
            this.panelControl1.Controls.Add(this.btnNationality);
            this.panelControl1.Controls.Add(this.btnPayGrade);
            this.panelControl1.Controls.Add(this.btnView);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1084, 40);
            this.panelControl1.TabIndex = 4;
            // 
            // chkActive
            // 
            this.chkActive.EditValue = true;
            this.chkActive.Location = new System.Drawing.Point(732, 10);
            this.chkActive.Name = "chkActive";
            this.chkActive.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkActive.Properties.Appearance.Options.UseFont = true;
            this.chkActive.Properties.Caption = "Active";
            this.chkActive.Size = new System.Drawing.Size(75, 21);
            this.chkActive.TabIndex = 8;
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            // 
            // btnPosition
            // 
            this.btnPosition.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPosition.Appearance.Options.UseFont = true;
            this.btnPosition.Location = new System.Drawing.Point(613, 5);
            this.btnPosition.Name = "btnPosition";
            this.btnPosition.Size = new System.Drawing.Size(100, 30);
            this.btnPosition.TabIndex = 7;
            this.btnPosition.Text = "Position";
            this.btnPosition.Click += new System.EventHandler(this.btnPosition_Click);
            // 
            // btnNationality
            // 
            this.btnNationality.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNationality.Appearance.Options.UseFont = true;
            this.btnNationality.Location = new System.Drawing.Point(507, 5);
            this.btnNationality.Name = "btnNationality";
            this.btnNationality.Size = new System.Drawing.Size(100, 30);
            this.btnNationality.TabIndex = 6;
            this.btnNationality.Text = "Nationality";
            this.btnNationality.Click += new System.EventHandler(this.chkNationality_Click);
            // 
            // btnPayGrade
            // 
            this.btnPayGrade.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnPayGrade.Appearance.Options.UseFont = true;
            this.btnPayGrade.Location = new System.Drawing.Point(401, 5);
            this.btnPayGrade.Name = "btnPayGrade";
            this.btnPayGrade.Size = new System.Drawing.Size(100, 30);
            this.btnPayGrade.TabIndex = 5;
            this.btnPayGrade.Text = "Pay Grade";
            this.btnPayGrade.Click += new System.EventHandler(this.btnPayGrade_Click);
            // 
            // btnView
            // 
            this.btnView.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnView.Appearance.Options.UseFont = true;
            this.btnView.Location = new System.Drawing.Point(300, 5);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(90, 30);
            this.btnView.TabIndex = 4;
            this.btnView.Text = "&View";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(204, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Terminate";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(108, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 30);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(12, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 30);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&New";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmPIM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 562);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmPIM";
            this.Text = "Personal Information Management";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkActive.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnView;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colIDNo;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colMaritalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colNationality;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthDate;
        private DevExpress.XtraGrid.Columns.GridColumn colKTP;
        private DevExpress.XtraGrid.Columns.GridColumn colSIMA;
        private DevExpress.XtraGrid.Columns.GridColumn colSIMB;
        private DevExpress.XtraGrid.Columns.GridColumn colSIMC;
        private DevExpress.XtraGrid.Columns.GridColumn colPassport;
        private DevExpress.XtraGrid.Columns.GridColumn colNPWP;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone1;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone2;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSalaryAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colAccountNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colBank;
        private DevExpress.XtraGrid.Columns.GridColumn colBranch;
        private DevExpress.XtraGrid.Columns.GridColumn colPayGrade;
        private DevExpress.XtraEditors.SimpleButton btnPayGrade;
        private DevExpress.XtraEditors.SimpleButton btnNationality;
        private DevExpress.XtraGrid.Columns.GridColumn colLeaveEntitlement;
        private DevExpress.XtraEditors.SimpleButton btnPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraEditors.CheckEdit chkActive;
    }
}