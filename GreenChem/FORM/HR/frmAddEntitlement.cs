﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuHR
{
    public partial class frmAddEntitlement : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmAddEntitlement()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            cmbDate.DateTime = db.GetServerDate();
            cmbEmployee.Properties.DataSource = from x in db.Employees
                                                where x.Active == true
                                                select new
                                                {
                                                    x.id,
                                                    Name = x.FullName
                                                };

            cmbLeaveType.Properties.DataSource = from x in db.LeaveTypes
                                                 where x.Active == true
                                                 select new
                                                 {
                                                     x.id,
                                                     x.Name
                                                 };
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!chkAddtoAll.Checked && cmbEmployee.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Choose employee first.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbEmployee.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                AddEntitlement ent = new AddEntitlement();
                ent.idEmployee = cmbEmployee.EditValue.ToNullInteger();
                ent.idLeaveType = cmbLeaveType.EditValue.ToNullInteger();
                ent.TransactionDate = cmbDate.DateTime;
                ent.Entitlement = txtEntitlement.EditValue.ToInteger();
                ent.AddToAll = chkAddtoAll.Checked;
                db.AddEntitlements.InsertOnSubmit(ent);

                var employee = from x in db.Employees
                               where x.Active == true
                               select new
                               {
                                   x.id
                               };

                if (!chkAddtoAll.Checked && cmbEmployee.EditValue.IsNotEmpty())
                    employee = employee.Where(x => x.id == cmbEmployee.EditValue.ToInteger());

                foreach (var item in employee)
                {
                    Employee emp = db.Employees.First(x => x.id == item.id);
                    emp.LeaveEntitlement = emp.LeaveEntitlement.ToInteger() + ent.Entitlement;
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void chkAddtoAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAddtoAll.Checked)
            {
                cmbEmployee.Enabled = false;
                cmbEmployee.EditValue = null;
            }
            else
            {
                cmbEmployee.Enabled = true;
            }
        }
    }
}