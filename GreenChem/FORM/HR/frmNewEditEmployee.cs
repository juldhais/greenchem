﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;

namespace GreenChem.MenuHR
{
    public partial class frmNewEditEmployee : DevExpress.XtraEditors.XtraForm
    {
        int? _id;
        GreenChemDataContext db;

        public frmNewEditEmployee(int? id = null)
        {
            InitializeComponent();
            _id = id;
            db = new GreenChemDataContext();
            cmbPayGrade.Properties.DataSource = from x in db.PayGrades
                                                where x.Active == true
                                                select new
                                                {
                                                    x.id,
                                                    x.Name
                                                };

            cmbPosition.Properties.DataSource = from x in db.EmployeePositions
                                                where x.Active == true
                                                select new
                                                {
                                                    x.id,
                                                    x.Name
                                                };

            cmbUserAccount.Properties.DataSource = db.UserAccounts.GetName();

            foreach (var item in db.Nationalities.Where(x => x.Active == true))
            {
                cmbNationality.Properties.Items.Add(item.Name);
            }

            if (_id != null)
            {
                var emp = db.Employees.First(x => x.id == _id);
                txtFullName.Text = emp.FullName;
                txtIDNo.Text = emp.IDNo;
                cmbPosition.EditValue = emp.idPosition;
                cmbGender.Text = emp.Gender;
                cmbMaritalStatus.Text = emp.MaritalStatus;
                cmbNationality.Text = emp.Nationality;
                cmbBirthDate.EditValue = emp.BirthDate;
                txtKTP.Text = emp.KTP;
                txtSIMA.Text = emp.SIMA;
                txtSIMB.Text = emp.SIMB;
                txtSIMC.Text = emp.SIMC;
                txtPassport.Text = emp.Passport;
                txtNPWP.Text = emp.NPWP;
                cmbStatus.Text = emp.Status;
                cmbUserAccount.EditValue = emp.idUserAccount;
                txtAddress.Text = emp.Address;
                txtPhone1.Text = emp.Phone1;
                txtPhone2.Text = emp.Phone2;
                txtWorkEmail.Text = emp.WorkEmail;
                txtOtherEmail.Text = emp.OtherEmail;
                cmbStartDate.EditValue = emp.StartDate;
                cmbEndDate.EditValue = emp.EndDate;
                cmbPayGrade.EditValue = emp.idPayGrade;
                txtSalaryAmount.EditValue = emp.SalaryAmount;
                txtRemarks.Text = emp.Remarks;
                txtAccountNumber.Text = emp.AccountNumber;
                txtBank.Text = emp.Bank;
                txtBranch.Text = emp.Branch;
                if (emp.Photo != null) picPhoto.Image = Functions.ByteArrayToImage(emp.Photo.ToArray());

                if (emp.TerminateReason.IsNotEmpty() && emp.TerminateDate.HasValue)
                {
                    lblTerminateReason.Text = "TERMINATED : " + emp.TerminateReason + "\nTerminate Date : " + emp.TerminateDate.Value.ToString("dd/MM/yyyy");
                }

                foreach (var item in emp.EmployeeEmergencyContacts)
                    employeeEmergencyContactBindingSource.Add(item);

                foreach (var item in emp.EmployeeWorkExperiences)
                    employeeWorkExperienceBindingSource.Add(item);

                foreach (var item in emp.EmployeeEducations)
                    employeeEducationBindingSource.Add(item);

                foreach (var item in emp.EmployeeSkills)
                    employeeSkillBindingSource.Add(item);

                //leave balance
                List<TempLeaveEntitlement> list = new List<TempLeaveEntitlement>();
                foreach (var item in db.LeaveTypes)
                {
                    var qentitlement = from x in db.AddEntitlements
                                       where x.idEmployee == _id
                                       && x.idLeaveType == item.id
                                       select new
                                       {
                                           x.Entitlement
                                       };

                    var qleave = from x in db.LeaveUsages
                                 where x.idEmployee == _id
                                 && x.idLeaveType == item.id
                                 select new
                                 {
                                     x.LeaveDay
                                 };

                    int leavebalance = 0;

                    if (qentitlement.Any()) leavebalance += qentitlement.Sum(x => x.Entitlement);
                    if (qleave.Any()) leavebalance -= qleave.Sum(x => x.LeaveDay);

                    TempLeaveEntitlement temp = new TempLeaveEntitlement();
                    temp.LeaveType = item.Name;
                    temp.Balance = leavebalance;
                    list.Add(temp);
                }

                gridControlLeaveEntitlement.DataSource = list;
                gridControlLeaveEntitlement.RefreshDataSource();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                Employee emp;
                if (_id == null) emp = new Employee();
                else
                {
                    emp = db.Employees.First(x => x.id == _id);

                    //delete detail lama
                    foreach (var item in emp.EmployeeEmergencyContacts.Select(x => x.id))
                    {
                        EmployeeEmergencyContact del = db.EmployeeEmergencyContacts.First(x => x.id == item);
                        db.EmployeeEmergencyContacts.DeleteOnSubmit(del);
                    }

                    foreach (var item in emp.EmployeeWorkExperiences.Select(x => x.id))
                    {
                        EmployeeWorkExperience del = db.EmployeeWorkExperiences.First(x => x.id == item);
                        db.EmployeeWorkExperiences.DeleteOnSubmit(del);
                    }

                    foreach (var item in emp.EmployeeEducations.Select(x => x.id))
                    {
                        EmployeeEducation del = db.EmployeeEducations.First(x => x.id == item);
                        db.EmployeeEducations.DeleteOnSubmit(del);
                    }

                    foreach (var item in emp.EmployeeSkills.Select(x => x.id))
                    {
                        EmployeeSkill del = db.EmployeeSkills.First(x => x.id == item);
                        db.EmployeeSkills.DeleteOnSubmit(del);
                    }
                }

                emp.Active = true;
                emp.FullName = txtFullName.Text;
                emp.IDNo = txtIDNo.Text;
                emp.idPosition = cmbPosition.EditValue.ToNullInteger();
                emp.Gender = cmbGender.Text;
                emp.MaritalStatus = cmbMaritalStatus.Text;
                emp.Nationality = cmbNationality.Text;
                emp.BirthDate = cmbBirthDate.EditValue.ToNullDateTime();
                emp.KTP = txtKTP.Text;
                emp.SIMA = txtSIMA.Text;
                emp.SIMB = txtSIMB.Text;
                emp.SIMC = txtSIMC.Text;
                emp.Passport = txtPassport.Text;
                emp.NPWP = txtNPWP.Text;
                emp.Status = cmbStatus.Text;
                emp.idUserAccount = cmbUserAccount.EditValue.ToNullInteger();
                emp.Address = txtAddress.Text;
                emp.Phone1 = txtPhone1.Text;
                emp.Phone2 = txtPhone2.Text;
                emp.WorkEmail = txtWorkEmail.Text;
                emp.OtherEmail = txtOtherEmail.Text;
                emp.StartDate = cmbStartDate.EditValue.ToNullDateTime();
                emp.EndDate = cmbEndDate.EditValue.ToNullDateTime();
                emp.idPayGrade = cmbPayGrade.EditValue.ToNullInteger();
                emp.SalaryAmount = txtSalaryAmount.EditValue.ToDecimal();
                emp.Remarks = txtRemarks.Text;
                emp.AccountNumber = txtAccountNumber.Text;
                emp.Bank = txtBank.Text;
                emp.Branch = txtBranch.Text;

                foreach (EmployeeEmergencyContact item in employeeEmergencyContactBindingSource)
                {
                    EmployeeEmergencyContact detail = new EmployeeEmergencyContact();
                    detail.Name = item.Name;
                    detail.Relationship = item.Relationship;
                    detail.Phone1 = item.Phone1;
                    detail.Phone2 = item.Phone2;
                    emp.EmployeeEmergencyContacts.Add(detail);
                }

                foreach (EmployeeWorkExperience item in employeeWorkExperienceBindingSource)
                {
                    EmployeeWorkExperience detail = new EmployeeWorkExperience();
                    detail.CompanyName = item.CompanyName;
                    detail.JobTitle = item.JobTitle;
                    detail.StartDate = item.StartDate;
                    detail.EndDate = item.EndDate;
                    detail.Remarks = item.Remarks;
                    emp.EmployeeWorkExperiences.Add(detail);
                }

                foreach (EmployeeEducation item in employeeEducationBindingSource)
                {
                    EmployeeEducation detail = new EmployeeEducation();
                    detail.EducationLevel = item.EducationLevel;
                    detail.Institute = item.Institute;
                    detail.Major = item.Major;
                    detail.Year = item.Year;
                    detail.Score = item.Score;
                    detail.StartDate = item.StartDate;
                    detail.EndDate = item.EndDate;
                    emp.EmployeeEducations.Add(detail);
                }

                foreach (EmployeeSkill item in employeeSkillBindingSource)
                {
                    EmployeeSkill detail = new EmployeeSkill();
                    detail.Year = item.Year;
                    detail.Languange = item.Languange;
                    detail.Remarks = item.Remarks;
                    emp.EmployeeSkills.Add(detail);
                }

                try
                {
                    emp.Photo = Functions.ImageToByteArray(picPhoto.Image);
                }
                catch { }

                if (_id == null) db.Employees.InsertOnSubmit(emp);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            var result = XtraMessageBox.Show("Close form?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (result == System.Windows.Forms.DialogResult.Yes) Close();
        }

        private void gridViewEmergencyContact_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridViewEmergencyContact.DeleteSelectedRows();
        }

        private void gridViewWorkExperience_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridViewWorkExperience.DeleteSelectedRows();
        }

        private void gridViewEducation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridViewEducation.DeleteSelectedRows();
        }

        private void gridViewSkill_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridViewSkill.DeleteSelectedRows();
        }

        private void picPhoto_DoubleClick(object sender, EventArgs e)
        {
            picPhoto.LoadImage();
        }
    }
}