﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuHR
{
    public partial class frmNewEditLeave : DevExpress.XtraEditors.XtraForm
    {
        int? _id;
        GreenChemDataContext db;

        public frmNewEditLeave(int? id = null)
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            _id = id;

            cmbEmployee.Properties.DataSource = from x in db.Employees
                                                where x.Active == true
                                                select new
                                                {
                                                    x.id,
                                                    Name =x.FullName
                                                };

            cmbLeaveType.Properties.DataSource = from x in db.LeaveTypes
                                                 where x.Active == true
                                                 select new
                                                 {
                                                     x.id,
                                                     x.Name
                                                 };

            cmbFrom.DateTime = DateTime.Today;
            cmbTo.DateTime = DateTime.Today;
            txtLeaveDay.EditValue = 1;

            if (_id != null)
            {
                var leave = db.LeaveUsages.First(x => x.id == _id);
                cmbEmployee.EditValue = leave.idEmployee;
                cmbFrom.DateTime = leave.FromDate;
                cmbTo.DateTime = leave.ToDate;
                txtLeaveDay.EditValue = leave.LeaveDay;
                txtLeaveBalance.EditValue = leave.LeaveBalance;
                cmbLeaveType.EditValue = leave.idLeaveType;
                txtRemarks.Text = leave.Remarks;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                LeaveUsage leave;

                if (_id == null) leave = new LeaveUsage();
                else
                {
                    leave = db.LeaveUsages.First(x => x.id == _id);
                    Employee oldemp = db.Employees.First(x => x.id == leave.idEmployee);
                    oldemp.LeaveEntitlement = oldemp.LeaveEntitlement.ToInteger() - leave.LeaveDay;
                }

                leave.idEmployee = cmbEmployee.EditValue.ToNullInteger();
                leave.idLeaveType = cmbLeaveType.EditValue.ToNullInteger();
                leave.FromDate = cmbFrom.DateTime;
                leave.ToDate = cmbTo.DateTime;
                leave.LeaveDay = txtLeaveDay.EditValue.ToInteger();
                leave.LeaveBalance = txtLeaveBalance.EditValue.ToInteger();
                leave.Remarks = txtRemarks.Text;

                if (_id == null) db.LeaveUsages.InsertOnSubmit(leave);

                //edit employee leave balance;
                Employee emp = db.Employees.First(x => x.id == leave.idEmployee);
                emp.LeaveEntitlement = emp.LeaveEntitlement.ToInteger() - leave.LeaveDay;

                if (emp.LeaveEntitlement.ToInteger() < 0)
                {
                    XtraMessageBox.Show("Leave over limit!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbFrom_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                var holidays = from x in db.Holidays
                               where x.TransactionDate.Date >= cmbFrom.DateTime.Date && x.TransactionDate.Date <= cmbTo.DateTime.Date
                               select x.TransactionDate;

                txtLeaveDay.EditValue = ExtensionFunctions.CountWorkDays(cmbFrom.DateTime, cmbTo.DateTime, holidays.ToArray());
            }
            catch { }
        }


        private void cmbEmployee_EditValueChanged(object sender, EventArgs e)
        {
            cmbLeaveType_EditValueChanged(null, null);
        }

        private void cmbLeaveType_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var qentitlement = from x in db.AddEntitlements
                                   where x.idEmployee == cmbEmployee.EditValue.ToInteger()
                                   && x.idLeaveType == cmbLeaveType.EditValue.ToInteger()
                                   select new
                                   {
                                       x.Entitlement
                                   };

                var qleave = from x in db.LeaveUsages
                             where x.idEmployee == cmbEmployee.EditValue.ToInteger()
                             && x.idLeaveType == cmbLeaveType.EditValue.ToInteger()
                             select new
                             {
                                 x.LeaveDay
                             };

                int leavebalance = 0;

                if (qentitlement.Any()) leavebalance += qentitlement.Sum(x => x.Entitlement);
                if (qleave.Any()) leavebalance -= qleave.Sum(x => x.LeaveDay);

                txtLeaveBalance.EditValue = leavebalance;

                if (leavebalance < 1)
                {
                    XtraMessageBox.Show("Jatah cuti sudah habis!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}