﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuHR
{
    public partial class frmLeave : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmLeave()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Leave");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Leave");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Leave");
            btnLeaveType.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Leave Type");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.LeaveUsages
                            orderby x.id descending
                            select new
                            {
                                x.id,
                                Employee = x.Employee.FullName,
                                x.FromDate,
                                x.ToDate,
                                x.LeaveDay,
                                x.Remarks,
                                LeaveType = x.LeaveType.Name
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewEditLeave form = new frmNewEditLeave();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmNewEditLeave form = new frmNewEditLeave(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = XtraMessageBox.Show("Delete this record?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                    LeaveUsage leave = db.LeaveUsages.First(x => x.id == id);
                    db.LeaveUsages.DeleteOnSubmit(leave);
                    db.SubmitChanges();
                    
                    RefreshData();
                    XtraMessageBox.Show("Data deleted.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnLeaveType_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmLeaveType form = new frmLeaveType();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void btnAddEntitlement_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmAddEntitlement form = new frmAddEntitlement();
            form.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void btnEntitlementList_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEntitlementList form = new frmEntitlementList();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }

        private void btnHolidayList_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmHoliday form = new frmHoliday();
            form.Show();
            form.BringToFront();
            Cursor.Current = Cursors.Default;
        }


    }
}