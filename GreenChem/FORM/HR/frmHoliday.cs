﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuHR
{
    public partial class frmHoliday : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmHoliday()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            holidayBindingSource.DataSource = db.Holidays;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}