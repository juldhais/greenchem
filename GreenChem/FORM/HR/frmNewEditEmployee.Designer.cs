﻿namespace GreenChem.MenuHR
{
    partial class frmNewEditEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.lblTerminateReason = new DevExpress.XtraEditors.LabelControl();
            this.cmbPosition = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtNPWP = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtPassport = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtSIMC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtSIMB = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtSIMA = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtKTP = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.cmbBirthDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cmbNationality = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.cmbMaritalStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbGender = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtIDNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtFullName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.picPhoto = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlEmergencyContact = new DevExpress.XtraGrid.GridControl();
            this.employeeEmergencyContactBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewEmergencyContact = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelationship = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmbName = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.txtOtherEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtWorkEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhone2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtPhone1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.txtBranch = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.txtBank = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txtAccountNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.txtRemarks = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalaryAmount = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.cmbPayGrade = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.cmbEndDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.cmbStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlWorkExperience = new DevExpress.XtraGrid.GridControl();
            this.employeeWorkExperienceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewWorkExperience = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSkill = new DevExpress.XtraGrid.GridControl();
            this.employeeSkillBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSkill = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colYear1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLanguange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlEducation = new DevExpress.XtraGrid.GridControl();
            this.employeeEducationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewEducation = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEducationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInstitute = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMajor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlLeaveEntitlement = new DevExpress.XtraGrid.GridControl();
            this.gridViewLeaveEntitlement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.cmbUserAccount = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSIMC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSIMB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSIMA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKTP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBirthDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBirthDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbNationality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMaritalStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullName.Properties)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPhoto.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEmergencyContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeEmergencyContactBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmergencyContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemarks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalaryAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPayGrade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEndDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStartDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStartDate.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkExperience)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeWorkExperienceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkExperience)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSkill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeSkillBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSkill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEducation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeEducationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEducation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLeaveEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLeaveEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 12);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(770, 462);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage7,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.labelControl32);
            this.xtraTabPage1.Controls.Add(this.cmbUserAccount);
            this.xtraTabPage1.Controls.Add(this.lblTerminateReason);
            this.xtraTabPage1.Controls.Add(this.cmbPosition);
            this.xtraTabPage1.Controls.Add(this.labelControl31);
            this.xtraTabPage1.Controls.Add(this.cmbStatus);
            this.xtraTabPage1.Controls.Add(this.labelControl13);
            this.xtraTabPage1.Controls.Add(this.txtNPWP);
            this.xtraTabPage1.Controls.Add(this.labelControl12);
            this.xtraTabPage1.Controls.Add(this.txtPassport);
            this.xtraTabPage1.Controls.Add(this.labelControl11);
            this.xtraTabPage1.Controls.Add(this.txtSIMC);
            this.xtraTabPage1.Controls.Add(this.labelControl10);
            this.xtraTabPage1.Controls.Add(this.txtSIMB);
            this.xtraTabPage1.Controls.Add(this.labelControl9);
            this.xtraTabPage1.Controls.Add(this.txtSIMA);
            this.xtraTabPage1.Controls.Add(this.labelControl8);
            this.xtraTabPage1.Controls.Add(this.txtKTP);
            this.xtraTabPage1.Controls.Add(this.labelControl7);
            this.xtraTabPage1.Controls.Add(this.cmbBirthDate);
            this.xtraTabPage1.Controls.Add(this.labelControl6);
            this.xtraTabPage1.Controls.Add(this.cmbNationality);
            this.xtraTabPage1.Controls.Add(this.labelControl5);
            this.xtraTabPage1.Controls.Add(this.cmbMaritalStatus);
            this.xtraTabPage1.Controls.Add(this.labelControl4);
            this.xtraTabPage1.Controls.Add(this.cmbGender);
            this.xtraTabPage1.Controls.Add(this.labelControl3);
            this.xtraTabPage1.Controls.Add(this.txtIDNo);
            this.xtraTabPage1.Controls.Add(this.labelControl2);
            this.xtraTabPage1.Controls.Add(this.txtFullName);
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(764, 433);
            this.xtraTabPage1.Text = "Personal Detail";
            // 
            // lblTerminateReason
            // 
            this.lblTerminateReason.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTerminateReason.Appearance.ForeColor = System.Drawing.Color.DarkRed;
            this.lblTerminateReason.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblTerminateReason.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTerminateReason.Location = new System.Drawing.Point(363, 102);
            this.lblTerminateReason.Name = "lblTerminateReason";
            this.lblTerminateReason.Size = new System.Drawing.Size(340, 109);
            this.lblTerminateReason.TabIndex = 47;
            // 
            // cmbPosition
            // 
            this.cmbPosition.Location = new System.Drawing.Point(103, 72);
            this.cmbPosition.Name = "cmbPosition";
            this.cmbPosition.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbPosition.Properties.Appearance.Options.UseFont = true;
            this.cmbPosition.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbPosition.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPosition.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbPosition.Properties.DisplayMember = "Name";
            this.cmbPosition.Properties.NullText = "";
            this.cmbPosition.Properties.ShowFooter = false;
            this.cmbPosition.Properties.ShowHeader = false;
            this.cmbPosition.Properties.ValueMember = "id";
            this.cmbPosition.Size = new System.Drawing.Size(320, 24);
            this.cmbPosition.TabIndex = 2;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl31.Location = new System.Drawing.Point(15, 75);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(46, 17);
            this.labelControl31.TabIndex = 46;
            this.labelControl31.Text = "Position";
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "Active";
            this.cmbStatus.Location = new System.Drawing.Point(103, 399);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.cmbStatus.Size = new System.Drawing.Size(150, 23);
            this.cmbStatus.TabIndex = 13;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(15, 401);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(35, 17);
            this.labelControl13.TabIndex = 43;
            this.labelControl13.Text = "Status";
            // 
            // txtNPWP
            // 
            this.txtNPWP.Location = new System.Drawing.Point(103, 369);
            this.txtNPWP.Name = "txtNPWP";
            this.txtNPWP.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtNPWP.Properties.Appearance.Options.UseFont = true;
            this.txtNPWP.Size = new System.Drawing.Size(600, 24);
            this.txtNPWP.TabIndex = 12;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(15, 372);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(36, 17);
            this.labelControl12.TabIndex = 41;
            this.labelControl12.Text = "NPWP";
            // 
            // txtPassport
            // 
            this.txtPassport.Location = new System.Drawing.Point(103, 339);
            this.txtPassport.Name = "txtPassport";
            this.txtPassport.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPassport.Properties.Appearance.Options.UseFont = true;
            this.txtPassport.Size = new System.Drawing.Size(600, 24);
            this.txtPassport.TabIndex = 11;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(15, 342);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(51, 17);
            this.labelControl11.TabIndex = 39;
            this.labelControl11.Text = "Passport";
            // 
            // txtSIMC
            // 
            this.txtSIMC.Location = new System.Drawing.Point(103, 309);
            this.txtSIMC.Name = "txtSIMC";
            this.txtSIMC.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSIMC.Properties.Appearance.Options.UseFont = true;
            this.txtSIMC.Size = new System.Drawing.Size(600, 24);
            this.txtSIMC.TabIndex = 10;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(15, 312);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(34, 17);
            this.labelControl10.TabIndex = 37;
            this.labelControl10.Text = "SIM C";
            // 
            // txtSIMB
            // 
            this.txtSIMB.Location = new System.Drawing.Point(103, 279);
            this.txtSIMB.Name = "txtSIMB";
            this.txtSIMB.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSIMB.Properties.Appearance.Options.UseFont = true;
            this.txtSIMB.Size = new System.Drawing.Size(600, 24);
            this.txtSIMB.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(15, 282);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(33, 17);
            this.labelControl9.TabIndex = 35;
            this.labelControl9.Text = "SIM B";
            // 
            // txtSIMA
            // 
            this.txtSIMA.Location = new System.Drawing.Point(103, 249);
            this.txtSIMA.Name = "txtSIMA";
            this.txtSIMA.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSIMA.Properties.Appearance.Options.UseFont = true;
            this.txtSIMA.Size = new System.Drawing.Size(600, 24);
            this.txtSIMA.TabIndex = 8;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(15, 252);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(34, 17);
            this.labelControl8.TabIndex = 33;
            this.labelControl8.Text = "SIM A";
            // 
            // txtKTP
            // 
            this.txtKTP.Location = new System.Drawing.Point(103, 219);
            this.txtKTP.Name = "txtKTP";
            this.txtKTP.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtKTP.Properties.Appearance.Options.UseFont = true;
            this.txtKTP.Size = new System.Drawing.Size(600, 24);
            this.txtKTP.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(15, 222);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(22, 17);
            this.labelControl7.TabIndex = 31;
            this.labelControl7.Text = "KTP";
            // 
            // cmbBirthDate
            // 
            this.cmbBirthDate.EditValue = null;
            this.cmbBirthDate.Location = new System.Drawing.Point(103, 189);
            this.cmbBirthDate.Name = "cmbBirthDate";
            this.cmbBirthDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbBirthDate.Properties.Appearance.Options.UseFont = true;
            this.cmbBirthDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBirthDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbBirthDate.Size = new System.Drawing.Size(150, 24);
            this.cmbBirthDate.TabIndex = 6;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(15, 192);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(57, 17);
            this.labelControl6.TabIndex = 29;
            this.labelControl6.Text = "Birth Date";
            // 
            // cmbNationality
            // 
            this.cmbNationality.EditValue = "";
            this.cmbNationality.Location = new System.Drawing.Point(103, 160);
            this.cmbNationality.Name = "cmbNationality";
            this.cmbNationality.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbNationality.Properties.Appearance.Options.UseFont = true;
            this.cmbNationality.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbNationality.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbNationality.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbNationality.Size = new System.Drawing.Size(150, 23);
            this.cmbNationality.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(15, 162);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(62, 17);
            this.labelControl5.TabIndex = 27;
            this.labelControl5.Text = "Nationality";
            // 
            // cmbMaritalStatus
            // 
            this.cmbMaritalStatus.EditValue = "";
            this.cmbMaritalStatus.Location = new System.Drawing.Point(103, 131);
            this.cmbMaritalStatus.Name = "cmbMaritalStatus";
            this.cmbMaritalStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbMaritalStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbMaritalStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbMaritalStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbMaritalStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbMaritalStatus.Properties.Items.AddRange(new object[] {
            "Single",
            "Married",
            "Other"});
            this.cmbMaritalStatus.Size = new System.Drawing.Size(150, 23);
            this.cmbMaritalStatus.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(15, 133);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(80, 17);
            this.labelControl4.TabIndex = 25;
            this.labelControl4.Text = "Marital Status";
            // 
            // cmbGender
            // 
            this.cmbGender.EditValue = "";
            this.cmbGender.Location = new System.Drawing.Point(103, 102);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbGender.Properties.Appearance.Options.UseFont = true;
            this.cmbGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbGender.Properties.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Other"});
            this.cmbGender.Size = new System.Drawing.Size(150, 23);
            this.cmbGender.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(15, 104);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(43, 17);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Gender";
            // 
            // txtIDNo
            // 
            this.txtIDNo.Location = new System.Drawing.Point(103, 42);
            this.txtIDNo.Name = "txtIDNo";
            this.txtIDNo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtIDNo.Properties.Appearance.Options.UseFont = true;
            this.txtIDNo.Size = new System.Drawing.Size(600, 24);
            this.txtIDNo.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(15, 45);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(37, 17);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "ID No.";
            // 
            // txtFullName
            // 
            this.txtFullName.Location = new System.Drawing.Point(103, 12);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFullName.Properties.Appearance.Options.UseFont = true;
            this.txtFullName.Size = new System.Drawing.Size(600, 24);
            this.txtFullName.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(15, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(58, 17);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Full Name";
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.picPhoto);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(764, 433);
            this.xtraTabPage7.Text = "Photo";
            // 
            // picPhoto
            // 
            this.picPhoto.Location = new System.Drawing.Point(3, 3);
            this.picPhoto.Name = "picPhoto";
            this.picPhoto.Properties.NullText = "Double Click to Add Photo";
            this.picPhoto.Size = new System.Drawing.Size(758, 407);
            this.picPhoto.TabIndex = 8;
            this.picPhoto.DoubleClick += new System.EventHandler(this.picPhoto_DoubleClick);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.labelControl19);
            this.xtraTabPage2.Controls.Add(this.gridControlEmergencyContact);
            this.xtraTabPage2.Controls.Add(this.txtOtherEmail);
            this.xtraTabPage2.Controls.Add(this.labelControl18);
            this.xtraTabPage2.Controls.Add(this.txtWorkEmail);
            this.xtraTabPage2.Controls.Add(this.labelControl17);
            this.xtraTabPage2.Controls.Add(this.txtPhone2);
            this.xtraTabPage2.Controls.Add(this.labelControl16);
            this.xtraTabPage2.Controls.Add(this.txtPhone1);
            this.xtraTabPage2.Controls.Add(this.labelControl15);
            this.xtraTabPage2.Controls.Add(this.labelControl14);
            this.xtraTabPage2.Controls.Add(this.txtAddress);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(764, 433);
            this.xtraTabPage2.Text = "Contact Detail";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelControl19.Location = new System.Drawing.Point(15, 215);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(134, 21);
            this.labelControl19.TabIndex = 54;
            this.labelControl19.Text = "Emergency Contact";
            // 
            // gridControlEmergencyContact
            // 
            this.gridControlEmergencyContact.DataSource = this.employeeEmergencyContactBindingSource;
            this.gridControlEmergencyContact.Location = new System.Drawing.Point(15, 242);
            this.gridControlEmergencyContact.MainView = this.gridViewEmergencyContact;
            this.gridControlEmergencyContact.Name = "gridControlEmergencyContact";
            this.gridControlEmergencyContact.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cmbName});
            this.gridControlEmergencyContact.Size = new System.Drawing.Size(730, 150);
            this.gridControlEmergencyContact.TabIndex = 53;
            this.gridControlEmergencyContact.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEmergencyContact});
            // 
            // employeeEmergencyContactBindingSource
            // 
            this.employeeEmergencyContactBindingSource.DataSource = typeof(GreenChem.EmployeeEmergencyContact);
            // 
            // gridViewEmergencyContact
            // 
            this.gridViewEmergencyContact.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewEmergencyContact.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewEmergencyContact.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridViewEmergencyContact.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewEmergencyContact.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewEmergencyContact.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewEmergencyContact.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewEmergencyContact.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewEmergencyContact.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewEmergencyContact.Appearance.Row.Options.UseFont = true;
            this.gridViewEmergencyContact.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colName,
            this.colRelationship,
            this.colPhone1,
            this.colPhone2});
            this.gridViewEmergencyContact.GridControl = this.gridControlEmergencyContact;
            this.gridViewEmergencyContact.Name = "gridViewEmergencyContact";
            this.gridViewEmergencyContact.OptionsCustomization.AllowGroup = false;
            this.gridViewEmergencyContact.OptionsDetail.ShowDetailTabs = false;
            this.gridViewEmergencyContact.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewEmergencyContact.OptionsView.ShowDetailButtons = false;
            this.gridViewEmergencyContact.OptionsView.ShowGroupPanel = false;
            this.gridViewEmergencyContact.RowHeight = 23;
            this.gridViewEmergencyContact.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewEmergencyContact_KeyDown);
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colRelationship
            // 
            this.colRelationship.FieldName = "Relationship";
            this.colRelationship.Name = "colRelationship";
            this.colRelationship.Visible = true;
            this.colRelationship.VisibleIndex = 1;
            // 
            // colPhone1
            // 
            this.colPhone1.Caption = "Home Phone";
            this.colPhone1.FieldName = "Phone1";
            this.colPhone1.Name = "colPhone1";
            this.colPhone1.Visible = true;
            this.colPhone1.VisibleIndex = 2;
            // 
            // colPhone2
            // 
            this.colPhone2.Caption = "Mobile Phone";
            this.colPhone2.FieldName = "Phone2";
            this.colPhone2.Name = "colPhone2";
            this.colPhone2.Visible = true;
            this.colPhone2.VisibleIndex = 3;
            // 
            // cmbName
            // 
            this.cmbName.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbName.Appearance.Options.UseFont = true;
            this.cmbName.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbName.AppearanceDropDown.Options.UseFont = true;
            this.cmbName.AutoHeight = false;
            this.cmbName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbName.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.cmbName.DisplayMember = "Name";
            this.cmbName.Name = "cmbName";
            this.cmbName.NullText = "";
            this.cmbName.ShowFooter = false;
            this.cmbName.ShowHeader = false;
            this.cmbName.ValueMember = "id";
            // 
            // txtOtherEmail
            // 
            this.txtOtherEmail.Location = new System.Drawing.Point(102, 168);
            this.txtOtherEmail.Name = "txtOtherEmail";
            this.txtOtherEmail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtOtherEmail.Properties.Appearance.Options.UseFont = true;
            this.txtOtherEmail.Size = new System.Drawing.Size(600, 24);
            this.txtOtherEmail.TabIndex = 52;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(17, 171);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(68, 17);
            this.labelControl18.TabIndex = 51;
            this.labelControl18.Text = "Other Email";
            // 
            // txtWorkEmail
            // 
            this.txtWorkEmail.Location = new System.Drawing.Point(102, 138);
            this.txtWorkEmail.Name = "txtWorkEmail";
            this.txtWorkEmail.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtWorkEmail.Properties.Appearance.Options.UseFont = true;
            this.txtWorkEmail.Size = new System.Drawing.Size(600, 24);
            this.txtWorkEmail.TabIndex = 50;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(17, 141);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(66, 17);
            this.labelControl17.TabIndex = 49;
            this.labelControl17.Text = "Work Email";
            // 
            // txtPhone2
            // 
            this.txtPhone2.Location = new System.Drawing.Point(102, 108);
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhone2.Properties.Appearance.Options.UseFont = true;
            this.txtPhone2.Size = new System.Drawing.Size(600, 24);
            this.txtPhone2.TabIndex = 48;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(15, 111);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(81, 17);
            this.labelControl16.TabIndex = 47;
            this.labelControl16.Text = "Mobile Phone";
            // 
            // txtPhone1
            // 
            this.txtPhone1.Location = new System.Drawing.Point(102, 78);
            this.txtPhone1.Name = "txtPhone1";
            this.txtPhone1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPhone1.Properties.Appearance.Options.UseFont = true;
            this.txtPhone1.Size = new System.Drawing.Size(600, 24);
            this.txtPhone1.TabIndex = 46;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Location = new System.Drawing.Point(15, 81);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(75, 17);
            this.labelControl15.TabIndex = 45;
            this.labelControl15.Text = "Home Phone";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(15, 15);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(48, 17);
            this.labelControl14.TabIndex = 43;
            this.labelControl14.Text = "Address";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(102, 12);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(600, 60);
            this.txtAddress.TabIndex = 44;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.txtBranch);
            this.xtraTabPage3.Controls.Add(this.labelControl28);
            this.xtraTabPage3.Controls.Add(this.txtBank);
            this.xtraTabPage3.Controls.Add(this.labelControl27);
            this.xtraTabPage3.Controls.Add(this.txtAccountNumber);
            this.xtraTabPage3.Controls.Add(this.labelControl26);
            this.xtraTabPage3.Controls.Add(this.txtRemarks);
            this.xtraTabPage3.Controls.Add(this.labelControl25);
            this.xtraTabPage3.Controls.Add(this.txtSalaryAmount);
            this.xtraTabPage3.Controls.Add(this.labelControl24);
            this.xtraTabPage3.Controls.Add(this.cmbPayGrade);
            this.xtraTabPage3.Controls.Add(this.labelControl23);
            this.xtraTabPage3.Controls.Add(this.labelControl22);
            this.xtraTabPage3.Controls.Add(this.cmbEndDate);
            this.xtraTabPage3.Controls.Add(this.labelControl21);
            this.xtraTabPage3.Controls.Add(this.cmbStartDate);
            this.xtraTabPage3.Controls.Add(this.labelControl20);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(764, 433);
            this.xtraTabPage3.Text = "Contract and Salary";
            // 
            // txtBranch
            // 
            this.txtBranch.Location = new System.Drawing.Point(92, 240);
            this.txtBranch.Name = "txtBranch";
            this.txtBranch.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBranch.Properties.Appearance.Options.UseFont = true;
            this.txtBranch.Size = new System.Drawing.Size(600, 24);
            this.txtBranch.TabIndex = 67;
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl28.Location = new System.Drawing.Point(15, 243);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(39, 17);
            this.labelControl28.TabIndex = 66;
            this.labelControl28.Text = "Branch";
            // 
            // txtBank
            // 
            this.txtBank.Location = new System.Drawing.Point(92, 210);
            this.txtBank.Name = "txtBank";
            this.txtBank.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtBank.Properties.Appearance.Options.UseFont = true;
            this.txtBank.Size = new System.Drawing.Size(600, 24);
            this.txtBank.TabIndex = 65;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl27.Location = new System.Drawing.Point(15, 213);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(27, 17);
            this.labelControl27.TabIndex = 64;
            this.labelControl27.Text = "Bank";
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.Location = new System.Drawing.Point(92, 180);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAccountNumber.Properties.Appearance.Options.UseFont = true;
            this.txtAccountNumber.Size = new System.Drawing.Size(600, 24);
            this.txtAccountNumber.TabIndex = 63;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl26.Location = new System.Drawing.Point(15, 183);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(71, 17);
            this.labelControl26.TabIndex = 62;
            this.labelControl26.Text = "Account No.";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(92, 150);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtRemarks.Properties.Appearance.Options.UseFont = true;
            this.txtRemarks.Size = new System.Drawing.Size(600, 24);
            this.txtRemarks.TabIndex = 61;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl25.Location = new System.Drawing.Point(15, 153);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(50, 17);
            this.labelControl25.TabIndex = 60;
            this.labelControl25.Text = "Remarks";
            // 
            // txtSalaryAmount
            // 
            this.txtSalaryAmount.Location = new System.Drawing.Point(92, 120);
            this.txtSalaryAmount.Name = "txtSalaryAmount";
            this.txtSalaryAmount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalaryAmount.Properties.Appearance.Options.UseFont = true;
            this.txtSalaryAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalaryAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSalaryAmount.Properties.DisplayFormat.FormatString = "n2";
            this.txtSalaryAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalaryAmount.Properties.EditFormat.FormatString = "n2";
            this.txtSalaryAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalaryAmount.Properties.Mask.EditMask = "n2";
            this.txtSalaryAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalaryAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtSalaryAmount.Size = new System.Drawing.Size(140, 24);
            this.txtSalaryAmount.TabIndex = 59;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl24.Location = new System.Drawing.Point(15, 123);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(45, 17);
            this.labelControl24.TabIndex = 58;
            this.labelControl24.Text = "Amount";
            // 
            // cmbPayGrade
            // 
            this.cmbPayGrade.EditValue = "";
            this.cmbPayGrade.Location = new System.Drawing.Point(92, 90);
            this.cmbPayGrade.Name = "cmbPayGrade";
            this.cmbPayGrade.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbPayGrade.Properties.Appearance.Options.UseFont = true;
            this.cmbPayGrade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPayGrade.Properties.DisplayMember = "Name";
            this.cmbPayGrade.Properties.NullText = "";
            this.cmbPayGrade.Properties.ValueMember = "id";
            this.cmbPayGrade.Properties.View = this.gridView1;
            this.cmbPayGrade.Size = new System.Drawing.Size(370, 24);
            this.cmbPayGrade.TabIndex = 56;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 23;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 264;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl23.Location = new System.Drawing.Point(15, 93);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(60, 17);
            this.labelControl23.TabIndex = 57;
            this.labelControl23.Text = "Pay Grade";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl22.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelControl22.Location = new System.Drawing.Point(15, 66);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(43, 21);
            this.labelControl22.TabIndex = 55;
            this.labelControl22.Text = "Salary";
            // 
            // cmbEndDate
            // 
            this.cmbEndDate.EditValue = null;
            this.cmbEndDate.Location = new System.Drawing.Point(312, 12);
            this.cmbEndDate.Name = "cmbEndDate";
            this.cmbEndDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbEndDate.Properties.Appearance.Options.UseFont = true;
            this.cmbEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbEndDate.Size = new System.Drawing.Size(150, 20);
            this.cmbEndDate.TabIndex = 34;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(253, 15);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(53, 17);
            this.labelControl21.TabIndex = 33;
            this.labelControl21.Text = "End Date";
            // 
            // cmbStartDate
            // 
            this.cmbStartDate.EditValue = null;
            this.cmbStartDate.Location = new System.Drawing.Point(79, 12);
            this.cmbStartDate.Name = "cmbStartDate";
            this.cmbStartDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbStartDate.Properties.Appearance.Options.UseFont = true;
            this.cmbStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStartDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.cmbStartDate.Size = new System.Drawing.Size(150, 20);
            this.cmbStartDate.TabIndex = 32;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(15, 15);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(58, 17);
            this.labelControl20.TabIndex = 31;
            this.labelControl20.Text = "Start Date";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControlWorkExperience);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(764, 433);
            this.xtraTabPage4.Text = "Work Experience";
            // 
            // gridControlWorkExperience
            // 
            this.gridControlWorkExperience.DataSource = this.employeeWorkExperienceBindingSource;
            this.gridControlWorkExperience.Location = new System.Drawing.Point(15, 16);
            this.gridControlWorkExperience.MainView = this.gridViewWorkExperience;
            this.gridControlWorkExperience.Name = "gridControlWorkExperience";
            this.gridControlWorkExperience.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1});
            this.gridControlWorkExperience.Size = new System.Drawing.Size(730, 380);
            this.gridControlWorkExperience.TabIndex = 57;
            this.gridControlWorkExperience.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWorkExperience});
            // 
            // employeeWorkExperienceBindingSource
            // 
            this.employeeWorkExperienceBindingSource.DataSource = typeof(GreenChem.EmployeeWorkExperience);
            // 
            // gridViewWorkExperience
            // 
            this.gridViewWorkExperience.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewWorkExperience.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewWorkExperience.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridViewWorkExperience.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewWorkExperience.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewWorkExperience.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewWorkExperience.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewWorkExperience.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewWorkExperience.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewWorkExperience.Appearance.Row.Options.UseFont = true;
            this.gridViewWorkExperience.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyName,
            this.colJobTitle,
            this.colStartDate,
            this.colEndDate,
            this.colRemarks});
            this.gridViewWorkExperience.GridControl = this.gridControlWorkExperience;
            this.gridViewWorkExperience.Name = "gridViewWorkExperience";
            this.gridViewWorkExperience.OptionsCustomization.AllowGroup = false;
            this.gridViewWorkExperience.OptionsDetail.ShowDetailTabs = false;
            this.gridViewWorkExperience.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewWorkExperience.OptionsView.ShowDetailButtons = false;
            this.gridViewWorkExperience.OptionsView.ShowGroupPanel = false;
            this.gridViewWorkExperience.RowHeight = 23;
            this.gridViewWorkExperience.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewWorkExperience_KeyDown);
            // 
            // colCompanyName
            // 
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 0;
            this.colCompanyName.Width = 142;
            // 
            // colJobTitle
            // 
            this.colJobTitle.FieldName = "JobTitle";
            this.colJobTitle.Name = "colJobTitle";
            this.colJobTitle.Visible = true;
            this.colJobTitle.VisibleIndex = 1;
            this.colJobTitle.Width = 142;
            // 
            // colStartDate
            // 
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 2;
            this.colStartDate.Width = 100;
            // 
            // colEndDate
            // 
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 3;
            this.colEndDate.Width = 100;
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 4;
            this.colRemarks.Width = 228;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.repositoryItemLookUpEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.repositoryItemLookUpEdit1.DisplayMember = "Name";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "";
            this.repositoryItemLookUpEdit1.ShowFooter = false;
            this.repositoryItemLookUpEdit1.ShowHeader = false;
            this.repositoryItemLookUpEdit1.ValueMember = "id";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridControlSkill);
            this.xtraTabPage5.Controls.Add(this.labelControl30);
            this.xtraTabPage5.Controls.Add(this.labelControl29);
            this.xtraTabPage5.Controls.Add(this.gridControlEducation);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(764, 433);
            this.xtraTabPage5.Text = "Education and Skill";
            // 
            // gridControlSkill
            // 
            this.gridControlSkill.DataSource = this.employeeSkillBindingSource;
            this.gridControlSkill.Location = new System.Drawing.Point(15, 285);
            this.gridControlSkill.MainView = this.gridViewSkill;
            this.gridControlSkill.Name = "gridControlSkill";
            this.gridControlSkill.Size = new System.Drawing.Size(730, 110);
            this.gridControlSkill.TabIndex = 61;
            this.gridControlSkill.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSkill});
            // 
            // employeeSkillBindingSource
            // 
            this.employeeSkillBindingSource.DataSource = typeof(GreenChem.EmployeeSkill);
            // 
            // gridViewSkill
            // 
            this.gridViewSkill.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewSkill.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewSkill.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridViewSkill.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewSkill.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewSkill.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewSkill.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewSkill.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewSkill.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewSkill.Appearance.Row.Options.UseFont = true;
            this.gridViewSkill.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colYear1,
            this.colLanguange,
            this.colRemarks1});
            this.gridViewSkill.GridControl = this.gridControlSkill;
            this.gridViewSkill.Name = "gridViewSkill";
            this.gridViewSkill.OptionsCustomization.AllowGroup = false;
            this.gridViewSkill.OptionsDetail.ShowDetailTabs = false;
            this.gridViewSkill.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewSkill.OptionsView.ShowDetailButtons = false;
            this.gridViewSkill.OptionsView.ShowGroupPanel = false;
            this.gridViewSkill.RowHeight = 23;
            this.gridViewSkill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewSkill_KeyDown);
            // 
            // colYear1
            // 
            this.colYear1.FieldName = "Year";
            this.colYear1.Name = "colYear1";
            this.colYear1.Visible = true;
            this.colYear1.VisibleIndex = 0;
            this.colYear1.Width = 80;
            // 
            // colLanguange
            // 
            this.colLanguange.FieldName = "Languange";
            this.colLanguange.Name = "colLanguange";
            this.colLanguange.Visible = true;
            this.colLanguange.VisibleIndex = 1;
            this.colLanguange.Width = 150;
            // 
            // colRemarks1
            // 
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 2;
            this.colRemarks1.Width = 200;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl30.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelControl30.Location = new System.Drawing.Point(15, 258);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(36, 21);
            this.labelControl30.TabIndex = 60;
            this.labelControl30.Text = "Skills";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl29.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelControl29.Location = new System.Drawing.Point(15, 15);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(68, 21);
            this.labelControl29.TabIndex = 59;
            this.labelControl29.Text = "Education";
            // 
            // gridControlEducation
            // 
            this.gridControlEducation.DataSource = this.employeeEducationBindingSource;
            this.gridControlEducation.Location = new System.Drawing.Point(15, 42);
            this.gridControlEducation.MainView = this.gridViewEducation;
            this.gridControlEducation.Name = "gridControlEducation";
            this.gridControlEducation.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit2});
            this.gridControlEducation.Size = new System.Drawing.Size(730, 200);
            this.gridControlEducation.TabIndex = 58;
            this.gridControlEducation.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEducation});
            // 
            // employeeEducationBindingSource
            // 
            this.employeeEducationBindingSource.DataSource = typeof(GreenChem.EmployeeEducation);
            // 
            // gridViewEducation
            // 
            this.gridViewEducation.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewEducation.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewEducation.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridViewEducation.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewEducation.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewEducation.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewEducation.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewEducation.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewEducation.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewEducation.Appearance.Row.Options.UseFont = true;
            this.gridViewEducation.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEducationLevel,
            this.colInstitute,
            this.colMajor,
            this.colYear,
            this.colScore,
            this.colStartDate1,
            this.colEndDate1});
            this.gridViewEducation.GridControl = this.gridControlEducation;
            this.gridViewEducation.Name = "gridViewEducation";
            this.gridViewEducation.OptionsCustomization.AllowGroup = false;
            this.gridViewEducation.OptionsDetail.ShowDetailTabs = false;
            this.gridViewEducation.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridViewEducation.OptionsView.ShowDetailButtons = false;
            this.gridViewEducation.OptionsView.ShowGroupPanel = false;
            this.gridViewEducation.RowHeight = 23;
            this.gridViewEducation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewEducation_KeyDown);
            // 
            // colEducationLevel
            // 
            this.colEducationLevel.FieldName = "EducationLevel";
            this.colEducationLevel.Name = "colEducationLevel";
            this.colEducationLevel.Visible = true;
            this.colEducationLevel.VisibleIndex = 0;
            this.colEducationLevel.Width = 100;
            // 
            // colInstitute
            // 
            this.colInstitute.FieldName = "Institute";
            this.colInstitute.Name = "colInstitute";
            this.colInstitute.Visible = true;
            this.colInstitute.VisibleIndex = 1;
            this.colInstitute.Width = 148;
            // 
            // colMajor
            // 
            this.colMajor.FieldName = "Major";
            this.colMajor.Name = "colMajor";
            this.colMajor.Visible = true;
            this.colMajor.VisibleIndex = 2;
            this.colMajor.Width = 111;
            // 
            // colYear
            // 
            this.colYear.FieldName = "Year";
            this.colYear.Name = "colYear";
            this.colYear.Visible = true;
            this.colYear.VisibleIndex = 3;
            this.colYear.Width = 64;
            // 
            // colScore
            // 
            this.colScore.FieldName = "Score";
            this.colScore.Name = "colScore";
            this.colScore.Visible = true;
            this.colScore.VisibleIndex = 4;
            this.colScore.Width = 64;
            // 
            // colStartDate1
            // 
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 5;
            this.colStartDate1.Width = 108;
            // 
            // colEndDate1
            // 
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 6;
            this.colEndDate1.Width = 117;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.repositoryItemLookUpEdit2.Appearance.Options.UseFont = true;
            this.repositoryItemLookUpEdit2.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.repositoryItemLookUpEdit2.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("id", "id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.repositoryItemLookUpEdit2.DisplayMember = "Name";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.NullText = "";
            this.repositoryItemLookUpEdit2.ShowFooter = false;
            this.repositoryItemLookUpEdit2.ShowHeader = false;
            this.repositoryItemLookUpEdit2.ValueMember = "id";
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.gridControlLeaveEntitlement);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(764, 433);
            this.xtraTabPage6.Text = "Leave Entitlement";
            // 
            // gridControlLeaveEntitlement
            // 
            this.gridControlLeaveEntitlement.Location = new System.Drawing.Point(8, 8);
            this.gridControlLeaveEntitlement.MainView = this.gridViewLeaveEntitlement;
            this.gridControlLeaveEntitlement.Name = "gridControlLeaveEntitlement";
            this.gridControlLeaveEntitlement.Size = new System.Drawing.Size(300, 402);
            this.gridControlLeaveEntitlement.TabIndex = 6;
            this.gridControlLeaveEntitlement.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLeaveEntitlement});
            // 
            // gridViewLeaveEntitlement
            // 
            this.gridViewLeaveEntitlement.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewLeaveEntitlement.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewLeaveEntitlement.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridViewLeaveEntitlement.Appearance.GroupPanel.Options.UseFont = true;
            this.gridViewLeaveEntitlement.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewLeaveEntitlement.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridViewLeaveEntitlement.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewLeaveEntitlement.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridViewLeaveEntitlement.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridViewLeaveEntitlement.Appearance.Row.Options.UseFont = true;
            this.gridViewLeaveEntitlement.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFullName,
            this.colIDNo});
            this.gridViewLeaveEntitlement.GridControl = this.gridControlLeaveEntitlement;
            this.gridViewLeaveEntitlement.Name = "gridViewLeaveEntitlement";
            this.gridViewLeaveEntitlement.OptionsBehavior.Editable = false;
            this.gridViewLeaveEntitlement.OptionsCustomization.AllowGroup = false;
            this.gridViewLeaveEntitlement.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewLeaveEntitlement.OptionsView.ShowDetailButtons = false;
            this.gridViewLeaveEntitlement.OptionsView.ShowGroupPanel = false;
            this.gridViewLeaveEntitlement.RowHeight = 23;
            // 
            // colFullName
            // 
            this.colFullName.Caption = "Leave Type";
            this.colFullName.FieldName = "LeaveType";
            this.colFullName.Name = "colFullName";
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 0;
            this.colFullName.Width = 896;
            // 
            // colIDNo
            // 
            this.colIDNo.Caption = "Balance";
            this.colIDNo.FieldName = "Balance";
            this.colIDNo.Name = "colIDNo";
            this.colIDNo.Visible = true;
            this.colIDNo.VisibleIndex = 1;
            this.colIDNo.Width = 284;
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(697, 480);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(591, 480);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl32.Location = new System.Drawing.Point(325, 402);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(77, 17);
            this.labelControl32.TabIndex = 48;
            this.labelControl32.Text = "User Account";
            // 
            // cmbUserAccount
            // 
            this.cmbUserAccount.EditValue = "";
            this.cmbUserAccount.Location = new System.Drawing.Point(408, 399);
            this.cmbUserAccount.Name = "cmbUserAccount";
            this.cmbUserAccount.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbUserAccount.Properties.Appearance.Options.UseFont = true;
            this.cmbUserAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUserAccount.Properties.DisplayMember = "Name";
            this.cmbUserAccount.Properties.NullText = "";
            this.cmbUserAccount.Properties.ValueMember = "id";
            this.cmbUserAccount.Properties.View = this.gridView4;
            this.cmbUserAccount.Size = new System.Drawing.Size(295, 24);
            this.cmbUserAccount.TabIndex = 49;
            // 
            // gridView4
            // 
            this.gridView4.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView4.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView4.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView4.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView4.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView4.Appearance.Row.Options.UseFont = true;
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn13});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.RowHeight = 23;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "id";
            this.gridColumn12.FieldName = "id";
            this.gridColumn12.Name = "gridColumn12";
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Name";
            this.gridColumn13.FieldName = "Name";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            this.gridColumn13.Width = 264;
            // 
            // frmNewEditEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 522);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.xtraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewEditEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Employee";
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSIMC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSIMB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSIMA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKTP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBirthDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBirthDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbNationality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMaritalStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullName.Properties)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picPhoto.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEmergencyContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeEmergencyContactBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmergencyContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRemarks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalaryAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPayGrade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStartDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStartDate.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkExperience)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeWorkExperienceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkExperience)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            this.xtraTabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSkill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeSkillBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSkill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEducation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeeEducationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEducation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLeaveEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLeaveEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUserAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.TextEdit txtFullName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtIDNo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit cmbGender;
        private DevExpress.XtraEditors.ComboBoxEdit cmbMaritalStatus;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit cmbNationality;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtNPWP;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtPassport;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtSIMC;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtSIMB;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtSIMA;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtKTP;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.DateEdit cmbBirthDate;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraEditors.PictureEdit picPhoto;
        private DevExpress.XtraEditors.TextEdit txtOtherEmail;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtWorkEmail;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtPhone2;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtPhone1;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraGrid.GridControl gridControlEmergencyContact;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEmergencyContact;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit cmbName;
        private System.Windows.Forms.BindingSource employeeEmergencyContactBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colRelationship;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone1;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone2;
        private DevExpress.XtraEditors.DateEdit cmbEndDate;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.DateEdit cmbStartDate;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbPayGrade;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit txtRemarks;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtSalaryAmount;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtBranch;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit txtBank;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit txtAccountNumber;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraGrid.GridControl gridControlWorkExperience;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWorkExperience;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.GridControl gridControlSkill;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSkill;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraGrid.GridControl gridControlEducation;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEducation;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private System.Windows.Forms.BindingSource employeeWorkExperienceBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private System.Windows.Forms.BindingSource employeeEducationBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEducationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colInstitute;
        private DevExpress.XtraGrid.Columns.GridColumn colMajor;
        private DevExpress.XtraGrid.Columns.GridColumn colYear;
        private DevExpress.XtraGrid.Columns.GridColumn colScore;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private System.Windows.Forms.BindingSource employeeSkillBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colYear1;
        private DevExpress.XtraGrid.Columns.GridColumn colLanguange;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.MemoEdit txtAddress;
        public DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraGrid.GridControl gridControlLeaveEntitlement;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLeaveEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colIDNo;
        private DevExpress.XtraEditors.LookUpEdit cmbPosition;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl lblTerminateReason;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbUserAccount;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
    }
}