﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;

namespace GreenChem.MenuHR
{
    public partial class frmPayGrade : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPayGrade()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            payGradeBindingSource.DataSource = db.PayGrades.Where(x => x.Active == true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetRowCellValue(e.RowHandle, "Active", true);
        }
    }
}