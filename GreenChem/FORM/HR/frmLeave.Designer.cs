﻿namespace GreenChem.MenuHR
{
    partial class frmLeave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeaveDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeaveType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnHolidayList = new DevExpress.XtraEditors.SimpleButton();
            this.btnEntitlementList = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddEntitlement = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnLeaveType = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnNew = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 40);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(984, 522);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // gridView
            // 
            this.gridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView.Appearance.GroupPanel.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gridView.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView.Appearance.Row.Options.UseFont = true;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid,
            this.colEmployee,
            this.colFromDate,
            this.colToDate,
            this.colLeaveDay,
            this.colRemarks,
            this.colLeaveType});
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsView.ShowAutoFilterRow = true;
            this.gridView.OptionsView.ShowDetailButtons = false;
            this.gridView.RowHeight = 23;
            // 
            // colid
            // 
            this.colid.FieldName = "id";
            this.colid.Name = "colid";
            // 
            // colEmployee
            // 
            this.colEmployee.FieldName = "Employee";
            this.colEmployee.Name = "colEmployee";
            this.colEmployee.Visible = true;
            this.colEmployee.VisibleIndex = 0;
            this.colEmployee.Width = 295;
            // 
            // colFromDate
            // 
            this.colFromDate.FieldName = "FromDate";
            this.colFromDate.Name = "colFromDate";
            this.colFromDate.Visible = true;
            this.colFromDate.VisibleIndex = 1;
            this.colFromDate.Width = 123;
            // 
            // colToDate
            // 
            this.colToDate.FieldName = "ToDate";
            this.colToDate.Name = "colToDate";
            this.colToDate.Visible = true;
            this.colToDate.VisibleIndex = 2;
            this.colToDate.Width = 123;
            // 
            // colLeaveDay
            // 
            this.colLeaveDay.FieldName = "LeaveDay";
            this.colLeaveDay.Name = "colLeaveDay";
            this.colLeaveDay.Visible = true;
            this.colLeaveDay.VisibleIndex = 3;
            this.colLeaveDay.Width = 98;
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 5;
            this.colRemarks.Width = 174;
            // 
            // colLeaveType
            // 
            this.colLeaveType.FieldName = "LeaveType";
            this.colLeaveType.Name = "colLeaveType";
            this.colLeaveType.Visible = true;
            this.colLeaveType.VisibleIndex = 4;
            this.colLeaveType.Width = 150;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnHolidayList);
            this.panelControl1.Controls.Add(this.btnEntitlementList);
            this.panelControl1.Controls.Add(this.btnAddEntitlement);
            this.panelControl1.Controls.Add(this.btnDelete);
            this.panelControl1.Controls.Add(this.btnLeaveType);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnNew);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(984, 40);
            this.panelControl1.TabIndex = 0;
            // 
            // btnHolidayList
            // 
            this.btnHolidayList.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnHolidayList.Appearance.Options.UseFont = true;
            this.btnHolidayList.Location = new System.Drawing.Point(793, 5);
            this.btnHolidayList.Name = "btnHolidayList";
            this.btnHolidayList.Size = new System.Drawing.Size(120, 30);
            this.btnHolidayList.TabIndex = 6;
            this.btnHolidayList.Text = "Holiday List";
            this.btnHolidayList.Click += new System.EventHandler(this.btnHolidayList_Click);
            // 
            // btnEntitlementList
            // 
            this.btnEntitlementList.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEntitlementList.Appearance.Options.UseFont = true;
            this.btnEntitlementList.Location = new System.Drawing.Point(662, 5);
            this.btnEntitlementList.Name = "btnEntitlementList";
            this.btnEntitlementList.Size = new System.Drawing.Size(120, 30);
            this.btnEntitlementList.TabIndex = 5;
            this.btnEntitlementList.Text = "Entitlement List";
            this.btnEntitlementList.Click += new System.EventHandler(this.btnEntitlementList_Click);
            // 
            // btnAddEntitlement
            // 
            this.btnAddEntitlement.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnAddEntitlement.Appearance.Options.UseFont = true;
            this.btnAddEntitlement.Location = new System.Drawing.Point(536, 5);
            this.btnAddEntitlement.Name = "btnAddEntitlement";
            this.btnAddEntitlement.Size = new System.Drawing.Size(120, 30);
            this.btnAddEntitlement.TabIndex = 4;
            this.btnAddEntitlement.Text = "Add Entitlement";
            this.btnAddEntitlement.Click += new System.EventHandler(this.btnAddEntitlement_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Location = new System.Drawing.Point(264, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Delete Leave";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnLeaveType
            // 
            this.btnLeaveType.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnLeaveType.Appearance.Options.UseFont = true;
            this.btnLeaveType.Location = new System.Drawing.Point(400, 5);
            this.btnLeaveType.Name = "btnLeaveType";
            this.btnLeaveType.Size = new System.Drawing.Size(120, 30);
            this.btnLeaveType.TabIndex = 3;
            this.btnLeaveType.Text = "&Leave Type";
            this.btnLeaveType.Click += new System.EventHandler(this.btnLeaveType_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.Location = new System.Drawing.Point(138, 5);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(120, 30);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit Leave";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnNew.Appearance.Options.UseFont = true;
            this.btnNew.Location = new System.Drawing.Point(12, 5);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(120, 30);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&New Leave";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmLeave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "frmLeave";
            this.Text = "Leave";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnNew;
        private DevExpress.XtraEditors.SimpleButton btnLeaveType;
        private DevExpress.XtraGrid.Columns.GridColumn colid;
        private DevExpress.XtraGrid.Columns.GridColumn colFromDate;
        private DevExpress.XtraGrid.Columns.GridColumn colToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLeaveDay;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployee;
        private DevExpress.XtraGrid.Columns.GridColumn colLeaveType;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnAddEntitlement;
        private DevExpress.XtraEditors.SimpleButton btnEntitlementList;
        private DevExpress.XtraEditors.SimpleButton btnHolidayList;
    }
}