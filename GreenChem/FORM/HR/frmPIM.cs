﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuHR
{
    public partial class frmPIM : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPIM()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New PIM");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit PIM");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete PIM");
            btnView.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "View PIM");
            btnPayGrade.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Pay Grade");
            btnNationality.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Nationality PIM");
            btnPosition.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Position PIM");
            chkActive.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Active PIM");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.Employees
                            where x.Active == chkActive.Checked
                            orderby x.Status, x.FullName
                            select new
                            {
                                x.id,
                                x.FullName,
                                x.IDNo,
                                x.Gender,
                                x.MaritalStatus,
                                x.Nationality,
                                x.BirthDate,
                                x.KTP,
                                x.SIMA,
                                x.SIMB,
                                x.SIMC,
                                x.Passport,
                                x.NPWP,
                                x.Status,
                                x.Address,
                                x.Phone1,
                                x.Phone2,
                                x.WorkEmail,
                                x.OtherEmail,
                                x.StartDate,
                                x.EndDate,
                                PayGrade = x.PayGrade.Name,
                                x.SalaryAmount,
                                x.Remarks,
                                x.AccountNumber,
                                x.Bank,
                                x.Branch,
                                x.LeaveEntitlement,
                                Position = x.idPosition != null ? db.EmployeePositions.First(a => a.id == x.idPosition).Name : "",
                                LeaveBalance = x.LeaveEntitlement
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewEditEmployee form = new frmNewEditEmployee();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmNewEditEmployee form = new frmNewEditEmployee(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("FullName").Safe();
                DialogResult = XtraMessageBox.Show("Terminate this employee : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    frmTerminateEmployee form = new frmTerminateEmployee(id);
                    form.Text = "Terminate employee : " + name;
                    form.ShowDialog();

                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmNewEditEmployee form = new frmNewEditEmployee(id);
                form.btnSave.Enabled = false;
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnPayGrade_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmPayGrade form = new frmPayGrade();
                form.ShowDialog();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void chkNationality_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmNationality form = new frmNationality();
                form.ShowDialog();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnPosition_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEmployeePosition form = new frmEmployeePosition();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}