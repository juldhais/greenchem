﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace GreenChem.MenuHR
{
    public partial class frmEntitlementList : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmEntitlementList()
        {
            InitializeComponent();
            RefreshData();
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                DateTime limit = DateTime.Today.AddYears(-1);

                var query = from x in db.AddEntitlements
                            where x.TransactionDate >= limit
                            orderby x.TransactionDate descending
                            select new
                            {
                                x.id,
                                Employee = x.Employee.FullName,
                                x.TransactionDate,
                                LeaveType = x.LeaveType.Name,
                                x.Entitlement
                            };

                gridControl.DataSource = query;
                gridControl.RefreshDataSource();

            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}