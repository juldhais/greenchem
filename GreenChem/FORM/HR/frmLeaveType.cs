﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuHR
{
    public partial class frmLeaveType : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmLeaveType()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            leaveTypeBindingSource.DataSource = db.LeaveTypes.Where(x => x.Active == true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridView_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gridView.SetRowCellValue(e.RowHandle, "Active", true);
        }
    }
}