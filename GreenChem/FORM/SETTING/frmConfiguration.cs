﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuOthers
{
    public partial class frmConfiguration : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmConfiguration()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            txtSalesOrderPrefix.Text = db.GetConfiguration("SalesOrderPrefix");
            txtSalesOrderSuffix.Text = db.GetConfiguration("SalesOrderSuffix");
            txtGeneralLetterPrefix.Text = db.GetConfiguration("GeneralLetterPrefix");
            txtGeneralLetterSuffix.Text = db.GetConfiguration("GeneralLetterSuffix");
            txtInternalLetterPrefix.Text = db.GetConfiguration("InternalLetterPrefix");
            txtInternalLetterSuffix.Text = db.GetConfiguration("InternalLetterSuffix");
            txtAutoNumberPrefix.Text = db.GetConfiguration("POtoSupplierRegularPrefix");
            txtAutoNumberSuffix.Text = db.GetConfiguration("POtoSupplierRegularSuffix");
            txtPOEXPrefix.Text = db.GetConfiguration("POtoSupplierExpedisiPrefix");
            txtPOEXSuffix.Text = db.GetConfiguration("POtoSupplierExpedisiSuffix");
            txtInquiryInPrefix.Text = db.GetConfiguration("InquiryInPrefix");
            txtInquiryOutPrefix.Text = db.GetConfiguration("InquiryOutPrefix");
            txtQuotationInPrefix.Text = db.GetConfiguration("QuotationInPrefix");
            txtQuotationOutPrefix.Text = db.GetConfiguration("QuotationOutPrefix");
            txtInquiryInSuffix.Text = db.GetConfiguration("InquiryInSuffix");
            txtInquiryOutSuffix.Text = db.GetConfiguration("InquiryOutSuffix");
            txtQuotationInSuffix.Text = db.GetConfiguration("QuotationInSuffix");
            txtQuotationOutSuffix.Text = db.GetConfiguration("QuotationOutSuffix");
            txtPPBPrefix.Text = db.GetConfiguration("PPBPrefix");
            txtPPBSuffix.Text = db.GetConfiguration("PPBSuffix");
            txtPPSPrefix.Text = db.GetConfiguration("PPSPrefix");
            txtPPSSuffix.Text = db.GetConfiguration("PPSSuffix");
            txtExpensePrefix.Text = db.GetConfiguration("ExpensePrefix");
            txtExpenseSuffix.Text = db.GetConfiguration("ExpenseSuffix");
            txtCustomerSatisfactionPrefix.Text = db.GetConfiguration("CustomerSatisfactionPrefix");
            txtCustomerSatisfactionSuffix.Text = db.GetConfiguration("CustomerSatisfactionSuffix");
            txtCustomerVoicePrefix.Text = db.GetConfiguration("CustomerVoicePrefix");
            txtCustomerVoiceSuffix.Text = db.GetConfiguration("CustomerVoiceSuffix");
            txtPenyerahanProduksiPrefix.Text = db.GetConfiguration("PenyerahanProduksiPrefix");
            txtPenyerahanProduksiSuffix.Text = db.GetConfiguration("PenyerahanProduksiSuffix");
            txtTransferStockPrefix.Text = db.GetConfiguration("TransferStockPrefix");
            txtTransferStockSuffix.Text = db.GetConfiguration("TransferStockSuffix");
            txtInternalComplaintPrefix.Text = db.GetConfiguration("InternalComplaintPrefix");
            txtInternalComplaintSuffix.Text = db.GetConfiguration("InternalComplaintSuffix");

            txtFTPServer.Text = db.GetConfiguration("FTPServer");
            txtFTPFolder.Text = db.GetConfiguration("FTPFolder");
            txtFTPUsername.Text = db.GetConfiguration("FTPUsername");
            txtFTPPassword.Text = db.GetConfiguration("FTPPassword");

            txtSignature1.Text = db.GetConfiguration("Signature1");
            txtSignature2.Text = db.GetConfiguration("Signature2");
            txtSignature3.Text = db.GetConfiguration("Signature3");
            txtSignature4.Text = db.GetConfiguration("Signature4");

            txtPosition1.Text = db.GetConfiguration("Position1");
            txtPosition2.Text = db.GetConfiguration("Position2");
            txtPosition3.Text = db.GetConfiguration("Position3");
            txtPosition4.Text = db.GetConfiguration("Position4");

            txtPrefixPengembanganProduk.Text = db.GetConfiguration("PrefixPengembanganProduk");
            txtSuffixPengembanganProduk.Text = db.GetConfiguration("SuffixPengembanganProduk");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                db.SetConfiguration("SalesOrderPrefix", txtSalesOrderPrefix.Text);
                db.SetConfiguration("SalesOrderSuffix", txtSalesOrderSuffix.Text);
                db.SetConfiguration("GeneralLetterPrefix", txtGeneralLetterPrefix.Text);
                db.SetConfiguration("GeneralLetterSuffix", txtGeneralLetterSuffix.Text);
                db.SetConfiguration("InternalLetterPrefix", txtInternalLetterPrefix.Text);
                db.SetConfiguration("InternalLetterSuffix", txtInternalLetterSuffix.Text);
                db.SetConfiguration("POtoSupplierRegularPrefix", txtAutoNumberPrefix.Text);
                db.SetConfiguration("POtoSupplierRegularSuffix", txtAutoNumberSuffix.Text);
                db.SetConfiguration("POtoSupplierExpedisiPrefix", txtPOEXPrefix.Text);
                db.SetConfiguration("POtoSupplierExpedisiSuffix", txtPOEXSuffix.Text);
                db.SetConfiguration("InquiryInPrefix", txtInquiryInPrefix.Text);
                db.SetConfiguration("InquiryOutPrefix", txtInquiryOutPrefix.Text);
                db.SetConfiguration("QuotationInPrefix", txtQuotationInPrefix.Text);
                db.SetConfiguration("QuotationOutPrefix", txtQuotationOutPrefix.Text);
                db.SetConfiguration("InquiryInSuffix", txtInquiryInSuffix.Text);
                db.SetConfiguration("InquiryOutSuffix", txtInquiryOutSuffix.Text);
                db.SetConfiguration("QuotationInSuffix", txtQuotationInSuffix.Text);
                db.SetConfiguration("QuotationOutSuffix", txtQuotationOutSuffix.Text);
                db.SetConfiguration("PPBPrefix", txtPPBPrefix.Text);
                db.SetConfiguration("PPBSuffix", txtPPBSuffix.Text);
                db.SetConfiguration("PPSPrefix", txtPPSPrefix.Text);
                db.SetConfiguration("PPSSuffix", txtPPSSuffix.Text);
                db.SetConfiguration("ExpensePrefix", txtExpensePrefix.Text);
                db.SetConfiguration("ExpenseSuffix", txtExpenseSuffix.Text);
                db.SetConfiguration("CustomerSatisfactionPrefix", txtCustomerSatisfactionPrefix.Text);
                db.SetConfiguration("CustomerSatisfactionSuffix", txtCustomerSatisfactionSuffix.Text);
                db.SetConfiguration("CustomerVoicePrefix", txtCustomerVoicePrefix.Text);
                db.SetConfiguration("CustomerVoiceSuffix", txtCustomerVoiceSuffix.Text);
                db.SetConfiguration("PenyerahanProduksiPrefix", txtPenyerahanProduksiPrefix.Text);
                db.SetConfiguration("PenyerahanProduksiSuffix", txtPenyerahanProduksiSuffix.Text);
                db.SetConfiguration("InternalComplaintPrefix", txtInternalComplaintPrefix.Text);
                db.SetConfiguration("InternalComplaintSuffix", txtInternalComplaintSuffix.Text);

                db.SetConfiguration("FTPServer", txtFTPServer.Text);
                db.SetConfiguration("FTPFolder", txtFTPFolder.Text);
                db.SetConfiguration("FTPUsername", txtFTPUsername.Text);
                db.SetConfiguration("FTPPassword", txtFTPPassword.Text);

                db.SetConfiguration("Signature1", txtSignature1.Text);
                db.SetConfiguration("Signature2", txtSignature2.Text);
                db.SetConfiguration("Signature3", txtSignature3.Text);
                db.SetConfiguration("Signature4", txtSignature4.Text);
                
                db.SetConfiguration("Position1", txtPosition1.Text);
                db.SetConfiguration("Position2", txtPosition2.Text);
                db.SetConfiguration("Position3", txtPosition3.Text);
                db.SetConfiguration("Position4", txtPosition4.Text);

                db.SetConfiguration("PrefixPengembanganProduk", txtPrefixPengembanganProduk.Text);
                db.SetConfiguration("SuffixPengembanganProduk", txtSuffixPengembanganProduk.Text);

                XtraMessageBox.Show("Data saved successfully", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}