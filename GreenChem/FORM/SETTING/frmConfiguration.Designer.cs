﻿namespace GreenChem.MenuOthers
{
    partial class frmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtPOEXSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.txtPOEXPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.txtPenyerahanProduksiSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtPenyerahanProduksiPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustomerVoiceSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustomerVoicePrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustomerSatisfactionSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustomerSatisfactionPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtExpenseSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtExpensePrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txtPPSSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtPPSPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtPPBSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtPPBPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtInternalLetterSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtInternalLetterPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtQuotationOutSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtQuotationOutPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtQuotationInSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtQuotationInPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtInquiryOutSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtInquiryOutPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtInquiryInSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtInquiryInPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtAutoNumberSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtAutoNumberPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtGeneralLetterSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtGeneralLetterPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalesOrderSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtSalesOrderPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtInternalComplaintSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.txtInternalComplaintPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.txtPosition4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.txtPosition3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.txtPosition2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.txtPosition1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.txtSignature4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.txtSignature3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.txtSignature2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.txtSignature1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.txtTransferStockSuffix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtTransferStockPrefix = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.txtFTPPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.txtFTPUsername = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txtFTPFolder = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.txtFTPServer = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtSuffixPengembanganProduk = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.txtPrefixPengembanganProduk = new DevExpress.XtraEditors.TextEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPOEXSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPOEXPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenyerahanProduksiSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenyerahanProduksiPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerVoiceSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerVoicePrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerSatisfactionSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerSatisfactionPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpenseSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpensePrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPSSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPSPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPBSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPBPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalLetterSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalLetterPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationOutSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationOutPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationInSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationInPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryOutSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryOutPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryInSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryInPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAutoNumberSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAutoNumberPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGeneralLetterSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGeneralLetterPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesOrderSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesOrderPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalComplaintSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalComplaintPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferStockSuffix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferStockPrefix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPUsername.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuffixPengembanganProduk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrefixPengembanganProduk.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtSuffixPengembanganProduk);
            this.panelControl1.Controls.Add(this.labelControl47);
            this.panelControl1.Controls.Add(this.txtPrefixPengembanganProduk);
            this.panelControl1.Controls.Add(this.labelControl48);
            this.panelControl1.Controls.Add(this.txtPOEXSuffix);
            this.panelControl1.Controls.Add(this.labelControl33);
            this.panelControl1.Controls.Add(this.txtPOEXPrefix);
            this.panelControl1.Controls.Add(this.labelControl34);
            this.panelControl1.Controls.Add(this.txtPenyerahanProduksiSuffix);
            this.panelControl1.Controls.Add(this.labelControl31);
            this.panelControl1.Controls.Add(this.txtPenyerahanProduksiPrefix);
            this.panelControl1.Controls.Add(this.labelControl32);
            this.panelControl1.Controls.Add(this.txtCustomerVoiceSuffix);
            this.panelControl1.Controls.Add(this.labelControl29);
            this.panelControl1.Controls.Add(this.txtCustomerVoicePrefix);
            this.panelControl1.Controls.Add(this.labelControl30);
            this.panelControl1.Controls.Add(this.txtCustomerSatisfactionSuffix);
            this.panelControl1.Controls.Add(this.labelControl23);
            this.panelControl1.Controls.Add(this.txtCustomerSatisfactionPrefix);
            this.panelControl1.Controls.Add(this.labelControl24);
            this.panelControl1.Controls.Add(this.txtExpenseSuffix);
            this.panelControl1.Controls.Add(this.labelControl21);
            this.panelControl1.Controls.Add(this.txtExpensePrefix);
            this.panelControl1.Controls.Add(this.labelControl22);
            this.panelControl1.Controls.Add(this.txtPPSSuffix);
            this.panelControl1.Controls.Add(this.labelControl19);
            this.panelControl1.Controls.Add(this.txtPPSPrefix);
            this.panelControl1.Controls.Add(this.labelControl20);
            this.panelControl1.Controls.Add(this.txtPPBSuffix);
            this.panelControl1.Controls.Add(this.labelControl17);
            this.panelControl1.Controls.Add(this.txtPPBPrefix);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.txtInternalLetterSuffix);
            this.panelControl1.Controls.Add(this.labelControl15);
            this.panelControl1.Controls.Add(this.txtInternalLetterPrefix);
            this.panelControl1.Controls.Add(this.labelControl16);
            this.panelControl1.Controls.Add(this.txtQuotationOutSuffix);
            this.panelControl1.Controls.Add(this.labelControl11);
            this.panelControl1.Controls.Add(this.txtQuotationOutPrefix);
            this.panelControl1.Controls.Add(this.labelControl12);
            this.panelControl1.Controls.Add(this.txtQuotationInSuffix);
            this.panelControl1.Controls.Add(this.labelControl13);
            this.panelControl1.Controls.Add(this.txtQuotationInPrefix);
            this.panelControl1.Controls.Add(this.labelControl14);
            this.panelControl1.Controls.Add(this.txtInquiryOutSuffix);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.txtInquiryOutPrefix);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.txtInquiryInSuffix);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.txtInquiryInPrefix);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtAutoNumberSuffix);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtAutoNumberPrefix);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtGeneralLetterSuffix);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtGeneralLetterPrefix);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.txtSalesOrderSuffix);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtSalesOrderPrefix);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(401, 492);
            this.panelControl1.TabIndex = 0;
            // 
            // txtPOEXSuffix
            // 
            this.txtPOEXSuffix.Location = new System.Drawing.Point(283, 132);
            this.txtPOEXSuffix.Name = "txtPOEXSuffix";
            this.txtPOEXSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPOEXSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtPOEXSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtPOEXSuffix.TabIndex = 59;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl33.Location = new System.Drawing.Point(246, 135);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(31, 17);
            this.labelControl33.TabIndex = 58;
            this.labelControl33.Text = "Suffix\r\n";
            // 
            // txtPOEXPrefix
            // 
            this.txtPOEXPrefix.Location = new System.Drawing.Point(140, 132);
            this.txtPOEXPrefix.Name = "txtPOEXPrefix";
            this.txtPOEXPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPOEXPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtPOEXPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtPOEXPrefix.TabIndex = 57;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl34.Location = new System.Drawing.Point(16, 135);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(118, 17);
            this.labelControl34.TabIndex = 56;
            this.labelControl34.Text = "PO to Sup. Ex. Prefix";
            // 
            // txtPenyerahanProduksiSuffix
            // 
            this.txtPenyerahanProduksiSuffix.Location = new System.Drawing.Point(283, 432);
            this.txtPenyerahanProduksiSuffix.Name = "txtPenyerahanProduksiSuffix";
            this.txtPenyerahanProduksiSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPenyerahanProduksiSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtPenyerahanProduksiSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtPenyerahanProduksiSuffix.TabIndex = 55;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl31.Location = new System.Drawing.Point(246, 435);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(31, 17);
            this.labelControl31.TabIndex = 54;
            this.labelControl31.Text = "Suffix\r\n";
            // 
            // txtPenyerahanProduksiPrefix
            // 
            this.txtPenyerahanProduksiPrefix.Location = new System.Drawing.Point(140, 432);
            this.txtPenyerahanProduksiPrefix.Name = "txtPenyerahanProduksiPrefix";
            this.txtPenyerahanProduksiPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPenyerahanProduksiPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtPenyerahanProduksiPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtPenyerahanProduksiPrefix.TabIndex = 53;
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl32.Location = new System.Drawing.Point(14, 435);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(120, 17);
            this.labelControl32.TabIndex = 52;
            this.labelControl32.Text = "Peny. Produksi Prefix";
            // 
            // txtCustomerVoiceSuffix
            // 
            this.txtCustomerVoiceSuffix.Location = new System.Drawing.Point(283, 402);
            this.txtCustomerVoiceSuffix.Name = "txtCustomerVoiceSuffix";
            this.txtCustomerVoiceSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCustomerVoiceSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtCustomerVoiceSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtCustomerVoiceSuffix.TabIndex = 51;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl29.Location = new System.Drawing.Point(246, 405);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(31, 17);
            this.labelControl29.TabIndex = 50;
            this.labelControl29.Text = "Suffix\r\n";
            // 
            // txtCustomerVoicePrefix
            // 
            this.txtCustomerVoicePrefix.Location = new System.Drawing.Point(140, 402);
            this.txtCustomerVoicePrefix.Name = "txtCustomerVoicePrefix";
            this.txtCustomerVoicePrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCustomerVoicePrefix.Properties.Appearance.Options.UseFont = true;
            this.txtCustomerVoicePrefix.Size = new System.Drawing.Size(100, 24);
            this.txtCustomerVoicePrefix.TabIndex = 49;
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl30.Location = new System.Drawing.Point(34, 405);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(100, 17);
            this.labelControl30.TabIndex = 48;
            this.labelControl30.Text = "Cust. Voice Prefix";
            // 
            // txtCustomerSatisfactionSuffix
            // 
            this.txtCustomerSatisfactionSuffix.Location = new System.Drawing.Point(283, 372);
            this.txtCustomerSatisfactionSuffix.Name = "txtCustomerSatisfactionSuffix";
            this.txtCustomerSatisfactionSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCustomerSatisfactionSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtCustomerSatisfactionSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtCustomerSatisfactionSuffix.TabIndex = 47;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl23.Location = new System.Drawing.Point(246, 375);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(31, 17);
            this.labelControl23.TabIndex = 46;
            this.labelControl23.Text = "Suffix\r\n";
            // 
            // txtCustomerSatisfactionPrefix
            // 
            this.txtCustomerSatisfactionPrefix.Location = new System.Drawing.Point(140, 372);
            this.txtCustomerSatisfactionPrefix.Name = "txtCustomerSatisfactionPrefix";
            this.txtCustomerSatisfactionPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtCustomerSatisfactionPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtCustomerSatisfactionPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtCustomerSatisfactionPrefix.TabIndex = 45;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl24.Location = new System.Drawing.Point(36, 375);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(98, 17);
            this.labelControl24.TabIndex = 44;
            this.labelControl24.Text = "Cust. Satis. Prefix";
            // 
            // txtExpenseSuffix
            // 
            this.txtExpenseSuffix.Location = new System.Drawing.Point(283, 342);
            this.txtExpenseSuffix.Name = "txtExpenseSuffix";
            this.txtExpenseSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtExpenseSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtExpenseSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtExpenseSuffix.TabIndex = 43;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl21.Location = new System.Drawing.Point(246, 345);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(31, 17);
            this.labelControl21.TabIndex = 42;
            this.labelControl21.Text = "Suffix\r\n";
            // 
            // txtExpensePrefix
            // 
            this.txtExpensePrefix.Location = new System.Drawing.Point(140, 342);
            this.txtExpensePrefix.Name = "txtExpensePrefix";
            this.txtExpensePrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtExpensePrefix.Properties.Appearance.Options.UseFont = true;
            this.txtExpensePrefix.Size = new System.Drawing.Size(100, 24);
            this.txtExpensePrefix.TabIndex = 41;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl22.Location = new System.Drawing.Point(50, 345);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(84, 17);
            this.labelControl22.TabIndex = 40;
            this.labelControl22.Text = "Expense Prefix";
            // 
            // txtPPSSuffix
            // 
            this.txtPPSSuffix.Location = new System.Drawing.Point(283, 312);
            this.txtPPSSuffix.Name = "txtPPSSuffix";
            this.txtPPSSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPPSSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtPPSSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtPPSSuffix.TabIndex = 39;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl19.Location = new System.Drawing.Point(246, 315);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(31, 17);
            this.labelControl19.TabIndex = 38;
            this.labelControl19.Text = "Suffix\r\n";
            // 
            // txtPPSPrefix
            // 
            this.txtPPSPrefix.Location = new System.Drawing.Point(140, 312);
            this.txtPPSPrefix.Name = "txtPPSPrefix";
            this.txtPPSPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPPSPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtPPSPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtPPSPrefix.TabIndex = 37;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl20.Location = new System.Drawing.Point(77, 315);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(57, 17);
            this.labelControl20.TabIndex = 36;
            this.labelControl20.Text = "PPS Prefix";
            // 
            // txtPPBSuffix
            // 
            this.txtPPBSuffix.Location = new System.Drawing.Point(283, 282);
            this.txtPPBSuffix.Name = "txtPPBSuffix";
            this.txtPPBSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPPBSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtPPBSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtPPBSuffix.TabIndex = 35;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl17.Location = new System.Drawing.Point(246, 285);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(31, 17);
            this.labelControl17.TabIndex = 34;
            this.labelControl17.Text = "Suffix\r\n";
            // 
            // txtPPBPrefix
            // 
            this.txtPPBPrefix.Location = new System.Drawing.Point(140, 282);
            this.txtPPBPrefix.Name = "txtPPBPrefix";
            this.txtPPBPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPPBPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtPPBPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtPPBPrefix.TabIndex = 33;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl18.Location = new System.Drawing.Point(77, 285);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(57, 17);
            this.labelControl18.TabIndex = 32;
            this.labelControl18.Text = "PPB Prefix";
            // 
            // txtInternalLetterSuffix
            // 
            this.txtInternalLetterSuffix.Location = new System.Drawing.Point(283, 72);
            this.txtInternalLetterSuffix.Name = "txtInternalLetterSuffix";
            this.txtInternalLetterSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInternalLetterSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtInternalLetterSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtInternalLetterSuffix.TabIndex = 11;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl15.Location = new System.Drawing.Point(246, 75);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(31, 17);
            this.labelControl15.TabIndex = 10;
            this.labelControl15.Text = "Suffix\r\n";
            // 
            // txtInternalLetterPrefix
            // 
            this.txtInternalLetterPrefix.Location = new System.Drawing.Point(140, 72);
            this.txtInternalLetterPrefix.Name = "txtInternalLetterPrefix";
            this.txtInternalLetterPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInternalLetterPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtInternalLetterPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtInternalLetterPrefix.TabIndex = 9;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl16.Location = new System.Drawing.Point(18, 75);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(116, 17);
            this.labelControl16.TabIndex = 8;
            this.labelControl16.Text = "Internal Letter Prefix";
            // 
            // txtQuotationOutSuffix
            // 
            this.txtQuotationOutSuffix.Location = new System.Drawing.Point(283, 252);
            this.txtQuotationOutSuffix.Name = "txtQuotationOutSuffix";
            this.txtQuotationOutSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQuotationOutSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtQuotationOutSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtQuotationOutSuffix.TabIndex = 31;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl11.Location = new System.Drawing.Point(246, 255);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(31, 17);
            this.labelControl11.TabIndex = 30;
            this.labelControl11.Text = "Suffix\r\n";
            // 
            // txtQuotationOutPrefix
            // 
            this.txtQuotationOutPrefix.Location = new System.Drawing.Point(140, 252);
            this.txtQuotationOutPrefix.Name = "txtQuotationOutPrefix";
            this.txtQuotationOutPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQuotationOutPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtQuotationOutPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtQuotationOutPrefix.TabIndex = 29;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl12.Location = new System.Drawing.Point(15, 255);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(119, 17);
            this.labelControl12.TabIndex = 28;
            this.labelControl12.Text = "Quotation Out Prefix";
            // 
            // txtQuotationInSuffix
            // 
            this.txtQuotationInSuffix.Location = new System.Drawing.Point(283, 222);
            this.txtQuotationInSuffix.Name = "txtQuotationInSuffix";
            this.txtQuotationInSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQuotationInSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtQuotationInSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtQuotationInSuffix.TabIndex = 27;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl13.Location = new System.Drawing.Point(246, 225);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(31, 17);
            this.labelControl13.TabIndex = 26;
            this.labelControl13.Text = "Suffix\r\n";
            // 
            // txtQuotationInPrefix
            // 
            this.txtQuotationInPrefix.Location = new System.Drawing.Point(140, 222);
            this.txtQuotationInPrefix.Name = "txtQuotationInPrefix";
            this.txtQuotationInPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtQuotationInPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtQuotationInPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtQuotationInPrefix.TabIndex = 25;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl14.Location = new System.Drawing.Point(26, 225);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(108, 17);
            this.labelControl14.TabIndex = 24;
            this.labelControl14.Text = "Quotation In Prefix";
            // 
            // txtInquiryOutSuffix
            // 
            this.txtInquiryOutSuffix.Location = new System.Drawing.Point(283, 192);
            this.txtInquiryOutSuffix.Name = "txtInquiryOutSuffix";
            this.txtInquiryOutSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInquiryOutSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtInquiryOutSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtInquiryOutSuffix.TabIndex = 23;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl9.Location = new System.Drawing.Point(246, 195);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(31, 17);
            this.labelControl9.TabIndex = 22;
            this.labelControl9.Text = "Suffix\r\n";
            // 
            // txtInquiryOutPrefix
            // 
            this.txtInquiryOutPrefix.Location = new System.Drawing.Point(140, 192);
            this.txtInquiryOutPrefix.Name = "txtInquiryOutPrefix";
            this.txtInquiryOutPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInquiryOutPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtInquiryOutPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtInquiryOutPrefix.TabIndex = 21;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(34, 195);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(100, 17);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = "Inquiry Out Prefix";
            // 
            // txtInquiryInSuffix
            // 
            this.txtInquiryInSuffix.Location = new System.Drawing.Point(283, 162);
            this.txtInquiryInSuffix.Name = "txtInquiryInSuffix";
            this.txtInquiryInSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInquiryInSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtInquiryInSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtInquiryInSuffix.TabIndex = 19;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl7.Location = new System.Drawing.Point(246, 165);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(31, 17);
            this.labelControl7.TabIndex = 18;
            this.labelControl7.Text = "Suffix\r\n";
            // 
            // txtInquiryInPrefix
            // 
            this.txtInquiryInPrefix.Location = new System.Drawing.Point(140, 162);
            this.txtInquiryInPrefix.Name = "txtInquiryInPrefix";
            this.txtInquiryInPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInquiryInPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtInquiryInPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtInquiryInPrefix.TabIndex = 17;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(45, 165);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(89, 17);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Inquiry In Prefix";
            // 
            // txtAutoNumberSuffix
            // 
            this.txtAutoNumberSuffix.Location = new System.Drawing.Point(283, 102);
            this.txtAutoNumberSuffix.Name = "txtAutoNumberSuffix";
            this.txtAutoNumberSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAutoNumberSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtAutoNumberSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtAutoNumberSuffix.TabIndex = 15;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(246, 105);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(31, 17);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Suffix\r\n";
            // 
            // txtAutoNumberPrefix
            // 
            this.txtAutoNumberPrefix.Location = new System.Drawing.Point(140, 102);
            this.txtAutoNumberPrefix.Name = "txtAutoNumberPrefix";
            this.txtAutoNumberPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtAutoNumberPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtAutoNumberPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtAutoNumberPrefix.TabIndex = 13;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl6.Location = new System.Drawing.Point(6, 105);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(128, 17);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "PO to Sup. Reg. Prefix";
            // 
            // txtGeneralLetterSuffix
            // 
            this.txtGeneralLetterSuffix.Location = new System.Drawing.Point(283, 42);
            this.txtGeneralLetterSuffix.Name = "txtGeneralLetterSuffix";
            this.txtGeneralLetterSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtGeneralLetterSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtGeneralLetterSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtGeneralLetterSuffix.TabIndex = 7;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl4.Location = new System.Drawing.Point(246, 45);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(31, 17);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Suffix\r\n";
            // 
            // txtGeneralLetterPrefix
            // 
            this.txtGeneralLetterPrefix.Location = new System.Drawing.Point(140, 42);
            this.txtGeneralLetterPrefix.Name = "txtGeneralLetterPrefix";
            this.txtGeneralLetterPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtGeneralLetterPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtGeneralLetterPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtGeneralLetterPrefix.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Location = new System.Drawing.Point(15, 45);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(119, 17);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "External Letter Prefix";
            // 
            // txtSalesOrderSuffix
            // 
            this.txtSalesOrderSuffix.Location = new System.Drawing.Point(283, 12);
            this.txtSalesOrderSuffix.Name = "txtSalesOrderSuffix";
            this.txtSalesOrderSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalesOrderSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtSalesOrderSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtSalesOrderSuffix.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Location = new System.Drawing.Point(246, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(31, 17);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Suffix\r\n";
            // 
            // txtSalesOrderPrefix
            // 
            this.txtSalesOrderPrefix.Location = new System.Drawing.Point(140, 12);
            this.txtSalesOrderPrefix.Name = "txtSalesOrderPrefix";
            this.txtSalesOrderPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSalesOrderPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtSalesOrderPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtSalesOrderPrefix.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Location = new System.Drawing.Point(29, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(105, 17);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Sales Order Prefix";
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(722, 510);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(616, 510);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.txtInternalComplaintSuffix);
            this.panelControl2.Controls.Add(this.labelControl45);
            this.panelControl2.Controls.Add(this.txtInternalComplaintPrefix);
            this.panelControl2.Controls.Add(this.labelControl46);
            this.panelControl2.Controls.Add(this.txtPosition4);
            this.panelControl2.Controls.Add(this.labelControl44);
            this.panelControl2.Controls.Add(this.txtPosition3);
            this.panelControl2.Controls.Add(this.labelControl43);
            this.panelControl2.Controls.Add(this.txtPosition2);
            this.panelControl2.Controls.Add(this.labelControl42);
            this.panelControl2.Controls.Add(this.txtPosition1);
            this.panelControl2.Controls.Add(this.labelControl41);
            this.panelControl2.Controls.Add(this.txtSignature4);
            this.panelControl2.Controls.Add(this.labelControl40);
            this.panelControl2.Controls.Add(this.txtSignature3);
            this.panelControl2.Controls.Add(this.labelControl39);
            this.panelControl2.Controls.Add(this.txtSignature2);
            this.panelControl2.Controls.Add(this.labelControl38);
            this.panelControl2.Controls.Add(this.txtSignature1);
            this.panelControl2.Controls.Add(this.labelControl37);
            this.panelControl2.Controls.Add(this.txtTransferStockSuffix);
            this.panelControl2.Controls.Add(this.labelControl35);
            this.panelControl2.Controls.Add(this.txtTransferStockPrefix);
            this.panelControl2.Controls.Add(this.labelControl36);
            this.panelControl2.Controls.Add(this.txtFTPPassword);
            this.panelControl2.Controls.Add(this.labelControl28);
            this.panelControl2.Controls.Add(this.txtFTPUsername);
            this.panelControl2.Controls.Add(this.labelControl27);
            this.panelControl2.Controls.Add(this.txtFTPFolder);
            this.panelControl2.Controls.Add(this.labelControl26);
            this.panelControl2.Controls.Add(this.txtFTPServer);
            this.panelControl2.Controls.Add(this.labelControl25);
            this.panelControl2.Location = new System.Drawing.Point(419, 12);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(383, 492);
            this.panelControl2.TabIndex = 3;
            // 
            // txtInternalComplaintSuffix
            // 
            this.txtInternalComplaintSuffix.Location = new System.Drawing.Point(278, 423);
            this.txtInternalComplaintSuffix.Name = "txtInternalComplaintSuffix";
            this.txtInternalComplaintSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInternalComplaintSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtInternalComplaintSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtInternalComplaintSuffix.TabIndex = 32;
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl45.Location = new System.Drawing.Point(241, 426);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(31, 17);
            this.labelControl45.TabIndex = 33;
            this.labelControl45.Text = "Suffix\r\n";
            // 
            // txtInternalComplaintPrefix
            // 
            this.txtInternalComplaintPrefix.Location = new System.Drawing.Point(135, 423);
            this.txtInternalComplaintPrefix.Name = "txtInternalComplaintPrefix";
            this.txtInternalComplaintPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtInternalComplaintPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtInternalComplaintPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtInternalComplaintPrefix.TabIndex = 31;
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl46.Location = new System.Drawing.Point(13, 426);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(116, 17);
            this.labelControl46.TabIndex = 30;
            this.labelControl46.Text = "Int. Complaint Prefix";
            // 
            // txtPosition4
            // 
            this.txtPosition4.Location = new System.Drawing.Point(102, 353);
            this.txtPosition4.Name = "txtPosition4";
            this.txtPosition4.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPosition4.Properties.Appearance.Options.UseFont = true;
            this.txtPosition4.Size = new System.Drawing.Size(260, 24);
            this.txtPosition4.TabIndex = 29;
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl44.Location = new System.Drawing.Point(13, 356);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(53, 17);
            this.labelControl44.TabIndex = 28;
            this.labelControl44.Text = "Position4";
            // 
            // txtPosition3
            // 
            this.txtPosition3.Location = new System.Drawing.Point(102, 323);
            this.txtPosition3.Name = "txtPosition3";
            this.txtPosition3.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPosition3.Properties.Appearance.Options.UseFont = true;
            this.txtPosition3.Size = new System.Drawing.Size(260, 24);
            this.txtPosition3.TabIndex = 27;
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl43.Location = new System.Drawing.Point(13, 326);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(53, 17);
            this.labelControl43.TabIndex = 26;
            this.labelControl43.Text = "Position3";
            // 
            // txtPosition2
            // 
            this.txtPosition2.Location = new System.Drawing.Point(102, 293);
            this.txtPosition2.Name = "txtPosition2";
            this.txtPosition2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPosition2.Properties.Appearance.Options.UseFont = true;
            this.txtPosition2.Size = new System.Drawing.Size(260, 24);
            this.txtPosition2.TabIndex = 25;
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl42.Location = new System.Drawing.Point(13, 296);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(53, 17);
            this.labelControl42.TabIndex = 24;
            this.labelControl42.Text = "Position2";
            // 
            // txtPosition1
            // 
            this.txtPosition1.Location = new System.Drawing.Point(102, 263);
            this.txtPosition1.Name = "txtPosition1";
            this.txtPosition1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPosition1.Properties.Appearance.Options.UseFont = true;
            this.txtPosition1.Size = new System.Drawing.Size(260, 24);
            this.txtPosition1.TabIndex = 23;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl41.Location = new System.Drawing.Point(13, 266);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(53, 17);
            this.labelControl41.TabIndex = 22;
            this.labelControl41.Text = "Position1";
            // 
            // txtSignature4
            // 
            this.txtSignature4.Location = new System.Drawing.Point(102, 228);
            this.txtSignature4.Name = "txtSignature4";
            this.txtSignature4.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSignature4.Properties.Appearance.Options.UseFont = true;
            this.txtSignature4.Size = new System.Drawing.Size(260, 24);
            this.txtSignature4.TabIndex = 21;
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl40.Location = new System.Drawing.Point(13, 231);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(62, 17);
            this.labelControl40.TabIndex = 20;
            this.labelControl40.Text = "Signature4";
            // 
            // txtSignature3
            // 
            this.txtSignature3.Location = new System.Drawing.Point(102, 198);
            this.txtSignature3.Name = "txtSignature3";
            this.txtSignature3.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSignature3.Properties.Appearance.Options.UseFont = true;
            this.txtSignature3.Size = new System.Drawing.Size(260, 24);
            this.txtSignature3.TabIndex = 19;
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl39.Location = new System.Drawing.Point(13, 201);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(62, 17);
            this.labelControl39.TabIndex = 18;
            this.labelControl39.Text = "Signature3";
            // 
            // txtSignature2
            // 
            this.txtSignature2.Location = new System.Drawing.Point(102, 168);
            this.txtSignature2.Name = "txtSignature2";
            this.txtSignature2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSignature2.Properties.Appearance.Options.UseFont = true;
            this.txtSignature2.Size = new System.Drawing.Size(260, 24);
            this.txtSignature2.TabIndex = 17;
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl38.Location = new System.Drawing.Point(13, 171);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(62, 17);
            this.labelControl38.TabIndex = 16;
            this.labelControl38.Text = "Signature2";
            // 
            // txtSignature1
            // 
            this.txtSignature1.Location = new System.Drawing.Point(102, 138);
            this.txtSignature1.Name = "txtSignature1";
            this.txtSignature1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSignature1.Properties.Appearance.Options.UseFont = true;
            this.txtSignature1.Size = new System.Drawing.Size(260, 24);
            this.txtSignature1.TabIndex = 15;
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl37.Location = new System.Drawing.Point(13, 141);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(62, 17);
            this.labelControl37.TabIndex = 14;
            this.labelControl37.Text = "Signature1";
            // 
            // txtTransferStockSuffix
            // 
            this.txtTransferStockSuffix.Location = new System.Drawing.Point(278, 393);
            this.txtTransferStockSuffix.Name = "txtTransferStockSuffix";
            this.txtTransferStockSuffix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTransferStockSuffix.Properties.Appearance.Options.UseFont = true;
            this.txtTransferStockSuffix.Size = new System.Drawing.Size(100, 24);
            this.txtTransferStockSuffix.TabIndex = 12;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl35.Location = new System.Drawing.Point(241, 396);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(31, 17);
            this.labelControl35.TabIndex = 13;
            this.labelControl35.Text = "Suffix\r\n";
            // 
            // txtTransferStockPrefix
            // 
            this.txtTransferStockPrefix.Location = new System.Drawing.Point(135, 393);
            this.txtTransferStockPrefix.Name = "txtTransferStockPrefix";
            this.txtTransferStockPrefix.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtTransferStockPrefix.Properties.Appearance.Options.UseFont = true;
            this.txtTransferStockPrefix.Size = new System.Drawing.Size(100, 24);
            this.txtTransferStockPrefix.TabIndex = 11;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl36.Location = new System.Drawing.Point(10, 396);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(119, 17);
            this.labelControl36.TabIndex = 10;
            this.labelControl36.Text = "Transfer Stock Prefix";
            // 
            // txtFTPPassword
            // 
            this.txtFTPPassword.Location = new System.Drawing.Point(102, 102);
            this.txtFTPPassword.Name = "txtFTPPassword";
            this.txtFTPPassword.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFTPPassword.Properties.Appearance.Options.UseFont = true;
            this.txtFTPPassword.Size = new System.Drawing.Size(260, 24);
            this.txtFTPPassword.TabIndex = 9;
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl28.Location = new System.Drawing.Point(13, 105);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(80, 17);
            this.labelControl28.TabIndex = 8;
            this.labelControl28.Text = "FTP Password";
            // 
            // txtFTPUsername
            // 
            this.txtFTPUsername.Location = new System.Drawing.Point(102, 72);
            this.txtFTPUsername.Name = "txtFTPUsername";
            this.txtFTPUsername.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFTPUsername.Properties.Appearance.Options.UseFont = true;
            this.txtFTPUsername.Size = new System.Drawing.Size(260, 24);
            this.txtFTPUsername.TabIndex = 7;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl27.Location = new System.Drawing.Point(13, 75);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(83, 17);
            this.labelControl27.TabIndex = 6;
            this.labelControl27.Text = "FTP Username";
            // 
            // txtFTPFolder
            // 
            this.txtFTPFolder.Location = new System.Drawing.Point(102, 42);
            this.txtFTPFolder.Name = "txtFTPFolder";
            this.txtFTPFolder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFTPFolder.Properties.Appearance.Options.UseFont = true;
            this.txtFTPFolder.Size = new System.Drawing.Size(260, 24);
            this.txtFTPFolder.TabIndex = 5;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl26.Location = new System.Drawing.Point(13, 45);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(61, 17);
            this.labelControl26.TabIndex = 4;
            this.labelControl26.Text = "FTP Folder";
            // 
            // txtFTPServer
            // 
            this.txtFTPServer.Location = new System.Drawing.Point(102, 12);
            this.txtFTPServer.Name = "txtFTPServer";
            this.txtFTPServer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtFTPServer.Properties.Appearance.Options.UseFont = true;
            this.txtFTPServer.Size = new System.Drawing.Size(260, 24);
            this.txtFTPServer.TabIndex = 3;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl25.Location = new System.Drawing.Point(13, 15);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(61, 17);
            this.labelControl25.TabIndex = 2;
            this.labelControl25.Text = "FTP Server";
            // 
            // txtSuffixPengembanganProduk
            // 
            this.txtSuffixPengembanganProduk.Location = new System.Drawing.Point(283, 462);
            this.txtSuffixPengembanganProduk.Name = "txtSuffixPengembanganProduk";
            this.txtSuffixPengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtSuffixPengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.txtSuffixPengembanganProduk.Size = new System.Drawing.Size(100, 24);
            this.txtSuffixPengembanganProduk.TabIndex = 63;
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl47.Location = new System.Drawing.Point(246, 465);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(31, 17);
            this.labelControl47.TabIndex = 62;
            this.labelControl47.Text = "Suffix\r\n";
            // 
            // txtPrefixPengembanganProduk
            // 
            this.txtPrefixPengembanganProduk.Location = new System.Drawing.Point(140, 462);
            this.txtPrefixPengembanganProduk.Name = "txtPrefixPengembanganProduk";
            this.txtPrefixPengembanganProduk.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPrefixPengembanganProduk.Properties.Appearance.Options.UseFont = true;
            this.txtPrefixPengembanganProduk.Size = new System.Drawing.Size(100, 24);
            this.txtPrefixPengembanganProduk.TabIndex = 61;
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl48.Location = new System.Drawing.Point(14, 465);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(113, 17);
            this.labelControl48.TabIndex = 60;
            this.labelControl48.Text = "Peng. Produk Prefix";
            // 
            // frmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 552);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPOEXSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPOEXPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenyerahanProduksiSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPenyerahanProduksiPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerVoiceSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerVoicePrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerSatisfactionSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerSatisfactionPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpenseSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExpensePrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPSSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPSPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPBSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPPBPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalLetterSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalLetterPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationOutSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationOutPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationInSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuotationInPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryOutSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryOutPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryInSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInquiryInPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAutoNumberSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAutoNumberPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGeneralLetterSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGeneralLetterPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesOrderSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalesOrderPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalComplaintSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInternalComplaintPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPosition1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignature1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferStockSuffix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransferStockPrefix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPUsername.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFTPServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuffixPengembanganProduk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrefixPengembanganProduk.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtSalesOrderPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtSalesOrderSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtGeneralLetterSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtGeneralLetterPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtAutoNumberSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtAutoNumberPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtQuotationOutSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtQuotationOutPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtQuotationInSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtQuotationInPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtInquiryOutSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtInquiryOutPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtInquiryInSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtInquiryInPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtInternalLetterSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtInternalLetterPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtPPBSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtPPBPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtPPSSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtPPSPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtExpenseSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtExpensePrefix;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtCustomerSatisfactionSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit txtCustomerSatisfactionPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit txtFTPPassword;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit txtFTPUsername;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit txtFTPFolder;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit txtFTPServer;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtCustomerVoiceSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.TextEdit txtCustomerVoicePrefix;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit txtPenyerahanProduksiSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit txtPenyerahanProduksiPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.TextEdit txtPOEXSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.TextEdit txtPOEXPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.TextEdit txtTransferStockSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtTransferStockPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit txtPosition4;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.TextEdit txtPosition3;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit txtPosition2;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit txtPosition1;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.TextEdit txtSignature4;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit txtSignature3;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit txtSignature2;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.TextEdit txtSignature1;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit txtInternalComplaintSuffix;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.TextEdit txtInternalComplaintPrefix;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.TextEdit txtSuffixPengembanganProduk;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.TextEdit txtPrefixPengembanganProduk;
        private DevExpress.XtraEditors.LabelControl labelControl48;
    }
}