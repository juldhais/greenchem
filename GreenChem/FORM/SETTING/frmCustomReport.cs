﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using GreenChem.MenuReport;
using System.IO;
using GreenChem.MenuTransaction;

namespace GreenChem.MenuOthers
{
    public partial class frmCustomReport : DevExpress.XtraEditors.XtraForm
    {
        public frmCustomReport()
        {
            InitializeComponent();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (cmbReport.Text == "Sales Order")
                {
                    rptSalesOrder report = new rptSalesOrder();
                    string name = "SalesOrder.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Delivery Order")
                {
                    rptDeliveryOrder report = new rptDeliveryOrder();
                    string name = "DeliveryOrder.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Sales Invoice")
                {
                    rptSalesInvoice report = new rptSalesInvoice();
                    string name = "SalesInvoice.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "External Letter Report")
                {
                    rptGeneralLetter report = new rptGeneralLetter();
                    string name = "ExternalLetterReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Internal Letter Report")
                {
                    rptGeneralLetter report = new rptGeneralLetter();
                    string name = "InternalLetterReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "PO to Supplier Report")
                {
                    rptPOtoSupplier report = new rptPOtoSupplier();
                    string name = "POtoSupplierReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Quotation In Report")
                {
                    rptQuotationIn report = new rptQuotationIn();
                    string name = "QuotationInReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Quotation Out Report")
                {
                    rptQuotationOut report = new rptQuotationOut();
                    string name = "QuotationOutReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Inquiry In Report")
                {
                    rptInquiryIn report = new rptInquiryIn();
                    string name = "InquiryInReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Inquiry Out Report")
                {
                    rptInquiryOut report = new rptInquiryOut();
                    string name = "InquiryOutReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "PPB Report")
                {
                    rptPPB report = new rptPPB();
                    string name = "PPBReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "PPS Report")
                {
                    rptPPS report = new rptPPS();
                    string name = "PPSReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Expense Report")
                {
                    rptExpense report = new rptExpense();
                    string name = "ExpenseReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Sales Order Summary")
                {
                    rptSalesOrderSummary report = new rptSalesOrderSummary();
                    string name = "SalesOrderSummary.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Delivery Order Summary")
                {
                    rptDeliveryOrderSummary report = new rptDeliveryOrderSummary();
                    string name = "DeliveryOrderSummary.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Sales Invoice Summary")
                {
                    rptSalesInvoiceSummary report = new rptSalesInvoiceSummary();
                    string name = "SalesInvoiceSummary.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Sales Order Detail")
                {
                    rptDetailSalesOrder report = new rptDetailSalesOrder();
                    string name = "SalesOrderDetail.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Delivery Order Location")
                {
                    rptDOLocation report = new rptDOLocation();
                    string name = "DeliveryOrderLocation.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Bahan Baku Report")
                {
                    rptBahanBaku report = new rptBahanBaku();
                    string name = "BahanBakuReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Bahan Penolong Report")
                {
                    rptBahanBaku report = new rptBahanBaku();
                    string name = "BahanPenolongReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Stock Report")
                {
                    rptStock report = new rptStock();
                    string name = "StockReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Stock Min Report")
                {
                    rptStockMin report = new rptStockMin();
                    string name = "StockMinReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Stock Max Report")
                {
                    rptStockMax report = new rptStockMax();
                    string name = "StockMaxReport.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Lab Service")
                {
                    rptLabService report = new rptLabService();
                    string name = "LabService.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Standard Delivery Time")
                {
                    rptStandardDeliveryTime report = new rptStandardDeliveryTime();
                    string name = "StandardDeliveryTime.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "PO to Supplier Regular")
                {
                    rptPOtoSupplierRegular report = new rptPOtoSupplierRegular();
                    string name = "POtoSupplierRegular.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "PO to Supplier Expedisi")
                {
                    rptPOtoSupplierExpedisi report = new rptPOtoSupplierExpedisi();
                    string name = "POtoSupplierExpedisi.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Omset Penjualan")
                {
                    rptOmsetPenjualan report = new rptOmsetPenjualan();
                    string name = "rptOmsetPenjualan.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Transfer Stock")
                {
                    rptTransferStock report = new rptTransferStock();
                    string name = "rptTransferStock.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Active Employee")
                {
                    rptEmployee report = new rptEmployee();
                    string name = "rptActiveEmployee.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Terminated Employee")
                {
                    rptEmployee report = new rptEmployee();
                    string name = "rptTerminatedEmployee.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "All Employee")
                {
                    rptEmployee report = new rptEmployee();
                    string name = "rptAllEmployee.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
                else if (cmbReport.Text == "Leave Balance")
                {
                    rptEmployee report = new rptEmployee();
                    string name = "rptLeaveBalance.repx";
                    if (File.Exists(name))
                        report.LoadLayout(name);
                    else report.SaveLayout(name);

                    report.DisplayName = name;
                    report.ShowDesigner();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}