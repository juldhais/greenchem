﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuReport
{
    public partial class frmReportPenyerahanProduksi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportPenyerahanProduksi()
        {
            InitializeComponent();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.DetailPenyerahanProduksis
                            where x.PenyerahanProduksi.TanggalPermintaanProduksi.Value.Date >= cmbStartDate.DateTime.Date && x.PenyerahanProduksi.TanggalPermintaanProduksi.Value.Date <= cmbEndDate.DateTime.Date
                            select new
                            {
                                x.PenyerahanProduksi.NomorPermintaanProduksi,
                                x.PenyerahanProduksi.TanggalPermintaanProduksi,
                                x.PenyerahanProduksi.TanggalPenyerahan,
                                x.PenyerahanProduksi.TanggalSelesaiProduksi,
                                x.PenyerahanProduksi.YangMenyerahkan,
                                x.PenyerahanProduksi.DisetujuiOleh,
                                x.PenyerahanProduksi.DiterimaOleh,
                                ProductCode = x.Product.Code,
                                ProductName = x.Product.Name,
                                x.Kemasan,
                                x.Quantity,
                                x.UoM
                            };

                rptPenyerahanProduksi report = new rptPenyerahanProduksi();
                report.DataSource = query;
                report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " to " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}