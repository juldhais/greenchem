﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.IO;

namespace GreenChem.MenuReport
{
    public partial class frmStockReport : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmStockReport()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            cmbCategory.Properties.DataSource = db.Categories.GetName();
            cmbProduct.Properties.DataSource = db.Products.GetName();
        }

        private void StockMutation()
        {
            if (cmbProduct.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Choose the product first.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbProduct.Focus();
                return;
            }

            var product = db.Products.First(x => x.id == cmbProduct.EditValue.ToInteger());
            
            var produksi = from x in db.DetailPenyerahanProduksis
                           where x.idProduct == product.id
                           && x.PenyerahanProduksi.Approved2 == true
                           && x.PenyerahanProduksi.TanggalPenyerahan.Value.Date >= cmbStartDate.DateTime.Date
                           && x.PenyerahanProduksi.TanggalPenyerahan.Value.Date <= cmbEndDate.DateTime.Date
                           select new
                           {
                               TransactionNumber = x.PenyerahanProduksi.NomorPermintaanProduksi,
                               TransactionDate = x.PenyerahanProduksi.TanggalPenyerahan.Value,
                               Quantity = x.Quantity.Value,
                               TransactionType = "Penyerahan Produksi",
                               Remarks = x.Kemasan,
                               Stock = x.PenyerahanProduksi.Stock
                           };

            var delivery = from x in db.DetailSalesOrders
                           where x.idProduct == product.id
                           && x.SalesOrder.DateDelivered != null
                           && x.SalesOrder.DateDelivered.Value.Date >= cmbStartDate.DateTime.Date
                           && x.SalesOrder.DateDelivered.Value.Date <= cmbEndDate.DateTime.Date
                           select new
                           {
                               TransactionNumber = x.SalesOrder.TransactionNumber,
                               TransactionDate = x.SalesOrder.DateDelivered.Value,
                               Quantity = x.QuantityDelivered * -1,
                               TransactionType = "Sales Order",
                               Remarks = x.SalesOrder.Customer.Name,
                               Stock = x.SalesOrder.Stock
                           };

            var adjustment = from x in db.AdjustStocks
                             where x.idProduct == product.id
                             && x.TransactionDate.Date >= cmbStartDate.DateTime.Date
                             && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                             select new
                             {
                                 TransactionNumber = x.TransactionNumber,
                                 TransactionDate = x.TransactionDate,
                                 Quantity = x.Quantity,
                                 TransactionType = "Stock Adjustment",
                                 Remarks = x.Remarks,
                                 Stock = "JKT"
                             };

            var transferstockin = from x in db.TransferStocks
                                where x.idProduct == product.id
                                && x.Approved2 == true
                                && x.TransactionDate.Date >= cmbStartDate.DateTime.Date
                                && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                                select new
                                {
                                    TransactionNumber = x.TransactionNumber,
                                    TransactionDate = x.TransactionDate,
                                    Quantity = x.Quantity.HasValue ?  x.Quantity.Value : 0,
                                    TransactionType = "Transfer Stock",
                                    Remarks = x.Remarks,
                                    Stock = x.ToLocation
                                };

            var transferstockout = from x in db.TransferStocks
                                  where x.idProduct == product.id
                                  && x.Approved2 == true
                                  && x.TransactionDate.Date >= cmbStartDate.DateTime.Date
                                  && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                                  select new
                                  {
                                      TransactionNumber = x.TransactionNumber,
                                      TransactionDate = x.TransactionDate,
                                      Quantity = x.Quantity.HasValue ? x.Quantity.Value * -1 : 0,
                                      TransactionType = "Transfer Stock",
                                      Remarks = x.Remarks,
                                      Stock = x.FromLocation
                                  };

            //query akhir setelah cmbEndDate untuk hitung stock awal
            var akhirproduksi = from x in db.DetailPenyerahanProduksis
                                where x.idProduct == product.id
                                && x.PenyerahanProduksi.Approved2 == true
                                && x.PenyerahanProduksi.TanggalPenyerahan.Value.Date > cmbEndDate.DateTime.Date
                                select new
                                {
                                    Quantity = x.Quantity.Value,
                                    Stock = x.PenyerahanProduksi.Stock
                                };

            var akhirdelivery = from x in db.DetailSalesOrders
                                where x.idProduct == product.id
                                && x.SalesOrder.DateDelivered != null
                                && x.SalesOrder.DateDelivered.Value.Date > cmbEndDate.DateTime.Date
                                select new
                                {
                                    Quantity = x.QuantityDelivered * -1,
                                    Stock = x.SalesOrder.Stock
                                };

            var akhiradjustment = from x in db.AdjustStocks
                                  where x.idProduct == product.id
                                  && x.TransactionDate.Date > cmbEndDate.DateTime.Date
                                  select new
                                  {
                                      Quantity = x.Quantity,
                                      Stock = "JKT"
                                  };

            var akhirtransferstockin = from x in db.TransferStocks
                                     where x.idProduct == product.id
                                     && x.Approved2 == true
                                     && x.TransactionDate.Date > cmbEndDate.DateTime.Date
                                     select new
                                     {
                                         Quantity = x.Quantity.HasValue ? x.Quantity.Value : 0,
                                         Stock = x.ToLocation
                                     };

            var akhirtransferstockout = from x in db.TransferStocks
                                       where x.idProduct == product.id
                                       && x.Approved2 == true
                                       && x.TransactionDate.Date > cmbEndDate.DateTime.Date
                                       select new
                                       {
                                           Quantity = x.Quantity.HasValue ? x.Quantity.Value * -1 : 0,
                                           Stock = x.FromLocation
                                       };

            var queryconcat = produksi.Concat(delivery).Concat(adjustment).Concat(transferstockin).Concat(transferstockout);
            var queryconcatakhir = akhirproduksi.Concat(akhirdelivery).Concat(akhiradjustment).Concat(akhirtransferstockin).Concat(akhirtransferstockout);

            List<ReportStockMutation> list = new List<ReportStockMutation>();

            //obal
            decimal qtyobal = product.Stock + product.Stock2.ToDecimal();

            if (cmbStock.Text == "JKT")
            {
                queryconcat = queryconcat.Where(x => x.Stock == "JKT" || x.Stock == "" || x.Stock == null);
                queryconcatakhir = queryconcatakhir.Where(x => x.Stock == "JKT" || x.Stock == "" || x.Stock == null);
                qtyobal = product.Stock;
            }
            else if (cmbStock.Text == "BPP")
            {
                queryconcat = queryconcat.Where(x => x.Stock == "BPP");
                queryconcatakhir = queryconcatakhir.Where(x => x.Stock == "BPP");
                qtyobal = product.Stock2.ToDecimal();
            }
            else if (cmbStock.Text == "BTG")
            {
                queryconcat = queryconcat.Where(x => x.Stock == "BTG");
                queryconcatakhir = queryconcatakhir.Where(x => x.Stock == "BTG");
                qtyobal = product.Stock3.ToDecimal();
            }

            if (queryconcat.Any()) qtyobal -= queryconcat.Sum(x => x.Quantity);
            if (queryconcatakhir.Any()) qtyobal -= queryconcatakhir.Sum(x => x.Quantity);

            ReportStockMutation obal = new ReportStockMutation();
            obal.TransactionNumber = "0/BAL";
            obal.TransactionDate = cmbStartDate.DateTime.Date;
            obal.TransactionType = "Opening Balance";
            obal.Remarks = "Opening Balance";
            obal.QtyBalance = qtyobal;
            list.Add(obal);

            //detail
            foreach (var item in queryconcat.OrderBy(x => x.TransactionDate).ThenByDescending(x => x.Quantity))
            {
                ReportStockMutation detail = new ReportStockMutation();
                detail.TransactionNumber = item.TransactionNumber;
                detail.TransactionDate = item.TransactionDate;
                detail.TransactionType = item.TransactionType;
                detail.Remarks = item.Remarks;
                if (item.Quantity > 0) detail.QtyIn = item.Quantity;
                else detail.QtyOut = item.Quantity * -1;
                qtyobal = qtyobal + detail.QtyIn - detail.QtyOut;
                detail.QtyBalance = qtyobal;
                list.Add(detail);
            }

            rptStokMutation report = new rptStokMutation();
            report.lblFilter.Text = cmbProduct.Text + "     " + cmbStock.Text  + "   " + cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " to " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            
        }

        private void Stock()
        {
            var query = from x in db.Products
                        where x.Active == true && x.StatusProduct != "Inactive"
                        orderby x.Code
                        select new
                        {
                            x.id,
                            x.idCategory,
                            x.Code,
                            x.Name,
                            Category = x.Category.Name,
                            x.UoM,
                            x.Stock,
                            x.Stock2,
                            x.Stock3,
                            x.TransitStock,
                            x.Cost
                        };

            if (cmbCategory.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCategory == cmbCategory.EditValue.ToInteger());
            }
            if (cmbProduct.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.id == cmbProduct.EditValue.ToInteger());
            }

            List<ReportStock> list = new List<ReportStock>();
            foreach (var item in query)
            {
                ReportStock detail = new ReportStock();
                detail.Code = item.Code;
                detail.Name = item.Name;
                detail.Category = item.Category;
                detail.UoM = item.UoM;
                detail.PriceList = item.Cost;

                if (cmbStock.Text == "JKT") detail.Stock = item.Stock;
                else if (cmbStock.Text == "BPP") detail.Stock = item.Stock2.ToDecimal();
                else if (cmbStock.Text == "BTG") detail.Stock = item.Stock3.ToDecimal();
                else if (cmbStock.Text == "ALL") detail.Stock = item.Stock + item.Stock2.ToDecimal() + item.Stock3.ToDecimal();
                else if (cmbStock.Text == "TRANSIT") detail.Stock = item.TransitStock.ToDecimal();

                detail.Subtotal = detail.Stock * detail.PriceList;
                list.Add(detail);
            }

            rptStock report = new rptStock();
            if (File.Exists("StockReport.repx")) report.LoadLayout("StockReport.repx");
            report.lblTitle.Text = cmbReport.Text;
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void StockMinimum()
        {
            var query = from x in db.Products
                        where x.Active == true && x.StatusProduct != "Inactive"
                        && x.StokMin != 0
                        orderby x.Code
                        select new
                        {
                            x.id,
                            x.idCategory,
                            x.Code,
                            x.Name,
                            Category = x.Category.Name,
                            x.UoM,
                            x.Stock,
                            x.Stock2,
                            x.Stock3,
                            x.Cost,
                            x.StokMin
                        };

            if (cmbCategory.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCategory == cmbCategory.EditValue.ToInteger());
            }
            if (cmbProduct.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.id == cmbProduct.EditValue.ToInteger());
            }

            if (cmbStock.Text == "JKT") query = query.Where(x => x.Stock <= x.StokMin);
            else if (cmbStock.Text == "BPP") query = query.Where(x => x.Stock2 <= x.StokMin);
            else if (cmbStock.Text == "BTG") query = query.Where(x => x.Stock3 <= x.StokMin);
            else query = query.Where(x => x.Stock + x.Stock2 + x.Stock3 <= x.StokMin);

            List<ReportStock> list = new List<ReportStock>();
            foreach (var item in query)
            {
                ReportStock detail = new ReportStock();
                detail.Code = item.Code;
                detail.Name = item.Name;
                detail.Category = item.Category;
                detail.UoM = item.UoM;
                detail.PriceList = item.Cost;
                detail.StokMin = item.StokMin.ToDecimal();

                if (cmbStock.Text == "JKT") detail.Stock = item.Stock;
                else if (cmbStock.Text == "BPP") detail.Stock = item.Stock2.ToDecimal();
                else if (cmbStock.Text == "ALL") detail.Stock = item.Stock + item.Stock2.ToDecimal();

                detail.Subtotal = detail.Stock * detail.PriceList;
                list.Add(detail);
            }

            rptStockMin report = new rptStockMin();
            if (File.Exists("StockMinReport.repx")) report.LoadLayout("StockMinReport.repx");
            //report.lblTitle.Text = cmbReport.Text;
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void StockMaximum()
        {
            var query = from x in db.Products
                        where x.Active == true && x.StatusProduct != "Inactive"
                        && x.StokMax != 0
                        orderby x.Code
                        select new
                        {
                            x.id,
                            x.idCategory,
                            x.Code,
                            x.Name,
                            Category = x.Category.Name,
                            x.UoM,
                            x.Stock,
                            x.Stock2,
                            x.Cost,
                            x.StokMax
                        };

            if (cmbCategory.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCategory == cmbCategory.EditValue.ToInteger());
            }
            if (cmbProduct.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.id == cmbProduct.EditValue.ToInteger());
            }

            if (cmbStock.Text == "JKT") query = query.Where(x => x.Stock >= x.StokMax);
            else if (cmbStock.Text == "BPP") query = query.Where(x => x.Stock2 >= x.StokMax);
            else query = query.Where(x => x.Stock + x.Stock2 >= x.StokMax);

            List<ReportStock> list = new List<ReportStock>();
            foreach (var item in query)
            {
                ReportStock detail = new ReportStock();
                detail.Code = item.Code;
                detail.Name = item.Name;
                detail.Category = item.Category;
                detail.UoM = item.UoM;
                detail.PriceList = item.Cost;
                detail.StokMax = item.StokMax.ToDecimal();

                if (cmbStock.Text == "JKT") detail.Stock = item.Stock;
                else if (cmbStock.Text == "BPP") detail.Stock = item.Stock2.ToDecimal();
                else if (cmbStock.Text == "ALL") detail.Stock = item.Stock + item.Stock2.ToDecimal();

                detail.Subtotal = detail.Stock * detail.PriceList;
                list.Add(detail);
            }

            rptStockMax report = new rptStockMax();
            if (File.Exists("StockMaxReport.repx")) report.LoadLayout("StockMaxReport.repx");
            //report.lblTitle.Text = cmbReport.Text;
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (cmbReport.Text == "Stock Mutation") StockMutation();
                else if (cmbReport.Text == "Stock Minimum") StockMinimum();
                else if (cmbReport.Text == "Stock Maximum") StockMaximum();
                else Stock();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}