﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuReport
{
    public partial class frmReportCustomerComplaint : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportCustomerComplaint()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string filter = string.Empty;
                db = new GreenChemDataContext();

                var query = from x in db.CustomerComplaints
                            where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                            select new
                            {
                                x.idCustomer,
                                Customer = x.Customer.Name,
                                x.TransactionDate,
                                Description = x.ComplaintDescription,
                                x.DepositionTo,
                                x.DueDate,
                                x.Category,
                                x.Action,
                                x.ClosingDate,
                                x.Status
                            };

                if (cmbCustomer.EditValue.IsNotEmpty())
                {
                    filter += "Customer: " + cmbCustomer.Text;
                    query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                }

                List<ReportCustomerComplaint> list = new List<ReportCustomerComplaint>();
                foreach (var item in query)
                {
                    ReportCustomerComplaint detail = new ReportCustomerComplaint();
                    detail.Customer = item.Customer;
                    detail.TransactionDate = item.TransactionDate;
                    detail.Description = item.Description;
                    detail.DepositionTo = item.DepositionTo;
                    if (item.DueDate.HasValue) detail.DueDate = item.DueDate.Value.ToString("dd/MM/yyyy");
                    detail.Category = item.Category;
                    detail.Action = item.Action;
                    if (item.ClosingDate.HasValue) detail.ClosingDate = item.ClosingDate.Value.ToString("dd/MM/yyyy");
                    detail.Status = item.Status;
                    list.Add(detail);
                }

                rptCustomerComplaint report = new rptCustomerComplaint();
                report.lblFilter.Text = filter;
                report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
                report.DataSource = list;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}