﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.IO;

namespace GreenChem.MenuReport
{
    public partial class frmReportBahanPenolong : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportBahanPenolong()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbKategori.Properties.DataSource = db.KategoriBahanPenolongs.GetName();
            cmbStatus.Properties.DataSource = db.StatusBahanPenolongs.GetName();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.BahanPenolongs
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.idKategoriBahanPenolong,
                                x.idStatusBahanPenolong,
                                x.Nomor,
                                x.Kode,
                                x.Nama,
                                Kategori = x.KategoriBahanPenolong.Name,
                                Status = x.StatusBahanPenolong.Name
                            };

                if (cmbKategori.EditValue.IsNotEmpty())
                    query = query.Where(x => x.idKategoriBahanPenolong == cmbKategori.EditValue.ToInteger());
                if (cmbStatus.EditValue.IsNotEmpty())
                    query = query.Where(x => x.idStatusBahanPenolong == cmbStatus.EditValue.ToInteger());

                MenuReport.rptBahanPenolong report = new MenuReport.rptBahanPenolong();
                if (File.Exists("BahanPenolongReport.repx")) report.LoadLayout("BahanPenolongReport.repx");
                report.DataSource = query;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}