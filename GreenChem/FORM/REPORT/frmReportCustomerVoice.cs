﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuReport
{
    public partial class frmReportCustomerVoice : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportCustomerVoice()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string filter = string.Empty;
                db = new GreenChemDataContext();

                var query = from x in db.CustomerVoices
                            where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                            select new
                            {
                                x.idCustomer,
                                Customer = x.Customer.Name,
                                x.TransactionDate,
                                x.ComplaintDescription,
                                x.DepositionTo,
                                x.DueDate,
                                x.Category,
                                x.Action,
                                x.ClosingDate,
                                x.Status,
                                x.TransactionNumber,
                                x.CustomerSupport,
                                x.Time,
                                x.Quality,
                                x.TechnicalSupport,
                                x.Suggestion,
                                x.PIC
                            };

                if (cmbCustomer.EditValue.IsNotEmpty())
                {
                    filter += "Customer: " + cmbCustomer.Text;
                    query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                }

                rptCustomerVoice report = new rptCustomerVoice();
                report.lblFilter.Text = filter;
                report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
                report.DataSource = query;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}