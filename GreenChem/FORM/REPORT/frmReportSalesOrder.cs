﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;
using System.IO;

namespace GreenChem.MenuReport
{
    public partial class frmReportSalesOrder : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportSalesOrder()
        {
            InitializeComponent();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
            cmbExpedition.Properties.DataSource = db.Couriers.GetName();
            cmbLocation.Properties.DataSource = db.Locations.GetName();
            cmbProduct.Properties.DataSource = db.Products.GetName();
        }

        private void SalesOrderSummary()
        {
            db = new GreenChemDataContext();
            string filter = string.Empty;

            var query = from x in db.SalesOrders
                        where x.DateOrder.Date >= cmbStartDate.DateTime.Date && x.DateOrder.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.idCourier,
                            x.idCustomer,
                            x.idLocation,
                            x.idUserAccount,
                            x.idSalesman,
                            x.TransactionNumber,
                            x.DateOrder,
                            x.Status,
                            Customer = x.Customer.Name,
                            x.TotalOrder,
                            x.Rate,
                            x.TaxOrder,
                            TotalIDR = x.TotalOrder * x.Rate,
                            x.Remarks,
                            //Currency = x.idCurrency != null ? db.Currencies.First(a => a.id == x.idCurrency).Symbol : ""
                            Salesman = db.UserAccounts.Any(a => a.id == x.idSalesman) ? db.UserAccounts.First(a => a.id == x.idSalesman).Name : ""
                        };

            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }
            if (cmbSalesman.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSalesman == cmbSalesman.EditValue.ToInteger());
                filter += "Salesman : " + cmbSalesman.Text + ";   ";
            }
            if (cmbExpedition.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCourier == cmbExpedition.EditValue.ToInteger());
                filter += "Courier : " + cmbExpedition.Text + ";   ";
            }

            if (cmbLocation.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idLocation == cmbLocation.EditValue.ToInteger());
                filter += "Location : " + cmbLocation.Text + ";   ";
            }
            if (cmbStatus.Text != "ALL")
            {
                query = query.Where(x => x.Status == cmbStatus.Text);
                filter += "Status : " + cmbStatus.Text + ";   ";
            }
            if (txtRemarks.Text.IsNotEmpty())
            {
                query = query.Where(x => x.Remarks == txtRemarks.Text);
                filter += "PO Number : " + txtRemarks.Text + ";   ";
            }

            rptSalesOrderSummary report = new rptSalesOrderSummary();
            if (File.Exists("SalesOrderSummary.repx")) report.LoadLayout("SalesOrderSummary.repx");
            report.DataSource = query;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.lblFilter.Text = filter;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void DeliveryOrderSummary()
        {
            db = new GreenChemDataContext();
            string filter = string.Empty;

            var query = from x in db.SalesOrders
                        where x.DateOrder.Date >= cmbStartDate.DateTime.Date && x.DateOrder.Date <= cmbEndDate.DateTime.Date
                        && x.DateDelivered != null
                        select new
                        {
                            x.idCourier,
                            x.idCustomer,
                            x.idLocation,
                            x.idSalesman,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.DONumber,
                            x.DateDelivered,
                            x.Status,
                            Customer = x.Customer.Name,
                            x.TotalDelivered,
                            x.Rate,
                            x.TaxDelivered,
                            TotalIDR = x.TotalDelivered * x.Rate,
                            x.Remarks,
                            x.Remarks2,
                            Salesman = db.UserAccounts.Any(a => a.id == x.idSalesman) ? db.UserAccounts.First(a => a.id == x.idSalesman).Name : ""
                        };

            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }
            if (cmbSalesman.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSalesman == cmbSalesman.EditValue.ToInteger());
                filter += "Salesman : " + cmbSalesman.Text + ";   ";
            }
            if (cmbExpedition.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCourier == cmbExpedition.EditValue.ToInteger());
                filter += "Courier : " + cmbExpedition.Text + ";   ";
            }
            if (cmbLocation.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idLocation == cmbLocation.EditValue.ToInteger());
                filter += "Location : " + cmbLocation.Text + ";   ";
            }
            if (cmbStatus.Text != "ALL")
            {
                query = query.Where(x => x.Status == cmbStatus.Text);
                filter += "Status : " + cmbStatus.Text + ";   ";
            }
            if (txtRemarks.Text.IsNotEmpty())
            {
                query = query.Where(x => x.Remarks == txtRemarks.Text);
                filter += "PO Number : " + txtRemarks.Text + ";   ";
            }

            rptDeliveryOrderSummary report = new rptDeliveryOrderSummary();
            if (File.Exists("DeliveryOrderSummary.repx")) report.LoadLayout("DeliveryOrderSummary.repx");
            report.DataSource = query;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.lblFilter.Text = filter;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void SalesInvoiceSummary()
        {
            db = new GreenChemDataContext();
            string filter = string.Empty;

            var query = from x in db.SalesOrders
                        where x.DateInvoice != null  && x.DateInvoice.Value.Date >= cmbStartDate.DateTime.Date && x.DateInvoice.Value.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.idCourier,
                            x.idCustomer,
                            x.idSalesman,
                            x.idLocation,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.DateInvoice,
                            x.Status,
                            Customer = x.Customer.Name,
                            x.TotalDelivered,
                            x.TaxOrder,
                            x.TaxDelivered,
                            x.Rate,
                            TotalIDR = x.TotalDelivered * x.Rate,
                            x.Remarks,
                            Salesman = db.UserAccounts.Any(a => a.id == x.idSalesman) ? db.UserAccounts.First(a => a.id == x.idSalesman).Name : ""
                            //Currency = x.idCurrency != null ? db.Currencies.First(a => a.id == x.idCurrency).Symbol : ""
                        };

            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }
            if (cmbSalesman.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSalesman == cmbSalesman.EditValue.ToInteger());
                filter += "Salesman : " + cmbSalesman.Text + ";   ";
            }
            if (cmbExpedition.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCourier == cmbExpedition.EditValue.ToInteger());
                filter += "Courier : " + cmbExpedition.Text + ";   ";
            }
            if (cmbLocation.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idLocation == cmbLocation.EditValue.ToInteger());
                filter += "Location : " + cmbLocation.Text + ";   ";
            }
            if (cmbStatus.Text != "ALL")
            {
                query = query.Where(x => x.Status == cmbStatus.Text);
                filter += "Status : " + cmbStatus.Text + ";   ";
            }
            if (txtRemarks.Text.IsNotEmpty())
            {
                query = query.Where(x => x.Remarks == txtRemarks.Text);
                filter += "PO Number : " + txtRemarks.Text + ";   ";
            }

            rptSalesInvoiceSummary report = new rptSalesInvoiceSummary();
            if (File.Exists("SalesInvoiceSummary.repx")) report.LoadLayout("SalesInvoiceSummary.repx");
            report.DataSource = query;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.lblFilter.Text = filter;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void DeliveryOrderLocation()
        {
            db = new GreenChemDataContext();
            string filter = string.Empty;

            var query = from x in db.SalesOrders
                        where x.DateDelivered != null && x.idLocation != null && x.DateModified != null && x.LastEditedBy != null
                        && x.DateDelivered.Value.Date >= cmbStartDate.DateTime.Date && x.DateDelivered.Value.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.idCourier,
                            x.idCustomer,
                            x.idSalesman,
                            x.idLocation,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.DateDelivered,
                            x.Status,
                            Customer = x.Customer.Name,
                            x.TotalDelivered,
                            x.Rate,
                            x.TaxDelivered,
                            TotalIDR = x.TotalDelivered * x.Rate,
                            x.Remarks,
                            x.Remarks2,
                            x.DateModified,
                            Location = x.Location.Name,
                            LastEditedBy = db.UserAccounts.First(a => a.id == x.LastEditedBy).UserName,
                            Salesman = db.UserAccounts.Any(a => a.id == x.idSalesman) ? db.UserAccounts.First(a => a.id == x.idSalesman).Name : ""
                        };

            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }
            if (cmbSalesman.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSalesman == cmbSalesman.EditValue.ToInteger());
                filter += "Salesman : " + cmbSalesman.Text + ";   ";
            }
            if (cmbExpedition.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCourier == cmbExpedition.EditValue.ToInteger());
                filter += "Courier : " + cmbExpedition.Text + ";   ";
            }
            if (cmbLocation.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idLocation == cmbLocation.EditValue.ToInteger());
                filter += "Location : " + cmbLocation.Text + ";   ";
            }
            if (cmbStatus.Text != "ALL")
            {
                query = query.Where(x => x.Status == cmbStatus.Text);
                filter += "Status : " + cmbStatus.Text + ";   ";
            }
            if (txtRemarks.Text.IsNotEmpty())
            {
                query = query.Where(x => x.Remarks == txtRemarks.Text);
                filter += "PO Number : " + txtRemarks.Text + ";   ";
            }

            rptDOLocation report = new rptDOLocation();
            if (File.Exists("DeliveryOrderLocation.repx")) report.LoadLayout("DeliveryOrderLocation.repx");
            report.DataSource = query;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.lblFilter.Text = filter;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void SalesOrderDetail()
        {
            db = new GreenChemDataContext();
            string filter = string.Empty;

            var query = from x in db.DetailSalesOrders
                        where x.SalesOrder.DateOrder.Date >= cmbStartDate.DateTime.Date && x.SalesOrder.DateOrder.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.SalesOrder.idCourier,
                            x.SalesOrder.idCustomer,
                            x.SalesOrder.idSalesman,
                            x.SalesOrder.idLocation,
                            x.SalesOrder.idUserAccount,
                            x.SalesOrder.TransactionNumber,
                            TransactionDate = x.SalesOrder.DateOrder,
                            x.SalesOrder.Status,
                            CustomerName = x.SalesOrder.Customer.Name,
                            x.SalesOrder.Remarks,
                            x.SalesOrder.Remarks2,
                            TransactionSubtotal = x.SalesOrder.SubtotalOrder * x.SalesOrder.Rate,
                            TransactionDiscount = x.SalesOrder.DiscountOrder * x.SalesOrder.Rate,
                            TransactionCost = x.SalesOrder.CostOrder * x.SalesOrder.Rate,
                            TransactionTax = x.SalesOrder.TaxOrder * x.SalesOrder.Rate,
                            TransactionTotal = x.SalesOrder.TotalOrder * x.SalesOrder.Rate,
                            ProductCode = x.Product.Code,
                            ProductName = x.Product.Name,
                            Quantity = x.QuantityOrder,
                            UoM = x.Product.UoM,
                            x.Packaging,
                            Price = x.PriceOrder * x.SalesOrder.Rate,
                            Discount = x.DiscountOrder * x.SalesOrder.Rate,
                            Subtotal = x.SubtotalOrder * x.SalesOrder.Rate,
                            Currency = x.SalesOrder.idCurrency != null ? db.Currencies.First(a => a.id == x.SalesOrder.idCurrency).Symbol : "",
                            Salesman = db.UserAccounts.Any(a => a.id == x.SalesOrder.idSalesman) ? db.UserAccounts.First(a => a.id == x.SalesOrder.idSalesman).Name : ""
                        };

            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }
            if (cmbSalesman.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSalesman == cmbSalesman.EditValue.ToInteger());
                filter += "Salesman : " + cmbSalesman.Text + ";   ";
            }
            if (cmbExpedition.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCourier == cmbExpedition.EditValue.ToInteger());
                filter += "Courier : " + cmbExpedition.Text + ";   ";
            }

            if (cmbLocation.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idLocation == cmbLocation.EditValue.ToInteger());
                filter += "Location : " + cmbLocation.Text + ";   ";
            }
            if (cmbStatus.Text != "ALL")
            {
                query = query.Where(x => x.Status == cmbStatus.Text);
                filter += "Status : " + cmbStatus.Text + ";   ";
            }
            if (txtRemarks.Text.IsNotEmpty())
            {
                query = query.Where(x => x.Remarks == txtRemarks.Text);
                filter += "PO Number : " + txtRemarks.Text + ";   ";
            }

            rptDetailSalesOrder report = new rptDetailSalesOrder();
            if (File.Exists("SalesOrderDetail.repx")) report.LoadLayout("SalesOrderDetail.repx");
            report.DataSource = query;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.lblFilter.Text = filter;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void OmsetPenjualan()
        {
            db = new GreenChemDataContext();
            string filter = string.Empty;

            var query = from x in db.SalesOrders
                        where x.DateOrder.Date >= cmbStartDate.DateTime.Date && x.DateOrder.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.idCourier,
                            x.idCustomer,
                            x.idSalesman,
                            x.idLocation,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.DateOrder,
                            x.Status,
                            Customer = x.Customer.Name,
                            x.TotalOrder,
                            x.Rate,
                            TotalIDR = x.TotalOrder * x.Rate,
                            x.Remarks,
                            TransactionTax = x.TaxDelivered,
                            TotalIncludeTax = x.TotalDelivered,
                            TotalExcludeTax = x.TotalDelivered - x.TaxDelivered,
                            Salesman = db.UserAccounts.Any(a => a.id == x.idSalesman) ? db.UserAccounts.First(a => a.id == x.idSalesman).Name : ""
                        };

            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }
            if (cmbSalesman.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSalesman == cmbSalesman.EditValue.ToInteger());
                filter += "Salesman : " + cmbSalesman.Text + ";   ";
            }
            if (cmbExpedition.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCourier == cmbExpedition.EditValue.ToInteger());
                filter += "Courier : " + cmbExpedition.Text + ";   ";
            }

            if (cmbLocation.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idLocation == cmbLocation.EditValue.ToInteger());
                filter += "Location : " + cmbLocation.Text + ";   ";
            }
            if (cmbStatus.Text != "ALL")
            {
                query = query.Where(x => x.Status == cmbStatus.Text);
                filter += "Status : " + cmbStatus.Text + ";   ";
            }
            if (txtRemarks.Text.IsNotEmpty())
            {
                query = query.Where(x => x.Remarks == txtRemarks.Text);
                filter += "PO Number : " + txtRemarks.Text + ";   ";
            }

            rptOmsetPenjualan report = new rptOmsetPenjualan();
            if (File.Exists("rptOmsetPenjualan.repx")) report.LoadLayout("rptOmsetPenjualan.repx");
            report.DataSource = query;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.lblFilter.Text = filter;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (cmbReport.Text == "Sales Order Summary" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Sales Order Summary")) SalesOrderSummary();
                else if (cmbReport.Text == "Delivery Order Summary" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Delivery Order Summary")) DeliveryOrderSummary();
                else if (cmbReport.Text == "Sales Invoice Summary" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Sales Invoice Summary")) SalesInvoiceSummary();
                else if (cmbReport.Text == "Delivery Order Location" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Delivery Order Location")) DeliveryOrderLocation();
                else if (cmbReport.Text == "Sales Order Detail" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Sales Order Detail")) SalesOrderDetail();
                else if (cmbReport.Text == "Omset Penjualan" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Sales Order Summary")) OmsetPenjualan();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}