﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.IO;

namespace GreenChem.MenuReport
{
    public partial class frmAutoNumberingReport : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmAutoNumberingReport()
        {
            InitializeComponent();

            db = new GreenChemDataContext();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void ExternalLetter()
        {
            string filter = string.Empty;

            var query = from x in db.GeneralLetters
                        where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        orderby x.TransactionNumber
                        select new
                        {
                            x.idDepartment,
                            x.idSubDepartment,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.TransactionDate,
                            Department = x.Department.Name,
                            x.FileName,
                            x.Remarks
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }

            rptGeneralLetter report = new rptGeneralLetter();
            if (File.Exists("ExternalLetterReport.repx")) report.LoadLayout("ExternalLetterReport.repx");
            report.lblTitle.Text = "External Letter Report";
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void InternalLetter()
        {
            string filter = string.Empty;

            var query = from x in db.InternalLetters
                        where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        orderby x.TransactionNumber
                        select new
                        {
                            x.idDepartment,
                            x.idSubDepartment,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.TransactionDate,
                            Department = x.Department.Name,
                            x.FileName,
                            x.Remarks
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }

            rptGeneralLetter report = new rptGeneralLetter();
            if (File.Exists("InternalLetterReport.repx")) report.LoadLayout("InternalLetterReport.repx");
            report.lblTitle.Text = "Internal Letter Report";
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void POtoSupplier()
        {
            string filter = string.Empty;

            var query = from x in db.AutoNumbers
                        where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.id,
                            x.idDepartment,
                            x.idSubDepartment,
                            x.idSupplier,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.TransactionDate,
                            Department = x.Department.Name,
                            SubDepartment = x.SubDepartment.Name,
                            Supplier  = x.Supplier.Name,
                            x.Remarks
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }
            if (cmbSupplier.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSupplier == cmbSupplier.EditValue.ToInteger());
                filter += "Supplier : " + cmbSupplier.Text + ";   ";
            }

            rptPOtoSupplier report = new rptPOtoSupplier();
            if (File.Exists("POtoSupplierReport.repx")) report.LoadLayout("POtoSupplierReport.repx");
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void QuotationIn()
        {
            string filter = string.Empty;

            var query = from x in db.DetailQuotationIns
                        join y in db.QuotationIns on x.idQuotationIn equals y.id
                        //join z in db.Products on x.idProduct equals z.id
                        where y.TransactionDate.Date >= cmbStartDate.DateTime.Date && y.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            y.id,
                            y.idDepartment,
                            y.idSubDepartment,
                            y.idSupplier,
                            y.idUserAccount,
                            y.TransactionNumber,
                            y.TransactionDate,
                            Department = y.Department.Name,
                            SubDepartment = y.SubDepartment.Name,
                            Name = y.Supplier.Name,
                            y.Remarks,
                            Product = x.ProductName,
                            x.Price,
                            x.Quantity,
                            x.Discount,
                            x.Subtotal
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }
            if (cmbSupplier.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSupplier == cmbSupplier.EditValue.ToInteger());
                filter += "Supplier : " + cmbSupplier.Text + ";   ";
            }


            rptQuotationIn report = new rptQuotationIn();
            if (File.Exists("QuotationInReport.repx")) report.LoadLayout("QuotationInReport.repx");
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void QuotationOut()
        {
            string filter = string.Empty;

            var query = from x in db.DetailQuotationOuts
                        join y in db.QuotationOuts on x.idQuotationOut equals y.id
                        join z in db.Products on x.idProduct equals z.id
                        where y.TransactionDate.Date >= cmbStartDate.DateTime.Date && y.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            y.id,
                            y.idDepartment,
                            y.idSubDepartment,
                            y.idCustomer,
                            y.idUserAccount,
                            y.TransactionNumber,
                            y.TransactionDate,
                            Department = y.Department.Name,
                            SubDepartment = y.SubDepartment.Name,
                            Name = y.Customer.Name,
                            y.Remarks,
                            Product = z.Name,
                            z.UoM,
                            x.Price,
                            x.Quantity,
                            x.Discount,
                            x.Subtotal
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }
            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbSupplier.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }


            rptQuotationOut report = new rptQuotationOut();
            if (File.Exists("QuotationOutReport.repx")) report.LoadLayout("QuotationOutReport.repx");
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void InquiryIn()
        {
            string filter = string.Empty;

            var query = from x in db.InquiryIns
                        where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.id,
                            x.idDepartment,
                            x.idSubDepartment,
                            x.idCustomer,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.TransactionDate,
                            Department = x.Department.Name,
                            SubDepartment = x.SubDepartment.Name,
                            Name = x.Customer.Name,
                            x.Remarks
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }
            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbSupplier.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }


            rptInquiryIn report = new rptInquiryIn();
            if (File.Exists("InquiryInInReport.repx")) report.LoadLayout("InquiryInReport.repx");
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void InquiryOut()
        {
            string filter = string.Empty;

            var query = from x in db.InquiryOuts
                        where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            x.id,
                            x.idDepartment,
                            x.idSubDepartment,
                            x.idSupplier,
                            x.idUserAccount,
                            x.TransactionNumber,
                            x.TransactionDate,
                            Department = x.Department.Name,
                            SubDepartment = x.SubDepartment.Name,
                            Name = x.Supplier.Name,
                            x.Remarks
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }
            if (cmbSupplier.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSupplier == cmbSupplier.EditValue.ToInteger());
                filter += "Supplier : " + cmbSupplier.Text + ";   ";
            }


            rptInquiryOut report = new rptInquiryOut();
            if (File.Exists("InquiryOutInReport.repx")) report.LoadLayout("InquiryOutReport.repx");
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void PPB()
        {
            string filter = string.Empty;

            var query = from x in db.DetailPPBs
                        join y in db.PPBs on x.idPPB equals y.id
                        join z in db.Products on x.idProduct equals z.id
                        where y.TransactionDate.Date >= cmbStartDate.DateTime.Date && y.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            y.id,
                            y.idDepartment,
                            y.idSubDepartment,
                            y.idCustomer,
                            y.idUserAccount,
                            y.TransactionNumber,
                            y.TransactionDate,
                            Department = y.Department.Name,
                            SubDepartment = y.SubDepartment.Name,
                            Customer = y.Customer.Name,
                            y.Remarks,
                            Product = z.Name,
                            z.UoM,
                            x.Price,
                            x.Quantity,
                            x.Discount,
                            x.Subtotal,
                            x.PPB.PONumber
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }
            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbSupplier.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }


            rptPPB report = new rptPPB();
            if (File.Exists("PPBReport.repx")) report.LoadLayout("PPBReport.repx");
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void PPS()
        {
            string filter = string.Empty;

            var query = from x in db.DetailPPS
                        join y in db.PPS on x.idPPS equals y.id
                        join z in db.Products on x.idProduct equals z.id
                        where y.TransactionDate.Date >= cmbStartDate.DateTime.Date && y.TransactionDate.Date <= cmbEndDate.DateTime.Date
                        select new
                        {
                            y.id,
                            y.idDepartment,
                            y.idSubDepartment,
                            y.idCustomer,
                            y.idUserAccount,
                            y.TransactionNumber,
                            y.TransactionDate,
                            Department = y.Department.Name,
                            SubDepartment = y.SubDepartment.Name,
                            Customer = y.Customer.Name,
                            y.Remarks,
                            Product = z.Name,
                            z.UoM,
                            x.Price,
                            x.Quantity,
                            x.Discount,
                            x.Subtotal,
                            Division = x.PPS.Division
                        };

            if (cmbDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                filter += "Department : " + cmbDepartment.Text + ";   ";
            }
            if (cmbSubDepartment.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                filter += "Subdepartment : " + cmbSubDepartment.Text + ";   ";
            }
            if (cmbCustomer.EditValue.IsNotEmpty())
            {
                query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                filter += "Customer : " + cmbCustomer.Text + ";   ";
            }


            rptPPS report = new rptPPS();
            if (File.Exists("PPSReport.repx")) report.LoadLayout("PPSReport.repx");
            report.DataSource = query;
            report.lblFilter.Text = filter;
            report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                if (cmbReport.Text == "External Letter Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "External Letter Report")) ExternalLetter();
                else if (cmbReport.Text == "Internal Letter Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Internal Letter Report")) InternalLetter();
                else if (cmbReport.Text == "PO To Supplier Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "PO to Supplier Report")) POtoSupplier();
                else if (cmbReport.Text == "Quotation In Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Quotation In Report")) QuotationIn();
                else if (cmbReport.Text == "Quotation Out Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Quotation Out Report")) QuotationOut();
                else if (cmbReport.Text == "RFQ Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Inquiry In Report")) InquiryIn();
                else if (cmbReport.Text == "Inquiry Out Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "Inquiry Out Report")) InquiryOut();
                else if (cmbReport.Text == "PPB Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "PPB Report")) PPB();
                else if (cmbReport.Text == "PPS Report" && db.CheckUserRole(frmUtama._iduseraccount.Value, "PPS Report")) PPS();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}