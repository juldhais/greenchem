﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuReport
{
    public partial class frmReportPPB : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportPPB()
        {
            InitializeComponent();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                string filter = string.Empty;

                var query = from x in db.DetailPPBs
                            join y in db.PPBs on x.idPPB equals y.id
                            join z in db.Products on x.idProduct equals z.id
                            where y.TransactionDate.Date >= cmbStartDate.DateTime.Date && y.TransactionDate.Date <= cmbEndDate.DateTime.Date
                            select new
                            {
                                y.id,
                                y.idDepartment,
                                y.idSubDepartment,
                                y.idCustomer,
                                y.idUserAccount,
                                y.TransactionNumber,
                                y.TransactionDate,
                                Department = y.Department.Name,
                                SubDepartment = y.SubDepartment.Name,
                                Customer = y.Customer.Name,
                                y.Remarks,
                                Product = z.Name,
                                z.UoM,
                                x.Price,
                                x.Quantity,
                                x.Discount,
                                x.Subtotal,
                                x.PPB.PONumber
                            };

                if (cmbCustomer.EditValue.IsNotEmpty())
                {
                    query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                    filter += "Customer : " + cmbCustomer.Text + ";   ";
                }


                rptPPB report = new rptPPB();
                report.DataSource = query;
                report.lblFilter.Text = filter;
                report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}