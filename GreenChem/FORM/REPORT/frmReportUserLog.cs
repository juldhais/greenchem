﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq;
using JulFunctions;

namespace GreenChem.MenuReport
{
    public partial class frmReportUserLog : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportUserLog()
        {
            InitializeComponent();
            cmbStartDate.DateTime = DateTime.Today;
            cmbEndDate.DateTime = DateTime.Today;
            RefreshData();
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.UserLogs
                            where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                            orderby x.TransactionDate descending
                            select new
                            {
                                x.UserAccount.Name,
                                x.Remarks,
                                x.TransactionDate
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbStartDate_EditValueChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void cmbEndDate_EditValueChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            gridControl.ShowPrintPreview();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}