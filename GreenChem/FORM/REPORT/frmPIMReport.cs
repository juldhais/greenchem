﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuReport
{
public partial class frmPIMReport : DevExpress.XtraEditors.XtraForm
    {
        string filter = string.Empty;
        GreenChemDataContext db;

        public frmPIMReport()
        {
            InitializeComponent();
        }

        private List<ReportEmployee> ReportEmployeeData()
        {
            var query = from x in db.Employees
                        select new
                        {
                            x.id,
                            x.FullName,
                            x.IDNo,
                            x.Gender,
                            x.MaritalStatus,
                            x.Nationality,
                            x.BirthDate,
                            x.KTP,
                            x.SIMA,
                            x.SIMB,
                            x.SIMC,
                            x.Passport,
                            x.NPWP,
                            x.Status,
                            x.Address,
                            x.Phone1,
                            x.Phone2,
                            x.WorkEmail,
                            x.OtherEmail,
                            x.StartDate,
                            x.EndDate,
                            PayGrade = x.PayGrade.Name,
                            x.SalaryAmount,
                            x.Remarks,
                            x.AccountNumber,
                            x.Bank,
                            x.Branch,
                            x.LeaveEntitlement,
                            Position = x.idPosition != null ? db.EmployeePositions.First(a => a.id == x.idPosition).Name : "",
                            x.TerminateReason,
                            x.TerminateDate,
                            x.Active
                        };

            List<ReportEmployee> list = new List<ReportEmployee>();
            foreach (var item in query)
            {
                ReportEmployee detail = new ReportEmployee();
                detail.FullName = item.FullName;
                detail.Phone1 = item.Phone1;
                detail.Phone2 = item.Phone2;
                detail.WorkEmail = item.WorkEmail;
                detail.OtherEmail = item.OtherEmail;
                detail.NPWP = item.NPWP;
                detail.KTP = item.KTP;
                detail.SIMA = item.SIMA;
                detail.SIMB = item.SIMB;
                detail.SIMC = item.SIMC;
                detail.LeaveBalance = item.LeaveEntitlement;
                detail.TerminateReason = item.TerminateReason;
                detail.TerminateDate = item.TerminateDate;
                detail.Active = item.Active;
                list.Add(detail);
            }

            return list;
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                filter = string.Empty;

                if (cmbReport.Text == "Active Employee")
                {
                    rptEmployee report = new rptEmployee();
                    report.DataSource = ReportEmployeeData().Where(x => x.Active == true);
                    report.DisplayName = "rptActiveEmployee";
                    report.lblFilter.Text = cmbReport.Text;
                    if (System.IO.File.Exists("rptActiveEmployee.repx")) report.LoadLayout("rptActiveEmployee.repx");
                    report.ShowPreview();
                    report.BringToFront();
                    report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
                }
                else if (cmbReport.Text == "Terminated Employee")
                {
                    rptEmployee report = new rptEmployee();
                    report.DataSource = ReportEmployeeData().Where(x => x.Active == false);
                    report.DisplayName = "rptTerminatedEmployee";
                    report.lblFilter.Text = cmbReport.Text;
                    if (System.IO.File.Exists("rptTerminatedEmployee.repx")) report.LoadLayout("rptTerminatedEmployee.repx");
                    report.ShowPreview();
                    report.BringToFront();
                    report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
                }
                else if (cmbReport.Text == "All Employee")
                {
                    rptEmployee report = new rptEmployee();
                    report.DisplayName = "rptAllEmployee";
                    report.DataSource = ReportEmployeeData();
                    report.lblFilter.Text = cmbReport.Text;
                    if (System.IO.File.Exists("rptAllEmployee.repx")) report.LoadLayout("rptAllEmployee.repx");
                    report.ShowPreview();
                    report.BringToFront();
                    report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
                }
                else if (cmbReport.Text == "Leave Balance")
                {
                    rptEmployee report = new rptEmployee();
                    report.DataSource = ReportEmployeeData();
                    report.DisplayName = "rptLeaveBalance";
                    report.lblFilter.Text = cmbReport.Text;
                    if (System.IO.File.Exists("rptLeaveBalance.repx")) report.LoadLayout("rptLeaveBalance.repx");
                    report.ShowPreview();
                    report.BringToFront();
                    report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}