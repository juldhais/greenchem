﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuReport
{
    public partial class frmExpenseReport : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmExpenseReport()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            cmbDepartment.Properties.DataSource = db.Departments.GetName();
            cmbSubDepartment.Properties.DataSource = db.SubDepartments.GetName();
            cmbSalesman.Properties.DataSource = db.Salesmans.GetName();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                string filter = string.Empty;

                var query = from x in db.Expenses
                            where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                            select new
                            {
                                x.idCustomer,
                                x.idDepartment,
                                x.idSubDepartment,
                                x.idUserAccount,
                                x.idSalesman,
                                x.TransactionNumber,
                                x.TransactionDate,
                                x.Remarks,
                                x.Amount,
                                Department = x.Department.Name,
                                SubDepartment = x.SubDepartment.Name,
                                Salesman = x.idSalesman != null ? db.Salesmans.First(a => a.id == x.idSalesman).Name : "",
                                UserAccount = x.UserAccount.Name,
                            };

                if (cmbDepartment.EditValue.IsNotEmpty())
                {
                    query = query.Where(x => x.idDepartment == cmbDepartment.EditValue.ToInteger());
                    filter += "Department : " + cmbDepartment.Text + ";   ";
                }
                if (cmbSubDepartment.EditValue.IsNotEmpty())
                {
                    query = query.Where(x => x.idSubDepartment == cmbSubDepartment.EditValue.ToInteger());
                    filter += "SubDepartment : " + cmbSubDepartment.Text + ";   ";
                }
                if (cmbSalesman.EditValue.IsNotEmpty())
                {
                    query = query.Where(x => x.idSalesman == cmbSalesman.EditValue.ToInteger());
                    filter += "Employee : " + cmbSalesman.Text + ";   ";
                }

                List<ReportExpense> list = new List<ReportExpense>();
                foreach (var item in query)
                {
                    ReportExpense detail = new ReportExpense();
                    detail.TransactionNumber = item.TransactionNumber;
                    detail.TransactionDate = item.TransactionDate;
                    detail.Department = item.Department;
                    detail.SubDepartment = item.SubDepartment;
                    detail.Salesman = item.Salesman;
                    detail.Remarks = item.Remarks;
                    detail.Amount = item.Amount;
                    list.Add(detail);
                }

                rptExpense report = new rptExpense();
                if (System.IO.File.Exists("ExpenseReport.repx")) report.LoadLayout("ExpenseReport.repx");
                report.DataSource = list;
                report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
                report.lblFilter.Text = filter;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbDeleteNull_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
        }
    }
}