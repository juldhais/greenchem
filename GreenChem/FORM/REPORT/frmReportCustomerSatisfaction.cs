﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuReport
{
    public partial class frmReportCustomerSatisfaction : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportCustomerSatisfaction()
        {
            InitializeComponent();
            cmbStartDate.DateTime = DateTime.Now;
            cmbEndDate.DateTime = DateTime.Now;
            db = new GreenChemDataContext();
            cmbCustomer.Properties.DataSource = db.Customers.GetName();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string filter = string.Empty;
                db = new GreenChemDataContext();

                var query = from x in db.CustomerSatisfactions
                            where x.TransactionDate.Date >= cmbStartDate.DateTime.Date && x.TransactionDate.Date <= cmbEndDate.DateTime.Date
                            select new
                            {
                                x.idCustomer,
                                x.TransactionNumber,
                                x.TransactionDate,
                                Customer = x.Customer.Name,
                                x.CustomerSupport,
                                x.Time,
                                x.Quality,
                                x.TechnicalSupport,
                                x.Suggestion,
                                x.PIC
                            };

                if (cmbCustomer.EditValue.IsNotEmpty())
                {
                    filter += "Customer: " + cmbCustomer.Text + ";   ";
                    query = query.Where(x => x.idCustomer == cmbCustomer.EditValue.ToInteger());
                }
                if (cmbCustomerSupport.Text != "ALL")
                {
                    filter += "Cust. Support: " + cmbCustomerSupport.Text + ";   ";
                    query = query.Where(x => x.CustomerSupport == cmbCustomerSupport.Text);
                }
                if (cmbTime.Text != "ALL")
                {
                    filter += "Time: " + cmbTime.Text + ";   ";
                    query = query.Where(x => x.Time == cmbTime.Text);
                }
                if (cmbQuality.Text != "ALL")
                {
                    filter += "Quality: " + cmbQuality.Text + ";   ";
                    query = query.Where(x => x.Quality == cmbQuality.Text);
                }
                if (cmbTechnicalSupport.Text != "ALL")
                {
                    filter += "Tech. Support: " + cmbTechnicalSupport.Text + ";   ";
                    query = query.Where(x => x.TechnicalSupport == cmbTechnicalSupport.Text);
                }

                rptCustomerSatisfaction report = new rptCustomerSatisfaction();
                report.DataSource = query;
                report.lblTanggal.Text = cmbStartDate.DateTime.ToString("dd/MM/yyyy") + " s/d " + cmbEndDate.DateTime.ToString("dd/MM/yyyy");
                report.lblFilter.Text = filter;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}