﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.IO;

namespace GreenChem.MenuReport
{
    public partial class frmReportBahanBaku : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmReportBahanBaku()
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbFisik.Properties.DataSource = db.Fisiks.GetName();
            cmbTipe.Properties.DataSource = db.Tipes.GetName();
            cmbSupplier.Properties.DataSource = db.Suppliers.GetName();

        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.BahanBakus
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.idFisik,
                                x.idSupplier,
                                x.idTipe,
                                x.Nomor,
                                x.Kode,
                                x.KodeMSDS,
                                x.Nama,
                                Fisik = x.Fisik.Name,
                                Tipe = x.Tipe.Name,
                                x.Fungsi,
                                Supplier = x.Supplier.Name,
                                Address = x.Supplier.Address,
                                Phone = x.Supplier.Phone,
                                Fax = x.Supplier.Fax,
                                Website = x.Supplier.Website,
                                Person = x.Supplier.Person
                            };

                if (cmbFisik.EditValue.IsNotEmpty())
                    query = query.Where(x => x.idFisik == cmbFisik.EditValue.ToInteger());
                if (cmbTipe.EditValue.IsNotEmpty())
                    query = query.Where(x => x.idTipe == cmbTipe.EditValue.ToInteger());
                if (cmbSupplier.EditValue.IsNotEmpty())
                    query = query.Where(x => x.idSupplier == cmbSupplier.EditValue.ToInteger());
                if (txtFungsi.Text.IsNotEmpty())
                    query = query.Where(x => x.Fungsi.Contains(txtFungsi.Text));

                MenuReport.rptBahanBaku report = new MenuReport.rptBahanBaku();
                if (File.Exists("BahanBakuReport.repx")) report.LoadLayout("BahanBakuReport.repx");
                report.DataSource = query;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}