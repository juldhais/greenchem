﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmEditCourier : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditCourier(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            var courier = db.Couriers.First(x => x.id == _id);
            txtName.Text = courier.Name;
            txtAddress.Text = courier.Address;
            txtPhone.Text = courier.Phone;
            txtRemarks.Text = courier.Remarks;
            txtPIC.Text = courier.PIC;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                Courier courier = db.Couriers.First(x => x.id == _id);
                courier.Name = txtName.Text;
                courier.Address = txtAddress.Text;
                courier.Phone = txtPhone.Text;
                courier.Remarks = txtRemarks.Text;
                courier.PIC = txtPIC.Text;
                
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}