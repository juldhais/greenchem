﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmDataCourier : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataCourier()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Expedition");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Expedition");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Expedition");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.Couriers
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.Name,
                                x.Address,
                                x.Phone,
                                x.Remarks,
                                x.PIC,
                                x.Active
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewCourier form = new frmNewCourier();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEditCourier form = new frmEditCourier(gridView.GetFocusedRowCellValue("id").ToInteger());
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this courier : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    Courier courier = db.Couriers.First(x => x.id == id);
                    courier.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Courier deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) RefreshData();
        }
    }
}