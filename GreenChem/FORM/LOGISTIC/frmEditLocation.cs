﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmEditLocation : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditLocation(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            var location = db.Locations.First(x => x.id == _id);
            txtCode.Text = location.Code;
            txtName.Text = location.Name;
            txtRemarks.Text = location.Remarks;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtCode.Text.IsEmpty())
            {
                XtraMessageBox.Show("Code can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCode.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                Location location = db.Locations.First(x => x.id == _id);
                location.Code = txtCode.Text;
                location.Name = txtName.Text;
                location.Remarks = txtRemarks.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}