﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;
using System.Collections.Generic;
using System.IO;
using GreenChem.MenuReport;

namespace GreenChem.MenuTransaction
{
    public partial class frmStandardDeliveryTime : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmStandardDeliveryTime()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Standard Delivery Time");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Standard Delivery Time");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Standard Delivery Time");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.StandardDeliveryTimes
                            select new
                            {
                                x.id,
                                Product = x.Product.Name,
                                x.Status,
                                x.Packaging,
                                x.Column1,
                                x.Column2,
                                x.Column3,
                                x.Column4,
                                x.Column5,
                                x.Column6,
                                x.Column7,
                                x.Column8,
                                x.Column9,
                                x.Column10,
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewStandardDeliveryTime form = new frmNewStandardDeliveryTime();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditStandardDeliveryTime form = new frmEditStandardDeliveryTime(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string nama = gridView.GetFocusedRowCellValue("Product").Safe();

                DialogResult = XtraMessageBox.Show("Delete this data " + nama + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    db = new GreenChemDataContext();
                    StandardDeliveryTime sdt = db.StandardDeliveryTimes.First(x => x.id == id);
                    db.StandardDeliveryTimes.DeleteOnSubmit(sdt);
                    db.SubmitChanges();

                    lblRefresh_Click(null, null);
                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                List<ReportStandardDeliveryTime> list = new List<ReportStandardDeliveryTime>();

                for (int i = 0; i < gridView.RowCount; i++)
                {
                    ReportStandardDeliveryTime detail = new ReportStandardDeliveryTime();
                    detail.Name = gridView.GetRowCellValue(i, "Product").Safe();
                    detail.Status = gridView.GetRowCellValue(i, "Status").Safe();
                    detail.Packaging = gridView.GetRowCellValue(i, "Packaging").Safe();
                    detail.Column1 = gridView.GetRowCellValue(i, "Column1").Safe();
                    detail.Column2 = gridView.GetRowCellValue(i, "Column2").Safe();
                    detail.Column3 = gridView.GetRowCellValue(i, "Column3").Safe();
                    detail.Column4 = gridView.GetRowCellValue(i, "Column4").Safe();
                    detail.Column5 = gridView.GetRowCellValue(i, "Column5").Safe();
                    detail.Column6 = gridView.GetRowCellValue(i, "Column6").Safe();
                    detail.Column7 = gridView.GetRowCellValue(i, "Column7").Safe();
                    detail.Column8 = gridView.GetRowCellValue(i, "Column8").Safe();
                    list.Add(detail);
                }

                rptStandardDeliveryTime report = new rptStandardDeliveryTime();
                if (File.Exists("StandardDeliveryTime.repx")) report.LoadLayout("StandardDeliveryTime.repx");
                report.DataSource = list;
                report.ShowPreview();
                report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}