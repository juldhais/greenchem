﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmChangeTSLocation : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmChangeTSLocation()
        {
            InitializeComponent();

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbNumber.Properties.DataSource = from x in db.TransferStocks
                                                  where (x.Status == "OPEN" || x.Status == "ON-PROGRESS" || x.Status == "TRANSITED")
                                                  && x.Approved == true && x.Approved2 == true
                                                  group x by x.TransactionNumber into g
                                                  select new
                                                  {
                                                      TransactionNumber = g.Key,
                                                      TransactionDate = g.First().TransactionDate,
                                                      FromLocation = g.First().FromLocation,
                                                      ToLocation = g.First().ToLocation
                                                  };

                cmbLocation.Properties.DataSource = db.Locations.GetName();
                cmbExpedition.Properties.DataSource = db.Couriers.GetName();
                cmbDate.DateTime = db.GetServerDate();
                cmbDeliveryDate.DateTime = cmbDate.DateTime;

            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbNumber_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var tranfers = db.TransferStocks.First(x => x.TransactionNumber == cmbNumber.Text);

                cmbDeliveryDate.EditValue = tranfers.DateDelivered;
                cmbStatus.Text = tranfers.Status;
                cmbLocation.EditValue = tranfers.idLocation;
                cmbExpedition.EditValue = tranfers.idCourier;
                txtRemarks.Text = tranfers.Remarks;

                if (cmbStatus.Text == "DELIVERED") cmbStatus.Enabled = false;

                cmbStatus_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbNumber.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Transaction number can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbNumber.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var transfers = from x in db.TransferStocks
                                where x.TransactionNumber == cmbNumber.Text
                                select new
                                {
                                    x.id,
                                    x.idProduct,
                                    x.Quantity,
                                    x.IsDelivered,
                                    x.ToLocation
                                };

                foreach (var item in transfers)
                {
                    TransferStock transfer = db.TransferStocks.First(x => x.id == item.id);
                    transfer.idLocation = cmbLocation.EditValue.ToNullInteger();
                    transfer.Status = cmbStatus.Text;
                    transfer.DateDelivered = cmbDeliveryDate.EditValue.ToNullDateTime();
                    transfer.Remarks = txtRemarks.Text;
                    transfer.IsDelivered = true;

                    //remove transit stock
                    if (transfer.Status == "DELIVERED")
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        product.TransitStock = product.TransitStock.ToDecimal() - item.Quantity.ToDecimal();

                        if (item.ToLocation == "JKT") product.Stock += item.Quantity.ToDecimal();
                        else if (item.ToLocation == "BPP") product.Stock2 = product.Stock2.ToDecimal() + item.Quantity.ToDecimal();
                        if (item.ToLocation == "BTG") product.Stock3 = product.Stock3.ToDecimal() + item.Quantity.ToDecimal();
                    }
                    
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStatus.Text == "DELIVERED") cmbDeliveryDate.Enabled = true;
            else cmbDeliveryDate.Enabled = false;
        }
    }
}