﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmDataLocation : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmDataLocation()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Location");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Location");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Location");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.Locations
                            where x.Active == true
                            select new
                            {
                                x.id,
                                x.Code,
                                x.Name,
                                x.Remarks
                            };

                if (txtSearch.Text.IsNotEmpty())
                    query = query.Where(x => x.Code == txtSearch.Text || x.Name.Contains(txtSearch.Text));

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewLocation form = new frmNewLocation();
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEditLocation form = new frmEditLocation(gridView.GetFocusedRowCellValue("id").ToInteger());
            form.ShowDialog();
            RefreshData();
            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string name = gridView.GetFocusedRowCellValue("Name").Safe();
                DialogResult = XtraMessageBox.Show("Are you sure want to delete this location : " + name + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    Location location = db.Locations.First(x => x.id == id);
                    location.Active = false;
                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Location deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    gridControl.Focus();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}