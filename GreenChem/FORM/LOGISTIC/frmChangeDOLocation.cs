﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmChangeDOLocation : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmChangeDOLocation()
        {
            InitializeComponent();

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbNumber.Properties.DataSource = from x in db.SalesOrders
                                                  where x.Status == "ON-PROGRESS" || x.Status == "TRANSITED"
                                                  select new
                                                  {
                                                      x.id,
                                                      x.TransactionNumber,
                                                      x.DateOrder,
                                                      Customer = x.Customer.Name,
                                                      x.TotalOrder
                                                  };

                cmbCustomer.Properties.DataSource = db.Customers.GetName();
                cmbLocation.Properties.DataSource = db.Locations.GetName();
                cmbExpedition.Properties.DataSource = db.Couriers.GetName();
                cmbDate.DateTime = db.GetServerDate();
                cmbDeliveryDate.DateTime = cmbDate.DateTime;
                

            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbNumber_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var salesorder = db.SalesOrders.First(x => x.id == cmbNumber.EditValue.ToInteger());

                cmbDeliveryDate.EditValue = salesorder.DateDelivered;
                cmbStatus.Text = cmbStatus.Text;
                cmbCustomer.EditValue = salesorder.idCustomer;
                cmbLocation.EditValue = salesorder.idLocation;
                cmbExpedition.EditValue = salesorder.idCourier;
                txtRemarks.Text = salesorder.Remarks;
                txtRemarks2.Text = salesorder.Remarks2;

                if (cmbStatus.Text == "DELIVERED") cmbStatus.Enabled = false;

                if (salesorder.DateModified.HasValue)
                {
                    Text += "Modified : " + salesorder.DateModified.Value.ToString("dd/MM/yyyy") + " | ";
                    if (salesorder.LastEditedBy.HasValue)
                    {
                        var username = db.UserAccounts.First(x => x.id == salesorder.LastEditedBy).Name;
                        Text += username;
                    }
                }

                cmbStatus_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cmbNumber.EditValue.IsEmpty())
            {
                XtraMessageBox.Show("Transaction number can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbNumber.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                SalesOrder salesorder = db.SalesOrders.First(x => x.id == cmbNumber.EditValue.ToInteger());
                salesorder.idUserAccount = frmUtama._iduseraccount;
                if (cmbLocation.EditValue.IsNotEmpty()) salesorder.idLocation = cmbLocation.EditValue.ToInteger();
                //salesorder.DateDelivered = cmbDeliveryDate.DateTime;
                salesorder.ChangeDODate = cmbDeliveryDate.DateTime;
                salesorder.Status = cmbStatus.Text;
                //salesorder.Remarks = txtRemarks.Text;
                salesorder.Remarks2 = txtRemarks2.Text;
                salesorder.DateModified = db.GetServerDate();
                salesorder.LastEditedBy = frmUtama._iduseraccount;

                var details = from x in salesorder.DetailSalesOrders
                              select new
                              {
                                  x.id,
                                  x.idProduct,
                                  x.QuantityDelivered,
                                  x.IsDelivered
                              };

                //remove transit stock
                foreach (var item in details)
                {
                    if (item.IsDelivered != true && salesorder.Status == "DELIVERED")
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        product.TransitStock = product.TransitStock.ToDecimal() - item.QuantityDelivered;

                        DetailSalesOrder detail = db.DetailSalesOrders.First(x => x.id == item.id);
                        detail.IsDelivered = true;
                    }
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStatus.Text == "DELIVERED") cmbDeliveryDate.Enabled = true;
            else cmbDeliveryDate.Enabled = false;
        }
    }
}