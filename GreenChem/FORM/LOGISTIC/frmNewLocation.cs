﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuData
{
    public partial class frmNewLocation : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewLocation()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtCode.Text.IsEmpty())
            {
                XtraMessageBox.Show("Code can't be empty.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCode.Focus();
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                Location location = new Location();
                location.Code = txtCode.Text;
                location.Name = txtName.Text;
                location.Remarks = txtRemarks.Text;
                location.Active = true;
                db.Locations.InsertOnSubmit(location);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}