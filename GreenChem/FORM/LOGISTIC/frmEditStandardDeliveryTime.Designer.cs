﻿namespace GreenChem.MenuTransaction
{
    partial class frmEditStandardDeliveryTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtColumn8 = new DevExpress.XtraEditors.TextEdit();
            this.lblCustom8 = new DevExpress.XtraEditors.LabelControl();
            this.txtColumn7 = new DevExpress.XtraEditors.TextEdit();
            this.lblCustom7 = new DevExpress.XtraEditors.LabelControl();
            this.txtColumn6 = new DevExpress.XtraEditors.TextEdit();
            this.lblCustom6 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtColumn5 = new DevExpress.XtraEditors.TextEdit();
            this.lblCustom5 = new DevExpress.XtraEditors.LabelControl();
            this.txtColumn4 = new DevExpress.XtraEditors.TextEdit();
            this.lblCustom4 = new DevExpress.XtraEditors.LabelControl();
            this.txtColumn3 = new DevExpress.XtraEditors.TextEdit();
            this.lblCustom3 = new DevExpress.XtraEditors.LabelControl();
            this.txtColumn2 = new DevExpress.XtraEditors.TextEdit();
            this.lblCustom2 = new DevExpress.XtraEditors.LabelControl();
            this.txtColumn1 = new DevExpress.XtraEditors.TextEdit();
            this.lblColumn1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtPackaging = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.cmbProduct = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPackaging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.txtColumn8);
            this.groupControl2.Controls.Add(this.lblCustom8);
            this.groupControl2.Controls.Add(this.txtColumn7);
            this.groupControl2.Controls.Add(this.lblCustom7);
            this.groupControl2.Controls.Add(this.txtColumn6);
            this.groupControl2.Controls.Add(this.lblCustom6);
            this.groupControl2.Location = new System.Drawing.Point(222, 129);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(200, 197);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Pengiriman Dari WH BPP";
            // 
            // txtColumn8
            // 
            this.txtColumn8.Location = new System.Drawing.Point(84, 97);
            this.txtColumn8.Name = "txtColumn8";
            this.txtColumn8.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn8.Properties.Appearance.Options.UseFont = true;
            this.txtColumn8.Size = new System.Drawing.Size(80, 24);
            this.txtColumn8.TabIndex = 5;
            // 
            // lblCustom8
            // 
            this.lblCustom8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblCustom8.Location = new System.Drawing.Point(27, 100);
            this.lblCustom8.Name = "lblCustom8";
            this.lblCustom8.Size = new System.Drawing.Size(35, 17);
            this.lblCustom8.TabIndex = 4;
            this.lblCustom8.Text = "Melak";
            // 
            // txtColumn7
            // 
            this.txtColumn7.Location = new System.Drawing.Point(84, 67);
            this.txtColumn7.Name = "txtColumn7";
            this.txtColumn7.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn7.Properties.Appearance.Options.UseFont = true;
            this.txtColumn7.Size = new System.Drawing.Size(80, 24);
            this.txtColumn7.TabIndex = 3;
            // 
            // lblCustom7
            // 
            this.lblCustom7.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblCustom7.Location = new System.Drawing.Point(27, 70);
            this.lblCustom7.Name = "lblCustom7";
            this.lblCustom7.Size = new System.Drawing.Size(23, 17);
            this.lblCustom7.TabIndex = 2;
            this.lblCustom7.Text = "BTG";
            // 
            // txtColumn6
            // 
            this.txtColumn6.Location = new System.Drawing.Point(84, 37);
            this.txtColumn6.Name = "txtColumn6";
            this.txtColumn6.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn6.Properties.Appearance.Options.UseFont = true;
            this.txtColumn6.Size = new System.Drawing.Size(80, 24);
            this.txtColumn6.TabIndex = 1;
            // 
            // lblCustom6
            // 
            this.lblCustom6.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblCustom6.Location = new System.Drawing.Point(27, 40);
            this.lblCustom6.Name = "lblCustom6";
            this.lblCustom6.Size = new System.Drawing.Size(54, 17);
            this.lblCustom6.TabIndex = 0;
            this.lblCustom6.Text = "Dlm Kota";
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Location = new System.Drawing.Point(342, 332);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(236, 332);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.txtColumn5);
            this.groupControl1.Controls.Add(this.lblCustom5);
            this.groupControl1.Controls.Add(this.txtColumn4);
            this.groupControl1.Controls.Add(this.lblCustom4);
            this.groupControl1.Controls.Add(this.txtColumn3);
            this.groupControl1.Controls.Add(this.lblCustom3);
            this.groupControl1.Controls.Add(this.txtColumn2);
            this.groupControl1.Controls.Add(this.lblCustom2);
            this.groupControl1.Controls.Add(this.txtColumn1);
            this.groupControl1.Controls.Add(this.lblColumn1);
            this.groupControl1.Location = new System.Drawing.Point(12, 129);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(200, 197);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Pengiriman Dari Jakarta";
            // 
            // txtColumn5
            // 
            this.txtColumn5.Location = new System.Drawing.Point(78, 157);
            this.txtColumn5.Name = "txtColumn5";
            this.txtColumn5.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn5.Properties.Appearance.Options.UseFont = true;
            this.txtColumn5.Size = new System.Drawing.Size(80, 24);
            this.txtColumn5.TabIndex = 9;
            // 
            // lblCustom5
            // 
            this.lblCustom5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblCustom5.Location = new System.Drawing.Point(21, 160);
            this.lblCustom5.Name = "lblCustom5";
            this.lblCustom5.Size = new System.Drawing.Size(26, 17);
            this.lblCustom5.TabIndex = 8;
            this.lblCustom5.Text = "SBW";
            // 
            // txtColumn4
            // 
            this.txtColumn4.Location = new System.Drawing.Point(78, 127);
            this.txtColumn4.Name = "txtColumn4";
            this.txtColumn4.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn4.Properties.Appearance.Options.UseFont = true;
            this.txtColumn4.Size = new System.Drawing.Size(80, 24);
            this.txtColumn4.TabIndex = 7;
            // 
            // lblCustom4
            // 
            this.lblCustom4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblCustom4.Location = new System.Drawing.Point(21, 130);
            this.lblCustom4.Name = "lblCustom4";
            this.lblCustom4.Size = new System.Drawing.Size(23, 17);
            this.lblCustom4.TabIndex = 6;
            this.lblCustom4.Text = "BTG";
            // 
            // txtColumn3
            // 
            this.txtColumn3.Location = new System.Drawing.Point(78, 97);
            this.txtColumn3.Name = "txtColumn3";
            this.txtColumn3.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn3.Properties.Appearance.Options.UseFont = true;
            this.txtColumn3.Size = new System.Drawing.Size(80, 24);
            this.txtColumn3.TabIndex = 5;
            // 
            // lblCustom3
            // 
            this.lblCustom3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblCustom3.Location = new System.Drawing.Point(21, 100);
            this.lblCustom3.Name = "lblCustom3";
            this.lblCustom3.Size = new System.Drawing.Size(35, 17);
            this.lblCustom3.TabIndex = 4;
            this.lblCustom3.Text = "Melak";
            // 
            // txtColumn2
            // 
            this.txtColumn2.Location = new System.Drawing.Point(78, 67);
            this.txtColumn2.Name = "txtColumn2";
            this.txtColumn2.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn2.Properties.Appearance.Options.UseFont = true;
            this.txtColumn2.Size = new System.Drawing.Size(80, 24);
            this.txtColumn2.TabIndex = 3;
            // 
            // lblCustom2
            // 
            this.lblCustom2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblCustom2.Location = new System.Drawing.Point(21, 70);
            this.lblCustom2.Name = "lblCustom2";
            this.lblCustom2.Size = new System.Drawing.Size(21, 17);
            this.lblCustom2.TabIndex = 2;
            this.lblCustom2.Text = "BPP";
            // 
            // txtColumn1
            // 
            this.txtColumn1.Location = new System.Drawing.Point(78, 37);
            this.txtColumn1.Name = "txtColumn1";
            this.txtColumn1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtColumn1.Properties.Appearance.Options.UseFont = true;
            this.txtColumn1.Size = new System.Drawing.Size(80, 24);
            this.txtColumn1.TabIndex = 1;
            // 
            // lblColumn1
            // 
            this.lblColumn1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblColumn1.Location = new System.Drawing.Point(21, 40);
            this.lblColumn1.Name = "lblColumn1";
            this.lblColumn1.Size = new System.Drawing.Size(54, 17);
            this.lblColumn1.TabIndex = 0;
            this.lblColumn1.Text = "Dlm Kota";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtPackaging);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.cmbStatus);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.cmbProduct);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(410, 111);
            this.panelControl1.TabIndex = 0;
            // 
            // txtPackaging
            // 
            this.txtPackaging.Location = new System.Drawing.Point(78, 73);
            this.txtPackaging.Name = "txtPackaging";
            this.txtPackaging.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.txtPackaging.Properties.Appearance.Options.UseFont = true;
            this.txtPackaging.Size = new System.Drawing.Size(306, 24);
            this.txtPackaging.TabIndex = 5;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl8.Location = new System.Drawing.Point(13, 76);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(59, 17);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "Packaging";
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "Standard";
            this.cmbStatus.Location = new System.Drawing.Point(78, 44);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cmbStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "Beta",
            "Standard",
            "Standard*",
            "Standard**",
            "Standard***",
            "OK"});
            this.cmbStatus.Size = new System.Drawing.Size(140, 23);
            this.cmbStatus.TabIndex = 3;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl10.Location = new System.Drawing.Point(37, 46);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(35, 17);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "Status";
            // 
            // cmbProduct
            // 
            this.cmbProduct.EditValue = "";
            this.cmbProduct.Location = new System.Drawing.Point(78, 14);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmbProduct.Properties.Appearance.Options.UseFont = true;
            this.cmbProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbProduct.Properties.DisplayMember = "Name";
            this.cmbProduct.Properties.NullText = "";
            this.cmbProduct.Properties.ValueMember = "id";
            this.cmbProduct.Properties.View = this.gridView2;
            this.cmbProduct.Size = new System.Drawing.Size(306, 24);
            this.cmbProduct.TabIndex = 1;
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowHeight = 23;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl5.Location = new System.Drawing.Point(27, 17);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(45, 17);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Product";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "id";
            this.gridColumn1.FieldName = "id";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Code";
            this.gridColumn2.FieldName = "Code";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Name";
            this.gridColumn3.FieldName = "Name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 264;
            // 
            // frmEditStandardDeliveryTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 372);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditStandardDeliveryTime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Standard Delivery Time";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColumn1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPackaging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtColumn8;
        private DevExpress.XtraEditors.LabelControl lblCustom8;
        private DevExpress.XtraEditors.TextEdit txtColumn7;
        private DevExpress.XtraEditors.LabelControl lblCustom7;
        private DevExpress.XtraEditors.TextEdit txtColumn6;
        private DevExpress.XtraEditors.LabelControl lblCustom6;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        public DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtColumn5;
        private DevExpress.XtraEditors.LabelControl lblCustom5;
        private DevExpress.XtraEditors.TextEdit txtColumn4;
        private DevExpress.XtraEditors.LabelControl lblCustom4;
        private DevExpress.XtraEditors.TextEdit txtColumn3;
        private DevExpress.XtraEditors.LabelControl lblCustom3;
        private DevExpress.XtraEditors.TextEdit txtColumn2;
        private DevExpress.XtraEditors.LabelControl lblCustom2;
        private DevExpress.XtraEditors.TextEdit txtColumn1;
        private DevExpress.XtraEditors.LabelControl lblColumn1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtPackaging;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SearchLookUpEdit cmbProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
    }
}