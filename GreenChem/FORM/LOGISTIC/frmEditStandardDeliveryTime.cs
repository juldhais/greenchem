﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditStandardDeliveryTime : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditStandardDeliveryTime(int id)
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            _id = id;
            cmbProduct.Properties.DataSource = db.Products.GetName();

            var sdt = db.StandardDeliveryTimes.First(x => x.id == _id);
            cmbProduct.EditValue = sdt.idProduct;
            cmbStatus.Text = sdt.Status;
            txtPackaging.Text = sdt.Packaging;
            txtColumn1.Text = sdt.Column1;
            txtColumn2.Text = sdt.Column2;
            txtColumn3.Text = sdt.Column3;
            txtColumn4.Text = sdt.Column4;
            txtColumn5.Text = sdt.Column5;
            txtColumn6.Text = sdt.Column6;
            txtColumn7.Text = sdt.Column7;
            txtColumn8.Text = sdt.Column8;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                StandardDeliveryTime sdt = db.StandardDeliveryTimes.First(x => x.id == _id);
                sdt.DateModified = db.GetServerDate();
                sdt.idProduct = (int?)cmbProduct.EditValue;
                sdt.Status = cmbStatus.Text;
                sdt.Packaging = txtPackaging.Text;
                sdt.Column1 = txtColumn1.Text;
                sdt.Column2 = txtColumn2.Text;
                sdt.Column3 = txtColumn3.Text;
                sdt.Column4 = txtColumn4.Text;
                sdt.Column5 = txtColumn5.Text;
                sdt.Column6 = txtColumn6.Text;
                sdt.Column7 = txtColumn7.Text;
                sdt.Column8 = txtColumn8.Text;
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}