﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuData
{
    public partial class frmNewCourier : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewCourier()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                Courier courier = new Courier();
                courier.Name = txtName.Text;
                courier.Address = txtAddress.Text;
                courier.Phone = txtPhone.Text;
                courier.Remarks = txtRemarks.Text;
                courier.PIC = txtPIC.Text;
                courier.Active = true;
                db.Couriers.InsertOnSubmit(courier);
                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtName.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtPhone.Text = string.Empty;
                txtRemarks.Text = string.Empty;
                txtPIC.Text = string.Empty;
                txtName.Focus();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}