﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmSetDONumberTS : DevExpress.XtraEditors.XtraForm
    {
        string _number;
        GreenChemDataContext db;

        public frmSetDONumberTS(string number)
        {
            InitializeComponent();

            _number = number;
            db = new GreenChemDataContext();
            var so = db.TransferStocks.First(x => x.TransactionNumber == _number);
            txtTransactionNumber.Text = so.TransactionNumber;
            txtDONumber.Text = so.DONumber;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var transferstocks = from x in db.TransferStocks
                                     where x.TransactionNumber == _number
                                     select new
                                         {
                                             x.id
                                         };

                foreach (var item in transferstocks)
                {
                    TransferStock transfer = db.TransferStocks.First(x => x.id == item.id);
                    transfer.DONumber = txtDONumber.Text;
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Error.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}