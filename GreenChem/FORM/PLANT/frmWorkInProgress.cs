﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmWorkInProgress : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmWorkInProgress()
        {
            InitializeComponent();
            cmbDateStart.DateTime = DateTime.Now.AddDays(-365);
            cmbDateEnd.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);
        }

        private void btnMark_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                DialogResult = XtraMessageBox.Show("Mark this Sales Order : " + number + " as Work In Progress?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    SalesOrder salesorder = db.SalesOrders.First(x => x.id == id);
                    salesorder.Status = "WIP";
                    db.SubmitChanges();

                    lblRefresh_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;

        }

        //private void btnUnMark_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int id = gridView.GetFocusedRowCellValue("id").ToInteger();
        //        string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
        //        DialogResult = XtraMessageBox.Show("Mark this Sales Order : " + number + " as Work In Progress?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
        //        if (DialogResult == DialogResult.Yes)
        //        {
        //            Cursor.Current = Cursors.WaitCursor;
        //            db = new GreenChemDataContext();
        //            SalesOrder salesorder = db.SalesOrders.First(x => x.id == id);
        //            salesorder.Status = "WIP";
        //            db.SubmitChanges();

        //            lblRefresh_Click(null, null);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }

        //    Cursor.Current = Cursors.Default;
        //}

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                var query = from x in db.SalesOrders
                            where x.Approved == true && x.ApprovedBy != null && x.Approved2 == true && x.Status == "OPEN"
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.DateOrder,
                                Customer = x.Customer.Name,
                                x.Status,
                                x.Remarks,
                                x.DateApproved,
                                ApprovedBy = db.UserAccounts.First(a => a.id == x.ApprovedBy).Name,
                                ApprovedBy2 = x.Approved2 != null ? db.UserAccounts.First(a => a.id == x.ApprovedBy2).Name : ""
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmEditSalesOrder form = new frmEditSalesOrder(gridView.GetFocusedRowCellValue("id").ToInteger(), true);
            form.Show();
            form.BringToFront();
            form.btnSave.Visible = false;
            Cursor.Current = Cursors.Default;
        }
    }
}