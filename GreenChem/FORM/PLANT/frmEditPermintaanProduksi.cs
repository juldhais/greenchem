﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmEditPermintaanProduksi : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditPermintaanProduksi(int id)
        {
            InitializeComponent();

            _id = id;

            db = new GreenChemDataContext();
            var produksi = db.PermintaanProduksis.First(x => x.id == _id);
            txtTransactionNumber.Text = produksi.TransactionNumber;
            txtNoRef.Text = produksi.NoRef;
            cmbTanggal.DateTime = produksi.TransactionDate;
            cmbTanggalDibutuhkan.EditValue = produksi.TanggalDibutuhkan;
            txtRemarks.Text = produksi.Remarks;
            txtPersyaratanPelanggan.Text = produksi.PersyaratanPelaggan;

            foreach (var item in produksi.DetailPermintaanProduksis)
            {
                TempPermintaanProduksi temp = new TempPermintaanProduksi();
                temp.idProduct = item.idProduct;
                temp.Quantity = item.Quantity;
                temp.Packing = item.Packing;
                temp.UoM = item.Product.UoM;
                tempPermintaanProduksiBindingSource.Add(temp);
            }

            gridControl.RefreshDataSource();

            lblRefresh_Click(null, null);
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbProduct.DataSource = db.Products.GetName();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                //delete old detail
                var olddetail = from x in db.DetailPermintaanProduksis
                                where x.idPermintaanProduksi == _id
                                select new
                                {
                                    x.id
                                };

                foreach (var item in olddetail)
                {
                    DetailPermintaanProduksi delete = db.DetailPermintaanProduksis.First(x => x.id == item.id);
                    db.DetailPermintaanProduksis.DeleteOnSubmit(delete);
                }

                PermintaanProduksi produksi = db.PermintaanProduksis.First(x => x.id == _id);
                produksi.TransactionNumber = txtTransactionNumber.Text;
                produksi.NoRef = txtNoRef.Text;
                produksi.TransactionDate = cmbTanggal.DateTime;
                produksi.TanggalDibutuhkan = cmbTanggalDibutuhkan.DateTime;
                produksi.idUserAccount = frmUtama._iduseraccount;
                produksi.Remarks = txtRemarks.Text;
                produksi.PersyaratanPelaggan = txtPersyaratanPelanggan.Text;

                foreach (TempPermintaanProduksi item in tempPermintaanProduksiBindingSource)
                {
                    if (item.idProduct.IsEmpty() || item.Quantity == 0) continue;
                    DetailPermintaanProduksi detail = new DetailPermintaanProduksi();
                    detail.idProduct = item.idProduct;
                    detail.Packing = item.Packing;
                    detail.Quantity = item.Quantity;
                    produksi.DetailPermintaanProduksis.Add(detail);
                }

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}