﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace GreenChem.MenuTransaction
{
    public partial class frmUpdateRemarksTransferStock : DevExpress.XtraEditors.XtraForm
    {
        string _nomor;
        GreenChemDataContext db;

        public frmUpdateRemarksTransferStock(string nomor)
        {
            InitializeComponent();

            _nomor = nomor;
            db = new GreenChemDataContext();
            var ts = db.TransferStocks.First(x => x.TransactionNumber == nomor);
            txtRemarks.Text = ts.Remarks;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                db = new GreenChemDataContext();
                var ts = db.TransferStocks.Where(x => x.TransactionNumber == _nomor);
                foreach (var item in ts) item.Remarks = txtRemarks.Text;

                db.SubmitChanges();

                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Error.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}