﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmNewTransferStock2 : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewTransferStock2()
        {
            InitializeComponent();
            RefreshData();
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbDate.DateTime = DateTime.Now;
                txtTransactionNumber.Text = db.GetNomorTransferStock();
                cmbExpedition.Properties.DataSource = db.Couriers.GetName();
                cmbProduct.DataSource = db.Products.GetName();
                cmbLocation.Properties.DataSource = db.Locations.GetName();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                foreach (TempTransferStock item in tempTransferStockBindingSource)
                {
                    if (item.idProduct == null) continue;

                    Product product = db.Products.First(x => x.id == item.idProduct);

                    TransferStock transfer = new TransferStock();
                    transfer.TransactionNumber = txtTransactionNumber.Text;
                    transfer.Status = cmbStatus.Text;
                    transfer.TransactionDate = cmbDate.DateTime;
                    transfer.idCourier = cmbExpedition.EditValue.ToNullInteger();
                    transfer.idLocation = cmbLocation.EditValue.ToNullInteger();
                    transfer.Remarks = txtRemarks.Text;
                    transfer.FOB = txtFOB.Text;
                    transfer.ShipVia = txtShipVia.Text;
                    transfer.FromLocation = cmbFromLocation.Text;
                    transfer.ToLocation = cmbToLocation.Text;
                    transfer.idUserAccount = frmUtama._iduseraccount;
                    transfer.idProduct = item.idProduct;
                    transfer.Cost = product.Cost;
                    transfer.Quantity = item.Quantity;
                    transfer.NoBatch = item.NoBatch;
                    transfer.Packaging = item.Packaging;
                    transfer.NoPKB = txtNoPKB.Text;
                    db.TransferStocks.InsertOnSubmit(transfer);
                }

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}