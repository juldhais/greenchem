﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewTransferStock : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewTransferStock()
        {
            InitializeComponent();
            cmbDate.DateTime = DateTime.Now;
            RefreshData();
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                txtTransactionNumber.Text = db.GetNomorTransferStock();
                cmbProduct.DataSource = db.Products.GetName();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }
            else if (e.Column.FieldName == "Stock1")
            {
                decimal stock1 = gridView.GetFocusedRowCellValue("Stock1").ToDecimal();
                decimal stock2 = gridView.GetFocusedRowCellValue("Stock2").ToDecimal();
                if (stock1 != stock2 * -1)
                    gridView.SetFocusedRowCellValue("Stock2", stock1 * -1);
            }
            else if (e.Column.FieldName == "Stock2")
            {
                decimal stock1 = gridView.GetFocusedRowCellValue("Stock1").ToDecimal();
                decimal stock2 = gridView.GetFocusedRowCellValue("Stock2").ToDecimal();
                if (stock1 != stock2 * -1)
                    gridView.SetFocusedRowCellValue("Stock1", stock2 * -1);
            }
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridView.DeleteSelectedRows();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                foreach (TempTransferStock item in tempTransferStockBindingSource)
                {
                    if (item.idProduct.IsNotEmpty())
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        product.Stock3 = product.Stock3.ToDecimal() + Math.Abs(item.Stock1);
                        //decimal stock = product.Stock;
                        //decimal stock2 = product.Stock2.ToDecimal();
                        //stock += item.Stock1;
                        //stock2 = stock2 + item.Stock2;

                        //if (stock < 0 || stock2.ToDecimal() < 0)
                        //{
                            //XtraMessageBox.Show("Insufficient stock for product : " + product.Name, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //return;
                        //}

                        TransferStock transfer = new TransferStock();
                        transfer.TransactionNumber = txtTransactionNumber.Text;
                        transfer.Status = cmbStatus.Text;
                        transfer.TransactionDate = cmbDate.DateTime;
                        transfer.Remarks = txtRemarks.Text;
                        transfer.idUserAccount = frmUtama._iduseraccount;
                        transfer.idProduct = item.idProduct;
                        transfer.Cost = product.Cost;
                        transfer.Stock = item.Stock1;
                        transfer.Stock2 = item.Stock2;
                        transfer.Stock3 = Math.Abs(item.Stock1);
                        transfer.ShipAddress = txtShipAddress.Text;
                        transfer.NoPKB = txtNoPKB.Text;
                        db.TransferStocks.InsertOnSubmit(transfer);
                    }
                }

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}