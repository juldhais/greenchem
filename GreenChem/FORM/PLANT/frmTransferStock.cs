﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmTransferStock : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmTransferStock()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Transfer Stock");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Transfer Stock");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Transfer Stock");
            btnApprove.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve Transfer Stock");
            btnApprove2.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve2 Transfer Stock");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Transfer Stock");
            btnUpdateRemarks.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Update Remarks Transfer Stock");

            if (System.IO.File.Exists("layout_transferstock.xml"))
                gridView.RestoreLayoutFromXml("layout_transferstock.xml");
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.TransferStocks
                            where x.TransactionDate.Date >= DateTime.Now.AddYears(-1)
                            group x by x.TransactionNumber into g
                            select new
                            {
                                TransactionNumber = g.First().TransactionNumber,
                                TransactionDate = g.First().TransactionDate,
                                UserAccount = db.UserAccounts.First(a => a.id == g.First().idUserAccount).Name,
                                FromLocation = g.First().FromLocation,
                                ToLocation = g.First().ToLocation,
                                Remarks = g.First().Remarks,
                                DateApproved = g.First().DateApproved,
                                DateApproved2 = g.First().DateApproved2,
                                ApprovedBy = g.First().ApprovedBy != null ? db.UserAccounts.First(a => a.id == g.First().ApprovedBy).Name : "",
                                ApprovedBy2 = g.First().ApprovedBy2 != null ? db.UserAccounts.First(a => a.id == g.First().ApprovedBy2).Name : "",
                                Status = g.First().Status,
                                NoPKB = g.First().NoPKB,
                                DONumber = g.First().DONumber
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query.OrderByDescending(x => x.TransactionNumber);
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            MenuTransaction2.frmNewTransferStock2 form = new MenuTransaction2.frmNewTransferStock2();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                var transfers = db.TransferStocks.Where(x => x.TransactionNumber == number);
                if (transfers.First().Approved2 == true)
                {
                    XtraMessageBox.Show("Already approved!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                MenuTransaction2.frmEditTransferStock2 form = new MenuTransaction2.frmEditTransferStock2(gridView.GetFocusedRowCellValue("TransactionNumber").Safe());
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Are you sure want to delete this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();

                    string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                    var transfers = db.TransferStocks.Where(x => x.TransactionNumber == number);
                    if (transfers.First().Approved2 == true)
                    {
                        XtraMessageBox.Show("Already approved!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    var detail = db.TransferStocks.Where(x => x.TransactionNumber == number);

                    foreach (var item in detail)
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);

                        product.TransitStock = product.TransitStock.ToDecimal() - item.Quantity.ToDecimal();

                        if (item.ToLocation == "JKT") product.Stock += item.Quantity.ToDecimal();
                        else if (item.ToLocation == "BPP") product.Stock2 = product.Stock2.ToDecimal() + item.Quantity.ToDecimal();
                        if (item.ToLocation == "BTG") product.Stock3 = product.Stock3.ToDecimal() + item.Quantity.ToDecimal();
                    }

                    db.TransferStocks.DeleteAllOnSubmit(detail);
                    db.SubmitChanges();

                    lblRefresh_Click(null, null);
                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                MenuTransaction2.frmEditTransferStock2 form = new MenuTransaction2.frmEditTransferStock2(gridView.GetFocusedRowCellValue("TransactionNumber").Safe());
                form.btnSave.Visible = false;
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = XtraMessageBox.Show("Approve this transfer stock?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();

                var transfers = db.TransferStocks.Where(x => x.TransactionNumber == number);
                if (transfers.First().Approved == true)
                {
                    XtraMessageBox.Show("Already approved!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                foreach (var item in transfers)
                {
                    TransferStock transfer = db.TransferStocks.First(x => x.id == item.id);
                    transfer.Approved = true;
                    transfer.ApprovedBy = frmUtama._iduseraccount;
                    transfer.DateApproved = DateTime.Now;
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Transfer stock is approved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblRefresh_Click(null, null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnApprove2_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = XtraMessageBox.Show("Approve this transfer stock?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();

                var transfers = db.TransferStocks.Where(x => x.TransactionNumber == number);
                if (transfers.First().Approved2 == true)
                {
                    XtraMessageBox.Show("Already approved!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (transfers.First().Approved == false || transfers.First().Approved == null)
                {
                    XtraMessageBox.Show("Transfer stock is not approved by Coordinator yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                foreach (var item in transfers)
                {
                    TransferStock transfer = db.TransferStocks.First(x => x.id == item.id);
                    transfer.Approved2 = true;
                    transfer.ApprovedBy2 = frmUtama._iduseraccount;
                    transfer.DateApproved2 = DateTime.Now;
                    //transfer.Stock3 = 0;

                    Product product = db.Products.First(x => x.id == item.idProduct);
                    if (item.FromLocation == "JKT") product.Stock -= item.Quantity.ToDecimal();
                    else if (item.FromLocation == "BPP") product.Stock2 = product.Stock2.ToDecimal() - item.Quantity.ToDecimal();
                    if (item.FromLocation == "BTG") product.Stock3 = product.Stock3.ToDecimal() - item.Quantity.ToDecimal();

                    product.TransitStock = product.TransitStock.ToDecimal() + item.Quantity.ToDecimal();
                }

                db.SubmitChanges();

                XtraMessageBox.Show("Transfer stock is approved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblRefresh_Click(null, null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnUpdateRemarks_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                db = new GreenChemDataContext();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();

                var transfers = db.TransferStocks.Where(x => x.TransactionNumber == number);
                if (transfers.First().Approved != true)
                {
                    XtraMessageBox.Show("Transaction is not approved yet!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                frmUpdateRemarksTransferStock form = new frmUpdateRemarksTransferStock(number);
                form.ShowDialog();
                lblRefresh_Click(null, null);
                
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                Cursor.Current = Cursors.WaitCursor;
                Printing.TransferStock(number);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    gridView.SaveLayoutToXml("layout_transferstock.xml");
                    XtraMessageBox.Show("Layout saved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch { }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnSetDONumber_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string donumber = gridView.GetFocusedRowCellValue("DONumber").Safe();
                if (donumber.IsNotEmpty())
                {
                    XtraMessageBox.Show("DO Number is already set.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                frmSetDONumberTS form = new frmSetDONumberTS(number);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }
    }
}