﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmNewPenyerahanProduksi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;
        string nomor = string.Empty;

        public frmNewPenyerahanProduksi()
        {
            InitializeComponent();
            cmbTanggalPermintaan.DateTime = DateTime.Now;
            cmbTanggalPenyerahan.DateTime = DateTime.Now;
            cmbTanggalSelesai.DateTime = DateTime.Now;
            lblRefresh_Click(null, null);
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                //txtTransactionNumber.Text = db.GetNomorPenyerahanProduksi();
                cmbProduct.DataSource = db.Products.GetName();
                txtYangMenyerahkan.Text = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount).Name;

                cmbNumber.Properties.DataSource = from x in db.PermintaanProduksis
                                                  where x.TransactionDate > DateTime.Now.AddYears(-1)
                                                  && x.ApprovedBy != null
                                                  && !db.PenyerahanProduksis.Any(a => a.idPermintaanProduksi == x.id)
                                                  select new
                                                  {
                                                      x.id,
                                                      x.TransactionNumber,
                                                      x.TransactionDate,
                                                      x.Remarks
                                                  };
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void cmbNumber_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var query = from x in db.DetailPermintaanProduksis
                            where x.idPermintaanProduksi == cmbNumber.EditValue.ToInteger()
                            select new
                            {
                                x.idProduct,
                                x.Quantity,
                                x.Packing,
                                x.Product.UoM,
                                x.PermintaanProduksi.TransactionDate
                            };

                detailPenyerahanProduksiBindingSource.Clear();
                foreach (var item in query)
                {
                    DetailPenyerahanProduksi detail = new DetailPenyerahanProduksi();
                    detail.idProduct = item.idProduct;
                    detail.Quantity = item.Quantity;
                    detail.Kemasan = item.Packing;
                    detail.UoM = item.UoM;
                    detailPenyerahanProduksiBindingSource.Add(detail);
                    cmbTanggalPermintaan.DateTime = item.TransactionDate;
                }

                gridControl.RefreshDataSource();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("Ratio", 1);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                PenyerahanProduksi produksi = new PenyerahanProduksi();
                produksi.idPermintaanProduksi = cmbNumber.EditValue.ToNullInteger();
                produksi.NomorPermintaanProduksi = cmbNumber.Text;
                produksi.TanggalPenyerahan = cmbTanggalPenyerahan.DateTime;
                produksi.TanggalPermintaanProduksi = cmbTanggalPermintaan.DateTime;
                produksi.TanggalSelesaiProduksi = cmbTanggalSelesai.DateTime;
                produksi.YangMenyerahkan = txtYangMenyerahkan.Text;
                produksi.DisetujuiOleh = txtDisetujuiOleh.Text;
                produksi.DiterimaOleh = txtDiterimaOleh.Text;
                produksi.idUserAccount = frmUtama._iduseraccount;
                produksi.Stock = cmbStock.Text;

                foreach (DetailPenyerahanProduksi item in detailPenyerahanProduksiBindingSource)
                {
                    if (item.idProduct.IsEmpty() || item.Quantity == 0) continue;
                    DetailPenyerahanProduksi detail = new DetailPenyerahanProduksi();
                    detail.idProduct = item.idProduct;
                    detail.Kemasan = item.Kemasan;
                    detail.Quantity = item.Quantity;
                    detail.Ratio = item.Ratio;
                    detail.UoM = item.UoM;
                    produksi.DetailPenyerahanProduksis.Add(detail);
                }

                db.PenyerahanProduksis.InsertOnSubmit(produksi);
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                Close();
            }
        }


    }
}