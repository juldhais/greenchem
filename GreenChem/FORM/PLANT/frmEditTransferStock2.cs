﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmEditTransferStock2 : DevExpress.XtraEditors.XtraForm
    {
        string _number;
        GreenChemDataContext db;

        public frmEditTransferStock2(string number)
        {
            InitializeComponent();
            RefreshData();

            _number = number;
            var transfers = db.TransferStocks.Where(x => x.TransactionNumber == _number);
            var first = transfers.First();
            txtTransactionNumber.Text = first.TransactionNumber;
            cmbDate.DateTime = first.TransactionDate;
            cmbStatus.Text = first.Status;
            cmbExpedition.EditValue = first.idCourier;
            cmbLocation.EditValue = first.idLocation;
            txtFOB.Text = first.FOB;
            cmbFromLocation.Text = first.FromLocation;
            cmbToLocation.Text = first.ToLocation;
            txtRemarks.Text = first.Remarks;
            txtShipVia.Text = first.ShipVia;
            txtNoPKB.Text = first.NoPKB;

            foreach (var item in transfers)
            {
                TempTransferStock temp = new TempTransferStock();
                temp.idProduct = item.idProduct;
                temp.Packaging = item.Packaging;
                temp.NoBatch = item.NoBatch;
                temp.UoM = item.Product.UoM;
                temp.Quantity = item.Quantity.ToDecimal();
                tempTransferStockBindingSource.Add(temp);
            }
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                cmbExpedition.Properties.DataSource = db.Couriers.GetName();
                cmbProduct.DataSource = db.Products.GetName();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                //delete old
                var deletes = from x in db.TransferStocks
                              where x.TransactionNumber == _number
                              select new
                              {
                                  x.id,
                                  x.idProduct,
                                  x.FromLocation,
                                  x.ToLocation,
                                  x.Quantity
                              };

                foreach (var item in deletes)
                {
                    TransferStock delete = db.TransferStocks.First(x => x.id == item.id);
                    db.TransferStocks.DeleteOnSubmit(delete);
                }
                db.SubmitChanges();

                //insert new
                foreach (TempTransferStock item in tempTransferStockBindingSource)
                {
                    if (item.idProduct == null) continue;

                    Product product = db.Products.First(x => x.id == item.idProduct);

                    TransferStock transfer = new TransferStock();
                    transfer.TransactionNumber = txtTransactionNumber.Text;
                    transfer.Status = cmbStatus.Text;
                    transfer.TransactionDate = cmbDate.DateTime;
                    transfer.idCourier = cmbExpedition.EditValue.ToNullInteger();
                    transfer.idLocation = cmbLocation.EditValue.ToNullInteger();
                    transfer.Remarks = txtRemarks.Text;
                    transfer.FOB = txtFOB.Text;
                    transfer.ShipVia = txtShipVia.Text;
                    transfer.FromLocation = cmbFromLocation.Text;
                    transfer.ToLocation = cmbToLocation.Text;
                    transfer.idUserAccount = frmUtama._iduseraccount;
                    transfer.idProduct = item.idProduct;
                    transfer.Cost = product.Cost;
                    transfer.Quantity = item.Quantity;
                    transfer.NoBatch = item.NoBatch;
                    transfer.Packaging = item.Packaging;
                    transfer.NoPKB = txtNoPKB.Text;
                    db.TransferStocks.InsertOnSubmit(transfer);
                }

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}