﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditPenyerahanProduksi : DevExpress.XtraEditors.XtraForm
    {
        int _id;
        GreenChemDataContext db;

        public frmEditPenyerahanProduksi(int id)
        {
            InitializeComponent();

            _id = id;

            lblRefresh_Click(null, null);

            var produksi = db.PenyerahanProduksis.First(x => x.id == _id);
            txtTransactionNumber.Text = produksi.NomorPermintaanProduksi;
            cmbTanggalPenyerahan.EditValue = produksi.TanggalPenyerahan;
            cmbTanggalPermintaan.EditValue = produksi.TanggalPermintaanProduksi;
            cmbTanggalSelesai.EditValue = produksi.TanggalSelesaiProduksi;
            txtYangMenyerahkan.Text = produksi.YangMenyerahkan;
            txtDisetujuiOleh.Text = produksi.DisetujuiOleh;
            txtDiterimaOleh.Text = produksi.DiterimaOleh;
            cmbStock.Text = produksi.Stock;
            if (cmbStock.Text.IsEmpty()) cmbStock.Text = "JKT";

            detailPenyerahanProduksiBindingSource.DataSource = produksi.DetailPenyerahanProduksis;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                txtTransactionNumber.Text = db.GetNomorPenyerahanProduksi();
                cmbProduct.DataSource = db.Products.GetName();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("Ratio", 1);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                db.SubmitChanges();
                db.ExecuteCommand("DELETE FROM dbo.DetailPenyerahanProduksi WHERE idPenyerahanProduksi = " + _id);             

                PenyerahanProduksi produksi = db.PenyerahanProduksis.First(x => x.id == _id);
                produksi.NomorPermintaanProduksi = txtTransactionNumber.Text;
                produksi.TanggalPenyerahan = cmbTanggalPenyerahan.DateTime;
                produksi.TanggalPermintaanProduksi = cmbTanggalPermintaan.DateTime;
                produksi.TanggalSelesaiProduksi = cmbTanggalSelesai.DateTime;
                produksi.YangMenyerahkan = txtYangMenyerahkan.Text;
                produksi.DisetujuiOleh = txtDisetujuiOleh.Text;
                produksi.DiterimaOleh = txtDiterimaOleh.Text;
                //produksi.idUserAccount = frmUtama._iduseraccount;
                produksi.Stock = cmbStock.Text;

                foreach (DetailPenyerahanProduksi item in detailPenyerahanProduksiBindingSource)
                {
                    if (item.idProduct.IsEmpty() || item.Quantity == 0) continue;
                    DetailPenyerahanProduksi detail = new DetailPenyerahanProduksi();
                    detail.idProduct = item.idProduct;
                    detail.Kemasan = item.Kemasan;
                    detail.Quantity = item.Quantity;
                    detail.Ratio = item.Ratio;
                    detail.UoM = item.UoM;
                    produksi.DetailPenyerahanProduksis.Add(detail);
                }

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}