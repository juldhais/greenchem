﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmNewPermintaanProduksi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmNewPermintaanProduksi()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                txtTransactionNumber.Text = db.GetNomorPermintaanProduksi();
                cmbProduct.DataSource = db.Products.GetName();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                PermintaanProduksi produksi = new PermintaanProduksi();
                produksi.TransactionNumber = txtTransactionNumber.Text;
                produksi.NoRef = txtNoRef.Text;
                produksi.TransactionDate = cmbTanggal.DateTime;
                produksi.TanggalDibutuhkan = cmbTanggalDibutuhkan.DateTime;
                produksi.idUserAccount = frmUtama._iduseraccount;
                produksi.Remarks = txtRemarks.Text;
                produksi.PersyaratanPelaggan = txtPersyaratanPelanggan.Text;

                foreach (TempPermintaanProduksi item in tempPermintaanProduksiBindingSource)
                {
                    if (item.idProduct.IsEmpty() || item.Quantity == 0) continue;
                    DetailPermintaanProduksi detail = new DetailPermintaanProduksi();
                    detail.idProduct = item.idProduct;
                    detail.Packing = item.Packing;
                    detail.Quantity = item.Quantity;
                    produksi.DetailPermintaanProduksis.Add(detail);
                }

                db.PermintaanProduksis.InsertOnSubmit(produksi);
                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}