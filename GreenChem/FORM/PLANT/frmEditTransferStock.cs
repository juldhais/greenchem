﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmEditTransferStock : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmEditTransferStock(string transactionnumber)
        {
            InitializeComponent();
            db = new GreenChemDataContext();
            cmbProduct.DataSource = db.Products.GetNameAll();
            txtTransactionNumber.Text = transactionnumber;

            var transfer = db.TransferStocks.Where(x => x.TransactionNumber == txtTransactionNumber.Text);
            var first = transfer.First();
            cmbDate.DateTime = first.TransactionDate;
            txtRemarks.Text = first.Remarks;
            cmbStatus.Text = first.Status;
            txtShipAddress.Text = first.ShipAddress;
            txtNoPKB.Text = first.NoPKB;

            foreach (var item in transfer)
            {
                TempTransferStock temp = new TempTransferStock();
                temp.idProduct = item.idProduct.ToInteger();
                temp.UoM = item.Product.UoM;
                temp.Stock1 = item.Stock;
                temp.Stock2 = item.Stock2;
                tempTransferStockBindingSource.Add(temp);
            }

            gridControl.RefreshDataSource();
        }

        private void gridView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "idProduct" && gridView.GetFocusedRowCellValue("idProduct").IsNotEmpty())
            {
                int id = gridView.GetFocusedRowCellValue("idProduct").ToInteger();
                var product = db.Products.First(x => x.id == id);
                gridView.SetFocusedRowCellValue("UoM", product.UoM);
            }
            else if (e.Column.FieldName == "Stock1")
            {
                decimal stock1 = gridView.GetFocusedRowCellValue("Stock1").ToDecimal();
                decimal stock2 = gridView.GetFocusedRowCellValue("Stock2").ToDecimal();
                if (stock1 != stock2 * -1)
                    gridView.SetFocusedRowCellValue("Stock2", stock1 * -1);
            }
            else if (e.Column.FieldName == "Stock2")
            {
                decimal stock1 = gridView.GetFocusedRowCellValue("Stock1").ToDecimal();
                decimal stock2 = gridView.GetFocusedRowCellValue("Stock2").ToDecimal();
                if (stock1 != stock2 * -1)
                    gridView.SetFocusedRowCellValue("Stock1", stock2 * -1);
            }
        }

        private void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                gridView.DeleteSelectedRows();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                //repair stock lama
                var detaillama = db.TransferStocks.Where(x => x.TransactionNumber == txtTransactionNumber.Text);
                foreach (var item in detaillama)
                {
                    if (item.Approved2 == true)
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        product.Stock -= item.Stock;
                        product.Stock2 = product.Stock2.ToDecimal() - item.Stock;
                        product.Stock3 = product.Stock3 + Math.Abs(item.Stock);
                        
                    }
                }
                db.TransferStocks.DeleteAllOnSubmit(detaillama);
                db.SubmitChanges();

                foreach (TempTransferStock item in tempTransferStockBindingSource)
                {
                    if (item.idProduct.IsNotEmpty())
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        
                        //product.Stock += item.Stock1;
                        //product.Stock2 = product.Stock2.ToDecimal() + item.Stock2;

                        TransferStock transfer = new TransferStock();
                        transfer.Status = cmbStatus.Text;
                        transfer.TransactionNumber = txtTransactionNumber.Text;
                        transfer.TransactionDate = cmbDate.DateTime;
                        transfer.Remarks = txtRemarks.Text;
                        transfer.idUserAccount = frmUtama._iduseraccount;
                        transfer.idProduct = item.idProduct;
                        transfer.Stock = item.Stock1;
                        transfer.Stock2 = item.Stock2;
                        transfer.Stock3 = Math.Abs(item.Stock1);
                        transfer.Cost = product.Cost;
                        transfer.ShipAddress = txtShipAddress.Text;
                        transfer.NoPKB = txtNoPKB.Text;
                        db.TransferStocks.InsertOnSubmit(transfer);
                    }
                }

                db.SubmitChanges();
                XtraMessageBox.Show("Data saved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = XtraMessageBox.Show("Cancel this transaction?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (DialogResult == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}