﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction
{
    public partial class frmPenyerahanProduksi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPenyerahanProduksi()
        {
            InitializeComponent();
            lblRefresh_Click(null, null);

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Penyerahan Produksi");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Penyerahan Produksi");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Penyerahan Produksi");
            btnPrint.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Print Penyerahan Produksi");
            btnApprove.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve Penyerahan Produksi");
            btnApprove2.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve2 Penyerahan Produksi");
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            frmNewPenyerahanProduksi form = new frmNewPenyerahanProduksi();
            form.ShowDialog();
            lblRefresh_Click(null, null);
            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();

                string disetujuioleh = gridView.GetFocusedRowCellValue("DisetujuiOleh").Safe();
                if (disetujuioleh.IsNotEmpty())
                {
                    XtraMessageBox.Show("Already approved!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                frmEditPenyerahanProduksi form = new frmEditPenyerahanProduksi(id);
                form.ShowDialog();
                lblRefresh_Click(null, null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("NomorPermintaanProduksi").Safe();
                DialogResult = XtraMessageBox.Show("Delete this data " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    //restore stock
                    var detail = from x in db.DetailPenyerahanProduksis
                                 where x.idPenyerahanProduksi == id
                                 select new
                                 {
                                     x.idProduct,
                                     Quantity = x.Quantity * x.Ratio
                                 };

                    foreach (var item in detail)
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        product.Stock -= item.Quantity.ToDecimal();
                    }
                    db.SubmitChanges();

                    db.ExecuteCommand("DELETE FROM dbo.DetailPenyerahanProduksi WHERE idPenyerahanProduksi = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.PenyerahanProduksi WHERE id = " + id);

                    lblRefresh_Click(null, null);
                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();
                var query = from x in db.PenyerahanProduksis
                            //where x.TanggalPermintaanProduksi.HasValue
                            //&& x.TanggalPermintaanProduksi.Value.Date >= cmbDateStart.DateTime.Date && x.TanggalPermintaanProduksi.Value.Date <= cmbDateEnd.DateTime.Date
                            orderby x.NomorPermintaanProduksi descending
                            select new
                            {
                                x.id,
                                x.NomorPermintaanProduksi,
                                x.TanggalPenyerahan,
                                x.TanggalPermintaanProduksi,
                                x.TanggalSelesaiProduksi,
                                x.YangMenyerahkan,
                                x.DisetujuiOleh,
                                x.DiterimaOleh,
                                x.DateApproved,
                                x.DateApproved2,
                                ApprovedBy2 = x.ApprovedBy2 != null ? db.UserAccounts.First(a => a.id == x.ApprovedBy2).Name : ""
                            };

                gridControl.DataSource = query;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string nomor = gridView.GetFocusedRowCellValue("NomorPermintaanProduksi").Safe();

                string disetujui = gridView.GetFocusedRowCellValue("DisetujuiOleh").Safe();
                if (disetujui.IsNotEmpty())
                {
                    XtraMessageBox.Show("Already approved!", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                
                DialogResult = XtraMessageBox.Show("Are you sure want to approve this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    PenyerahanProduksi produksi = db.PenyerahanProduksis.First(x => x.id == id);
                    produksi.DisetujuiOleh = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount).Name;
                    produksi.ApprovedBy = frmUtama._iduseraccount;
                    produksi.Approved = true;
                    produksi.DateApproved = DateTime.Now;
                    db.SubmitChanges();
                    lblRefresh_Click(null, null);

                    XtraMessageBox.Show("Data approved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnApprove2_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string nomor = gridView.GetFocusedRowCellValue("NomorPermintaanProduksi").Safe();
                string disetujui = gridView.GetFocusedRowCellValue("DisetujuiOleh").Safe();
                string approved2 = gridView.GetFocusedRowCellValue("Approved2").Safe();

                if (approved2 != "")
                {
                    XtraMessageBox.Show("Transaction is already approved.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (disetujui.IsEmpty())
                {
                    XtraMessageBox.Show("Transaction is not approved by Coordinator yet.", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                DialogResult = XtraMessageBox.Show("Are you sure want to approve this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    PenyerahanProduksi produksi = db.PenyerahanProduksis.First(x => x.id == id);
                    produksi.Approved2 = true;
                    produksi.ApprovedBy2 = frmUtama._iduseraccount;
                    produksi.DateApproved2 = DateTime.Now;

                    //stok
                    foreach (var item in produksi.DetailPenyerahanProduksis)
                    {
                        Product product = db.Products.First(x => x.id == item.idProduct);
                        if (produksi.Stock == "JKT" || produksi.Stock.IsEmpty())
                            product.Stock += item.Quantity.ToDecimal();
                        else if (produksi.Stock == "BPP") product.Stock2 = product.Stock2.ToDecimal() + item.Quantity.ToDecimal();
                        else if (produksi.Stock == "BTG") product.Stock3 = product.Stock3.ToDecimal() + item.Quantity.ToDecimal();
                    }

                    db.SubmitChanges();
                    lblRefresh_Click(null, null);

                    XtraMessageBox.Show("Data approved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditPenyerahanProduksi form = new frmEditPenyerahanProduksi(id);
                form.btnSave.Enabled = false;
                form.ShowDialog();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                gridControl.ShowPrintPreview();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void gridView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            try
            {
                DateTime? tanggalselesai = gridView.GetRowCellValue(e.RowHandle, "TanggalSelesaiProduksi").ToNullDateTime();
                DateTime? tanggalpenyerahan = gridView.GetRowCellValue(e.RowHandle, "TanggalPenyerahan").ToNullDateTime();

                if (tanggalselesai.HasValue && tanggalpenyerahan.HasValue)
                {
                    int selisih = (tanggalselesai.Value - tanggalpenyerahan.Value).Days;
                    if (selisih > 0)
                        e.Appearance.ForeColor = System.Drawing.Color.Red;
                    
                }
            }
            catch { }
        }


    }
}