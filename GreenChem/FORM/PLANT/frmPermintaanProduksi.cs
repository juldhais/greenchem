﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using JulFunctions;

namespace GreenChem.MenuTransaction2
{
    public partial class frmPermintaanProduksi : DevExpress.XtraEditors.XtraForm
    {
        GreenChemDataContext db;

        public frmPermintaanProduksi()
        {
            InitializeComponent();
            RefreshData();

            btnNew.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "New Permintaan Produksi");
            btnEdit.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Edit Permintaan Produksi");
            btnDelete.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Delete Permintaan Produksi");
            btnView.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "View Permintaan Produksi");
            btnApprove.Visible = db.CheckUserRole(frmUtama._iduseraccount.Value, "Approve Permintaan Produksi");
        }

        private void RefreshData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                db = new GreenChemDataContext();

                DateTime limit = DateTime.Today.AddYears(-1);

                var query = from x in db.PermintaanProduksis
                            where x.TransactionDate.Date >= limit
                            orderby x.TransactionDate descending
                            select new
                            {
                                x.id,
                                x.TransactionNumber,
                                x.NoRef,
                                x.TransactionDate,
                                x.TanggalDibutuhkan,
                                x.Remarks,
                                x.PersyaratanPelaggan,
                                x.DateApproved,
                                ApprovedBy = x.ApprovedBy != null ? db.UserAccounts.First(a => a.id == x.ApprovedBy).Name : ""
                            };

                int rowhandle = gridView.FocusedRowHandle;
                gridControl.DataSource = query;
                gridView.FocusedRowHandle = rowhandle;
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                frmNewPermintaanProduksi form = new frmNewPermintaanProduksi();
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditPermintaanProduksi form = new frmEditPermintaanProduksi(id);
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string number = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                DialogResult = XtraMessageBox.Show("Hapus permintaan produksi : " + number + "?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();

                    db.ExecuteCommand("DELETE FROM dbo.DetailPermintaanProduksi WHERE idPermintaanProduksi = " + id);
                    db.ExecuteCommand("DELETE FROM dbo.PermintaanProduksi WHERE id = " + id);
                    db.ExecuteCommand("UPDATE dbo.PenyerahanProduksi SET idPermintaanProduksi = null WHERE idPermintaanProduksi = " + id);

                    RefreshData();

                    XtraMessageBox.Show("Data deleted successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                frmEditPermintaanProduksi form = new frmEditPermintaanProduksi(id);
                form.btnSave.Visible = false;
                form.ShowDialog();
                RefreshData();
            }
            catch { }

            Cursor.Current = Cursors.Default;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                gridControl.ShowPrintPreview();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                int id = gridView.GetFocusedRowCellValue("id").ToInteger();
                string nomor = gridView.GetFocusedRowCellValue("TransactionNumber").Safe();
                string disetujui = gridView.GetFocusedRowCellValue("ApprovedBy").Safe();

                DialogResult = XtraMessageBox.Show("Are you sure want to approve this data?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (DialogResult == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    db = new GreenChemDataContext();
                    PermintaanProduksi produksi = db.PermintaanProduksis.First(x => x.id == id);
                    produksi.ApprovedBy = frmUtama._iduseraccount;
                    produksi.DateApproved = DateTime.Now;

                    db.SubmitChanges();
                    RefreshData();

                    XtraMessageBox.Show("Data approved successfully.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Gagal.\n" + ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}