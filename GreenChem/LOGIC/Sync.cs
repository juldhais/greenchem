﻿using System.IO;
using System.Linq;
using JulFunctions;
using System.Data.Linq;

namespace GreenChem
{
    public static class Sync
    {
        public static void UploadToFTP(string remotefile, string localfile)
        {
            GreenChemDataContext db = new GreenChemDataContext();
            string ftpserver = db.GetConfiguration("FTPServer");
            string ftpfolder = db.GetConfiguration("FTPFolder");
            string ftpusername = db.GetConfiguration("FTPUsername");
            string ftppassword = db.GetConfiguration("FTPPassword");
            if (ftpserver.IsNotEmpty() && remotefile.IsNotEmpty() && localfile.IsNotEmpty())
            {
                FTP ftp = new FTP(ftpserver, ftpusername, ftppassword);
                ftp.Upload(ftpfolder + "/" + remotefile, localfile);
            }
        }

        public static void DownloadFromFTP(string remotefile, string localfile)
        {
            GreenChemDataContext db = new GreenChemDataContext();
            string ftpserver = db.GetConfiguration("FTPServer");
            string ftpfolder = db.GetConfiguration("FTPFolder");
            string ftpusername = db.GetConfiguration("FTPUsername");
            string ftppassword = db.GetConfiguration("FTPPassword");
            if (ftpserver.IsNotEmpty() && remotefile.IsNotEmpty() && localfile.IsNotEmpty())
            {
                FTP ftp = new FTP(ftpserver, ftpusername, ftppassword);
                ftp.Download(ftpfolder + "/" + remotefile, localfile);
            }
        }

        public static bool UploadToDatabase(string remotefile, string localfile)
        {
            try
            {
                GreenChemDataContext db = new GreenChemDataContext();
                FileStream stream = new FileStream(localfile, FileMode.Open);
                byte[] bytedata = new byte[stream.Length];
                stream.Read(bytedata, 0, stream.Length.ToInteger());
                Binary binary = new Binary(bytedata);
                stream.Close();

                Attachment attachment;
                bool isnew = false;

                if (db.Attachments.Any(x => x.FileName == remotefile))
                    attachment = db.Attachments.First(x => x.FileName == remotefile);
                else
                {
                    attachment = new Attachment();
                    isnew = true;
                }

                attachment.FileName = remotefile;
                attachment.FileData = binary;
                if (isnew) db.Attachments.InsertOnSubmit(attachment);
                db.SubmitChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool DownloadFromDatabase(string remotefile, string localfile)
        {
            try
            {
                GreenChemDataContext db = new GreenChemDataContext();
                Attachment attachment = db.Attachments.First(x => x.FileName == remotefile);
                byte[] bytedata = attachment.FileData.ToArray();

                FileStream stream = new FileStream(localfile, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                stream.Write(bytedata, 0, bytedata.Length);
                stream.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
