﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GreenChem
{
    public static class ExtensionFunctions
    {
        public static DateTime? ToNullDateTime(this object value)
        {
            try
            {
                if (value == null) return null;
                else return Convert.ToDateTime(value);
            }
            catch
            {
                return null;
            }
        }

        public static int? ToNullInteger(this object value)
        {
            try
            {
                if (value == null) return null;
                else return Convert.ToInt32(value);
            }
            catch
            {
                return null;
            }
        }

        public static int CountWorkDays(this DateTime startdate, DateTime enddate, params DateTime[] holidays)
        {
            startdate = startdate.Date;
            enddate = enddate.Date;
            if (startdate > enddate) return 0;

            TimeSpan span = enddate - startdate;
            int workdays = span.Days + 1;
            int fullweekcount = workdays / 7;
            // find out if there are weekends during the time exceedng the full weeks
            if (workdays > fullweekcount * 7)
            {
                // we are here to find out if there is a 1-day or 2-days weekend
                // in the time interval remaining after subtracting the complete weeks
                int firstdayofweek = (int)startdate.DayOfWeek;
                int lastdayofweek = (int)enddate.DayOfWeek;
                if (lastdayofweek < firstdayofweek)
                    lastdayofweek += 7;
                if (firstdayofweek <= 6)
                {
                    if (lastdayofweek >= 7)// Both Saturday and Sunday are in the remaining time interval
                        workdays -= 2;
                    else if (lastdayofweek >= 6)// Only Saturday is in the remaining time interval
                        workdays -= 1;
                }
                else if (firstdayofweek <= 7 && lastdayofweek >= 7)// Only Sunday is in the remaining time interval
                    workdays -= 1;
            }

            // subtract the weekends during the full weeks in the interval
            workdays -= fullweekcount + fullweekcount;

            // subtract the number of bank holidays during the time interval
            foreach (DateTime holiday in holidays)
            {
                DateTime h = holiday.Date;
                if (startdate <= h && h <= enddate)
                    --workdays;
            }

            return workdays;
        }
    }
}
