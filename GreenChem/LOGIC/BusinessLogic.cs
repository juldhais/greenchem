﻿using System.Data.Linq;
using System.Linq;
using JulFunctions;
using System;

namespace GreenChem
{
    public static class BusinessLogic
    {
        public static DateTime GetServerDate(this GreenChemDataContext db)
        {
            return db.ExecuteQuery<DateTime>("SELECT GETDATE()").FirstOrDefault();
        }

        public static string GetConfiguration(this GreenChemDataContext db, string key)
        {
            try
            {
                Configuration config = db.Configurations.First(x => x.K == key);
                return config.V;
            }
            catch
            {
                return "";
            }
        }

        public static int GetPrintCount(this GreenChemDataContext db, string report, string transactionnumber)
        {
            try
            {
                if (db.PrintCounts.Any(x => x.Report == report && x.TransactionNumber == transactionnumber))
                {
                    var printcount = db.PrintCounts.First(x => x.Report == report && x.TransactionNumber == transactionnumber);
                    return printcount.Count + 1;
                }
                else return 1;
            }
            catch
            {
                return 1;
            }
        }

        public static void UpdatePrintCount(this GreenChemDataContext db, string report, string transactionnumber)
        {
            try
            {
                if (db.PrintCounts.Any(x => x.Report == report && x.TransactionNumber == transactionnumber))
                {
                    PrintCount printcount = db.PrintCounts.First(x => x.Report == report && x.TransactionNumber == transactionnumber);
                    printcount.Count += 1;
                }
                else
                {
                    PrintCount princount = new PrintCount();
                    princount.Report = report;
                    princount.TransactionNumber = transactionnumber;
                    princount.Count = 1;
                    db.PrintCounts.InsertOnSubmit(princount);
                }
                db.SubmitChanges();
            }
            catch { }
        }

        public static void SetConfiguration(this GreenChemDataContext db, string key, string value)
        {
            try
            {
                if (db.Configurations.Any(x => x.K == key))
                {
                    Configuration config = db.Configurations.First(x => x.K == key);
                    config.V = value;
                }
                else
                {
                    Configuration config = new Configuration();
                    config.K = key;
                    config.V = value;
                    db.Configurations.InsertOnSubmit(config);
                }
                db.SubmitChanges();
            }
            catch { }
        }

        public static bool CheckUserRole(this GreenChemDataContext db, int iduseraccount, string role)
        {
            try
            {
                var userrole = db.UserAccounts.First(x => x.id == iduseraccount).AccessRole;
                if (userrole.Contains("[" + role + "]")) return true;
                else return false;
            }
            catch
            {
                return false;
            }

        }

        public static IQueryable GetName(this Table<Category> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Supplier> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<SupplierSC> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Customer> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Location> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Department> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<SubDepartment> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<SubDepartment> value, int iddepartment)
        {
            return from x in value
                   where x.Active == true
                   && x.idDepartment == iddepartment
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Group> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Priority> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Courier> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Product> value)
        {
            return from x in value
                   where x.Active == true
                   && x.StatusProduct != "Inactive"
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetNameAll(this Table<Product> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Code,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<Currency> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       Name = x.Symbol
                   };
        }

        public static IQueryable GetName(this Table<Terms> value)
        {
            return from x in value
                   select new
                   {
                       x.id,
                       x.Name
                   };
        }

        public static IQueryable GetName(this Table<UserAccount> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name,
                   };
        }

        public static IQueryable GetName(this Table<Salesman> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name,
                   };
        }

        public static IQueryable GetName(this Table<Tipe> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name,
                   };
        }

        public static IQueryable GetName(this Table<Fisik> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name,
                   };
        }

        public static IQueryable GetName(this Table<KategoriBahanPenolong> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name,
                   };
        }

        public static IQueryable GetName(this Table<StatusBahanPenolong> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       x.Name,
                   };
        }

        public static IQueryable GetName(this Table<BahanBaku> value)
        {
            return from x in value
                   where x.Active == true
                   && x.StatusBahanBaku != "Inactive"
                   select new
                   {
                       x.id,
                       Code = x.Kode,
                       Name = x.Nama
                   };
        }

        public static IQueryable GetName(this Table<BahanPenolong> value)
        {
            return from x in value
                   where x.Active == true
                   select new
                   {
                       x.id,
                       Code = x.Kode,
                       Name = x.Nama
                   };
        }

        //number
        public static string GetSalesOrderNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("SalesOrderPrefix");
                string suffix = db.GetConfiguration("SalesOrderSuffix");

                var query = (from x in db.SalesOrders
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch 
            {
                string prefix = db.GetConfiguration("SalesOrderPrefix");
                string suffix = db.GetConfiguration("SalesOrderSuffix");
                return prefix +  "000001" + suffix;
            }
        }

        public static string GetGeneralLetterNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("GeneralLetterPrefix");
                string suffix = db.GetConfiguration("GeneralLetterSuffix");

                var query = (from x in db.GeneralLetters
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("GeneralLetterPrefix");
                string suffix = db.GetConfiguration("GeneralLetterSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetInternalLetterNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("InternalLetterPrefix");
                string suffix = db.GetConfiguration("InternalLetterSuffix");

                var query = (from x in db.InternalLetters
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("InternalLetterPrefix");
                string suffix = db.GetConfiguration("InternalLetterSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetPPBNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("PPBPrefix");
                string suffix = db.GetConfiguration("PPBSuffix");

                var query = (from x in db.PPBs
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("PPBPrefix");
                string suffix = db.GetConfiguration("PPBSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetPPSNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("PPSPrefix");
                string suffix = db.GetConfiguration("PPSSuffix");

                var query = (from x in db.PPS
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("PPSPrefix");
                string suffix = db.GetConfiguration("PPSSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetAutoNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("AutoNumberPrefix");
                string suffix = db.GetConfiguration("AutoNumberSuffix");

                var query = (from x in db.AutoNumbers
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("AutoNumberPrefix");
                string suffix = db.GetConfiguration("AutoNumberSuffix");
                return prefix +  "000001" + suffix;
            }
        }

        public static string GetPOEXNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("POEXPrefix");
                string suffix = db.GetConfiguration("POEXSuffix");

                var query = (from x in db.AutoNumbers
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             && x.Status == "Expedisi"
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("POEXPrefix");
                string suffix = db.GetConfiguration("POEXSuffix");
                return prefix + "000001" + suffix;
            }
        }


        public static string GetStockAdjustmentNumber(this GreenChemDataContext db, int year, int month)
        {
            try
            {
                var query = (from x in db.AdjustStocks
                             where x.TransactionDate.Year == year && x.TransactionDate.Month == month
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(7, 4).ToInteger() + 1;
                return "AS-" + year.ToString().Substring(2, 2) + month.ToString("00") + newnumber.ToString("0000");
            }
            catch
            {
                return "AS-" + year.ToString().Substring(2, 2) + month.ToString("00") + "0001";
            }
        }

        public static string GetInquiryInNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("InquiryInPrefix");
                string suffix = db.GetConfiguration("InquiryInSuffix");

                var query = (from x in db.InquiryIns
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("InquiryInPrefix");
                string suffix = db.GetConfiguration("InquiryInSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetInquiryOutNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("InquiryOutPrefix");
                string suffix = db.GetConfiguration("InquiryOutSuffix");

                var query = (from x in db.InquiryOuts
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("InquiryOutPrefix");
                string suffix = db.GetConfiguration("InquiryOutSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetQuotationInNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("QuotationInPrefix");
                string suffix = db.GetConfiguration("QuotationInSuffix");

                var query = (from x in db.QuotationIns
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("QuotationInPrefix");
                string suffix = db.GetConfiguration("QuotationInSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetQuotationOutNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("QuotationOutPrefix");
                string suffix = db.GetConfiguration("QuotationOutSuffix");

                var query = (from x in db.QuotationOuts
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("QuotationOutPrefix");
                string suffix = db.GetConfiguration("QuotationOutSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetExpenseNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("ExpensePrefix");
                string suffix = db.GetConfiguration("ExpenseSuffix");

                var query = (from x in db.Expenses
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("ExpensePrefix");
                string suffix = db.GetConfiguration("ExpenseSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetCustomerSatisfactionNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("CustomerSatisfactionPrefix");
                string suffix = db.GetConfiguration("CustomerSatisfactionSuffix");

                var query = (from x in db.CustomerSatisfactions
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;
            }
            catch
            {
                string prefix = db.GetConfiguration("CustomerSatisfactionPrefix");
                string suffix = db.GetConfiguration("CustomerSatisfactionSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetCustomerVoiceNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("CustomerVoicePrefix").Safe();
                string suffix = db.GetConfiguration("CustomerVoiceSuffix").Safe();

                var query = (from x in db.CustomerVoices
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;
            }
            catch
            {
                string prefix = db.GetConfiguration("CustomerVoicePrefix");
                string suffix = db.GetConfiguration("CustomerVoicePrefix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetNomorPenyerahanProduksi(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("PenyerahanProduksiPrefix").Safe();
                string suffix = db.GetConfiguration("PenyerahanProduksiSuffix").Safe();

                var query = (from x in db.PenyerahanProduksis
                             where x.NomorPermintaanProduksi.StartsWith(prefix) && x.NomorPermintaanProduksi.EndsWith(suffix)
                             orderby x.NomorPermintaanProduksi descending
                             select x.NomorPermintaanProduksi).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;
            }
            catch
            {
                string prefix = db.GetConfiguration("PenyerahanProduksiPrefix");
                string suffix = db.GetConfiguration("PenyerahanProduksiPrefix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetNomorPermintaanProduksi(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("PermintaanProduksiPrefix").Safe();
                string suffix = db.GetConfiguration("PermintaanProduksiSuffix").Safe();

                var query = (from x in db.PermintaanProduksis
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;
            }
            catch
            {
                string prefix = db.GetConfiguration("PermintaanProduksiPrefix");
                string suffix = db.GetConfiguration("PermintaanProduksiSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetNomorTransferStock(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("TransferStockPrefix").Safe();
                string suffix = db.GetConfiguration("TransferStockSuffix").Safe();

                var query = (from x in db.TransferStocks
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;
            }
            catch
            {
                string prefix = db.GetConfiguration("TransferStockPrefix");
                string suffix = db.GetConfiguration("TransferStockSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetPOtoSupplierRegularNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("POtoSupplierRegularPrefix");
                string suffix = db.GetConfiguration("POtoSupplierRegularSuffix");

                var query = (from x in db.POtoSupplierRegulars
                             where x.NomorPO.StartsWith(prefix) && x.NomorPO.EndsWith(suffix)
                             orderby x.NomorPO descending
                             select x.NomorPO).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("POtoSupplierRegularPrefix");
                string suffix = db.GetConfiguration("POtoSupplierRegularSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetPOtoSupplierExpedisiNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("POtoSupplierExpedisiPrefix");
                string suffix = db.GetConfiguration("POtoSupplierExpedisiSuffix");

                var query = (from x in db.POtoSupplierExpedisis
                             where x.NomorPO.StartsWith(prefix) && x.NomorPO.EndsWith(suffix)
                             orderby x.NomorPO descending
                             select x.NomorPO).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("POtoSupplierExpedisiPrefix");
                string suffix = db.GetConfiguration("POtoSupplierExpedisiSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetInternalComplaintNumber(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("InternalComplaintPrefix");
                string suffix = db.GetConfiguration("InternalComplaintSuffix");

                var query = (from x in db.InternalComplaints
                             where x.TransactionNumber.StartsWith(prefix) && x.TransactionNumber.EndsWith(suffix)
                             orderby x.TransactionNumber descending
                             select x.TransactionNumber).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;

            }
            catch
            {
                string prefix = db.GetConfiguration("InternalComplaintPrefix");
                string suffix = db.GetConfiguration("InternalComplaintSuffix");
                return prefix + "000001" + suffix;
            }
        }

        public static string GetNomorPengembanganProduk(this GreenChemDataContext db)
        {
            try
            {
                string prefix = db.GetConfiguration("PrefixPengembanganProduk").Safe();
                string suffix = db.GetConfiguration("SuffixPengembanganProduk").Safe();

                var query = (from x in db.PengembanganProduks
                             where x.NoPRD.StartsWith(prefix) && x.NoPRD.EndsWith(suffix)
                             orderby x.NoPRD descending
                             select x.NoPRD).First();

                int newnumber = query.Substring(prefix.Length, 6).ToInteger() + 1;
                return prefix + newnumber.ToString("000000") + suffix;
            }
            catch
            {
                string prefix = db.GetConfiguration("PrefixPengembanganProduk");
                string suffix = db.GetConfiguration("SuffixPengembanganProduk");
                return prefix + "000001" + suffix;
            }
        }

        public static string NumberToWords(this int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
    }
}
