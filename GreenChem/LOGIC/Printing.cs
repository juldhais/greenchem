﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using JulFunctions;

namespace GreenChem
{
    public static class Printing
    {
        public static void SalesOrder(int id)
        {
            GreenChemDataContext db = new GreenChemDataContext();
            var salesorder = db.SalesOrders.First(x => x.id == id);
            List<ReportSalesOrder> list = new List<ReportSalesOrder>();

            foreach (var item in salesorder.DetailSalesOrders)
            {
                ReportSalesOrder detail = new ReportSalesOrder();
                detail.TransactionNumber = salesorder.TransactionNumber;
                detail.InvoiceNumber = salesorder.InvoiceNumber;
                detail.TransactionDate = salesorder.DateOrder;
                detail.DueDate = salesorder.DueDate;
                detail.Status = salesorder.Status;
                detail.Remarks = salesorder.Remarks;
                detail.CustomerCode = salesorder.Customer.Code;
                detail.CustomerName = salesorder.Customer.Name;
                detail.CustomerAddress = salesorder.Customer.Address;
                detail.CustomerPhone = salesorder.Customer.Phone;
                detail.TransactionSubtotal = salesorder.SubtotalOrder;
                detail.TransactionDiscount = salesorder.DiscountOrder;
                detail.TransactionCost = salesorder.CostOrder;
                detail.TransactionTax = salesorder.TaxOrder;
                detail.TransactionTotal = salesorder.TotalOrder;
                detail.FOB = salesorder.FOB;
                detail.ShipVia = salesorder.ShipVia;
                detail.ShipAddress = salesorder.Customer.ShipAddress;
                detail.Terms = salesorder.Terms;
                detail.SubtotalAfterDiscount = salesorder.SubtotalOrder - salesorder.DiscountOrder;
                detail.SubtotalAfterTax = detail.SubtotalAfterDiscount + detail.TransactionTax;

                detail.ProductCode = item.Product.Code;
                detail.ProductName = item.Product.Name;
                detail.Quantity = item.QuantityOrder;
                detail.UoM = item.Product.UoM;
                detail.Price = item.PriceOrder;
                detail.Discount = item.DiscountOrder;
                detail.Subtotal = item.SubtotalOrder;
                detail.Packaging = item.Packaging;
                detail.NoBatch = item.NoBatch;

                detail.Signature1 = db.GetConfiguration("Signature1");
                detail.Signature2 = db.GetConfiguration("Signature2");
                detail.Signature3 = db.GetConfiguration("Signature3");
                detail.Signature4 = db.GetConfiguration("Signature4");
                detail.Position1 = db.GetConfiguration("Position1");
                detail.Position2 = db.GetConfiguration("Position2");
                detail.Position3 = db.GetConfiguration("Position3");
                detail.Position4 = db.GetConfiguration("Position4");

                detail.DatePrinted = DateTime.Now;
                detail.PrintedBy = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount).Name;
                detail.PrintCount = db.GetPrintCount("SalesOrder", salesorder.TransactionNumber);

                list.Add(detail);
            }

            db.UpdatePrintCount("SalesOrder", salesorder.TransactionNumber);

            MenuTransaction.rptSalesOrder report = new MenuTransaction.rptSalesOrder();
            if (File.Exists("SalesOrder.repx")) report.LoadLayout("SalesOrder.repx");
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        public static void DeliveryOrder(int id)
        {
            GreenChemDataContext db = new GreenChemDataContext();
            var salesorder = db.SalesOrders.First(x => x.id == id);
            List<ReportSalesOrder> list = new List<ReportSalesOrder>();

            foreach (var item in salesorder.DetailSalesOrders)
            {
                ReportSalesOrder detail = new ReportSalesOrder();
                detail.TransactionNumber = salesorder.TransactionNumber;
                detail.InvoiceNumber = salesorder.InvoiceNumber;
                detail.DONumber = salesorder.DONumber;
                detail.TransactionDate = salesorder.DateDelivered.Value;
                detail.Status = salesorder.Status;
                detail.Remarks = salesorder.Remarks;
                detail.CustomerCode = salesorder.Customer.Code;
                detail.CustomerName = salesorder.Customer.Name;
                detail.CustomerAddress = salesorder.Customer.Address;
                detail.CustomerPhone = salesorder.Customer.Phone;
                detail.CustomerFax = salesorder.Customer.Fax;
                detail.TransactionSubtotal = salesorder.SubtotalDelivered;
                detail.TransactionDiscount = salesorder.DiscountDelivered;
                detail.TransactionCost = salesorder.CostDelivered;
                detail.TransactionTax = salesorder.TaxDelivered;
                detail.TransactionTotal = salesorder.TotalDelivered;
                detail.FOB = salesorder.FOB;
                detail.ShipVia = salesorder.ShipVia;
                detail.ShipAddress = salesorder.Customer.ShipAddress;
                detail.Terms = salesorder.Terms;
                detail.ShipTo = detail.ShipAddress;
                detail.PONumber = salesorder.Remarks;
                detail.SubtotalAfterDiscount = salesorder.SubtotalDelivered - salesorder.DiscountDelivered;
                detail.SubtotalAfterTax = detail.SubtotalAfterDiscount + detail.TransactionTax;

                detail.ProductCode = item.Product.Code;
                detail.ProductName = item.Product.Name;
                detail.Quantity = item.QuantityDelivered;
                detail.UoM = item.Product.UoM;
                detail.Price = item.PriceDelivered;
                detail.Discount = item.DiscountDelivered;
                detail.Subtotal = item.SubtotalDelivered;
                detail.NoBatch = item.NoBatch;
                detail.Packaging = item.Packaging;

                detail.Signature1 = db.GetConfiguration("Signature1");
                detail.Signature2 = db.GetConfiguration("Signature2");
                detail.Signature3 = db.GetConfiguration("Signature3");
                detail.Signature4 = db.GetConfiguration("Signature4");
                detail.Position1 = db.GetConfiguration("Position1");
                detail.Position2 = db.GetConfiguration("Position2");
                detail.Position3 = db.GetConfiguration("Position3");
                detail.Position4 = db.GetConfiguration("Position4");

                detail.DatePrinted = DateTime.Now;
                detail.PrintedBy = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount).Name;
                detail.PrintCount = db.GetPrintCount("DeliveryOrder", salesorder.TransactionNumber);

                list.Add(detail);
            }

            db.UpdatePrintCount("DeliveryOrder", salesorder.TransactionNumber);

            MenuTransaction.rptDeliveryOrder report = new MenuTransaction.rptDeliveryOrder();
            if (File.Exists("DeliveryOrder.repx")) report.LoadLayout("DeliveryOrder.repx");
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        public static void SalesInvoice(int id)
        {
            GreenChemDataContext db = new GreenChemDataContext();
            var salesorder = db.SalesOrders.First(x => x.id == id);
            List<ReportSalesOrder> list = new List<ReportSalesOrder>();

            foreach (var item in salesorder.DetailSalesOrders)
            {
                ReportSalesOrder detail = new ReportSalesOrder();
                detail.TransactionNumber = salesorder.TransactionNumber;
                detail.InvoiceNumber = salesorder.InvoiceNumber;
                detail.TransactionDate = salesorder.DateInvoice.Value;
                detail.Status = salesorder.Status;
                detail.Remarks = salesorder.Remarks;
                detail.CustomerCode = salesorder.Customer.Code;
                detail.CustomerName = salesorder.Customer.Name;
                detail.CustomerAddress = salesorder.Customer.Address;
                detail.CustomerPhone = salesorder.Customer.Phone;
                detail.CustomerFax = salesorder.Customer.Fax;
                detail.TransactionSubtotal = salesorder.SubtotalDelivered;
                detail.TransactionDiscount = salesorder.DiscountDelivered;
                detail.TransactionCost = salesorder.CostDelivered;
                detail.TransactionTax = salesorder.TaxDelivered;
                detail.TransactionTotal = salesorder.TotalDelivered;
                detail.Currency = db.Currencies.First(x => x.id == salesorder.idCurrency).Symbol;
                detail.FOB = salesorder.FOB;
                detail.ShipVia = salesorder.ShipVia;
                detail.ShipAddress = salesorder.Customer.ShipAddress;
                detail.Terms = salesorder.Terms;
                detail.BillTo = detail.CustomerAddress;
                detail.ShipTo = detail.ShipAddress;
                detail.ShipDate = salesorder.DateDelivered.Value;
                detail.PONumber = salesorder.Remarks;
                detail.TOTAL = detail.TransactionTotal;
                detail.SubtotalAfterDiscount = detail.TransactionSubtotal - detail.TransactionDiscount;
                detail.SubtotalAfterTax = detail.SubtotalAfterDiscount + detail.TransactionTax;
                detail.Terbilang = BusinessLogic.NumberToWords(detail.TransactionTotal.ToInteger()).ToUpper();

                detail.ProductCode = item.Product.Code;
                detail.ProductName = item.Product.Name;
                detail.Quantity = item.QuantityDelivered;
                detail.UoM = item.Product.UoM;
                detail.Price = item.PriceDelivered;
                detail.Discount = item.DiscountDelivered;
                detail.Subtotal = item.SubtotalDelivered;
                detail.Packaging = item.Packaging;
                detail.NoBatch = item.NoBatch;

                detail.Signature1 = db.GetConfiguration("Signature1");
                detail.Signature2 = db.GetConfiguration("Signature2");
                detail.Signature3 = db.GetConfiguration("Signature3");
                detail.Signature4 = db.GetConfiguration("Signature4");
                detail.Position1 = db.GetConfiguration("Position1");
                detail.Position2 = db.GetConfiguration("Position2");
                detail.Position3 = db.GetConfiguration("Position3");
                detail.Position4 = db.GetConfiguration("Position4");

                detail.DatePrinted = DateTime.Now;
                detail.PrintedBy = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount).Name;
                detail.PrintCount = db.GetPrintCount("SalesInvoice", salesorder.TransactionNumber);

                list.Add(detail);
            }

            db.UpdatePrintCount("SalesInvoice", salesorder.TransactionNumber);

            MenuTransaction.rptSalesInvoice report = new MenuTransaction.rptSalesInvoice();
            if (File.Exists("SalesInvoice.repx")) report.LoadLayout("SalesInvoice.repx");
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }

        public static void TransferStock(string number)
        {
            GreenChemDataContext db = new GreenChemDataContext();
            var query = from x in db.TransferStocks
                        join y in db.Couriers on x.idCourier equals y.id
                        where x.TransactionNumber == number
                        select new
                        {
                            x.TransactionNumber,
                            x.TransactionDate,
                            x.Remarks,
                            ProductCode = x.Product.Code,
                            ProductName = x.Product.Name,
                            x.Cost,
                            Subtotal = x.Quantity * x.Cost,
                            UoM = x.Product.UoM,
                            Courier = x.idCourier != null ? db.Couriers.First(a => a.id == x.idCourier).Name : "",
                            x.FromLocation,
                            x.ToLocation,
                            x.Quantity,
                            x.ShipVia,
                            x.FOB,
                            x.NoBatch,
                            x.Packaging,
                            x.ShipAddress,
                            x.NoPKB,
                            ExpeditionPhone = y.Phone,
                            ExpeditionPIC = y.PIC,
                            ExpeditionAddress = y.Address,
                            x.DONumber
                        };

            List<ReportTransferStock> list = new List<ReportTransferStock>();
            foreach (var item in query)
            {
                ReportTransferStock detail = new ReportTransferStock();
                detail.TransactionNumber = item.TransactionNumber;
                detail.TransactionDate = item.TransactionDate;
                detail.Remarks = item.Remarks;
                detail.ProductCode = item.ProductCode;
                detail.ProductName = item.ProductName;
                detail.Cost = item.Cost;
                detail.UoM = item.UoM;
                detail.Quantity = item.Quantity.ToDecimal();
                detail.Subtotal = item.Subtotal.ToDecimal();
                detail.Courier = item.Courier;
                detail.FromLocation = item.FromLocation;
                detail.ToLocation = item.ToLocation;
                detail.ShipVia = item.ShipVia;
                detail.FOB = item.FOB;
                detail.NoBatch = item.NoBatch;
                detail.Packaging = item.Packaging;
                detail.ShipAddress = item.ShipAddress;
                detail.DONumber = item.DONumber;

                detail.Signature1 = db.GetConfiguration("Signature1");
                detail.Signature2 = db.GetConfiguration("Signature2");
                detail.Signature3 = db.GetConfiguration("Signature3");
                detail.Signature4 = db.GetConfiguration("Signature4");
                detail.Position1 = db.GetConfiguration("Position1");
                detail.Position2 = db.GetConfiguration("Position2");
                detail.Position3 = db.GetConfiguration("Position3");
                detail.Position4 = db.GetConfiguration("Position4");

                detail.ExpeditionAddress = item.ExpeditionAddress;
                detail.ExpeditionPhone = item.ExpeditionPhone;
                detail.ExpeditionPIC = item.ExpeditionPIC;

                detail.DatePrinted = DateTime.Now;
                detail.PrintedBy = db.UserAccounts.First(x => x.id == frmUtama._iduseraccount).Name;
                detail.PrintCount = db.GetPrintCount("TransferStock", item.TransactionNumber);

                list.Add(detail);
            }

            db.UpdatePrintCount("TransferStock", number);

            MenuTransaction.rptTransferStock report = new MenuTransaction.rptTransferStock();
            if (File.Exists("rptTransferStock.repx")) report.LoadLayout("rptTransferStock.repx");
            report.DataSource = list;
            report.ShowPreview();
            report.PrintingSystem.ExecCommand(DevExpress.XtraPrinting.PrintingSystemCommand.ZoomToPageWidth);
        }
    }
}
